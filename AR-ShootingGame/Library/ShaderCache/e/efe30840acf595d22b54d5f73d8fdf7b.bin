hI  <Q                      
   CAP_SQUARE     STEREO_MULTIVIEW_ON     {C  #ifdef VERTEX
#version 300 es
#extension GL_OVR_multiview2 : require

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 _ScreenParams;
uniform 	vec4 unity_OrthoParams;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	int _ScaleMode;
uniform 	vec3 _PointStart;
uniform 	vec3 _PointEnd;
uniform 	mediump float _Thickness;
uniform 	int _ThicknessSpace;
uniform 	mediump float _DashSize;
uniform 	mediump float _DashOffset;
uniform 	mediump float _DashSpacing;
uniform 	int _DashSpace;
uniform 	int _DashSnap;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(0) uniform UnityStereoGlobals {
#endif
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_StereoMatrixP[8];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_StereoMatrixV[8];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_StereoMatrixInvV[8];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_StereoMatrixVP[8];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_StereoCameraProjection[8];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_StereoCameraInvProjection[8];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_StereoWorldToCamera[8];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_StereoCameraToWorld[8];
	UNITY_UNIFORM vec3 unity_StereoWorldSpaceCameraPos[2];
	UNITY_UNIFORM vec4 unity_StereoScaleOffset[2];
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(1) uniform UnityStereoEyeIndices {
#endif
	UNITY_UNIFORM vec4 unity_StereoEyeIndices[2];
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
layout(num_views = 2) in;
in highp vec4 in_POSITION0;
out mediump vec4 vs_TEXCOORD1;
out mediump float vs_TEXCOORD0;
out highp float vs_BLENDWEIGHT0;
vec3 u_xlat0;
uint u_xlatu0;
bvec3 u_xlatb0;
vec4 u_xlat1;
vec4 u_xlat2;
bool u_xlatb2;
mediump vec2 u_xlat16_3;
mediump vec4 u_xlat16_4;
vec3 u_xlat5;
int u_xlati5;
bool u_xlatb5;
vec2 u_xlat6;
vec3 u_xlat7;
mediump vec3 u_xlat16_7;
vec3 u_xlat8;
mediump vec3 u_xlat16_9;
mediump vec3 u_xlat16_10;
vec3 u_xlat11;
mediump vec3 u_xlat16_11;
mediump vec3 u_xlat16_12;
mediump vec3 u_xlat16_13;
mediump vec3 u_xlat16_14;
vec3 u_xlat15;
float u_xlat16;
bool u_xlatb16;
mediump float u_xlat16_20;
vec2 u_xlat21;
float u_xlat23;
float u_xlat32;
mediump float u_xlat16_35;
float u_xlat36;
mediump float u_xlat16_36;
float u_xlat37;
bool u_xlatb37;
vec2 u_xlat38;
mediump float u_xlat16_39;
float u_xlat48;
bool u_xlatb50;
mediump float u_xlat16_52;
float u_xlat53;
int u_xlati53;
uint u_xlatu53;
bool u_xlatb53;
void main()
{
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[1].xyz * _PointStart.yyy;
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[0].xyz * _PointStart.xxx + u_xlat0.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].xyz * _PointStart.zzz + u_xlat0.xyz;
    u_xlat0.xyz = u_xlat0.xyz + hlslcc_mtx4x4unity_ObjectToWorld[3].xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_ObjectToWorld[1].xyz * _PointEnd.yyy;
    u_xlat1.xyz = hlslcc_mtx4x4unity_ObjectToWorld[0].xyz * _PointEnd.xxx + u_xlat1.xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].xyz * _PointEnd.zzz + u_xlat1.xyz;
    u_xlat2.xyz = u_xlat1.xyz + hlslcc_mtx4x4unity_ObjectToWorld[3].xyz;
    u_xlat16_3.x = dot(hlslcc_mtx4x4unity_ObjectToWorld[0].xyz, hlslcc_mtx4x4unity_ObjectToWorld[0].xyz);
    u_xlat16_3.y = dot(hlslcc_mtx4x4unity_ObjectToWorld[1].xyz, hlslcc_mtx4x4unity_ObjectToWorld[1].xyz);
    u_xlat16_3.xy = sqrt(u_xlat16_3.xy);
    u_xlat16_35 = dot(hlslcc_mtx4x4unity_ObjectToWorld[2].xyz, hlslcc_mtx4x4unity_ObjectToWorld[2].xyz);
    u_xlat16_4.x = sqrt(u_xlat16_35);
    u_xlat16_20 = u_xlat16_3.y + u_xlat16_3.x;
    u_xlat16_4.x = u_xlat16_4.x + u_xlat16_20;
    u_xlat16_4.x = u_xlat16_4.x * 0.333333343;
    u_xlat16_20 = (_ScaleMode != 0) ? 1.0 : u_xlat16_4.x;
    u_xlat16_20 = u_xlat16_20 * _Thickness;
    u_xlat5.xyz = (-u_xlat0.xyz) + u_xlat2.xyz;
    u_xlat48 = dot(u_xlat5.xyz, u_xlat5.xyz);
    u_xlat6.y = sqrt(u_xlat48);
    u_xlat48 = in_POSITION0.z;
#ifdef UNITY_ADRENO_ES3
    u_xlat48 = min(max(u_xlat48, 0.0), 1.0);
#else
    u_xlat48 = clamp(u_xlat48, 0.0, 1.0);
#endif
#ifdef UNITY_ADRENO_ES3
    u_xlatb50 = !!(0.5<u_xlat48);
#else
    u_xlatb50 = 0.5<u_xlat48;
#endif
    u_xlat1.xyz = (bool(u_xlatb50)) ? u_xlat2.xyz : u_xlat0.xyz;
#ifdef UNITY_ADRENO_ES3
    u_xlatb2 = !!(unity_OrthoParams.w==1.0);
#else
    u_xlatb2 = unity_OrthoParams.w==1.0;
#endif
    if(u_xlatb2){
        u_xlati53 = int(int(gl_ViewID_OVR) << 2);
        u_xlat16_7.xyz = (-hlslcc_mtx4x4unity_StereoMatrixInvV[(u_xlati53 + 2)].xyz);
    } else {
        u_xlatu53 = gl_ViewID_OVR;
        u_xlat8.xyz = (-u_xlat1.xyz) + unity_StereoWorldSpaceCameraPos[int(u_xlatu53)].xyz;
        u_xlat53 = dot(u_xlat8.xyz, u_xlat8.xyz);
        u_xlat53 = inversesqrt(u_xlat53);
        u_xlat7.xyz = vec3(u_xlat53) * u_xlat8.xyz;
        u_xlat16_7.xyz = u_xlat7.xyz;
    }
#ifdef UNITY_ADRENO_ES3
    u_xlatb53 = !!(u_xlat6.y<9.99999975e-05);
#else
    u_xlatb53 = u_xlat6.y<9.99999975e-05;
#endif
    if(u_xlatb53){
        u_xlati53 = int(int(gl_ViewID_OVR) << 2);
        u_xlat16_9.xyz = hlslcc_mtx4x4unity_StereoMatrixInvV[u_xlati53].xyz;
        u_xlat16_10.xyz = hlslcc_mtx4x4unity_StereoMatrixInvV[(u_xlati53 + 1)].xyz;
        u_xlat16_11.xyz = hlslcc_mtx4x4unity_StereoMatrixInvV[(u_xlati53 + 2)].xyz;
        u_xlat16_12.xyz = hlslcc_mtx4x4unity_StereoMatrixInvV[(u_xlati53 + 1)].xyz;
    } else {
        u_xlat11.xyz = u_xlat5.xyz / u_xlat6.yyy;
#ifdef UNITY_ADRENO_ES3
        u_xlatb5 = !!(abs(u_xlat11.y)>=0.99000001);
#else
        u_xlatb5 = abs(u_xlat11.y)>=0.99000001;
#endif
        u_xlat16_13.xyz = (bool(u_xlatb5)) ? vec3(0.0, 1.0, 0.0) : vec3(0.0, 0.0, 1.0);
        u_xlat16_14.xyz = u_xlat11.yzx * u_xlat16_13.xyz;
        u_xlat16_13.xyz = u_xlat16_13.zxy * u_xlat11.zxy + (-u_xlat16_14.xyz);
        u_xlat16_36 = dot(u_xlat16_13.xyz, u_xlat16_13.xyz);
        u_xlat16_36 = inversesqrt(u_xlat16_36);
        u_xlat16_10.xyz = vec3(u_xlat16_36) * u_xlat16_13.xyz;
        u_xlat16_13.xyz = u_xlat11.yzx * u_xlat16_10.zxy;
        u_xlat16_9.xyz = u_xlat16_10.yzx * u_xlat11.zxy + (-u_xlat16_13.xyz);
        u_xlat16_13.xyz = (-u_xlat16_7.zxy) * u_xlat11.yzx;
        u_xlat16_7.xyz = (-u_xlat16_7.yzx) * u_xlat11.zxy + (-u_xlat16_13.xyz);
        u_xlat16_36 = dot(u_xlat16_7.xyz, u_xlat16_7.xyz);
        u_xlat16_36 = inversesqrt(u_xlat16_36);
        u_xlat16_12.xyz = vec3(u_xlat16_36) * u_xlat16_7.xyz;
        u_xlat16_11.xyz = u_xlat11.xyz;
    }
    u_xlati5 = int(int(gl_ViewID_OVR) << 2);
    u_xlat2.x = hlslcc_mtx4x4unity_StereoMatrixVP[u_xlati5].w;
    u_xlat2.y = hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati5 + 1)].w;
    u_xlat2.z = hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati5 + 2)].w;
    u_xlat2.w = hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati5 + 3)].w;
    u_xlat1.w = 1.0;
    u_xlat21.x = dot(u_xlat2, u_xlat1);
    u_xlat8.x = hlslcc_mtx4x4unity_StereoMatrixVP[u_xlati5].x;
    u_xlat8.y = hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati5 + 1)].x;
    u_xlat8.z = hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati5 + 2)].x;
    u_xlat37 = dot(u_xlat8.xyz, u_xlat16_12.xyz);
    u_xlat8.x = u_xlat37 / u_xlat21.x;
    u_xlat15.x = hlslcc_mtx4x4unity_StereoMatrixVP[u_xlati5].y;
    u_xlat15.y = hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati5 + 1)].y;
    u_xlat15.z = hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati5 + 2)].y;
    u_xlat37 = dot(u_xlat15.xyz, u_xlat16_12.xyz);
    u_xlat8.y = u_xlat37 / u_xlat21.x;
    u_xlat21.xy = u_xlat8.xy * _ScreenParams.xy;
    u_xlat21.x = dot(u_xlat21.xy, u_xlat21.xy);
    u_xlat21.x = sqrt(u_xlat21.x);
    u_xlat21.x = u_xlat21.x * 0.5;
    switch(_ThicknessSpace){
        case 0:
            u_xlat53 = u_xlat16_20 * u_xlat21.x;
            break;
        case 1:
            u_xlat53 = u_xlat16_20;
            break;
        case 2:
            u_xlat37 = min(_ScreenParams.y, _ScreenParams.x);
            u_xlat38.x = u_xlat16_20 * 0.00999999978;
            u_xlat53 = u_xlat37 * u_xlat38.x;
            break;
        default:
            u_xlat53 = 0.0;
            break;
    }
    u_xlat16_20 = max(u_xlat53, 1.0);
    u_xlat16_36 = u_xlat16_20 / u_xlat21.x;
    u_xlat16_52 = u_xlat16_36 * 0.5;
    u_xlat38.xy = vec2(u_xlat16_52) * in_POSITION0.xy;
    u_xlat8.xyz = u_xlat38.xxx * u_xlat16_9.xyz + u_xlat1.xyz;
    u_xlat8.xyz = u_xlat38.yyy * u_xlat16_10.xyz + u_xlat8.xyz;
    u_xlat37 = u_xlat48 * 2.0 + -1.0;
    u_xlat15.xyz = u_xlat16_11.xyz * vec3(u_xlat37);
    u_xlat8.xyz = u_xlat15.xyz * vec3(u_xlat16_52) + u_xlat8.xyz;
    u_xlat37 = u_xlat16_36 / u_xlat6.y;
    u_xlat37 = u_xlat37 + 1.0;
    u_xlat38.x = (-u_xlat16_52) / u_xlat6.y;
    u_xlat48 = u_xlat37 * u_xlat48 + u_xlat38.x;
#ifdef UNITY_ADRENO_ES3
    u_xlatb37 = !!(6.10351563e-05<_DashSize);
#else
    u_xlatb37 = 6.10351563e-05<_DashSize;
#endif
    if(u_xlatb37){
        u_xlat0.xyz = (-u_xlat0.xyz) + u_xlat8.xyz;
        u_xlat6.x = dot(u_xlat16_11.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
        u_xlatb0.x = !!(_ScaleMode==0);
#else
        u_xlatb0.x = _ScaleMode==0;
#endif
#ifdef UNITY_ADRENO_ES3
        u_xlatb16 = !!(_DashSpace!=int(0xFFFFFFFEu));
#else
        u_xlatb16 = _DashSpace!=int(0xFFFFFFFEu);
#endif
        u_xlatb0.x = u_xlatb16 && u_xlatb0.x;
        u_xlat16_4.xw = u_xlat16_4.xx * vec2(_DashSize, _DashSpacing);
        u_xlat16_4.xw = (u_xlatb0.x) ? u_xlat16_4.xw : vec2(_DashSize, _DashSpacing);
        u_xlatb0.xyz = equal(ivec4(_DashSpace, _DashSpace, _DashSnap, _DashSpace), ivec4(int(0xFFFFFFFFu), int(0xFFFFFFFEu), 3, 0)).xyz;
        if(u_xlatb0.x){
            switch(_ThicknessSpace){
                case 0:
                    u_xlat16_7.x = u_xlat6.x;
                    break;
                case 1:
                    u_xlat16_7.x = u_xlat21.x * u_xlat6.x;
                    break;
                case 2:
                    u_xlat16_39 = u_xlat21.x * u_xlat6.x;
                    u_xlat0.x = u_xlat16_39 * 100.0;
                    u_xlat37 = min(_ScreenParams.y, _ScreenParams.x);
                    u_xlat7.x = u_xlat0.x / u_xlat37;
                    u_xlat16_7.x = u_xlat7.x;
                    break;
                default:
                    u_xlat16_7.x = 0.0;
                    break;
            }
            switch(_ThicknessSpace){
                case 0:
                    u_xlat16_7.y = u_xlat6.y;
                    break;
                case 1:
                    u_xlat16_7.y = u_xlat21.x * u_xlat6.y;
                    break;
                case 2:
                    u_xlat16_39 = u_xlat21.x * u_xlat6.y;
                    u_xlat0.x = u_xlat16_39 * 100.0;
                    u_xlat21.x = min(_ScreenParams.y, _ScreenParams.x);
                    u_xlat23 = u_xlat0.x / u_xlat21.x;
                    u_xlat16_7.y = u_xlat23;
                    break;
                default:
                    u_xlat16_7.y = 0.0;
                    break;
            }
            switch(_ThicknessSpace){
                case 0:
                    break;
                case 1:
                    u_xlat16_36 = u_xlat16_20;
                    break;
                case 2:
                    u_xlat0.x = u_xlat16_20 * 100.0;
                    u_xlat21.x = min(_ScreenParams.y, _ScreenParams.x);
                    u_xlat36 = u_xlat0.x / u_xlat21.x;
                    u_xlat16_36 = u_xlat36;
                    break;
                default:
                    u_xlat16_36 = 0.0;
                    break;
            }
            u_xlat6.xy = u_xlat16_7.xy;
        }
        if(u_xlatb0.y){
            switch(_DashSnap){
                case 0:
                    break;
                case 1:
                case 3:
                    u_xlat16_20 = roundEven(u_xlat16_4.x);
                    u_xlat16_4.x = max(u_xlat16_20, 1.0);
                    break;
                case 2:
                    u_xlat16_20 = u_xlat16_4.w + u_xlat16_4.x;
                    u_xlat16_20 = roundEven(u_xlat16_20);
                    u_xlat16_20 = max(u_xlat16_20, 1.0);
                    u_xlat16_4.x = (-u_xlat16_4.w) + u_xlat16_20;
                    break;
                default:
                    u_xlat16_4.x = 0.0;
                    break;
            }
            u_xlat0.x = u_xlat16_4.x;
            u_xlat15.y = u_xlat16_4.w;
        } else {
            u_xlat16_4.x = u_xlat16_4.w + u_xlat16_4.x;
            u_xlat15.y = u_xlat16_4.w / u_xlat16_4.x;
            u_xlat16 = u_xlat6.y / u_xlat16_4.x;
            switch(_DashSnap){
                case 0:
                    u_xlat16_4.x = u_xlat16;
                    break;
                case 1:
                case 3:
                    u_xlat16_20 = roundEven(u_xlat16);
                    u_xlat16_4.x = max(u_xlat16_20, 1.0);
                    break;
                case 2:
                    u_xlat16_20 = u_xlat15.y + u_xlat16;
                    u_xlat16_20 = roundEven(u_xlat16_20);
                    u_xlat16_20 = max(u_xlat16_20, 1.0);
                    u_xlat16_4.x = (-u_xlat15.y) + u_xlat16_20;
                    break;
                default:
                    u_xlat16_4.x = 0.0;
                    break;
            }
            u_xlat0.x = u_xlat16_4.x;
        }
        u_xlat16 = (-u_xlat15.y) + 1.0;
        u_xlat21.x = (-u_xlat16) * 0.5 + _DashOffset;
        u_xlat16_4.x = (u_xlatb0.z) ? u_xlat21.x : _DashOffset;
        u_xlat32 = u_xlat0.x * u_xlat16_36;
        u_xlat15.z = u_xlat32 / u_xlat6.y;
        u_xlat32 = u_xlat6.x / u_xlat6.y;
        u_xlat0.x = u_xlat32 * u_xlat0.x + (-u_xlat16_4.x);
        u_xlat15.x = (-u_xlat16) * 0.5 + u_xlat0.x;
        u_xlat16_4.xyz = u_xlat15.xyz;
    } else {
        u_xlat16_4.x = float(0.0);
        u_xlat16_4.y = float(0.0);
        u_xlat16_4.z = float(0.0);
    }
    u_xlat1 = u_xlat8.yyyy * hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati5 + 1)];
    u_xlat1 = hlslcc_mtx4x4unity_StereoMatrixVP[u_xlati5] * u_xlat8.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati5 + 2)] * u_xlat8.zzzz + u_xlat1;
    gl_Position = u_xlat1 + hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati5 + 3)];
    vs_TEXCOORD1.xyz = u_xlat16_4.xyz;
    vs_TEXCOORD1.w = u_xlat53;
    vs_TEXCOORD0 = u_xlat48;
    u_xlatu0 = gl_ViewID_OVR;
    vs_BLENDWEIGHT0 = unity_StereoEyeIndices[int(u_xlatu0)].x;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp float;
precision highp int;
#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	mediump vec4 _Color;
uniform 	mediump vec4 _ColorEnd;
in mediump vec4 vs_TEXCOORD1;
in mediump float vs_TEXCOORD0;
layout(location = 0) out mediump vec4 SV_Target0;
mediump vec4 u_xlat16_0;
float u_xlat1;
mediump vec4 u_xlat16_1;
float u_xlat2;
bool u_xlatb2;
mediump float u_xlat16_3;
bool u_xlatb4;
mediump float u_xlat16_6;
void main()
{
    u_xlat16_0.x = fract(vs_TEXCOORD1.x);
    u_xlat16_0.x = u_xlat16_0.x * 2.0 + -1.0;
    u_xlat16_0.x = abs(u_xlat16_0.x) + (-vs_TEXCOORD1.y);
    u_xlat16_3 = (-vs_TEXCOORD1.y) + 1.0;
    u_xlat16_0.x = u_xlat16_0.x / u_xlat16_3;
    u_xlat16_3 = dFdx(u_xlat16_0.x);
    u_xlat16_6 = dFdy(u_xlat16_0.x);
    u_xlat16_3 = abs(u_xlat16_6) + abs(u_xlat16_3);
    u_xlat16_0.x = u_xlat16_0.x / u_xlat16_3;
    u_xlat16_0.x = u_xlat16_0.x + 0.5;
#ifdef UNITY_ADRENO_ES3
    u_xlat16_0.x = min(max(u_xlat16_0.x, 0.0), 1.0);
#else
    u_xlat16_0.x = clamp(u_xlat16_0.x, 0.0, 1.0);
#endif
    u_xlat16_3 = vs_TEXCOORD1.w;
#ifdef UNITY_ADRENO_ES3
    u_xlat16_3 = min(max(u_xlat16_3, 0.0), 1.0);
#else
    u_xlat16_3 = clamp(u_xlat16_3, 0.0, 1.0);
#endif
    u_xlat1 = u_xlat16_3 * u_xlat16_0.x;
#ifdef UNITY_ADRENO_ES3
    u_xlatb4 = !!(vs_TEXCOORD1.y>=0.999938965);
#else
    u_xlatb4 = vs_TEXCOORD1.y>=0.999938965;
#endif
    u_xlat16_0.x = (u_xlatb4) ? 0.0 : u_xlat1;
    u_xlat16_3 = vs_TEXCOORD0;
#ifdef UNITY_ADRENO_ES3
    u_xlat16_3 = min(max(u_xlat16_3, 0.0), 1.0);
#else
    u_xlat16_3 = clamp(u_xlat16_3, 0.0, 1.0);
#endif
    u_xlat16_1 = (-_Color) + _ColorEnd;
    u_xlat16_1 = vec4(u_xlat16_3) * u_xlat16_1 + _Color;
    u_xlat16_3 = u_xlat16_0.x * u_xlat16_1.w + -6.10351563e-05;
    u_xlat2 = u_xlat16_0.x * u_xlat16_1.w;
    u_xlat16_0.xzw = u_xlat16_1.xyz + vec3(-1.0, -1.0, -1.0);
    u_xlat16_0.xzw = vec3(u_xlat2) * u_xlat16_0.xzw + vec3(1.0, 1.0, 1.0);
    SV_Target0.w = u_xlat2;
    u_xlat16_0.xzw = max(u_xlat16_0.xzw, vec3(6.10351563e-05, 6.10351563e-05, 6.10351563e-05));
    u_xlat16_0.xzw = vec3(1.0, 1.0, 1.0) / u_xlat16_0.xzw;
    SV_Target0.xyz = (-u_xlat16_0.xzw) + vec3(1.0, 1.0, 1.0);
#ifdef UNITY_ADRENO_ES3
    u_xlatb2 = !!(u_xlat16_3<0.0);
#else
    u_xlatb2 = u_xlat16_3<0.0;
#endif
    if(u_xlatb2){discard;}
    return;
}

#endif
                              $Globals          _Color                        	   _ColorEnd                               $Globals¨         _ScreenParams                            unity_OrthoParams                        
   _ScaleMode                   `      _PointStart                   p   	   _PointEnd                        
   _Thickness                          _ThicknessSpace                     	   _DashSize                           _DashOffset                         _DashSpacing                     
   _DashSpace                       	   _DashSnap                    €      unity_ObjectToWorld                             UnityStereoGlobals  @  
      unity_StereoWorldSpaceCameraPos                        unity_StereoScaleOffset                        unity_StereoMatrixP                        unity_StereoMatrixV                       unity_StereoMatrixInvV                        unity_StereoMatrixVP                     unity_StereoCameraProjection                      unity_StereoCameraInvProjection                      unity_StereoWorldToCamera                         unity_StereoCameraToWorld                            UnityStereoEyeIndices             unity_StereoEyeIndices                                 UnityStereoGlobals                UnityStereoEyeIndices                 