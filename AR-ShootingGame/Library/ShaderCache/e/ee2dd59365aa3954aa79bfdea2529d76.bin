*  <Q                         STEREO_MULTIVIEW_ON     0%  #ifdef VERTEX
#version 300 es
#extension GL_OVR_multiview2 : require

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 _ScreenParams;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	int _ScaleMode;
uniform 	mediump vec4 _Rect;
uniform 	int _FillType;
uniform 	int _FillSpace;
uniform 	vec4 _FillStart;
uniform 	vec3 _FillEnd;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(0) uniform UnityStereoGlobals {
#endif
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_StereoMatrixP[8];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_StereoMatrixV[8];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_StereoMatrixInvV[8];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_StereoMatrixVP[8];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_StereoCameraProjection[8];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_StereoCameraInvProjection[8];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_StereoWorldToCamera[8];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_StereoCameraToWorld[8];
	UNITY_UNIFORM vec3 unity_StereoWorldSpaceCameraPos[2];
	UNITY_UNIFORM vec4 unity_StereoScaleOffset[2];
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(1) uniform UnityStereoEyeIndices {
#endif
	UNITY_UNIFORM vec4 unity_StereoEyeIndices[2];
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
layout(num_views = 2) in;
in highp vec4 in_POSITION0;
out highp vec3 vs_TEXCOORD0;
out highp float vs_BLENDWEIGHT0;
out mediump vec4 vs_TEXCOORD1;
out mediump vec4 vs_TEXCOORD2;
vec4 u_xlat0;
mediump vec4 u_xlat16_0;
vec3 u_xlat1;
int u_xlati1;
uint u_xlatu1;
vec4 u_xlat2;
vec3 u_xlat3;
mediump vec3 u_xlat16_4;
vec4 u_xlat5;
mediump vec2 u_xlat16_5;
vec4 u_xlat6;
mediump vec2 u_xlat16_7;
vec2 u_xlat9;
float u_xlat17;
mediump vec2 u_xlat16_20;
mediump vec2 u_xlat16_21;
float u_xlat25;
bool u_xlatb25;
void main()
{
    u_xlat16_0.xy = _Rect.zw * vec2(0.5, 0.5) + _Rect.xy;
    u_xlat1.xyz = u_xlat16_0.yyy * hlslcc_mtx4x4unity_ObjectToWorld[1].xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_ObjectToWorld[0].xyz * u_xlat16_0.xxx + u_xlat1.xyz;
    u_xlat0.xyz = u_xlat1.xyz + hlslcc_mtx4x4unity_ObjectToWorld[3].xyz;
    u_xlat0.w = 1.0;
    u_xlati1 = int(int(gl_ViewID_OVR) << 2);
    u_xlat2.x = hlslcc_mtx4x4unity_StereoMatrixVP[u_xlati1].w;
    u_xlat2.y = hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati1 + 1)].w;
    u_xlat2.z = hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati1 + 2)].w;
    u_xlat2.w = hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati1 + 3)].w;
    u_xlat9.x = dot(u_xlat2, u_xlat0);
    u_xlat2.x = hlslcc_mtx4x4unity_StereoMatrixVP[u_xlati1].x;
    u_xlat2.y = hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati1 + 1)].x;
    u_xlat2.z = hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati1 + 2)].x;
    u_xlat17 = dot(u_xlat2.xyz, hlslcc_mtx4x4unity_StereoMatrixInvV[(u_xlati1 + 1)].xyz);
    u_xlat2.x = u_xlat17 / u_xlat9.x;
    u_xlat3.x = hlslcc_mtx4x4unity_StereoMatrixVP[u_xlati1].y;
    u_xlat3.y = hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati1 + 1)].y;
    u_xlat3.z = hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati1 + 2)].y;
    u_xlat17 = dot(u_xlat3.xyz, hlslcc_mtx4x4unity_StereoMatrixInvV[(u_xlati1 + 1)].xyz);
    u_xlat2.y = u_xlat17 / u_xlat9.x;
    u_xlat9.xy = u_xlat2.xy * _ScreenParams.xy;
    u_xlat9.x = dot(u_xlat9.xy, u_xlat9.xy);
    u_xlat9.x = sqrt(u_xlat9.x);
    u_xlat9.x = u_xlat9.x * 0.5;
    u_xlat16_4.x = dot(hlslcc_mtx4x4unity_ObjectToWorld[0].xyz, hlslcc_mtx4x4unity_ObjectToWorld[0].xyz);
    u_xlat16_4.z = dot(hlslcc_mtx4x4unity_ObjectToWorld[1].xyz, hlslcc_mtx4x4unity_ObjectToWorld[1].xyz);
    u_xlat16_4.xy = sqrt(u_xlat16_4.xz);
    u_xlat16_7.xy = (_ScaleMode != 0) ? vec2(1.0, 1.0) : u_xlat16_4.xy;
    u_xlat16_5.xy = (_ScaleMode != 0) ? u_xlat16_4.xy : vec2(1.0, 1.0);
    u_xlat16_4.xy = u_xlat16_7.xy;
    u_xlat16_4.xy = u_xlat9.xx * u_xlat16_4.xy;
    u_xlat16_4.xy = vec2(2.0, 2.0) / u_xlat16_4.xy;
    u_xlat16_20.xy = _Rect.xy * u_xlat16_5.xy + (-u_xlat16_4.xy);
    u_xlat16_0 = u_xlat16_5.xyxy * _Rect;
    u_xlat16_21.xy = u_xlat16_0.zw + u_xlat16_0.xy;
    u_xlat16_21.xy = u_xlat16_4.xy + u_xlat16_21.xy;
    u_xlat9.xy = (-u_xlat16_20.xy) + u_xlat16_21.xy;
    u_xlat2.xy = in_POSITION0.xy + vec2(1.0, 1.0);
    u_xlat2.xy = u_xlat2.xy * vec2(0.5, 0.5);
    u_xlat9.xy = u_xlat2.xy * u_xlat9.xy + u_xlat16_20.xy;
    u_xlat3.xy = u_xlat9.xy / u_xlat16_5.xy;
    u_xlat5 = u_xlat3.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat5 = hlslcc_mtx4x4unity_ObjectToWorld[0] * u_xlat3.xxxx + u_xlat5;
    u_xlat5 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat5;
    u_xlat5 = u_xlat5 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat6 = u_xlat5.yyyy * hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati1 + 1)];
    u_xlat6 = hlslcc_mtx4x4unity_StereoMatrixVP[u_xlati1] * u_xlat5.xxxx + u_xlat6;
    u_xlat6 = hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati1 + 2)] * u_xlat5.zzzz + u_xlat6;
    gl_Position = hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati1 + 3)] * u_xlat5.wwww + u_xlat6;
    u_xlat3.z = in_POSITION0.z;
    u_xlat1.xyz = (int(_FillSpace) != 0) ? u_xlat5.xyz : u_xlat3.xyz;
    u_xlat1.xyz = u_xlat1.xyz + (-_FillStart.xyz);
    u_xlat3.xyz = (-_FillStart.xyz) + _FillEnd.xyz;
    u_xlat25 = dot(u_xlat3.xyz, u_xlat1.xyz);
    u_xlat16_20.x = dot(u_xlat3.xyz, u_xlat3.xyz);
    u_xlat3.x = u_xlat25 / u_xlat16_20.x;
#ifdef UNITY_ADRENO_ES3
    u_xlatb25 = !!(_FillType==1);
#else
    u_xlatb25 = _FillType==1;
#endif
    u_xlat3.y = float(0.0);
    u_xlat3.z = float(0.0);
    u_xlat1.xyz = (bool(u_xlatb25)) ? u_xlat1.xyz : u_xlat3.xyz;
#ifdef UNITY_ADRENO_ES3
    u_xlatb25 = !!(_FillType!=int(0xFFFFFFFFu));
#else
    u_xlatb25 = _FillType!=int(0xFFFFFFFFu);
#endif
    vs_TEXCOORD0.xyz = bool(u_xlatb25) ? u_xlat1.xyz : vec3(0.0, 0.0, 0.0);
    u_xlatu1 = gl_ViewID_OVR;
    vs_BLENDWEIGHT0 = unity_StereoEyeIndices[int(u_xlatu1)].x;
    u_xlat16_20.xy = u_xlat16_0.zw * vec2(0.5, 0.5) + u_xlat16_4.xy;
    u_xlat16_4.xy = u_xlat16_0.zw * vec2(-0.5, -0.5) + (-u_xlat16_4.xy);
    vs_TEXCOORD2 = u_xlat16_0;
    u_xlat1.xy = (-u_xlat16_4.xy) + u_xlat16_20.xy;
    u_xlat1.xy = u_xlat2.xy * u_xlat1.xy + u_xlat16_4.xy;
    vs_TEXCOORD1.xy = u_xlat1.xy;
    vs_TEXCOORD1.zw = vec2(0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp float;
precision highp int;
#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	mediump vec4 _Color;
uniform 	mediump vec4 _ColorEnd;
uniform 	int _FillType;
uniform 	vec4 _FillStart;
in highp vec3 vs_TEXCOORD0;
in mediump vec4 vs_TEXCOORD1;
in mediump vec4 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec2 u_xlat0;
bool u_xlatb0;
vec2 u_xlat1;
mediump vec4 u_xlat16_1;
float u_xlat2;
mediump float u_xlat16_2;
float u_xlat3;
bool u_xlatb3;
vec2 u_xlat6;
void main()
{
    u_xlat0.xy = (-vs_TEXCOORD2.zw) * vec2(0.5, 0.5) + abs(vs_TEXCOORD1.xy);
    u_xlat6.xy = max(u_xlat0.xy, vec2(0.0, 0.0));
    u_xlat6.x = dot(u_xlat6.xy, u_xlat6.xy);
    u_xlat6.x = sqrt(u_xlat6.x);
    u_xlat0.x = max(u_xlat0.y, u_xlat0.x);
    u_xlat0.x = min(u_xlat0.x, 0.0);
    u_xlat0.x = u_xlat0.x + u_xlat6.x;
    u_xlat1.x = dFdx(u_xlat0.x);
    u_xlat1.y = dFdy(u_xlat0.x);
    u_xlat3 = dot(u_xlat1.xy, u_xlat1.xy);
    u_xlat3 = sqrt(u_xlat3);
    u_xlat3 = max(u_xlat3, 9.99999975e-06);
    u_xlat0.x = u_xlat0.x / u_xlat3;
    u_xlat0.x = u_xlat0.x + 0.5;
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat0.x = (-u_xlat0.x) + 1.0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb3 = !!(_FillType!=int(0xFFFFFFFFu));
#else
    u_xlatb3 = _FillType!=int(0xFFFFFFFFu);
#endif
    if(u_xlatb3){
        switch(_FillType){
            case 0:
                u_xlat2 = vs_TEXCOORD0.x;
#ifdef UNITY_ADRENO_ES3
                u_xlat2 = min(max(u_xlat2, 0.0), 1.0);
#else
                u_xlat2 = clamp(u_xlat2, 0.0, 1.0);
#endif
                u_xlat16_2 = u_xlat2;
                break;
            case 1:
                u_xlat3 = dot(vs_TEXCOORD0.xyz, vs_TEXCOORD0.xyz);
                u_xlat3 = sqrt(u_xlat3);
                u_xlat2 = u_xlat3 / _FillStart.w;
#ifdef UNITY_ADRENO_ES3
                u_xlat2 = min(max(u_xlat2, 0.0), 1.0);
#else
                u_xlat2 = clamp(u_xlat2, 0.0, 1.0);
#endif
                u_xlat16_2 = u_xlat2;
                break;
            default:
                u_xlat16_2 = 0.0;
                break;
        }
        u_xlat16_1 = (-_Color) + _ColorEnd;
        u_xlat16_1 = vec4(u_xlat16_2) * u_xlat16_1 + _Color;
    } else {
        u_xlat16_1 = _Color;
    }
    u_xlat3 = u_xlat0.x * u_xlat16_1.w;
    u_xlat16_2 = u_xlat0.x * u_xlat16_1.w + -6.10351563e-05;
#ifdef UNITY_ADRENO_ES3
    u_xlatb0 = !!(u_xlat16_2<0.0);
#else
    u_xlatb0 = u_xlat16_2<0.0;
#endif
    if(u_xlatb0){discard;}
    SV_Target0.xyz = vec3(u_xlat3) * u_xlat16_1.xyz;
    SV_Target0.w = u_xlat3;
    return;
}

#endif
                             $Globals@         _Color                        	   _ColorEnd                        	   _FillType                        
   _FillStart                    0          $Globals         _ScreenParams                         
   _ScaleMode                   P      _Rect                     `   	   _FillType                    p   
   _FillSpace                   t   
   _FillStart                          _FillEnd                        unity_ObjectToWorld                            UnityStereoGlobals  @  
      unity_StereoWorldSpaceCameraPos                        unity_StereoScaleOffset                        unity_StereoMatrixP                        unity_StereoMatrixV                       unity_StereoMatrixInvV                        unity_StereoMatrixVP                     unity_StereoCameraProjection                      unity_StereoCameraInvProjection                      unity_StereoWorldToCamera                         unity_StereoCameraToWorld                            UnityStereoEyeIndices             unity_StereoEyeIndices                                 UnityStereoGlobals                UnityStereoEyeIndices                 