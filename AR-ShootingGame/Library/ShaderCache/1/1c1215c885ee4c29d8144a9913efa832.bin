p1  <Q                           +1  #ifdef VERTEX
#version 100

uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 _ScreenParams;
uniform 	vec4 unity_OrthoParams;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixInvV[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	int _ScaleMode;
uniform 	mediump vec4 _Color;
uniform 	mediump vec4 _ColorEnd;
uniform 	vec3 _PointStart;
uniform 	vec3 _PointEnd;
uniform 	mediump float _Thickness;
uniform 	int _ThicknessSpace;
uniform 	mediump float _DashSize;
uniform 	mediump float _DashOffset;
uniform 	mediump float _DashSpacing;
uniform 	int _DashSpace;
uniform 	int _DashSnap;
attribute highp vec4 in_POSITION0;
varying mediump vec4 vs_TEXCOORD1;
varying mediump vec4 vs_TEXCOORD0;
vec4 u_xlat0;
bvec3 u_xlatb0;
vec4 u_xlat1;
mediump vec4 u_xlat16_2;
vec3 u_xlat3;
vec2 u_xlat4;
vec4 u_xlat5;
float u_xlat6;
mediump vec3 u_xlat16_6;
mediump vec3 u_xlat16_7;
mediump vec3 u_xlat16_8;
mediump vec3 u_xlat16_9;
vec3 u_xlat10;
float u_xlat11;
bool u_xlatb11;
mediump float u_xlat16_13;
float u_xlat17;
float u_xlat22;
float u_xlat24;
mediump float u_xlat16_24;
vec2 u_xlat26;
bool u_xlatb26;
mediump float u_xlat16_28;
float u_xlat33;
bool u_xlatb33;
float u_xlat34;
bool u_xlatb34;
mediump float u_xlat16_35;
bool u_xlatb36;
float u_xlat37;
float unity_roundEven(float x) { float y = floor(x + 0.5); return (y - x == 0.5) ? floor(0.5*y) * 2.0 : y; }
vec2 unity_roundEven(vec2 a) { a.x = unity_roundEven(a.x); a.y = unity_roundEven(a.y); return a; }
vec3 unity_roundEven(vec3 a) { a.x = unity_roundEven(a.x); a.y = unity_roundEven(a.y); a.z = unity_roundEven(a.z); return a; }
vec4 unity_roundEven(vec4 a) { a.x = unity_roundEven(a.x); a.y = unity_roundEven(a.y); a.z = unity_roundEven(a.z); a.w = unity_roundEven(a.w); return a; }

void main()
{
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[1].xyz * _PointStart.yyy;
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[0].xyz * _PointStart.xxx + u_xlat0.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].xyz * _PointStart.zzz + u_xlat0.xyz;
    u_xlat0.xyz = u_xlat0.xyz + hlslcc_mtx4x4unity_ObjectToWorld[3].xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_ObjectToWorld[1].xyz * _PointEnd.yyy;
    u_xlat1.xyz = hlslcc_mtx4x4unity_ObjectToWorld[0].xyz * _PointEnd.xxx + u_xlat1.xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].xyz * _PointEnd.zzz + u_xlat1.xyz;
    u_xlat1.xyz = u_xlat1.xyz + hlslcc_mtx4x4unity_ObjectToWorld[3].xyz;
    u_xlat16_2.x = dot(hlslcc_mtx4x4unity_ObjectToWorld[0].xyz, hlslcc_mtx4x4unity_ObjectToWorld[0].xyz);
    u_xlat16_2.y = dot(hlslcc_mtx4x4unity_ObjectToWorld[1].xyz, hlslcc_mtx4x4unity_ObjectToWorld[1].xyz);
    u_xlat16_2.z = dot(hlslcc_mtx4x4unity_ObjectToWorld[2].xyz, hlslcc_mtx4x4unity_ObjectToWorld[2].xyz);
    u_xlat16_2.xyz = sqrt(u_xlat16_2.xyz);
    u_xlat16_2.x = u_xlat16_2.y + u_xlat16_2.x;
    u_xlat16_2.x = u_xlat16_2.z + u_xlat16_2.x;
    u_xlat16_2.x = u_xlat16_2.x * 0.333333343;
    u_xlat16_13 = (_ScaleMode != 0) ? 1.0 : u_xlat16_2.x;
    u_xlat16_13 = u_xlat16_13 * _Thickness;
    u_xlat3.xyz = (-u_xlat0.xyz) + u_xlat1.xyz;
    u_xlat33 = dot(u_xlat3.xyz, u_xlat3.xyz);
    u_xlat4.y = sqrt(u_xlat33);
    u_xlat3.xyz = u_xlat3.xyz / u_xlat4.yyy;
    u_xlat33 = in_POSITION0.z;
    u_xlat33 = clamp(u_xlat33, 0.0, 1.0);
    u_xlatb34 = 0.5<u_xlat33;
    u_xlat1.xyz = (bool(u_xlatb34)) ? u_xlat1.xyz : u_xlat0.xyz;
    u_xlatb36 = unity_OrthoParams.w==1.0;
    u_xlat5.xyz = (-u_xlat1.zxy) + _WorldSpaceCameraPos.zxy;
    u_xlat26.x = dot(u_xlat5.xyz, u_xlat5.xyz);
    u_xlat26.x = inversesqrt(u_xlat26.x);
    u_xlat5.xyz = u_xlat26.xxx * u_xlat5.xyz;
    u_xlat16_6.xyz = (bool(u_xlatb36)) ? (-hlslcc_mtx4x4unity_MatrixInvV[2].zxy) : u_xlat5.xyz;
    u_xlatb36 = u_xlat4.y<9.99999975e-05;
    u_xlatb26 = abs(u_xlat3.y)>=0.99000001;
    u_xlat16_7.xyz = (bool(u_xlatb26)) ? vec3(0.0, 1.0, 0.0) : vec3(0.0, 0.0, 1.0);
    u_xlat16_8.xyz = u_xlat3.yzx * u_xlat16_7.xyz;
    u_xlat16_7.xyz = u_xlat16_7.zxy * u_xlat3.zxy + (-u_xlat16_8.xyz);
    u_xlat16_24 = dot(u_xlat16_7.xyz, u_xlat16_7.xyz);
    u_xlat16_24 = inversesqrt(u_xlat16_24);
    u_xlat16_7.xyz = vec3(u_xlat16_24) * u_xlat16_7.xyz;
    u_xlat16_8.xyz = u_xlat3.yzx * u_xlat16_7.zxy;
    u_xlat16_8.xyz = u_xlat16_7.yzx * u_xlat3.zxy + (-u_xlat16_8.xyz);
    u_xlat16_9.xyz = u_xlat3.yzx * (-u_xlat16_6.xyz);
    u_xlat16_6.xyz = (-u_xlat16_6.zxy) * u_xlat3.zxy + (-u_xlat16_9.xyz);
    u_xlat16_24 = dot(u_xlat16_6.xyz, u_xlat16_6.xyz);
    u_xlat16_24 = inversesqrt(u_xlat16_24);
    u_xlat16_6.xyz = vec3(u_xlat16_24) * u_xlat16_6.xyz;
    u_xlat16_8.xyz = (bool(u_xlatb36)) ? hlslcc_mtx4x4unity_MatrixInvV[0].xyz : u_xlat16_8.xyz;
    u_xlat16_7.xyz = (bool(u_xlatb36)) ? hlslcc_mtx4x4unity_MatrixInvV[1].xyz : u_xlat16_7.xyz;
    u_xlat16_6.xyz = (bool(u_xlatb36)) ? hlslcc_mtx4x4unity_MatrixInvV[1].xyz : u_xlat16_6.xyz;
    u_xlat5.x = hlslcc_mtx4x4unity_MatrixVP[0].w;
    u_xlat5.y = hlslcc_mtx4x4unity_MatrixVP[1].w;
    u_xlat5.z = hlslcc_mtx4x4unity_MatrixVP[2].w;
    u_xlat5.w = hlslcc_mtx4x4unity_MatrixVP[3].w;
    u_xlat1.w = 1.0;
    u_xlat34 = dot(u_xlat5, u_xlat1);
    u_xlat5.x = hlslcc_mtx4x4unity_MatrixVP[0].x;
    u_xlat5.y = hlslcc_mtx4x4unity_MatrixVP[1].x;
    u_xlat5.z = hlslcc_mtx4x4unity_MatrixVP[2].x;
    u_xlat26.x = dot(u_xlat5.xyz, u_xlat16_6.xyz);
    u_xlat5.x = u_xlat26.x / u_xlat34;
    u_xlat10.x = hlslcc_mtx4x4unity_MatrixVP[0].y;
    u_xlat10.y = hlslcc_mtx4x4unity_MatrixVP[1].y;
    u_xlat10.z = hlslcc_mtx4x4unity_MatrixVP[2].y;
    u_xlat26.x = dot(u_xlat10.xyz, u_xlat16_6.xyz);
    u_xlat5.y = u_xlat26.x / u_xlat34;
    u_xlat26.xy = u_xlat5.xy * _ScreenParams.xy;
    u_xlat34 = dot(u_xlat26.xy, u_xlat26.xy);
    u_xlat34 = sqrt(u_xlat34);
    u_xlat34 = u_xlat34 * 0.5;
    if(_ThicknessSpace == 0) {
        u_xlat37 = u_xlat34 * u_xlat16_13;
    } else if(_ThicknessSpace == 1) {
        u_xlat37 = u_xlat16_13;
    } else if(_ThicknessSpace == 2) {
        u_xlat26.x = min(_ScreenParams.y, _ScreenParams.x);
        u_xlat5.x = u_xlat16_13 * 0.00999999978;
        u_xlat37 = u_xlat26.x * u_xlat5.x;
    } else {
        u_xlat37 = 0.0;
    }
    u_xlat16_13 = max(u_xlat37, 1.0);
    u_xlat16_24 = u_xlat16_13 / u_xlat34;
    u_xlat16_35 = u_xlat16_24 * 0.5;
    u_xlat5.xy = vec2(u_xlat16_35) * in_POSITION0.xy;
    u_xlat1.xyz = u_xlat5.xxx * u_xlat16_8.xyz + u_xlat1.xyz;
    u_xlat1.xyz = u_xlat5.yyy * u_xlat16_7.xyz + u_xlat1.xyz;
    u_xlat5 = (-_Color) + _ColorEnd;
    u_xlat5 = vec4(u_xlat33) * u_xlat5 + _Color;
    u_xlatb33 = 6.10351563e-05<_DashSize;
    if(u_xlatb33){
        u_xlat16_6.xyz = (bool(u_xlatb36)) ? hlslcc_mtx4x4unity_MatrixInvV[2].xyz : u_xlat3.xyz;
        u_xlat0.xyz = (-u_xlat0.xyz) + u_xlat1.xyz;
        u_xlat4.x = dot(u_xlat16_6.xyz, u_xlat0.xyz);
        u_xlatb0.x = _ScaleMode==0;
        u_xlatb11 = _DashSpace!=-2;
        u_xlatb0.x = u_xlatb11 && u_xlatb0.x;
        u_xlat16_2.xw = u_xlat16_2.xx * vec2(_DashSize, _DashSpacing);
        u_xlat16_2.xw = (u_xlatb0.x) ? u_xlat16_2.xw : vec2(_DashSize, _DashSpacing);
        u_xlatb0.xyz = equal(ivec4(_DashSpace, _DashSpace, _DashSnap, _DashSpace), ivec4(-1, -2, 3, 0)).xyz;
        if(u_xlatb0.x){
            if(_ThicknessSpace == 0) {
                u_xlat16_6.x = u_xlat4.x;
            } else if(_ThicknessSpace == 1) {
                u_xlat16_6.x = u_xlat34 * u_xlat4.x;
            } else if(_ThicknessSpace == 2) {
                u_xlat16_28 = u_xlat34 * u_xlat4.x;
                u_xlat0.x = u_xlat16_28 * 100.0;
                u_xlat33 = min(_ScreenParams.y, _ScreenParams.x);
                u_xlat6 = u_xlat0.x / u_xlat33;
                u_xlat16_6.x = u_xlat6;
            } else {
                u_xlat16_6.x = 0.0;
            }
            if(_ThicknessSpace == 0) {
                u_xlat16_6.y = u_xlat4.y;
            } else if(_ThicknessSpace == 1) {
                u_xlat16_6.y = u_xlat34 * u_xlat4.y;
            } else if(_ThicknessSpace == 2) {
                u_xlat16_28 = u_xlat34 * u_xlat4.y;
                u_xlat0.x = u_xlat16_28 * 100.0;
                u_xlat33 = min(_ScreenParams.y, _ScreenParams.x);
                u_xlat17 = u_xlat0.x / u_xlat33;
                u_xlat16_6.y = u_xlat17;
            } else {
                u_xlat16_6.y = 0.0;
            }
            if(_ThicknessSpace == 0) {
            } else if(_ThicknessSpace == 1) {
                u_xlat16_24 = u_xlat16_13;
            } else if(_ThicknessSpace == 2) {
                u_xlat0.x = u_xlat16_13 * 100.0;
                u_xlat33 = min(_ScreenParams.y, _ScreenParams.x);
                u_xlat24 = u_xlat0.x / u_xlat33;
                u_xlat16_24 = u_xlat24;
            } else {
                u_xlat16_24 = 0.0;
            }
            u_xlat4.xy = u_xlat16_6.xy;
        }
        if(u_xlatb0.y){
            if(_DashSnap == 0) {
            } else if(_DashSnap == 1 || _DashSnap == 3) {
                u_xlat16_13 = unity_roundEven(u_xlat16_2.x);
                u_xlat16_2.x = max(u_xlat16_13, 1.0);
            } else if(_DashSnap == 2) {
                u_xlat16_13 = u_xlat16_2.w + u_xlat16_2.x;
                u_xlat16_13 = unity_roundEven(u_xlat16_13);
                u_xlat16_13 = max(u_xlat16_13, 1.0);
                u_xlat16_2.x = (-u_xlat16_2.w) + u_xlat16_13;
            } else {
                u_xlat16_2.x = 0.0;
            }
            u_xlat0.x = u_xlat16_2.x;
            u_xlat3.y = u_xlat16_2.w;
        } else {
            u_xlat16_2.x = u_xlat16_2.w + u_xlat16_2.x;
            u_xlat3.y = u_xlat16_2.w / u_xlat16_2.x;
            u_xlat11 = u_xlat4.y / u_xlat16_2.x;
            if(_DashSnap == 0) {
                u_xlat16_2.x = u_xlat11;
            } else if(_DashSnap == 1 || _DashSnap == 3) {
                u_xlat16_13 = unity_roundEven(u_xlat11);
                u_xlat16_2.x = max(u_xlat16_13, 1.0);
            } else if(_DashSnap == 2) {
                u_xlat16_13 = u_xlat3.y + u_xlat11;
                u_xlat16_13 = unity_roundEven(u_xlat16_13);
                u_xlat16_13 = max(u_xlat16_13, 1.0);
                u_xlat16_2.x = (-u_xlat3.y) + u_xlat16_13;
            } else {
                u_xlat16_2.x = 0.0;
            }
            u_xlat0.x = u_xlat16_2.x;
        }
        u_xlat11 = (-u_xlat3.y) + 1.0;
        u_xlat33 = (-u_xlat11) * 0.5 + _DashOffset;
        u_xlat16_2.x = (u_xlatb0.z) ? u_xlat33 : _DashOffset;
        u_xlat22 = u_xlat0.x * u_xlat16_24;
        u_xlat3.z = u_xlat22 / u_xlat4.y;
        u_xlat22 = u_xlat4.x / u_xlat4.y;
        u_xlat0.x = u_xlat22 * u_xlat0.x + (-u_xlat16_2.x);
        u_xlat3.x = (-u_xlat11) * 0.5 + u_xlat0.x;
        u_xlat16_2.xyz = u_xlat3.xyz;
    } else {
        u_xlat16_2.x = float(0.0);
        u_xlat16_2.y = float(0.0);
        u_xlat16_2.z = float(0.0);
    }
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = u_xlat0 + hlslcc_mtx4x4unity_MatrixVP[3];
    vs_TEXCOORD1.xyz = u_xlat16_2.xyz;
    vs_TEXCOORD1.w = u_xlat37;
    vs_TEXCOORD0 = u_xlat5;
    return;
}

#endif
#ifdef FRAGMENT
#version 100
#ifdef GL_OES_standard_derivatives
#extension GL_OES_standard_derivatives : enable
#endif

#ifdef GL_FRAGMENT_PRECISION_HIGH
    precision highp float;
#else
    precision mediump float;
#endif
precision highp int;
varying mediump vec4 vs_TEXCOORD1;
varying mediump vec4 vs_TEXCOORD0;
#define SV_Target0 gl_FragData[0]
mediump float u_xlat16_0;
float u_xlat1;
mediump float u_xlat16_2;
bool u_xlatb3;
mediump float u_xlat16_4;
void main()
{
    u_xlat16_0 = fract(vs_TEXCOORD1.x);
    u_xlat16_0 = u_xlat16_0 * 2.0 + -1.0;
    u_xlat16_0 = abs(u_xlat16_0) + (-vs_TEXCOORD1.y);
    u_xlat16_2 = (-vs_TEXCOORD1.y) + 1.0;
    u_xlat16_0 = u_xlat16_0 / u_xlat16_2;
    u_xlat16_2 = dFdx(u_xlat16_0);
    u_xlat16_4 = dFdy(u_xlat16_0);
    u_xlat16_2 = abs(u_xlat16_4) + abs(u_xlat16_2);
    u_xlat16_0 = u_xlat16_0 / u_xlat16_2;
    u_xlat16_0 = u_xlat16_0 + 0.5;
    u_xlat16_0 = clamp(u_xlat16_0, 0.0, 1.0);
    u_xlat16_2 = vs_TEXCOORD1.w;
    u_xlat16_2 = clamp(u_xlat16_2, 0.0, 1.0);
    u_xlat1 = u_xlat16_2 * u_xlat16_0;
    u_xlat1 = u_xlat1 * vs_TEXCOORD0.w;
    u_xlatb3 = vs_TEXCOORD1.y>=0.999938965;
    u_xlat1 = (u_xlatb3) ? 0.0 : u_xlat1;
    u_xlat16_0 = u_xlat1 + -6.10351563e-05;
    u_xlatb3 = u_xlat16_0<0.0;
    if(u_xlatb3){discard;}
    SV_Target0.xyz = vec3(u_xlat1) * vs_TEXCOORD0.xyz;
    SV_Target0.w = u_xlat1;
    return;
}

#endif
                                   