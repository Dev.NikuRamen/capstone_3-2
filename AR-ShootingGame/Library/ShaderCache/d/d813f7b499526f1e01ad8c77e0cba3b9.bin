x  <Q                         INSTANCING_ON       �q  #ifdef VERTEX
#version 300 es

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 _ScreenParams;
uniform 	vec4 unity_OrthoParams;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixInvV[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	int _ScaleMode;
uniform 	int _Alignment;
uniform 	mediump float _Radius;
uniform 	int _RadiusSpace;
uniform 	mediump float _Thickness;
uniform 	int _ThicknessSpace;
uniform 	mediump float _Angle;
uniform 	int _Hollow;
uniform 	int _Sides;
uniform 	int _FillType;
uniform 	int _FillSpace;
uniform 	vec4 _FillStart;
uniform 	vec3 _FillEnd;
in highp vec4 in_POSITION0;
in highp vec2 in_TEXCOORD0;
out mediump vec4 vs_TEXCOORD0;
out mediump vec4 vs_TEXCOORD1;
out mediump vec3 vs_TEXCOORD2;
out highp vec3 vs_TEXCOORD3;
mediump vec3 u_xlat16_0;
mediump vec3 u_xlat16_1;
vec3 u_xlat2;
bool u_xlatb2;
vec4 u_xlat3;
bool u_xlatb3;
vec4 u_xlat4;
mediump vec4 u_xlat16_4;
vec4 u_xlat5;
mediump vec3 u_xlat16_6;
mediump vec3 u_xlat16_7;
vec3 u_xlat10;
bool u_xlatb10;
vec3 u_xlat11;
mediump float u_xlat16_16;
vec2 u_xlat18;
bool u_xlatb18;
mediump float u_xlat16_24;
float u_xlat26;
bool u_xlatb26;
void main()
{
    u_xlat16_0.x = dot(hlslcc_mtx4x4unity_ObjectToWorld[0].xyz, hlslcc_mtx4x4unity_ObjectToWorld[0].xyz);
    u_xlat16_0.z = dot(hlslcc_mtx4x4unity_ObjectToWorld[1].xyz, hlslcc_mtx4x4unity_ObjectToWorld[1].xyz);
    u_xlat16_0.xy = sqrt(u_xlat16_0.xz);
    u_xlat16_16 = u_xlat16_0.y + u_xlat16_0.x;
    u_xlat16_1.z = u_xlat16_16 * 0.5;
    u_xlat16_16 = u_xlat16_1.z * _Radius;
    u_xlat10.x = u_xlat16_16 + u_xlat16_16;
    u_xlat3.x = hlslcc_mtx4x4unity_MatrixVP[0].w;
    u_xlat3.y = hlslcc_mtx4x4unity_MatrixVP[1].w;
    u_xlat3.z = hlslcc_mtx4x4unity_MatrixVP[2].w;
    u_xlat3.w = hlslcc_mtx4x4unity_MatrixVP[3].w;
    u_xlat4.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz;
    u_xlat4.w = 1.0;
    u_xlat2.x = dot(u_xlat3, u_xlat4);
    u_xlat3.x = hlslcc_mtx4x4unity_MatrixVP[0].x;
    u_xlat3.y = hlslcc_mtx4x4unity_MatrixVP[1].x;
    u_xlat3.z = hlslcc_mtx4x4unity_MatrixVP[2].x;
    u_xlat18.x = dot(u_xlat3.xyz, hlslcc_mtx4x4unity_MatrixInvV[0].xyz);
    u_xlat3.x = u_xlat18.x / u_xlat2.x;
    u_xlat4.x = hlslcc_mtx4x4unity_MatrixVP[0].y;
    u_xlat4.y = hlslcc_mtx4x4unity_MatrixVP[1].y;
    u_xlat4.z = hlslcc_mtx4x4unity_MatrixVP[2].y;
    u_xlat18.x = dot(u_xlat4.xyz, hlslcc_mtx4x4unity_MatrixInvV[0].xyz);
    u_xlat3.y = u_xlat18.x / u_xlat2.x;
    u_xlat2.xz = u_xlat3.xy * _ScreenParams.xy;
    u_xlat2.x = dot(u_xlat2.xz, u_xlat2.xz);
    u_xlat2.x = sqrt(u_xlat2.x);
    u_xlat2.x = u_xlat2.x * 0.5;
    switch(_RadiusSpace){
        case 0:
            u_xlat10.x = u_xlat2.x * u_xlat10.x;
            break;
        case 1:
            break;
        case 2:
            u_xlat18.x = min(_ScreenParams.y, _ScreenParams.x);
            u_xlat26 = u_xlat16_16 * 0.0199999996;
            u_xlat10.x = u_xlat26 * u_xlat18.x;
            break;
        default:
            u_xlat10.x = 0.0;
            break;
    }
    u_xlat16_16 = max(u_xlat10.x, 1.0);
    u_xlat16_16 = u_xlat16_16 / u_xlat2.x;
    u_xlat16_1.y = u_xlat16_16 * 0.5;
#ifdef UNITY_ADRENO_ES3
    u_xlatb18 = !!(_Hollow==1);
#else
    u_xlatb18 = _Hollow==1;
#endif
    if(u_xlatb18){
        u_xlat16_16 = (_ScaleMode != 0) ? 1.0 : u_xlat16_1.z;
        u_xlat16_16 = u_xlat16_16 * _Thickness;
        switch(_ThicknessSpace){
            case 0:
                u_xlat11.x = u_xlat2.x * u_xlat16_16;
                break;
            case 1:
                u_xlat11.x = u_xlat16_16;
                break;
            case 2:
                u_xlat18.x = min(_ScreenParams.y, _ScreenParams.x);
                u_xlat26 = u_xlat16_16 * 0.00999999978;
                u_xlat11.x = u_xlat26 * u_xlat18.x;
                break;
            default:
                u_xlat11.x = 0.0;
                break;
        }
        u_xlat16_16 = max(u_xlat11.x, 1.0);
        u_xlat16_1.x = u_xlat16_16 / u_xlat2.x;
        u_xlat16_16 = u_xlat16_1.x * 0.5 + u_xlat16_1.y;
        u_xlat16_24 = (-u_xlat16_1.x) + u_xlat16_16;
        u_xlat16_4.x = u_xlat16_24 / u_xlat16_16;
        u_xlat18.xy = vec2(u_xlat16_16) * in_TEXCOORD0.xy;
        u_xlat16_4.y = u_xlat11.x;
        u_xlat16_16 = u_xlat2.x;
    } else {
        u_xlat18.xy = u_xlat16_1.yy * in_TEXCOORD0.xy;
        u_xlat16_4.x = 0.0;
        u_xlat16_4.y = u_xlat10.x;
        u_xlat16_16 = 0.0;
        u_xlat16_1.x = 0.0;
    }
    u_xlat2.x = sin(_Angle);
    u_xlat3.x = cos(_Angle);
    u_xlat2.xy = u_xlat18.yx * u_xlat2.xx;
    u_xlat5.x = u_xlat3.x * u_xlat18.x + (-u_xlat2.x);
    u_xlat5.y = u_xlat3.x * u_xlat18.y + u_xlat2.y;
#ifdef UNITY_ADRENO_ES3
    u_xlatb2 = !!(_Alignment==1);
#else
    u_xlatb2 = _Alignment==1;
#endif
#ifdef UNITY_ADRENO_ES3
    u_xlatb10 = !!(unity_OrthoParams.w==1.0);
#else
    u_xlatb10 = unity_OrthoParams.w==1.0;
#endif
    u_xlat3.xyz = _WorldSpaceCameraPos.xyz + (-hlslcc_mtx4x4unity_ObjectToWorld[3].xyz);
    u_xlat18.x = dot(u_xlat3.xyz, u_xlat3.xyz);
    u_xlat18.x = inversesqrt(u_xlat18.x);
    u_xlat3.xyz = u_xlat18.xxx * u_xlat3.xyz;
    u_xlat16_6.xyz = (bool(u_xlatb10)) ? (-hlslcc_mtx4x4unity_MatrixInvV[2].xyz) : u_xlat3.xyz;
    u_xlat10.xyz = (-u_xlat16_6.yyy) * hlslcc_mtx4x4unity_WorldToObject[1].yzx;
    u_xlat10.xyz = hlslcc_mtx4x4unity_WorldToObject[0].yzx * (-u_xlat16_6.xxx) + u_xlat10.xyz;
    u_xlat10.xyz = hlslcc_mtx4x4unity_WorldToObject[2].yzx * (-u_xlat16_6.zzz) + u_xlat10.xyz;
    u_xlat3.xyz = hlslcc_mtx4x4unity_WorldToObject[1].yzx * hlslcc_mtx4x4unity_MatrixInvV[0].yyy;
    u_xlat3.xyz = hlslcc_mtx4x4unity_WorldToObject[0].yzx * hlslcc_mtx4x4unity_MatrixInvV[0].xxx + u_xlat3.xyz;
    u_xlat3.xyz = hlslcc_mtx4x4unity_WorldToObject[2].yzx * hlslcc_mtx4x4unity_MatrixInvV[0].zzz + u_xlat3.xyz;
    u_xlat16_6.xyz = u_xlat10.yzx * u_xlat3.xyz;
    u_xlat16_6.xyz = u_xlat10.xyz * u_xlat3.yzx + (-u_xlat16_6.xyz);
    u_xlat16_24 = dot(u_xlat16_6.xyz, u_xlat16_6.xyz);
    u_xlat16_24 = inversesqrt(u_xlat16_24);
    u_xlat16_6.xyz = vec3(u_xlat16_24) * u_xlat16_6.xyz;
    u_xlat16_7.xyz = u_xlat10.xyz * u_xlat16_6.zxy;
    u_xlat16_7.xyz = u_xlat16_6.yzx * u_xlat10.yzx + (-u_xlat16_7.xyz);
    u_xlat10.xyz = u_xlat5.yyy * u_xlat16_6.xyz;
    u_xlat10.xyz = u_xlat5.xxx * u_xlat16_7.xyz + u_xlat10.xyz;
    u_xlat5.z = in_POSITION0.z;
    u_xlat2.xyz = (bool(u_xlatb2)) ? u_xlat10.xyz : u_xlat5.xyz;
    u_xlat26 = float(_Sides);
    u_xlat26 = 6.28318548 / u_xlat26;
    u_xlat16_4.z = u_xlat26 * 0.5;
    u_xlat16_6.x = sin(u_xlat16_4.z);
    u_xlat16_7.x = cos(u_xlat16_4.z);
    u_xlat2.xy = u_xlat2.xy / u_xlat16_0.xy;
    u_xlat3 = u_xlat2.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat3 = hlslcc_mtx4x4unity_ObjectToWorld[0] * u_xlat2.xxxx + u_xlat3;
    u_xlat3 = hlslcc_mtx4x4unity_ObjectToWorld[2] * u_xlat2.zzzz + u_xlat3;
    u_xlat3 = u_xlat3 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat5 = u_xlat3.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat5 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat3.xxxx + u_xlat5;
    u_xlat5 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat3.zzzz + u_xlat5;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat3.wwww + u_xlat5;
#ifdef UNITY_ADRENO_ES3
    u_xlatb26 = !!(_FillType!=int(0xFFFFFFFFu));
#else
    u_xlatb26 = _FillType!=int(0xFFFFFFFFu);
#endif
    u_xlat2.xyz = (int(_FillSpace) != 0) ? u_xlat3.xyz : u_xlat2.xyz;
    u_xlat2.xyz = u_xlat2.xyz + (-_FillStart.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlatb3 = !!(_FillType==1);
#else
    u_xlatb3 = _FillType==1;
#endif
    u_xlat11.xyz = (-_FillStart.xyz) + _FillEnd.xyz;
    u_xlat5.x = dot(u_xlat11.xyz, u_xlat2.xyz);
    u_xlat16_0.x = dot(u_xlat11.xyz, u_xlat11.xyz);
    u_xlat5.x = u_xlat5.x / u_xlat16_0.x;
    u_xlat5.y = float(0.0);
    u_xlat5.z = float(0.0);
    u_xlat2.xyz = (bool(u_xlatb3)) ? u_xlat2.xyz : u_xlat5.xyz;
    vs_TEXCOORD3.xyz = bool(u_xlatb26) ? u_xlat2.xyz : vec3(0.0, 0.0, 0.0);
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
    vs_TEXCOORD0.z = u_xlat16_16;
    vs_TEXCOORD0.w = u_xlat16_7.x;
    u_xlat16_4.w = u_xlat16_6.x;
    vs_TEXCOORD1 = u_xlat16_4;
    vs_TEXCOORD2.xyz = u_xlat16_1.xyz;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp float;
precision highp int;
#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 _ScreenParams;
uniform 	int _ScaleMode;
uniform 	mediump vec4 _Color;
uniform 	int _ThicknessSpace;
uniform 	mediump float _Roundness;
uniform 	int _Hollow;
uniform 	int _Sides;
uniform 	int _DashType;
uniform 	mediump float _DashSize;
uniform 	mediump float _DashShapeModifier;
uniform 	mediump float _DashOffset;
uniform 	mediump float _DashSpacing;
uniform 	int _DashSpace;
uniform 	int _DashSnap;
uniform 	mediump vec4 _ColorEnd;
uniform 	int _FillType;
uniform 	vec4 _FillStart;
in mediump vec4 vs_TEXCOORD0;
in mediump vec4 vs_TEXCOORD1;
in mediump vec3 vs_TEXCOORD2;
in highp vec3 vs_TEXCOORD3;
layout(location = 0) out mediump vec4 SV_Target0;
vec2 u_xlat0;
mediump vec4 u_xlat16_0;
int u_xlati0;
bool u_xlatb0;
vec4 u_xlat1;
mediump vec2 u_xlat16_1;
mediump vec2 u_xlat16_2;
vec2 u_xlat3;
bool u_xlatb3;
mediump vec2 u_xlat16_4;
float u_xlat5;
mediump vec2 u_xlat16_5;
mediump vec3 u_xlat16_6;
vec2 u_xlat7;
float u_xlat8;
bvec3 u_xlatb8;
vec2 u_xlat9;
bool u_xlatb9;
vec2 u_xlat10;
vec2 u_xlat11;
bool u_xlatb11;
vec2 u_xlat12;
ivec2 u_xlati12;
bool u_xlatb12;
mediump float u_xlat16_13;
float u_xlat14;
mediump vec2 u_xlat16_14;
float u_xlat15;
mediump float u_xlat16_16;
float u_xlat17;
bool u_xlatb20;
bool u_xlatb21;
vec2 u_xlat22;
bool u_xlatb23;
vec2 u_xlat24;
bool u_xlatb24;
mediump float u_xlat16_25;
float u_xlat26;
mediump float u_xlat16_26;
vec2 u_xlat33;
bool u_xlatb33;
float u_xlat34;
float u_xlat36;
int u_xlati36;
bool u_xlatb36;
mediump float u_xlat16_37;
mediump float u_xlat16_38;
float u_xlat44;
float u_xlat45;
bool u_xlatb45;
float u_xlat46;
void main()
{
    u_xlat0.x = float(_Sides);
    u_xlat12.x = 6.28318548 / u_xlat0.x;
    u_xlat16_1.x = (-_Roundness) + 1.0;
    u_xlat16_2.x = u_xlat16_1.x * vs_TEXCOORD0.w;
    u_xlat16_1.x = u_xlat16_1.x * vs_TEXCOORD1.w;
    u_xlat16_13 = u_xlat12.x * 0.5;
    u_xlat16_25 = min(abs(vs_TEXCOORD0.x), abs(vs_TEXCOORD0.y));
    u_xlat16_37 = max(abs(vs_TEXCOORD0.x), abs(vs_TEXCOORD0.y));
    u_xlat16_37 = float(1.0) / u_xlat16_37;
    u_xlat16_25 = u_xlat16_37 * u_xlat16_25;
    u_xlat16_37 = u_xlat16_25 * u_xlat16_25;
    u_xlat24.x = u_xlat16_37 * 0.0208350997 + -0.0851330012;
    u_xlat24.x = u_xlat16_37 * u_xlat24.x + 0.180141002;
    u_xlat24.x = u_xlat16_37 * u_xlat24.x + -0.330299497;
    u_xlat24.x = u_xlat16_37 * u_xlat24.x + 0.999866009;
    u_xlat36 = u_xlat24.x * u_xlat16_25;
#ifdef UNITY_ADRENO_ES3
    u_xlatb3 = !!(abs(vs_TEXCOORD0.x)<abs(vs_TEXCOORD0.y));
#else
    u_xlatb3 = abs(vs_TEXCOORD0.x)<abs(vs_TEXCOORD0.y);
#endif
    u_xlat36 = u_xlat36 * -2.0 + 1.57079637;
    u_xlat36 = u_xlatb3 ? u_xlat36 : float(0.0);
    u_xlat24.x = u_xlat16_25 * u_xlat24.x + u_xlat36;
#ifdef UNITY_ADRENO_ES3
    u_xlatb36 = !!(vs_TEXCOORD0.x<(-vs_TEXCOORD0.x));
#else
    u_xlatb36 = vs_TEXCOORD0.x<(-vs_TEXCOORD0.x);
#endif
    u_xlat36 = u_xlatb36 ? -3.14159274 : float(0.0);
    u_xlat24.x = u_xlat36 + u_xlat24.x;
    u_xlat16_25 = min(vs_TEXCOORD0.x, vs_TEXCOORD0.y);
    u_xlat16_37 = max(vs_TEXCOORD0.x, vs_TEXCOORD0.y);
#ifdef UNITY_ADRENO_ES3
    u_xlatb36 = !!(u_xlat16_25<(-u_xlat16_25));
#else
    u_xlatb36 = u_xlat16_25<(-u_xlat16_25);
#endif
#ifdef UNITY_ADRENO_ES3
    u_xlatb3 = !!(u_xlat16_37>=(-u_xlat16_37));
#else
    u_xlatb3 = u_xlat16_37>=(-u_xlat16_37);
#endif
    u_xlatb36 = u_xlatb36 && u_xlatb3;
    u_xlat24.x = (u_xlatb36) ? (-u_xlat24.x) : u_xlat24.x;
    u_xlat16_25 = u_xlat24.x / u_xlat12.x;
    u_xlat16_25 = floor(u_xlat16_25);
    u_xlat16_13 = u_xlat12.x * u_xlat16_25 + u_xlat16_13;
    u_xlat16_4.x = sin(u_xlat16_13);
    u_xlat16_5.x = cos(u_xlat16_13);
    u_xlat16_6.x = (-u_xlat16_4.x);
    u_xlat16_6.y = u_xlat16_5.x;
    u_xlat16_6.z = u_xlat16_4.x;
    u_xlat16_4.x = dot(vs_TEXCOORD0.xy, u_xlat16_6.yz);
    u_xlat16_4.y = dot(vs_TEXCOORD0.xy, u_xlat16_6.xy);
    u_xlat16_13 = max((-u_xlat16_1.x), u_xlat16_4.y);
    u_xlat16_2.y = min(u_xlat16_1.x, u_xlat16_13);
    u_xlat16_1.xy = (-u_xlat16_2.xy) + u_xlat16_4.xy;
    u_xlat16_13 = dot(u_xlat16_1.xy, u_xlat16_1.xy);
    u_xlat16_13 = sqrt(u_xlat16_13);
#ifdef UNITY_ADRENO_ES3
    { bool cond = 0.0<u_xlat16_1.x; u_xlati12.x = int(!!cond ? 0xFFFFFFFFu : uint(0)); }
#else
    u_xlati12.x = int((0.0<u_xlat16_1.x) ? 0xFFFFFFFFu : uint(0));
#endif
#ifdef UNITY_ADRENO_ES3
    { bool cond = u_xlat16_1.x<0.0; u_xlati36 = int(!!cond ? 0xFFFFFFFFu : uint(0)); }
#else
    u_xlati36 = int((u_xlat16_1.x<0.0) ? 0xFFFFFFFFu : uint(0));
#endif
    u_xlati12.x = (-u_xlati12.x) + u_xlati36;
    u_xlat16_1.x = float(u_xlati12.x);
    u_xlat16_25 = u_xlat16_1.x * u_xlat16_13;
    u_xlat16_37 = vs_TEXCOORD0.w * _Roundness;
    u_xlat12.x = _Roundness * vs_TEXCOORD0.w + (-u_xlat16_25);
    u_xlat3.x = dFdx(u_xlat12.x);
    u_xlat3.y = dFdy(u_xlat12.x);
    u_xlat36 = dot(u_xlat3.xy, u_xlat3.xy);
    u_xlat36 = sqrt(u_xlat36);
    u_xlat36 = max(u_xlat36, 9.99999975e-06);
    u_xlat12.x = u_xlat12.x / u_xlat36;
    u_xlat12.x = u_xlat12.x + 0.5;
#ifdef UNITY_ADRENO_ES3
    u_xlat12.x = min(max(u_xlat12.x, 0.0), 1.0);
#else
    u_xlat12.x = clamp(u_xlat12.x, 0.0, 1.0);
#endif
#ifdef UNITY_ADRENO_ES3
    u_xlatb36 = !!(_Hollow==1);
#else
    u_xlatb36 = _Hollow==1;
#endif
    u_xlat16_25 = u_xlat16_13 * u_xlat16_1.x + 1.0;
    u_xlat16_2.x = u_xlat16_25 + (-vs_TEXCOORD1.x);
    u_xlat16_14.x = u_xlat16_13 * u_xlat16_1.x + (-u_xlat16_37);
    u_xlat16_26 = (-vs_TEXCOORD1.x) + 1.0;
    u_xlat16_14.x = u_xlat16_14.x / u_xlat16_26;
    u_xlat16_14.x = u_xlat16_14.x + 1.0;
    u_xlat3.x = _Roundness * vs_TEXCOORD0.w + (-u_xlat16_2.x);
    u_xlat7.x = dFdx(u_xlat3.x);
    u_xlat7.y = dFdy(u_xlat3.x);
    u_xlat15 = dot(u_xlat7.xy, u_xlat7.xy);
    u_xlat15 = sqrt(u_xlat15);
    u_xlat8 = max(u_xlat15, 9.99999975e-06);
    u_xlat8 = u_xlat3.x / u_xlat8;
    u_xlat8 = u_xlat8 + 0.5;
#ifdef UNITY_ADRENO_ES3
    u_xlat8 = min(max(u_xlat8, 0.0), 1.0);
#else
    u_xlat8 = clamp(u_xlat8, 0.0, 1.0);
#endif
    u_xlat8 = (-u_xlat8) + 1.0;
    u_xlat8 = min(u_xlat12.x, u_xlat8);
    u_xlat16_2.x = (u_xlatb36) ? u_xlat8 : u_xlat12.x;
#ifdef UNITY_ADRENO_ES3
    u_xlatb12 = !!(-1.0<u_xlat16_14.x);
#else
    u_xlatb12 = -1.0<u_xlat16_14.x;
#endif
#ifdef UNITY_ADRENO_ES3
    u_xlatb36 = !!(u_xlat16_14.x<2.0);
#else
    u_xlatb36 = u_xlat16_14.x<2.0;
#endif
    u_xlatb12 = u_xlatb36 && u_xlatb12;
#ifdef UNITY_ADRENO_ES3
    u_xlatb36 = !!(6.10351563e-05<_DashSize);
#else
    u_xlatb36 = 6.10351563e-05<_DashSize;
#endif
    u_xlatb12 = u_xlatb36 && u_xlatb12;
#ifdef UNITY_ADRENO_ES3
    u_xlatb36 = !!(0<_Hollow);
#else
    u_xlatb36 = 0<_Hollow;
#endif
    u_xlatb12 = u_xlatb36 && u_xlatb12;
    if(u_xlatb12){
        u_xlatb8.xyz = equal(ivec4(_DashSnap, _DashSpace, _DashSpace, _DashSnap), ivec4(2, int(0xFFFFFFFFu), int(0xFFFFFFFEu), 0)).xyz;
        u_xlat16_38 = float(_Sides);
        u_xlat16_4.x = u_xlat24.x * 0.159154937;
        u_xlat16_38 = u_xlat16_38 * u_xlat16_4.x;
        u_xlat16_38 = floor(u_xlat16_38);
        u_xlati12.x = int(u_xlat16_38);
#ifdef UNITY_ADRENO_ES3
        u_xlatb24 = !!(u_xlati12.x<0);
#else
        u_xlatb24 = u_xlati12.x<0;
#endif
        u_xlati36 = u_xlati12.x + _Sides;
        u_xlati12.x = (u_xlatb24) ? u_xlati36 : u_xlati12.x;
        u_xlat24.x = float((-u_xlati12.x));
        u_xlat24.x = u_xlat24.x * 6.28318548;
        u_xlat24.x = u_xlat24.x / u_xlat0.x;
        u_xlat9.x = sin(u_xlat24.x);
        u_xlat10.x = cos(u_xlat24.x);
        u_xlat24.xy = u_xlat9.xx * vs_TEXCOORD0.yx;
        u_xlat9.y = u_xlat10.x * vs_TEXCOORD0.x + (-u_xlat24.x);
        u_xlat9.x = u_xlat10.x * vs_TEXCOORD0.y + u_xlat24.y;
        u_xlati12.y = int(_Sides << 1);
        u_xlat12.xy = vec2(u_xlati12.xy);
        u_xlat24.x = 6.28318548 / u_xlat12.y;
        u_xlat10.x = sin(u_xlat24.x);
        u_xlat11.x = cos(u_xlat24.x);
        u_xlat36 = u_xlat10.x + u_xlat10.x;
        u_xlat10.y = u_xlat11.x;
        u_xlat33.xy = vec2(u_xlat36) * u_xlat10.yx;
        u_xlat44 = u_xlat9.y * u_xlat33.y;
        u_xlat44 = u_xlat33.x * u_xlat9.x + (-u_xlat44);
#ifdef UNITY_ADRENO_ES3
        u_xlatb33 = !!(0.0<u_xlat44);
#else
        u_xlatb33 = 0.0<u_xlat44;
#endif
        u_xlat45 = dot((-u_xlat9.xxyy), u_xlat10.xxyy);
        u_xlat22.xy = u_xlat10.xy * (-vec2(u_xlat45)) + (-u_xlat9.xy);
        u_xlat9.xy = (bool(u_xlatb33)) ? u_xlat22.xy : u_xlat9.xy;
        u_xlat45 = u_xlat36 * u_xlat36;
        u_xlat44 = abs(u_xlat44) / u_xlat45;
        u_xlat44 = (-u_xlat44) + 0.5;
        u_xlat44 = max(u_xlat44, 0.0);
        u_xlat45 = u_xlat36 * u_xlat44;
        u_xlat22.x = u_xlat11.x * _Roundness;
        u_xlat10.x = u_xlat10.x * _Roundness;
        u_xlat34 = u_xlat24.x * u_xlat22.x;
        u_xlat46 = (-u_xlat10.x) * 2.0 + u_xlat36;
        u_xlat34 = u_xlat34 * 2.0 + u_xlat46;
        u_xlat0.x = u_xlat0.x * u_xlat34;
        u_xlat0.x = (u_xlatb8.x) ? u_xlat34 : u_xlat0.x;
        u_xlat12.x = u_xlat12.x * u_xlat34;
        u_xlat12.x = (u_xlatb8.x) ? 0.0 : u_xlat12.x;
#ifdef UNITY_ADRENO_ES3
        u_xlatb45 = !!(u_xlat45<u_xlat10.x);
#else
        u_xlatb45 = u_xlat45<u_xlat10.x;
#endif
        u_xlat11.y = (-_Roundness) + 1.0;
        u_xlat11.x = 0.0;
        u_xlat9.xy = u_xlat9.xy + (-u_xlat11.xy);
        u_xlat16_38 = min(abs(u_xlat9.y), abs(u_xlat9.x));
        u_xlat16_4.x = max(abs(u_xlat9.y), abs(u_xlat9.x));
        u_xlat16_4.x = float(1.0) / u_xlat16_4.x;
        u_xlat16_38 = u_xlat16_38 * u_xlat16_4.x;
        u_xlat16_4.x = u_xlat16_38 * u_xlat16_38;
        u_xlat46 = u_xlat16_4.x * 0.0208350997 + -0.0851330012;
        u_xlat46 = u_xlat16_4.x * u_xlat46 + 0.180141002;
        u_xlat46 = u_xlat16_4.x * u_xlat46 + -0.330299497;
        u_xlat46 = u_xlat16_4.x * u_xlat46 + 0.999866009;
        u_xlat11.x = u_xlat16_38 * u_xlat46;
#ifdef UNITY_ADRENO_ES3
        u_xlatb23 = !!(abs(u_xlat9.y)<abs(u_xlat9.x));
#else
        u_xlatb23 = abs(u_xlat9.y)<abs(u_xlat9.x);
#endif
        u_xlat11.x = u_xlat11.x * -2.0 + 1.57079637;
        u_xlat11.x = u_xlatb23 ? u_xlat11.x : float(0.0);
        u_xlat46 = u_xlat16_38 * u_xlat46 + u_xlat11.x;
#ifdef UNITY_ADRENO_ES3
        u_xlatb11 = !!(u_xlat9.y<(-u_xlat9.y));
#else
        u_xlatb11 = u_xlat9.y<(-u_xlat9.y);
#endif
        u_xlat11.x = u_xlatb11 ? -3.14159274 : float(0.0);
        u_xlat46 = u_xlat46 + u_xlat11.x;
        u_xlat16_38 = min(u_xlat9.y, u_xlat9.x);
        u_xlat16_4.x = max(u_xlat9.y, u_xlat9.x);
#ifdef UNITY_ADRENO_ES3
        u_xlatb9 = !!(u_xlat16_38<(-u_xlat16_38));
#else
        u_xlatb9 = u_xlat16_38<(-u_xlat16_38);
#endif
#ifdef UNITY_ADRENO_ES3
        u_xlatb21 = !!(u_xlat16_4.x>=(-u_xlat16_4.x));
#else
        u_xlatb21 = u_xlat16_4.x>=(-u_xlat16_4.x);
#endif
        u_xlatb9 = u_xlatb21 && u_xlatb9;
        u_xlat9.x = (u_xlatb9) ? (-u_xlat46) : u_xlat46;
        u_xlat9.x = u_xlat22.x * u_xlat9.x;
        u_xlat36 = u_xlat44 * u_xlat36 + (-u_xlat10.x);
        u_xlat24.x = u_xlat24.x * u_xlat22.x + u_xlat36;
        u_xlat24.x = (u_xlatb45) ? u_xlat9.x : u_xlat24.x;
        u_xlat36 = (-u_xlat24.x) + u_xlat34;
        u_xlat24.x = (u_xlatb33) ? u_xlat36 : u_xlat24.x;
        u_xlat0.y = u_xlat24.x + u_xlat12.x;
        u_xlat9.xy = u_xlat0.yx * vs_TEXCOORD2.yy;
        u_xlati0 = (u_xlatb8.x) ? 3 : _DashSnap;
#ifdef UNITY_ADRENO_ES3
        u_xlatb12 = !!(_ScaleMode==0);
#else
        u_xlatb12 = _ScaleMode==0;
#endif
#ifdef UNITY_ADRENO_ES3
        u_xlatb24 = !!(_DashSpace!=int(0xFFFFFFFEu));
#else
        u_xlatb24 = _DashSpace!=int(0xFFFFFFFEu);
#endif
        u_xlatb12 = u_xlatb24 && u_xlatb12;
        u_xlat16_4.xy = vs_TEXCOORD2.zz * vec2(_DashSize, _DashSpacing);
        u_xlat16_4.xy = (bool(u_xlatb12)) ? u_xlat16_4.xy : vec2(_DashSize, _DashSpacing);
        u_xlat16_26 = u_xlat16_26 * vs_TEXCOORD2.z;
        if(u_xlatb8.y){
            switch(_ThicknessSpace){
                case 0:
                    u_xlat16_5.x = u_xlat9.x;
                    break;
                case 1:
                    u_xlat16_5.x = u_xlat9.x * vs_TEXCOORD0.z;
                    break;
                case 2:
                    u_xlat16_38 = u_xlat9.x * vs_TEXCOORD0.z;
                    u_xlat12.x = u_xlat16_38 * 100.0;
                    u_xlat24.x = min(_ScreenParams.y, _ScreenParams.x);
                    u_xlat5 = u_xlat12.x / u_xlat24.x;
                    u_xlat16_5.x = u_xlat5;
                    break;
                default:
                    u_xlat16_5.x = 0.0;
                    break;
            }
            switch(_ThicknessSpace){
                case 0:
                    u_xlat16_5.y = u_xlat9.y;
                    break;
                case 1:
                    u_xlat16_5.y = u_xlat9.y * vs_TEXCOORD0.z;
                    break;
                case 2:
                    u_xlat16_38 = u_xlat9.y * vs_TEXCOORD0.z;
                    u_xlat12.x = u_xlat16_38 * 100.0;
                    u_xlat24.x = min(_ScreenParams.y, _ScreenParams.x);
                    u_xlat17 = u_xlat12.x / u_xlat24.x;
                    u_xlat16_5.y = u_xlat17;
                    break;
                default:
                    u_xlat16_5.y = 0.0;
                    break;
            }
            switch(_ThicknessSpace){
                case 0:
                    break;
                case 1:
                    u_xlat16_26 = u_xlat16_26 * vs_TEXCOORD0.z;
                    break;
                case 2:
                    u_xlat16_38 = u_xlat16_26 * vs_TEXCOORD0.z;
                    u_xlat12.x = u_xlat16_38 * 100.0;
                    u_xlat24.x = min(_ScreenParams.y, _ScreenParams.x);
                    u_xlat26 = u_xlat12.x / u_xlat24.x;
                    u_xlat16_26 = u_xlat26;
                    break;
                default:
                    u_xlat16_26 = 0.0;
                    break;
            }
            u_xlat9.xy = u_xlat16_5.xy;
        }
        if(u_xlatb8.z){
            switch(u_xlati0){
                case 0:
                    break;
                case 1:
                case 3:
                    u_xlat16_38 = roundEven(u_xlat16_4.x);
                    u_xlat16_4.x = max(u_xlat16_38, 1.0);
                    break;
                case 2:
                    u_xlat16_38 = u_xlat16_4.y + u_xlat16_4.x;
                    u_xlat16_38 = roundEven(u_xlat16_38);
                    u_xlat16_38 = max(u_xlat16_38, 1.0);
                    u_xlat16_4.x = (-u_xlat16_4.y) + u_xlat16_38;
                    break;
                default:
                    u_xlat16_4.x = 0.0;
                    break;
            }
            u_xlat12.x = u_xlat16_4.x;
            u_xlat24.x = u_xlat16_4.y;
        } else {
            u_xlat16_38 = u_xlat16_4.y + u_xlat16_4.x;
            u_xlat24.x = u_xlat16_4.y / u_xlat16_38;
            u_xlat36 = u_xlat9.y / u_xlat16_38;
            switch(u_xlati0){
                case 0:
                    u_xlat16_38 = u_xlat36;
                    break;
                case 1:
                case 3:
                    u_xlat16_4.x = roundEven(u_xlat36);
                    u_xlat16_38 = max(u_xlat16_4.x, 1.0);
                    break;
                case 2:
                    u_xlat16_4.x = u_xlat24.x + u_xlat36;
                    u_xlat16_4.x = roundEven(u_xlat16_4.x);
                    u_xlat16_4.x = max(u_xlat16_4.x, 1.0);
                    u_xlat16_38 = (-u_xlat24.x) + u_xlat16_4.x;
                    break;
                default:
                    u_xlat16_38 = 0.0;
                    break;
            }
            u_xlat12.x = u_xlat16_38;
        }
        u_xlat36 = (-u_xlat24.x) + 1.0;
#ifdef UNITY_ADRENO_ES3
        u_xlatb0 = !!(u_xlati0==3);
#else
        u_xlatb0 = u_xlati0==3;
#endif
        u_xlat8 = (-u_xlat36) * 0.5 + _DashOffset;
        u_xlat16_38 = (u_xlatb0) ? u_xlat8 : _DashOffset;
        u_xlat0.x = u_xlat12.x * u_xlat16_26;
        u_xlat0.x = u_xlat0.x / u_xlat9.y;
        u_xlat8 = u_xlat9.x / u_xlat9.y;
        u_xlat12.x = u_xlat8 * u_xlat12.x + (-u_xlat16_38);
        u_xlat12.x = (-u_xlat36) * 0.5 + u_xlat12.x;
        u_xlat1.y = u_xlat16_14.x * 2.0 + -1.0;
#ifdef UNITY_ADRENO_ES3
        u_xlatb36 = !!(u_xlat24.x>=0.999938965);
#else
        u_xlatb36 = u_xlat24.x>=0.999938965;
#endif
        u_xlatb8.xy = equal(ivec4(ivec4(_DashType, _DashType, _DashType, _DashType)), ivec4(1, 2, 0, 0)).xy;
        u_xlat16_14.x = u_xlat1.y * 0.5;
        u_xlat16_14.x = u_xlat0.x * u_xlat16_14.x;
        u_xlat16_14.x = u_xlat16_14.x * _DashShapeModifier + u_xlat12.x;
        u_xlat16_14.x = (u_xlatb8.x) ? u_xlat16_14.x : u_xlat12.x;
        u_xlat16_14.x = fract(u_xlat16_14.x);
        u_xlat16_14.x = u_xlat16_14.x * 2.0 + -1.0;
        u_xlat16_14.x = (-u_xlat24.x) + abs(u_xlat16_14.x);
        u_xlat16_26 = (-u_xlat24.x) + 1.0;
        u_xlat16_14.x = u_xlat16_14.x / u_xlat16_26;
        u_xlat16_26 = u_xlat16_26 / u_xlat0.x;
        u_xlat16_25 = (-u_xlat16_14.x) * u_xlat16_26 + 1.0;
        u_xlat1.xz = vec2(u_xlat16_25);
        u_xlat1.w = u_xlat1.y;
        u_xlat16_38 = dot(u_xlat1.xy, u_xlat1.zw);
        u_xlat16_38 = sqrt(u_xlat16_38);
        u_xlat16_38 = u_xlat16_38 + -1.0;
        u_xlat16_4.x = dFdx(u_xlat16_38);
        u_xlat16_16 = dFdy(u_xlat16_38);
        u_xlat16_4.x = abs(u_xlat16_16) + abs(u_xlat16_4.x);
        u_xlat16_38 = u_xlat16_38 / u_xlat16_4.x;
        u_xlat16_38 = u_xlat16_38 + 0.5;
#ifdef UNITY_ADRENO_ES3
        u_xlat16_38 = min(max(u_xlat16_38, 0.0), 1.0);
#else
        u_xlat16_38 = clamp(u_xlat16_38, 0.0, 1.0);
#endif
        u_xlat16_38 = (-u_xlat16_38) + 1.0;
        u_xlat16_26 = float(1.0) / u_xlat16_26;
        u_xlat16_14.y = (-u_xlat16_26) + u_xlat16_14.x;
        u_xlat16_4.x = dFdx(u_xlat16_14.x);
        u_xlat16_16 = dFdy(u_xlat16_14.x);
        u_xlat16_4.x = abs(u_xlat16_16) + abs(u_xlat16_4.x);
        u_xlat16_14.xy = u_xlat16_14.xy / u_xlat16_4.xx;
        u_xlat16_14.xy = u_xlat16_14.xy + vec2(0.5, 0.5);
#ifdef UNITY_ADRENO_ES3
        u_xlat16_14.xy = min(max(u_xlat16_14.xy, 0.0), 1.0);
#else
        u_xlat16_14.xy = clamp(u_xlat16_14.xy, 0.0, 1.0);
#endif
        u_xlat16_26 = max(u_xlat16_38, u_xlat16_14.y);
        u_xlat16_14.x = (u_xlatb8.y) ? u_xlat16_26 : u_xlat16_14.x;
        u_xlat16_14.x = min(u_xlat16_14.x, u_xlat16_2.x);
        u_xlat16_2.x = (u_xlatb36) ? 0.0 : u_xlat16_14.x;
    }
    u_xlat16_14.x = vs_TEXCOORD1.y;
#ifdef UNITY_ADRENO_ES3
    u_xlat16_14.x = min(max(u_xlat16_14.x, 0.0), 1.0);
#else
    u_xlat16_14.x = clamp(u_xlat16_14.x, 0.0, 1.0);
#endif
    u_xlat16_2.x = u_xlat16_14.x * u_xlat16_2.x;
#ifdef UNITY_ADRENO_ES3
    u_xlatb0 = !!(_FillType!=int(0xFFFFFFFFu));
#else
    u_xlatb0 = _FillType!=int(0xFFFFFFFFu);
#endif
    if(u_xlatb0){
        switch(_FillType){
            case 0:
                u_xlat14 = vs_TEXCOORD3.x;
#ifdef UNITY_ADRENO_ES3
                u_xlat14 = min(max(u_xlat14, 0.0), 1.0);
#else
                u_xlat14 = clamp(u_xlat14, 0.0, 1.0);
#endif
                u_xlat16_14.x = u_xlat14;
                break;
            case 1:
                u_xlat0.x = dot(vs_TEXCOORD3.xyz, vs_TEXCOORD3.xyz);
                u_xlat0.x = sqrt(u_xlat0.x);
                u_xlat14 = u_xlat0.x / _FillStart.w;
#ifdef UNITY_ADRENO_ES3
                u_xlat14 = min(max(u_xlat14, 0.0), 1.0);
#else
                u_xlat14 = clamp(u_xlat14, 0.0, 1.0);
#endif
                u_xlat16_14.x = u_xlat14;
                break;
            default:
                u_xlat16_14.x = 0.0;
                break;
        }
        u_xlat16_0 = (-_Color) + _ColorEnd;
        u_xlat16_0 = u_xlat16_14.xxxx * u_xlat16_0 + _Color;
    } else {
        u_xlat16_0 = _Color;
    }
    u_xlat8 = u_xlat16_0.w * u_xlat16_2.x;
    u_xlat16_2.x = u_xlat16_2.x * u_xlat16_0.w + -6.10351563e-05;
#ifdef UNITY_ADRENO_ES3
    u_xlatb20 = !!(u_xlat16_2.x<0.0);
#else
    u_xlatb20 = u_xlat16_2.x<0.0;
#endif
    if(u_xlatb20){discard;}
    SV_Target0.xyz = u_xlat16_0.xyz;
    SV_Target0.w = u_xlat8;
    return;
}

#endif
                               $Globals�         _ScreenParams                         
   _ScaleMode                         _Color                           _ThicknessSpace                  0   
   _Roundness                    4      _Hollow                  8      _Sides                   <   	   _DashType                    @   	   _DashSize                     D      _DashShapeModifier                    H      _DashOffset                   L      _DashSpacing                  P   
   _DashSpace                   T   	   _DashSnap                    X   	   _ColorEnd                     `   	   _FillType                    p   
   _FillStart                    �          $Globals|        _WorldSpaceCameraPos                         _ScreenParams                           unity_OrthoParams                         
   _ScaleMode                   0  
   _Alignment                   4     _Radius                   8     _RadiusSpace                 <  
   _Thickness                    @     _ThicknessSpace                  D     _Angle                    H     _Hollow                  L     _Sides                   P  	   _FillType                    T  
   _FillSpace                   X  
   _FillStart                    `     _FillEnd                  p     unity_ObjectToWorld                  0      unity_WorldToObject                  p      unity_MatrixInvV                 �      unity_MatrixVP                   �               