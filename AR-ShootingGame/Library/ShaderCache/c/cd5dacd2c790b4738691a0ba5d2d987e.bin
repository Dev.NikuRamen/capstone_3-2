8'  <Q                         CORNER_RADIUS      INSTANCING_ON       :$  #ifdef VERTEX
#version 300 es

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 _ScreenParams;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixInvV[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	int _ScaleMode;
uniform 	mediump vec4 _Rect;
uniform 	int _FillType;
uniform 	int _FillSpace;
uniform 	vec4 _FillStart;
uniform 	vec3 _FillEnd;
in highp vec4 in_POSITION0;
out highp vec3 vs_TEXCOORD0;
out mediump vec4 vs_TEXCOORD1;
out mediump vec4 vs_TEXCOORD2;
out mediump vec3 vs_TEXCOORD3;
vec4 u_xlat0;
mediump vec4 u_xlat16_0;
vec4 u_xlat1;
bool u_xlatb1;
vec3 u_xlat2;
vec4 u_xlat3;
mediump vec3 u_xlat16_4;
vec4 u_xlat5;
mediump vec2 u_xlat16_5;
mediump vec2 u_xlat16_6;
float u_xlat8;
vec2 u_xlat15;
mediump vec2 u_xlat16_18;
mediump vec2 u_xlat16_19;
void main()
{
    u_xlat16_0.xy = _Rect.zw * vec2(0.5, 0.5) + _Rect.xy;
    u_xlat1.xyz = u_xlat16_0.yyy * hlslcc_mtx4x4unity_ObjectToWorld[1].xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_ObjectToWorld[0].xyz * u_xlat16_0.xxx + u_xlat1.xyz;
    u_xlat0.xyz = u_xlat1.xyz + hlslcc_mtx4x4unity_ObjectToWorld[3].xyz;
    u_xlat1.x = hlslcc_mtx4x4unity_MatrixVP[0].w;
    u_xlat1.y = hlslcc_mtx4x4unity_MatrixVP[1].w;
    u_xlat1.z = hlslcc_mtx4x4unity_MatrixVP[2].w;
    u_xlat1.w = hlslcc_mtx4x4unity_MatrixVP[3].w;
    u_xlat0.w = 1.0;
    u_xlat1.x = dot(u_xlat1, u_xlat0);
    u_xlat2.x = hlslcc_mtx4x4unity_MatrixVP[0].x;
    u_xlat2.y = hlslcc_mtx4x4unity_MatrixVP[1].x;
    u_xlat2.z = hlslcc_mtx4x4unity_MatrixVP[2].x;
    u_xlat8 = dot(u_xlat2.xyz, hlslcc_mtx4x4unity_MatrixInvV[1].xyz);
    u_xlat2.x = u_xlat8 / u_xlat1.x;
    u_xlat3.x = hlslcc_mtx4x4unity_MatrixVP[0].y;
    u_xlat3.y = hlslcc_mtx4x4unity_MatrixVP[1].y;
    u_xlat3.z = hlslcc_mtx4x4unity_MatrixVP[2].y;
    u_xlat8 = dot(u_xlat3.xyz, hlslcc_mtx4x4unity_MatrixInvV[1].xyz);
    u_xlat2.y = u_xlat8 / u_xlat1.x;
    u_xlat1.xy = u_xlat2.xy * _ScreenParams.xy;
    u_xlat1.x = dot(u_xlat1.xy, u_xlat1.xy);
    u_xlat1.x = sqrt(u_xlat1.x);
    u_xlat1.x = u_xlat1.x * 0.5;
    u_xlat16_4.x = dot(hlslcc_mtx4x4unity_ObjectToWorld[0].xyz, hlslcc_mtx4x4unity_ObjectToWorld[0].xyz);
    u_xlat16_4.z = dot(hlslcc_mtx4x4unity_ObjectToWorld[1].xyz, hlslcc_mtx4x4unity_ObjectToWorld[1].xyz);
    u_xlat16_4.xy = sqrt(u_xlat16_4.xz);
    u_xlat16_6.xy = (_ScaleMode != 0) ? vec2(1.0, 1.0) : u_xlat16_4.xy;
    u_xlat16_5.xy = (_ScaleMode != 0) ? u_xlat16_4.xy : vec2(1.0, 1.0);
    u_xlat16_4.xy = u_xlat16_6.xy;
    u_xlat16_4.xy = u_xlat1.xx * u_xlat16_4.xy;
    u_xlat16_4.xy = vec2(2.0, 2.0) / u_xlat16_4.xy;
    u_xlat16_18.xy = _Rect.xy * u_xlat16_5.xy + (-u_xlat16_4.xy);
    u_xlat16_0 = u_xlat16_5.xyxy * _Rect;
    u_xlat16_19.xy = u_xlat16_0.zw + u_xlat16_0.xy;
    u_xlat16_19.xy = u_xlat16_4.xy + u_xlat16_19.xy;
    u_xlat1.xy = (-u_xlat16_18.xy) + u_xlat16_19.xy;
    u_xlat15.xy = in_POSITION0.xy + vec2(1.0, 1.0);
    u_xlat15.xy = u_xlat15.xy * vec2(0.5, 0.5);
    u_xlat1.xy = u_xlat15.xy * u_xlat1.xy + u_xlat16_18.xy;
    u_xlat2.xy = u_xlat1.xy / u_xlat16_5.xy;
    u_xlat3 = u_xlat2.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat3 = hlslcc_mtx4x4unity_ObjectToWorld[0] * u_xlat2.xxxx + u_xlat3;
    u_xlat3 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat3;
    u_xlat3 = u_xlat3 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat5 = u_xlat3.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat5 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat3.xxxx + u_xlat5;
    u_xlat5 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat3.zzzz + u_xlat5;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat3.wwww + u_xlat5;
    u_xlat2.z = in_POSITION0.z;
    u_xlat2.xyz = (int(_FillSpace) != 0) ? u_xlat3.xyz : u_xlat2.xyz;
    u_xlat2.xyz = u_xlat2.xyz + (-_FillStart.xyz);
    u_xlat3.xyz = (-_FillStart.xyz) + _FillEnd.xyz;
    u_xlat1.x = dot(u_xlat3.xyz, u_xlat2.xyz);
    u_xlat16_18.x = dot(u_xlat3.xyz, u_xlat3.xyz);
    u_xlat3.x = u_xlat1.x / u_xlat16_18.x;
#ifdef UNITY_ADRENO_ES3
    u_xlatb1 = !!(_FillType==1);
#else
    u_xlatb1 = _FillType==1;
#endif
    u_xlat3.y = float(0.0);
    u_xlat3.z = float(0.0);
    u_xlat2.xyz = (bool(u_xlatb1)) ? u_xlat2.xyz : u_xlat3.xyz;
#ifdef UNITY_ADRENO_ES3
    u_xlatb1 = !!(_FillType!=int(0xFFFFFFFFu));
#else
    u_xlatb1 = _FillType!=int(0xFFFFFFFFu);
#endif
    vs_TEXCOORD0.xyz = bool(u_xlatb1) ? u_xlat2.xyz : vec3(0.0, 0.0, 0.0);
    u_xlat16_18.xy = u_xlat16_0.zw * vec2(0.5, 0.5) + u_xlat16_4.xy;
    u_xlat16_4.xy = u_xlat16_0.zw * vec2(-0.5, -0.5) + (-u_xlat16_4.xy);
    vs_TEXCOORD2 = u_xlat16_0;
    u_xlat1.xy = (-u_xlat16_4.xy) + u_xlat16_18.xy;
    u_xlat0.xy = u_xlat15.xy * u_xlat1.xy + u_xlat16_4.xy;
    u_xlat0.zw = in_POSITION0.xy;
    vs_TEXCOORD1 = u_xlat0;
    vs_TEXCOORD3.xyz = vec3(0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp float;
precision highp int;
#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
vec4 ImmCB_0[4];
uniform 	mediump vec4 _Color;
uniform 	mediump vec4 _CornerRadii;
uniform 	mediump vec4 _ColorEnd;
uniform 	int _FillType;
uniform 	vec4 _FillStart;
in highp vec3 vs_TEXCOORD0;
in mediump vec4 vs_TEXCOORD1;
in mediump vec4 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
mediump vec4 u_xlat16_0;
ivec2 u_xlati0;
float u_xlat1;
mediump vec2 u_xlat16_1;
vec2 u_xlat2;
int u_xlati2;
bool u_xlatb2;
vec2 u_xlat3;
mediump vec2 u_xlat16_5;
float u_xlat6;
bool u_xlatb6;
ivec2 u_xlati8;
mediump float u_xlat16_9;
vec2 u_xlat10;
void main()
{
ImmCB_0[0] = vec4(1.0,0.0,0.0,0.0);
ImmCB_0[1] = vec4(0.0,1.0,0.0,0.0);
ImmCB_0[2] = vec4(0.0,0.0,1.0,0.0);
ImmCB_0[3] = vec4(0.0,0.0,0.0,1.0);
    u_xlati0.xy = ivec2(uvec2(lessThan(vec4(0.0, 0.0, 0.0, 0.0), vs_TEXCOORD1.zwzz).xy) * 0xFFFFFFFFu);
    u_xlati8.xy = ivec2(uvec2(lessThan(vs_TEXCOORD1.zwzw, vec4(0.0, 0.0, 0.0, 0.0)).xy) * 0xFFFFFFFFu);
    u_xlati0.xy = (-u_xlati0.xy) + u_xlati8.xy;
    u_xlat16_1.xy = vec2(u_xlati0.xy);
    u_xlat16_9 = min(vs_TEXCOORD2.w, vs_TEXCOORD2.z);
    u_xlat16_9 = u_xlat16_9 * 0.5;
    u_xlat16_0 = min(vec4(u_xlat16_9), _CornerRadii);
    u_xlat16_5.x = u_xlat16_1.x * u_xlat16_1.y;
    u_xlat16_1.x = (-u_xlat16_5.x) * 0.5 + u_xlat16_1.x;
    u_xlat16_1.x = u_xlat16_1.x + 1.5;
    u_xlati2 = int(u_xlat16_1.x);
    u_xlat16_1.x = dot(u_xlat16_0, ImmCB_0[u_xlati2]);
    u_xlat16_5.xy = (-u_xlat16_1.xx) * vec2(2.0, 2.0) + vs_TEXCOORD2.zw;
    u_xlat2.xy = (-u_xlat16_5.xy) * vec2(0.5, 0.5) + abs(vs_TEXCOORD1.xy);
    u_xlat10.xy = max(u_xlat2.xy, vec2(0.0, 0.0));
    u_xlat10.x = dot(u_xlat10.xy, u_xlat10.xy);
    u_xlat10.x = sqrt(u_xlat10.x);
    u_xlat2.x = max(u_xlat2.y, u_xlat2.x);
    u_xlat2.x = min(u_xlat2.x, 0.0);
    u_xlat2.x = u_xlat2.x + u_xlat10.x;
    u_xlat2.x = (-u_xlat16_1.x) + u_xlat2.x;
    u_xlat3.x = dFdx(u_xlat2.x);
    u_xlat3.y = dFdy(u_xlat2.x);
    u_xlat6 = dot(u_xlat3.xy, u_xlat3.xy);
    u_xlat6 = sqrt(u_xlat6);
    u_xlat6 = max(u_xlat6, 9.99999975e-06);
    u_xlat2.x = u_xlat2.x / u_xlat6;
    u_xlat2.x = u_xlat2.x + 0.5;
#ifdef UNITY_ADRENO_ES3
    u_xlat2.x = min(max(u_xlat2.x, 0.0), 1.0);
#else
    u_xlat2.x = clamp(u_xlat2.x, 0.0, 1.0);
#endif
    u_xlat2.x = (-u_xlat2.x) + 1.0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb6 = !!(_FillType!=int(0xFFFFFFFFu));
#else
    u_xlatb6 = _FillType!=int(0xFFFFFFFFu);
#endif
    if(u_xlatb6){
        switch(_FillType){
            case 0:
                u_xlat1 = vs_TEXCOORD0.x;
#ifdef UNITY_ADRENO_ES3
                u_xlat1 = min(max(u_xlat1, 0.0), 1.0);
#else
                u_xlat1 = clamp(u_xlat1, 0.0, 1.0);
#endif
                u_xlat16_1.x = u_xlat1;
                break;
            case 1:
                u_xlat6 = dot(vs_TEXCOORD0.xyz, vs_TEXCOORD0.xyz);
                u_xlat6 = sqrt(u_xlat6);
                u_xlat1 = u_xlat6 / _FillStart.w;
#ifdef UNITY_ADRENO_ES3
                u_xlat1 = min(max(u_xlat1, 0.0), 1.0);
#else
                u_xlat1 = clamp(u_xlat1, 0.0, 1.0);
#endif
                u_xlat16_1.x = u_xlat1;
                break;
            default:
                u_xlat16_1.x = 0.0;
                break;
        }
        u_xlat16_0 = (-_Color) + _ColorEnd;
        u_xlat16_0 = u_xlat16_1.xxxx * u_xlat16_0 + _Color;
    } else {
        u_xlat16_0 = _Color;
    }
    u_xlat6 = u_xlat16_0.w * u_xlat2.x;
    u_xlat16_1.x = u_xlat2.x * u_xlat16_0.w + -6.10351563e-05;
#ifdef UNITY_ADRENO_ES3
    u_xlatb2 = !!(u_xlat16_1.x<0.0);
#else
    u_xlatb2 = u_xlat16_1.x<0.0;
#endif
    if(u_xlatb2){discard;}
    SV_Target0.xyz = u_xlat16_0.xyz;
    SV_Target0.w = u_xlat6;
    return;
}

#endif
                               $GlobalsP         _Color                           _CornerRadii                     	   _ColorEnd                         	   _FillType                    0   
   _FillStart                    @          $Globals  
      _ScreenParams                         
   _ScaleMode                   �      _Rect                     �   	   _FillType                    �   
   _FillSpace                   �   
   _FillStart                          _FillEnd                       unity_ObjectToWorld                        unity_MatrixInvV                 P      unity_MatrixVP                   �               