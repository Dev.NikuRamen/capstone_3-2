`3  <Q                         STEREO_MULTIVIEW_ON     .  #ifdef VERTEX
#version 300 es
#extension GL_OVR_multiview2 : require

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 _ScreenParams;
uniform 	vec4 unity_OrthoParams;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	int _Alignment;
uniform 	mediump float _Radius;
uniform 	int _RadiusSpace;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(0) uniform UnityStereoGlobals {
#endif
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_StereoMatrixP[8];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_StereoMatrixV[8];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_StereoMatrixInvV[8];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_StereoMatrixVP[8];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_StereoCameraProjection[8];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_StereoCameraInvProjection[8];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_StereoWorldToCamera[8];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_StereoCameraToWorld[8];
	UNITY_UNIFORM vec3 unity_StereoWorldSpaceCameraPos[2];
	UNITY_UNIFORM vec4 unity_StereoScaleOffset[2];
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(1) uniform UnityStereoEyeIndices {
#endif
	UNITY_UNIFORM vec4 unity_StereoEyeIndices[2];
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
layout(num_views = 2) in;
in highp vec4 in_POSITION0;
in highp vec2 in_TEXCOORD0;
out mediump vec4 vs_TEXCOORD0;
out highp float vs_BLENDWEIGHT0;
vec4 u_xlat0;
mediump vec3 u_xlat16_0;
int u_xlati1;
uint u_xlatu1;
vec4 u_xlat2;
vec4 u_xlat3;
vec3 u_xlat4;
mediump vec3 u_xlat16_5;
vec3 u_xlat6;
mediump vec3 u_xlat16_6;
vec3 u_xlat7;
mediump float u_xlat16_12;
float u_xlat13;
mediump float u_xlat16_18;
float u_xlat19;
float u_xlat20;
uint u_xlatu20;
bool u_xlatb20;
void main()
{
    u_xlat16_0.x = dot(hlslcc_mtx4x4unity_ObjectToWorld[0].xyz, hlslcc_mtx4x4unity_ObjectToWorld[0].xyz);
    u_xlat16_0.y = dot(hlslcc_mtx4x4unity_ObjectToWorld[1].xyz, hlslcc_mtx4x4unity_ObjectToWorld[1].xyz);
    u_xlat16_0.z = dot(hlslcc_mtx4x4unity_ObjectToWorld[2].xyz, hlslcc_mtx4x4unity_ObjectToWorld[2].xyz);
    u_xlat16_0.xyz = sqrt(u_xlat16_0.xyz);
    u_xlat16_0.x = u_xlat16_0.y + u_xlat16_0.x;
    u_xlat16_0.x = u_xlat16_0.z + u_xlat16_0.x;
    u_xlat16_0.x = u_xlat16_0.x * 0.333333343;
    u_xlat16_6.x = u_xlat16_0.x * _Radius;
    u_xlati1 = int(int(gl_ViewID_OVR) << 2);
    u_xlat16_12 = u_xlat16_6.x + u_xlat16_6.x;
    u_xlat2.x = hlslcc_mtx4x4unity_StereoMatrixVP[u_xlati1].w;
    u_xlat2.y = hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati1 + 1)].w;
    u_xlat2.z = hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati1 + 2)].w;
    u_xlat2.w = hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati1 + 3)].w;
    u_xlat3.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz;
    u_xlat3.w = 1.0;
    u_xlat7.x = dot(u_xlat2, u_xlat3);
    u_xlat2.x = hlslcc_mtx4x4unity_StereoMatrixVP[u_xlati1].x;
    u_xlat2.y = hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati1 + 1)].x;
    u_xlat2.z = hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati1 + 2)].x;
    u_xlat13 = dot(u_xlat2.xyz, hlslcc_mtx4x4unity_StereoMatrixInvV[u_xlati1].xyz);
    u_xlat2.x = u_xlat13 / u_xlat7.x;
    u_xlat3.x = hlslcc_mtx4x4unity_StereoMatrixVP[u_xlati1].y;
    u_xlat3.y = hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati1 + 1)].y;
    u_xlat3.z = hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati1 + 2)].y;
    u_xlat13 = dot(u_xlat3.xyz, hlslcc_mtx4x4unity_StereoMatrixInvV[u_xlati1].xyz);
    u_xlat2.y = u_xlat13 / u_xlat7.x;
    u_xlat7.xy = u_xlat2.xy * _ScreenParams.xy;
    u_xlat7.x = dot(u_xlat7.xy, u_xlat7.xy);
    u_xlat7.x = sqrt(u_xlat7.x);
    u_xlat7.x = u_xlat7.x * 0.5;
    switch(_RadiusSpace){
        case 0:
            u_xlat7.y = u_xlat16_12 * u_xlat7.x;
            break;
        case 1:
            u_xlat7.y = u_xlat16_12;
            break;
        case 2:
            u_xlat19 = min(_ScreenParams.y, _ScreenParams.x);
            u_xlat2.x = u_xlat16_6.x * 0.0199999996;
            u_xlat7.y = u_xlat19 * u_xlat2.x;
            break;
        default:
            u_xlat7.y = 0.0;
            break;
    }
    u_xlat16_6.x = max(u_xlat7.y, 1.0);
    u_xlat16_6.x = u_xlat16_6.x / u_xlat7.x;
    u_xlat16_12 = 2.0 / u_xlat7.x;
    u_xlat16_18 = u_xlat16_6.x * 0.5;
    u_xlat16_6.x = u_xlat16_6.x * 0.5 + u_xlat16_12;
    u_xlat16_12 = u_xlat16_18 / u_xlat16_6.x;
    u_xlat2.xy = u_xlat16_6.xx * in_TEXCOORD0.xy;
    u_xlat7.xz = in_TEXCOORD0.xy / vec2(u_xlat16_12);
#ifdef UNITY_ADRENO_ES3
    u_xlatb20 = !!(_Alignment==1);
#else
    u_xlatb20 = _Alignment==1;
#endif
    if(u_xlatb20){
#ifdef UNITY_ADRENO_ES3
        u_xlatb20 = !!(unity_OrthoParams.w==1.0);
#else
        u_xlatb20 = unity_OrthoParams.w==1.0;
#endif
        if(u_xlatb20){
            u_xlat16_6.xyz = (-hlslcc_mtx4x4unity_StereoMatrixInvV[(u_xlati1 + 2)].xyz);
        } else {
            u_xlatu20 = gl_ViewID_OVR;
            u_xlat3.xyz = (-hlslcc_mtx4x4unity_ObjectToWorld[3].xyz) + unity_StereoWorldSpaceCameraPos[int(u_xlatu20)].xyz;
            u_xlat20 = dot(u_xlat3.xyz, u_xlat3.xyz);
            u_xlat20 = inversesqrt(u_xlat20);
            u_xlat6.xyz = vec3(u_xlat20) * u_xlat3.xyz;
            u_xlat16_6.xyz = u_xlat6.xyz;
        }
        u_xlat3.xyz = (-u_xlat16_6.yyy) * hlslcc_mtx4x4unity_WorldToObject[1].yzx;
        u_xlat3.xyz = hlslcc_mtx4x4unity_WorldToObject[0].yzx * (-u_xlat16_6.xxx) + u_xlat3.xyz;
        u_xlat3.xyz = hlslcc_mtx4x4unity_WorldToObject[2].yzx * (-u_xlat16_6.zzz) + u_xlat3.xyz;
        u_xlat4.xyz = hlslcc_mtx4x4unity_WorldToObject[1].yzx * hlslcc_mtx4x4unity_StereoMatrixInvV[u_xlati1].yyy;
        u_xlat4.xyz = hlslcc_mtx4x4unity_WorldToObject[0].yzx * hlslcc_mtx4x4unity_StereoMatrixInvV[u_xlati1].xxx + u_xlat4.xyz;
        u_xlat4.xyz = hlslcc_mtx4x4unity_WorldToObject[2].yzx * hlslcc_mtx4x4unity_StereoMatrixInvV[u_xlati1].zzz + u_xlat4.xyz;
        u_xlat16_6.xyz = u_xlat3.yzx * u_xlat4.xyz;
        u_xlat16_6.xyz = u_xlat3.xyz * u_xlat4.yzx + (-u_xlat16_6.xyz);
        u_xlat16_5.x = dot(u_xlat16_6.xyz, u_xlat16_6.xyz);
        u_xlat16_5.x = inversesqrt(u_xlat16_5.x);
        u_xlat16_6.xyz = u_xlat16_6.xyz * u_xlat16_5.xxx;
        u_xlat16_5.xyz = u_xlat3.xyz * u_xlat16_6.zxy;
        u_xlat16_5.xyz = u_xlat16_6.yzx * u_xlat3.yzx + (-u_xlat16_5.xyz);
        u_xlat3.xyz = u_xlat16_6.xyz * u_xlat2.yyy;
        u_xlat2.xyz = u_xlat2.xxx * u_xlat16_5.xyz + u_xlat3.xyz;
    } else {
        u_xlat2.z = in_POSITION0.z;
    }
    u_xlat2.xyz = u_xlat2.xyz / u_xlat16_0.xxx;
    u_xlat0 = u_xlat2.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * u_xlat2.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * u_xlat2.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat2 = u_xlat0.yyyy * hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati1 + 1)];
    u_xlat2 = hlslcc_mtx4x4unity_StereoMatrixVP[u_xlati1] * u_xlat0.xxxx + u_xlat2;
    u_xlat2 = hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati1 + 2)] * u_xlat0.zzzz + u_xlat2;
    gl_Position = hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati1 + 3)] * u_xlat0.wwww + u_xlat2;
    vs_TEXCOORD0.xyz = u_xlat7.xzy;
    vs_TEXCOORD0.w = 0.0;
    u_xlatu1 = gl_ViewID_OVR;
    vs_BLENDWEIGHT0 = unity_StereoEyeIndices[int(u_xlatu1)].x;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp float;
precision highp int;
#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	mediump vec4 _Color;
uniform 	mediump vec4 _ColorOuterStart;
uniform 	mediump vec4 _ColorInnerEnd;
uniform 	mediump vec4 _ColorOuterEnd;
in mediump vec4 vs_TEXCOORD0;
layout(location = 0) out mediump vec4 SV_Target0;
mediump vec4 u_xlat16_0;
float u_xlat1;
bool u_xlatb1;
mediump vec4 u_xlat16_2;
mediump vec4 u_xlat16_3;
vec2 u_xlat4;
mediump float u_xlat16_5;
float u_xlat6;
bool u_xlatb6;
mediump float u_xlat16_7;
bool u_xlatb11;
void main()
{
    u_xlat16_0.x = max(abs(vs_TEXCOORD0.x), abs(vs_TEXCOORD0.y));
    u_xlat16_0.x = float(1.0) / u_xlat16_0.x;
    u_xlat16_5 = min(abs(vs_TEXCOORD0.x), abs(vs_TEXCOORD0.y));
    u_xlat16_0.x = u_xlat16_0.x * u_xlat16_5;
    u_xlat16_5 = u_xlat16_0.x * u_xlat16_0.x;
    u_xlat1 = u_xlat16_5 * 0.0208350997 + -0.0851330012;
    u_xlat1 = u_xlat16_5 * u_xlat1 + 0.180141002;
    u_xlat1 = u_xlat16_5 * u_xlat1 + -0.330299497;
    u_xlat1 = u_xlat16_5 * u_xlat1 + 0.999866009;
    u_xlat6 = u_xlat16_0.x * u_xlat1;
    u_xlat6 = u_xlat6 * -2.0 + 1.57079637;
#ifdef UNITY_ADRENO_ES3
    u_xlatb11 = !!(abs(vs_TEXCOORD0.x)<abs(vs_TEXCOORD0.y));
#else
    u_xlatb11 = abs(vs_TEXCOORD0.x)<abs(vs_TEXCOORD0.y);
#endif
    u_xlat6 = u_xlatb11 ? u_xlat6 : float(0.0);
    u_xlat1 = u_xlat16_0.x * u_xlat1 + u_xlat6;
#ifdef UNITY_ADRENO_ES3
    u_xlatb6 = !!((-vs_TEXCOORD0.x)<vs_TEXCOORD0.x);
#else
    u_xlatb6 = (-vs_TEXCOORD0.x)<vs_TEXCOORD0.x;
#endif
    u_xlat6 = u_xlatb6 ? -3.14159274 : float(0.0);
    u_xlat1 = u_xlat6 + u_xlat1;
    u_xlat16_0.x = min((-vs_TEXCOORD0.x), (-vs_TEXCOORD0.y));
#ifdef UNITY_ADRENO_ES3
    u_xlatb6 = !!(u_xlat16_0.x<(-u_xlat16_0.x));
#else
    u_xlatb6 = u_xlat16_0.x<(-u_xlat16_0.x);
#endif
    u_xlat16_0.x = max((-vs_TEXCOORD0.x), (-vs_TEXCOORD0.y));
#ifdef UNITY_ADRENO_ES3
    u_xlatb11 = !!(u_xlat16_0.x>=(-u_xlat16_0.x));
#else
    u_xlatb11 = u_xlat16_0.x>=(-u_xlat16_0.x);
#endif
    u_xlatb6 = u_xlatb11 && u_xlatb6;
    u_xlat1 = (u_xlatb6) ? (-u_xlat1) : u_xlat1;
    u_xlat1 = u_xlat1 * 0.159154937 + 0.5;
#ifdef UNITY_ADRENO_ES3
    u_xlat1 = min(max(u_xlat1, 0.0), 1.0);
#else
    u_xlat1 = clamp(u_xlat1, 0.0, 1.0);
#endif
    u_xlat16_0 = (-_ColorInnerEnd) + _ColorOuterEnd;
    u_xlat16_2.x = dot(vs_TEXCOORD0.xy, vs_TEXCOORD0.xy);
    u_xlat16_2.x = sqrt(u_xlat16_2.x);
    u_xlat16_7 = min(u_xlat16_2.x, 1.0);
    u_xlat6 = (-u_xlat16_2.x) + 1.0;
    u_xlat16_0 = vec4(u_xlat16_7) * u_xlat16_0 + _ColorInnerEnd;
    u_xlat16_3 = (-_Color) + _ColorOuterStart;
    u_xlat16_2 = vec4(u_xlat16_7) * u_xlat16_3 + _Color;
    u_xlat16_0 = u_xlat16_0 + (-u_xlat16_2);
    u_xlat16_0 = vec4(u_xlat1) * u_xlat16_0 + u_xlat16_2;
    u_xlat4.x = dFdx(u_xlat6);
    u_xlat4.y = dFdy(u_xlat6);
    u_xlat1 = dot(u_xlat4.xy, u_xlat4.xy);
    u_xlat1 = sqrt(u_xlat1);
    u_xlat1 = max(u_xlat1, 9.99999975e-06);
    u_xlat1 = u_xlat6 / u_xlat1;
    u_xlat1 = u_xlat1 + 0.5;
#ifdef UNITY_ADRENO_ES3
    u_xlat1 = min(max(u_xlat1, 0.0), 1.0);
#else
    u_xlat1 = clamp(u_xlat1, 0.0, 1.0);
#endif
    u_xlat16_2.x = vs_TEXCOORD0.z;
#ifdef UNITY_ADRENO_ES3
    u_xlat16_2.x = min(max(u_xlat16_2.x, 0.0), 1.0);
#else
    u_xlat16_2.x = clamp(u_xlat16_2.x, 0.0, 1.0);
#endif
    u_xlat16_2.x = u_xlat1 * u_xlat16_2.x;
    u_xlat16_7 = u_xlat16_2.x * u_xlat16_0.w + -6.10351563e-05;
    u_xlat1 = u_xlat16_0.w * u_xlat16_2.x;
    u_xlat16_0.xyz = (-u_xlat16_0.xyz) * vec3(u_xlat1) + vec3(1.0, 1.0, 1.0);
    SV_Target0.w = u_xlat1;
    u_xlat16_0.xyz = max(u_xlat16_0.xyz, vec3(6.10351563e-05, 6.10351563e-05, 6.10351563e-05));
    SV_Target0.xyz = vec3(1.0, 1.0, 1.0) / u_xlat16_0.xyz;
#ifdef UNITY_ADRENO_ES3
    u_xlatb1 = !!(u_xlat16_7<0.0);
#else
    u_xlatb1 = u_xlat16_7<0.0;
#endif
    if(u_xlatb1){discard;}
    return;
}

#endif
                              $Globals@         _Color                           _ColorOuterStart                        _ColorInnerEnd                           _ColorOuterEnd                    0          $Globals�         _ScreenParams                            unity_OrthoParams                        
   _Alignment                   �      _Radius                   �      _RadiusSpace                 �      unity_ObjectToWorld                         unity_WorldToObject                  `          UnityStereoGlobals  @  
      unity_StereoWorldSpaceCameraPos                        unity_StereoScaleOffset                        unity_StereoMatrixP                        unity_StereoMatrixV                 �      unity_StereoMatrixInvV                        unity_StereoMatrixVP                �     unity_StereoCameraProjection                      unity_StereoCameraInvProjection                 �     unity_StereoWorldToCamera                         unity_StereoCameraToWorld                   �         UnityStereoEyeIndices             unity_StereoEyeIndices                                 UnityStereoGlobals                UnityStereoEyeIndices                 