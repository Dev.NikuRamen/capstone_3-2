�  <Q                         STEREO_MULTIVIEW_ON     h  #ifdef VERTEX
#version 300 es
#extension GL_OVR_multiview2 : require

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 _ScreenParams;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	float _Radius;
uniform 	float _Length;
uniform 	int _SizeSpace;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(0) uniform UnityStereoGlobals {
#endif
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_StereoMatrixP[8];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_StereoMatrixV[8];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_StereoMatrixInvV[8];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_StereoMatrixVP[8];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_StereoCameraProjection[8];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_StereoCameraInvProjection[8];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_StereoWorldToCamera[8];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_StereoCameraToWorld[8];
	UNITY_UNIFORM vec3 unity_StereoWorldSpaceCameraPos[2];
	UNITY_UNIFORM vec4 unity_StereoScaleOffset[2];
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(1) uniform UnityStereoEyeIndices {
#endif
	UNITY_UNIFORM vec4 unity_StereoEyeIndices[2];
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
layout(num_views = 2) in;
in highp vec4 in_POSITION0;
out highp float vs_TEXCOORD0;
out highp float vs_BLENDWEIGHT0;
int u_xlati0;
uint u_xlatu0;
vec4 u_xlat1;
vec4 u_xlat2;
mediump vec2 u_xlat16_3;
vec3 u_xlat4;
bool u_xlatb4;
mediump float u_xlat16_7;
float u_xlat8;
mediump float u_xlat16_11;
float u_xlat12;
void main()
{
    u_xlati0 = int(int(gl_ViewID_OVR) << 2);
    u_xlat1.x = hlslcc_mtx4x4unity_StereoMatrixVP[u_xlati0].w;
    u_xlat1.y = hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati0 + 1)].w;
    u_xlat1.z = hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati0 + 2)].w;
    u_xlat1.w = hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati0 + 3)].w;
    u_xlat2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz;
    u_xlat2.w = 1.0;
    u_xlat4.x = dot(u_xlat1, u_xlat2);
    u_xlat1.x = hlslcc_mtx4x4unity_StereoMatrixVP[u_xlati0].x;
    u_xlat1.y = hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati0 + 1)].x;
    u_xlat1.z = hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati0 + 2)].x;
    u_xlat8 = dot(u_xlat1.xyz, hlslcc_mtx4x4unity_StereoMatrixInvV[u_xlati0].xyz);
    u_xlat1.x = u_xlat8 / u_xlat4.x;
    u_xlat2.x = hlslcc_mtx4x4unity_StereoMatrixVP[u_xlati0].y;
    u_xlat2.y = hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati0 + 1)].y;
    u_xlat2.z = hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati0 + 2)].y;
    u_xlat8 = dot(u_xlat2.xyz, hlslcc_mtx4x4unity_StereoMatrixInvV[u_xlati0].xyz);
    u_xlat1.y = u_xlat8 / u_xlat4.x;
    u_xlat4.xy = u_xlat1.xy * _ScreenParams.xy;
    u_xlat4.x = dot(u_xlat4.xy, u_xlat4.xy);
    u_xlat4.x = sqrt(u_xlat4.x);
    u_xlat4.x = u_xlat4.x * 0.5;
    switch(_SizeSpace){
        case 0:
            u_xlat8 = u_xlat4.x * _Length;
            break;
        case 1:
            u_xlat8 = _Length;
            break;
        case 2:
            u_xlat12 = min(_ScreenParams.y, _ScreenParams.x);
            u_xlat12 = u_xlat12 * _Length;
            u_xlat8 = u_xlat12 * 0.00999999978;
            break;
        default:
            u_xlat8 = 0.0;
            break;
    }
    u_xlat16_3.x = max(u_xlat8, 1.0);
    u_xlat12 = _Radius + _Radius;
    switch(_SizeSpace){
        case 0:
            u_xlat12 = u_xlat4.x * u_xlat12;
            break;
        case 1:
            break;
        case 2:
            u_xlat1.x = min(_ScreenParams.y, _ScreenParams.x);
            u_xlat1.x = u_xlat1.x * _Radius;
            u_xlat12 = u_xlat1.x * 0.0199999996;
            break;
        default:
            u_xlat12 = 0.0;
            break;
    }
    u_xlat16_3.y = max(u_xlat12, 1.0);
    u_xlat16_3.xy = u_xlat16_3.xy / u_xlat4.xx;
    u_xlat16_7 = u_xlat16_3.y * 0.5;
    u_xlat16_11 = min(u_xlat12, u_xlat8);
#ifdef UNITY_ADRENO_ES3
    u_xlatb4 = !!(0.5<in_POSITION0.z);
#else
    u_xlatb4 = 0.5<in_POSITION0.z;
#endif
    u_xlat4.x = (u_xlatb4) ? u_xlat16_3.x : u_xlat16_7;
    u_xlat4.xyz = u_xlat4.xxx * in_POSITION0.xyz;
    u_xlat1 = u_xlat4.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat1 = hlslcc_mtx4x4unity_ObjectToWorld[0] * u_xlat4.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_ObjectToWorld[2] * u_xlat4.zzzz + u_xlat1;
    u_xlat1 = u_xlat1 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat2 = u_xlat1.yyyy * hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati0 + 1)];
    u_xlat2 = hlslcc_mtx4x4unity_StereoMatrixVP[u_xlati0] * u_xlat1.xxxx + u_xlat2;
    u_xlat2 = hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati0 + 2)] * u_xlat1.zzzz + u_xlat2;
    gl_Position = hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati0 + 3)] * u_xlat1.wwww + u_xlat2;
    vs_TEXCOORD0 = u_xlat16_11;
    u_xlatu0 = gl_ViewID_OVR;
    vs_BLENDWEIGHT0 = unity_StereoEyeIndices[int(u_xlatu0)].x;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp float;
precision highp int;
#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 _Color;
in highp float vs_TEXCOORD0;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
mediump float u_xlat16_1;
bool u_xlatb2;
void main()
{
    u_xlat0.x = vs_TEXCOORD0;
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat16_1 = u_xlat0.x * _Color.w + -6.10351563e-05;
    u_xlat0.w = u_xlat0.x * _Color.w;
#ifdef UNITY_ADRENO_ES3
    u_xlatb2 = !!(u_xlat16_1<0.0);
#else
    u_xlatb2 = u_xlat16_1<0.0;
#endif
    if(u_xlatb2){discard;}
    u_xlat0.xyz = _Color.xyz;
    SV_Target0 = u_xlat0;
    return;
}

#endif
                             $Globals         _Color                               $Globals\         _ScreenParams                            _Radius                   P      _Length                   T   
   _SizeSpace                   X      unity_ObjectToWorld                            UnityStereoGlobals  @  
      unity_StereoWorldSpaceCameraPos                        unity_StereoScaleOffset                        unity_StereoMatrixP                        unity_StereoMatrixV                 �      unity_StereoMatrixInvV                        unity_StereoMatrixVP                �     unity_StereoCameraProjection                      unity_StereoCameraInvProjection                 �     unity_StereoWorldToCamera                         unity_StereoCameraToWorld                   �         UnityStereoEyeIndices             unity_StereoEyeIndices                                 UnityStereoGlobals                UnityStereoEyeIndices                 