8T  <Q                         BORDERED   INSTANCING_ON       �O  #ifdef VERTEX
#version 300 es

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 _ScreenParams;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixInvV[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	int _ScaleMode;
uniform 	mediump vec4 _Rect;
uniform 	mediump float _Thickness;
uniform 	mediump float _ThicknessSpace;
uniform 	int _FillType;
uniform 	int _FillSpace;
uniform 	vec4 _FillStart;
uniform 	vec3 _FillEnd;
in highp vec4 in_POSITION0;
out highp vec3 vs_TEXCOORD0;
out mediump vec4 vs_TEXCOORD1;
out mediump vec4 vs_TEXCOORD2;
out mediump vec3 vs_TEXCOORD3;
mediump vec4 u_xlat16_0;
vec4 u_xlat1;
mediump vec2 u_xlat16_1;
vec4 u_xlat2;
mediump vec2 u_xlat16_2;
vec4 u_xlat3;
vec4 u_xlat4;
int u_xlati4;
vec4 u_xlat5;
vec3 u_xlat6;
mediump float u_xlat16_7;
mediump vec2 u_xlat16_8;
mediump float u_xlat16_9;
vec2 u_xlat12;
bool u_xlatb12;
mediump vec2 u_xlat16_18;
mediump vec2 u_xlat16_19;
mediump vec2 u_xlat16_20;
float u_xlat21;
bool u_xlatb21;
float u_xlat30;
void main()
{
    u_xlat16_0.x = dot(hlslcc_mtx4x4unity_ObjectToWorld[0].xyz, hlslcc_mtx4x4unity_ObjectToWorld[0].xyz);
    u_xlat16_0.z = dot(hlslcc_mtx4x4unity_ObjectToWorld[1].xyz, hlslcc_mtx4x4unity_ObjectToWorld[1].xyz);
    u_xlat16_0.xy = sqrt(u_xlat16_0.xz);
    u_xlat16_8.xy = (_ScaleMode != 0) ? vec2(1.0, 1.0) : u_xlat16_0.xy;
    u_xlat16_2.xy = (_ScaleMode != 0) ? u_xlat16_0.xy : vec2(1.0, 1.0);
    u_xlat16_1.xy = u_xlat16_8.xy;
    u_xlat16_0.x = u_xlat16_0.y + u_xlat16_0.x;
    u_xlat16_0.x = u_xlat16_0.x * 0.5;
    u_xlat16_0.x = (_ScaleMode != 0) ? 1.0 : u_xlat16_0.x;
    u_xlat16_9 = u_xlat16_0.x * _Thickness;
    u_xlat16_18.xy = _Rect.zw * vec2(0.5, 0.5) + _Rect.xy;
    u_xlat3.xyz = u_xlat16_18.yyy * hlslcc_mtx4x4unity_ObjectToWorld[1].xyz;
    u_xlat3.xyz = hlslcc_mtx4x4unity_ObjectToWorld[0].xyz * u_xlat16_18.xxx + u_xlat3.xyz;
    u_xlat3.xyz = u_xlat3.xyz + hlslcc_mtx4x4unity_ObjectToWorld[3].xyz;
    u_xlati4 = int(_ThicknessSpace);
    u_xlat5.x = hlslcc_mtx4x4unity_MatrixVP[0].w;
    u_xlat5.y = hlslcc_mtx4x4unity_MatrixVP[1].w;
    u_xlat5.z = hlslcc_mtx4x4unity_MatrixVP[2].w;
    u_xlat5.w = hlslcc_mtx4x4unity_MatrixVP[3].w;
    u_xlat3.w = 1.0;
    u_xlat3.x = dot(u_xlat5, u_xlat3);
    u_xlat5.x = hlslcc_mtx4x4unity_MatrixVP[0].x;
    u_xlat5.y = hlslcc_mtx4x4unity_MatrixVP[1].x;
    u_xlat5.z = hlslcc_mtx4x4unity_MatrixVP[2].x;
    u_xlat12.x = dot(u_xlat5.xyz, hlslcc_mtx4x4unity_MatrixInvV[1].xyz);
    u_xlat5.x = u_xlat12.x / u_xlat3.x;
    u_xlat6.x = hlslcc_mtx4x4unity_MatrixVP[0].y;
    u_xlat6.y = hlslcc_mtx4x4unity_MatrixVP[1].y;
    u_xlat6.z = hlslcc_mtx4x4unity_MatrixVP[2].y;
    u_xlat12.x = dot(u_xlat6.xyz, hlslcc_mtx4x4unity_MatrixInvV[1].xyz);
    u_xlat5.y = u_xlat12.x / u_xlat3.x;
    u_xlat3.xy = u_xlat5.xy * _ScreenParams.xy;
    u_xlat3.x = dot(u_xlat3.xy, u_xlat3.xy);
    u_xlat3.x = sqrt(u_xlat3.x);
    u_xlat3.x = u_xlat3.x * 0.5;
    switch(u_xlati4){
        case 0:
            u_xlat12.x = u_xlat16_9 * u_xlat3.x;
            break;
        case 1:
            u_xlat12.x = u_xlat16_9;
            break;
        case 2:
            u_xlat21 = min(_ScreenParams.y, _ScreenParams.x);
            u_xlat30 = u_xlat16_9 * 0.00999999978;
            u_xlat12.x = u_xlat30 * u_xlat21;
            break;
        default:
            u_xlat12.x = 0.0;
            break;
    }
    u_xlat16_9 = max(u_xlat12.x, 1.0);
    u_xlat16_9 = u_xlat16_9 / u_xlat3.x;
    vs_TEXCOORD3.y = u_xlat12.x;
#ifdef UNITY_ADRENO_ES3
    vs_TEXCOORD3.y = min(max(vs_TEXCOORD3.y, 0.0), 1.0);
#else
    vs_TEXCOORD3.y = clamp(vs_TEXCOORD3.y, 0.0, 1.0);
#endif
    vs_TEXCOORD3.x = u_xlat16_9 / u_xlat16_0.x;
    u_xlat16_0 = u_xlat16_2.xyxy * _Rect;
    u_xlat16_1.xy = u_xlat16_1.xy * u_xlat3.xx;
    u_xlat16_1.xy = vec2(2.0, 2.0) / u_xlat16_1.xy;
    u_xlat16_19.xy = u_xlat16_0.zw * vec2(-0.5, -0.5) + (-u_xlat16_1.xy);
    u_xlat16_20.xy = u_xlat16_0.zw * vec2(0.5, 0.5) + u_xlat16_1.xy;
    u_xlat12.xy = in_POSITION0.xy + vec2(1.0, 1.0);
    u_xlat12.xy = u_xlat12.xy * vec2(0.5, 0.5);
    u_xlat4.xy = (-u_xlat16_19.xy) + u_xlat16_20.xy;
    u_xlat4.xy = u_xlat12.xy * u_xlat4.xy + u_xlat16_19.xy;
    u_xlat16_19.xy = _Rect.xy * u_xlat16_2.xy + (-u_xlat16_1.xy);
    u_xlat16_20.xy = u_xlat16_0.zw + u_xlat16_0.xy;
    u_xlat16_1.xy = u_xlat16_1.xy + u_xlat16_20.xy;
    u_xlat5.xy = (-u_xlat16_19.xy) + u_xlat16_1.xy;
    u_xlat12.xy = u_xlat12.xy * u_xlat5.xy + u_xlat16_19.xy;
    u_xlat5.xy = u_xlat12.xy / u_xlat16_2.xy;
    u_xlat1 = u_xlat5.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat1 = hlslcc_mtx4x4unity_ObjectToWorld[0] * u_xlat5.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat1;
    u_xlat1 = u_xlat1 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat2 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat2 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat2;
    u_xlat2 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat2;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat2;
#ifdef UNITY_ADRENO_ES3
    u_xlatb12 = !!(_FillType!=int(0xFFFFFFFFu));
#else
    u_xlatb12 = _FillType!=int(0xFFFFFFFFu);
#endif
    u_xlat5.z = in_POSITION0.z;
    u_xlat5.xyz = (int(_FillSpace) != 0) ? u_xlat1.xyz : u_xlat5.xyz;
    u_xlat5.xyz = u_xlat5.xyz + (-_FillStart.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlatb21 = !!(_FillType==1);
#else
    u_xlatb21 = _FillType==1;
#endif
    u_xlat6.xyz = (-_FillStart.xyz) + _FillEnd.xyz;
    u_xlat30 = dot(u_xlat6.xyz, u_xlat5.xyz);
    u_xlat16_7 = dot(u_xlat6.xyz, u_xlat6.xyz);
    u_xlat6.x = u_xlat30 / u_xlat16_7;
    u_xlat6.y = float(0.0);
    u_xlat6.z = float(0.0);
    u_xlat5.xyz = (bool(u_xlatb21)) ? u_xlat5.xyz : u_xlat6.xyz;
    vs_TEXCOORD0.xyz = bool(u_xlatb12) ? u_xlat5.xyz : vec3(0.0, 0.0, 0.0);
    u_xlat4.zw = in_POSITION0.xy;
    vs_TEXCOORD1 = u_xlat4;
    vs_TEXCOORD2 = u_xlat16_0;
    vs_TEXCOORD3.z = u_xlat3.x;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp float;
precision highp int;
#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
vec4 ImmCB_0[4];
uniform 	vec4 _ScreenParams;
uniform 	mediump vec4 _Color;
uniform 	mediump float _ThicknessSpace;
uniform 	int _DashType;
uniform 	mediump float _DashSize;
uniform 	mediump float _DashShapeModifier;
uniform 	mediump float _DashOffset;
uniform 	mediump float _DashSpacing;
uniform 	int _DashSpace;
uniform 	int _DashSnap;
uniform 	mediump vec4 _ColorEnd;
uniform 	int _FillType;
uniform 	vec4 _FillStart;
in highp vec3 vs_TEXCOORD0;
in mediump vec4 vs_TEXCOORD1;
in mediump vec4 vs_TEXCOORD2;
in mediump vec3 vs_TEXCOORD3;
layout(location = 0) out mediump vec4 SV_Target0;
vec2 u_xlat0;
mediump vec4 u_xlat16_0;
int u_xlati0;
bvec4 u_xlatb0;
mediump vec3 u_xlat16_1;
vec2 u_xlat2;
ivec2 u_xlati2;
uint u_xlatu2;
bool u_xlatb2;
vec2 u_xlat3;
int u_xlati3;
vec3 u_xlat4;
bvec3 u_xlatb4;
float u_xlat5;
mediump vec2 u_xlat16_5;
float u_xlat6;
float u_xlat7;
mediump vec2 u_xlat16_7;
vec2 u_xlat8;
bvec2 u_xlatb8;
float u_xlat11;
vec2 u_xlat12;
int u_xlati12;
bool u_xlatb12;
mediump float u_xlat16_13;
ivec2 u_xlati14;
float u_xlat15;
float u_xlat18;
mediump float u_xlat16_19;
float u_xlat20;
int u_xlati20;
bool u_xlatb20;
float u_xlat21;
void main()
{
ImmCB_0[0] = vec4(1.0,0.0,0.0,0.0);
ImmCB_0[1] = vec4(0.0,1.0,0.0,0.0);
ImmCB_0[2] = vec4(0.0,0.0,1.0,0.0);
ImmCB_0[3] = vec4(0.0,0.0,0.0,1.0);
    u_xlat0.xy = (-vs_TEXCOORD2.zw) * vec2(0.5, 0.5) + abs(vs_TEXCOORD1.xy);
    u_xlat12.xy = max(u_xlat0.xy, vec2(0.0, 0.0));
    u_xlat12.x = dot(u_xlat12.xy, u_xlat12.xy);
    u_xlat12.x = sqrt(u_xlat12.x);
    u_xlat0.x = max(u_xlat0.y, u_xlat0.x);
    u_xlat0.x = min(u_xlat0.x, 0.0);
    u_xlat0.x = u_xlat0.x + u_xlat12.x;
    u_xlat16_1.x = vs_TEXCOORD3.x * 0.5 + u_xlat0.x;
    u_xlat16_1.x = (-vs_TEXCOORD3.x) * 0.5 + abs(u_xlat16_1.x);
    u_xlat2.x = dFdx(u_xlat16_1.x);
    u_xlat2.y = dFdy(u_xlat16_1.x);
    u_xlat6 = dot(u_xlat2.xy, u_xlat2.xy);
    u_xlat6 = sqrt(u_xlat6);
    u_xlat6 = max(u_xlat6, 9.99999975e-06);
    u_xlat6 = u_xlat16_1.x / u_xlat6;
    u_xlat6 = u_xlat6 + 0.5;
#ifdef UNITY_ADRENO_ES3
    u_xlat6 = min(max(u_xlat6, 0.0), 1.0);
#else
    u_xlat6 = clamp(u_xlat6, 0.0, 1.0);
#endif
    u_xlat6 = (-u_xlat6) + 1.0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb12 = !!(6.10351563e-05<_DashSize);
#else
    u_xlatb12 = 6.10351563e-05<_DashSize;
#endif
    if(u_xlatb12){
        u_xlat16_1.x = u_xlat0.x / vs_TEXCOORD3.x;
        u_xlat16_1.x = u_xlat16_1.x + 1.0;
        u_xlatb0.xzw = equal(ivec4(_DashSnap, _DashSnap, _DashSpace, _DashSpace), ivec4(2, 0, int(0xFFFFFFFFu), int(0xFFFFFFFEu))).xzw;
        u_xlati2.xy = ivec2(uvec2(lessThan(vec4(0.0, 0.0, 0.0, 0.0), vs_TEXCOORD1.yxyy).xy) * 0xFFFFFFFFu);
        u_xlati14.xy = ivec2(uvec2(lessThan(vs_TEXCOORD1.yxyx, vec4(0.0, 0.0, 0.0, 0.0)).xy) * 0xFFFFFFFFu);
        u_xlati2.xy = (-u_xlati2.xy) + u_xlati14.xy;
        u_xlat2.xy = vec2(u_xlati2.xy);
        u_xlat8.x = (-u_xlat2.y) * u_xlat2.x + 3.0;
        u_xlat2.x = u_xlat8.x * 0.5 + (-u_xlat2.x);
        u_xlatu2 =  uint(int(u_xlat2.x));
        u_xlat8.x = vs_TEXCOORD1.y * vs_TEXCOORD1.x;
#ifdef UNITY_ADRENO_ES3
        u_xlatb8.x = !!(u_xlat8.x<0.0);
#else
        u_xlatb8.x = u_xlat8.x<0.0;
#endif
        u_xlat8.xy = (u_xlatb8.x) ? vs_TEXCOORD1.yx : vs_TEXCOORD1.xy;
        u_xlati20 = int(uint(u_xlatu2 & 1u));
#ifdef UNITY_ADRENO_ES3
        u_xlatb20 = !!(u_xlati20==1);
#else
        u_xlatb20 = u_xlati20==1;
#endif
        u_xlat16_7.xy = (bool(u_xlatb20)) ? vs_TEXCOORD2.wz : vs_TEXCOORD2.zw;
        u_xlat3.xy = (-u_xlat16_7.xy) * vec2(0.5, 0.5) + abs(u_xlat8.xy);
        u_xlat20 = (-u_xlat3.y) + u_xlat3.x;
#ifdef UNITY_ADRENO_ES3
        u_xlatb20 = !!(0.0<u_xlat20);
#else
        u_xlatb20 = 0.0<u_xlat20;
#endif
        if(u_xlatb0.x){
            if(u_xlatb20){
                u_xlati3 = int(u_xlatu2) + 3;
                u_xlati3 = int(uint(uint(u_xlati3) & 3u));
                u_xlat3.y = dot(vs_TEXCOORD2.zwzw, ImmCB_0[u_xlati3]);
                u_xlat3.x = u_xlat16_7.y * 0.5 + abs(u_xlat8.y);
            } else {
                u_xlat3.y = dot(vs_TEXCOORD2.zwzw, ImmCB_0[int(u_xlatu2)]);
                u_xlat3.x = u_xlat16_7.x * 0.5 + -abs(u_xlat8.x);
            }
        } else {
            u_xlat15 = u_xlat16_7.y + u_xlat16_7.x;
            u_xlat21 = u_xlat15 * 0.5;
            u_xlat3.y = u_xlat15 + u_xlat15;
            u_xlatb4.xyz = greaterThanEqual(uvec4(u_xlatu2), uvec4(1u, 2u, 3u, uint(0u))).xyz;
            u_xlat4.x = u_xlatb4.x ? float(1.0) : 0.0;
            u_xlat4.y = u_xlatb4.y ? float(1.0) : 0.0;
            u_xlat4.z = u_xlatb4.z ? float(1.0) : 0.0;
;
            u_xlat15 = dot(u_xlat4.xyz, vec3(u_xlat21));
            if(u_xlatb20){
                u_xlat3.x = abs(u_xlat8.y) + u_xlat15;
            } else {
                u_xlat2.x = dot(vec4(u_xlat21), ImmCB_0[int(u_xlatu2)]);
                u_xlat2.x = -abs(u_xlat8.x) + u_xlat2.x;
                u_xlat3.x = u_xlat2.x + u_xlat15;
            }
        }
        u_xlati0 = (u_xlatb0.x) ? 3 : _DashSnap;
        if(u_xlatb0.z){
            u_xlati12 = int(_ThicknessSpace);
            switch(u_xlati12){
                case 0:
                    u_xlat16_5.x = u_xlat3.x;
                    break;
                case 1:
                    u_xlat16_5.x = u_xlat3.x * vs_TEXCOORD3.z;
                    break;
                case 2:
                    u_xlat16_7.x = u_xlat3.x * vs_TEXCOORD3.z;
                    u_xlat2.x = u_xlat16_7.x * 100.0;
                    u_xlat8.x = min(_ScreenParams.y, _ScreenParams.x);
                    u_xlat5 = u_xlat2.x / u_xlat8.x;
                    u_xlat16_5.x = u_xlat5;
                    break;
                default:
                    u_xlat16_5.x = 0.0;
                    break;
            }
            switch(u_xlati12){
                case 0:
                    u_xlat16_5.y = u_xlat3.y;
                    break;
                case 1:
                    u_xlat16_5.y = u_xlat3.y * vs_TEXCOORD3.z;
                    break;
                case 2:
                    u_xlat16_7.x = u_xlat3.y * vs_TEXCOORD3.z;
                    u_xlat2.x = u_xlat16_7.x * 100.0;
                    u_xlat8.x = min(_ScreenParams.y, _ScreenParams.x);
                    u_xlat11 = u_xlat2.x / u_xlat8.x;
                    u_xlat16_5.y = u_xlat11;
                    break;
                default:
                    u_xlat16_5.y = 0.0;
                    break;
            }
            switch(u_xlati12){
                case 0:
                    u_xlat16_7.x = vs_TEXCOORD3.x;
                    break;
                case 1:
                    u_xlat16_7.x = vs_TEXCOORD3.z * vs_TEXCOORD3.x;
                    break;
                case 2:
                    u_xlat16_13 = vs_TEXCOORD3.z * vs_TEXCOORD3.x;
                    u_xlat12.x = u_xlat16_13 * 100.0;
                    u_xlat2.x = min(_ScreenParams.y, _ScreenParams.x);
                    u_xlat7 = u_xlat12.x / u_xlat2.x;
                    u_xlat16_7.x = u_xlat7;
                    break;
                default:
                    u_xlat16_7.x = 0.0;
                    break;
            }
            u_xlat3.xy = u_xlat16_5.xy;
        } else {
            u_xlat16_7.x = vs_TEXCOORD3.x;
        }
        if(u_xlatb0.w){
            switch(u_xlati0){
                case 0:
                    u_xlat16_13 = _DashSize;
                    break;
                case 1:
                case 3:
                    u_xlat16_19 = roundEven(_DashSize);
                    u_xlat16_13 = max(u_xlat16_19, 1.0);
                    break;
                case 2:
                    u_xlat16_19 = _DashSpacing + _DashSize;
                    u_xlat16_19 = roundEven(u_xlat16_19);
                    u_xlat16_19 = max(u_xlat16_19, 1.0);
                    u_xlat16_13 = u_xlat16_19 + (-_DashSpacing);
                    break;
                default:
                    u_xlat16_13 = 0.0;
                    break;
            }
            u_xlat12.x = u_xlat16_13;
            u_xlat18 = _DashSpacing;
        } else {
            u_xlat16_13 = _DashSpacing + _DashSize;
            u_xlat18 = _DashSpacing / u_xlat16_13;
            u_xlat2.x = u_xlat3.y / u_xlat16_13;
            switch(u_xlati0){
                case 0:
                    u_xlat16_13 = u_xlat2.x;
                    break;
                case 1:
                case 3:
                    u_xlat16_19 = roundEven(u_xlat2.x);
                    u_xlat16_13 = max(u_xlat16_19, 1.0);
                    break;
                case 2:
                    u_xlat16_19 = u_xlat18 + u_xlat2.x;
                    u_xlat16_19 = roundEven(u_xlat16_19);
                    u_xlat16_19 = max(u_xlat16_19, 1.0);
                    u_xlat16_13 = (-u_xlat18) + u_xlat16_19;
                    break;
                default:
                    u_xlat16_13 = 0.0;
                    break;
            }
            u_xlat12.x = u_xlat16_13;
        }
        u_xlat2.x = (-u_xlat18) + 1.0;
#ifdef UNITY_ADRENO_ES3
        u_xlatb0.x = !!(u_xlati0==3);
#else
        u_xlatb0.x = u_xlati0==3;
#endif
        u_xlat8.x = (-u_xlat2.x) * 0.5 + _DashOffset;
        u_xlat16_13 = (u_xlatb0.x) ? u_xlat8.x : _DashOffset;
        u_xlat0.x = u_xlat12.x * u_xlat16_7.x;
        u_xlat0.x = u_xlat0.x / u_xlat3.y;
        u_xlat8.x = u_xlat3.x / u_xlat3.y;
        u_xlat12.x = u_xlat8.x * u_xlat12.x + (-u_xlat16_13);
        u_xlat12.x = (-u_xlat2.x) * 0.5 + u_xlat12.x;
        u_xlat16_1.y = u_xlat16_1.x * 2.0 + -1.0;
#ifdef UNITY_ADRENO_ES3
        u_xlatb2 = !!(u_xlat18>=0.999938965);
#else
        u_xlatb2 = u_xlat18>=0.999938965;
#endif
        u_xlatb8.xy = equal(ivec4(_DashType), ivec4(1, 2, 0, 0)).xy;
        u_xlat16_13 = u_xlat16_1.y * 0.5;
        u_xlat16_13 = u_xlat0.x * u_xlat16_13;
        u_xlat16_13 = u_xlat16_13 * _DashShapeModifier + u_xlat12.x;
        u_xlat16_13 = (u_xlatb8.x) ? u_xlat16_13 : u_xlat12.x;
        u_xlat16_13 = fract(u_xlat16_13);
        u_xlat16_13 = u_xlat16_13 * 2.0 + -1.0;
        u_xlat16_13 = (-u_xlat18) + abs(u_xlat16_13);
        u_xlat16_19 = (-u_xlat18) + 1.0;
        u_xlat16_13 = u_xlat16_13 / u_xlat16_19;
        u_xlat16_19 = u_xlat16_19 / u_xlat0.x;
        u_xlat16_1.x = (-u_xlat16_13) * u_xlat16_19 + 1.0;
        u_xlat16_1.x = dot(u_xlat16_1.xy, u_xlat16_1.xy);
        u_xlat16_1.x = sqrt(u_xlat16_1.x);
        u_xlat16_1.x = u_xlat16_1.x + -1.0;
        u_xlat16_7.x = dFdx(u_xlat16_1.x);
        u_xlat16_5.x = dFdy(u_xlat16_1.x);
        u_xlat16_7.x = abs(u_xlat16_7.x) + abs(u_xlat16_5.x);
        u_xlat16_1.x = u_xlat16_1.x / u_xlat16_7.x;
        u_xlat16_1.x = u_xlat16_1.x + 0.5;
#ifdef UNITY_ADRENO_ES3
        u_xlat16_1.x = min(max(u_xlat16_1.x, 0.0), 1.0);
#else
        u_xlat16_1.x = clamp(u_xlat16_1.x, 0.0, 1.0);
#endif
        u_xlat16_1.x = (-u_xlat16_1.x) + 1.0;
        u_xlat16_7.x = float(1.0) / u_xlat16_19;
        u_xlat16_7.x = (-u_xlat16_7.x) + u_xlat16_13;
        u_xlat16_19 = dFdx(u_xlat16_13);
        u_xlat16_5.x = dFdy(u_xlat16_13);
        u_xlat16_19 = abs(u_xlat16_19) + abs(u_xlat16_5.x);
        u_xlat16_7.x = u_xlat16_7.x / u_xlat16_19;
        u_xlat16_7.x = u_xlat16_7.x + 0.5;
#ifdef UNITY_ADRENO_ES3
        u_xlat16_7.x = min(max(u_xlat16_7.x, 0.0), 1.0);
#else
        u_xlat16_7.x = clamp(u_xlat16_7.x, 0.0, 1.0);
#endif
        u_xlat16_1.x = max(u_xlat16_1.x, u_xlat16_7.x);
        u_xlat16_7.x = u_xlat16_13 / u_xlat16_19;
        u_xlat16_7.x = u_xlat16_7.x + 0.5;
#ifdef UNITY_ADRENO_ES3
        u_xlat16_7.x = min(max(u_xlat16_7.x, 0.0), 1.0);
#else
        u_xlat16_7.x = clamp(u_xlat16_7.x, 0.0, 1.0);
#endif
        u_xlat16_1.x = (u_xlatb8.y) ? u_xlat16_1.x : u_xlat16_7.x;
        u_xlat16_1.x = min(u_xlat6, u_xlat16_1.x);
        u_xlat16_1.x = (u_xlatb2) ? 0.0 : u_xlat16_1.x;
    } else {
        u_xlat16_1.x = u_xlat6;
    }
    u_xlat16_1.x = u_xlat16_1.x * vs_TEXCOORD3.y;
#ifdef UNITY_ADRENO_ES3
    u_xlatb0.x = !!(_FillType!=int(0xFFFFFFFFu));
#else
    u_xlatb0.x = _FillType!=int(0xFFFFFFFFu);
#endif
    if(u_xlatb0.x){
        switch(_FillType){
            case 0:
                u_xlat7 = vs_TEXCOORD0.x;
#ifdef UNITY_ADRENO_ES3
                u_xlat7 = min(max(u_xlat7, 0.0), 1.0);
#else
                u_xlat7 = clamp(u_xlat7, 0.0, 1.0);
#endif
                u_xlat16_7.x = u_xlat7;
                break;
            case 1:
                u_xlat0.x = dot(vs_TEXCOORD0.xyz, vs_TEXCOORD0.xyz);
                u_xlat0.x = sqrt(u_xlat0.x);
                u_xlat7 = u_xlat0.x / _FillStart.w;
#ifdef UNITY_ADRENO_ES3
                u_xlat7 = min(max(u_xlat7, 0.0), 1.0);
#else
                u_xlat7 = clamp(u_xlat7, 0.0, 1.0);
#endif
                u_xlat16_7.x = u_xlat7;
                break;
            default:
                u_xlat16_7.x = 0.0;
                break;
        }
        u_xlat16_0 = (-_Color) + _ColorEnd;
        u_xlat16_0 = u_xlat16_7.xxxx * u_xlat16_0 + _Color;
    } else {
        u_xlat16_0 = _Color;
    }
    u_xlat2.x = u_xlat16_0.w * u_xlat16_1.x;
    u_xlat16_1.x = u_xlat16_1.x * u_xlat16_0.w + -6.10351563e-05;
#ifdef UNITY_ADRENO_ES3
    u_xlatb8.x = !!(u_xlat16_1.x<0.0);
#else
    u_xlatb8.x = u_xlat16_1.x<0.0;
#endif
    if(u_xlatb8.x){discard;}
    u_xlat16_1.xyz = (-u_xlat16_0.xyz) * u_xlat2.xxx + vec3(1.0, 1.0, 1.0);
    u_xlat16_1.xyz = max(u_xlat16_1.xyz, vec3(6.10351563e-05, 6.10351563e-05, 6.10351563e-05));
    SV_Target0.xyz = vec3(1.0, 1.0, 1.0) / u_xlat16_1.xyz;
    SV_Target0.w = u_xlat2.x;
    return;
}

#endif
                               $Globalsp         _ScreenParams                            _Color                          _ThicknessSpace                       	   _DashType                    $   	   _DashSize                     (      _DashShapeModifier                    ,      _DashOffset                   0      _DashSpacing                  4   
   _DashSpace                   8   	   _DashSnap                    <   	   _ColorEnd                     @   	   _FillType                    P   
   _FillStart                    `          $Globals        _ScreenParams                         
   _ScaleMode                   �      _Rect                     �   
   _Thickness                    �      _ThicknessSpace                   �   	   _FillType                    �   
   _FillSpace                   �   
   _FillStart                          _FillEnd                       unity_ObjectToWorld                        unity_MatrixInvV                 P      unity_MatrixVP                   �               