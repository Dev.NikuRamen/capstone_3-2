`  <Q                         INSTANCING_ON      STEREO_MULTIVIEW_ON     X  #ifdef VERTEX
#version 300 es
#extension GL_OVR_multiview2 : require
#ifndef UNITY_RUNTIME_INSTANCING_ARRAY_SIZE
	#define UNITY_RUNTIME_INSTANCING_ARRAY_SIZE 2
#endif

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 _ScreenParams;
uniform 	vec4 unity_OrthoParams;
uniform 	int unity_BaseInstanceID;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(1) uniform UnityStereoGlobals {
#endif
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_StereoMatrixP[8];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_StereoMatrixV[8];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_StereoMatrixInvV[8];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_StereoMatrixVP[8];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_StereoCameraProjection[8];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_StereoCameraInvProjection[8];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_StereoWorldToCamera[8];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_StereoCameraToWorld[8];
	UNITY_UNIFORM vec3 unity_StereoWorldSpaceCameraPos[2];
	UNITY_UNIFORM vec4 unity_StereoScaleOffset[2];
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(2) uniform UnityStereoEyeIndices {
#endif
	UNITY_UNIFORM vec4 unity_StereoEyeIndices[2];
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
layout(num_views = 2) in;
struct unity_Builtins0Array_Type {
	vec4 hlslcc_mtx4x4unity_ObjectToWorldArray[4];
	vec4 hlslcc_mtx4x4unity_WorldToObjectArray[4];
};
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(3) uniform UnityInstancing_PerDraw0 {
#endif
	UNITY_UNIFORM unity_Builtins0Array_Type unity_Builtins0Array[UNITY_RUNTIME_INSTANCING_ARRAY_SIZE];
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
struct PropsArray_Type {
	int _ScaleMode;
	mediump vec4 _Color;
	mediump vec4 _ColorEnd;
	vec3 _PointStart;
	vec3 _PointEnd;
	mediump float _Thickness;
	int _ThicknessSpace;
	int _Alignment;
	int _DashType;
	mediump float _DashSize;
	mediump float _DashShapeModifier;
	mediump float _DashOffset;
	mediump float _DashSpacing;
	int _DashSpace;
	int _DashSnap;
};
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(0) uniform UnityInstancing_Props {
#endif
	UNITY_UNIFORM PropsArray_Type PropsArray[UNITY_RUNTIME_INSTANCING_ARRAY_SIZE];
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
in highp vec4 in_POSITION0;
out mediump vec4 vs_TEXCOORD0;
out mediump vec4 vs_TEXCOORD1;
flat out highp uint vs_SV_InstanceID0;
out highp float vs_BLENDWEIGHT0;
int u_xlati0;
uint u_xlatu0;
vec4 u_xlat1;
bvec3 u_xlatb1;
vec3 u_xlat2;
vec4 u_xlat3;
vec2 u_xlat4;
float u_xlat5;
mediump vec3 u_xlat16_5;
vec3 u_xlat6;
mediump vec3 u_xlat16_6;
vec4 u_xlat7;
mediump vec3 u_xlat16_7;
vec3 u_xlat8;
vec3 u_xlat9;
float u_xlat10;
int u_xlati10;
float u_xlat11;
bool u_xlatb11;
mediump vec2 u_xlat16_15;
float u_xlat16;
mediump float u_xlat16_16;
vec2 u_xlat20;
int u_xlati20;
uint u_xlatu20;
bool u_xlatb20;
mediump float u_xlat16_25;
mediump float u_xlat16_26;
float u_xlat30;
float u_xlat31;
bool u_xlatb31;
float u_xlat32;
mediump float u_xlat16_35;
void main()
{
    u_xlati0 = gl_InstanceID + unity_BaseInstanceID;
    u_xlati10 = u_xlati0 * 9;
    u_xlat20.x = float(PropsArray[u_xlati10 / 9]._Alignment);
    u_xlat20.x = u_xlat20.x;
#ifdef UNITY_ADRENO_ES3
    u_xlat20.x = min(max(u_xlat20.x, 0.0), 1.0);
#else
    u_xlat20.x = clamp(u_xlat20.x, 0.0, 1.0);
#endif
    u_xlat30 = u_xlat20.x * PropsArray[u_xlati10 / 9]._PointStart.z;
    u_xlat20.x = u_xlat20.x * PropsArray[u_xlati10 / 9]._PointEnd.z;
    u_xlati0 = int(u_xlati0 << 3);
    u_xlat1.xyz = unity_Builtins0Array[u_xlati0 / 8].hlslcc_mtx4x4unity_ObjectToWorldArray[1].xyz * PropsArray[u_xlati10 / 9]._PointStart.yyy;
    u_xlat1.xyz = unity_Builtins0Array[u_xlati0 / 8].hlslcc_mtx4x4unity_ObjectToWorldArray[0].xyz * PropsArray[u_xlati10 / 9]._PointStart.xxx + u_xlat1.xyz;
    u_xlat1.xyz = unity_Builtins0Array[u_xlati0 / 8].hlslcc_mtx4x4unity_ObjectToWorldArray[2].xyz * vec3(u_xlat30) + u_xlat1.xyz;
    u_xlat1.xyz = u_xlat1.xyz + unity_Builtins0Array[u_xlati0 / 8].hlslcc_mtx4x4unity_ObjectToWorldArray[3].xyz;
    u_xlat2.xyz = unity_Builtins0Array[u_xlati0 / 8].hlslcc_mtx4x4unity_ObjectToWorldArray[1].xyz * PropsArray[u_xlati10 / 9]._PointEnd.yyy;
    u_xlat2.xyz = unity_Builtins0Array[u_xlati0 / 8].hlslcc_mtx4x4unity_ObjectToWorldArray[0].xyz * PropsArray[u_xlati10 / 9]._PointEnd.xxx + u_xlat2.xyz;
    u_xlat2.xyz = unity_Builtins0Array[u_xlati0 / 8].hlslcc_mtx4x4unity_ObjectToWorldArray[2].xyz * u_xlat20.xxx + u_xlat2.xyz;
    u_xlat2.xyz = u_xlat2.xyz + unity_Builtins0Array[u_xlati0 / 8].hlslcc_mtx4x4unity_ObjectToWorldArray[3].xyz;
#ifdef UNITY_ADRENO_ES3
    u_xlatb20 = !!(in_POSITION0.y<0.0);
#else
    u_xlatb20 = in_POSITION0.y<0.0;
#endif
    u_xlat3.xyz = (bool(u_xlatb20)) ? u_xlat1.xyz : u_xlat2.xyz;
    u_xlat2.xyz = (-u_xlat1.xyz) + u_xlat2.xyz;
    u_xlat20.x = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat4.y = sqrt(u_xlat20.x);
    u_xlat2.xyz = u_xlat2.xyz / u_xlat4.yyy;
    switch(PropsArray[u_xlati10 / 9]._Alignment){
        case 0:
            u_xlat16_35 = dot(unity_Builtins0Array[u_xlati0 / 8].hlslcc_mtx4x4unity_ObjectToWorldArray[2].xyz, unity_Builtins0Array[u_xlati0 / 8].hlslcc_mtx4x4unity_ObjectToWorldArray[2].xyz);
            u_xlat16_35 = inversesqrt(u_xlat16_35);
            u_xlat16_6.xyz = vec3(u_xlat16_35) * unity_Builtins0Array[u_xlati0 / 8].hlslcc_mtx4x4unity_ObjectToWorldArray[2].yzx;
            u_xlat16_7.xyz = u_xlat2.zxy * u_xlat16_6.xyz;
            u_xlat16_5.xyz = u_xlat2.yzx * u_xlat16_6.yzx + (-u_xlat16_7.xyz);
            break;
        case 1:
#ifdef UNITY_ADRENO_ES3
            u_xlatb20 = !!(unity_OrthoParams.w==1.0);
#else
            u_xlatb20 = unity_OrthoParams.w==1.0;
#endif
            if(u_xlatb20){
                u_xlati20 = int(int(gl_ViewID_OVR) << 2);
                u_xlat16_6.xyz = (-hlslcc_mtx4x4unity_StereoMatrixInvV[(u_xlati20 + 2)].xyz);
            } else {
                u_xlatu20 = gl_ViewID_OVR;
                u_xlat8.xyz = (-u_xlat3.xyz) + unity_StereoWorldSpaceCameraPos[int(u_xlatu20)].xyz;
                u_xlat20.x = dot(u_xlat8.xyz, u_xlat8.xyz);
                u_xlat20.x = inversesqrt(u_xlat20.x);
                u_xlat6.xyz = u_xlat20.xxx * u_xlat8.xyz;
                u_xlat16_6.xyz = u_xlat6.xyz;
            }
            u_xlat16_7.xyz = u_xlat2.zxy * (-u_xlat16_6.yzx);
            u_xlat16_6.xyz = u_xlat2.yzx * (-u_xlat16_6.zxy) + (-u_xlat16_7.xyz);
            u_xlat16_35 = dot(u_xlat16_6.xyz, u_xlat16_6.xyz);
            u_xlat16_35 = inversesqrt(u_xlat16_35);
            u_xlat16_5.xyz = vec3(u_xlat16_35) * u_xlat16_6.xyz;
            break;
        default:
            u_xlat16_5.x = float(0.0);
            u_xlat16_5.y = float(0.0);
            u_xlat16_5.z = float(0.0);
            break;
    }
    u_xlat16_35 = dot(unity_Builtins0Array[u_xlati0 / 8].hlslcc_mtx4x4unity_ObjectToWorldArray[0].xyz, unity_Builtins0Array[u_xlati0 / 8].hlslcc_mtx4x4unity_ObjectToWorldArray[0].xyz);
    u_xlat16_35 = sqrt(u_xlat16_35);
    u_xlat16_6.x = dot(unity_Builtins0Array[u_xlati0 / 8].hlslcc_mtx4x4unity_ObjectToWorldArray[1].xyz, unity_Builtins0Array[u_xlati0 / 8].hlslcc_mtx4x4unity_ObjectToWorldArray[1].xyz);
    u_xlat16_6.y = dot(unity_Builtins0Array[u_xlati0 / 8].hlslcc_mtx4x4unity_ObjectToWorldArray[2].xyz, unity_Builtins0Array[u_xlati0 / 8].hlslcc_mtx4x4unity_ObjectToWorldArray[2].xyz);
    u_xlat16_6.xy = sqrt(u_xlat16_6.xy);
    u_xlat16_35 = u_xlat16_35 + u_xlat16_6.x;
    u_xlat16_35 = u_xlat16_6.y + u_xlat16_35;
    u_xlat16_35 = u_xlat16_35 * 0.333333343;
    u_xlat16_6.x = (PropsArray[u_xlati10 / 9]._ScaleMode != 0) ? 1.0 : u_xlat16_35;
    u_xlat16_6.x = u_xlat16_6.x * PropsArray[u_xlati10 / 9]._Thickness;
    u_xlati0 = int(int(gl_ViewID_OVR) << 2);
    u_xlat7.x = hlslcc_mtx4x4unity_StereoMatrixVP[u_xlati0].w;
    u_xlat7.y = hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati0 + 1)].w;
    u_xlat7.z = hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati0 + 2)].w;
    u_xlat7.w = hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati0 + 3)].w;
    u_xlat3.w = 1.0;
    u_xlat20.x = dot(u_xlat7, u_xlat3);
    u_xlat8.x = hlslcc_mtx4x4unity_StereoMatrixVP[u_xlati0].x;
    u_xlat8.y = hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati0 + 1)].x;
    u_xlat8.z = hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati0 + 2)].x;
    u_xlat30 = dot(u_xlat8.xyz, u_xlat16_5.xyz);
    u_xlat8.x = u_xlat30 / u_xlat20.x;
    u_xlat9.x = hlslcc_mtx4x4unity_StereoMatrixVP[u_xlati0].y;
    u_xlat9.y = hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati0 + 1)].y;
    u_xlat9.z = hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati0 + 2)].y;
    u_xlat30 = dot(u_xlat9.xyz, u_xlat16_5.xyz);
    u_xlat8.y = u_xlat30 / u_xlat20.x;
    u_xlat20.xy = u_xlat8.xy * _ScreenParams.xy;
    u_xlat20.x = dot(u_xlat20.xy, u_xlat20.xy);
    u_xlat20.x = sqrt(u_xlat20.x);
    u_xlat20.x = u_xlat20.x * 0.5;
    switch(PropsArray[u_xlati10 / 9]._ThicknessSpace){
        case 0:
            u_xlat30 = u_xlat20.x * u_xlat16_6.x;
            break;
        case 1:
            u_xlat30 = u_xlat16_6.x;
            break;
        case 2:
            u_xlat31 = min(_ScreenParams.y, _ScreenParams.x);
            u_xlat32 = u_xlat16_6.x * 0.00999999978;
            u_xlat30 = u_xlat31 * u_xlat32;
            break;
        default:
            u_xlat30 = 0.0;
            break;
    }
    u_xlat16_6.x = u_xlat30 + 2.0;
    u_xlat16_6.x = max(u_xlat16_6.x, 1.0);
    u_xlat16_16 = max(u_xlat30, 6.10351563e-05);
    u_xlat16_16 = u_xlat16_6.x / u_xlat16_16;
    u_xlat16_6.x = u_xlat16_6.x / u_xlat20.x;
    u_xlat16_26 = 2.0 / u_xlat20.x;
    vs_TEXCOORD1.x = u_xlat16_16 * in_POSITION0.x;
    u_xlat16_16 = u_xlat4.y + u_xlat16_26;
    u_xlat16_16 = u_xlat16_16 / u_xlat4.y;
    vs_TEXCOORD1.y = u_xlat16_16 * in_POSITION0.y;
    u_xlat16_6.x = u_xlat16_6.x * 0.5;
    u_xlat31 = u_xlat16_6.x * in_POSITION0.x;
    u_xlat3.xyz = u_xlat16_5.xyz * vec3(u_xlat31) + u_xlat3.xyz;
    u_xlat31 = u_xlat16_26 * in_POSITION0.y;
    u_xlat31 = u_xlat31 * 0.5;
    u_xlat3.xyz = u_xlat2.xyz * vec3(u_xlat31) + u_xlat3.xyz;
    u_xlat16_5.x = u_xlat30 * 0.5;
    u_xlat16_5.x = u_xlat16_5.x / u_xlat20.x;
    u_xlat16_5.x = u_xlat16_5.x + u_xlat16_5.x;
    vs_TEXCOORD1.w = u_xlat16_5.x / u_xlat4.y;
#ifdef UNITY_ADRENO_ES3
    u_xlatb31 = !!(6.10351563e-05<PropsArray[u_xlati10 / 9]._DashSize);
#else
    u_xlatb31 = 6.10351563e-05<PropsArray[u_xlati10 / 9]._DashSize;
#endif
    if(u_xlatb31){
        u_xlat1.xyz = (-u_xlat1.xyz) + u_xlat3.xyz;
        u_xlat4.x = dot(u_xlat2.xyz, u_xlat1.xyz);
#ifdef UNITY_ADRENO_ES3
        u_xlatb1.x = !!(0==PropsArray[u_xlati10 / 9]._ScaleMode);
#else
        u_xlatb1.x = 0==PropsArray[u_xlati10 / 9]._ScaleMode;
#endif
#ifdef UNITY_ADRENO_ES3
        u_xlatb11 = !!(int(0xFFFFFFFEu)!=PropsArray[u_xlati10 / 9]._DashSpace);
#else
        u_xlatb11 = int(0xFFFFFFFEu)!=PropsArray[u_xlati10 / 9]._DashSpace;
#endif
        u_xlatb1.x = u_xlatb11 && u_xlatb1.x;
        u_xlat16_15.xy = vec2(u_xlat16_35) * vec2(PropsArray[u_xlati10 / 9]._DashSize, PropsArray[u_xlati10 / 9]._DashSpacing);
        u_xlat16_15.xy = (u_xlatb1.x) ? u_xlat16_15.xy : vec2(PropsArray[u_xlati10 / 9]._DashSize, PropsArray[u_xlati10 / 9]._DashSpacing);
        u_xlatb1.xyz = equal(ivec4(int(0xFFFFFFFFu), int(0xFFFFFFFEu), 3, 0), ivec4(PropsArray[u_xlati10 / 9]._DashSpace, PropsArray[u_xlati10 / 9]._DashSpace, PropsArray[u_xlati10 / 9]._DashSnap, PropsArray[u_xlati10 / 9]._DashSpace)).xyz;
        if(u_xlatb1.x){
            switch(PropsArray[u_xlati10 / 9]._ThicknessSpace){
                case 0:
                    u_xlat16_6.x = u_xlat4.x;
                    break;
                case 1:
                    u_xlat16_6.x = u_xlat20.x * u_xlat4.x;
                    break;
                case 2:
                    u_xlat16_35 = u_xlat20.x * u_xlat4.x;
                    u_xlat1.x = u_xlat16_35 * 100.0;
                    u_xlat31 = min(_ScreenParams.y, _ScreenParams.x);
                    u_xlat6.x = u_xlat1.x / u_xlat31;
                    u_xlat16_6.x = u_xlat6.x;
                    break;
                default:
                    u_xlat16_6.x = 0.0;
                    break;
            }
            switch(PropsArray[u_xlati10 / 9]._ThicknessSpace){
                case 0:
                    u_xlat16_6.y = u_xlat4.y;
                    break;
                case 1:
                    u_xlat16_6.y = u_xlat20.x * u_xlat4.y;
                    break;
                case 2:
                    u_xlat16_35 = u_xlat20.x * u_xlat4.y;
                    u_xlat1.x = u_xlat16_35 * 100.0;
                    u_xlat31 = min(_ScreenParams.y, _ScreenParams.x);
                    u_xlat16 = u_xlat1.x / u_xlat31;
                    u_xlat16_6.y = u_xlat16;
                    break;
                default:
                    u_xlat16_6.y = 0.0;
                    break;
            }
            switch(PropsArray[u_xlati10 / 9]._ThicknessSpace){
                case 0:
                    break;
                case 1:
                    u_xlat16_5.x = u_xlat20.x * u_xlat16_5.x;
                    break;
                case 2:
                    u_xlat16_35 = u_xlat20.x * u_xlat16_5.x;
                    u_xlat20.x = u_xlat16_35 * 100.0;
                    u_xlat1.x = min(_ScreenParams.y, _ScreenParams.x);
                    u_xlat5 = u_xlat20.x / u_xlat1.x;
                    u_xlat16_5.x = u_xlat5;
                    break;
                default:
                    u_xlat16_5.x = 0.0;
                    break;
            }
            u_xlat4.xy = u_xlat16_6.xy;
        }
        if(u_xlatb1.y){
            switch(PropsArray[u_xlati10 / 9]._DashSnap){
                case 0:
                    break;
                case 1:
                case 3:
                    u_xlat16_35 = roundEven(u_xlat16_15.x);
                    u_xlat16_15.x = max(u_xlat16_35, 1.0);
                    break;
                case 2:
                    u_xlat16_35 = u_xlat16_15.y + u_xlat16_15.x;
                    u_xlat16_35 = roundEven(u_xlat16_35);
                    u_xlat16_35 = max(u_xlat16_35, 1.0);
                    u_xlat16_15.x = (-u_xlat16_15.y) + u_xlat16_35;
                    break;
                default:
                    u_xlat16_15.x = 0.0;
                    break;
            }
            u_xlat20.x = u_xlat16_15.x;
            u_xlat2.y = u_xlat16_15.y;
        } else {
            u_xlat16_15.x = u_xlat16_15.y + u_xlat16_15.x;
            u_xlat2.y = u_xlat16_15.y / u_xlat16_15.x;
            u_xlat1.x = u_xlat4.y / u_xlat16_15.x;
            switch(PropsArray[u_xlati10 / 9]._DashSnap){
                case 0:
                    u_xlat16_15.x = u_xlat1.x;
                    break;
                case 1:
                case 3:
                    u_xlat16_25 = roundEven(u_xlat1.x);
                    u_xlat16_15.x = max(u_xlat16_25, 1.0);
                    break;
                case 2:
                    u_xlat16_25 = u_xlat2.y + u_xlat1.x;
                    u_xlat16_25 = roundEven(u_xlat16_25);
                    u_xlat16_25 = max(u_xlat16_25, 1.0);
                    u_xlat16_15.x = (-u_xlat2.y) + u_xlat16_25;
                    break;
                default:
                    u_xlat16_15.x = 0.0;
                    break;
            }
            u_xlat20.x = u_xlat16_15.x;
        }
        u_xlat1.x = (-u_xlat2.y) + 1.0;
        u_xlat11 = (-u_xlat1.x) * 0.5 + PropsArray[u_xlati10 / 9]._DashOffset;
        u_xlat16_15.x = (u_xlatb1.z) ? u_xlat11 : PropsArray[u_xlati10 / 9]._DashOffset;
        u_xlat10 = u_xlat20.x * u_xlat16_5.x;
        u_xlat2.z = u_xlat10 / u_xlat4.y;
        u_xlat10 = u_xlat4.x / u_xlat4.y;
        u_xlat10 = u_xlat10 * u_xlat20.x + (-u_xlat16_15.x);
        u_xlat2.x = (-u_xlat1.x) * 0.5 + u_xlat10;
        u_xlat16_5.xyz = u_xlat2.xyz;
    } else {
        u_xlat16_5.x = float(0.0);
        u_xlat16_5.y = float(0.0);
        u_xlat16_5.z = float(0.0);
    }
    u_xlat10 = in_POSITION0.y * 0.5 + 0.5;
    u_xlat1 = u_xlat3.yyyy * hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati0 + 1)];
    u_xlat1 = hlslcc_mtx4x4unity_StereoMatrixVP[u_xlati0] * u_xlat3.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati0 + 2)] * u_xlat3.zzzz + u_xlat1;
    gl_Position = u_xlat1 + hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati0 + 3)];
    vs_TEXCOORD0.xyz = u_xlat16_5.xyz;
    vs_TEXCOORD0.w = u_xlat30;
    vs_TEXCOORD1.z = u_xlat10;
    vs_SV_InstanceID0 =  uint(gl_InstanceID);
    u_xlatu0 = gl_ViewID_OVR;
    vs_BLENDWEIGHT0 = unity_StereoEyeIndices[int(u_xlatu0)].x;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifndef UNITY_RUNTIME_INSTANCING_ARRAY_SIZE
	#define UNITY_RUNTIME_INSTANCING_ARRAY_SIZE 2
#endif

precision highp float;
precision highp int;
#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	int unity_BaseInstanceID;
struct PropsArray_Type {
	int _ScaleMode;
	mediump vec4 _Color;
	mediump vec4 _ColorEnd;
	vec3 _PointStart;
	vec3 _PointEnd;
	mediump float _Thickness;
	int _ThicknessSpace;
	int _Alignment;
	int _DashType;
	mediump float _DashSize;
	mediump float _DashShapeModifier;
	mediump float _DashOffset;
	mediump float _DashSpacing;
	int _DashSpace;
	int _DashSnap;
};
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(0) uniform UnityInstancing_Props {
#endif
	UNITY_UNIFORM PropsArray_Type PropsArray[UNITY_RUNTIME_INSTANCING_ARRAY_SIZE];
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
in mediump vec4 vs_TEXCOORD0;
in mediump vec4 vs_TEXCOORD1;
flat in highp uint vs_SV_InstanceID0;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
bool u_xlatb0;
mediump vec2 u_xlat16_1;
vec4 u_xlat2;
mediump float u_xlat16_2;
int u_xlati3;
mediump vec3 u_xlat16_4;
vec2 u_xlat6;
bvec2 u_xlatb6;
mediump float u_xlat16_7;
mediump float u_xlat16_10;
void main()
{
    u_xlat0.xy = dFdx(vs_TEXCOORD1.xy);
    u_xlat0.zw = dFdy(vs_TEXCOORD1.xy);
    u_xlat0.x = dot(u_xlat0.xz, u_xlat0.xz);
    u_xlat0.y = dot(u_xlat0.yw, u_xlat0.yw);
    u_xlat0.xy = sqrt(u_xlat0.xy);
    u_xlat0.xy = max(u_xlat0.xy, vec2(9.99999975e-06, 9.99999975e-06));
    u_xlat6.xy = abs(vs_TEXCOORD1.xy) + vec2(-1.0, -1.0);
    u_xlat0.xy = u_xlat6.xy / u_xlat0.xy;
    u_xlat6.x = vs_TEXCOORD0.w * 0.909090877;
#ifdef UNITY_ADRENO_ES3
    u_xlat6.x = min(max(u_xlat6.x, 0.0), 1.0);
#else
    u_xlat6.x = clamp(u_xlat6.x, 0.0, 1.0);
#endif
    u_xlat0.x = u_xlat6.x * 0.5 + u_xlat0.x;
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat0.y = u_xlat6.x * 0.5 + u_xlat0.y;
#ifdef UNITY_ADRENO_ES3
    u_xlat0.y = min(max(u_xlat0.y, 0.0), 1.0);
#else
    u_xlat0.y = clamp(u_xlat0.y, 0.0, 1.0);
#endif
    u_xlat0.xy = (-u_xlat0.xy) + vec2(1.0, 1.0);
    u_xlat0.x = min(u_xlat0.y, u_xlat0.x);
    u_xlat16_1.y = vs_TEXCOORD1.x;
    u_xlat16_7 = vs_TEXCOORD0.z * vs_TEXCOORD1.x;
    u_xlat16_7 = u_xlat16_7 * 0.5;
    u_xlati3 = int(vs_SV_InstanceID0) + unity_BaseInstanceID;
    u_xlati3 = u_xlati3 * 9;
    u_xlat16_7 = u_xlat16_7 * PropsArray[u_xlati3 / 9]._DashShapeModifier + vs_TEXCOORD0.x;
    u_xlatb6.xy = equal(ivec4(1, 2, 1, 2), ivec4(ivec4(PropsArray[u_xlati3 / 9]._DashType, PropsArray[u_xlati3 / 9]._DashType, PropsArray[u_xlati3 / 9]._DashType, PropsArray[u_xlati3 / 9]._DashType))).xy;
    u_xlat16_7 = (u_xlatb6.x) ? u_xlat16_7 : vs_TEXCOORD0.x;
    u_xlat16_7 = fract(u_xlat16_7);
    u_xlat16_7 = u_xlat16_7 * 2.0 + -1.0;
    u_xlat16_7 = abs(u_xlat16_7) + (-vs_TEXCOORD0.y);
    u_xlat16_10 = (-vs_TEXCOORD0.y) + 1.0;
    u_xlat16_7 = u_xlat16_7 / u_xlat16_10;
    u_xlat16_10 = u_xlat16_10 / vs_TEXCOORD0.z;
    u_xlat16_1.x = (-u_xlat16_7) * u_xlat16_10 + 1.0;
    u_xlat16_10 = float(1.0) / u_xlat16_10;
    u_xlat16_10 = (-u_xlat16_10) + u_xlat16_7;
    u_xlat16_1.x = dot(u_xlat16_1.xy, u_xlat16_1.xy);
    u_xlat16_1.x = sqrt(u_xlat16_1.x);
    u_xlat16_1.x = u_xlat16_1.x + -1.0;
    u_xlat16_4.x = dFdx(u_xlat16_1.x);
    u_xlat16_2 = dFdy(u_xlat16_1.x);
    u_xlat16_4.x = abs(u_xlat16_4.x) + abs(u_xlat16_2);
    u_xlat16_1.x = u_xlat16_1.x / u_xlat16_4.x;
    u_xlat16_1.x = u_xlat16_1.x + 0.5;
#ifdef UNITY_ADRENO_ES3
    u_xlat16_1.x = min(max(u_xlat16_1.x, 0.0), 1.0);
#else
    u_xlat16_1.x = clamp(u_xlat16_1.x, 0.0, 1.0);
#endif
    u_xlat16_1.x = (-u_xlat16_1.x) + 1.0;
    u_xlat16_4.x = dFdx(u_xlat16_7);
    u_xlat16_2 = dFdy(u_xlat16_7);
    u_xlat16_4.x = abs(u_xlat16_4.x) + abs(u_xlat16_2);
    u_xlat16_4.z = u_xlat16_10 / u_xlat16_4.x;
    u_xlat16_4.x = u_xlat16_7 / u_xlat16_4.x;
    u_xlat16_4.xy = u_xlat16_4.xz + vec2(0.5, 0.5);
#ifdef UNITY_ADRENO_ES3
    u_xlat16_4.xy = min(max(u_xlat16_4.xy, 0.0), 1.0);
#else
    u_xlat16_4.xy = clamp(u_xlat16_4.xy, 0.0, 1.0);
#endif
    u_xlat16_1.x = max(u_xlat16_1.x, u_xlat16_4.y);
    u_xlat16_1.x = (u_xlatb6.y) ? u_xlat16_1.x : u_xlat16_4.x;
    u_xlat16_1.x = min(u_xlat0.x, u_xlat16_1.x);
    u_xlat16_4.x = vs_TEXCOORD0.w;
#ifdef UNITY_ADRENO_ES3
    u_xlat16_4.x = min(max(u_xlat16_4.x, 0.0), 1.0);
#else
    u_xlat16_4.x = clamp(u_xlat16_4.x, 0.0, 1.0);
#endif
    u_xlat0.x = u_xlat16_4.x * u_xlat16_1.x;
#ifdef UNITY_ADRENO_ES3
    u_xlatb6.x = !!(vs_TEXCOORD0.y>=0.999938965);
#else
    u_xlatb6.x = vs_TEXCOORD0.y>=0.999938965;
#endif
    u_xlat16_1.x = (u_xlatb6.x) ? 0.0 : u_xlat0.x;
    u_xlat2 = (-PropsArray[u_xlati3 / 9]._Color) + PropsArray[u_xlati3 / 9]._ColorEnd;
    u_xlat16_4.x = vs_TEXCOORD1.z;
#ifdef UNITY_ADRENO_ES3
    u_xlat16_4.x = min(max(u_xlat16_4.x, 0.0), 1.0);
#else
    u_xlat16_4.x = clamp(u_xlat16_4.x, 0.0, 1.0);
#endif
    u_xlat0 = u_xlat16_4.xxxx * u_xlat2 + PropsArray[u_xlati3 / 9]._Color;
    u_xlat16_4.x = u_xlat16_1.x * u_xlat0.w + -6.10351563e-05;
    u_xlat0.w = u_xlat0.w * u_xlat16_1.x;
    SV_Target0 = u_xlat0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb0 = !!(u_xlat16_4.x<0.0);
#else
    u_xlatb0 = u_xlat16_4.x<0.0;
#endif
    if(u_xlatb0){discard;}
    return;
}

#endif
                              $Globals         unity_BaseInstanceID                            UnityInstancing_Props             
   PropsArray         �      
   _ScaleMode                          _Color                       	   _ColorEnd                            _PointStart                   0   	   _PointEnd                     @   
   _Thickness                    L      _ThicknessSpace                  P   
   _Alignment                   T   	   _DashType                    X   	   _DashSize                     \      _DashShapeModifier                    `      _DashOffset                   d      _DashSpacing                  h   
   _DashSpace                   l   	   _DashSnap                    p      $Globals$         _ScreenParams                            unity_OrthoParams                           unity_BaseInstanceID                            UnityStereoGlobals  @  
      unity_StereoWorldSpaceCameraPos                        unity_StereoScaleOffset                        unity_StereoMatrixP                        unity_StereoMatrixV                 �      unity_StereoMatrixInvV                        unity_StereoMatrixVP                �     unity_StereoCameraProjection                      unity_StereoCameraInvProjection                 �     unity_StereoWorldToCamera                         unity_StereoCameraToWorld                   �         UnityStereoEyeIndices             unity_StereoEyeIndices                              UnityInstancing_PerDraw0             unity_Builtins0Array       �         unity_ObjectToWorldArray                        unity_WorldToObjectArray                 @         UnityInstancing_Props                 UnityStereoGlobals               UnityStereoEyeIndices                UnityInstancing_PerDraw0              