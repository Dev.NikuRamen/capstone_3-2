﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
// System.Int64[]
struct Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6;
// System.IntPtr[]
struct IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6;
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971;
// UnityEngine.AnimationCurve
struct AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03;
// UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// System.Collections.IDictionary
struct IDictionary_t99871C56B8EC2452AC5C4CF3831695E617B89D3A;
// System.Collections.IEnumerator
struct IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105;
// MoreMountains.Feedbacks.MMFeedback
struct MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4;
// MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics
struct MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4;
// MoreMountains.Feedbacks.MMFeedbackTiming
struct MMFeedbackTiming_tA2ECBE1844BDDE1A8986173D25959D9A863892A6;
// MoreMountains.Feedbacks.MMFeedbacks
struct MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914;
// MoreMountains.NiceVibrations.MMNVAndroidWaveForm
struct MMNVAndroidWaveForm_t7C12B4C6921EB2A4AFA8981BE10C077613392802;
// MoreMountains.NiceVibrations.MMNVAndroidWaveFormAsset
struct MMNVAndroidWaveFormAsset_tBC1568B780A9FB318F2030DECC54E75ED7ECF911;
// MoreMountains.NiceVibrations.MMNVRumbleWaveForm
struct MMNVRumbleWaveForm_t07D2E145C7FD2044901F051E21F0E65756AED720;
// MoreMountains.NiceVibrations.MMNVRumbleWaveFormAsset
struct MMNVRumbleWaveFormAsset_t366E4FEF311EB34F357E906A3FA3D44477244F9D;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A;
// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F;
// System.String
struct String_t;
// UnityEngine.TextAsset
struct TextAsset_t1969F5FD1F628C7C0A70D9605C0D251B4F547234;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics/<ContinuousHapticsCoroutine>d__38
struct U3CContinuousHapticsCoroutineU3Ed__38_t65DCA4B7CFC1877537768C18A7AB7BE8866C6FD9;

IL2CPP_EXTERN_C RuntimeClass* AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MMVibrationManager_tB11EBD05CD3C1FCD39B3313F1256138262E32001_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CContinuousHapticsCoroutineU3Ed__38_t65DCA4B7CFC1877537768C18A7AB7BE8866C6FD9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
IL2CPP_EXTERN_C const RuntimeMethod* U3CContinuousHapticsCoroutineU3Ed__38_System_Collections_IEnumerator_Reset_mD2AAB00CAD47726A832F2DAE13EF8651AAA1CFD0_RuntimeMethod_var;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
struct Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6;
struct KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_t7AB0057579B9ED25A040FFD5091048B8EDB71F52 
{
public:

public:
};


// System.Object

struct Il2CppArrayBounds;

// System.Array


// MoreMountains.NiceVibrations.MMNVAndroidWaveForm
struct MMNVAndroidWaveForm_t7C12B4C6921EB2A4AFA8981BE10C077613392802  : public RuntimeObject
{
public:
	// System.Int64[] MoreMountains.NiceVibrations.MMNVAndroidWaveForm::Pattern
	Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6* ___Pattern_0;
	// System.Int32[] MoreMountains.NiceVibrations.MMNVAndroidWaveForm::Amplitudes
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___Amplitudes_1;

public:
	inline static int32_t get_offset_of_Pattern_0() { return static_cast<int32_t>(offsetof(MMNVAndroidWaveForm_t7C12B4C6921EB2A4AFA8981BE10C077613392802, ___Pattern_0)); }
	inline Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6* get_Pattern_0() const { return ___Pattern_0; }
	inline Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6** get_address_of_Pattern_0() { return &___Pattern_0; }
	inline void set_Pattern_0(Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6* value)
	{
		___Pattern_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Pattern_0), (void*)value);
	}

	inline static int32_t get_offset_of_Amplitudes_1() { return static_cast<int32_t>(offsetof(MMNVAndroidWaveForm_t7C12B4C6921EB2A4AFA8981BE10C077613392802, ___Amplitudes_1)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_Amplitudes_1() const { return ___Amplitudes_1; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_Amplitudes_1() { return &___Amplitudes_1; }
	inline void set_Amplitudes_1(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___Amplitudes_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Amplitudes_1), (void*)value);
	}
};


// MoreMountains.NiceVibrations.MMNVRumbleWaveForm
struct MMNVRumbleWaveForm_t07D2E145C7FD2044901F051E21F0E65756AED720  : public RuntimeObject
{
public:
	// System.Int64[] MoreMountains.NiceVibrations.MMNVRumbleWaveForm::Pattern
	Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6* ___Pattern_0;
	// System.Int32[] MoreMountains.NiceVibrations.MMNVRumbleWaveForm::LowFrequencyAmplitudes
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___LowFrequencyAmplitudes_1;
	// System.Int32[] MoreMountains.NiceVibrations.MMNVRumbleWaveForm::HighFrequencyAmplitudes
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___HighFrequencyAmplitudes_2;

public:
	inline static int32_t get_offset_of_Pattern_0() { return static_cast<int32_t>(offsetof(MMNVRumbleWaveForm_t07D2E145C7FD2044901F051E21F0E65756AED720, ___Pattern_0)); }
	inline Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6* get_Pattern_0() const { return ___Pattern_0; }
	inline Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6** get_address_of_Pattern_0() { return &___Pattern_0; }
	inline void set_Pattern_0(Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6* value)
	{
		___Pattern_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Pattern_0), (void*)value);
	}

	inline static int32_t get_offset_of_LowFrequencyAmplitudes_1() { return static_cast<int32_t>(offsetof(MMNVRumbleWaveForm_t07D2E145C7FD2044901F051E21F0E65756AED720, ___LowFrequencyAmplitudes_1)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_LowFrequencyAmplitudes_1() const { return ___LowFrequencyAmplitudes_1; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_LowFrequencyAmplitudes_1() { return &___LowFrequencyAmplitudes_1; }
	inline void set_LowFrequencyAmplitudes_1(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___LowFrequencyAmplitudes_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___LowFrequencyAmplitudes_1), (void*)value);
	}

	inline static int32_t get_offset_of_HighFrequencyAmplitudes_2() { return static_cast<int32_t>(offsetof(MMNVRumbleWaveForm_t07D2E145C7FD2044901F051E21F0E65756AED720, ___HighFrequencyAmplitudes_2)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_HighFrequencyAmplitudes_2() const { return ___HighFrequencyAmplitudes_2; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_HighFrequencyAmplitudes_2() { return &___HighFrequencyAmplitudes_2; }
	inline void set_HighFrequencyAmplitudes_2(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___HighFrequencyAmplitudes_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___HighFrequencyAmplitudes_2), (void*)value);
	}
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
};

// MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics/<ContinuousHapticsCoroutine>d__38
struct U3CContinuousHapticsCoroutineU3Ed__38_t65DCA4B7CFC1877537768C18A7AB7BE8866C6FD9  : public RuntimeObject
{
public:
	// System.Int32 MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics/<ContinuousHapticsCoroutine>d__38::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics/<ContinuousHapticsCoroutine>d__38::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics/<ContinuousHapticsCoroutine>d__38::<>4__this
	MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4 * ___U3CU3E4__this_2;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics/<ContinuousHapticsCoroutine>d__38::<elapsedTime>5__2
	float ___U3CelapsedTimeU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CContinuousHapticsCoroutineU3Ed__38_t65DCA4B7CFC1877537768C18A7AB7BE8866C6FD9, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CContinuousHapticsCoroutineU3Ed__38_t65DCA4B7CFC1877537768C18A7AB7BE8866C6FD9, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CContinuousHapticsCoroutineU3Ed__38_t65DCA4B7CFC1877537768C18A7AB7BE8866C6FD9, ___U3CU3E4__this_2)); }
	inline MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CelapsedTimeU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CContinuousHapticsCoroutineU3Ed__38_t65DCA4B7CFC1877537768C18A7AB7BE8866C6FD9, ___U3CelapsedTimeU3E5__2_3)); }
	inline float get_U3CelapsedTimeU3E5__2_3() const { return ___U3CelapsedTimeU3E5__2_3; }
	inline float* get_address_of_U3CelapsedTimeU3E5__2_3() { return &___U3CelapsedTimeU3E5__2_3; }
	inline void set_U3CelapsedTimeU3E5__2_3(float value)
	{
		___U3CelapsedTimeU3E5__2_3 = value;
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.Int64
struct Int64_t378EE0D608BD3107E77238E85F30D2BBD46981F3 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int64_t378EE0D608BD3107E77238E85F30D2BBD46981F3, ___m_value_0)); }
	inline int64_t get_m_value_0() const { return ___m_value_0; }
	inline int64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int64_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.Keyframe
struct Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F 
{
public:
	// System.Single UnityEngine.Keyframe::m_Time
	float ___m_Time_0;
	// System.Single UnityEngine.Keyframe::m_Value
	float ___m_Value_1;
	// System.Single UnityEngine.Keyframe::m_InTangent
	float ___m_InTangent_2;
	// System.Single UnityEngine.Keyframe::m_OutTangent
	float ___m_OutTangent_3;
	// System.Int32 UnityEngine.Keyframe::m_WeightedMode
	int32_t ___m_WeightedMode_4;
	// System.Single UnityEngine.Keyframe::m_InWeight
	float ___m_InWeight_5;
	// System.Single UnityEngine.Keyframe::m_OutWeight
	float ___m_OutWeight_6;

public:
	inline static int32_t get_offset_of_m_Time_0() { return static_cast<int32_t>(offsetof(Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F, ___m_Time_0)); }
	inline float get_m_Time_0() const { return ___m_Time_0; }
	inline float* get_address_of_m_Time_0() { return &___m_Time_0; }
	inline void set_m_Time_0(float value)
	{
		___m_Time_0 = value;
	}

	inline static int32_t get_offset_of_m_Value_1() { return static_cast<int32_t>(offsetof(Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F, ___m_Value_1)); }
	inline float get_m_Value_1() const { return ___m_Value_1; }
	inline float* get_address_of_m_Value_1() { return &___m_Value_1; }
	inline void set_m_Value_1(float value)
	{
		___m_Value_1 = value;
	}

	inline static int32_t get_offset_of_m_InTangent_2() { return static_cast<int32_t>(offsetof(Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F, ___m_InTangent_2)); }
	inline float get_m_InTangent_2() const { return ___m_InTangent_2; }
	inline float* get_address_of_m_InTangent_2() { return &___m_InTangent_2; }
	inline void set_m_InTangent_2(float value)
	{
		___m_InTangent_2 = value;
	}

	inline static int32_t get_offset_of_m_OutTangent_3() { return static_cast<int32_t>(offsetof(Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F, ___m_OutTangent_3)); }
	inline float get_m_OutTangent_3() const { return ___m_OutTangent_3; }
	inline float* get_address_of_m_OutTangent_3() { return &___m_OutTangent_3; }
	inline void set_m_OutTangent_3(float value)
	{
		___m_OutTangent_3 = value;
	}

	inline static int32_t get_offset_of_m_WeightedMode_4() { return static_cast<int32_t>(offsetof(Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F, ___m_WeightedMode_4)); }
	inline int32_t get_m_WeightedMode_4() const { return ___m_WeightedMode_4; }
	inline int32_t* get_address_of_m_WeightedMode_4() { return &___m_WeightedMode_4; }
	inline void set_m_WeightedMode_4(int32_t value)
	{
		___m_WeightedMode_4 = value;
	}

	inline static int32_t get_offset_of_m_InWeight_5() { return static_cast<int32_t>(offsetof(Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F, ___m_InWeight_5)); }
	inline float get_m_InWeight_5() const { return ___m_InWeight_5; }
	inline float* get_address_of_m_InWeight_5() { return &___m_InWeight_5; }
	inline void set_m_InWeight_5(float value)
	{
		___m_InWeight_5 = value;
	}

	inline static int32_t get_offset_of_m_OutWeight_6() { return static_cast<int32_t>(offsetof(Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F, ___m_OutWeight_6)); }
	inline float get_m_OutWeight_6() const { return ___m_OutWeight_6; }
	inline float* get_address_of_m_OutWeight_6() { return &___m_OutWeight_6; }
	inline void set_m_OutWeight_6(float value)
	{
		___m_OutWeight_6 = value;
	}
};


// System.Single
struct Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.Vector3
struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// UnityEngine.AnimationCurve
struct AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.AnimationCurve::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7  : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7_marshaled_pinvoke : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7_marshaled_com : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// System.Exception
struct Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// MoreMountains.NiceVibrations.HapticTypes
struct HapticTypes_tDB2397BACC6CA297FDEA262A11E5ABD964E1709C 
{
public:
	// System.Int32 MoreMountains.NiceVibrations.HapticTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HapticTypes_tDB2397BACC6CA297FDEA262A11E5ABD964E1709C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics/HapticMethods
struct HapticMethods_tD7CCB09D6DC67EDB0A1A5A48189B6FB5D06FE6E7 
{
public:
	// System.Int32 MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics/HapticMethods::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HapticMethods_tD7CCB09D6DC67EDB0A1A5A48189B6FB5D06FE6E7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics/Timescales
struct Timescales_tBFCC8913E3E2A15C5EB3D152FDF2836E2B1A5D18 
{
public:
	// System.Int32 MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics/Timescales::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Timescales_tBFCC8913E3E2A15C5EB3D152FDF2836E2B1A5D18, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A_marshaled_pinvoke : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A_marshaled_com : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
};

// System.SystemException
struct SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62  : public Exception_t
{
public:

public:
};


// UnityEngine.TextAsset
struct TextAsset_t1969F5FD1F628C7C0A70D9605C0D251B4F547234  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// MoreMountains.NiceVibrations.MMNVAndroidWaveFormAsset
struct MMNVAndroidWaveFormAsset_tBC1568B780A9FB318F2030DECC54E75ED7ECF911  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:
	// MoreMountains.NiceVibrations.MMNVAndroidWaveForm MoreMountains.NiceVibrations.MMNVAndroidWaveFormAsset::WaveForm
	MMNVAndroidWaveForm_t7C12B4C6921EB2A4AFA8981BE10C077613392802 * ___WaveForm_4;
	// UnityEngine.TextAsset MoreMountains.NiceVibrations.MMNVAndroidWaveFormAsset::AHAPFile
	TextAsset_t1969F5FD1F628C7C0A70D9605C0D251B4F547234 * ___AHAPFile_5;
	// System.Single MoreMountains.NiceVibrations.MMNVAndroidWaveFormAsset::IntensityMultiplier
	float ___IntensityMultiplier_6;
	// System.Single MoreMountains.NiceVibrations.MMNVAndroidWaveFormAsset::SharpnessMultiplier
	float ___SharpnessMultiplier_7;

public:
	inline static int32_t get_offset_of_WaveForm_4() { return static_cast<int32_t>(offsetof(MMNVAndroidWaveFormAsset_tBC1568B780A9FB318F2030DECC54E75ED7ECF911, ___WaveForm_4)); }
	inline MMNVAndroidWaveForm_t7C12B4C6921EB2A4AFA8981BE10C077613392802 * get_WaveForm_4() const { return ___WaveForm_4; }
	inline MMNVAndroidWaveForm_t7C12B4C6921EB2A4AFA8981BE10C077613392802 ** get_address_of_WaveForm_4() { return &___WaveForm_4; }
	inline void set_WaveForm_4(MMNVAndroidWaveForm_t7C12B4C6921EB2A4AFA8981BE10C077613392802 * value)
	{
		___WaveForm_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___WaveForm_4), (void*)value);
	}

	inline static int32_t get_offset_of_AHAPFile_5() { return static_cast<int32_t>(offsetof(MMNVAndroidWaveFormAsset_tBC1568B780A9FB318F2030DECC54E75ED7ECF911, ___AHAPFile_5)); }
	inline TextAsset_t1969F5FD1F628C7C0A70D9605C0D251B4F547234 * get_AHAPFile_5() const { return ___AHAPFile_5; }
	inline TextAsset_t1969F5FD1F628C7C0A70D9605C0D251B4F547234 ** get_address_of_AHAPFile_5() { return &___AHAPFile_5; }
	inline void set_AHAPFile_5(TextAsset_t1969F5FD1F628C7C0A70D9605C0D251B4F547234 * value)
	{
		___AHAPFile_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AHAPFile_5), (void*)value);
	}

	inline static int32_t get_offset_of_IntensityMultiplier_6() { return static_cast<int32_t>(offsetof(MMNVAndroidWaveFormAsset_tBC1568B780A9FB318F2030DECC54E75ED7ECF911, ___IntensityMultiplier_6)); }
	inline float get_IntensityMultiplier_6() const { return ___IntensityMultiplier_6; }
	inline float* get_address_of_IntensityMultiplier_6() { return &___IntensityMultiplier_6; }
	inline void set_IntensityMultiplier_6(float value)
	{
		___IntensityMultiplier_6 = value;
	}

	inline static int32_t get_offset_of_SharpnessMultiplier_7() { return static_cast<int32_t>(offsetof(MMNVAndroidWaveFormAsset_tBC1568B780A9FB318F2030DECC54E75ED7ECF911, ___SharpnessMultiplier_7)); }
	inline float get_SharpnessMultiplier_7() const { return ___SharpnessMultiplier_7; }
	inline float* get_address_of_SharpnessMultiplier_7() { return &___SharpnessMultiplier_7; }
	inline void set_SharpnessMultiplier_7(float value)
	{
		___SharpnessMultiplier_7 = value;
	}
};


// MoreMountains.NiceVibrations.MMNVRumbleWaveFormAsset
struct MMNVRumbleWaveFormAsset_t366E4FEF311EB34F357E906A3FA3D44477244F9D  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:
	// MoreMountains.NiceVibrations.MMNVRumbleWaveForm MoreMountains.NiceVibrations.MMNVRumbleWaveFormAsset::WaveForm
	MMNVRumbleWaveForm_t07D2E145C7FD2044901F051E21F0E65756AED720 * ___WaveForm_4;
	// UnityEngine.TextAsset MoreMountains.NiceVibrations.MMNVRumbleWaveFormAsset::AHAPFile
	TextAsset_t1969F5FD1F628C7C0A70D9605C0D251B4F547234 * ___AHAPFile_5;
	// System.Single MoreMountains.NiceVibrations.MMNVRumbleWaveFormAsset::IntensityMultiplier
	float ___IntensityMultiplier_6;
	// System.Single MoreMountains.NiceVibrations.MMNVRumbleWaveFormAsset::SharpnessMultiplier
	float ___SharpnessMultiplier_7;

public:
	inline static int32_t get_offset_of_WaveForm_4() { return static_cast<int32_t>(offsetof(MMNVRumbleWaveFormAsset_t366E4FEF311EB34F357E906A3FA3D44477244F9D, ___WaveForm_4)); }
	inline MMNVRumbleWaveForm_t07D2E145C7FD2044901F051E21F0E65756AED720 * get_WaveForm_4() const { return ___WaveForm_4; }
	inline MMNVRumbleWaveForm_t07D2E145C7FD2044901F051E21F0E65756AED720 ** get_address_of_WaveForm_4() { return &___WaveForm_4; }
	inline void set_WaveForm_4(MMNVRumbleWaveForm_t07D2E145C7FD2044901F051E21F0E65756AED720 * value)
	{
		___WaveForm_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___WaveForm_4), (void*)value);
	}

	inline static int32_t get_offset_of_AHAPFile_5() { return static_cast<int32_t>(offsetof(MMNVRumbleWaveFormAsset_t366E4FEF311EB34F357E906A3FA3D44477244F9D, ___AHAPFile_5)); }
	inline TextAsset_t1969F5FD1F628C7C0A70D9605C0D251B4F547234 * get_AHAPFile_5() const { return ___AHAPFile_5; }
	inline TextAsset_t1969F5FD1F628C7C0A70D9605C0D251B4F547234 ** get_address_of_AHAPFile_5() { return &___AHAPFile_5; }
	inline void set_AHAPFile_5(TextAsset_t1969F5FD1F628C7C0A70D9605C0D251B4F547234 * value)
	{
		___AHAPFile_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AHAPFile_5), (void*)value);
	}

	inline static int32_t get_offset_of_IntensityMultiplier_6() { return static_cast<int32_t>(offsetof(MMNVRumbleWaveFormAsset_t366E4FEF311EB34F357E906A3FA3D44477244F9D, ___IntensityMultiplier_6)); }
	inline float get_IntensityMultiplier_6() const { return ___IntensityMultiplier_6; }
	inline float* get_address_of_IntensityMultiplier_6() { return &___IntensityMultiplier_6; }
	inline void set_IntensityMultiplier_6(float value)
	{
		___IntensityMultiplier_6 = value;
	}

	inline static int32_t get_offset_of_SharpnessMultiplier_7() { return static_cast<int32_t>(offsetof(MMNVRumbleWaveFormAsset_t366E4FEF311EB34F357E906A3FA3D44477244F9D, ___SharpnessMultiplier_7)); }
	inline float get_SharpnessMultiplier_7() const { return ___SharpnessMultiplier_7; }
	inline float* get_address_of_SharpnessMultiplier_7() { return &___SharpnessMultiplier_7; }
	inline void set_SharpnessMultiplier_7(float value)
	{
		___SharpnessMultiplier_7 = value;
	}
};


// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// MoreMountains.Feedbacks.MMFeedback
struct MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Boolean MoreMountains.Feedbacks.MMFeedback::Active
	bool ___Active_4;
	// System.String MoreMountains.Feedbacks.MMFeedback::Label
	String_t* ___Label_5;
	// System.Single MoreMountains.Feedbacks.MMFeedback::Chance
	float ___Chance_6;
	// MoreMountains.Feedbacks.MMFeedbackTiming MoreMountains.Feedbacks.MMFeedback::Timing
	MMFeedbackTiming_tA2ECBE1844BDDE1A8986173D25959D9A863892A6 * ___Timing_7;
	// UnityEngine.GameObject MoreMountains.Feedbacks.MMFeedback::<Owner>k__BackingField
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3COwnerU3Ek__BackingField_8;
	// System.Boolean MoreMountains.Feedbacks.MMFeedback::DebugActive
	bool ___DebugActive_9;
	// System.Boolean MoreMountains.Feedbacks.MMFeedback::<ScriptDrivenPause>k__BackingField
	bool ___U3CScriptDrivenPauseU3Ek__BackingField_10;
	// System.Single MoreMountains.Feedbacks.MMFeedback::<ScriptDrivenPauseAutoResume>k__BackingField
	float ___U3CScriptDrivenPauseAutoResumeU3Ek__BackingField_11;
	// System.Single MoreMountains.Feedbacks.MMFeedback::_lastPlayTimestamp
	float ____lastPlayTimestamp_12;
	// System.Int32 MoreMountains.Feedbacks.MMFeedback::_playsLeft
	int32_t ____playsLeft_13;
	// System.Boolean MoreMountains.Feedbacks.MMFeedback::_initialized
	bool ____initialized_14;
	// UnityEngine.Coroutine MoreMountains.Feedbacks.MMFeedback::_playCoroutine
	Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * ____playCoroutine_15;
	// UnityEngine.Coroutine MoreMountains.Feedbacks.MMFeedback::_infinitePlayCoroutine
	Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * ____infinitePlayCoroutine_16;
	// UnityEngine.Coroutine MoreMountains.Feedbacks.MMFeedback::_sequenceCoroutine
	Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * ____sequenceCoroutine_17;
	// UnityEngine.Coroutine MoreMountains.Feedbacks.MMFeedback::_repeatedPlayCoroutine
	Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * ____repeatedPlayCoroutine_18;
	// System.Int32 MoreMountains.Feedbacks.MMFeedback::_sequenceTrackID
	int32_t ____sequenceTrackID_19;
	// MoreMountains.Feedbacks.MMFeedbacks MoreMountains.Feedbacks.MMFeedback::_hostMMFeedbacks
	MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * ____hostMMFeedbacks_20;
	// System.Single MoreMountains.Feedbacks.MMFeedback::_beatInterval
	float ____beatInterval_21;
	// System.Boolean MoreMountains.Feedbacks.MMFeedback::BeatThisFrame
	bool ___BeatThisFrame_22;
	// System.Int32 MoreMountains.Feedbacks.MMFeedback::LastBeatIndex
	int32_t ___LastBeatIndex_23;
	// System.Int32 MoreMountains.Feedbacks.MMFeedback::CurrentSequenceIndex
	int32_t ___CurrentSequenceIndex_24;
	// System.Single MoreMountains.Feedbacks.MMFeedback::LastBeatTimestamp
	float ___LastBeatTimestamp_25;
	// System.Boolean MoreMountains.Feedbacks.MMFeedback::_isHostMMFeedbacksNotNull
	bool ____isHostMMFeedbacksNotNull_26;

public:
	inline static int32_t get_offset_of_Active_4() { return static_cast<int32_t>(offsetof(MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4, ___Active_4)); }
	inline bool get_Active_4() const { return ___Active_4; }
	inline bool* get_address_of_Active_4() { return &___Active_4; }
	inline void set_Active_4(bool value)
	{
		___Active_4 = value;
	}

	inline static int32_t get_offset_of_Label_5() { return static_cast<int32_t>(offsetof(MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4, ___Label_5)); }
	inline String_t* get_Label_5() const { return ___Label_5; }
	inline String_t** get_address_of_Label_5() { return &___Label_5; }
	inline void set_Label_5(String_t* value)
	{
		___Label_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Label_5), (void*)value);
	}

	inline static int32_t get_offset_of_Chance_6() { return static_cast<int32_t>(offsetof(MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4, ___Chance_6)); }
	inline float get_Chance_6() const { return ___Chance_6; }
	inline float* get_address_of_Chance_6() { return &___Chance_6; }
	inline void set_Chance_6(float value)
	{
		___Chance_6 = value;
	}

	inline static int32_t get_offset_of_Timing_7() { return static_cast<int32_t>(offsetof(MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4, ___Timing_7)); }
	inline MMFeedbackTiming_tA2ECBE1844BDDE1A8986173D25959D9A863892A6 * get_Timing_7() const { return ___Timing_7; }
	inline MMFeedbackTiming_tA2ECBE1844BDDE1A8986173D25959D9A863892A6 ** get_address_of_Timing_7() { return &___Timing_7; }
	inline void set_Timing_7(MMFeedbackTiming_tA2ECBE1844BDDE1A8986173D25959D9A863892A6 * value)
	{
		___Timing_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Timing_7), (void*)value);
	}

	inline static int32_t get_offset_of_U3COwnerU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4, ___U3COwnerU3Ek__BackingField_8)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_U3COwnerU3Ek__BackingField_8() const { return ___U3COwnerU3Ek__BackingField_8; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_U3COwnerU3Ek__BackingField_8() { return &___U3COwnerU3Ek__BackingField_8; }
	inline void set_U3COwnerU3Ek__BackingField_8(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___U3COwnerU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3COwnerU3Ek__BackingField_8), (void*)value);
	}

	inline static int32_t get_offset_of_DebugActive_9() { return static_cast<int32_t>(offsetof(MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4, ___DebugActive_9)); }
	inline bool get_DebugActive_9() const { return ___DebugActive_9; }
	inline bool* get_address_of_DebugActive_9() { return &___DebugActive_9; }
	inline void set_DebugActive_9(bool value)
	{
		___DebugActive_9 = value;
	}

	inline static int32_t get_offset_of_U3CScriptDrivenPauseU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4, ___U3CScriptDrivenPauseU3Ek__BackingField_10)); }
	inline bool get_U3CScriptDrivenPauseU3Ek__BackingField_10() const { return ___U3CScriptDrivenPauseU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CScriptDrivenPauseU3Ek__BackingField_10() { return &___U3CScriptDrivenPauseU3Ek__BackingField_10; }
	inline void set_U3CScriptDrivenPauseU3Ek__BackingField_10(bool value)
	{
		___U3CScriptDrivenPauseU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CScriptDrivenPauseAutoResumeU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4, ___U3CScriptDrivenPauseAutoResumeU3Ek__BackingField_11)); }
	inline float get_U3CScriptDrivenPauseAutoResumeU3Ek__BackingField_11() const { return ___U3CScriptDrivenPauseAutoResumeU3Ek__BackingField_11; }
	inline float* get_address_of_U3CScriptDrivenPauseAutoResumeU3Ek__BackingField_11() { return &___U3CScriptDrivenPauseAutoResumeU3Ek__BackingField_11; }
	inline void set_U3CScriptDrivenPauseAutoResumeU3Ek__BackingField_11(float value)
	{
		___U3CScriptDrivenPauseAutoResumeU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of__lastPlayTimestamp_12() { return static_cast<int32_t>(offsetof(MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4, ____lastPlayTimestamp_12)); }
	inline float get__lastPlayTimestamp_12() const { return ____lastPlayTimestamp_12; }
	inline float* get_address_of__lastPlayTimestamp_12() { return &____lastPlayTimestamp_12; }
	inline void set__lastPlayTimestamp_12(float value)
	{
		____lastPlayTimestamp_12 = value;
	}

	inline static int32_t get_offset_of__playsLeft_13() { return static_cast<int32_t>(offsetof(MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4, ____playsLeft_13)); }
	inline int32_t get__playsLeft_13() const { return ____playsLeft_13; }
	inline int32_t* get_address_of__playsLeft_13() { return &____playsLeft_13; }
	inline void set__playsLeft_13(int32_t value)
	{
		____playsLeft_13 = value;
	}

	inline static int32_t get_offset_of__initialized_14() { return static_cast<int32_t>(offsetof(MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4, ____initialized_14)); }
	inline bool get__initialized_14() const { return ____initialized_14; }
	inline bool* get_address_of__initialized_14() { return &____initialized_14; }
	inline void set__initialized_14(bool value)
	{
		____initialized_14 = value;
	}

	inline static int32_t get_offset_of__playCoroutine_15() { return static_cast<int32_t>(offsetof(MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4, ____playCoroutine_15)); }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * get__playCoroutine_15() const { return ____playCoroutine_15; }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 ** get_address_of__playCoroutine_15() { return &____playCoroutine_15; }
	inline void set__playCoroutine_15(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * value)
	{
		____playCoroutine_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____playCoroutine_15), (void*)value);
	}

	inline static int32_t get_offset_of__infinitePlayCoroutine_16() { return static_cast<int32_t>(offsetof(MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4, ____infinitePlayCoroutine_16)); }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * get__infinitePlayCoroutine_16() const { return ____infinitePlayCoroutine_16; }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 ** get_address_of__infinitePlayCoroutine_16() { return &____infinitePlayCoroutine_16; }
	inline void set__infinitePlayCoroutine_16(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * value)
	{
		____infinitePlayCoroutine_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____infinitePlayCoroutine_16), (void*)value);
	}

	inline static int32_t get_offset_of__sequenceCoroutine_17() { return static_cast<int32_t>(offsetof(MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4, ____sequenceCoroutine_17)); }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * get__sequenceCoroutine_17() const { return ____sequenceCoroutine_17; }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 ** get_address_of__sequenceCoroutine_17() { return &____sequenceCoroutine_17; }
	inline void set__sequenceCoroutine_17(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * value)
	{
		____sequenceCoroutine_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____sequenceCoroutine_17), (void*)value);
	}

	inline static int32_t get_offset_of__repeatedPlayCoroutine_18() { return static_cast<int32_t>(offsetof(MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4, ____repeatedPlayCoroutine_18)); }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * get__repeatedPlayCoroutine_18() const { return ____repeatedPlayCoroutine_18; }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 ** get_address_of__repeatedPlayCoroutine_18() { return &____repeatedPlayCoroutine_18; }
	inline void set__repeatedPlayCoroutine_18(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * value)
	{
		____repeatedPlayCoroutine_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____repeatedPlayCoroutine_18), (void*)value);
	}

	inline static int32_t get_offset_of__sequenceTrackID_19() { return static_cast<int32_t>(offsetof(MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4, ____sequenceTrackID_19)); }
	inline int32_t get__sequenceTrackID_19() const { return ____sequenceTrackID_19; }
	inline int32_t* get_address_of__sequenceTrackID_19() { return &____sequenceTrackID_19; }
	inline void set__sequenceTrackID_19(int32_t value)
	{
		____sequenceTrackID_19 = value;
	}

	inline static int32_t get_offset_of__hostMMFeedbacks_20() { return static_cast<int32_t>(offsetof(MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4, ____hostMMFeedbacks_20)); }
	inline MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * get__hostMMFeedbacks_20() const { return ____hostMMFeedbacks_20; }
	inline MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 ** get_address_of__hostMMFeedbacks_20() { return &____hostMMFeedbacks_20; }
	inline void set__hostMMFeedbacks_20(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * value)
	{
		____hostMMFeedbacks_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____hostMMFeedbacks_20), (void*)value);
	}

	inline static int32_t get_offset_of__beatInterval_21() { return static_cast<int32_t>(offsetof(MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4, ____beatInterval_21)); }
	inline float get__beatInterval_21() const { return ____beatInterval_21; }
	inline float* get_address_of__beatInterval_21() { return &____beatInterval_21; }
	inline void set__beatInterval_21(float value)
	{
		____beatInterval_21 = value;
	}

	inline static int32_t get_offset_of_BeatThisFrame_22() { return static_cast<int32_t>(offsetof(MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4, ___BeatThisFrame_22)); }
	inline bool get_BeatThisFrame_22() const { return ___BeatThisFrame_22; }
	inline bool* get_address_of_BeatThisFrame_22() { return &___BeatThisFrame_22; }
	inline void set_BeatThisFrame_22(bool value)
	{
		___BeatThisFrame_22 = value;
	}

	inline static int32_t get_offset_of_LastBeatIndex_23() { return static_cast<int32_t>(offsetof(MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4, ___LastBeatIndex_23)); }
	inline int32_t get_LastBeatIndex_23() const { return ___LastBeatIndex_23; }
	inline int32_t* get_address_of_LastBeatIndex_23() { return &___LastBeatIndex_23; }
	inline void set_LastBeatIndex_23(int32_t value)
	{
		___LastBeatIndex_23 = value;
	}

	inline static int32_t get_offset_of_CurrentSequenceIndex_24() { return static_cast<int32_t>(offsetof(MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4, ___CurrentSequenceIndex_24)); }
	inline int32_t get_CurrentSequenceIndex_24() const { return ___CurrentSequenceIndex_24; }
	inline int32_t* get_address_of_CurrentSequenceIndex_24() { return &___CurrentSequenceIndex_24; }
	inline void set_CurrentSequenceIndex_24(int32_t value)
	{
		___CurrentSequenceIndex_24 = value;
	}

	inline static int32_t get_offset_of_LastBeatTimestamp_25() { return static_cast<int32_t>(offsetof(MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4, ___LastBeatTimestamp_25)); }
	inline float get_LastBeatTimestamp_25() const { return ___LastBeatTimestamp_25; }
	inline float* get_address_of_LastBeatTimestamp_25() { return &___LastBeatTimestamp_25; }
	inline void set_LastBeatTimestamp_25(float value)
	{
		___LastBeatTimestamp_25 = value;
	}

	inline static int32_t get_offset_of__isHostMMFeedbacksNotNull_26() { return static_cast<int32_t>(offsetof(MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4, ____isHostMMFeedbacksNotNull_26)); }
	inline bool get__isHostMMFeedbacksNotNull_26() const { return ____isHostMMFeedbacksNotNull_26; }
	inline bool* get_address_of__isHostMMFeedbacksNotNull_26() { return &____isHostMMFeedbacksNotNull_26; }
	inline void set__isHostMMFeedbacksNotNull_26(bool value)
	{
		____isHostMMFeedbacksNotNull_26 = value;
	}
};


// MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics
struct MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4  : public MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4
{
public:
	// MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics/HapticMethods MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::HapticMethod
	int32_t ___HapticMethod_27;
	// MoreMountains.NiceVibrations.HapticTypes MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::HapticType
	int32_t ___HapticType_28;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::TransientIntensity
	float ___TransientIntensity_29;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::TransientSharpness
	float ___TransientSharpness_30;
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::ATVibrateIOS
	bool ___ATVibrateIOS_31;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::ATIOSIntensity
	float ___ATIOSIntensity_32;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::ATIOSSharpness
	float ___ATIOSSharpness_33;
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::ATVibrateAndroid
	bool ___ATVibrateAndroid_34;
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::ATVibrateAndroidIfNoSupport
	bool ___ATVibrateAndroidIfNoSupport_35;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::ATAndroidIntensity
	float ___ATAndroidIntensity_36;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::ATAndroidSharpness
	float ___ATAndroidSharpness_37;
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::ATRumble
	bool ___ATRumble_38;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::ATRumbleIntensity
	float ___ATRumbleIntensity_39;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::ATRumbleSharpness
	float ___ATRumbleSharpness_40;
	// System.Int32 MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::ATRumbleControllerID
	int32_t ___ATRumbleControllerID_41;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::InitialContinuousIntensity
	float ___InitialContinuousIntensity_42;
	// UnityEngine.AnimationCurve MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::ContinuousIntensityCurve
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___ContinuousIntensityCurve_43;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::InitialContinuousSharpness
	float ___InitialContinuousSharpness_44;
	// UnityEngine.AnimationCurve MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::ContinuousSharpnessCurve
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___ContinuousSharpnessCurve_45;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::ContinuousDuration
	float ___ContinuousDuration_46;
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::APVibrateIOS
	bool ___APVibrateIOS_47;
	// UnityEngine.TextAsset MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::AHAPFileForIOS
	TextAsset_t1969F5FD1F628C7C0A70D9605C0D251B4F547234 * ___AHAPFileForIOS_48;
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::APVibrateAndroid
	bool ___APVibrateAndroid_49;
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::APVibrateAndroidIfNoSupport
	bool ___APVibrateAndroidIfNoSupport_50;
	// MoreMountains.NiceVibrations.MMNVAndroidWaveFormAsset MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::AndroidWaveFormFile
	MMNVAndroidWaveFormAsset_tBC1568B780A9FB318F2030DECC54E75ED7ECF911 * ___AndroidWaveFormFile_51;
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::APRumble
	bool ___APRumble_52;
	// MoreMountains.NiceVibrations.MMNVRumbleWaveFormAsset MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::RumbleWaveFormFile
	MMNVRumbleWaveFormAsset_t366E4FEF311EB34F357E906A3FA3D44477244F9D * ___RumbleWaveFormFile_53;
	// System.Int32 MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::AndroidRepeat
	int32_t ___AndroidRepeat_54;
	// System.Int32 MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::RumbleRepeat
	int32_t ___RumbleRepeat_55;
	// MoreMountains.NiceVibrations.HapticTypes MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::OldIOSFallback
	int32_t ___OldIOSFallback_56;
	// MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics/Timescales MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::Timescale
	int32_t ___Timescale_57;
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::AllowRumble
	bool ___AllowRumble_58;
	// System.Int32 MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::ControllerID
	int32_t ___ControllerID_59;

public:
	inline static int32_t get_offset_of_HapticMethod_27() { return static_cast<int32_t>(offsetof(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4, ___HapticMethod_27)); }
	inline int32_t get_HapticMethod_27() const { return ___HapticMethod_27; }
	inline int32_t* get_address_of_HapticMethod_27() { return &___HapticMethod_27; }
	inline void set_HapticMethod_27(int32_t value)
	{
		___HapticMethod_27 = value;
	}

	inline static int32_t get_offset_of_HapticType_28() { return static_cast<int32_t>(offsetof(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4, ___HapticType_28)); }
	inline int32_t get_HapticType_28() const { return ___HapticType_28; }
	inline int32_t* get_address_of_HapticType_28() { return &___HapticType_28; }
	inline void set_HapticType_28(int32_t value)
	{
		___HapticType_28 = value;
	}

	inline static int32_t get_offset_of_TransientIntensity_29() { return static_cast<int32_t>(offsetof(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4, ___TransientIntensity_29)); }
	inline float get_TransientIntensity_29() const { return ___TransientIntensity_29; }
	inline float* get_address_of_TransientIntensity_29() { return &___TransientIntensity_29; }
	inline void set_TransientIntensity_29(float value)
	{
		___TransientIntensity_29 = value;
	}

	inline static int32_t get_offset_of_TransientSharpness_30() { return static_cast<int32_t>(offsetof(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4, ___TransientSharpness_30)); }
	inline float get_TransientSharpness_30() const { return ___TransientSharpness_30; }
	inline float* get_address_of_TransientSharpness_30() { return &___TransientSharpness_30; }
	inline void set_TransientSharpness_30(float value)
	{
		___TransientSharpness_30 = value;
	}

	inline static int32_t get_offset_of_ATVibrateIOS_31() { return static_cast<int32_t>(offsetof(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4, ___ATVibrateIOS_31)); }
	inline bool get_ATVibrateIOS_31() const { return ___ATVibrateIOS_31; }
	inline bool* get_address_of_ATVibrateIOS_31() { return &___ATVibrateIOS_31; }
	inline void set_ATVibrateIOS_31(bool value)
	{
		___ATVibrateIOS_31 = value;
	}

	inline static int32_t get_offset_of_ATIOSIntensity_32() { return static_cast<int32_t>(offsetof(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4, ___ATIOSIntensity_32)); }
	inline float get_ATIOSIntensity_32() const { return ___ATIOSIntensity_32; }
	inline float* get_address_of_ATIOSIntensity_32() { return &___ATIOSIntensity_32; }
	inline void set_ATIOSIntensity_32(float value)
	{
		___ATIOSIntensity_32 = value;
	}

	inline static int32_t get_offset_of_ATIOSSharpness_33() { return static_cast<int32_t>(offsetof(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4, ___ATIOSSharpness_33)); }
	inline float get_ATIOSSharpness_33() const { return ___ATIOSSharpness_33; }
	inline float* get_address_of_ATIOSSharpness_33() { return &___ATIOSSharpness_33; }
	inline void set_ATIOSSharpness_33(float value)
	{
		___ATIOSSharpness_33 = value;
	}

	inline static int32_t get_offset_of_ATVibrateAndroid_34() { return static_cast<int32_t>(offsetof(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4, ___ATVibrateAndroid_34)); }
	inline bool get_ATVibrateAndroid_34() const { return ___ATVibrateAndroid_34; }
	inline bool* get_address_of_ATVibrateAndroid_34() { return &___ATVibrateAndroid_34; }
	inline void set_ATVibrateAndroid_34(bool value)
	{
		___ATVibrateAndroid_34 = value;
	}

	inline static int32_t get_offset_of_ATVibrateAndroidIfNoSupport_35() { return static_cast<int32_t>(offsetof(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4, ___ATVibrateAndroidIfNoSupport_35)); }
	inline bool get_ATVibrateAndroidIfNoSupport_35() const { return ___ATVibrateAndroidIfNoSupport_35; }
	inline bool* get_address_of_ATVibrateAndroidIfNoSupport_35() { return &___ATVibrateAndroidIfNoSupport_35; }
	inline void set_ATVibrateAndroidIfNoSupport_35(bool value)
	{
		___ATVibrateAndroidIfNoSupport_35 = value;
	}

	inline static int32_t get_offset_of_ATAndroidIntensity_36() { return static_cast<int32_t>(offsetof(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4, ___ATAndroidIntensity_36)); }
	inline float get_ATAndroidIntensity_36() const { return ___ATAndroidIntensity_36; }
	inline float* get_address_of_ATAndroidIntensity_36() { return &___ATAndroidIntensity_36; }
	inline void set_ATAndroidIntensity_36(float value)
	{
		___ATAndroidIntensity_36 = value;
	}

	inline static int32_t get_offset_of_ATAndroidSharpness_37() { return static_cast<int32_t>(offsetof(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4, ___ATAndroidSharpness_37)); }
	inline float get_ATAndroidSharpness_37() const { return ___ATAndroidSharpness_37; }
	inline float* get_address_of_ATAndroidSharpness_37() { return &___ATAndroidSharpness_37; }
	inline void set_ATAndroidSharpness_37(float value)
	{
		___ATAndroidSharpness_37 = value;
	}

	inline static int32_t get_offset_of_ATRumble_38() { return static_cast<int32_t>(offsetof(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4, ___ATRumble_38)); }
	inline bool get_ATRumble_38() const { return ___ATRumble_38; }
	inline bool* get_address_of_ATRumble_38() { return &___ATRumble_38; }
	inline void set_ATRumble_38(bool value)
	{
		___ATRumble_38 = value;
	}

	inline static int32_t get_offset_of_ATRumbleIntensity_39() { return static_cast<int32_t>(offsetof(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4, ___ATRumbleIntensity_39)); }
	inline float get_ATRumbleIntensity_39() const { return ___ATRumbleIntensity_39; }
	inline float* get_address_of_ATRumbleIntensity_39() { return &___ATRumbleIntensity_39; }
	inline void set_ATRumbleIntensity_39(float value)
	{
		___ATRumbleIntensity_39 = value;
	}

	inline static int32_t get_offset_of_ATRumbleSharpness_40() { return static_cast<int32_t>(offsetof(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4, ___ATRumbleSharpness_40)); }
	inline float get_ATRumbleSharpness_40() const { return ___ATRumbleSharpness_40; }
	inline float* get_address_of_ATRumbleSharpness_40() { return &___ATRumbleSharpness_40; }
	inline void set_ATRumbleSharpness_40(float value)
	{
		___ATRumbleSharpness_40 = value;
	}

	inline static int32_t get_offset_of_ATRumbleControllerID_41() { return static_cast<int32_t>(offsetof(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4, ___ATRumbleControllerID_41)); }
	inline int32_t get_ATRumbleControllerID_41() const { return ___ATRumbleControllerID_41; }
	inline int32_t* get_address_of_ATRumbleControllerID_41() { return &___ATRumbleControllerID_41; }
	inline void set_ATRumbleControllerID_41(int32_t value)
	{
		___ATRumbleControllerID_41 = value;
	}

	inline static int32_t get_offset_of_InitialContinuousIntensity_42() { return static_cast<int32_t>(offsetof(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4, ___InitialContinuousIntensity_42)); }
	inline float get_InitialContinuousIntensity_42() const { return ___InitialContinuousIntensity_42; }
	inline float* get_address_of_InitialContinuousIntensity_42() { return &___InitialContinuousIntensity_42; }
	inline void set_InitialContinuousIntensity_42(float value)
	{
		___InitialContinuousIntensity_42 = value;
	}

	inline static int32_t get_offset_of_ContinuousIntensityCurve_43() { return static_cast<int32_t>(offsetof(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4, ___ContinuousIntensityCurve_43)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_ContinuousIntensityCurve_43() const { return ___ContinuousIntensityCurve_43; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_ContinuousIntensityCurve_43() { return &___ContinuousIntensityCurve_43; }
	inline void set_ContinuousIntensityCurve_43(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___ContinuousIntensityCurve_43 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ContinuousIntensityCurve_43), (void*)value);
	}

	inline static int32_t get_offset_of_InitialContinuousSharpness_44() { return static_cast<int32_t>(offsetof(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4, ___InitialContinuousSharpness_44)); }
	inline float get_InitialContinuousSharpness_44() const { return ___InitialContinuousSharpness_44; }
	inline float* get_address_of_InitialContinuousSharpness_44() { return &___InitialContinuousSharpness_44; }
	inline void set_InitialContinuousSharpness_44(float value)
	{
		___InitialContinuousSharpness_44 = value;
	}

	inline static int32_t get_offset_of_ContinuousSharpnessCurve_45() { return static_cast<int32_t>(offsetof(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4, ___ContinuousSharpnessCurve_45)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_ContinuousSharpnessCurve_45() const { return ___ContinuousSharpnessCurve_45; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_ContinuousSharpnessCurve_45() { return &___ContinuousSharpnessCurve_45; }
	inline void set_ContinuousSharpnessCurve_45(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___ContinuousSharpnessCurve_45 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ContinuousSharpnessCurve_45), (void*)value);
	}

	inline static int32_t get_offset_of_ContinuousDuration_46() { return static_cast<int32_t>(offsetof(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4, ___ContinuousDuration_46)); }
	inline float get_ContinuousDuration_46() const { return ___ContinuousDuration_46; }
	inline float* get_address_of_ContinuousDuration_46() { return &___ContinuousDuration_46; }
	inline void set_ContinuousDuration_46(float value)
	{
		___ContinuousDuration_46 = value;
	}

	inline static int32_t get_offset_of_APVibrateIOS_47() { return static_cast<int32_t>(offsetof(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4, ___APVibrateIOS_47)); }
	inline bool get_APVibrateIOS_47() const { return ___APVibrateIOS_47; }
	inline bool* get_address_of_APVibrateIOS_47() { return &___APVibrateIOS_47; }
	inline void set_APVibrateIOS_47(bool value)
	{
		___APVibrateIOS_47 = value;
	}

	inline static int32_t get_offset_of_AHAPFileForIOS_48() { return static_cast<int32_t>(offsetof(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4, ___AHAPFileForIOS_48)); }
	inline TextAsset_t1969F5FD1F628C7C0A70D9605C0D251B4F547234 * get_AHAPFileForIOS_48() const { return ___AHAPFileForIOS_48; }
	inline TextAsset_t1969F5FD1F628C7C0A70D9605C0D251B4F547234 ** get_address_of_AHAPFileForIOS_48() { return &___AHAPFileForIOS_48; }
	inline void set_AHAPFileForIOS_48(TextAsset_t1969F5FD1F628C7C0A70D9605C0D251B4F547234 * value)
	{
		___AHAPFileForIOS_48 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AHAPFileForIOS_48), (void*)value);
	}

	inline static int32_t get_offset_of_APVibrateAndroid_49() { return static_cast<int32_t>(offsetof(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4, ___APVibrateAndroid_49)); }
	inline bool get_APVibrateAndroid_49() const { return ___APVibrateAndroid_49; }
	inline bool* get_address_of_APVibrateAndroid_49() { return &___APVibrateAndroid_49; }
	inline void set_APVibrateAndroid_49(bool value)
	{
		___APVibrateAndroid_49 = value;
	}

	inline static int32_t get_offset_of_APVibrateAndroidIfNoSupport_50() { return static_cast<int32_t>(offsetof(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4, ___APVibrateAndroidIfNoSupport_50)); }
	inline bool get_APVibrateAndroidIfNoSupport_50() const { return ___APVibrateAndroidIfNoSupport_50; }
	inline bool* get_address_of_APVibrateAndroidIfNoSupport_50() { return &___APVibrateAndroidIfNoSupport_50; }
	inline void set_APVibrateAndroidIfNoSupport_50(bool value)
	{
		___APVibrateAndroidIfNoSupport_50 = value;
	}

	inline static int32_t get_offset_of_AndroidWaveFormFile_51() { return static_cast<int32_t>(offsetof(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4, ___AndroidWaveFormFile_51)); }
	inline MMNVAndroidWaveFormAsset_tBC1568B780A9FB318F2030DECC54E75ED7ECF911 * get_AndroidWaveFormFile_51() const { return ___AndroidWaveFormFile_51; }
	inline MMNVAndroidWaveFormAsset_tBC1568B780A9FB318F2030DECC54E75ED7ECF911 ** get_address_of_AndroidWaveFormFile_51() { return &___AndroidWaveFormFile_51; }
	inline void set_AndroidWaveFormFile_51(MMNVAndroidWaveFormAsset_tBC1568B780A9FB318F2030DECC54E75ED7ECF911 * value)
	{
		___AndroidWaveFormFile_51 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AndroidWaveFormFile_51), (void*)value);
	}

	inline static int32_t get_offset_of_APRumble_52() { return static_cast<int32_t>(offsetof(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4, ___APRumble_52)); }
	inline bool get_APRumble_52() const { return ___APRumble_52; }
	inline bool* get_address_of_APRumble_52() { return &___APRumble_52; }
	inline void set_APRumble_52(bool value)
	{
		___APRumble_52 = value;
	}

	inline static int32_t get_offset_of_RumbleWaveFormFile_53() { return static_cast<int32_t>(offsetof(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4, ___RumbleWaveFormFile_53)); }
	inline MMNVRumbleWaveFormAsset_t366E4FEF311EB34F357E906A3FA3D44477244F9D * get_RumbleWaveFormFile_53() const { return ___RumbleWaveFormFile_53; }
	inline MMNVRumbleWaveFormAsset_t366E4FEF311EB34F357E906A3FA3D44477244F9D ** get_address_of_RumbleWaveFormFile_53() { return &___RumbleWaveFormFile_53; }
	inline void set_RumbleWaveFormFile_53(MMNVRumbleWaveFormAsset_t366E4FEF311EB34F357E906A3FA3D44477244F9D * value)
	{
		___RumbleWaveFormFile_53 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___RumbleWaveFormFile_53), (void*)value);
	}

	inline static int32_t get_offset_of_AndroidRepeat_54() { return static_cast<int32_t>(offsetof(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4, ___AndroidRepeat_54)); }
	inline int32_t get_AndroidRepeat_54() const { return ___AndroidRepeat_54; }
	inline int32_t* get_address_of_AndroidRepeat_54() { return &___AndroidRepeat_54; }
	inline void set_AndroidRepeat_54(int32_t value)
	{
		___AndroidRepeat_54 = value;
	}

	inline static int32_t get_offset_of_RumbleRepeat_55() { return static_cast<int32_t>(offsetof(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4, ___RumbleRepeat_55)); }
	inline int32_t get_RumbleRepeat_55() const { return ___RumbleRepeat_55; }
	inline int32_t* get_address_of_RumbleRepeat_55() { return &___RumbleRepeat_55; }
	inline void set_RumbleRepeat_55(int32_t value)
	{
		___RumbleRepeat_55 = value;
	}

	inline static int32_t get_offset_of_OldIOSFallback_56() { return static_cast<int32_t>(offsetof(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4, ___OldIOSFallback_56)); }
	inline int32_t get_OldIOSFallback_56() const { return ___OldIOSFallback_56; }
	inline int32_t* get_address_of_OldIOSFallback_56() { return &___OldIOSFallback_56; }
	inline void set_OldIOSFallback_56(int32_t value)
	{
		___OldIOSFallback_56 = value;
	}

	inline static int32_t get_offset_of_Timescale_57() { return static_cast<int32_t>(offsetof(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4, ___Timescale_57)); }
	inline int32_t get_Timescale_57() const { return ___Timescale_57; }
	inline int32_t* get_address_of_Timescale_57() { return &___Timescale_57; }
	inline void set_Timescale_57(int32_t value)
	{
		___Timescale_57 = value;
	}

	inline static int32_t get_offset_of_AllowRumble_58() { return static_cast<int32_t>(offsetof(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4, ___AllowRumble_58)); }
	inline bool get_AllowRumble_58() const { return ___AllowRumble_58; }
	inline bool* get_address_of_AllowRumble_58() { return &___AllowRumble_58; }
	inline void set_AllowRumble_58(bool value)
	{
		___AllowRumble_58 = value;
	}

	inline static int32_t get_offset_of_ControllerID_59() { return static_cast<int32_t>(offsetof(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4, ___ControllerID_59)); }
	inline int32_t get_ControllerID_59() const { return ___ControllerID_59; }
	inline int32_t* get_address_of_ControllerID_59() { return &___ControllerID_59; }
	inline void set_ControllerID_59(int32_t value)
	{
		___ControllerID_59 = value;
	}
};

struct MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4_StaticFields
{
public:
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::_continuousPlaying
	bool ____continuousPlaying_60;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::_continuousStartedAt
	float ____continuousStartedAt_61;

public:
	inline static int32_t get_offset_of__continuousPlaying_60() { return static_cast<int32_t>(offsetof(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4_StaticFields, ____continuousPlaying_60)); }
	inline bool get__continuousPlaying_60() const { return ____continuousPlaying_60; }
	inline bool* get_address_of__continuousPlaying_60() { return &____continuousPlaying_60; }
	inline void set__continuousPlaying_60(bool value)
	{
		____continuousPlaying_60 = value;
	}

	inline static int32_t get_offset_of__continuousStartedAt_61() { return static_cast<int32_t>(offsetof(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4_StaticFields, ____continuousStartedAt_61)); }
	inline float get__continuousStartedAt_61() const { return ____continuousStartedAt_61; }
	inline float* get_address_of__continuousStartedAt_61() { return &____continuousStartedAt_61; }
	inline void set__continuousStartedAt_61(float value)
	{
		____continuousStartedAt_61 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Int64[]
struct Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int64_t m_Items[1];

public:
	inline int64_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int64_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int64_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int64_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int64_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int64_t value)
	{
		m_Items[index] = value;
	}
};
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  m_Items[1];

public:
	inline Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  value)
	{
		m_Items[index] = value;
	}
};



// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// System.String UnityEngine.TextAsset::get_text()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* TextAsset_get_text_m89A756483BA3218E173F5D62A582070714BC1218 (TextAsset_t1969F5FD1F628C7C0A70D9605C0D251B4F547234 * __this, const RuntimeMethod* method);
// System.Void MoreMountains.NiceVibrations.MMVibrationManager::AdvancedHapticPattern(System.Boolean,System.String,System.Boolean,System.Int64[],System.Int32[],System.Int32,System.Boolean,System.Boolean,System.Int64[],System.Int32[],System.Int32[],System.Int32,MoreMountains.NiceVibrations.HapticTypes,UnityEngine.MonoBehaviour,System.Int32,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMVibrationManager_AdvancedHapticPattern_m6467FF5FF371810386F806F372F8AB5EC2AFC87A (bool ___ios0, String_t* ___iOSJSONString1, bool ___android2, Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6* ___androidPattern3, Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___androidAmplitudes4, int32_t ___androidRepeat5, bool ___vibrateAndroidIfNoSupport6, bool ___rumble7, Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6* ___rumblePattern8, Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___rumbleLowFreqAmplitudes9, Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___rumbleHighFreqAmplitudes10, int32_t ___rumbleRepeat11, int32_t ___fallbackOldiOS12, MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * ___coroutineSupport13, int32_t ___controllerID14, bool ___threaded15, const RuntimeMethod* method);
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719 (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, RuntimeObject* ___routine0, const RuntimeMethod* method);
// System.Void MoreMountains.NiceVibrations.MMVibrationManager::Haptic(MoreMountains.NiceVibrations.HapticTypes,System.Boolean,System.Boolean,UnityEngine.MonoBehaviour,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMVibrationManager_Haptic_m3E0D47F9FF62A6F01196A9A8F8A09DFEE4A60BED (int32_t ___type0, bool ___defaultToRegularVibrate1, bool ___alsoRumble2, MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * ___coroutineSupport3, int32_t ___controllerID4, const RuntimeMethod* method);
// System.Void MoreMountains.NiceVibrations.MMVibrationManager::TransientHaptic(System.Single,System.Single,System.Boolean,UnityEngine.MonoBehaviour,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMVibrationManager_TransientHaptic_m8BE60B2A22EC7151FEB890AF9BC635FD5150164B (float ___intensity0, float ___sharpness1, bool ___alsoRumble2, MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * ___coroutineSupport3, int32_t ___controllerID4, const RuntimeMethod* method);
// System.Void MoreMountains.NiceVibrations.MMVibrationManager::TransientHaptic(System.Boolean,System.Single,System.Single,System.Boolean,System.Single,System.Single,System.Boolean,System.Boolean,System.Single,System.Single,System.Int32,UnityEngine.MonoBehaviour,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMVibrationManager_TransientHaptic_m0DDF95EF69D7C2340F126BE952E06E9DA35AE541 (bool ___vibrateiOS0, float ___iOSIntensity1, float ___iOSSharpness2, bool ___vibrateAndroid3, float ___androidIntensity4, float ___androidSharpness5, bool ___vibrateAndroidIfNoSupport6, bool ___rumble7, float ___rumbleLowFrequency8, float ___rumbleHighFrequency9, int32_t ___controllerID10, MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * ___coroutineSupport11, bool ___threaded12, const RuntimeMethod* method);
// System.Void MoreMountains.NiceVibrations.MMVibrationManager::StopContinuousHaptic(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMVibrationManager_StopContinuousHaptic_mBC0A5B8BB4889C60D9BAF9E52FDA52786F6C9DA7 (bool ___alsoRumble0, const RuntimeMethod* method);
// System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics/<ContinuousHapticsCoroutine>d__38::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CContinuousHapticsCoroutineU3Ed__38__ctor_mE821571FC13CCCEB8D334847F1B87F3150DD02F9 (U3CContinuousHapticsCoroutineU3Ed__38_t65DCA4B7CFC1877537768C18A7AB7BE8866C6FD9 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_unscaledTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_unscaledTime_m85A3479E3D78D05FEDEEFEF36944AC5EF9B31258 (const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_time()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844 (const RuntimeMethod* method);
// System.Void UnityEngine.Keyframe::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F * __this, float ___time0, float ___value1, const RuntimeMethod* method);
// System.Void UnityEngine.AnimationCurve::.ctor(UnityEngine.Keyframe[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnimationCurve__ctor_mDF6C1314A61F0E6F286865DD8BEA991795C07AC0 (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * __this, KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* ___keys0, const RuntimeMethod* method);
// System.Void MoreMountains.Feedbacks.MMFeedback::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMFeedback__ctor_m3A0FE0C8FEDD2D759EC31C4432C65DBD4B1A06A0 (MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4 * __this, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void MoreMountains.NiceVibrations.MMVibrationManager::ContinuousHaptic(System.Single,System.Single,System.Single,MoreMountains.NiceVibrations.HapticTypes,UnityEngine.MonoBehaviour,System.Boolean,System.Int32,System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMVibrationManager_ContinuousHaptic_mF6819043B22C8734BB3F57528C39ED84A7F481FC (float ___intensity0, float ___sharpness1, float ___duration2, int32_t ___fallbackOldiOS3, MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * ___mono4, bool ___alsoRumble5, int32_t ___controllerID6, bool ___threaded7, bool ___fullIntensity8, const RuntimeMethod* method);
// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::Remap(System.Single,System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MMFeedbackHaptics_Remap_m86D254DD73ED991398E7B74A4C7B8559E0963A5E (float ___x0, float ___A1, float ___B2, float ___C3, float ___D4, const RuntimeMethod* method);
// System.Single UnityEngine.AnimationCurve::Evaluate(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float AnimationCurve_Evaluate_m1248B5B167F1FFFDC847A08C56B7D63B32311E6A (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * __this, float ___time0, const RuntimeMethod* method);
// System.Void MoreMountains.NiceVibrations.MMVibrationManager::UpdateContinuousHaptic(System.Single,System.Single,System.Boolean,System.Int32,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMVibrationManager_UpdateContinuousHaptic_mC6F1C52965D1CB05CB6782E7B1264BA9244FD5DC (float ___intensity0, float ___sharpness1, bool ___alsoRumble2, int32_t ___controllerID3, bool ___threaded4, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * __this, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMFeedbackHaptics_CustomPlayFeedback_m6AA592FE07B84FC795577FCB970A43F0B8426A9D (MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position0, float ___feedbacksIntensity1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMVibrationManager_tB11EBD05CD3C1FCD39B3313F1256138262E32001_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6* V_1 = NULL;
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* V_2 = NULL;
	Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6* V_3 = NULL;
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* V_4 = NULL;
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* V_5 = NULL;
	int32_t V_6 = 0;
	String_t* G_B7_0 = NULL;
	Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6* G_B10_0 = NULL;
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* G_B13_0 = NULL;
	Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6* G_B16_0 = NULL;
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* G_B19_0 = NULL;
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* G_B22_0 = NULL;
	{
		// if (!Active)
		bool L_0 = ((MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4 *)__this)->get_Active_4();
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		// return;
		return;
	}

IL_0009:
	{
		// switch(HapticMethod)
		int32_t L_1 = __this->get_HapticMethod_27();
		V_6 = L_1;
		int32_t L_2 = V_6;
		switch (L_2)
		{
			case 0:
			{
				goto IL_0150;
			}
			case 1:
			{
				goto IL_016a;
			}
			case 2:
			{
				goto IL_0142;
			}
			case 3:
			{
				goto IL_0035;
			}
			case 4:
			{
				goto IL_01d3;
			}
			case 5:
			{
				goto IL_0189;
			}
			case 6:
			{
				goto IL_01eb;
			}
		}
	}
	{
		return;
	}

IL_0035:
	{
		// string iOSString = (AHAPFileForIOS == null) ? "" : AHAPFileForIOS.text;
		TextAsset_t1969F5FD1F628C7C0A70D9605C0D251B4F547234 * L_3 = __this->get_AHAPFileForIOS_48();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_4;
		L_4 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_3, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0050;
		}
	}
	{
		TextAsset_t1969F5FD1F628C7C0A70D9605C0D251B4F547234 * L_5 = __this->get_AHAPFileForIOS_48();
		NullCheck(L_5);
		String_t* L_6;
		L_6 = TextAsset_get_text_m89A756483BA3218E173F5D62A582070714BC1218(L_5, /*hidden argument*/NULL);
		G_B7_0 = L_6;
		goto IL_0055;
	}

IL_0050:
	{
		G_B7_0 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
	}

IL_0055:
	{
		V_0 = G_B7_0;
		// long[] androidPattern = (AndroidWaveFormFile == null) ? null : AndroidWaveFormFile.WaveForm.Pattern;
		MMNVAndroidWaveFormAsset_tBC1568B780A9FB318F2030DECC54E75ED7ECF911 * L_7 = __this->get_AndroidWaveFormFile_51();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_8;
		L_8 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_7, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0076;
		}
	}
	{
		MMNVAndroidWaveFormAsset_tBC1568B780A9FB318F2030DECC54E75ED7ECF911 * L_9 = __this->get_AndroidWaveFormFile_51();
		NullCheck(L_9);
		MMNVAndroidWaveForm_t7C12B4C6921EB2A4AFA8981BE10C077613392802 * L_10 = L_9->get_WaveForm_4();
		NullCheck(L_10);
		Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6* L_11 = L_10->get_Pattern_0();
		G_B10_0 = L_11;
		goto IL_0077;
	}

IL_0076:
	{
		G_B10_0 = ((Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6*)(NULL));
	}

IL_0077:
	{
		V_1 = G_B10_0;
		// int[] androidAmplitude = (AndroidWaveFormFile == null) ? null : AndroidWaveFormFile.WaveForm.Amplitudes;
		MMNVAndroidWaveFormAsset_tBC1568B780A9FB318F2030DECC54E75ED7ECF911 * L_12 = __this->get_AndroidWaveFormFile_51();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_13;
		L_13 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_12, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_0098;
		}
	}
	{
		MMNVAndroidWaveFormAsset_tBC1568B780A9FB318F2030DECC54E75ED7ECF911 * L_14 = __this->get_AndroidWaveFormFile_51();
		NullCheck(L_14);
		MMNVAndroidWaveForm_t7C12B4C6921EB2A4AFA8981BE10C077613392802 * L_15 = L_14->get_WaveForm_4();
		NullCheck(L_15);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_16 = L_15->get_Amplitudes_1();
		G_B13_0 = L_16;
		goto IL_0099;
	}

IL_0098:
	{
		G_B13_0 = ((Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(NULL));
	}

IL_0099:
	{
		V_2 = G_B13_0;
		// long[] rumblePattern = (RumbleWaveFormFile == null) ? null : RumbleWaveFormFile.WaveForm.Pattern;
		MMNVRumbleWaveFormAsset_t366E4FEF311EB34F357E906A3FA3D44477244F9D * L_17 = __this->get_RumbleWaveFormFile_53();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_18;
		L_18 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_17, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (L_18)
		{
			goto IL_00ba;
		}
	}
	{
		MMNVRumbleWaveFormAsset_t366E4FEF311EB34F357E906A3FA3D44477244F9D * L_19 = __this->get_RumbleWaveFormFile_53();
		NullCheck(L_19);
		MMNVRumbleWaveForm_t07D2E145C7FD2044901F051E21F0E65756AED720 * L_20 = L_19->get_WaveForm_4();
		NullCheck(L_20);
		Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6* L_21 = L_20->get_Pattern_0();
		G_B16_0 = L_21;
		goto IL_00bb;
	}

IL_00ba:
	{
		G_B16_0 = ((Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6*)(NULL));
	}

IL_00bb:
	{
		V_3 = G_B16_0;
		// int[] lowFreqAmplitude = (RumbleWaveFormFile == null) ? null : RumbleWaveFormFile.WaveForm.LowFrequencyAmplitudes;
		MMNVRumbleWaveFormAsset_t366E4FEF311EB34F357E906A3FA3D44477244F9D * L_22 = __this->get_RumbleWaveFormFile_53();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_23;
		L_23 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_22, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (L_23)
		{
			goto IL_00dc;
		}
	}
	{
		MMNVRumbleWaveFormAsset_t366E4FEF311EB34F357E906A3FA3D44477244F9D * L_24 = __this->get_RumbleWaveFormFile_53();
		NullCheck(L_24);
		MMNVRumbleWaveForm_t07D2E145C7FD2044901F051E21F0E65756AED720 * L_25 = L_24->get_WaveForm_4();
		NullCheck(L_25);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_26 = L_25->get_LowFrequencyAmplitudes_1();
		G_B19_0 = L_26;
		goto IL_00dd;
	}

IL_00dc:
	{
		G_B19_0 = ((Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(NULL));
	}

IL_00dd:
	{
		V_4 = G_B19_0;
		// int[] highFreqAmplitude = (RumbleWaveFormFile == null) ? null : RumbleWaveFormFile.WaveForm.HighFrequencyAmplitudes;
		MMNVRumbleWaveFormAsset_t366E4FEF311EB34F357E906A3FA3D44477244F9D * L_27 = __this->get_RumbleWaveFormFile_53();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_28;
		L_28 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_27, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (L_28)
		{
			goto IL_00ff;
		}
	}
	{
		MMNVRumbleWaveFormAsset_t366E4FEF311EB34F357E906A3FA3D44477244F9D * L_29 = __this->get_RumbleWaveFormFile_53();
		NullCheck(L_29);
		MMNVRumbleWaveForm_t07D2E145C7FD2044901F051E21F0E65756AED720 * L_30 = L_29->get_WaveForm_4();
		NullCheck(L_30);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_31 = L_30->get_HighFrequencyAmplitudes_2();
		G_B22_0 = L_31;
		goto IL_0100;
	}

IL_00ff:
	{
		G_B22_0 = ((Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(NULL));
	}

IL_0100:
	{
		V_5 = G_B22_0;
		// MMVibrationManager.AdvancedHapticPattern(APVibrateIOS, iOSString, APVibrateAndroid, androidPattern, androidAmplitude,
		//     AndroidRepeat, APVibrateAndroidIfNoSupport, APRumble,
		//                                                     rumblePattern, lowFreqAmplitude, highFreqAmplitude, RumbleRepeat,
		//                                             OldIOSFallback, this, ControllerID);
		bool L_32 = __this->get_APVibrateIOS_47();
		String_t* L_33 = V_0;
		bool L_34 = __this->get_APVibrateAndroid_49();
		Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6* L_35 = V_1;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_36 = V_2;
		int32_t L_37 = __this->get_AndroidRepeat_54();
		bool L_38 = __this->get_APVibrateAndroidIfNoSupport_50();
		bool L_39 = __this->get_APRumble_52();
		Int64U5BU5D_tCA61E42872C63A4286B24EEE6E0650143B43DCE6* L_40 = V_3;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_41 = V_4;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_42 = V_5;
		int32_t L_43 = __this->get_RumbleRepeat_55();
		int32_t L_44 = __this->get_OldIOSFallback_56();
		int32_t L_45 = __this->get_ControllerID_59();
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tB11EBD05CD3C1FCD39B3313F1256138262E32001_il2cpp_TypeInfo_var);
		MMVibrationManager_AdvancedHapticPattern_m6467FF5FF371810386F806F372F8AB5EC2AFC87A(L_32, L_33, L_34, L_35, L_36, L_37, L_38, L_39, L_40, L_41, L_42, L_43, L_44, __this, L_45, (bool)0, /*hidden argument*/NULL);
		// break;
		return;
	}

IL_0142:
	{
		// StartCoroutine(ContinuousHapticsCoroutine());
		RuntimeObject* L_46;
		L_46 = VirtFuncInvoker0< RuntimeObject* >::Invoke(39 /* System.Collections.IEnumerator MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::ContinuousHapticsCoroutine() */, __this);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_47;
		L_47 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_46, /*hidden argument*/NULL);
		// break;
		return;
	}

IL_0150:
	{
		// MMVibrationManager.Haptic(HapticType, false, AllowRumble, this, ControllerID);
		int32_t L_48 = __this->get_HapticType_28();
		bool L_49 = __this->get_AllowRumble_58();
		int32_t L_50 = __this->get_ControllerID_59();
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tB11EBD05CD3C1FCD39B3313F1256138262E32001_il2cpp_TypeInfo_var);
		MMVibrationManager_Haptic_m3E0D47F9FF62A6F01196A9A8F8A09DFEE4A60BED(L_48, (bool)0, L_49, __this, L_50, /*hidden argument*/NULL);
		// break;
		return;
	}

IL_016a:
	{
		// MMVibrationManager.TransientHaptic(TransientIntensity, TransientSharpness, AllowRumble, this, ControllerID);
		float L_51 = __this->get_TransientIntensity_29();
		float L_52 = __this->get_TransientSharpness_30();
		bool L_53 = __this->get_AllowRumble_58();
		int32_t L_54 = __this->get_ControllerID_59();
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tB11EBD05CD3C1FCD39B3313F1256138262E32001_il2cpp_TypeInfo_var);
		MMVibrationManager_TransientHaptic_m8BE60B2A22EC7151FEB890AF9BC635FD5150164B(L_51, L_52, L_53, __this, L_54, /*hidden argument*/NULL);
		// break;
		return;
	}

IL_0189:
	{
		// MMVibrationManager.TransientHaptic(ATVibrateIOS, ATIOSIntensity, ATIOSSharpness, ATVibrateAndroid,
		//     ATAndroidIntensity, ATAndroidSharpness, ATVibrateAndroidIfNoSupport, ATRumble,
		//     ATRumbleIntensity, ATRumbleSharpness, ATRumbleControllerID, this);
		bool L_55 = __this->get_ATVibrateIOS_31();
		float L_56 = __this->get_ATIOSIntensity_32();
		float L_57 = __this->get_ATIOSSharpness_33();
		bool L_58 = __this->get_ATVibrateAndroid_34();
		float L_59 = __this->get_ATAndroidIntensity_36();
		float L_60 = __this->get_ATAndroidSharpness_37();
		bool L_61 = __this->get_ATVibrateAndroidIfNoSupport_35();
		bool L_62 = __this->get_ATRumble_38();
		float L_63 = __this->get_ATRumbleIntensity_39();
		float L_64 = __this->get_ATRumbleSharpness_40();
		int32_t L_65 = __this->get_ATRumbleControllerID_41();
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tB11EBD05CD3C1FCD39B3313F1256138262E32001_il2cpp_TypeInfo_var);
		MMVibrationManager_TransientHaptic_m0DDF95EF69D7C2340F126BE952E06E9DA35AE541(L_55, L_56, L_57, L_58, L_59, L_60, L_61, L_62, L_63, L_64, L_65, __this, (bool)1, /*hidden argument*/NULL);
		// break;
		return;
	}

IL_01d3:
	{
		// if (_continuousPlaying)
		IL2CPP_RUNTIME_CLASS_INIT(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4_il2cpp_TypeInfo_var);
		bool L_66 = ((MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4_StaticFields*)il2cpp_codegen_static_fields_for(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4_il2cpp_TypeInfo_var))->get__continuousPlaying_60();
		if (!L_66)
		{
			goto IL_01eb;
		}
	}
	{
		// MMVibrationManager.StopContinuousHaptic(AllowRumble);
		bool L_67 = __this->get_AllowRumble_58();
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tB11EBD05CD3C1FCD39B3313F1256138262E32001_il2cpp_TypeInfo_var);
		MMVibrationManager_StopContinuousHaptic_mBC0A5B8BB4889C60D9BAF9E52FDA52786F6C9DA7(L_67, /*hidden argument*/NULL);
		// _continuousPlaying = false;
		IL2CPP_RUNTIME_CLASS_INIT(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4_il2cpp_TypeInfo_var);
		((MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4_StaticFields*)il2cpp_codegen_static_fields_for(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4_il2cpp_TypeInfo_var))->set__continuousPlaying_60((bool)0);
	}

IL_01eb:
	{
		// }
		return;
	}
}
// System.Collections.IEnumerator MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::ContinuousHapticsCoroutine()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* MMFeedbackHaptics_ContinuousHapticsCoroutine_m01993FA703820E851718C960CB95D508AF49E51F (MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CContinuousHapticsCoroutineU3Ed__38_t65DCA4B7CFC1877537768C18A7AB7BE8866C6FD9_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CContinuousHapticsCoroutineU3Ed__38_t65DCA4B7CFC1877537768C18A7AB7BE8866C6FD9 * L_0 = (U3CContinuousHapticsCoroutineU3Ed__38_t65DCA4B7CFC1877537768C18A7AB7BE8866C6FD9 *)il2cpp_codegen_object_new(U3CContinuousHapticsCoroutineU3Ed__38_t65DCA4B7CFC1877537768C18A7AB7BE8866C6FD9_il2cpp_TypeInfo_var);
		U3CContinuousHapticsCoroutineU3Ed__38__ctor_mE821571FC13CCCEB8D334847F1B87F3150DD02F9(L_0, 0, /*hidden argument*/NULL);
		U3CContinuousHapticsCoroutineU3Ed__38_t65DCA4B7CFC1877537768C18A7AB7BE8866C6FD9 * L_1 = L_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_2(__this);
		return L_1;
	}
}
// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::ComputeElapsedTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MMFeedbackHaptics_ComputeElapsedTime_m5C35BF378C9BF0870200F5C0F55B854260AFA6C4 (MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// float elapsedTime = (Timescale == Timescales.ScaledTime) ? Time.time - _continuousStartedAt : Time.unscaledTime - _continuousStartedAt;
		int32_t L_0 = __this->get_Timescale_57();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		float L_1;
		L_1 = Time_get_unscaledTime_m85A3479E3D78D05FEDEEFEF36944AC5EF9B31258(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4_il2cpp_TypeInfo_var);
		float L_2 = ((MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4_StaticFields*)il2cpp_codegen_static_fields_for(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4_il2cpp_TypeInfo_var))->get__continuousStartedAt_61();
		return ((float)il2cpp_codegen_subtract((float)L_1, (float)L_2));
	}

IL_0014:
	{
		float L_3;
		L_3 = Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4_il2cpp_TypeInfo_var);
		float L_4 = ((MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4_StaticFields*)il2cpp_codegen_static_fields_for(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4_il2cpp_TypeInfo_var))->get__continuousStartedAt_61();
		// return elapsedTime;
		return ((float)il2cpp_codegen_subtract((float)L_3, (float)L_4));
	}
}
// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::Remap(System.Single,System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MMFeedbackHaptics_Remap_m86D254DD73ED991398E7B74A4C7B8559E0963A5E (float ___x0, float ___A1, float ___B2, float ___C3, float ___D4, const RuntimeMethod* method)
{
	{
		// float remappedValue = C + (x - A) / (B - A) * (D - C);
		float L_0 = ___C3;
		float L_1 = ___x0;
		float L_2 = ___A1;
		float L_3 = ___B2;
		float L_4 = ___A1;
		float L_5 = ___D4;
		float L_6 = ___C3;
		// return remappedValue;
		return ((float)il2cpp_codegen_add((float)L_0, (float)((float)il2cpp_codegen_multiply((float)((float)((float)((float)il2cpp_codegen_subtract((float)L_1, (float)L_2))/(float)((float)il2cpp_codegen_subtract((float)L_3, (float)L_4)))), (float)((float)il2cpp_codegen_subtract((float)L_5, (float)L_6))))));
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMFeedbackHaptics__ctor_mBC83ABA2DC49DA6DA6F80A87382D07B559559068 (MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public HapticTypes HapticType = HapticTypes.None;
		__this->set_HapticType_28(((int32_t)9));
		// public float TransientIntensity = 1f;
		__this->set_TransientIntensity_29((1.0f));
		// public float TransientSharpness = 1f;
		__this->set_TransientSharpness_30((1.0f));
		// public bool ATVibrateIOS = true;
		__this->set_ATVibrateIOS_31((bool)1);
		// public float ATIOSIntensity = 1f;
		__this->set_ATIOSIntensity_32((1.0f));
		// public float ATIOSSharpness = 1f;
		__this->set_ATIOSSharpness_33((1.0f));
		// public bool ATVibrateAndroid = true;
		__this->set_ATVibrateAndroid_34((bool)1);
		// public float ATAndroidIntensity = 1f;
		__this->set_ATAndroidIntensity_36((1.0f));
		// public float ATAndroidSharpness = 1f;
		__this->set_ATAndroidSharpness_37((1.0f));
		// public bool ATRumble = true;
		__this->set_ATRumble_38((bool)1);
		// public float ATRumbleIntensity = 1f;
		__this->set_ATRumbleIntensity_39((1.0f));
		// public float ATRumbleSharpness = 1f;
		__this->set_ATRumbleSharpness_40((1.0f));
		// public int ATRumbleControllerID = -1;
		__this->set_ATRumbleControllerID_41((-1));
		// public float InitialContinuousIntensity = 1f;
		__this->set_InitialContinuousIntensity_42((1.0f));
		// public AnimationCurve ContinuousIntensityCurve = new AnimationCurve(new Keyframe(0, 1), new Keyframe(1f, 1f));
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_0 = (KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)SZArrayNew(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var, (uint32_t)2);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_1 = L_0;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_2;
		memset((&L_2), 0, sizeof(L_2));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_2), (0.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_2);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_3 = L_1;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_4), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_4);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_5 = (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *)il2cpp_codegen_object_new(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_mDF6C1314A61F0E6F286865DD8BEA991795C07AC0(L_5, L_3, /*hidden argument*/NULL);
		__this->set_ContinuousIntensityCurve_43(L_5);
		// public float InitialContinuousSharpness = 1f;
		__this->set_InitialContinuousSharpness_44((1.0f));
		// public AnimationCurve ContinuousSharpnessCurve = new AnimationCurve(new Keyframe(0, 1), new Keyframe(1f, 1f));
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_6 = (KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)SZArrayNew(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var, (uint32_t)2);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_7 = L_6;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_8;
		memset((&L_8), 0, sizeof(L_8));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_8), (0.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_7);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_8);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_9 = L_7;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_10;
		memset((&L_10), 0, sizeof(L_10));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_10), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_9);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(1), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_10);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_11 = (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *)il2cpp_codegen_object_new(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_mDF6C1314A61F0E6F286865DD8BEA991795C07AC0(L_11, L_9, /*hidden argument*/NULL);
		__this->set_ContinuousSharpnessCurve_45(L_11);
		// public float ContinuousDuration = 1f;
		__this->set_ContinuousDuration_46((1.0f));
		// public bool APVibrateIOS = true;
		__this->set_APVibrateIOS_47((bool)1);
		// public bool APVibrateAndroid = true;
		__this->set_APVibrateAndroid_49((bool)1);
		// public bool APRumble = true;
		__this->set_APRumble_52((bool)1);
		// public int AndroidRepeat = -1;
		__this->set_AndroidRepeat_54((-1));
		// public int RumbleRepeat = -1;
		__this->set_RumbleRepeat_55((-1));
		// public Timescales Timescale = Timescales.UnscaledTime;
		__this->set_Timescale_57(1);
		// public bool AllowRumble = true;
		__this->set_AllowRumble_58((bool)1);
		// public int ControllerID = -1;
		__this->set_ControllerID_59((-1));
		MMFeedback__ctor_m3A0FE0C8FEDD2D759EC31C4432C65DBD4B1A06A0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMFeedbackHaptics__cctor_m8EB9BB4B754FBA8BEAB3E7398646C6648D61FE16 (const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics/<ContinuousHapticsCoroutine>d__38::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CContinuousHapticsCoroutineU3Ed__38__ctor_mE821571FC13CCCEB8D334847F1B87F3150DD02F9 (U3CContinuousHapticsCoroutineU3Ed__38_t65DCA4B7CFC1877537768C18A7AB7BE8866C6FD9 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics/<ContinuousHapticsCoroutine>d__38::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CContinuousHapticsCoroutineU3Ed__38_System_IDisposable_Dispose_mF3F216154793B8C8257532C6FDDF27832CA71807 (U3CContinuousHapticsCoroutineU3Ed__38_t65DCA4B7CFC1877537768C18A7AB7BE8866C6FD9 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics/<ContinuousHapticsCoroutine>d__38::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CContinuousHapticsCoroutineU3Ed__38_MoveNext_mB05D03D0CEA6E67203D782F92F64CDAACAE6E53A (U3CContinuousHapticsCoroutineU3Ed__38_t65DCA4B7CFC1877537768C18A7AB7BE8866C6FD9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMVibrationManager_tB11EBD05CD3C1FCD39B3313F1256138262E32001_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4 * V_1 = NULL;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float G_B6_0 = 0.0f;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4 * L_1 = __this->get_U3CU3E4__this_2();
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_00d1;
		}
	}
	{
		return (bool)0;
	}

IL_001a:
	{
		__this->set_U3CU3E1__state_0((-1));
		// _continuousStartedAt = (Timescale == Timescales.ScaledTime) ? Time.time : Time.unscaledTime;
		MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4 * L_4 = V_1;
		NullCheck(L_4);
		int32_t L_5 = L_4->get_Timescale_57();
		if (!L_5)
		{
			goto IL_0030;
		}
	}
	{
		float L_6;
		L_6 = Time_get_unscaledTime_m85A3479E3D78D05FEDEEFEF36944AC5EF9B31258(/*hidden argument*/NULL);
		G_B6_0 = L_6;
		goto IL_0035;
	}

IL_0030:
	{
		float L_7;
		L_7 = Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844(/*hidden argument*/NULL);
		G_B6_0 = L_7;
	}

IL_0035:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4_il2cpp_TypeInfo_var);
		((MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4_StaticFields*)il2cpp_codegen_static_fields_for(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4_il2cpp_TypeInfo_var))->set__continuousStartedAt_61(G_B6_0);
		// _continuousPlaying = true;
		((MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4_StaticFields*)il2cpp_codegen_static_fields_for(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4_il2cpp_TypeInfo_var))->set__continuousPlaying_60((bool)1);
		// float elapsedTime = ComputeElapsedTime();
		MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4 * L_8 = V_1;
		NullCheck(L_8);
		float L_9;
		L_9 = VirtFuncInvoker0< float >::Invoke(40 /* System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::ComputeElapsedTime() */, L_8);
		__this->set_U3CelapsedTimeU3E5__2_3(L_9);
		// MMVibrationManager.ContinuousHaptic(InitialContinuousIntensity, InitialContinuousSharpness, ContinuousDuration, HapticTypes.Success, this);
		MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4 * L_10 = V_1;
		NullCheck(L_10);
		float L_11 = L_10->get_InitialContinuousIntensity_42();
		MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4 * L_12 = V_1;
		NullCheck(L_12);
		float L_13 = L_12->get_InitialContinuousSharpness_44();
		MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4 * L_14 = V_1;
		NullCheck(L_14);
		float L_15 = L_14->get_ContinuousDuration_46();
		MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4 * L_16 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tB11EBD05CD3C1FCD39B3313F1256138262E32001_il2cpp_TypeInfo_var);
		MMVibrationManager_ContinuousHaptic_mF6819043B22C8734BB3F57528C39ED84A7F481FC(L_11, L_13, L_15, 1, L_16, (bool)0, (-1), (bool)0, (bool)1, /*hidden argument*/NULL);
		goto IL_00d8;
	}

IL_006b:
	{
		// elapsedTime = ComputeElapsedTime();
		MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4 * L_17 = V_1;
		NullCheck(L_17);
		float L_18;
		L_18 = VirtFuncInvoker0< float >::Invoke(40 /* System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::ComputeElapsedTime() */, L_17);
		__this->set_U3CelapsedTimeU3E5__2_3(L_18);
		// float remappedTime = Remap(elapsedTime, 0f, ContinuousDuration, 0f, 1f);
		float L_19 = __this->get_U3CelapsedTimeU3E5__2_3();
		MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4 * L_20 = V_1;
		NullCheck(L_20);
		float L_21 = L_20->get_ContinuousDuration_46();
		IL2CPP_RUNTIME_CLASS_INIT(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4_il2cpp_TypeInfo_var);
		float L_22;
		L_22 = MMFeedbackHaptics_Remap_m86D254DD73ED991398E7B74A4C7B8559E0963A5E(L_19, (0.0f), L_21, (0.0f), (1.0f), /*hidden argument*/NULL);
		V_2 = L_22;
		// float intensity = ContinuousIntensityCurve.Evaluate(remappedTime);
		MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4 * L_23 = V_1;
		NullCheck(L_23);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_24 = L_23->get_ContinuousIntensityCurve_43();
		float L_25 = V_2;
		NullCheck(L_24);
		float L_26;
		L_26 = AnimationCurve_Evaluate_m1248B5B167F1FFFDC847A08C56B7D63B32311E6A(L_24, L_25, /*hidden argument*/NULL);
		// float sharpness = ContinuousSharpnessCurve.Evaluate(remappedTime);
		MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4 * L_27 = V_1;
		NullCheck(L_27);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_28 = L_27->get_ContinuousSharpnessCurve_45();
		float L_29 = V_2;
		NullCheck(L_28);
		float L_30;
		L_30 = AnimationCurve_Evaluate_m1248B5B167F1FFFDC847A08C56B7D63B32311E6A(L_28, L_29, /*hidden argument*/NULL);
		V_3 = L_30;
		// MMVibrationManager.UpdateContinuousHaptic(intensity, sharpness, true);
		float L_31 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tB11EBD05CD3C1FCD39B3313F1256138262E32001_il2cpp_TypeInfo_var);
		MMVibrationManager_UpdateContinuousHaptic_mC6F1C52965D1CB05CB6782E7B1264BA9244FD5DC(L_26, L_31, (bool)1, (-1), (bool)0, /*hidden argument*/NULL);
		// if (AllowRumble)
		MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4 * L_32 = V_1;
		NullCheck(L_32);
		bool L_33 = L_32->get_AllowRumble_58();
		// yield return null;
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_00d1:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_00d8:
	{
		// while (_continuousPlaying && (elapsedTime < ContinuousDuration))
		IL2CPP_RUNTIME_CLASS_INIT(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4_il2cpp_TypeInfo_var);
		bool L_34 = ((MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4_StaticFields*)il2cpp_codegen_static_fields_for(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4_il2cpp_TypeInfo_var))->get__continuousPlaying_60();
		if (!L_34)
		{
			goto IL_00f0;
		}
	}
	{
		float L_35 = __this->get_U3CelapsedTimeU3E5__2_3();
		MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4 * L_36 = V_1;
		NullCheck(L_36);
		float L_37 = L_36->get_ContinuousDuration_46();
		if ((((float)L_35) < ((float)L_37)))
		{
			goto IL_006b;
		}
	}

IL_00f0:
	{
		// if (_continuousPlaying)
		IL2CPP_RUNTIME_CLASS_INIT(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4_il2cpp_TypeInfo_var);
		bool L_38 = ((MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4_StaticFields*)il2cpp_codegen_static_fields_for(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4_il2cpp_TypeInfo_var))->get__continuousPlaying_60();
		if (!L_38)
		{
			goto IL_0108;
		}
	}
	{
		// _continuousPlaying = false;
		IL2CPP_RUNTIME_CLASS_INIT(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4_il2cpp_TypeInfo_var);
		((MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4_StaticFields*)il2cpp_codegen_static_fields_for(MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4_il2cpp_TypeInfo_var))->set__continuousPlaying_60((bool)0);
		// MMVibrationManager.StopContinuousHaptic(AllowRumble);
		MMFeedbackHaptics_t35B21689DCB01EBC054EAC1DA2DF99E3179F54C4 * L_39 = V_1;
		NullCheck(L_39);
		bool L_40 = L_39->get_AllowRumble_58();
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tB11EBD05CD3C1FCD39B3313F1256138262E32001_il2cpp_TypeInfo_var);
		MMVibrationManager_StopContinuousHaptic_mBC0A5B8BB4889C60D9BAF9E52FDA52786F6C9DA7(L_40, /*hidden argument*/NULL);
	}

IL_0108:
	{
		// }
		return (bool)0;
	}
}
// System.Object MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics/<ContinuousHapticsCoroutine>d__38::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CContinuousHapticsCoroutineU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEE0FF6509222F8F64F5E91BE269030C2C034A0AB (U3CContinuousHapticsCoroutineU3Ed__38_t65DCA4B7CFC1877537768C18A7AB7BE8866C6FD9 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics/<ContinuousHapticsCoroutine>d__38::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CContinuousHapticsCoroutineU3Ed__38_System_Collections_IEnumerator_Reset_mD2AAB00CAD47726A832F2DAE13EF8651AAA1CFD0 (U3CContinuousHapticsCoroutineU3Ed__38_t65DCA4B7CFC1877537768C18A7AB7BE8866C6FD9 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CContinuousHapticsCoroutineU3Ed__38_System_Collections_IEnumerator_Reset_mD2AAB00CAD47726A832F2DAE13EF8651AAA1CFD0_RuntimeMethod_var)));
	}
}
// System.Object MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics/<ContinuousHapticsCoroutine>d__38::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CContinuousHapticsCoroutineU3Ed__38_System_Collections_IEnumerator_get_Current_mEAC49B3532158B78993DF5C64BDAA742D8BA0AA1 (U3CContinuousHapticsCoroutineU3Ed__38_t65DCA4B7CFC1877537768C18A7AB7BE8866C6FD9 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
