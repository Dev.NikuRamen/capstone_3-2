﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Single MoreMountains.FeedbacksForThirdParty.MMCameraZoom::GetTime()
extern void MMCameraZoom_GetTime_m2068491132423511BA20DAC2D89A54D300B1A864 (void);
// 0x00000002 System.Single MoreMountains.FeedbacksForThirdParty.MMCameraZoom::GetDeltaTime()
extern void MMCameraZoom_GetDeltaTime_m7847DB5E04392CCE331C2AA265E9FB8631ADE613 (void);
// 0x00000003 MoreMountains.Feedbacks.TimescaleModes MoreMountains.FeedbacksForThirdParty.MMCameraZoom::get_TimescaleMode()
extern void MMCameraZoom_get_TimescaleMode_m17A6D072080C167A32D7E1099DDB509091503DD4 (void);
// 0x00000004 System.Void MoreMountains.FeedbacksForThirdParty.MMCameraZoom::set_TimescaleMode(MoreMountains.Feedbacks.TimescaleModes)
extern void MMCameraZoom_set_TimescaleMode_m550264151A96573D6AF4413269FD40131015EA9B (void);
// 0x00000005 System.Void MoreMountains.FeedbacksForThirdParty.MMCameraZoom::Awake()
extern void MMCameraZoom_Awake_m194DF77FAC0B8B443AA46EDF6BF0A96476017AEF (void);
// 0x00000006 System.Void MoreMountains.FeedbacksForThirdParty.MMCameraZoom::Update()
extern void MMCameraZoom_Update_m84A25601A88F888A3A809ADD0880266EF1CB0835 (void);
// 0x00000007 System.Void MoreMountains.FeedbacksForThirdParty.MMCameraZoom::Zoom(MoreMountains.Feedbacks.MMCameraZoomModes,System.Single,System.Single,System.Single,System.Boolean,System.Boolean)
extern void MMCameraZoom_Zoom_mA05EDB60F7F1222A0C644ABBB858D3011A4830A9 (void);
// 0x00000008 System.Void MoreMountains.FeedbacksForThirdParty.MMCameraZoom::TestZoom()
extern void MMCameraZoom_TestZoom_mC31D0FD024743CB4383AF2CF073663BD094D0913 (void);
// 0x00000009 System.Void MoreMountains.FeedbacksForThirdParty.MMCameraZoom::OnCameraZoomEvent(MoreMountains.Feedbacks.MMCameraZoomModes,System.Single,System.Single,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean)
extern void MMCameraZoom_OnCameraZoomEvent_mF50C3D3681E80C550701C1F006AB9841E49751D1 (void);
// 0x0000000A System.Void MoreMountains.FeedbacksForThirdParty.MMCameraZoom::OnEnable()
extern void MMCameraZoom_OnEnable_m12E2D1AC820A4919A1EAEFAC8E2F47AB6C9A00EC (void);
// 0x0000000B System.Void MoreMountains.FeedbacksForThirdParty.MMCameraZoom::OnDisable()
extern void MMCameraZoom_OnDisable_m53129CF3FDB98B162CE7CDFA8172EE75CC1D4995 (void);
// 0x0000000C System.Void MoreMountains.FeedbacksForThirdParty.MMCameraZoom::.ctor()
extern void MMCameraZoom__ctor_m24634315897A63754BB1C388BC0CF3D47FDF114F (void);
// 0x0000000D UnityEngine.GameObject MoreMountains.Feedbacks.MMFeedback::get_Owner()
extern void MMFeedback_get_Owner_m63F9C3D5969DC9FF3A9F0A53D1F6BC0EB3532701 (void);
// 0x0000000E System.Void MoreMountains.Feedbacks.MMFeedback::set_Owner(UnityEngine.GameObject)
extern void MMFeedback_set_Owner_m5E2F72382AD898B827BFDE0FEC443B6713040483 (void);
// 0x0000000F System.Collections.IEnumerator MoreMountains.Feedbacks.MMFeedback::get_Pause()
extern void MMFeedback_get_Pause_mC2DE89B84D2AD70BF9546DD90089551498EE54F1 (void);
// 0x00000010 System.Boolean MoreMountains.Feedbacks.MMFeedback::get_HoldingPause()
extern void MMFeedback_get_HoldingPause_m66061AEBE8C9C03193790BEE3B7DA3FB6830A4A6 (void);
// 0x00000011 System.Boolean MoreMountains.Feedbacks.MMFeedback::get_LooperPause()
extern void MMFeedback_get_LooperPause_m902D6A894F4EF3EFE4638B580A8F0449F5BF4F8A (void);
// 0x00000012 System.Boolean MoreMountains.Feedbacks.MMFeedback::get_ScriptDrivenPause()
extern void MMFeedback_get_ScriptDrivenPause_m85C6C86000E39C09F73DC8E060C05B1BE31CA475 (void);
// 0x00000013 System.Void MoreMountains.Feedbacks.MMFeedback::set_ScriptDrivenPause(System.Boolean)
extern void MMFeedback_set_ScriptDrivenPause_m54FE672636C77CDEDD86D09EB6940C14C01E91F3 (void);
// 0x00000014 System.Single MoreMountains.Feedbacks.MMFeedback::get_ScriptDrivenPauseAutoResume()
extern void MMFeedback_get_ScriptDrivenPauseAutoResume_m3A8E31CE15BE39FFDF957436A6AD7B04A679BF03 (void);
// 0x00000015 System.Void MoreMountains.Feedbacks.MMFeedback::set_ScriptDrivenPauseAutoResume(System.Single)
extern void MMFeedback_set_ScriptDrivenPauseAutoResume_m4044262829AC46F5443CBD5D5FC5535FF49004C8 (void);
// 0x00000016 System.Boolean MoreMountains.Feedbacks.MMFeedback::get_LooperStart()
extern void MMFeedback_get_LooperStart_m32F59753E10FE4B3A105275C7A6F3229C1279959 (void);
// 0x00000017 System.Boolean MoreMountains.Feedbacks.MMFeedback::get_InCooldown()
extern void MMFeedback_get_InCooldown_mD63F98DAF3FA28B2A69CE8A7628A517B04D0C1BE (void);
// 0x00000018 System.Single MoreMountains.Feedbacks.MMFeedback::get_FeedbackTime()
extern void MMFeedback_get_FeedbackTime_m859557BFFE2B1372640EBF091A04110D905237EA (void);
// 0x00000019 System.Single MoreMountains.Feedbacks.MMFeedback::get_FeedbackDeltaTime()
extern void MMFeedback_get_FeedbackDeltaTime_m3EF4F11444CD2F00CF1E656478EFC2EC0DF541E7 (void);
// 0x0000001A System.Single MoreMountains.Feedbacks.MMFeedback::get_TotalDuration()
extern void MMFeedback_get_TotalDuration_mBE68C3225F4D5C260128E0B017A20780DED656B9 (void);
// 0x0000001B System.Single MoreMountains.Feedbacks.MMFeedback::get_FeedbackStartedAt()
extern void MMFeedback_get_FeedbackStartedAt_m9D55849F3AE573E152B40B81601BDD63DAB70854 (void);
// 0x0000001C System.Single MoreMountains.Feedbacks.MMFeedback::get_FeedbackDuration()
extern void MMFeedback_get_FeedbackDuration_m779C7BCA789A12C27B51FA16DBAEC13FA14CE4F0 (void);
// 0x0000001D System.Void MoreMountains.Feedbacks.MMFeedback::set_FeedbackDuration(System.Single)
extern void MMFeedback_set_FeedbackDuration_m041777E9B351A2FCFA282CCCC0054B6ECD9D8A55 (void);
// 0x0000001E System.Boolean MoreMountains.Feedbacks.MMFeedback::get_FeedbackPlaying()
extern void MMFeedback_get_FeedbackPlaying_m6DDD0E0F5ECA73755FDCDFE3C26B980B755B704B (void);
// 0x0000001F System.Void MoreMountains.Feedbacks.MMFeedback::OnEnable()
extern void MMFeedback_OnEnable_m0AD9E20E74DCE5FF5F61C4FB851AFE61523E7B43 (void);
// 0x00000020 System.Void MoreMountains.Feedbacks.MMFeedback::Initialization(UnityEngine.GameObject)
extern void MMFeedback_Initialization_m46BB5D333ADB8DFCDC08D574D81874ADFB1ED754 (void);
// 0x00000021 System.Void MoreMountains.Feedbacks.MMFeedback::Play(UnityEngine.Vector3,System.Single)
extern void MMFeedback_Play_m000C46C50EB43FB7602F8820C7C0AB47912A71CA (void);
// 0x00000022 System.Collections.IEnumerator MoreMountains.Feedbacks.MMFeedback::PlayCoroutine(UnityEngine.Vector3,System.Single)
extern void MMFeedback_PlayCoroutine_m3F88EED83FC1665F5E2D6ACB54C45F84AA70D8B1 (void);
// 0x00000023 System.Void MoreMountains.Feedbacks.MMFeedback::RegularPlay(UnityEngine.Vector3,System.Single)
extern void MMFeedback_RegularPlay_m72B414DA67FD949B77A325BA76871CF10A34D944 (void);
// 0x00000024 System.Collections.IEnumerator MoreMountains.Feedbacks.MMFeedback::InfinitePlay(UnityEngine.Vector3,System.Single)
extern void MMFeedback_InfinitePlay_m0F1BEA028328D998EE7EEBB806B08408248E7F4F (void);
// 0x00000025 System.Collections.IEnumerator MoreMountains.Feedbacks.MMFeedback::RepeatedPlay(UnityEngine.Vector3,System.Single)
extern void MMFeedback_RepeatedPlay_mCF60A914ECD12385697ED9CFEF0B4EB223F35855 (void);
// 0x00000026 System.Collections.IEnumerator MoreMountains.Feedbacks.MMFeedback::SequenceCoroutine(UnityEngine.Vector3,System.Single)
extern void MMFeedback_SequenceCoroutine_m1EB54B24BD156A2BC19C0B2D98760437AAAB1CD6 (void);
// 0x00000027 System.Void MoreMountains.Feedbacks.MMFeedback::Stop(UnityEngine.Vector3,System.Single)
extern void MMFeedback_Stop_m95B7701199FE07D732A17EC9558E5B08276F9839 (void);
// 0x00000028 System.Void MoreMountains.Feedbacks.MMFeedback::ResetFeedback()
extern void MMFeedback_ResetFeedback_mBABB311F16940183E4C74724CC4FDA3EB5D3F880 (void);
// 0x00000029 System.Void MoreMountains.Feedbacks.MMFeedback::SetSequence(MoreMountains.Feedbacks.MMSequence)
extern void MMFeedback_SetSequence_mD700BF49C2780F56592C25A6E23A3671852BE123 (void);
// 0x0000002A System.Void MoreMountains.Feedbacks.MMFeedback::SetDelayBetweenRepeats(System.Single)
extern void MMFeedback_SetDelayBetweenRepeats_m873523C356931E3DCF1037D713592FD42D770FED (void);
// 0x0000002B System.Void MoreMountains.Feedbacks.MMFeedback::SetInitialDelay(System.Single)
extern void MMFeedback_SetInitialDelay_m8671E6147069DBC1F5DA94A318C511B46172D1E9 (void);
// 0x0000002C System.Single MoreMountains.Feedbacks.MMFeedback::ApplyDirection(System.Single)
extern void MMFeedback_ApplyDirection_m31EF74481694DD129B1B74B6E2109CB4BE8E16E6 (void);
// 0x0000002D System.Boolean MoreMountains.Feedbacks.MMFeedback::get_NormalPlayDirection()
extern void MMFeedback_get_NormalPlayDirection_m44B2BC2A146B5B4F7DAEB23E8D8B438411F6AD8B (void);
// 0x0000002E System.Boolean MoreMountains.Feedbacks.MMFeedback::get_ShouldPlayInThisSequenceDirection()
extern void MMFeedback_get_ShouldPlayInThisSequenceDirection_m34D1046B1515B51EA22D1D94C0A290B68130ED35 (void);
// 0x0000002F System.Single MoreMountains.Feedbacks.MMFeedback::get_FinalNormalizedTime()
extern void MMFeedback_get_FinalNormalizedTime_m2145A7381BF90871988EFDFBE1565D2AA6245D72 (void);
// 0x00000030 System.Single MoreMountains.Feedbacks.MMFeedback::ApplyTimeMultiplier(System.Single)
extern void MMFeedback_ApplyTimeMultiplier_mE0954046F0D9F7C571D0FDF16A192015476A5255 (void);
// 0x00000031 System.Void MoreMountains.Feedbacks.MMFeedback::CustomInitialization(UnityEngine.GameObject)
extern void MMFeedback_CustomInitialization_mAC357FEAB8BCCF8CC643C52735DD8353A8266108 (void);
// 0x00000032 System.Void MoreMountains.Feedbacks.MMFeedback::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
// 0x00000033 System.Void MoreMountains.Feedbacks.MMFeedback::CustomStopFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedback_CustomStopFeedback_m637BE3F6114F85830A2E7073DEEF05B95CA79A83 (void);
// 0x00000034 System.Void MoreMountains.Feedbacks.MMFeedback::CustomReset()
extern void MMFeedback_CustomReset_mE76A604F77F74907462823CD9DD8C637154C4E62 (void);
// 0x00000035 System.Void MoreMountains.Feedbacks.MMFeedback::.ctor()
extern void MMFeedback__ctor_m3A0FE0C8FEDD2D759EC31C4432C65DBD4B1A06A0 (void);
// 0x00000036 System.Void MoreMountains.Feedbacks.MMFeedback/<PlayCoroutine>d__58::.ctor(System.Int32)
extern void U3CPlayCoroutineU3Ed__58__ctor_mC9C5017FD78844E0F590C43CFD73A580B1434709 (void);
// 0x00000037 System.Void MoreMountains.Feedbacks.MMFeedback/<PlayCoroutine>d__58::System.IDisposable.Dispose()
extern void U3CPlayCoroutineU3Ed__58_System_IDisposable_Dispose_m44DD2EA42BC393E7D82F11A428D92D5E75C1D181 (void);
// 0x00000038 System.Boolean MoreMountains.Feedbacks.MMFeedback/<PlayCoroutine>d__58::MoveNext()
extern void U3CPlayCoroutineU3Ed__58_MoveNext_m7423612FC0742A009203CDFA2DDA1F71CBFAAC3A (void);
// 0x00000039 System.Object MoreMountains.Feedbacks.MMFeedback/<PlayCoroutine>d__58::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPlayCoroutineU3Ed__58_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9E74D0E47E0AE1A5D999511AFE5A066BD0A8C320 (void);
// 0x0000003A System.Void MoreMountains.Feedbacks.MMFeedback/<PlayCoroutine>d__58::System.Collections.IEnumerator.Reset()
extern void U3CPlayCoroutineU3Ed__58_System_Collections_IEnumerator_Reset_m8538C8448939CDC0819C8DB13A5C794EE27AD950 (void);
// 0x0000003B System.Object MoreMountains.Feedbacks.MMFeedback/<PlayCoroutine>d__58::System.Collections.IEnumerator.get_Current()
extern void U3CPlayCoroutineU3Ed__58_System_Collections_IEnumerator_get_Current_mE2352A87DE9F7E54897223BFAA151AB92929FDC0 (void);
// 0x0000003C System.Void MoreMountains.Feedbacks.MMFeedback/<InfinitePlay>d__60::.ctor(System.Int32)
extern void U3CInfinitePlayU3Ed__60__ctor_mA73B1C0ACC3B6FEA9CA35F0C970E47DF45402430 (void);
// 0x0000003D System.Void MoreMountains.Feedbacks.MMFeedback/<InfinitePlay>d__60::System.IDisposable.Dispose()
extern void U3CInfinitePlayU3Ed__60_System_IDisposable_Dispose_m55FBB56BFDAE9EECD3BA8FC2B8BF5A703B81CECD (void);
// 0x0000003E System.Boolean MoreMountains.Feedbacks.MMFeedback/<InfinitePlay>d__60::MoveNext()
extern void U3CInfinitePlayU3Ed__60_MoveNext_m8EAD1A52935D311ACE8C34B78317074E7BCBC557 (void);
// 0x0000003F System.Object MoreMountains.Feedbacks.MMFeedback/<InfinitePlay>d__60::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CInfinitePlayU3Ed__60_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m365B5009C628E89C60865D59235B8A8A3F833589 (void);
// 0x00000040 System.Void MoreMountains.Feedbacks.MMFeedback/<InfinitePlay>d__60::System.Collections.IEnumerator.Reset()
extern void U3CInfinitePlayU3Ed__60_System_Collections_IEnumerator_Reset_mC54B04E994A532C5AEEE35019ABEA377B2680353 (void);
// 0x00000041 System.Object MoreMountains.Feedbacks.MMFeedback/<InfinitePlay>d__60::System.Collections.IEnumerator.get_Current()
extern void U3CInfinitePlayU3Ed__60_System_Collections_IEnumerator_get_Current_mC7CC84D5220097B3FC374576454BA030424C4727 (void);
// 0x00000042 System.Void MoreMountains.Feedbacks.MMFeedback/<RepeatedPlay>d__61::.ctor(System.Int32)
extern void U3CRepeatedPlayU3Ed__61__ctor_m38C82BD8EBBE5CB0B105C4F8FE4EC8B2F87042DB (void);
// 0x00000043 System.Void MoreMountains.Feedbacks.MMFeedback/<RepeatedPlay>d__61::System.IDisposable.Dispose()
extern void U3CRepeatedPlayU3Ed__61_System_IDisposable_Dispose_m06EFD40CE9952851BBB70ECA63A56C9257FE3E14 (void);
// 0x00000044 System.Boolean MoreMountains.Feedbacks.MMFeedback/<RepeatedPlay>d__61::MoveNext()
extern void U3CRepeatedPlayU3Ed__61_MoveNext_m568C94D2FA3E1E8F266A98246E2745F10C1AC2D3 (void);
// 0x00000045 System.Object MoreMountains.Feedbacks.MMFeedback/<RepeatedPlay>d__61::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRepeatedPlayU3Ed__61_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3954ADB6A68EAC2027B93A1B68844D1B47A0328D (void);
// 0x00000046 System.Void MoreMountains.Feedbacks.MMFeedback/<RepeatedPlay>d__61::System.Collections.IEnumerator.Reset()
extern void U3CRepeatedPlayU3Ed__61_System_Collections_IEnumerator_Reset_m35D061548946F2AE3873A189035B44EDED0A5875 (void);
// 0x00000047 System.Object MoreMountains.Feedbacks.MMFeedback/<RepeatedPlay>d__61::System.Collections.IEnumerator.get_Current()
extern void U3CRepeatedPlayU3Ed__61_System_Collections_IEnumerator_get_Current_m20D38276E19684B89A6037B22F70136D887237E9 (void);
// 0x00000048 System.Void MoreMountains.Feedbacks.MMFeedback/<SequenceCoroutine>d__62::.ctor(System.Int32)
extern void U3CSequenceCoroutineU3Ed__62__ctor_mAD65179E5DABB6CEF15ABA9E5B8C3F31443AC10C (void);
// 0x00000049 System.Void MoreMountains.Feedbacks.MMFeedback/<SequenceCoroutine>d__62::System.IDisposable.Dispose()
extern void U3CSequenceCoroutineU3Ed__62_System_IDisposable_Dispose_m0BEB40525A7B844D42E3C19695E60059F01BA53A (void);
// 0x0000004A System.Boolean MoreMountains.Feedbacks.MMFeedback/<SequenceCoroutine>d__62::MoveNext()
extern void U3CSequenceCoroutineU3Ed__62_MoveNext_m3C38C93033DF2855AF968B7E71FD21B498CA1032 (void);
// 0x0000004B System.Object MoreMountains.Feedbacks.MMFeedback/<SequenceCoroutine>d__62::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSequenceCoroutineU3Ed__62_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m902F740D5A7D238FA3E1AA2D1C9ED7CD078DAEF7 (void);
// 0x0000004C System.Void MoreMountains.Feedbacks.MMFeedback/<SequenceCoroutine>d__62::System.Collections.IEnumerator.Reset()
extern void U3CSequenceCoroutineU3Ed__62_System_Collections_IEnumerator_Reset_m987A51776BCB2B27E87BB0915A3A41C6E9374EC7 (void);
// 0x0000004D System.Object MoreMountains.Feedbacks.MMFeedback/<SequenceCoroutine>d__62::System.Collections.IEnumerator.get_Current()
extern void U3CSequenceCoroutineU3Ed__62_System_Collections_IEnumerator_get_Current_m64C0C9DA602E6222F01E7B04622A7DAEEB6293AF (void);
// 0x0000004E System.Void MoreMountains.Feedbacks.MMFeedbackTiming::.ctor()
extern void MMFeedbackTiming__ctor_m4787969CCD5E69414860BF69AB073363C35F3505 (void);
// 0x0000004F System.Boolean MoreMountains.Feedbacks.MMFeedbacks::get_IsPlaying()
extern void MMFeedbacks_get_IsPlaying_m3C443D7BB441842F86D0C290899BE5F2E59A2A10 (void);
// 0x00000050 System.Void MoreMountains.Feedbacks.MMFeedbacks::set_IsPlaying(System.Boolean)
extern void MMFeedbacks_set_IsPlaying_mAEEBF8B704BC482599E1B1C407A5B70310D37208 (void);
// 0x00000051 System.Boolean MoreMountains.Feedbacks.MMFeedbacks::get_InScriptDrivenPause()
extern void MMFeedbacks_get_InScriptDrivenPause_m356B03C06DD785C0578AF32266F10997350A7752 (void);
// 0x00000052 System.Void MoreMountains.Feedbacks.MMFeedbacks::set_InScriptDrivenPause(System.Boolean)
extern void MMFeedbacks_set_InScriptDrivenPause_m0FA29E2487517BE4D14A64D0B29C3C8F12331728 (void);
// 0x00000053 System.Boolean MoreMountains.Feedbacks.MMFeedbacks::get_ContainsLoop()
extern void MMFeedbacks_get_ContainsLoop_m64535C00630AEA3B035B12B0739F71924C73A553 (void);
// 0x00000054 System.Void MoreMountains.Feedbacks.MMFeedbacks::set_ContainsLoop(System.Boolean)
extern void MMFeedbacks_set_ContainsLoop_m40799578BB397910F523CEF4B849BB0E3AE8F38C (void);
// 0x00000055 System.Boolean MoreMountains.Feedbacks.MMFeedbacks::get_ShouldRevertOnNextPlay()
extern void MMFeedbacks_get_ShouldRevertOnNextPlay_m44C356D9B5D61FE7BE0997F8D40006D53173AF0F (void);
// 0x00000056 System.Void MoreMountains.Feedbacks.MMFeedbacks::set_ShouldRevertOnNextPlay(System.Boolean)
extern void MMFeedbacks_set_ShouldRevertOnNextPlay_m6CAD5F633C7316C193838BBC384FC6CE1124961C (void);
// 0x00000057 System.Single MoreMountains.Feedbacks.MMFeedbacks::get_TotalDuration()
extern void MMFeedbacks_get_TotalDuration_m5DD5C14AE3B09159B8ECC8B49315F7DB586C7FC0 (void);
// 0x00000058 System.Void MoreMountains.Feedbacks.MMFeedbacks::Awake()
extern void MMFeedbacks_Awake_m2E44C906A44F5491827A235182A6A6CB6B0BC795 (void);
// 0x00000059 System.Void MoreMountains.Feedbacks.MMFeedbacks::Start()
extern void MMFeedbacks_Start_mE74BADC5EC5F6CA76A95A03A60F389789B62BFC0 (void);
// 0x0000005A System.Void MoreMountains.Feedbacks.MMFeedbacks::OnEnable()
extern void MMFeedbacks_OnEnable_mCD331E81D3B043EF2F77354CD3D47CCBA1E69887 (void);
// 0x0000005B System.Void MoreMountains.Feedbacks.MMFeedbacks::Initialization()
extern void MMFeedbacks_Initialization_m5F3790C04A488F928BAB90B02263764468C8800F (void);
// 0x0000005C System.Void MoreMountains.Feedbacks.MMFeedbacks::Initialization(UnityEngine.GameObject)
extern void MMFeedbacks_Initialization_m862463CC3B0FCFE5A6850DDE14EDBBD9D2A2C97D (void);
// 0x0000005D System.Void MoreMountains.Feedbacks.MMFeedbacks::PlayFeedbacks()
extern void MMFeedbacks_PlayFeedbacks_m0AF9AF19A7F328FAE775FB2C97098EAC314CBFB6 (void);
// 0x0000005E System.Void MoreMountains.Feedbacks.MMFeedbacks::PlayFeedbacks(UnityEngine.Vector3,System.Single,System.Boolean)
extern void MMFeedbacks_PlayFeedbacks_m0BB902E0BC8DF75635DC4638F57541E6EFD22498 (void);
// 0x0000005F System.Void MoreMountains.Feedbacks.MMFeedbacks::PlayFeedbacksInReverse()
extern void MMFeedbacks_PlayFeedbacksInReverse_m54CE2D030E7208C4A6AD1892D37ABBA540DFC636 (void);
// 0x00000060 System.Void MoreMountains.Feedbacks.MMFeedbacks::PlayFeedbacksInReverse(UnityEngine.Vector3,System.Single,System.Boolean)
extern void MMFeedbacks_PlayFeedbacksInReverse_mB6E69A04B86463C902EA1E212D4DB7748405DCD4 (void);
// 0x00000061 System.Void MoreMountains.Feedbacks.MMFeedbacks::PlayFeedbacksOnlyIfReversed()
extern void MMFeedbacks_PlayFeedbacksOnlyIfReversed_m812C631AB362A3CD01B1A990DCE297C2C9515E77 (void);
// 0x00000062 System.Void MoreMountains.Feedbacks.MMFeedbacks::PlayFeedbacksOnlyIfReversed(UnityEngine.Vector3,System.Single,System.Boolean)
extern void MMFeedbacks_PlayFeedbacksOnlyIfReversed_mFA70A32F3A1DE031A2228AB7F511ABA08E769243 (void);
// 0x00000063 System.Void MoreMountains.Feedbacks.MMFeedbacks::PlayFeedbacksOnlyIfNormalDirection()
extern void MMFeedbacks_PlayFeedbacksOnlyIfNormalDirection_m5785FE8FE9CDE22AF0CF3B5895C66AD2ECC43057 (void);
// 0x00000064 System.Void MoreMountains.Feedbacks.MMFeedbacks::PlayFeedbacksOnlyIfNormalDirection(UnityEngine.Vector3,System.Single,System.Boolean)
extern void MMFeedbacks_PlayFeedbacksOnlyIfNormalDirection_m77E30A22360FB55E4BE2A906A93EE0AF78310A44 (void);
// 0x00000065 System.Collections.IEnumerator MoreMountains.Feedbacks.MMFeedbacks::PlayFeedbacksCoroutine(UnityEngine.Vector3,System.Single,System.Boolean)
extern void MMFeedbacks_PlayFeedbacksCoroutine_m2FE237BD4BB72E0B2C8CA2634FEA72AEAC9FD661 (void);
// 0x00000066 System.Void MoreMountains.Feedbacks.MMFeedbacks::PlayFeedbacksInternal(UnityEngine.Vector3,System.Single,System.Boolean)
extern void MMFeedbacks_PlayFeedbacksInternal_mCA46E8DB621294C797EEF0D88851B19286C732DC (void);
// 0x00000067 System.Void MoreMountains.Feedbacks.MMFeedbacks::PreparePlay(UnityEngine.Vector3,System.Single,System.Boolean)
extern void MMFeedbacks_PreparePlay_mAF7F0F42D92A27DC308B2BC28156C73F62C039A3 (void);
// 0x00000068 System.Void MoreMountains.Feedbacks.MMFeedbacks::PlayAllFeedbacks(UnityEngine.Vector3,System.Single,System.Boolean)
extern void MMFeedbacks_PlayAllFeedbacks_m0E781BDF8A3624EB97ACD077C753956C9DF0C7B1 (void);
// 0x00000069 System.Collections.IEnumerator MoreMountains.Feedbacks.MMFeedbacks::HandleInitialDelayCo(UnityEngine.Vector3,System.Single,System.Boolean)
extern void MMFeedbacks_HandleInitialDelayCo_mC7B15E61934CCBFDB2E909A6B3672DC58983F716 (void);
// 0x0000006A System.Void MoreMountains.Feedbacks.MMFeedbacks::Update()
extern void MMFeedbacks_Update_mA73C2E16AEA8D193037FEE8152C5724727FD0815 (void);
// 0x0000006B System.Collections.IEnumerator MoreMountains.Feedbacks.MMFeedbacks::PausedFeedbacksCo(UnityEngine.Vector3,System.Single)
extern void MMFeedbacks_PausedFeedbacksCo_mD4824A4FF58457AF9E6A4354195AC923422DE198 (void);
// 0x0000006C System.Void MoreMountains.Feedbacks.MMFeedbacks::StopFeedbacks()
extern void MMFeedbacks_StopFeedbacks_m41AABDDB5B9DAC8DA6BBF61F67D0375C8C0ED455 (void);
// 0x0000006D System.Void MoreMountains.Feedbacks.MMFeedbacks::StopFeedbacks(System.Boolean)
extern void MMFeedbacks_StopFeedbacks_mE8CA30B713B29DEAC2371D8CF4A6B99D18282D23 (void);
// 0x0000006E System.Void MoreMountains.Feedbacks.MMFeedbacks::StopFeedbacks(UnityEngine.Vector3,System.Single,System.Boolean)
extern void MMFeedbacks_StopFeedbacks_m5A45CD3B04CC076018BCD599C1E8252DABC42E1F (void);
// 0x0000006F System.Void MoreMountains.Feedbacks.MMFeedbacks::ResetFeedbacks()
extern void MMFeedbacks_ResetFeedbacks_mECF6867D1FD85336ABD59F6875CE41168017396B (void);
// 0x00000070 System.Void MoreMountains.Feedbacks.MMFeedbacks::Revert()
extern void MMFeedbacks_Revert_mA2876FCFD9A9B44F4A72F254DCF512161ED5F7FE (void);
// 0x00000071 System.Void MoreMountains.Feedbacks.MMFeedbacks::PauseFeedbacks()
extern void MMFeedbacks_PauseFeedbacks_m48FDCCA8544B37337376BCD9DD07EDF3C7D01AC4 (void);
// 0x00000072 System.Void MoreMountains.Feedbacks.MMFeedbacks::ResumeFeedbacks()
extern void MMFeedbacks_ResumeFeedbacks_mFC86928F24E25F551638C24104A7A2C04DFE26D6 (void);
// 0x00000073 System.Void MoreMountains.Feedbacks.MMFeedbacks::CheckForLoops()
extern void MMFeedbacks_CheckForLoops_mB435419B576D22C5D2B9845A772DD3C1A6F20BD8 (void);
// 0x00000074 System.Boolean MoreMountains.Feedbacks.MMFeedbacks::FeedbackCanPlay(MoreMountains.Feedbacks.MMFeedback)
extern void MMFeedbacks_FeedbackCanPlay_m1B8CE132BA0F40691F8FC6FED3C83E6906993E5E (void);
// 0x00000075 System.Void MoreMountains.Feedbacks.MMFeedbacks::ApplyAutoRevert()
extern void MMFeedbacks_ApplyAutoRevert_mE3E5062B711C587BEC7FF625214F3627F7E74B4E (void);
// 0x00000076 System.Single MoreMountains.Feedbacks.MMFeedbacks::ApplyTimeMultiplier(System.Single)
extern void MMFeedbacks_ApplyTimeMultiplier_m222E6077ECF7BB3A3F38033612F4BB37EF880A38 (void);
// 0x00000077 System.Void MoreMountains.Feedbacks.MMFeedbacks::AutoRepair()
extern void MMFeedbacks_AutoRepair_mA532BFC1ECC42AF1534CADA8BC8F4EEB7B852DFA (void);
// 0x00000078 System.Void MoreMountains.Feedbacks.MMFeedbacks::OnDisable()
extern void MMFeedbacks_OnDisable_mDE76F3B1AE108DF402AE21924282488E3858EDB7 (void);
// 0x00000079 System.Void MoreMountains.Feedbacks.MMFeedbacks::OnValidate()
extern void MMFeedbacks_OnValidate_m8F01E4D093BC1F29F040A17E2099110A5B7ECD0B (void);
// 0x0000007A System.Void MoreMountains.Feedbacks.MMFeedbacks::OnDestroy()
extern void MMFeedbacks_OnDestroy_mC30B75631467DF692D696FAC7E99F036F55D186B (void);
// 0x0000007B System.Void MoreMountains.Feedbacks.MMFeedbacks::.ctor()
extern void MMFeedbacks__ctor_mCCC22610F29B910990B179458757258070A2C5C3 (void);
// 0x0000007C System.Void MoreMountains.Feedbacks.MMFeedbacks::.cctor()
extern void MMFeedbacks__cctor_m7FCA76CCD2A3C35199278B8920D6A9B23BECC116 (void);
// 0x0000007D System.Void MoreMountains.Feedbacks.MMFeedbacks/<PlayFeedbacksCoroutine>d__55::.ctor(System.Int32)
extern void U3CPlayFeedbacksCoroutineU3Ed__55__ctor_m2C5134D9A3D7999ACAA5E4C35EB6A42134EC6413 (void);
// 0x0000007E System.Void MoreMountains.Feedbacks.MMFeedbacks/<PlayFeedbacksCoroutine>d__55::System.IDisposable.Dispose()
extern void U3CPlayFeedbacksCoroutineU3Ed__55_System_IDisposable_Dispose_mD50066F28EB32BF9092EF1970941D5AB112C7E40 (void);
// 0x0000007F System.Boolean MoreMountains.Feedbacks.MMFeedbacks/<PlayFeedbacksCoroutine>d__55::MoveNext()
extern void U3CPlayFeedbacksCoroutineU3Ed__55_MoveNext_mBFC0D6143B972825A9204B610916E5011BE32937 (void);
// 0x00000080 System.Object MoreMountains.Feedbacks.MMFeedbacks/<PlayFeedbacksCoroutine>d__55::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPlayFeedbacksCoroutineU3Ed__55_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m34AF67D1655C1355F01BCE2C21DFB171A0E20F13 (void);
// 0x00000081 System.Void MoreMountains.Feedbacks.MMFeedbacks/<PlayFeedbacksCoroutine>d__55::System.Collections.IEnumerator.Reset()
extern void U3CPlayFeedbacksCoroutineU3Ed__55_System_Collections_IEnumerator_Reset_m8D94DB4AEDE5D4AD0866D29E4A90578F88F10079 (void);
// 0x00000082 System.Object MoreMountains.Feedbacks.MMFeedbacks/<PlayFeedbacksCoroutine>d__55::System.Collections.IEnumerator.get_Current()
extern void U3CPlayFeedbacksCoroutineU3Ed__55_System_Collections_IEnumerator_get_Current_m4C1CE11FE20BC30743B93B057235192260FD93D1 (void);
// 0x00000083 System.Void MoreMountains.Feedbacks.MMFeedbacks/<HandleInitialDelayCo>d__59::.ctor(System.Int32)
extern void U3CHandleInitialDelayCoU3Ed__59__ctor_m4FF7EA1DDA7C52CE5D6C45AFE621738D137B257C (void);
// 0x00000084 System.Void MoreMountains.Feedbacks.MMFeedbacks/<HandleInitialDelayCo>d__59::System.IDisposable.Dispose()
extern void U3CHandleInitialDelayCoU3Ed__59_System_IDisposable_Dispose_m7ACAE8CF67073E559A8F6A8C7B661DA37E21D38A (void);
// 0x00000085 System.Boolean MoreMountains.Feedbacks.MMFeedbacks/<HandleInitialDelayCo>d__59::MoveNext()
extern void U3CHandleInitialDelayCoU3Ed__59_MoveNext_mC3656BD0516533DBFA55A45AF2ACCA87545B149C (void);
// 0x00000086 System.Object MoreMountains.Feedbacks.MMFeedbacks/<HandleInitialDelayCo>d__59::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CHandleInitialDelayCoU3Ed__59_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD9929CD4025779BABD62C1105179BD66C7812C0C (void);
// 0x00000087 System.Void MoreMountains.Feedbacks.MMFeedbacks/<HandleInitialDelayCo>d__59::System.Collections.IEnumerator.Reset()
extern void U3CHandleInitialDelayCoU3Ed__59_System_Collections_IEnumerator_Reset_m138C129253DBB87D95D51D9DF97074ECA9F36485 (void);
// 0x00000088 System.Object MoreMountains.Feedbacks.MMFeedbacks/<HandleInitialDelayCo>d__59::System.Collections.IEnumerator.get_Current()
extern void U3CHandleInitialDelayCoU3Ed__59_System_Collections_IEnumerator_get_Current_m36D3A25FB97FF664039DDCF195E7483D592296A1 (void);
// 0x00000089 System.Void MoreMountains.Feedbacks.MMFeedbacks/<PausedFeedbacksCo>d__61::.ctor(System.Int32)
extern void U3CPausedFeedbacksCoU3Ed__61__ctor_mE828EE8E7A0A7D319E71D73195F874B871074802 (void);
// 0x0000008A System.Void MoreMountains.Feedbacks.MMFeedbacks/<PausedFeedbacksCo>d__61::System.IDisposable.Dispose()
extern void U3CPausedFeedbacksCoU3Ed__61_System_IDisposable_Dispose_mD72C088F8929CF2F53E9394515D2C3ED73FB35D1 (void);
// 0x0000008B System.Boolean MoreMountains.Feedbacks.MMFeedbacks/<PausedFeedbacksCo>d__61::MoveNext()
extern void U3CPausedFeedbacksCoU3Ed__61_MoveNext_m131A82B702D2DA9FB180ADF62D8CD493E450DFBB (void);
// 0x0000008C System.Object MoreMountains.Feedbacks.MMFeedbacks/<PausedFeedbacksCo>d__61::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPausedFeedbacksCoU3Ed__61_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m30A2361BCB01AC87C75487CAFA3C4705677B5BDC (void);
// 0x0000008D System.Void MoreMountains.Feedbacks.MMFeedbacks/<PausedFeedbacksCo>d__61::System.Collections.IEnumerator.Reset()
extern void U3CPausedFeedbacksCoU3Ed__61_System_Collections_IEnumerator_Reset_m602A5BF1882100229396E49AA72E831AC07AA8AD (void);
// 0x0000008E System.Object MoreMountains.Feedbacks.MMFeedbacks/<PausedFeedbacksCo>d__61::System.Collections.IEnumerator.get_Current()
extern void U3CPausedFeedbacksCoU3Ed__61_System_Collections_IEnumerator_get_Current_mB0BB9D318DD4174EAB161F835869244CF64AA955 (void);
// 0x0000008F System.Collections.IEnumerator MoreMountains.Feedbacks.MMFeedbacksCoroutine::WaitForFrames(System.Int32)
extern void MMFeedbacksCoroutine_WaitForFrames_m889F49D355BDFCE9BB18745C3B4986F41E37CDE3 (void);
// 0x00000090 System.Collections.IEnumerator MoreMountains.Feedbacks.MMFeedbacksCoroutine::WaitFor(System.Single)
extern void MMFeedbacksCoroutine_WaitFor_m7EDC39169AFDDFE2167439BBAEC3162C5A9F7F3A (void);
// 0x00000091 System.Collections.IEnumerator MoreMountains.Feedbacks.MMFeedbacksCoroutine::WaitForUnscaled(System.Single)
extern void MMFeedbacksCoroutine_WaitForUnscaled_m70F048519D44202AED12373058EAB07E73BD6392 (void);
// 0x00000092 System.Void MoreMountains.Feedbacks.MMFeedbacksCoroutine/<WaitForFrames>d__0::.ctor(System.Int32)
extern void U3CWaitForFramesU3Ed__0__ctor_m51A7E4D200869F6385BBCE67471F1210DD676AAE (void);
// 0x00000093 System.Void MoreMountains.Feedbacks.MMFeedbacksCoroutine/<WaitForFrames>d__0::System.IDisposable.Dispose()
extern void U3CWaitForFramesU3Ed__0_System_IDisposable_Dispose_mBF0817F633C4CFE00D9A9BB36E67D163460912E3 (void);
// 0x00000094 System.Boolean MoreMountains.Feedbacks.MMFeedbacksCoroutine/<WaitForFrames>d__0::MoveNext()
extern void U3CWaitForFramesU3Ed__0_MoveNext_m5EB9853CB33B0384A3E78D2221C9DB60F3BC1445 (void);
// 0x00000095 System.Object MoreMountains.Feedbacks.MMFeedbacksCoroutine/<WaitForFrames>d__0::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForFramesU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBFB79431CD3410C1371FA6F19B2017E12D1A1F91 (void);
// 0x00000096 System.Void MoreMountains.Feedbacks.MMFeedbacksCoroutine/<WaitForFrames>d__0::System.Collections.IEnumerator.Reset()
extern void U3CWaitForFramesU3Ed__0_System_Collections_IEnumerator_Reset_mF510577CB66261DA47F52AF852724629220F1B33 (void);
// 0x00000097 System.Object MoreMountains.Feedbacks.MMFeedbacksCoroutine/<WaitForFrames>d__0::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForFramesU3Ed__0_System_Collections_IEnumerator_get_Current_m9BD6AF8E22BCEE245A4639263C057B4D2F469399 (void);
// 0x00000098 System.Void MoreMountains.Feedbacks.MMFeedbacksCoroutine/<WaitFor>d__1::.ctor(System.Int32)
extern void U3CWaitForU3Ed__1__ctor_mF2D4A952345A4505C6BCB3718320FA386DE95231 (void);
// 0x00000099 System.Void MoreMountains.Feedbacks.MMFeedbacksCoroutine/<WaitFor>d__1::System.IDisposable.Dispose()
extern void U3CWaitForU3Ed__1_System_IDisposable_Dispose_mFFB0FC691566115E3A54E7A98D2A4C89F28A2B90 (void);
// 0x0000009A System.Boolean MoreMountains.Feedbacks.MMFeedbacksCoroutine/<WaitFor>d__1::MoveNext()
extern void U3CWaitForU3Ed__1_MoveNext_m272532ADA03BAD4697311BFF660891D4CAF82C33 (void);
// 0x0000009B System.Object MoreMountains.Feedbacks.MMFeedbacksCoroutine/<WaitFor>d__1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4C08B005478726D43D693AB7E929C86213574DF4 (void);
// 0x0000009C System.Void MoreMountains.Feedbacks.MMFeedbacksCoroutine/<WaitFor>d__1::System.Collections.IEnumerator.Reset()
extern void U3CWaitForU3Ed__1_System_Collections_IEnumerator_Reset_mBD14C6A7E82361E0024C5F9F9D76422AB31A48BE (void);
// 0x0000009D System.Object MoreMountains.Feedbacks.MMFeedbacksCoroutine/<WaitFor>d__1::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForU3Ed__1_System_Collections_IEnumerator_get_Current_mD9413A0EB26D8EAA3A90404C05A5B64AF32F9EC4 (void);
// 0x0000009E System.Void MoreMountains.Feedbacks.MMFeedbacksCoroutine/<WaitForUnscaled>d__2::.ctor(System.Int32)
extern void U3CWaitForUnscaledU3Ed__2__ctor_m1D107AA135A7A4BFEEBCC101EBC4F8C560C50D79 (void);
// 0x0000009F System.Void MoreMountains.Feedbacks.MMFeedbacksCoroutine/<WaitForUnscaled>d__2::System.IDisposable.Dispose()
extern void U3CWaitForUnscaledU3Ed__2_System_IDisposable_Dispose_m267475D7DCC00092E11755197BDA9FB114BCF304 (void);
// 0x000000A0 System.Boolean MoreMountains.Feedbacks.MMFeedbacksCoroutine/<WaitForUnscaled>d__2::MoveNext()
extern void U3CWaitForUnscaledU3Ed__2_MoveNext_m6F62BDA7B47E0AC95871E93E6BF1A4789F2569B7 (void);
// 0x000000A1 System.Object MoreMountains.Feedbacks.MMFeedbacksCoroutine/<WaitForUnscaled>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForUnscaledU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6E41B2E3DE4BF58827B9C16B54D8909CB2799686 (void);
// 0x000000A2 System.Void MoreMountains.Feedbacks.MMFeedbacksCoroutine/<WaitForUnscaled>d__2::System.Collections.IEnumerator.Reset()
extern void U3CWaitForUnscaledU3Ed__2_System_Collections_IEnumerator_Reset_mE7F7DCA1A43AC58DC6006C2557F040CB73575754 (void);
// 0x000000A3 System.Object MoreMountains.Feedbacks.MMFeedbacksCoroutine/<WaitForUnscaled>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForUnscaledU3Ed__2_System_Collections_IEnumerator_get_Current_mD732E15C90C4EF2BDE33E230E61F6E52EB886B86 (void);
// 0x000000A4 MoreMountains.Feedbacks.MMFeedbacks MoreMountains.Feedbacks.MMFeedbacksEnabler::get_TargetMMFeedbacks()
extern void MMFeedbacksEnabler_get_TargetMMFeedbacks_mD20D74821D41BF1FF32C578E2954EF1594263867 (void);
// 0x000000A5 System.Void MoreMountains.Feedbacks.MMFeedbacksEnabler::set_TargetMMFeedbacks(MoreMountains.Feedbacks.MMFeedbacks)
extern void MMFeedbacksEnabler_set_TargetMMFeedbacks_mF95CAD31DA383C45A554C0A6499C2D9B85FCCD1A (void);
// 0x000000A6 System.Void MoreMountains.Feedbacks.MMFeedbacksEnabler::OnEnable()
extern void MMFeedbacksEnabler_OnEnable_mA9D1C595CA467AB39F490E5C4F970DFD727D2BE4 (void);
// 0x000000A7 System.Void MoreMountains.Feedbacks.MMFeedbacksEnabler::.ctor()
extern void MMFeedbacksEnabler__ctor_m7040DC6BFB5FCD0E186DC669798DFF4757A04918 (void);
// 0x000000A8 System.Void MoreMountains.Feedbacks.MMFeedbacksEvent::add_OnEvent(MoreMountains.Feedbacks.MMFeedbacksEvent/Delegate)
extern void MMFeedbacksEvent_add_OnEvent_mB2C6B5745AE0F6BEB298CED750A428D7093889A1 (void);
// 0x000000A9 System.Void MoreMountains.Feedbacks.MMFeedbacksEvent::remove_OnEvent(MoreMountains.Feedbacks.MMFeedbacksEvent/Delegate)
extern void MMFeedbacksEvent_remove_OnEvent_mF1FE13A8F480127061F78CA7E2E2C0F05F5972CE (void);
// 0x000000AA System.Void MoreMountains.Feedbacks.MMFeedbacksEvent::Register(MoreMountains.Feedbacks.MMFeedbacksEvent/Delegate)
extern void MMFeedbacksEvent_Register_m8189959F836C8C02CD61B7E18CDBDABB87C255EF (void);
// 0x000000AB System.Void MoreMountains.Feedbacks.MMFeedbacksEvent::Unregister(MoreMountains.Feedbacks.MMFeedbacksEvent/Delegate)
extern void MMFeedbacksEvent_Unregister_m44210D34C4679B739CAD0A64C233673A188937AE (void);
// 0x000000AC System.Void MoreMountains.Feedbacks.MMFeedbacksEvent::Trigger(MoreMountains.Feedbacks.MMFeedbacks,MoreMountains.Feedbacks.MMFeedbacksEvent/EventTypes)
extern void MMFeedbacksEvent_Trigger_m706BC4BE3219AF3202527D0816A30A27B4578702 (void);
// 0x000000AD System.Void MoreMountains.Feedbacks.MMFeedbacksEvent/Delegate::.ctor(System.Object,System.IntPtr)
extern void Delegate__ctor_mE6E828D19AFC3684CF4932FC5A3EFD004513C53E (void);
// 0x000000AE System.Void MoreMountains.Feedbacks.MMFeedbacksEvent/Delegate::Invoke(MoreMountains.Feedbacks.MMFeedbacks,MoreMountains.Feedbacks.MMFeedbacksEvent/EventTypes)
extern void Delegate_Invoke_m3680BDB32E14FDDD44A1D03C9443EB2D3FB1A548 (void);
// 0x000000AF System.IAsyncResult MoreMountains.Feedbacks.MMFeedbacksEvent/Delegate::BeginInvoke(MoreMountains.Feedbacks.MMFeedbacks,MoreMountains.Feedbacks.MMFeedbacksEvent/EventTypes,System.AsyncCallback,System.Object)
extern void Delegate_BeginInvoke_m3FD7E4A6ABC990ECB3990B66A9236430F1B617EB (void);
// 0x000000B0 System.Void MoreMountains.Feedbacks.MMFeedbacksEvent/Delegate::EndInvoke(System.IAsyncResult)
extern void Delegate_EndInvoke_m0D6DA0ED27360FCB04353D4EAE02AEBC6B97F67D (void);
// 0x000000B1 System.Boolean MoreMountains.Feedbacks.MMFeedbacksEvents::get_OnPlayIsNull()
extern void MMFeedbacksEvents_get_OnPlayIsNull_m50C6B5A87E27E4953652E25C680A51B14D244F57 (void);
// 0x000000B2 System.Void MoreMountains.Feedbacks.MMFeedbacksEvents::set_OnPlayIsNull(System.Boolean)
extern void MMFeedbacksEvents_set_OnPlayIsNull_m7E23F57BF5ED7F3FD7FA3AADBA7B62DA353FDEF6 (void);
// 0x000000B3 System.Boolean MoreMountains.Feedbacks.MMFeedbacksEvents::get_OnPauseIsNull()
extern void MMFeedbacksEvents_get_OnPauseIsNull_m200330A23EC251094DD69F0D9BBD028695DCE579 (void);
// 0x000000B4 System.Void MoreMountains.Feedbacks.MMFeedbacksEvents::set_OnPauseIsNull(System.Boolean)
extern void MMFeedbacksEvents_set_OnPauseIsNull_m44604A1DF09463075A67BA32B3B47B310E7F0EDD (void);
// 0x000000B5 System.Boolean MoreMountains.Feedbacks.MMFeedbacksEvents::get_OnResumeIsNull()
extern void MMFeedbacksEvents_get_OnResumeIsNull_m5F402F0D594F64EBB248F82BFF14A8F1589E4A89 (void);
// 0x000000B6 System.Void MoreMountains.Feedbacks.MMFeedbacksEvents::set_OnResumeIsNull(System.Boolean)
extern void MMFeedbacksEvents_set_OnResumeIsNull_m3FFA6FC75D64C8970F0383B9B6FA6491883D328D (void);
// 0x000000B7 System.Boolean MoreMountains.Feedbacks.MMFeedbacksEvents::get_OnRevertIsNull()
extern void MMFeedbacksEvents_get_OnRevertIsNull_mE1819EB1DD5078CAB44188A09A78FD3ACD809E90 (void);
// 0x000000B8 System.Void MoreMountains.Feedbacks.MMFeedbacksEvents::set_OnRevertIsNull(System.Boolean)
extern void MMFeedbacksEvents_set_OnRevertIsNull_mF6A3F80FFE08003C99389CFCAA0B3F9A31462E0D (void);
// 0x000000B9 System.Boolean MoreMountains.Feedbacks.MMFeedbacksEvents::get_OnCompleteIsNull()
extern void MMFeedbacksEvents_get_OnCompleteIsNull_m4C0DA7A468C4BAD3DE6649F86CF66573C265010B (void);
// 0x000000BA System.Void MoreMountains.Feedbacks.MMFeedbacksEvents::set_OnCompleteIsNull(System.Boolean)
extern void MMFeedbacksEvents_set_OnCompleteIsNull_m6481DC89A306058719C7E3C106DAA62762ACBB18 (void);
// 0x000000BB System.Void MoreMountains.Feedbacks.MMFeedbacksEvents::Initialization()
extern void MMFeedbacksEvents_Initialization_m920437E7851C073E2D24635152886AEFAE343667 (void);
// 0x000000BC System.Void MoreMountains.Feedbacks.MMFeedbacksEvents::TriggerOnPlay(MoreMountains.Feedbacks.MMFeedbacks)
extern void MMFeedbacksEvents_TriggerOnPlay_m442327A8831309A0B98E2D3021173EFFDCE2520E (void);
// 0x000000BD System.Void MoreMountains.Feedbacks.MMFeedbacksEvents::TriggerOnPause(MoreMountains.Feedbacks.MMFeedbacks)
extern void MMFeedbacksEvents_TriggerOnPause_mF27B9E8C8893FC347C5DCA0856ECBCDAFF743A5D (void);
// 0x000000BE System.Void MoreMountains.Feedbacks.MMFeedbacksEvents::TriggerOnResume(MoreMountains.Feedbacks.MMFeedbacks)
extern void MMFeedbacksEvents_TriggerOnResume_m8070597601B720D2DFB49622E804AF41FD0E3EDE (void);
// 0x000000BF System.Void MoreMountains.Feedbacks.MMFeedbacksEvents::TriggerOnRevert(MoreMountains.Feedbacks.MMFeedbacks)
extern void MMFeedbacksEvents_TriggerOnRevert_m2581D7D5CBD3319A09D0B0E121A26F004BC1FAF6 (void);
// 0x000000C0 System.Void MoreMountains.Feedbacks.MMFeedbacksEvents::TriggerOnComplete(MoreMountains.Feedbacks.MMFeedbacks)
extern void MMFeedbacksEvents_TriggerOnComplete_mA45D38B3DAB67A52A5588AD9AD96A6061907F3D1 (void);
// 0x000000C1 System.Void MoreMountains.Feedbacks.MMFeedbacksEvents::.ctor()
extern void MMFeedbacksEvents__ctor_m9FFC68B7BCCCCC30C0D40168D4349535B95A4E4F (void);
// 0x000000C2 System.Single MoreMountains.Feedbacks.MMFeedbacksHelpers::Remap(System.Single,System.Single,System.Single,System.Single,System.Single)
extern void MMFeedbacksHelpers_Remap_mF91639B3964272F011DC211808184EC25AF9571F (void);
// 0x000000C3 System.Void MoreMountains.Feedbacks.MMFeedbacksHelpers::.ctor()
extern void MMFeedbacksHelpers__ctor_m6EBAD14745D011242953EA76E992102551D68A1D (void);
// 0x000000C4 System.Void MoreMountains.Feedbacks.MMFReadOnlyAttribute::.ctor()
extern void MMFReadOnlyAttribute__ctor_mED0F52D7CF9B8B44132D444186F6E8148E6BDFC3 (void);
// 0x000000C5 System.Void MoreMountains.Feedbacks.MMFInspectorButtonAttribute::.ctor(System.String)
extern void MMFInspectorButtonAttribute__ctor_m9F7D6C8785E6F916CBCF7844EEDFDE4E942F648D (void);
// 0x000000C6 System.Boolean MoreMountains.Feedbacks.MMFEnumConditionAttribute::ContainsBitFlag(System.Int32)
extern void MMFEnumConditionAttribute_ContainsBitFlag_m3A55739C9BE0CD1954041C6750C055FB854392D9 (void);
// 0x000000C7 System.Void MoreMountains.Feedbacks.MMFEnumConditionAttribute::.ctor(System.String,System.Int32[])
extern void MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F (void);
// 0x000000C8 System.Void MoreMountains.Feedbacks.MMFInformationAttribute::.ctor(System.String,MoreMountains.Feedbacks.MMFInformationAttribute/InformationType,System.Boolean)
extern void MMFInformationAttribute__ctor_m004E74EBE93920E7BAC370DDB10E141EDE1A073D (void);
// 0x000000C9 System.Void MoreMountains.Feedbacks.MMFHiddenAttribute::.ctor()
extern void MMFHiddenAttribute__ctor_m4586259502097BA3F7078D7F8926807CB993D69B (void);
// 0x000000CA System.Void MoreMountains.Feedbacks.MMFConditionAttribute::.ctor(System.String)
extern void MMFConditionAttribute__ctor_m2EFB750D34B58F0035B297E3968682303584D7B4 (void);
// 0x000000CB System.Void MoreMountains.Feedbacks.MMFConditionAttribute::.ctor(System.String,System.Boolean)
extern void MMFConditionAttribute__ctor_mABD947D1EABD573CF6BED72E3FD7886D4DEA81A3 (void);
// 0x000000CC UnityEngine.Component MoreMountains.Feedbacks.MMFeedbackStaticMethods::GetComponentNoAlloc(UnityEngine.GameObject,System.Type)
extern void MMFeedbackStaticMethods_GetComponentNoAlloc_m6087AB4282C2D5B39BA0C805CF86A3068CCC103E (void);
// 0x000000CD T MoreMountains.Feedbacks.MMFeedbackStaticMethods::MMFGetComponentNoAlloc(UnityEngine.GameObject)
// 0x000000CE System.Void MoreMountains.Feedbacks.MMFeedbackStaticMethods::.cctor()
extern void MMFeedbackStaticMethods__cctor_m96E545DCC2E4AE64916EB195CD35F2FF5C44B3DB (void);
// 0x000000CF System.Void MoreMountains.Feedbacks.FeedbackPathAttribute::.ctor(System.String)
extern void FeedbackPathAttribute__ctor_mF36AB244DDE78F2BF1F0B9285F977B1D7212E2BC (void);
// 0x000000D0 System.String MoreMountains.Feedbacks.FeedbackPathAttribute::GetFeedbackDefaultName(System.Type)
extern void FeedbackPathAttribute_GetFeedbackDefaultName_m3FFAF101E28D8D884FF6292DD1771D3D7511446B (void);
// 0x000000D1 System.String MoreMountains.Feedbacks.FeedbackPathAttribute::GetFeedbackDefaultPath(System.Type)
extern void FeedbackPathAttribute_GetFeedbackDefaultPath_m134A9FA8AE3B21A5A47AFCA7881792FCD59B7EBF (void);
// 0x000000D2 System.Void MoreMountains.Feedbacks.FeedbackHelpAttribute::.ctor(System.String)
extern void FeedbackHelpAttribute__ctor_mACF45ABD072B9B8871B703E67DF4E7076C9AFF94 (void);
// 0x000000D3 System.String MoreMountains.Feedbacks.FeedbackHelpAttribute::GetFeedbackHelpText(System.Type)
extern void FeedbackHelpAttribute_GetFeedbackHelpText_mE34CAAB95E3BF984992A628438D75193BFB9D244 (void);
// 0x000000D4 System.Void MoreMountains.Feedbacks.MMFeedbacksInspectorColors::.ctor()
extern void MMFeedbacksInspectorColors__ctor_m0249F7399DE838C14CCD297078C5493B281A8EEE (void);
// 0x000000D5 System.Void MoreMountains.Feedbacks.MMFeedbacksInspectorColors::.cctor()
extern void MMFeedbacksInspectorColors__cctor_m8AFDB8625880BFB650450C2F25115B44E41408A3 (void);
// 0x000000D6 System.Single MoreMountains.Feedbacks.MMShaker::GetTime()
extern void MMShaker_GetTime_m838AC29CF09C07EC55ED9B66663EC0E30825621A (void);
// 0x000000D7 System.Single MoreMountains.Feedbacks.MMShaker::GetDeltaTime()
extern void MMShaker_GetDeltaTime_mE30E675C17F6820E8405BE7A579BA041FC9EB9B4 (void);
// 0x000000D8 System.Boolean MoreMountains.Feedbacks.MMShaker::get_ListeningToEvents()
extern void MMShaker_get_ListeningToEvents_mC4D91748475F610B92507BFCC46065A58BCC8CBF (void);
// 0x000000D9 System.Void MoreMountains.Feedbacks.MMShaker::Awake()
extern void MMShaker_Awake_m83FADB62D0163F5D3F9269635BB75B1BE95A361A (void);
// 0x000000DA System.Void MoreMountains.Feedbacks.MMShaker::Initialization()
extern void MMShaker_Initialization_mA0C4D4855C7877F7FD4BE6DB7C55D309A53753B3 (void);
// 0x000000DB System.Void MoreMountains.Feedbacks.MMShaker::StartShaking()
extern void MMShaker_StartShaking_m9558903CD708F69075930EA811BDA90E150398A1 (void);
// 0x000000DC System.Void MoreMountains.Feedbacks.MMShaker::ShakeStarts()
extern void MMShaker_ShakeStarts_mE85CF38CAF236AE4170F956F2E2CC8BA316E24AA (void);
// 0x000000DD System.Void MoreMountains.Feedbacks.MMShaker::GrabInitialValues()
extern void MMShaker_GrabInitialValues_m3D924C41760705BF9248469E5E169F1369810597 (void);
// 0x000000DE System.Void MoreMountains.Feedbacks.MMShaker::Update()
extern void MMShaker_Update_m30C3E8BC8F4BF2F8AB6397F10E9E9F89808C150D (void);
// 0x000000DF System.Void MoreMountains.Feedbacks.MMShaker::Shake()
extern void MMShaker_Shake_mE02B7B6B1D3ADA802A6338A641EFFBB929E76972 (void);
// 0x000000E0 System.Single MoreMountains.Feedbacks.MMShaker::ShakeFloat(UnityEngine.AnimationCurve,System.Single,System.Single,System.Boolean,System.Single)
extern void MMShaker_ShakeFloat_mF9F54D49B940E1A772BB6490100C3C348983DA39 (void);
// 0x000000E1 System.Void MoreMountains.Feedbacks.MMShaker::ResetTargetValues()
extern void MMShaker_ResetTargetValues_m4A621683BB61D3FD391AAC8F7983528F725B24C2 (void);
// 0x000000E2 System.Void MoreMountains.Feedbacks.MMShaker::ResetShakerValues()
extern void MMShaker_ResetShakerValues_m93AA8579FE316AD1BBCB482D7DC47593B5F70092 (void);
// 0x000000E3 System.Void MoreMountains.Feedbacks.MMShaker::ShakeComplete()
extern void MMShaker_ShakeComplete_m2F8B3295AA1926EC6F3FB4AF07D73565B9B8B8D7 (void);
// 0x000000E4 System.Void MoreMountains.Feedbacks.MMShaker::OnEnable()
extern void MMShaker_OnEnable_m6C88321C29032BB691EC7BDB99CBC1B77F0D3B12 (void);
// 0x000000E5 System.Void MoreMountains.Feedbacks.MMShaker::OnDestroy()
extern void MMShaker_OnDestroy_m3F93893AC5915BBAAE9E6F7DEFFEAB80974CDD9D (void);
// 0x000000E6 System.Void MoreMountains.Feedbacks.MMShaker::OnDisable()
extern void MMShaker_OnDisable_mE674ADF7B6D142F76FF2731F4831447241C411FC (void);
// 0x000000E7 System.Void MoreMountains.Feedbacks.MMShaker::Play()
extern void MMShaker_Play_m7198F7132EA436C45FBEBF9178BA0C400844A478 (void);
// 0x000000E8 System.Void MoreMountains.Feedbacks.MMShaker::Stop()
extern void MMShaker_Stop_m784E87C0407DCE0F89F3046FF89934CAD5ECA356 (void);
// 0x000000E9 System.Void MoreMountains.Feedbacks.MMShaker::StartListening()
extern void MMShaker_StartListening_m2FD2D6DA250E2613C19EB5F54D1B4F78803D644F (void);
// 0x000000EA System.Void MoreMountains.Feedbacks.MMShaker::StopListening()
extern void MMShaker_StopListening_m1D60A4786564D907C4378080302EC52998682078 (void);
// 0x000000EB System.Boolean MoreMountains.Feedbacks.MMShaker::CheckEventAllowed(System.Int32,System.Boolean,System.Single,UnityEngine.Vector3)
extern void MMShaker_CheckEventAllowed_m3D0BAF591A15E941D8ABDE2BEECCA357EF227B5F (void);
// 0x000000EC System.Void MoreMountains.Feedbacks.MMShaker::.ctor()
extern void MMShaker__ctor_m19139ACF2B49DCE913269ED0C2D23D9B08E85C74 (void);
// 0x000000ED System.Void MoreMountains.Feedbacks.MMMiniObjectPooler::Awake()
extern void MMMiniObjectPooler_Awake_m1AC08CFF3BFDD74842AF5F42FFB31F6B4A16CCD7 (void);
// 0x000000EE System.Void MoreMountains.Feedbacks.MMMiniObjectPooler::CreateWaitingPool()
extern void MMMiniObjectPooler_CreateWaitingPool_m8801BE4304E76F3B82D90F44C453E03988785B08 (void);
// 0x000000EF System.String MoreMountains.Feedbacks.MMMiniObjectPooler::DetermineObjectPoolName()
extern void MMMiniObjectPooler_DetermineObjectPoolName_m2C89E679BAD3C4386BE6CA5C91B67CECB744714F (void);
// 0x000000F0 System.Void MoreMountains.Feedbacks.MMMiniObjectPooler::FillObjectPool()
extern void MMMiniObjectPooler_FillObjectPool_mADD73BE5FF97BE42B33CAF7ECCA48BA0E76A871C (void);
// 0x000000F1 UnityEngine.GameObject MoreMountains.Feedbacks.MMMiniObjectPooler::GetPooledGameObject()
extern void MMMiniObjectPooler_GetPooledGameObject_m59EC410CBCCBC4F9F27AE50E9A06DB5A10CCB0B4 (void);
// 0x000000F2 UnityEngine.GameObject MoreMountains.Feedbacks.MMMiniObjectPooler::AddOneObjectToThePool()
extern void MMMiniObjectPooler_AddOneObjectToThePool_m5EBE1E3EC43D5992B5FE387DD3C162B9B2E22682 (void);
// 0x000000F3 System.Void MoreMountains.Feedbacks.MMMiniObjectPooler::DestroyObjectPool()
extern void MMMiniObjectPooler_DestroyObjectPool_mD6E88BFBE8DB5634F4DAD4F130A8B06F1D89CB44 (void);
// 0x000000F4 System.Void MoreMountains.Feedbacks.MMMiniObjectPooler::.ctor()
extern void MMMiniObjectPooler__ctor_mF39E7F0727047EEA2317D6D5AD417633D1F14D6C (void);
// 0x000000F5 System.Void MoreMountains.Feedbacks.MMMiniObjectPool::.ctor()
extern void MMMiniObjectPool__ctor_m532EA76284743CADD3E20981AB89B2351D315FD0 (void);
// 0x000000F6 System.Void MoreMountains.Feedbacks.MMMiniPoolableObject::add_OnSpawnComplete(MoreMountains.Feedbacks.MMMiniPoolableObject/Events)
extern void MMMiniPoolableObject_add_OnSpawnComplete_m8FE54EC9EFE60E277A763F4AF0A8F662BF98A0E1 (void);
// 0x000000F7 System.Void MoreMountains.Feedbacks.MMMiniPoolableObject::remove_OnSpawnComplete(MoreMountains.Feedbacks.MMMiniPoolableObject/Events)
extern void MMMiniPoolableObject_remove_OnSpawnComplete_mA7237858F56A57AC40ED152BAC2C15011DAE685A (void);
// 0x000000F8 System.Void MoreMountains.Feedbacks.MMMiniPoolableObject::Destroy()
extern void MMMiniPoolableObject_Destroy_m4BB22A25373E968C2C55A2AE0F3A6D911819984B (void);
// 0x000000F9 System.Void MoreMountains.Feedbacks.MMMiniPoolableObject::OnEnable()
extern void MMMiniPoolableObject_OnEnable_m25E461030FD27AACE169C8085CED5D5A9D9E4850 (void);
// 0x000000FA System.Void MoreMountains.Feedbacks.MMMiniPoolableObject::OnDisable()
extern void MMMiniPoolableObject_OnDisable_mE970BB260BC05CB65D05897E674CE33D582B6A5F (void);
// 0x000000FB System.Void MoreMountains.Feedbacks.MMMiniPoolableObject::TriggerOnSpawnComplete()
extern void MMMiniPoolableObject_TriggerOnSpawnComplete_m3626D19FC696C7BFFFDDF31947E0CCE5D8E39A07 (void);
// 0x000000FC System.Void MoreMountains.Feedbacks.MMMiniPoolableObject::.ctor()
extern void MMMiniPoolableObject__ctor_m6B356DFFCC997D601B9F0BC1FB56BD6BAC2E7B06 (void);
// 0x000000FD System.Void MoreMountains.Feedbacks.MMMiniPoolableObject/Events::.ctor(System.Object,System.IntPtr)
extern void Events__ctor_m5B93F86C13019CDC7733BBC5AC6E35CC6A9AEAED (void);
// 0x000000FE System.Void MoreMountains.Feedbacks.MMMiniPoolableObject/Events::Invoke()
extern void Events_Invoke_mEDC030AD3C04E3C0CE15D459122043A10BD9B0DC (void);
// 0x000000FF System.IAsyncResult MoreMountains.Feedbacks.MMMiniPoolableObject/Events::BeginInvoke(System.AsyncCallback,System.Object)
extern void Events_BeginInvoke_m7540D5B581AFC55D2F51770876A18B649A9C8A72 (void);
// 0x00000100 System.Void MoreMountains.Feedbacks.MMMiniPoolableObject/Events::EndInvoke(System.IAsyncResult)
extern void Events_EndInvoke_mF454180134FE4C69A3C63955B6333438E44984B6 (void);
// 0x00000101 System.Void MoreMountains.Feedbacks.MMFeedbackAnimation::CustomInitialization(UnityEngine.GameObject)
extern void MMFeedbackAnimation_CustomInitialization_m7BB9427CCA8A923A226C524422F2A01ABCC25D88 (void);
// 0x00000102 System.Void MoreMountains.Feedbacks.MMFeedbackAnimation::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackAnimation_CustomPlayFeedback_m8B893790F285B18673773DE6289E6642BCD52444 (void);
// 0x00000103 System.Void MoreMountains.Feedbacks.MMFeedbackAnimation::CustomStopFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackAnimation_CustomStopFeedback_m3BD26DCECDFE2EC00FC5104F8E0BE4DC7766CB41 (void);
// 0x00000104 System.Void MoreMountains.Feedbacks.MMFeedbackAnimation::.ctor()
extern void MMFeedbackAnimation__ctor_m5A63B89F3925BA9F6A3F0450E40CEB455D1B547C (void);
// 0x00000105 System.Single MoreMountains.Feedbacks.MMFeedbackAudioFilterDistortion::get_FeedbackDuration()
extern void MMFeedbackAudioFilterDistortion_get_FeedbackDuration_m328E2667245937BF6253BBBF5DE7C175B85C7363 (void);
// 0x00000106 System.Void MoreMountains.Feedbacks.MMFeedbackAudioFilterDistortion::set_FeedbackDuration(System.Single)
extern void MMFeedbackAudioFilterDistortion_set_FeedbackDuration_mA89BAB7D6E3A84100CCAEB4C6BF7BE149E5BAF34 (void);
// 0x00000107 System.Void MoreMountains.Feedbacks.MMFeedbackAudioFilterDistortion::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackAudioFilterDistortion_CustomPlayFeedback_m7B27B9ED5F0F3E66283ECD2D4C22E1716F7349DA (void);
// 0x00000108 System.Void MoreMountains.Feedbacks.MMFeedbackAudioFilterDistortion::CustomStopFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackAudioFilterDistortion_CustomStopFeedback_mB2F08EBD42A8BC84C375FAC3512869BF58FA9B2C (void);
// 0x00000109 System.Void MoreMountains.Feedbacks.MMFeedbackAudioFilterDistortion::.ctor()
extern void MMFeedbackAudioFilterDistortion__ctor_m3F78BEDAD9DB540DCAE5E598214DA5FBF5366C3F (void);
// 0x0000010A System.Single MoreMountains.Feedbacks.MMFeedbackAudioFilterEcho::get_FeedbackDuration()
extern void MMFeedbackAudioFilterEcho_get_FeedbackDuration_mACFBF8D7377DB724A5EE70509968EE6A4FFD1890 (void);
// 0x0000010B System.Void MoreMountains.Feedbacks.MMFeedbackAudioFilterEcho::set_FeedbackDuration(System.Single)
extern void MMFeedbackAudioFilterEcho_set_FeedbackDuration_m848D1BDAB04EF17AC970BD02F90E69494F4CBE76 (void);
// 0x0000010C System.Void MoreMountains.Feedbacks.MMFeedbackAudioFilterEcho::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackAudioFilterEcho_CustomPlayFeedback_m4B89204964744F5F3BF3BEFE8DF4D766F802494E (void);
// 0x0000010D System.Void MoreMountains.Feedbacks.MMFeedbackAudioFilterEcho::CustomStopFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackAudioFilterEcho_CustomStopFeedback_m83ED6F0546DC3D9518D15F6B5376F8D4C252325B (void);
// 0x0000010E System.Void MoreMountains.Feedbacks.MMFeedbackAudioFilterEcho::.ctor()
extern void MMFeedbackAudioFilterEcho__ctor_mF22E1CAD26A3CF3AFDA89FD9A3DC8D0703DF8653 (void);
// 0x0000010F System.Single MoreMountains.Feedbacks.MMFeedbackAudioFilterHighPass::get_FeedbackDuration()
extern void MMFeedbackAudioFilterHighPass_get_FeedbackDuration_m5F1DA3183EFB2CA38A4B0A721382B8EC4930DF17 (void);
// 0x00000110 System.Void MoreMountains.Feedbacks.MMFeedbackAudioFilterHighPass::set_FeedbackDuration(System.Single)
extern void MMFeedbackAudioFilterHighPass_set_FeedbackDuration_m4B48192EEE44880172523198B9646A4D6D8EEEA9 (void);
// 0x00000111 System.Void MoreMountains.Feedbacks.MMFeedbackAudioFilterHighPass::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackAudioFilterHighPass_CustomPlayFeedback_mCC07D9533A1234D7B79D95824A46E6F37505FF73 (void);
// 0x00000112 System.Void MoreMountains.Feedbacks.MMFeedbackAudioFilterHighPass::CustomStopFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackAudioFilterHighPass_CustomStopFeedback_m98166F96962CF2B9436D634F8935D9786B12848D (void);
// 0x00000113 System.Void MoreMountains.Feedbacks.MMFeedbackAudioFilterHighPass::.ctor()
extern void MMFeedbackAudioFilterHighPass__ctor_m94000BD0A0C46AFA0FDF8ADD5DDDD77F326748F4 (void);
// 0x00000114 System.Single MoreMountains.Feedbacks.MMFeedbackAudioFilterLowPass::get_FeedbackDuration()
extern void MMFeedbackAudioFilterLowPass_get_FeedbackDuration_m9D2C9F1F36CDDF6C771655818170681AB3530EDB (void);
// 0x00000115 System.Void MoreMountains.Feedbacks.MMFeedbackAudioFilterLowPass::set_FeedbackDuration(System.Single)
extern void MMFeedbackAudioFilterLowPass_set_FeedbackDuration_m6EACF5C25A23F105214EBB78461822144521EFC5 (void);
// 0x00000116 System.Void MoreMountains.Feedbacks.MMFeedbackAudioFilterLowPass::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackAudioFilterLowPass_CustomPlayFeedback_mB01C1E46E919E305AEFE048E71E38D27417DFF36 (void);
// 0x00000117 System.Void MoreMountains.Feedbacks.MMFeedbackAudioFilterLowPass::CustomStopFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackAudioFilterLowPass_CustomStopFeedback_mCCB9804846A0F553B65CE90671A750930D2646BC (void);
// 0x00000118 System.Void MoreMountains.Feedbacks.MMFeedbackAudioFilterLowPass::.ctor()
extern void MMFeedbackAudioFilterLowPass__ctor_m2481D76BA6A755AF0F7E68E77675FAD2616E11E6 (void);
// 0x00000119 System.Single MoreMountains.Feedbacks.MMFeedbackAudioFilterReverb::get_FeedbackDuration()
extern void MMFeedbackAudioFilterReverb_get_FeedbackDuration_m105EC6D8A35571439066076B4D2377737E2C3336 (void);
// 0x0000011A System.Void MoreMountains.Feedbacks.MMFeedbackAudioFilterReverb::set_FeedbackDuration(System.Single)
extern void MMFeedbackAudioFilterReverb_set_FeedbackDuration_m0A7E9EF2279D16D54693D87E384E87D3E3A3B24F (void);
// 0x0000011B System.Void MoreMountains.Feedbacks.MMFeedbackAudioFilterReverb::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackAudioFilterReverb_CustomPlayFeedback_m6564A41B68F960C003BC32FA7725F2EE8C2EAC00 (void);
// 0x0000011C System.Void MoreMountains.Feedbacks.MMFeedbackAudioFilterReverb::CustomStopFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackAudioFilterReverb_CustomStopFeedback_m5BCFDB3E056E5DE43089B9E02EB4EDCFAFAD040F (void);
// 0x0000011D System.Void MoreMountains.Feedbacks.MMFeedbackAudioFilterReverb::.ctor()
extern void MMFeedbackAudioFilterReverb__ctor_m5177EF5F75DFB689A12AD1E5D907C1043C9E0E85 (void);
// 0x0000011E System.Void MoreMountains.Feedbacks.MMFeedbackAudioMixerSnapshotTransition::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackAudioMixerSnapshotTransition_CustomPlayFeedback_mA5F0D6D5A6CE511A9E47CDB677C2C713F1A8A907 (void);
// 0x0000011F System.Void MoreMountains.Feedbacks.MMFeedbackAudioMixerSnapshotTransition::.ctor()
extern void MMFeedbackAudioMixerSnapshotTransition__ctor_m03000066326DBEEA507B11172512ACFE2984FF2D (void);
// 0x00000120 System.Single MoreMountains.Feedbacks.MMFeedbackAudioSource::get_FeedbackDuration()
extern void MMFeedbackAudioSource_get_FeedbackDuration_m2A3B5ABC4366B6234ACD25544074CFF0289A6356 (void);
// 0x00000121 System.Void MoreMountains.Feedbacks.MMFeedbackAudioSource::set_FeedbackDuration(System.Single)
extern void MMFeedbackAudioSource_set_FeedbackDuration_mFDBC2D2C1A8ADC98CEB34A5C4E6004BBD1E2BB9A (void);
// 0x00000122 System.Void MoreMountains.Feedbacks.MMFeedbackAudioSource::CustomInitialization(UnityEngine.GameObject)
extern void MMFeedbackAudioSource_CustomInitialization_m072BD94D3EC7A276BDA0BFA07C0821F21D61FD98 (void);
// 0x00000123 System.Void MoreMountains.Feedbacks.MMFeedbackAudioSource::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackAudioSource_CustomPlayFeedback_mEBAB46EA1633C9719F091387F7AD8EAF1B0BF98F (void);
// 0x00000124 System.Void MoreMountains.Feedbacks.MMFeedbackAudioSource::PlayAudioSource(UnityEngine.AudioSource,System.Single,System.Single)
extern void MMFeedbackAudioSource_PlayAudioSource_m0C44E0D72738BFFC12C69604BDB2A37A5B22A1E1 (void);
// 0x00000125 System.Void MoreMountains.Feedbacks.MMFeedbackAudioSource::Stop(UnityEngine.Vector3,System.Single)
extern void MMFeedbackAudioSource_Stop_m5B8B1942A1C89A708CE100909C3B9B182AB78C26 (void);
// 0x00000126 System.Void MoreMountains.Feedbacks.MMFeedbackAudioSource::.ctor()
extern void MMFeedbackAudioSource__ctor_m81E3231025E14EEF9B4E873C7C528F8970822486 (void);
// 0x00000127 System.Single MoreMountains.Feedbacks.MMFeedbackAudioSourcePitch::get_FeedbackDuration()
extern void MMFeedbackAudioSourcePitch_get_FeedbackDuration_mA7FE084862F201DD6B83078B03759A63DD763C38 (void);
// 0x00000128 System.Void MoreMountains.Feedbacks.MMFeedbackAudioSourcePitch::set_FeedbackDuration(System.Single)
extern void MMFeedbackAudioSourcePitch_set_FeedbackDuration_m4C470F7F807EC52F9942243693700FEA8098A404 (void);
// 0x00000129 System.Void MoreMountains.Feedbacks.MMFeedbackAudioSourcePitch::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackAudioSourcePitch_CustomPlayFeedback_m0D39FE7BD0BDF78CD1D633AB18F7D3F23C6AE3F2 (void);
// 0x0000012A System.Void MoreMountains.Feedbacks.MMFeedbackAudioSourcePitch::CustomStopFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackAudioSourcePitch_CustomStopFeedback_mB760852632A33386E9CA1A4BAD274B978FA08AAA (void);
// 0x0000012B System.Void MoreMountains.Feedbacks.MMFeedbackAudioSourcePitch::.ctor()
extern void MMFeedbackAudioSourcePitch__ctor_mD4351639476BFAC7860F534815EA2BEC31753A7D (void);
// 0x0000012C System.Single MoreMountains.Feedbacks.MMFeedbackAudioSourceStereoPan::get_FeedbackDuration()
extern void MMFeedbackAudioSourceStereoPan_get_FeedbackDuration_m697C94EEDDFAEB25B87E05A62DE39C7A4A04BCDC (void);
// 0x0000012D System.Void MoreMountains.Feedbacks.MMFeedbackAudioSourceStereoPan::set_FeedbackDuration(System.Single)
extern void MMFeedbackAudioSourceStereoPan_set_FeedbackDuration_mB8747CD979B5E953A513E5E503168949F1EB8680 (void);
// 0x0000012E System.Void MoreMountains.Feedbacks.MMFeedbackAudioSourceStereoPan::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackAudioSourceStereoPan_CustomPlayFeedback_m96280553351055A5F5246721CE9E44BA104B33CC (void);
// 0x0000012F System.Void MoreMountains.Feedbacks.MMFeedbackAudioSourceStereoPan::CustomStopFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackAudioSourceStereoPan_CustomStopFeedback_m42DDD7A7A2D868127217BB7DCFEEC92E8C9277A4 (void);
// 0x00000130 System.Void MoreMountains.Feedbacks.MMFeedbackAudioSourceStereoPan::.ctor()
extern void MMFeedbackAudioSourceStereoPan__ctor_mEFAC2188FE0C02EFEE6F41A8B6A32E3093E91820 (void);
// 0x00000131 System.Single MoreMountains.Feedbacks.MMFeedbackAudioSourceVolume::get_FeedbackDuration()
extern void MMFeedbackAudioSourceVolume_get_FeedbackDuration_mDF91E5A7549F9DDA4D563824E3DF1412691E82E5 (void);
// 0x00000132 System.Void MoreMountains.Feedbacks.MMFeedbackAudioSourceVolume::set_FeedbackDuration(System.Single)
extern void MMFeedbackAudioSourceVolume_set_FeedbackDuration_m6CF701A25EEF9C6AB71991B141352359D08B18E6 (void);
// 0x00000133 System.Void MoreMountains.Feedbacks.MMFeedbackAudioSourceVolume::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackAudioSourceVolume_CustomPlayFeedback_mA7107CBB4C69A5CC88DFF68BECB6648B0E75DECC (void);
// 0x00000134 System.Void MoreMountains.Feedbacks.MMFeedbackAudioSourceVolume::CustomStopFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackAudioSourceVolume_CustomStopFeedback_mADAA24A38A0731A2CC9CED88565122E1D92B2663 (void);
// 0x00000135 System.Void MoreMountains.Feedbacks.MMFeedbackAudioSourceVolume::.ctor()
extern void MMFeedbackAudioSourceVolume__ctor_m28CF50087315A29A83E2280CE608C35DD8A7CA05 (void);
// 0x00000136 System.Single MoreMountains.Feedbacks.MMFeedbackCameraClippingPlanes::get_FeedbackDuration()
extern void MMFeedbackCameraClippingPlanes_get_FeedbackDuration_m9E73C543A67F5BDDD80711232D0ADE90E132FA75 (void);
// 0x00000137 System.Void MoreMountains.Feedbacks.MMFeedbackCameraClippingPlanes::set_FeedbackDuration(System.Single)
extern void MMFeedbackCameraClippingPlanes_set_FeedbackDuration_m6DF65E7142F7690CB21A38247330C38CDB689ACD (void);
// 0x00000138 System.Void MoreMountains.Feedbacks.MMFeedbackCameraClippingPlanes::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackCameraClippingPlanes_CustomPlayFeedback_mEFB9C9C14E472E9A573467A16BB75D9DABABD47E (void);
// 0x00000139 System.Void MoreMountains.Feedbacks.MMFeedbackCameraClippingPlanes::CustomStopFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackCameraClippingPlanes_CustomStopFeedback_m0D6C0DC68F3B5372E3E8199A138CC157D0EF0659 (void);
// 0x0000013A System.Void MoreMountains.Feedbacks.MMFeedbackCameraClippingPlanes::.ctor()
extern void MMFeedbackCameraClippingPlanes__ctor_mBD44901BCCEC3EAE69E969ADB6D2AEBDCFC1CD3B (void);
// 0x0000013B System.Single MoreMountains.Feedbacks.MMFeedbackCameraFieldOfView::get_FeedbackDuration()
extern void MMFeedbackCameraFieldOfView_get_FeedbackDuration_m215E57DF280D2F5C06E695D889FA4BE7840556F3 (void);
// 0x0000013C System.Void MoreMountains.Feedbacks.MMFeedbackCameraFieldOfView::set_FeedbackDuration(System.Single)
extern void MMFeedbackCameraFieldOfView_set_FeedbackDuration_mEE9CE5C02C21F7D9B5FA300FC71962FEFDD80697 (void);
// 0x0000013D System.Void MoreMountains.Feedbacks.MMFeedbackCameraFieldOfView::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackCameraFieldOfView_CustomPlayFeedback_m16DEFBCB6F851CBB9DD67FBA39F50C8F6E72F890 (void);
// 0x0000013E System.Void MoreMountains.Feedbacks.MMFeedbackCameraFieldOfView::CustomStopFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackCameraFieldOfView_CustomStopFeedback_mE1986923FF61C94DF08B6453F3FB5CB8CC44FBD3 (void);
// 0x0000013F System.Void MoreMountains.Feedbacks.MMFeedbackCameraFieldOfView::.ctor()
extern void MMFeedbackCameraFieldOfView__ctor_m418D7427A2D64555E3F9E347EADC1FAAE586A6B2 (void);
// 0x00000140 System.Single MoreMountains.Feedbacks.MMFeedbackCameraOrthographicSize::get_FeedbackDuration()
extern void MMFeedbackCameraOrthographicSize_get_FeedbackDuration_m72AA01A11409AAEFB58048D2A45D0CF33B242711 (void);
// 0x00000141 System.Void MoreMountains.Feedbacks.MMFeedbackCameraOrthographicSize::set_FeedbackDuration(System.Single)
extern void MMFeedbackCameraOrthographicSize_set_FeedbackDuration_m2D2DA88832B78138E3B6C69339388511CC794BC5 (void);
// 0x00000142 System.Void MoreMountains.Feedbacks.MMFeedbackCameraOrthographicSize::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackCameraOrthographicSize_CustomPlayFeedback_m6D8CCB36690519D140C0D95697FDCE87A6813760 (void);
// 0x00000143 System.Void MoreMountains.Feedbacks.MMFeedbackCameraOrthographicSize::CustomStopFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackCameraOrthographicSize_CustomStopFeedback_m4702A4F1120075338BA9D23859471FC23E2F91D1 (void);
// 0x00000144 System.Void MoreMountains.Feedbacks.MMFeedbackCameraOrthographicSize::.ctor()
extern void MMFeedbackCameraOrthographicSize__ctor_m99959D63E367629B21500EF6707085664CC44217 (void);
// 0x00000145 System.Single MoreMountains.Feedbacks.MMFeedbackCameraShake::get_FeedbackDuration()
extern void MMFeedbackCameraShake_get_FeedbackDuration_m283B4279AC7C9335E8540FF1BAADA02146FC3814 (void);
// 0x00000146 System.Void MoreMountains.Feedbacks.MMFeedbackCameraShake::set_FeedbackDuration(System.Single)
extern void MMFeedbackCameraShake_set_FeedbackDuration_m48292E491E37CA9D8836F8DA4267086A25A936CB (void);
// 0x00000147 System.Void MoreMountains.Feedbacks.MMFeedbackCameraShake::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackCameraShake_CustomPlayFeedback_m0E4CAD5D8FE1D27CD7C8B7446809CE86CC8E07F3 (void);
// 0x00000148 System.Void MoreMountains.Feedbacks.MMFeedbackCameraShake::CustomStopFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackCameraShake_CustomStopFeedback_m8904AADF2ED77F1CA4C64378581928B9B6F8E8C3 (void);
// 0x00000149 System.Void MoreMountains.Feedbacks.MMFeedbackCameraShake::.ctor()
extern void MMFeedbackCameraShake__ctor_mF9CCBDD5176B753E0DBECA3E38844DA66680F708 (void);
// 0x0000014A System.Single MoreMountains.Feedbacks.MMFeedbackCameraZoom::get_FeedbackDuration()
extern void MMFeedbackCameraZoom_get_FeedbackDuration_m2523894B8E1AE95D9CF7FD1A1C0241B061963325 (void);
// 0x0000014B System.Void MoreMountains.Feedbacks.MMFeedbackCameraZoom::set_FeedbackDuration(System.Single)
extern void MMFeedbackCameraZoom_set_FeedbackDuration_m246212ADAA2B5DB9AA334975030DFD02AC58CE2E (void);
// 0x0000014C System.Void MoreMountains.Feedbacks.MMFeedbackCameraZoom::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackCameraZoom_CustomPlayFeedback_m783046A566543634BA75DBF0F9D78C5637302640 (void);
// 0x0000014D System.Void MoreMountains.Feedbacks.MMFeedbackCameraZoom::CustomStopFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackCameraZoom_CustomStopFeedback_m38FDF997D3D17698CA0EFD2F9692B533D5BDC247 (void);
// 0x0000014E System.Void MoreMountains.Feedbacks.MMFeedbackCameraZoom::.ctor()
extern void MMFeedbackCameraZoom__ctor_m9CE11AA15D0FC3150A96CD2A2F77CA9A66781D50 (void);
// 0x0000014F System.Void MoreMountains.Feedbacks.MMFeedbackCanvasGroupBlocksRaycasts::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackCanvasGroupBlocksRaycasts_CustomPlayFeedback_m62301B04B27377C797C66A94E8E6F259DA86F076 (void);
// 0x00000150 System.Void MoreMountains.Feedbacks.MMFeedbackCanvasGroupBlocksRaycasts::.ctor()
extern void MMFeedbackCanvasGroupBlocksRaycasts__ctor_mAEC6E90997EEC0856E1AA58765824306B661EFAF (void);
// 0x00000151 System.Single MoreMountains.Feedbacks.MMFeedbackDestinationTransform::get_FeedbackDuration()
extern void MMFeedbackDestinationTransform_get_FeedbackDuration_mCC7ECCEED4C679FC6EEE72CA9398E409DDA7DF89 (void);
// 0x00000152 System.Void MoreMountains.Feedbacks.MMFeedbackDestinationTransform::set_FeedbackDuration(System.Single)
extern void MMFeedbackDestinationTransform_set_FeedbackDuration_m48DEA369690EF3BBE486FADFFA3B55F9C4B90AA3 (void);
// 0x00000153 System.Void MoreMountains.Feedbacks.MMFeedbackDestinationTransform::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackDestinationTransform_CustomPlayFeedback_m7B5A404478DEE37FBFDB3261F145B29B8BAECF4D (void);
// 0x00000154 System.Collections.IEnumerator MoreMountains.Feedbacks.MMFeedbackDestinationTransform::AnimateToDestination()
extern void MMFeedbackDestinationTransform_AnimateToDestination_m479F6EB59B0CA179CA2A863AF81AAE86ED4188E6 (void);
// 0x00000155 System.Void MoreMountains.Feedbacks.MMFeedbackDestinationTransform::CustomStopFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackDestinationTransform_CustomStopFeedback_mC6AC1FE7F60B74D25746F52135EB958A694C53D7 (void);
// 0x00000156 System.Void MoreMountains.Feedbacks.MMFeedbackDestinationTransform::.ctor()
extern void MMFeedbackDestinationTransform__ctor_m166DACE856BEF2785D1DABD3D44F2A6FBB9AAC46 (void);
// 0x00000157 System.Void MoreMountains.Feedbacks.MMFeedbackDestinationTransform/<AnimateToDestination>d__40::.ctor(System.Int32)
extern void U3CAnimateToDestinationU3Ed__40__ctor_m8E597C5E47D4B51D7EF677E4986AE9487AC8B103 (void);
// 0x00000158 System.Void MoreMountains.Feedbacks.MMFeedbackDestinationTransform/<AnimateToDestination>d__40::System.IDisposable.Dispose()
extern void U3CAnimateToDestinationU3Ed__40_System_IDisposable_Dispose_m8CB6F82FE8F28C595144DEF8849AC4EF7E10F3F2 (void);
// 0x00000159 System.Boolean MoreMountains.Feedbacks.MMFeedbackDestinationTransform/<AnimateToDestination>d__40::MoveNext()
extern void U3CAnimateToDestinationU3Ed__40_MoveNext_m462AF3CD42DDE2FB0DE70C84AFC8366BB3BE59AB (void);
// 0x0000015A System.Object MoreMountains.Feedbacks.MMFeedbackDestinationTransform/<AnimateToDestination>d__40::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateToDestinationU3Ed__40_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9EA75D1CAB42ABB2DB716F25D2FFC7C96519A8A1 (void);
// 0x0000015B System.Void MoreMountains.Feedbacks.MMFeedbackDestinationTransform/<AnimateToDestination>d__40::System.Collections.IEnumerator.Reset()
extern void U3CAnimateToDestinationU3Ed__40_System_Collections_IEnumerator_Reset_m308D5C300EA8D602778199AE76CCC9A9F00C0139 (void);
// 0x0000015C System.Object MoreMountains.Feedbacks.MMFeedbackDestinationTransform/<AnimateToDestination>d__40::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateToDestinationU3Ed__40_System_Collections_IEnumerator_get_Current_m9ABDEE4365722E0B7BE6C1E3E9CBE37B51FAA43D (void);
// 0x0000015D System.Void MoreMountains.Feedbacks.MMFeedbackDestroy::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackDestroy_CustomPlayFeedback_m00DC8C244218EB344187C8C739E04E0B604A05F6 (void);
// 0x0000015E System.Void MoreMountains.Feedbacks.MMFeedbackDestroy::ProceedWithDestruction(UnityEngine.GameObject)
extern void MMFeedbackDestroy_ProceedWithDestruction_mFA107E2074F083F539984482A86A6C11FC3C51A5 (void);
// 0x0000015F System.Void MoreMountains.Feedbacks.MMFeedbackDestroy::.ctor()
extern void MMFeedbackDestroy__ctor_mA952EC05CD41F8DBA14984855F6A704159EFED28 (void);
// 0x00000160 System.Void MoreMountains.Feedbacks.MMFeedbackEnable::CustomInitialization(UnityEngine.GameObject)
extern void MMFeedbackEnable_CustomInitialization_mB5E4F810E8239634C022ED1877582B258E97BE49 (void);
// 0x00000161 System.Void MoreMountains.Feedbacks.MMFeedbackEnable::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackEnable_CustomPlayFeedback_m36E01B4CCEA1FC124CB4D5E618858109A5F6D725 (void);
// 0x00000162 System.Void MoreMountains.Feedbacks.MMFeedbackEnable::CustomStopFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackEnable_CustomStopFeedback_m85E697513B74E2BDF737A93370ACAE2EC1C10C7D (void);
// 0x00000163 System.Void MoreMountains.Feedbacks.MMFeedbackEnable::CustomReset()
extern void MMFeedbackEnable_CustomReset_m0AE901EFBABF619B7DC67E5E7EE26DEB53ECDCFA (void);
// 0x00000164 System.Void MoreMountains.Feedbacks.MMFeedbackEnable::SetStatus(MoreMountains.Feedbacks.MMFeedbackEnable/PossibleStates)
extern void MMFeedbackEnable_SetStatus_m050AA63187850EE559103B4BC4C02C86718CDB7A (void);
// 0x00000165 System.Void MoreMountains.Feedbacks.MMFeedbackEnable::.ctor()
extern void MMFeedbackEnable__ctor_m7E43DFB3DD3451268F8E866EBE050BD4CD180CF4 (void);
// 0x00000166 System.Void MoreMountains.Feedbacks.MMFeedbackEvents::CustomInitialization(UnityEngine.GameObject)
extern void MMFeedbackEvents_CustomInitialization_mA593134F949336906E9E622B42526F979CB90D0F (void);
// 0x00000167 System.Void MoreMountains.Feedbacks.MMFeedbackEvents::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackEvents_CustomPlayFeedback_mB61426319A9D9D5F3A0E2ED8252C7558CF916035 (void);
// 0x00000168 System.Void MoreMountains.Feedbacks.MMFeedbackEvents::CustomStopFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackEvents_CustomStopFeedback_m455ED7E79552E9E7D288E40B2BFAEF16430BAC78 (void);
// 0x00000169 System.Void MoreMountains.Feedbacks.MMFeedbackEvents::CustomReset()
extern void MMFeedbackEvents_CustomReset_m4620407B3833A4090B6FF4741BA068C443A0A61F (void);
// 0x0000016A System.Void MoreMountains.Feedbacks.MMFeedbackEvents::.ctor()
extern void MMFeedbackEvents__ctor_m6BBBB623FAE015607BC34B0392745F5CC090540C (void);
// 0x0000016B System.Single MoreMountains.Feedbacks.MMFeedbackFeedbacks::get_FeedbackDuration()
extern void MMFeedbackFeedbacks_get_FeedbackDuration_mC4DD6375C212FC30F9DFEE1888698AC57CED8BC6 (void);
// 0x0000016C System.Void MoreMountains.Feedbacks.MMFeedbackFeedbacks::CustomInitialization(UnityEngine.GameObject)
extern void MMFeedbackFeedbacks_CustomInitialization_mE0C6C38D6B2353BAF2A4C79E8A970C5BB0E155DD (void);
// 0x0000016D System.Void MoreMountains.Feedbacks.MMFeedbackFeedbacks::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackFeedbacks_CustomPlayFeedback_mD164257FE6F7F463E5D8D56159F339D0D699149A (void);
// 0x0000016E System.Void MoreMountains.Feedbacks.MMFeedbackFeedbacks::.ctor()
extern void MMFeedbackFeedbacks__ctor_m78B0DAC1C5B34AE7F96C28F231DEC212064D8946 (void);
// 0x0000016F System.Single MoreMountains.Feedbacks.MMFeedbackFlash::get_FeedbackDuration()
extern void MMFeedbackFlash_get_FeedbackDuration_mCE75BB049C0EA50999985657B342FEADECB9BE16 (void);
// 0x00000170 System.Void MoreMountains.Feedbacks.MMFeedbackFlash::set_FeedbackDuration(System.Single)
extern void MMFeedbackFlash_set_FeedbackDuration_mC4E55473B63888E80E30A99AB1E257E1225FD7F4 (void);
// 0x00000171 System.Void MoreMountains.Feedbacks.MMFeedbackFlash::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackFlash_CustomPlayFeedback_m730DF1CBD2A331DEF14F2AEB56193E8141D91F92 (void);
// 0x00000172 System.Void MoreMountains.Feedbacks.MMFeedbackFlash::CustomStopFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackFlash_CustomStopFeedback_mC29B7F2C109717CD65930E01C6FDD6604D52FF62 (void);
// 0x00000173 System.Void MoreMountains.Feedbacks.MMFeedbackFlash::.ctor()
extern void MMFeedbackFlash__ctor_mDD33BFB7BFC53C9B91020D7C980304D23F30B670 (void);
// 0x00000174 System.Single MoreMountains.Feedbacks.MMFeedbackFlicker::get_FeedbackDuration()
extern void MMFeedbackFlicker_get_FeedbackDuration_m3E67EEDFA105D9DF46876A57AE2E4FFD14C4E30B (void);
// 0x00000175 System.Void MoreMountains.Feedbacks.MMFeedbackFlicker::set_FeedbackDuration(System.Single)
extern void MMFeedbackFlicker_set_FeedbackDuration_m75098F331F62FF449F12C6FBDF897BC13F718A6F (void);
// 0x00000176 System.Void MoreMountains.Feedbacks.MMFeedbackFlicker::CustomInitialization(UnityEngine.GameObject)
extern void MMFeedbackFlicker_CustomInitialization_mDDED4C37CB310F0E15B18B7EB95375D217B2C29E (void);
// 0x00000177 System.Void MoreMountains.Feedbacks.MMFeedbackFlicker::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackFlicker_CustomPlayFeedback_m90BABD0D01DC075064BE0FB9813F80700ECB799D (void);
// 0x00000178 System.Void MoreMountains.Feedbacks.MMFeedbackFlicker::CustomReset()
extern void MMFeedbackFlicker_CustomReset_m2C9D415AAFCE67C8E5FDE44DFE9039E34B0CACE3 (void);
// 0x00000179 System.Collections.IEnumerator MoreMountains.Feedbacks.MMFeedbackFlicker::Flicker(UnityEngine.Renderer,System.Int32,UnityEngine.Color,UnityEngine.Color,System.Single,System.Single)
extern void MMFeedbackFlicker_Flicker_mB6BB6EC8D51B04FB5ADDF24161A6A37F7D1B408C (void);
// 0x0000017A System.Void MoreMountains.Feedbacks.MMFeedbackFlicker::SetColor(System.Int32,UnityEngine.Color)
extern void MMFeedbackFlicker_SetColor_mF5F710072C6F51051A0CFFCE81B84509E4B096D2 (void);
// 0x0000017B System.Void MoreMountains.Feedbacks.MMFeedbackFlicker::CustomStopFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackFlicker_CustomStopFeedback_mC7E355FCF799FB1B7C81B0BB392CDFC66C79E615 (void);
// 0x0000017C System.Void MoreMountains.Feedbacks.MMFeedbackFlicker::.ctor()
extern void MMFeedbackFlicker__ctor_mDBE7014EB42C36C9499306DC9A7139650803351E (void);
// 0x0000017D System.Void MoreMountains.Feedbacks.MMFeedbackFlicker/<Flicker>d__21::.ctor(System.Int32)
extern void U3CFlickerU3Ed__21__ctor_m6F8921D8CC97152EB3E62B866EF935DA55CFE901 (void);
// 0x0000017E System.Void MoreMountains.Feedbacks.MMFeedbackFlicker/<Flicker>d__21::System.IDisposable.Dispose()
extern void U3CFlickerU3Ed__21_System_IDisposable_Dispose_mF19AEAF1C7841AACCFCF9F8D1FD42FA43758541F (void);
// 0x0000017F System.Boolean MoreMountains.Feedbacks.MMFeedbackFlicker/<Flicker>d__21::MoveNext()
extern void U3CFlickerU3Ed__21_MoveNext_m4B0C7815B4B19322A7EC47E63EE5B0CB87890493 (void);
// 0x00000180 System.Object MoreMountains.Feedbacks.MMFeedbackFlicker/<Flicker>d__21::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFlickerU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m722B53DCB38F58D5222B225654F18432ADC21387 (void);
// 0x00000181 System.Void MoreMountains.Feedbacks.MMFeedbackFlicker/<Flicker>d__21::System.Collections.IEnumerator.Reset()
extern void U3CFlickerU3Ed__21_System_Collections_IEnumerator_Reset_m5F7BEC98818D9D640D6EA54BB956A0893B9364F8 (void);
// 0x00000182 System.Object MoreMountains.Feedbacks.MMFeedbackFlicker/<Flicker>d__21::System.Collections.IEnumerator.get_Current()
extern void U3CFlickerU3Ed__21_System_Collections_IEnumerator_get_Current_mAAAAD7521E2EF5896A54B3D93D3AE9AD7887BAC0 (void);
// 0x00000183 System.Single MoreMountains.Feedbacks.MMFeedbackFreezeFrame::get_FeedbackDuration()
extern void MMFeedbackFreezeFrame_get_FeedbackDuration_m6ADBBFE77144C965D20CAD6C1A48E72A65CE94BE (void);
// 0x00000184 System.Void MoreMountains.Feedbacks.MMFeedbackFreezeFrame::set_FeedbackDuration(System.Single)
extern void MMFeedbackFreezeFrame_set_FeedbackDuration_mDE35C1F80D6DBE8F65A6847558010131CE355D90 (void);
// 0x00000185 System.Void MoreMountains.Feedbacks.MMFeedbackFreezeFrame::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackFreezeFrame_CustomPlayFeedback_m97A3564570E9CA3AEF14FF559284189EEACB13FD (void);
// 0x00000186 System.Void MoreMountains.Feedbacks.MMFeedbackFreezeFrame::.ctor()
extern void MMFeedbackFreezeFrame__ctor_mF96EFB23BB42266E070F88669631C4508B14E971 (void);
// 0x00000187 System.Boolean MoreMountains.Feedbacks.MMFeedbackHoldingPause::get_HoldingPause()
extern void MMFeedbackHoldingPause_get_HoldingPause_mED6CF75663006193CE6C74316DECD71153C94C8F (void);
// 0x00000188 System.Single MoreMountains.Feedbacks.MMFeedbackHoldingPause::get_FeedbackDuration()
extern void MMFeedbackHoldingPause_get_FeedbackDuration_m88B8F60AB42640FF5826C9C3C10B1987665F7166 (void);
// 0x00000189 System.Void MoreMountains.Feedbacks.MMFeedbackHoldingPause::set_FeedbackDuration(System.Single)
extern void MMFeedbackHoldingPause_set_FeedbackDuration_mA8FDBCF907D95AA2EEAC4B1E9714D67F165C22DF (void);
// 0x0000018A System.Void MoreMountains.Feedbacks.MMFeedbackHoldingPause::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackHoldingPause_CustomPlayFeedback_m17606D5BA214761CADC9021EA85060BB5C51464A (void);
// 0x0000018B System.Void MoreMountains.Feedbacks.MMFeedbackHoldingPause::.ctor()
extern void MMFeedbackHoldingPause__ctor_m6533300D5B5BFFAEBF145D432EE2DD94A702B62C (void);
// 0x0000018C System.Single MoreMountains.Feedbacks.MMFeedbackImage::get_FeedbackDuration()
extern void MMFeedbackImage_get_FeedbackDuration_mFFC5DA6BF0FADDF551FC8670D1B9134DFB70698D (void);
// 0x0000018D System.Void MoreMountains.Feedbacks.MMFeedbackImage::set_FeedbackDuration(System.Single)
extern void MMFeedbackImage_set_FeedbackDuration_mB1A12D73570BCD632B6EDD31196C808F0B010609 (void);
// 0x0000018E System.Void MoreMountains.Feedbacks.MMFeedbackImage::CustomInitialization(UnityEngine.GameObject)
extern void MMFeedbackImage_CustomInitialization_m38F907BC271B94EA078181AEAC1AF9B0257F63EF (void);
// 0x0000018F System.Void MoreMountains.Feedbacks.MMFeedbackImage::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackImage_CustomPlayFeedback_m3B92DF2751EA22B62BD1FF56267DFB61DA311653 (void);
// 0x00000190 System.Collections.IEnumerator MoreMountains.Feedbacks.MMFeedbackImage::ImageSequence()
extern void MMFeedbackImage_ImageSequence_m0983D6404F04D6697C00C3845050BE0A7954A53A (void);
// 0x00000191 System.Void MoreMountains.Feedbacks.MMFeedbackImage::SetImageValues(System.Single)
extern void MMFeedbackImage_SetImageValues_m7D856CF9BD723B045DC04B2832898505F617F37B (void);
// 0x00000192 System.Void MoreMountains.Feedbacks.MMFeedbackImage::CustomStopFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackImage_CustomStopFeedback_mA3B39BC8FD411B1CC7CB12D9C8D8F7B3B5D13ECA (void);
// 0x00000193 System.Void MoreMountains.Feedbacks.MMFeedbackImage::Turn(System.Boolean)
extern void MMFeedbackImage_Turn_m3B348627A750D65ED9DAD8CAA6ABCC78C8517433 (void);
// 0x00000194 System.Void MoreMountains.Feedbacks.MMFeedbackImage::.ctor()
extern void MMFeedbackImage__ctor_mF136B20823F51E24DE223E8958629677A7B1DD2B (void);
// 0x00000195 System.Void MoreMountains.Feedbacks.MMFeedbackImage/<ImageSequence>d__21::.ctor(System.Int32)
extern void U3CImageSequenceU3Ed__21__ctor_m644622C038CDC21631A6C6F0AD1DF1A154116867 (void);
// 0x00000196 System.Void MoreMountains.Feedbacks.MMFeedbackImage/<ImageSequence>d__21::System.IDisposable.Dispose()
extern void U3CImageSequenceU3Ed__21_System_IDisposable_Dispose_m31E455F7DC0F615B0A1D04491541DF105048CEDB (void);
// 0x00000197 System.Boolean MoreMountains.Feedbacks.MMFeedbackImage/<ImageSequence>d__21::MoveNext()
extern void U3CImageSequenceU3Ed__21_MoveNext_m27D652A2525E052A66D871BB62B2AB00112CDDCA (void);
// 0x00000198 System.Object MoreMountains.Feedbacks.MMFeedbackImage/<ImageSequence>d__21::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CImageSequenceU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m245A51C921DFFDCE82A884049C5E1AFA177DAE65 (void);
// 0x00000199 System.Void MoreMountains.Feedbacks.MMFeedbackImage/<ImageSequence>d__21::System.Collections.IEnumerator.Reset()
extern void U3CImageSequenceU3Ed__21_System_Collections_IEnumerator_Reset_m2CE5EDCC78D87D6B67626261522C724687F0CDE7 (void);
// 0x0000019A System.Object MoreMountains.Feedbacks.MMFeedbackImage/<ImageSequence>d__21::System.Collections.IEnumerator.get_Current()
extern void U3CImageSequenceU3Ed__21_System_Collections_IEnumerator_get_Current_mB276F1856BF5886B2517F23731BDE346A49AC1BA (void);
// 0x0000019B System.Void MoreMountains.Feedbacks.MMFeedbackImageRaycastTarget::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackImageRaycastTarget_CustomPlayFeedback_m6F4BEF0A0C07AA74E96D21EF7A5D8633A0768E33 (void);
// 0x0000019C System.Void MoreMountains.Feedbacks.MMFeedbackImageRaycastTarget::.ctor()
extern void MMFeedbackImageRaycastTarget__ctor_mF21908F318EF17DA60F2200F0FE2DCD651336177 (void);
// 0x0000019D System.Void MoreMountains.Feedbacks.MMFeedbackInstantiateObject::CustomInitialization(UnityEngine.GameObject)
extern void MMFeedbackInstantiateObject_CustomInitialization_m104AD8BAE9C39B03303E64649F3492F2F50EA79C (void);
// 0x0000019E System.Void MoreMountains.Feedbacks.MMFeedbackInstantiateObject::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackInstantiateObject_CustomPlayFeedback_m87F40BC7878C61FC6D8AFFDBD4550CF7C16F2FE0 (void);
// 0x0000019F System.Void MoreMountains.Feedbacks.MMFeedbackInstantiateObject::PositionObject(UnityEngine.Vector3)
extern void MMFeedbackInstantiateObject_PositionObject_m3EC28B536CFCBEF6802BBF1FCE5631969DF676D3 (void);
// 0x000001A0 UnityEngine.Vector3 MoreMountains.Feedbacks.MMFeedbackInstantiateObject::GetPosition(UnityEngine.Vector3)
extern void MMFeedbackInstantiateObject_GetPosition_mC19A3BC52E6F6AC0B842370B7157279BE71DA33C (void);
// 0x000001A1 UnityEngine.Quaternion MoreMountains.Feedbacks.MMFeedbackInstantiateObject::GetRotation()
extern void MMFeedbackInstantiateObject_GetRotation_m0CD1FC5D4267BF2ADAD638396C0C271435510AD7 (void);
// 0x000001A2 UnityEngine.Vector3 MoreMountains.Feedbacks.MMFeedbackInstantiateObject::GetScale()
extern void MMFeedbackInstantiateObject_GetScale_m5A5216D413FB073D7DA3BA100376E7EDE2CF878A (void);
// 0x000001A3 System.Void MoreMountains.Feedbacks.MMFeedbackInstantiateObject::.ctor()
extern void MMFeedbackInstantiateObject__ctor_m6E1E936D7CB750A3DFC500199DA39B0F9ED0C53F (void);
// 0x000001A4 System.Single MoreMountains.Feedbacks.MMFeedbackLight::get_FeedbackDuration()
extern void MMFeedbackLight_get_FeedbackDuration_m085AC6856302EC9546A6BAE6E42C84CA4252DBF3 (void);
// 0x000001A5 System.Void MoreMountains.Feedbacks.MMFeedbackLight::set_FeedbackDuration(System.Single)
extern void MMFeedbackLight_set_FeedbackDuration_m9F5537835C36D31E05711BCE10388BFA59F51EBA (void);
// 0x000001A6 System.Void MoreMountains.Feedbacks.MMFeedbackLight::CustomInitialization(UnityEngine.GameObject)
extern void MMFeedbackLight_CustomInitialization_m5359DEE0E5CF4FB480AEA0052D09D34DB4FB4D31 (void);
// 0x000001A7 System.Void MoreMountains.Feedbacks.MMFeedbackLight::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackLight_CustomPlayFeedback_m834DF756BDE0244ACD94EC2B989FC86AC095108E (void);
// 0x000001A8 System.Collections.IEnumerator MoreMountains.Feedbacks.MMFeedbackLight::LightSequence(System.Single)
extern void MMFeedbackLight_LightSequence_m5228494B9651582CD9FD859979A28060D215DD30 (void);
// 0x000001A9 System.Void MoreMountains.Feedbacks.MMFeedbackLight::SetLightValues(System.Single,System.Single)
extern void MMFeedbackLight_SetLightValues_m8DABFA36687C341956703012B71E07DCC2A75467 (void);
// 0x000001AA System.Void MoreMountains.Feedbacks.MMFeedbackLight::CustomStopFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackLight_CustomStopFeedback_m3942300B48AA6DEC4259CA4B323FBA1EBC8ED477 (void);
// 0x000001AB System.Void MoreMountains.Feedbacks.MMFeedbackLight::Turn(System.Boolean)
extern void MMFeedbackLight_Turn_mA52FF854EE6435E9A4DF0701C2755D68F437F937 (void);
// 0x000001AC System.Void MoreMountains.Feedbacks.MMFeedbackLight::.ctor()
extern void MMFeedbackLight__ctor_m6C909AC21CE83092C619DB4E5151D8C13DCF2444 (void);
// 0x000001AD System.Void MoreMountains.Feedbacks.MMFeedbackLight/<LightSequence>d__38::.ctor(System.Int32)
extern void U3CLightSequenceU3Ed__38__ctor_m59193416E1320E02C5F85794717BC51E397C9912 (void);
// 0x000001AE System.Void MoreMountains.Feedbacks.MMFeedbackLight/<LightSequence>d__38::System.IDisposable.Dispose()
extern void U3CLightSequenceU3Ed__38_System_IDisposable_Dispose_m95C894B6744861F940BDBE83F7B315B79BFCC385 (void);
// 0x000001AF System.Boolean MoreMountains.Feedbacks.MMFeedbackLight/<LightSequence>d__38::MoveNext()
extern void U3CLightSequenceU3Ed__38_MoveNext_m5526294317C1B14E64CFB7269BC02F35E99E8C26 (void);
// 0x000001B0 System.Object MoreMountains.Feedbacks.MMFeedbackLight/<LightSequence>d__38::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLightSequenceU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4946412FC2EE2DA2AD890B76FA67726962209B06 (void);
// 0x000001B1 System.Void MoreMountains.Feedbacks.MMFeedbackLight/<LightSequence>d__38::System.Collections.IEnumerator.Reset()
extern void U3CLightSequenceU3Ed__38_System_Collections_IEnumerator_Reset_m73F65ED932CA56C5D6FDE3D3B0E6F258BB9C3280 (void);
// 0x000001B2 System.Object MoreMountains.Feedbacks.MMFeedbackLight/<LightSequence>d__38::System.Collections.IEnumerator.get_Current()
extern void U3CLightSequenceU3Ed__38_System_Collections_IEnumerator_get_Current_mB3B9DEF4BFB983C67B7D4B34363D14BC6982ACBC (void);
// 0x000001B3 System.Boolean MoreMountains.Feedbacks.MMFeedbackLooper::get_LooperPause()
extern void MMFeedbackLooper_get_LooperPause_m3A77F9CE20ACA7BA515DD5C73F4CE4ACF564C68F (void);
// 0x000001B4 System.Single MoreMountains.Feedbacks.MMFeedbackLooper::get_FeedbackDuration()
extern void MMFeedbackLooper_get_FeedbackDuration_m3AC17B24AF95EDA7932B2E738F402992CFB22129 (void);
// 0x000001B5 System.Void MoreMountains.Feedbacks.MMFeedbackLooper::set_FeedbackDuration(System.Single)
extern void MMFeedbackLooper_set_FeedbackDuration_m36C6098E75A6F5CA40ACA422EBB85B7ED3DA5AE2 (void);
// 0x000001B6 System.Void MoreMountains.Feedbacks.MMFeedbackLooper::CustomInitialization(UnityEngine.GameObject)
extern void MMFeedbackLooper_CustomInitialization_mA5E971822E1398AA20C47CF71CAB5DF13AC7D141 (void);
// 0x000001B7 System.Void MoreMountains.Feedbacks.MMFeedbackLooper::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackLooper_CustomPlayFeedback_m3E2774EBB1BD68D9468841D38B39A9B23BA1671B (void);
// 0x000001B8 System.Void MoreMountains.Feedbacks.MMFeedbackLooper::CustomStopFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackLooper_CustomStopFeedback_mE5CC7C8EAD5B3E316786FC35774A13955DC28FC4 (void);
// 0x000001B9 System.Void MoreMountains.Feedbacks.MMFeedbackLooper::CustomReset()
extern void MMFeedbackLooper_CustomReset_mA94A67FAD487F2E86B30F968A16D442BB4FCA89E (void);
// 0x000001BA System.Void MoreMountains.Feedbacks.MMFeedbackLooper::.ctor()
extern void MMFeedbackLooper__ctor_mB04DFE85B90D81C20E31A4260BF3E954ED4E0439 (void);
// 0x000001BB System.Boolean MoreMountains.Feedbacks.MMFeedbackLooperStart::get_LooperStart()
extern void MMFeedbackLooperStart_get_LooperStart_mA32D2F44FFB40E1EF651F8C8585CFBCCE846A6FF (void);
// 0x000001BC System.Single MoreMountains.Feedbacks.MMFeedbackLooperStart::get_FeedbackDuration()
extern void MMFeedbackLooperStart_get_FeedbackDuration_mC4F25969B5452C2078BC3572BF8A6C0FD6B446B0 (void);
// 0x000001BD System.Void MoreMountains.Feedbacks.MMFeedbackLooperStart::set_FeedbackDuration(System.Single)
extern void MMFeedbackLooperStart_set_FeedbackDuration_mF75A934E9B85D68E874C55769FD8F9BB08F8F756 (void);
// 0x000001BE System.Void MoreMountains.Feedbacks.MMFeedbackLooperStart::Reset()
extern void MMFeedbackLooperStart_Reset_m4BFD474E20A8C9B9902D917237762EEFD503219D (void);
// 0x000001BF System.Void MoreMountains.Feedbacks.MMFeedbackLooperStart::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackLooperStart_CustomPlayFeedback_m9AD1BF88273A83AAC05C5A09AC5D5222F39B77F7 (void);
// 0x000001C0 System.Void MoreMountains.Feedbacks.MMFeedbackLooperStart::.ctor()
extern void MMFeedbackLooperStart__ctor_mCB9E3DEAAB55F129A9D2BDCD70FC5281823106E3 (void);
// 0x000001C1 System.Single MoreMountains.Feedbacks.MMFeedbackMaterial::get_FeedbackDuration()
extern void MMFeedbackMaterial_get_FeedbackDuration_m57BFFA1D26F62DE393DF2751B686062FC8C31F4A (void);
// 0x000001C2 System.Void MoreMountains.Feedbacks.MMFeedbackMaterial::set_FeedbackDuration(System.Single)
extern void MMFeedbackMaterial_set_FeedbackDuration_mA199FFF43F1F885E8EF89B6B60EF6E4DE00DE721 (void);
// 0x000001C3 System.Single MoreMountains.Feedbacks.MMFeedbackMaterial::GetTime()
extern void MMFeedbackMaterial_GetTime_m9506396177743B0BF6F867E59E193E2E029533C3 (void);
// 0x000001C4 System.Single MoreMountains.Feedbacks.MMFeedbackMaterial::GetDeltaTime()
extern void MMFeedbackMaterial_GetDeltaTime_mA49CE814BC714C9E429B47CF2699C9DF2F4A1732 (void);
// 0x000001C5 System.Void MoreMountains.Feedbacks.MMFeedbackMaterial::CustomInitialization(UnityEngine.GameObject)
extern void MMFeedbackMaterial_CustomInitialization_mF3034F1CEE4A776FF8D8FCC6DE629E6F36B48DC0 (void);
// 0x000001C6 System.Void MoreMountains.Feedbacks.MMFeedbackMaterial::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackMaterial_CustomPlayFeedback_m9DA98D450D9395738ED0C06D19009C9835343C22 (void);
// 0x000001C7 System.Collections.IEnumerator MoreMountains.Feedbacks.MMFeedbackMaterial::TransitionMaterial(UnityEngine.Material,UnityEngine.Material)
extern void MMFeedbackMaterial_TransitionMaterial_mA7C6FE1C24AC91C6993AA88689FE33CCDE4B5B1E (void);
// 0x000001C8 System.Int32 MoreMountains.Feedbacks.MMFeedbackMaterial::DetermineNextIndex()
extern void MMFeedbackMaterial_DetermineNextIndex_mA4EDD19E944C005038D381EA91DFBDBA00EF0685 (void);
// 0x000001C9 System.Void MoreMountains.Feedbacks.MMFeedbackMaterial::CustomStopFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackMaterial_CustomStopFeedback_m7CDD0D158CCBFDC86006FAC2B59C197D6EEC8D5A (void);
// 0x000001CA System.Void MoreMountains.Feedbacks.MMFeedbackMaterial::.ctor()
extern void MMFeedbackMaterial__ctor_mA1595E14B4DD3547C7AB09DB89659D1085DD2095 (void);
// 0x000001CB System.Void MoreMountains.Feedbacks.MMFeedbackMaterial/<TransitionMaterial>d__20::.ctor(System.Int32)
extern void U3CTransitionMaterialU3Ed__20__ctor_m9FC4A446768F6604DC08CEFD597B44AA4625FC68 (void);
// 0x000001CC System.Void MoreMountains.Feedbacks.MMFeedbackMaterial/<TransitionMaterial>d__20::System.IDisposable.Dispose()
extern void U3CTransitionMaterialU3Ed__20_System_IDisposable_Dispose_m16E93E1AE6ED44F90D813C1DE4621FB808E38075 (void);
// 0x000001CD System.Boolean MoreMountains.Feedbacks.MMFeedbackMaterial/<TransitionMaterial>d__20::MoveNext()
extern void U3CTransitionMaterialU3Ed__20_MoveNext_mBC5CF21EB3934BAFC097CE893B61D965F1857B52 (void);
// 0x000001CE System.Object MoreMountains.Feedbacks.MMFeedbackMaterial/<TransitionMaterial>d__20::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTransitionMaterialU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA51A692387A295AA63AE2D5182D2D8772C9697EB (void);
// 0x000001CF System.Void MoreMountains.Feedbacks.MMFeedbackMaterial/<TransitionMaterial>d__20::System.Collections.IEnumerator.Reset()
extern void U3CTransitionMaterialU3Ed__20_System_Collections_IEnumerator_Reset_m7596BED3EA32CF2B64D44AE3239859945E96CF68 (void);
// 0x000001D0 System.Object MoreMountains.Feedbacks.MMFeedbackMaterial/<TransitionMaterial>d__20::System.Collections.IEnumerator.get_Current()
extern void U3CTransitionMaterialU3Ed__20_System_Collections_IEnumerator_get_Current_m13F75D2BDBBF60CF16B5D86C614EEE7542B1AE48 (void);
// 0x000001D1 System.Void MoreMountains.Feedbacks.MMFeedbackParticles::CustomInitialization(UnityEngine.GameObject)
extern void MMFeedbackParticles_CustomInitialization_m00FF0EF6DB7B1636CEDDE46C1E8DAE2B83206AE2 (void);
// 0x000001D2 System.Void MoreMountains.Feedbacks.MMFeedbackParticles::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackParticles_CustomPlayFeedback_mBC5BA56FA0582F39E545E127F57BEC3BA14CFCFD (void);
// 0x000001D3 System.Void MoreMountains.Feedbacks.MMFeedbackParticles::CustomStopFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackParticles_CustomStopFeedback_mB464677A8AE9317B14A27FCDBEC4033B30E4ABCE (void);
// 0x000001D4 System.Void MoreMountains.Feedbacks.MMFeedbackParticles::CustomReset()
extern void MMFeedbackParticles_CustomReset_m12CF1D8F8D576A64AF0A716876FED5CB9B3656F7 (void);
// 0x000001D5 System.Void MoreMountains.Feedbacks.MMFeedbackParticles::PlayParticles(UnityEngine.Vector3)
extern void MMFeedbackParticles_PlayParticles_m2D0E99509DF2F7E51345BB092C6A6BAFC13D4CFE (void);
// 0x000001D6 System.Void MoreMountains.Feedbacks.MMFeedbackParticles::StopParticles()
extern void MMFeedbackParticles_StopParticles_m4D7617404315C06B83B1E3EA7A96F30579FBDF27 (void);
// 0x000001D7 System.Void MoreMountains.Feedbacks.MMFeedbackParticles::.ctor()
extern void MMFeedbackParticles__ctor_mCAFF0B8996D353951AE5F17C093F46710D7F6E7E (void);
// 0x000001D8 System.Void MoreMountains.Feedbacks.MMFeedbackParticlesInstantiation::CustomInitialization(UnityEngine.GameObject)
extern void MMFeedbackParticlesInstantiation_CustomInitialization_m12F5BB99EF9F5FE07D7673A301CDD91CD6C55765 (void);
// 0x000001D9 System.Void MoreMountains.Feedbacks.MMFeedbackParticlesInstantiation::InstantiateParticleSystem()
extern void MMFeedbackParticlesInstantiation_InstantiateParticleSystem_mAB560135D6DA9AA6AE08243D742EC89BCB5A6BE9 (void);
// 0x000001DA System.Void MoreMountains.Feedbacks.MMFeedbackParticlesInstantiation::PositionParticleSystem(UnityEngine.ParticleSystem)
extern void MMFeedbackParticlesInstantiation_PositionParticleSystem_m0D754B07F551E25FFAD565515675916E4265493B (void);
// 0x000001DB UnityEngine.Quaternion MoreMountains.Feedbacks.MMFeedbackParticlesInstantiation::GetRotation(UnityEngine.Transform)
extern void MMFeedbackParticlesInstantiation_GetRotation_m4B68F5C1235A00B01847F1231DBB8A7CFE55CCAC (void);
// 0x000001DC UnityEngine.Vector3 MoreMountains.Feedbacks.MMFeedbackParticlesInstantiation::GetScale(UnityEngine.Transform)
extern void MMFeedbackParticlesInstantiation_GetScale_m4D36DD1FA7716246FE9CDD3CDF6234E9A4EDCEED (void);
// 0x000001DD UnityEngine.Vector3 MoreMountains.Feedbacks.MMFeedbackParticlesInstantiation::GetPosition(UnityEngine.Vector3)
extern void MMFeedbackParticlesInstantiation_GetPosition_mDCF1D0285E232F4347F617334FB69C2D84964CB9 (void);
// 0x000001DE System.Void MoreMountains.Feedbacks.MMFeedbackParticlesInstantiation::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackParticlesInstantiation_CustomPlayFeedback_mA806CD6A024F422D76910262D15170F9ED7B87FA (void);
// 0x000001DF System.Void MoreMountains.Feedbacks.MMFeedbackParticlesInstantiation::CustomStopFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackParticlesInstantiation_CustomStopFeedback_mBED162B0F8BC0354545A62D7741890A8444BE6C6 (void);
// 0x000001E0 System.Void MoreMountains.Feedbacks.MMFeedbackParticlesInstantiation::CustomReset()
extern void MMFeedbackParticlesInstantiation_CustomReset_m434E629E77047348F50E6023D17D1D2C69F321B9 (void);
// 0x000001E1 System.Void MoreMountains.Feedbacks.MMFeedbackParticlesInstantiation::.ctor()
extern void MMFeedbackParticlesInstantiation__ctor_m9FC56BF9CB458C79EC516810E928538506258CBD (void);
// 0x000001E2 System.Collections.IEnumerator MoreMountains.Feedbacks.MMFeedbackPause::get_Pause()
extern void MMFeedbackPause_get_Pause_m5910BEF324A488BCC27427338A31A1D8DEE8D0A0 (void);
// 0x000001E3 System.Single MoreMountains.Feedbacks.MMFeedbackPause::get_FeedbackDuration()
extern void MMFeedbackPause_get_FeedbackDuration_mC3DCC8BA6C24B4537B48DC4FDE7EA3B756407237 (void);
// 0x000001E4 System.Void MoreMountains.Feedbacks.MMFeedbackPause::set_FeedbackDuration(System.Single)
extern void MMFeedbackPause_set_FeedbackDuration_m2FC5BE236B07645665875B4306220E017109830E (void);
// 0x000001E5 System.Collections.IEnumerator MoreMountains.Feedbacks.MMFeedbackPause::PauseWait()
extern void MMFeedbackPause_PauseWait_m6D91157FCDD2AB1872BE0ED9760B201B0A72F318 (void);
// 0x000001E6 System.Void MoreMountains.Feedbacks.MMFeedbackPause::CustomInitialization(UnityEngine.GameObject)
extern void MMFeedbackPause_CustomInitialization_mF17BED69AB8720429057AE47E0BEB39463108339 (void);
// 0x000001E7 System.Void MoreMountains.Feedbacks.MMFeedbackPause::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackPause_CustomPlayFeedback_m53318C83E28CBEB7FFF87385861642D1212C3BAE (void);
// 0x000001E8 System.Collections.IEnumerator MoreMountains.Feedbacks.MMFeedbackPause::PlayPause()
extern void MMFeedbackPause_PlayPause_m4517993403D86D98B927187460F8248CE905980C (void);
// 0x000001E9 System.Void MoreMountains.Feedbacks.MMFeedbackPause::.ctor()
extern void MMFeedbackPause__ctor_m6D2446F76328923CB421EF492AC28057D9443E87 (void);
// 0x000001EA System.Void MoreMountains.Feedbacks.MMFeedbackPause/<PlayPause>d__16::.ctor(System.Int32)
extern void U3CPlayPauseU3Ed__16__ctor_m1D921774D1B3523F36E770BFFE3DEF0E9168CC18 (void);
// 0x000001EB System.Void MoreMountains.Feedbacks.MMFeedbackPause/<PlayPause>d__16::System.IDisposable.Dispose()
extern void U3CPlayPauseU3Ed__16_System_IDisposable_Dispose_mB5A11948942DBA4086A5005A12508498F3DF8765 (void);
// 0x000001EC System.Boolean MoreMountains.Feedbacks.MMFeedbackPause/<PlayPause>d__16::MoveNext()
extern void U3CPlayPauseU3Ed__16_MoveNext_m890434F2361A450F5C115E5485B4F6995295E05A (void);
// 0x000001ED System.Object MoreMountains.Feedbacks.MMFeedbackPause/<PlayPause>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPlayPauseU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8D1B6475D31078E1AEE1729A075BA01226C00551 (void);
// 0x000001EE System.Void MoreMountains.Feedbacks.MMFeedbackPause/<PlayPause>d__16::System.Collections.IEnumerator.Reset()
extern void U3CPlayPauseU3Ed__16_System_Collections_IEnumerator_Reset_m679EC44C67BE9503BA41624580C7EAF0BE077A3C (void);
// 0x000001EF System.Object MoreMountains.Feedbacks.MMFeedbackPause/<PlayPause>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CPlayPauseU3Ed__16_System_Collections_IEnumerator_get_Current_m6BEAE6E7FD360205EC5E5CFE0AC8EA18DDE34BE2 (void);
// 0x000001F0 System.Single MoreMountains.Feedbacks.MMFeedbackPosition::get_FeedbackDuration()
extern void MMFeedbackPosition_get_FeedbackDuration_m41C0AC6ED63ED23F9F99B1DC7A300632FB234391 (void);
// 0x000001F1 System.Void MoreMountains.Feedbacks.MMFeedbackPosition::set_FeedbackDuration(System.Single)
extern void MMFeedbackPosition_set_FeedbackDuration_mA068AFF0FDA1FECC01BEC075C0A21C826581F870 (void);
// 0x000001F2 System.Void MoreMountains.Feedbacks.MMFeedbackPosition::CustomInitialization(UnityEngine.GameObject)
extern void MMFeedbackPosition_CustomInitialization_m508BAD11CBDBE402F6836901BD0FD9300765D636 (void);
// 0x000001F3 System.Void MoreMountains.Feedbacks.MMFeedbackPosition::DeterminePositions()
extern void MMFeedbackPosition_DeterminePositions_mB91310BB35D25B0423D5B89831A7028423B5FE57 (void);
// 0x000001F4 System.Void MoreMountains.Feedbacks.MMFeedbackPosition::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackPosition_CustomPlayFeedback_mC9C87A5C1F12BFB7F91B2DA5296BDA6C55FEF5F3 (void);
// 0x000001F5 System.Collections.IEnumerator MoreMountains.Feedbacks.MMFeedbackPosition::MoveAlongCurve(UnityEngine.GameObject,UnityEngine.Vector3,System.Single,System.Single)
extern void MMFeedbackPosition_MoveAlongCurve_m12F14693C26DDE4DD83358F5599909FE92626CC3 (void);
// 0x000001F6 System.Void MoreMountains.Feedbacks.MMFeedbackPosition::ComputeNewCurvePosition(UnityEngine.GameObject,UnityEngine.Vector3,System.Single,System.Single)
extern void MMFeedbackPosition_ComputeNewCurvePosition_mDBDCBAAF44F7F4C9E3B60B75C0E1DFA4F4CAEA72 (void);
// 0x000001F7 System.Collections.IEnumerator MoreMountains.Feedbacks.MMFeedbackPosition::MoveFromTo(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.AnimationCurve)
extern void MMFeedbackPosition_MoveFromTo_mFCEA35167012B6AA620CADD2B60A9AC08791E79B (void);
// 0x000001F8 UnityEngine.Vector3 MoreMountains.Feedbacks.MMFeedbackPosition::GetPosition(UnityEngine.Transform)
extern void MMFeedbackPosition_GetPosition_mB6D4B0CE2D68A4FA3BD0A3E78F47D2B6740B335D (void);
// 0x000001F9 System.Void MoreMountains.Feedbacks.MMFeedbackPosition::SetPosition(UnityEngine.Transform,UnityEngine.Vector3)
extern void MMFeedbackPosition_SetPosition_m3277ACE8ECA68A284CE918BC227CE0982C8E4B00 (void);
// 0x000001FA System.Void MoreMountains.Feedbacks.MMFeedbackPosition::CustomStopFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackPosition_CustomStopFeedback_m8746CB88773DB13B1C9972A624CD1958D34A57BD (void);
// 0x000001FB System.Void MoreMountains.Feedbacks.MMFeedbackPosition::OnDisable()
extern void MMFeedbackPosition_OnDisable_mF8BC49A5519EE3CADE74208EDED5CFD8163D6545 (void);
// 0x000001FC System.Void MoreMountains.Feedbacks.MMFeedbackPosition::.ctor()
extern void MMFeedbackPosition__ctor_m5F28D587A6B63A33FE9C66D44A52B54898AB9DE5 (void);
// 0x000001FD System.Void MoreMountains.Feedbacks.MMFeedbackPosition/<MoveAlongCurve>d__35::.ctor(System.Int32)
extern void U3CMoveAlongCurveU3Ed__35__ctor_m73CE0900885E23DE43E65F74DAF231580CCF3AB3 (void);
// 0x000001FE System.Void MoreMountains.Feedbacks.MMFeedbackPosition/<MoveAlongCurve>d__35::System.IDisposable.Dispose()
extern void U3CMoveAlongCurveU3Ed__35_System_IDisposable_Dispose_mA8FD281D0543F631E97D8F90E0CEAAC043196D1E (void);
// 0x000001FF System.Boolean MoreMountains.Feedbacks.MMFeedbackPosition/<MoveAlongCurve>d__35::MoveNext()
extern void U3CMoveAlongCurveU3Ed__35_MoveNext_m1A44A040AC9DB7BC4A3764DF57B430DCE1402F74 (void);
// 0x00000200 System.Object MoreMountains.Feedbacks.MMFeedbackPosition/<MoveAlongCurve>d__35::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CMoveAlongCurveU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCE714F42F97BA96AB7C550F6ABD3B257BF43937B (void);
// 0x00000201 System.Void MoreMountains.Feedbacks.MMFeedbackPosition/<MoveAlongCurve>d__35::System.Collections.IEnumerator.Reset()
extern void U3CMoveAlongCurveU3Ed__35_System_Collections_IEnumerator_Reset_m176344A6644E8EA051D3011378AB424DC3EABE55 (void);
// 0x00000202 System.Object MoreMountains.Feedbacks.MMFeedbackPosition/<MoveAlongCurve>d__35::System.Collections.IEnumerator.get_Current()
extern void U3CMoveAlongCurveU3Ed__35_System_Collections_IEnumerator_get_Current_m91D855D9E3FC86E3BE30C167E56AED1481D87508 (void);
// 0x00000203 System.Void MoreMountains.Feedbacks.MMFeedbackPosition/<MoveFromTo>d__37::.ctor(System.Int32)
extern void U3CMoveFromToU3Ed__37__ctor_mCCFAF53BBE7674E5691149B59CA1A43453C20DE9 (void);
// 0x00000204 System.Void MoreMountains.Feedbacks.MMFeedbackPosition/<MoveFromTo>d__37::System.IDisposable.Dispose()
extern void U3CMoveFromToU3Ed__37_System_IDisposable_Dispose_mDD539198B4CE9BA0494AAC611EC94A7FF5E9CFA2 (void);
// 0x00000205 System.Boolean MoreMountains.Feedbacks.MMFeedbackPosition/<MoveFromTo>d__37::MoveNext()
extern void U3CMoveFromToU3Ed__37_MoveNext_m9B50A388204C37EF4A88242C3657A960D0B304B6 (void);
// 0x00000206 System.Object MoreMountains.Feedbacks.MMFeedbackPosition/<MoveFromTo>d__37::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CMoveFromToU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6C4C325373524F9B8CDB5385DB293E9F532E0BE4 (void);
// 0x00000207 System.Void MoreMountains.Feedbacks.MMFeedbackPosition/<MoveFromTo>d__37::System.Collections.IEnumerator.Reset()
extern void U3CMoveFromToU3Ed__37_System_Collections_IEnumerator_Reset_mD107BBF36A7025D52B84AA9975B8D301D542E184 (void);
// 0x00000208 System.Object MoreMountains.Feedbacks.MMFeedbackPosition/<MoveFromTo>d__37::System.Collections.IEnumerator.get_Current()
extern void U3CMoveFromToU3Ed__37_System_Collections_IEnumerator_get_Current_m7F1913CD92901AE729C9B4147BBD4EB91E2395C9 (void);
// 0x00000209 System.Void MoreMountains.Feedbacks.MMFeedbackRigidbody::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackRigidbody_CustomPlayFeedback_mCAD1B28DC53EF25A0ADB2C5CA6CC7A43BA442E96 (void);
// 0x0000020A System.Void MoreMountains.Feedbacks.MMFeedbackRigidbody::.ctor()
extern void MMFeedbackRigidbody__ctor_m91B7797C1675B3D0AA605F76241FA17DFDEC1576 (void);
// 0x0000020B System.Void MoreMountains.Feedbacks.MMFeedbackRigidbody2D::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackRigidbody2D_CustomPlayFeedback_mE22D54348FB89D731F71902D58E69C13A4F5DE86 (void);
// 0x0000020C System.Void MoreMountains.Feedbacks.MMFeedbackRigidbody2D::.ctor()
extern void MMFeedbackRigidbody2D__ctor_mCA04A7BCB1E6394AFD04F8858F77E5F92A7CA6CF (void);
// 0x0000020D System.Single MoreMountains.Feedbacks.MMFeedbackRotation::get_FeedbackDuration()
extern void MMFeedbackRotation_get_FeedbackDuration_m7881FB2C1D56B00C081EF28200963830C8308FEC (void);
// 0x0000020E System.Void MoreMountains.Feedbacks.MMFeedbackRotation::set_FeedbackDuration(System.Single)
extern void MMFeedbackRotation_set_FeedbackDuration_m66044F630340BB409D973814D4F87862D5658C53 (void);
// 0x0000020F System.Void MoreMountains.Feedbacks.MMFeedbackRotation::CustomInitialization(UnityEngine.GameObject)
extern void MMFeedbackRotation_CustomInitialization_m659FFF5BE6A2BD2628F9FB617E22A455FCC8E28F (void);
// 0x00000210 System.Void MoreMountains.Feedbacks.MMFeedbackRotation::GetInitialRotation()
extern void MMFeedbackRotation_GetInitialRotation_m562F84705695C03D837F68BB2DCB7DC056958ED4 (void);
// 0x00000211 System.Void MoreMountains.Feedbacks.MMFeedbackRotation::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackRotation_CustomPlayFeedback_m8C42E64D9250A125685F2CB1D8D62E18D265644E (void);
// 0x00000212 System.Collections.IEnumerator MoreMountains.Feedbacks.MMFeedbackRotation::RotateToDestination()
extern void MMFeedbackRotation_RotateToDestination_m2B22EE212D080C3A540CA27E0069427CF956C936 (void);
// 0x00000213 System.Collections.IEnumerator MoreMountains.Feedbacks.MMFeedbackRotation::AnimateRotation(UnityEngine.Transform,UnityEngine.Vector3,System.Single,UnityEngine.AnimationCurve,UnityEngine.AnimationCurve,UnityEngine.AnimationCurve,System.Single,System.Single)
extern void MMFeedbackRotation_AnimateRotation_m1EA58D02BD4C8CD3CE0CFFD07E4BE899710EA468 (void);
// 0x00000214 System.Void MoreMountains.Feedbacks.MMFeedbackRotation::ApplyRotation(UnityEngine.Transform,System.Single,System.Single,UnityEngine.AnimationCurve,UnityEngine.AnimationCurve,UnityEngine.AnimationCurve,System.Single)
extern void MMFeedbackRotation_ApplyRotation_mECE88B789AD15FB7E2F0568EFC956A66EB64C5BE (void);
// 0x00000215 System.Void MoreMountains.Feedbacks.MMFeedbackRotation::CustomStopFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackRotation_CustomStopFeedback_mB753B09276633AF1493B38F0B96E038F6965B909 (void);
// 0x00000216 System.Void MoreMountains.Feedbacks.MMFeedbackRotation::OnDisable()
extern void MMFeedbackRotation_OnDisable_m3F19222D3FE83D41DB524032BD24D9E89F13AEF5 (void);
// 0x00000217 System.Void MoreMountains.Feedbacks.MMFeedbackRotation::.ctor()
extern void MMFeedbackRotation__ctor_m8DCAC5F56CC12A414A8C54C94EE6375F73AC6326 (void);
// 0x00000218 System.Void MoreMountains.Feedbacks.MMFeedbackRotation/<RotateToDestination>d__30::.ctor(System.Int32)
extern void U3CRotateToDestinationU3Ed__30__ctor_mCC46CEBFFE1476A781081BC250CC84FB65CC61DE (void);
// 0x00000219 System.Void MoreMountains.Feedbacks.MMFeedbackRotation/<RotateToDestination>d__30::System.IDisposable.Dispose()
extern void U3CRotateToDestinationU3Ed__30_System_IDisposable_Dispose_m5D1A91B4963E0372659413BC6D19365153245FED (void);
// 0x0000021A System.Boolean MoreMountains.Feedbacks.MMFeedbackRotation/<RotateToDestination>d__30::MoveNext()
extern void U3CRotateToDestinationU3Ed__30_MoveNext_m79CCB5D39C6486360572EFE14A03BC4AFA8FA434 (void);
// 0x0000021B System.Object MoreMountains.Feedbacks.MMFeedbackRotation/<RotateToDestination>d__30::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRotateToDestinationU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5EC2B29A701A2D84E4DB32F0BDBDAE3B3FD33DF1 (void);
// 0x0000021C System.Void MoreMountains.Feedbacks.MMFeedbackRotation/<RotateToDestination>d__30::System.Collections.IEnumerator.Reset()
extern void U3CRotateToDestinationU3Ed__30_System_Collections_IEnumerator_Reset_m7CF6C26AE7D8130E93AEF5389C6E750EAEE667A1 (void);
// 0x0000021D System.Object MoreMountains.Feedbacks.MMFeedbackRotation/<RotateToDestination>d__30::System.Collections.IEnumerator.get_Current()
extern void U3CRotateToDestinationU3Ed__30_System_Collections_IEnumerator_get_Current_m7DEE82F95D53FC3BBC90E29EDDD83B1314369D5C (void);
// 0x0000021E System.Void MoreMountains.Feedbacks.MMFeedbackRotation/<AnimateRotation>d__31::.ctor(System.Int32)
extern void U3CAnimateRotationU3Ed__31__ctor_mF3E893CACB9944A052592F7BEECBDB501B61903C (void);
// 0x0000021F System.Void MoreMountains.Feedbacks.MMFeedbackRotation/<AnimateRotation>d__31::System.IDisposable.Dispose()
extern void U3CAnimateRotationU3Ed__31_System_IDisposable_Dispose_m7F2C45B82A047FB954F23EB205684BD48ABB90AF (void);
// 0x00000220 System.Boolean MoreMountains.Feedbacks.MMFeedbackRotation/<AnimateRotation>d__31::MoveNext()
extern void U3CAnimateRotationU3Ed__31_MoveNext_m9C354A2BC9F37E399F1DE837C204034CB2FAFAA5 (void);
// 0x00000221 System.Object MoreMountains.Feedbacks.MMFeedbackRotation/<AnimateRotation>d__31::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateRotationU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m888EF09C54D09A71787652AB0A42624A8355E5A3 (void);
// 0x00000222 System.Void MoreMountains.Feedbacks.MMFeedbackRotation/<AnimateRotation>d__31::System.Collections.IEnumerator.Reset()
extern void U3CAnimateRotationU3Ed__31_System_Collections_IEnumerator_Reset_m5D1D65CB9F03A92B9EA056CFF6CC20C948B92B73 (void);
// 0x00000223 System.Object MoreMountains.Feedbacks.MMFeedbackRotation/<AnimateRotation>d__31::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateRotationU3Ed__31_System_Collections_IEnumerator_get_Current_m1171A16764B62EB16A7E67B16537AF01E3D49DBE (void);
// 0x00000224 System.Single MoreMountains.Feedbacks.MMFeedbackScale::get_FeedbackDuration()
extern void MMFeedbackScale_get_FeedbackDuration_mD192E2FB5146D2B4CE41FCA8FE71D6C8C40005A8 (void);
// 0x00000225 System.Void MoreMountains.Feedbacks.MMFeedbackScale::set_FeedbackDuration(System.Single)
extern void MMFeedbackScale_set_FeedbackDuration_m2B48177DBCF15502D51D95C2E27E6779DFDAE613 (void);
// 0x00000226 System.Void MoreMountains.Feedbacks.MMFeedbackScale::CustomInitialization(UnityEngine.GameObject)
extern void MMFeedbackScale_CustomInitialization_mC2B33DB4FFABAE791B3FEC4947D7CE9CFB3579CD (void);
// 0x00000227 System.Void MoreMountains.Feedbacks.MMFeedbackScale::GetInitialScale()
extern void MMFeedbackScale_GetInitialScale_m321C3B873245FE15163CDCB421F7332F367D5E84 (void);
// 0x00000228 System.Void MoreMountains.Feedbacks.MMFeedbackScale::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackScale_CustomPlayFeedback_m7FAB75C0501A3360E5C928AB1A586289B34870AF (void);
// 0x00000229 System.Collections.IEnumerator MoreMountains.Feedbacks.MMFeedbackScale::ScaleToDestination()
extern void MMFeedbackScale_ScaleToDestination_m1B7C547AAA613B43577AADB27787F3B59AD645CD (void);
// 0x0000022A System.Collections.IEnumerator MoreMountains.Feedbacks.MMFeedbackScale::AnimateScale(UnityEngine.Transform,UnityEngine.Vector3,System.Single,UnityEngine.AnimationCurve,UnityEngine.AnimationCurve,UnityEngine.AnimationCurve,System.Single,System.Single)
extern void MMFeedbackScale_AnimateScale_m5F071C3F5B4524E99583DB09093F30BF6BF98F38 (void);
// 0x0000022B System.Void MoreMountains.Feedbacks.MMFeedbackScale::CustomStopFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackScale_CustomStopFeedback_mC269CAE9630FA0CF44915468C44CC31D69453391 (void);
// 0x0000022C System.Void MoreMountains.Feedbacks.MMFeedbackScale::OnDisable()
extern void MMFeedbackScale_OnDisable_m475B15A332C8CD34DA8000F7FD6D353D83B6116D (void);
// 0x0000022D System.Void MoreMountains.Feedbacks.MMFeedbackScale::.ctor()
extern void MMFeedbackScale__ctor_mC5DD376174F264723C5031550BCDE3B4F9C7701A (void);
// 0x0000022E System.Void MoreMountains.Feedbacks.MMFeedbackScale/<ScaleToDestination>d__27::.ctor(System.Int32)
extern void U3CScaleToDestinationU3Ed__27__ctor_m20AD5414CB9ACEBCC297F2141593CC573B556A16 (void);
// 0x0000022F System.Void MoreMountains.Feedbacks.MMFeedbackScale/<ScaleToDestination>d__27::System.IDisposable.Dispose()
extern void U3CScaleToDestinationU3Ed__27_System_IDisposable_Dispose_m6700A46FA41A84C399E845916B317BEF48EE8330 (void);
// 0x00000230 System.Boolean MoreMountains.Feedbacks.MMFeedbackScale/<ScaleToDestination>d__27::MoveNext()
extern void U3CScaleToDestinationU3Ed__27_MoveNext_m35CF66A5C133567640A0177FDBFC16A8745E9489 (void);
// 0x00000231 System.Object MoreMountains.Feedbacks.MMFeedbackScale/<ScaleToDestination>d__27::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CScaleToDestinationU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF10EE5B9D9B06CB1A0D60A3F08C733895B5DDCF4 (void);
// 0x00000232 System.Void MoreMountains.Feedbacks.MMFeedbackScale/<ScaleToDestination>d__27::System.Collections.IEnumerator.Reset()
extern void U3CScaleToDestinationU3Ed__27_System_Collections_IEnumerator_Reset_mC0BBEB91104B6BAC2EB1533BA8968EBC59FE0998 (void);
// 0x00000233 System.Object MoreMountains.Feedbacks.MMFeedbackScale/<ScaleToDestination>d__27::System.Collections.IEnumerator.get_Current()
extern void U3CScaleToDestinationU3Ed__27_System_Collections_IEnumerator_get_Current_m3096601618E872CF68B683982FBAB2308ADC209A (void);
// 0x00000234 System.Void MoreMountains.Feedbacks.MMFeedbackScale/<AnimateScale>d__28::.ctor(System.Int32)
extern void U3CAnimateScaleU3Ed__28__ctor_m1C239E3C280E0807E3354F199519D7A4F814F303 (void);
// 0x00000235 System.Void MoreMountains.Feedbacks.MMFeedbackScale/<AnimateScale>d__28::System.IDisposable.Dispose()
extern void U3CAnimateScaleU3Ed__28_System_IDisposable_Dispose_m2BFA9F03D764363D583B4B0C84837239D23843F0 (void);
// 0x00000236 System.Boolean MoreMountains.Feedbacks.MMFeedbackScale/<AnimateScale>d__28::MoveNext()
extern void U3CAnimateScaleU3Ed__28_MoveNext_m992821804EBDDF78D317DC88E902E428EB1C20C6 (void);
// 0x00000237 System.Object MoreMountains.Feedbacks.MMFeedbackScale/<AnimateScale>d__28::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateScaleU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5DB179F4A90E2A312BFF467EEE9C68618719349F (void);
// 0x00000238 System.Void MoreMountains.Feedbacks.MMFeedbackScale/<AnimateScale>d__28::System.Collections.IEnumerator.Reset()
extern void U3CAnimateScaleU3Ed__28_System_Collections_IEnumerator_Reset_m2E212688DF3F003D1986FD7C53997D5D3E000E19 (void);
// 0x00000239 System.Object MoreMountains.Feedbacks.MMFeedbackScale/<AnimateScale>d__28::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateScaleU3Ed__28_System_Collections_IEnumerator_get_Current_m484F065959BFD4E99E4CEE8A7BE43A7FF5DD39DA (void);
// 0x0000023A System.Void MoreMountains.Feedbacks.MMFeedbackSetActive::CustomInitialization(UnityEngine.GameObject)
extern void MMFeedbackSetActive_CustomInitialization_m54A71AD11A6C7EC8112CD88A90213DE57AEDE767 (void);
// 0x0000023B System.Void MoreMountains.Feedbacks.MMFeedbackSetActive::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackSetActive_CustomPlayFeedback_m8BB480A010174F5EA2280AD6B6BACFD37E802F53 (void);
// 0x0000023C System.Void MoreMountains.Feedbacks.MMFeedbackSetActive::CustomStopFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackSetActive_CustomStopFeedback_mCD37C9D4DD9E014E685ECA51D9C4C828BC92AAEC (void);
// 0x0000023D System.Void MoreMountains.Feedbacks.MMFeedbackSetActive::CustomReset()
extern void MMFeedbackSetActive_CustomReset_mA2016A5084BDE2BE3F901E790AF4D355707D0023 (void);
// 0x0000023E System.Void MoreMountains.Feedbacks.MMFeedbackSetActive::SetStatus(MoreMountains.Feedbacks.MMFeedbackSetActive/PossibleStates)
extern void MMFeedbackSetActive_SetStatus_mF949DFF6CDA19924F302104607639FDF6C8B9151 (void);
// 0x0000023F System.Void MoreMountains.Feedbacks.MMFeedbackSetActive::.ctor()
extern void MMFeedbackSetActive__ctor_m1226C3AAB1D91840C41B713CC7B115D81E2F2A28 (void);
// 0x00000240 System.Void MoreMountains.Feedbacks.MMFeedbackSkybox::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackSkybox_CustomPlayFeedback_mD1934B6D5AB4D7439761EBFC7A9B51E91B1D846F (void);
// 0x00000241 System.Void MoreMountains.Feedbacks.MMFeedbackSkybox::.ctor()
extern void MMFeedbackSkybox__ctor_m7B8EB2CDFD5B9C35BD4E48DECA3D2B10049941FF (void);
// 0x00000242 System.Single MoreMountains.Feedbacks.MMFeedbackSpriteRenderer::get_FeedbackDuration()
extern void MMFeedbackSpriteRenderer_get_FeedbackDuration_m324BCF93DD386DC91BB65CF0F2D316CF8F60E3EE (void);
// 0x00000243 System.Void MoreMountains.Feedbacks.MMFeedbackSpriteRenderer::set_FeedbackDuration(System.Single)
extern void MMFeedbackSpriteRenderer_set_FeedbackDuration_m547EC9055B98895F3F4EEC4FE80767931BA4A2EC (void);
// 0x00000244 System.Void MoreMountains.Feedbacks.MMFeedbackSpriteRenderer::CustomInitialization(UnityEngine.GameObject)
extern void MMFeedbackSpriteRenderer_CustomInitialization_mE10BC9839C6E9964790900A0828C5EECC1002FDB (void);
// 0x00000245 System.Void MoreMountains.Feedbacks.MMFeedbackSpriteRenderer::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackSpriteRenderer_CustomPlayFeedback_mE96183F492549ABEDC01E3150BDE000893F4E467 (void);
// 0x00000246 System.Collections.IEnumerator MoreMountains.Feedbacks.MMFeedbackSpriteRenderer::SpriteRendererSequence()
extern void MMFeedbackSpriteRenderer_SpriteRendererSequence_mF738A5BD1D640F048FF872B475D89EC48768573E (void);
// 0x00000247 System.Collections.IEnumerator MoreMountains.Feedbacks.MMFeedbackSpriteRenderer::SpriteRendererToDestinationSequence(System.Boolean)
extern void MMFeedbackSpriteRenderer_SpriteRendererToDestinationSequence_m03A3886FC4F76CE098AFBAD101C238ED6D367BC4 (void);
// 0x00000248 System.Void MoreMountains.Feedbacks.MMFeedbackSpriteRenderer::Flip()
extern void MMFeedbackSpriteRenderer_Flip_m19558B95BCD31535848958BEEAF4DF11425997A6 (void);
// 0x00000249 System.Void MoreMountains.Feedbacks.MMFeedbackSpriteRenderer::SetSpriteRendererValues(System.Single)
extern void MMFeedbackSpriteRenderer_SetSpriteRendererValues_m16C19E2B40EEB448FFB02AD53E5A0254DCC4E5F8 (void);
// 0x0000024A System.Void MoreMountains.Feedbacks.MMFeedbackSpriteRenderer::CustomStopFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackSpriteRenderer_CustomStopFeedback_mA2AAACF8073A43B0E6896F3115E60B6DB9A4A54D (void);
// 0x0000024B System.Void MoreMountains.Feedbacks.MMFeedbackSpriteRenderer::Turn(System.Boolean)
extern void MMFeedbackSpriteRenderer_Turn_m4AD609EA6ED7C5FB83E069A83A332DF21D91CFA6 (void);
// 0x0000024C System.Void MoreMountains.Feedbacks.MMFeedbackSpriteRenderer::.ctor()
extern void MMFeedbackSpriteRenderer__ctor_m606A704AF847E17F5D7AC3B9E4C0D2970212E9D3 (void);
// 0x0000024D System.Void MoreMountains.Feedbacks.MMFeedbackSpriteRenderer/<SpriteRendererSequence>d__28::.ctor(System.Int32)
extern void U3CSpriteRendererSequenceU3Ed__28__ctor_m5CED94C7E93BEDC0C92FE05CC8868138DE1B4FB1 (void);
// 0x0000024E System.Void MoreMountains.Feedbacks.MMFeedbackSpriteRenderer/<SpriteRendererSequence>d__28::System.IDisposable.Dispose()
extern void U3CSpriteRendererSequenceU3Ed__28_System_IDisposable_Dispose_m6A003D74EAC1815F5F6BBAAE0AC9B1D3B1A36E93 (void);
// 0x0000024F System.Boolean MoreMountains.Feedbacks.MMFeedbackSpriteRenderer/<SpriteRendererSequence>d__28::MoveNext()
extern void U3CSpriteRendererSequenceU3Ed__28_MoveNext_mABB226DAE4F8973F112C70E0846C5EE61FAF5345 (void);
// 0x00000250 System.Object MoreMountains.Feedbacks.MMFeedbackSpriteRenderer/<SpriteRendererSequence>d__28::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSpriteRendererSequenceU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m590A28C1FCA91B678282A2A7AFDC00693FC16364 (void);
// 0x00000251 System.Void MoreMountains.Feedbacks.MMFeedbackSpriteRenderer/<SpriteRendererSequence>d__28::System.Collections.IEnumerator.Reset()
extern void U3CSpriteRendererSequenceU3Ed__28_System_Collections_IEnumerator_Reset_mF50832E244BFC055EA2D6F328B7501A8AE9B32D2 (void);
// 0x00000252 System.Object MoreMountains.Feedbacks.MMFeedbackSpriteRenderer/<SpriteRendererSequence>d__28::System.Collections.IEnumerator.get_Current()
extern void U3CSpriteRendererSequenceU3Ed__28_System_Collections_IEnumerator_get_Current_m7B4468051BB25DEBB4F1B9B0FA9DFA681695A592 (void);
// 0x00000253 System.Void MoreMountains.Feedbacks.MMFeedbackSpriteRenderer/<SpriteRendererToDestinationSequence>d__29::.ctor(System.Int32)
extern void U3CSpriteRendererToDestinationSequenceU3Ed__29__ctor_m677734AF7036B92816613F1AF39DE8EA479C8FFB (void);
// 0x00000254 System.Void MoreMountains.Feedbacks.MMFeedbackSpriteRenderer/<SpriteRendererToDestinationSequence>d__29::System.IDisposable.Dispose()
extern void U3CSpriteRendererToDestinationSequenceU3Ed__29_System_IDisposable_Dispose_mEBAAACDA5E0CA3667208EFC36C4640A408987673 (void);
// 0x00000255 System.Boolean MoreMountains.Feedbacks.MMFeedbackSpriteRenderer/<SpriteRendererToDestinationSequence>d__29::MoveNext()
extern void U3CSpriteRendererToDestinationSequenceU3Ed__29_MoveNext_m5ABFDA560D11A7E5282273C9750DF01C3360EA93 (void);
// 0x00000256 System.Object MoreMountains.Feedbacks.MMFeedbackSpriteRenderer/<SpriteRendererToDestinationSequence>d__29::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSpriteRendererToDestinationSequenceU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m54D8AA907F7B9C6FA580F4C1256E5FC96580B78A (void);
// 0x00000257 System.Void MoreMountains.Feedbacks.MMFeedbackSpriteRenderer/<SpriteRendererToDestinationSequence>d__29::System.Collections.IEnumerator.Reset()
extern void U3CSpriteRendererToDestinationSequenceU3Ed__29_System_Collections_IEnumerator_Reset_m2EF3DE0D7B3A99930B0011629CC3B75C8EBF5FD7 (void);
// 0x00000258 System.Object MoreMountains.Feedbacks.MMFeedbackSpriteRenderer/<SpriteRendererToDestinationSequence>d__29::System.Collections.IEnumerator.get_Current()
extern void U3CSpriteRendererToDestinationSequenceU3Ed__29_System_Collections_IEnumerator_get_Current_m5475D85BB93572BFDC9E65885619587D84C51A98 (void);
// 0x00000259 System.Void MoreMountains.Feedbacks.MMFeedbackText::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackText_CustomPlayFeedback_m7FE53CE564718E4A1D629418C43B5D3E40167702 (void);
// 0x0000025A System.Void MoreMountains.Feedbacks.MMFeedbackText::.ctor()
extern void MMFeedbackText__ctor_mFCAABFBF5F7501DFBE33DB025EF48D63081E5BE4 (void);
// 0x0000025B System.Single MoreMountains.Feedbacks.MMFeedbackTextColor::get_FeedbackDuration()
extern void MMFeedbackTextColor_get_FeedbackDuration_mD825CB737A85B59C4A3916252B187A3DAEED21BB (void);
// 0x0000025C System.Void MoreMountains.Feedbacks.MMFeedbackTextColor::set_FeedbackDuration(System.Single)
extern void MMFeedbackTextColor_set_FeedbackDuration_m2D7ECB3FCC383E7A182007A4E28A477460DBCEE5 (void);
// 0x0000025D System.Void MoreMountains.Feedbacks.MMFeedbackTextColor::CustomInitialization(UnityEngine.GameObject)
extern void MMFeedbackTextColor_CustomInitialization_m5CE7B4E3FF185AF97A52996BB5F2C4E6977350E9 (void);
// 0x0000025E System.Void MoreMountains.Feedbacks.MMFeedbackTextColor::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackTextColor_CustomPlayFeedback_m10D99BCFC5BDB6AEF41B6BBD272237C3BB4A1581 (void);
// 0x0000025F System.Collections.IEnumerator MoreMountains.Feedbacks.MMFeedbackTextColor::ChangeColor()
extern void MMFeedbackTextColor_ChangeColor_mDDB2D7661C71753532D5E9312F196DA43C508C9F (void);
// 0x00000260 System.Void MoreMountains.Feedbacks.MMFeedbackTextColor::SetColor(System.Single)
extern void MMFeedbackTextColor_SetColor_m115A7ED17F04F544E022BE25102AAAC4A6529FAE (void);
// 0x00000261 System.Void MoreMountains.Feedbacks.MMFeedbackTextColor::CustomStopFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackTextColor_CustomStopFeedback_mD616E56451D6307660634DCB9F4B6F76FC712B5C (void);
// 0x00000262 System.Void MoreMountains.Feedbacks.MMFeedbackTextColor::.ctor()
extern void MMFeedbackTextColor__ctor_m5C0308514E7B51D038918442B1652E55D1393023 (void);
// 0x00000263 System.Void MoreMountains.Feedbacks.MMFeedbackTextColor/<ChangeColor>d__16::.ctor(System.Int32)
extern void U3CChangeColorU3Ed__16__ctor_mAEEDFCEDB77C2150E7D70A91FFEA167B5383E470 (void);
// 0x00000264 System.Void MoreMountains.Feedbacks.MMFeedbackTextColor/<ChangeColor>d__16::System.IDisposable.Dispose()
extern void U3CChangeColorU3Ed__16_System_IDisposable_Dispose_m26902A0ED9D0BB0B1FF26BCFDBDEB73A9F716EBA (void);
// 0x00000265 System.Boolean MoreMountains.Feedbacks.MMFeedbackTextColor/<ChangeColor>d__16::MoveNext()
extern void U3CChangeColorU3Ed__16_MoveNext_m317400CCA4A483386CEAECFC00B67603E979B7DC (void);
// 0x00000266 System.Object MoreMountains.Feedbacks.MMFeedbackTextColor/<ChangeColor>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CChangeColorU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB36B8CBE1719FB3A571C69B0DCE6C870643CA94A (void);
// 0x00000267 System.Void MoreMountains.Feedbacks.MMFeedbackTextColor/<ChangeColor>d__16::System.Collections.IEnumerator.Reset()
extern void U3CChangeColorU3Ed__16_System_Collections_IEnumerator_Reset_m969E98EA6343C9106E80F897DC0AF09B84FC47D2 (void);
// 0x00000268 System.Object MoreMountains.Feedbacks.MMFeedbackTextColor/<ChangeColor>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CChangeColorU3Ed__16_System_Collections_IEnumerator_get_Current_mBEC1702CB943CA807D617A3BFE096C30B4DB20C8 (void);
// 0x00000269 System.Single MoreMountains.Feedbacks.MMFeedbackTextureOffset::get_FeedbackDuration()
extern void MMFeedbackTextureOffset_get_FeedbackDuration_mF0D0DB2A3393E9769E60757160554AE21F52410E (void);
// 0x0000026A System.Void MoreMountains.Feedbacks.MMFeedbackTextureOffset::set_FeedbackDuration(System.Single)
extern void MMFeedbackTextureOffset_set_FeedbackDuration_m16079FD3AA2DA39564CAAB62DDFA2A9519621879 (void);
// 0x0000026B System.Void MoreMountains.Feedbacks.MMFeedbackTextureOffset::CustomInitialization(UnityEngine.GameObject)
extern void MMFeedbackTextureOffset_CustomInitialization_m3551D9E9A3CDB427AA52CD19DB7C854823C359F6 (void);
// 0x0000026C System.Void MoreMountains.Feedbacks.MMFeedbackTextureOffset::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackTextureOffset_CustomPlayFeedback_mE68EF79345A059F929C570F8C03AE1577B5EE35D (void);
// 0x0000026D System.Collections.IEnumerator MoreMountains.Feedbacks.MMFeedbackTextureOffset::TransitionCo(System.Single)
extern void MMFeedbackTextureOffset_TransitionCo_m7C3891143737CD7F59D24A793CE9823395F036CA (void);
// 0x0000026E System.Void MoreMountains.Feedbacks.MMFeedbackTextureOffset::SetMaterialValues(System.Single,System.Single)
extern void MMFeedbackTextureOffset_SetMaterialValues_m2489F1D122936609235661BCAA8218F86D42E9DC (void);
// 0x0000026F System.Void MoreMountains.Feedbacks.MMFeedbackTextureOffset::ApplyValue(UnityEngine.Vector2)
extern void MMFeedbackTextureOffset_ApplyValue_mCA93FD39A666C86DF9FDD5E63CEC045B2104A3B6 (void);
// 0x00000270 System.Void MoreMountains.Feedbacks.MMFeedbackTextureOffset::CustomStopFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackTextureOffset_CustomStopFeedback_m9AC036542FC6AA757CC5B2117721FD0A417E803F (void);
// 0x00000271 System.Void MoreMountains.Feedbacks.MMFeedbackTextureOffset::.ctor()
extern void MMFeedbackTextureOffset__ctor_m230DCB60C586E982353FEE1B9C2EEEF63DE13AB2 (void);
// 0x00000272 System.Void MoreMountains.Feedbacks.MMFeedbackTextureOffset/<TransitionCo>d__23::.ctor(System.Int32)
extern void U3CTransitionCoU3Ed__23__ctor_m118C61E6A101AB2A30303CA758644F6231300AA4 (void);
// 0x00000273 System.Void MoreMountains.Feedbacks.MMFeedbackTextureOffset/<TransitionCo>d__23::System.IDisposable.Dispose()
extern void U3CTransitionCoU3Ed__23_System_IDisposable_Dispose_mF19802027AFF49B7F7590372E2A24DAB268DE1F5 (void);
// 0x00000274 System.Boolean MoreMountains.Feedbacks.MMFeedbackTextureOffset/<TransitionCo>d__23::MoveNext()
extern void U3CTransitionCoU3Ed__23_MoveNext_m3110D6137A1A672398CD73FAB676524D9B38224F (void);
// 0x00000275 System.Object MoreMountains.Feedbacks.MMFeedbackTextureOffset/<TransitionCo>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTransitionCoU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5771D7B39B6227AA30E8375F5016E400C8787877 (void);
// 0x00000276 System.Void MoreMountains.Feedbacks.MMFeedbackTextureOffset/<TransitionCo>d__23::System.Collections.IEnumerator.Reset()
extern void U3CTransitionCoU3Ed__23_System_Collections_IEnumerator_Reset_m173232904CE6EE9E8B9A24CA09924933B4A69EB3 (void);
// 0x00000277 System.Object MoreMountains.Feedbacks.MMFeedbackTextureOffset/<TransitionCo>d__23::System.Collections.IEnumerator.get_Current()
extern void U3CTransitionCoU3Ed__23_System_Collections_IEnumerator_get_Current_m525409B35B1FC006C52176178684B7C6F851E7CE (void);
// 0x00000278 System.Single MoreMountains.Feedbacks.MMFeedbackTextureScale::get_FeedbackDuration()
extern void MMFeedbackTextureScale_get_FeedbackDuration_mB4C7DA5A9B2F417455B6F9A11763F25D73DFC03A (void);
// 0x00000279 System.Void MoreMountains.Feedbacks.MMFeedbackTextureScale::set_FeedbackDuration(System.Single)
extern void MMFeedbackTextureScale_set_FeedbackDuration_m12002AC1146CB904AA784F45840DA1351F4CF84F (void);
// 0x0000027A System.Void MoreMountains.Feedbacks.MMFeedbackTextureScale::CustomInitialization(UnityEngine.GameObject)
extern void MMFeedbackTextureScale_CustomInitialization_m38BA70129DDEC029E27845B19E2FEFDC37766A7F (void);
// 0x0000027B System.Void MoreMountains.Feedbacks.MMFeedbackTextureScale::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackTextureScale_CustomPlayFeedback_m3F8C978C70E75D361A5864034D35F4A66B739A6E (void);
// 0x0000027C System.Collections.IEnumerator MoreMountains.Feedbacks.MMFeedbackTextureScale::TransitionCo(System.Single)
extern void MMFeedbackTextureScale_TransitionCo_mC83BA175162BE4913418AD1FFD66E6C9AA6E1B8D (void);
// 0x0000027D System.Void MoreMountains.Feedbacks.MMFeedbackTextureScale::SetMaterialValues(System.Single,System.Single)
extern void MMFeedbackTextureScale_SetMaterialValues_mBFC6F808A400E8EBDB8CD7ADA297047146EF67A1 (void);
// 0x0000027E System.Void MoreMountains.Feedbacks.MMFeedbackTextureScale::ApplyValue(UnityEngine.Vector2)
extern void MMFeedbackTextureScale_ApplyValue_m21D091A473BAFE19375DD459966DEF2769B6C790 (void);
// 0x0000027F System.Void MoreMountains.Feedbacks.MMFeedbackTextureScale::CustomStopFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackTextureScale_CustomStopFeedback_m5956907F4BC04F19385E1593CC6CA0A5C80333BD (void);
// 0x00000280 System.Void MoreMountains.Feedbacks.MMFeedbackTextureScale::.ctor()
extern void MMFeedbackTextureScale__ctor_mE5E269DDAB55978411C00BA0D6415B5829323D17 (void);
// 0x00000281 System.Void MoreMountains.Feedbacks.MMFeedbackTextureScale/<TransitionCo>d__23::.ctor(System.Int32)
extern void U3CTransitionCoU3Ed__23__ctor_m0FE0498F5EB529913C30136DD7F6F714CECA1D71 (void);
// 0x00000282 System.Void MoreMountains.Feedbacks.MMFeedbackTextureScale/<TransitionCo>d__23::System.IDisposable.Dispose()
extern void U3CTransitionCoU3Ed__23_System_IDisposable_Dispose_m0372256108BCE4DE0D8DE082040C5E1E787572F0 (void);
// 0x00000283 System.Boolean MoreMountains.Feedbacks.MMFeedbackTextureScale/<TransitionCo>d__23::MoveNext()
extern void U3CTransitionCoU3Ed__23_MoveNext_m23AD41489E7A06BE03C4342F32FAB61FD48B7B0C (void);
// 0x00000284 System.Object MoreMountains.Feedbacks.MMFeedbackTextureScale/<TransitionCo>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTransitionCoU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6B6EF4DC83ED6F2C59BC25D852874985FB7E97BF (void);
// 0x00000285 System.Void MoreMountains.Feedbacks.MMFeedbackTextureScale/<TransitionCo>d__23::System.Collections.IEnumerator.Reset()
extern void U3CTransitionCoU3Ed__23_System_Collections_IEnumerator_Reset_mC964142325F1AAE3A13C8649811B915E38457513 (void);
// 0x00000286 System.Object MoreMountains.Feedbacks.MMFeedbackTextureScale/<TransitionCo>d__23::System.Collections.IEnumerator.get_Current()
extern void U3CTransitionCoU3Ed__23_System_Collections_IEnumerator_get_Current_m19D31A8F86E0C1D0354BFDE36E4BB3A1D845AC38 (void);
// 0x00000287 System.Single MoreMountains.Feedbacks.MMFeedbackTimescaleModifier::get_FeedbackDuration()
extern void MMFeedbackTimescaleModifier_get_FeedbackDuration_mD90ECAB42E8D47D0E6CE7F2196736ED33116C180 (void);
// 0x00000288 System.Void MoreMountains.Feedbacks.MMFeedbackTimescaleModifier::set_FeedbackDuration(System.Single)
extern void MMFeedbackTimescaleModifier_set_FeedbackDuration_m2888730154E9DF10E5811F3A981CF001DB07AB8F (void);
// 0x00000289 System.Void MoreMountains.Feedbacks.MMFeedbackTimescaleModifier::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackTimescaleModifier_CustomPlayFeedback_m99557E03A071E07E332A2412DF157A5CBA94DCC9 (void);
// 0x0000028A System.Void MoreMountains.Feedbacks.MMFeedbackTimescaleModifier::CustomStopFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackTimescaleModifier_CustomStopFeedback_m3D95E41AD8E00283BA4172C4B32F602DA24443B1 (void);
// 0x0000028B System.Void MoreMountains.Feedbacks.MMFeedbackTimescaleModifier::.ctor()
extern void MMFeedbackTimescaleModifier__ctor_m1B9CB06CBFFED0B3A4A80503816855648C5BEDFE (void);
// 0x0000028C System.Void MoreMountains.Feedbacks.MMFeedbackUnloadScene::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackUnloadScene_CustomPlayFeedback_m86FC4437ADBB4F3B4DDA897A51C196261C3D48B0 (void);
// 0x0000028D System.Void MoreMountains.Feedbacks.MMFeedbackUnloadScene::.ctor()
extern void MMFeedbackUnloadScene__ctor_m5E66BEBD91A1FB5AD62575A604D10DA90D3EA8ED (void);
// 0x0000028E System.Void MoreMountains.Feedbacks.MMFeedbackVideoPlayer::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackVideoPlayer_CustomPlayFeedback_m72F88568E9DF3CE4BA1F01CFB59A0D1ED386FC3F (void);
// 0x0000028F System.Void MoreMountains.Feedbacks.MMFeedbackVideoPlayer::.ctor()
extern void MMFeedbackVideoPlayer__ctor_mF9999EABAAED2522D54918353DBCAC19CECDB1C6 (void);
// 0x00000290 System.Single MoreMountains.Feedbacks.MMFeedbackWiggle::get_FeedbackDuration()
extern void MMFeedbackWiggle_get_FeedbackDuration_m639AEE55CB43600F69B9AE3CE73BF51FAE2619E7 (void);
// 0x00000291 System.Void MoreMountains.Feedbacks.MMFeedbackWiggle::set_FeedbackDuration(System.Single)
extern void MMFeedbackWiggle_set_FeedbackDuration_mF04D8C6D1051C35318C78B7E3B3DC8BB84392594 (void);
// 0x00000292 System.Void MoreMountains.Feedbacks.MMFeedbackWiggle::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackWiggle_CustomPlayFeedback_mB019536151806DC35BE6DB5306BC026579B80FBA (void);
// 0x00000293 System.Void MoreMountains.Feedbacks.MMFeedbackWiggle::CustomStopFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackWiggle_CustomStopFeedback_m38268E9F35BB842D30F06E7EBC1F9F280F63FA11 (void);
// 0x00000294 System.Void MoreMountains.Feedbacks.MMFeedbackWiggle::.ctor()
extern void MMFeedbackWiggle__ctor_mE6CF6CB28D2302DD2B1D3559CA9E2FDCE93C6249 (void);
// 0x00000295 System.Void MoreMountains.Feedbacks.BlinkPhase::.ctor()
extern void BlinkPhase__ctor_m4A0D848606699F88E7F9BD1FB3E8515DE95EFFEE (void);
// 0x00000296 System.Single MoreMountains.Feedbacks.MMBlink::GetTime()
extern void MMBlink_GetTime_m46BB0E2E4A0F9639F242EB2C6F9944EE16FF066E (void);
// 0x00000297 System.Single MoreMountains.Feedbacks.MMBlink::GetDeltaTime()
extern void MMBlink_GetDeltaTime_mBAD5B03B691DD2D59B60BDE9DB063211964C2D4A (void);
// 0x00000298 System.Void MoreMountains.Feedbacks.MMBlink::ToggleBlinking()
extern void MMBlink_ToggleBlinking_mCB6DC265B2D46CC0724389116BDCA5AAF12F2DCA (void);
// 0x00000299 System.Void MoreMountains.Feedbacks.MMBlink::StartBlinking()
extern void MMBlink_StartBlinking_m2AFD839653684F6EFB893EAA93435335103DE235 (void);
// 0x0000029A System.Void MoreMountains.Feedbacks.MMBlink::StopBlinking()
extern void MMBlink_StopBlinking_m6C5F0F0F87D9518093C4DDE75EEE0F83FEAF2F34 (void);
// 0x0000029B System.Void MoreMountains.Feedbacks.MMBlink::Update()
extern void MMBlink_Update_m4780E237065D305D0F03A2CD780F65D1BC8F3EB3 (void);
// 0x0000029C System.Void MoreMountains.Feedbacks.MMBlink::DetermineState()
extern void MMBlink_DetermineState_mC41D1B6DCA9A12E5BCF3E689F6241C0D929DA32C (void);
// 0x0000029D System.Void MoreMountains.Feedbacks.MMBlink::Blink()
extern void MMBlink_Blink_m37D42DCD20EA180E37633D263E8A433F0070E0FE (void);
// 0x0000029E System.Void MoreMountains.Feedbacks.MMBlink::ApplyBlink(System.Boolean,System.Single)
extern void MMBlink_ApplyBlink_mCE21009B1C6ADCB845AEB9AAE0B6AE809AE8D314 (void);
// 0x0000029F System.Void MoreMountains.Feedbacks.MMBlink::DetermineCurrentPhase()
extern void MMBlink_DetermineCurrentPhase_mADBEAECE3DE01B6EE806D8D604AA2D18880E4D74 (void);
// 0x000002A0 System.Void MoreMountains.Feedbacks.MMBlink::OnEnable()
extern void MMBlink_OnEnable_m189E886D64927989F21D012E078D18A396EADA0E (void);
// 0x000002A1 System.Void MoreMountains.Feedbacks.MMBlink::InitializeBlinkProperties()
extern void MMBlink_InitializeBlinkProperties_mC8CDC4D6DF0E1FBAD3676A1F75FF102697C7A057 (void);
// 0x000002A2 System.Void MoreMountains.Feedbacks.MMBlink::ResetBlinkProperties()
extern void MMBlink_ResetBlinkProperties_mA68C5F36BEDB839F570297A79C572E01EC3C5128 (void);
// 0x000002A3 System.Void MoreMountains.Feedbacks.MMBlink::.ctor()
extern void MMBlink__ctor_m919930E7CD8359A34FCFFABC3C5BE03452064217 (void);
// 0x000002A4 System.Void MoreMountains.Feedbacks.MMAudioSourceSequencer::OnBeat()
extern void MMAudioSourceSequencer_OnBeat_m8951E2C4FDA87E937591710532EB57C8C36F9318 (void);
// 0x000002A5 System.Void MoreMountains.Feedbacks.MMAudioSourceSequencer::PlayTrackEvent(System.Int32)
extern void MMAudioSourceSequencer_PlayTrackEvent_m39214658688549242236EA9C0171E5097315B247 (void);
// 0x000002A6 System.Void MoreMountains.Feedbacks.MMAudioSourceSequencer::EditorMaintenance()
extern void MMAudioSourceSequencer_EditorMaintenance_m22B8A5EB0935EA8AB6BC9AA1AADEE0181E9A5BC4 (void);
// 0x000002A7 System.Void MoreMountains.Feedbacks.MMAudioSourceSequencer::SetupSounds()
extern void MMAudioSourceSequencer_SetupSounds_m100825BFCFC7A8BB10A7871104C7F18367C52B79 (void);
// 0x000002A8 System.Void MoreMountains.Feedbacks.MMAudioSourceSequencer::.ctor()
extern void MMAudioSourceSequencer__ctor_m3D983FC0B191A6303D93806318BA60C50C24AB65 (void);
// 0x000002A9 System.Void MoreMountains.Feedbacks.MMFeedbacksSequencer::OnBeat()
extern void MMFeedbacksSequencer_OnBeat_m8B64E0DE28F84CFA137F869D20E31FAE44C7642D (void);
// 0x000002AA System.Void MoreMountains.Feedbacks.MMFeedbacksSequencer::PlayTrackEvent(System.Int32)
extern void MMFeedbacksSequencer_PlayTrackEvent_m3B5D78FAE8B99A4B03991DF1A5FC9C0AF97A552E (void);
// 0x000002AB System.Void MoreMountains.Feedbacks.MMFeedbacksSequencer::EditorMaintenance()
extern void MMFeedbacksSequencer_EditorMaintenance_mF81BF4D583A20AFA1EF24F3F57B750FAD85000D2 (void);
// 0x000002AC System.Void MoreMountains.Feedbacks.MMFeedbacksSequencer::SetupFeedbacks()
extern void MMFeedbacksSequencer_SetupFeedbacks_m39429891536A7E035020229494A283186457C70D (void);
// 0x000002AD System.Void MoreMountains.Feedbacks.MMFeedbacksSequencer::.ctor()
extern void MMFeedbacksSequencer__ctor_mD76AD981418A355DD89B94F2DC0B0D907307E0FD (void);
// 0x000002AE System.Void MoreMountains.Feedbacks.MMInputSequenceRecorder::Awake()
extern void MMInputSequenceRecorder_Awake_m22641C15913E7A82136119DA57423911EC4C2CE5 (void);
// 0x000002AF System.Void MoreMountains.Feedbacks.MMInputSequenceRecorder::Initialization()
extern void MMInputSequenceRecorder_Initialization_m47BF0E0D03725316E2B1C74430BA0052C919BFC9 (void);
// 0x000002B0 System.Void MoreMountains.Feedbacks.MMInputSequenceRecorder::Start()
extern void MMInputSequenceRecorder_Start_m7C60B0C8020999C81C62813B4F276738869812A7 (void);
// 0x000002B1 System.Void MoreMountains.Feedbacks.MMInputSequenceRecorder::StartRecording()
extern void MMInputSequenceRecorder_StartRecording_m6596C98339CD208C5FC4A3F3E2DBC13D4C5DC669 (void);
// 0x000002B2 System.Void MoreMountains.Feedbacks.MMInputSequenceRecorder::StopRecording()
extern void MMInputSequenceRecorder_StopRecording_m76994357E84969793F9C44433169AF0031D28761 (void);
// 0x000002B3 System.Void MoreMountains.Feedbacks.MMInputSequenceRecorder::Update()
extern void MMInputSequenceRecorder_Update_m6D5DC2658C024B564391614C961D49B296E50AB2 (void);
// 0x000002B4 System.Void MoreMountains.Feedbacks.MMInputSequenceRecorder::DetectStartAndEnd()
extern void MMInputSequenceRecorder_DetectStartAndEnd_mE6F87A8D290822081C79F0CEF4801E41B1690C1D (void);
// 0x000002B5 System.Void MoreMountains.Feedbacks.MMInputSequenceRecorder::DetectRecording()
extern void MMInputSequenceRecorder_DetectRecording_mF55B0AB9FAD5B9DEABCB6CA3756108A0CF3A21AB (void);
// 0x000002B6 System.Void MoreMountains.Feedbacks.MMInputSequenceRecorder::AddNoteToTrack(MoreMountains.Feedbacks.MMSequenceTrack)
extern void MMInputSequenceRecorder_AddNoteToTrack_m0C1DE4F57454AACD9DB6EFE082E78F8611ACC26F (void);
// 0x000002B7 System.Void MoreMountains.Feedbacks.MMInputSequenceRecorder::.ctor()
extern void MMInputSequenceRecorder__ctor_m02DFF8CF878BBF1782EC1C28997765B2C7268E83 (void);
// 0x000002B8 MoreMountains.Feedbacks.MMSequenceNote MoreMountains.Feedbacks.MMSequenceNote::Copy()
extern void MMSequenceNote_Copy_mF6124B2041EEE4C7C9D8432F0940734788E7F990 (void);
// 0x000002B9 System.Void MoreMountains.Feedbacks.MMSequenceNote::.ctor()
extern void MMSequenceNote__ctor_m1B031E1E53135020C92795164BE72B91649ABD3C (void);
// 0x000002BA System.Void MoreMountains.Feedbacks.MMSequenceTrack::SetDefaults(System.Int32)
extern void MMSequenceTrack_SetDefaults_mFD45389DC9DA66162387F810BE42D889E20F5813 (void);
// 0x000002BB System.Void MoreMountains.Feedbacks.MMSequenceTrack::.ctor()
extern void MMSequenceTrack__ctor_m38E01455A5630B4A20371F474F015A5A4B414C47 (void);
// 0x000002BC System.Void MoreMountains.Feedbacks.MMSequenceList::.ctor()
extern void MMSequenceList__ctor_m420889EF0B48A1390C5583746851AC973DEE7DCF (void);
// 0x000002BD System.Int32 MoreMountains.Feedbacks.MMSequence::SortByTimestamp(MoreMountains.Feedbacks.MMSequenceNote,MoreMountains.Feedbacks.MMSequenceNote)
extern void MMSequence_SortByTimestamp_mD47E391798136023D51DD83F52661B418BD4A8A7 (void);
// 0x000002BE System.Void MoreMountains.Feedbacks.MMSequence::SortOriginalSequence()
extern void MMSequence_SortOriginalSequence_m3B5D1C2804EC58B3C6C7259B99B72DC0ADB80156 (void);
// 0x000002BF System.Void MoreMountains.Feedbacks.MMSequence::QuantizeOriginalSequence()
extern void MMSequence_QuantizeOriginalSequence_m7CF80B6C020C9EF66E97BC0FA5EB47D034D32032 (void);
// 0x000002C0 System.Void MoreMountains.Feedbacks.MMSequence::ComputeLength()
extern void MMSequence_ComputeLength_mFB384860819AB5AE18C4A20DEA38284BAFE2E873 (void);
// 0x000002C1 System.Void MoreMountains.Feedbacks.MMSequence::QuantizeSequenceToBPM(System.Collections.Generic.List`1<MoreMountains.Feedbacks.MMSequenceNote>)
extern void MMSequence_QuantizeSequenceToBPM_m75C1E17EB903D32A7AA19D8D9F51FE826E3040D2 (void);
// 0x000002C2 System.Void MoreMountains.Feedbacks.MMSequence::OnValidate()
extern void MMSequence_OnValidate_mD105EABBC9DBBF88324D0C1B2D58634C75F4D722 (void);
// 0x000002C3 System.Void MoreMountains.Feedbacks.MMSequence::RandomizeTrackColors()
extern void MMSequence_RandomizeTrackColors_m04AE9285F7D4338C71FF179DB92192478B4727FC (void);
// 0x000002C4 UnityEngine.Color MoreMountains.Feedbacks.MMSequence::RandomSequenceColor()
extern void MMSequence_RandomSequenceColor_m8D4C9849F03B2853CBA287D6A926C726C506E342 (void);
// 0x000002C5 System.Single MoreMountains.Feedbacks.MMSequence::RoundFloatToArray(System.Single,System.Single[])
extern void MMSequence_RoundFloatToArray_m926E105D8D4BD3FD3B0258E200B36223F7568A10 (void);
// 0x000002C6 System.Void MoreMountains.Feedbacks.MMSequence::.ctor()
extern void MMSequence__ctor_m5554755ECE94AEB5DC4E02A83C17B27A34216324 (void);
// 0x000002C7 System.Void MoreMountains.Feedbacks.MMSequencer::Start()
extern void MMSequencer_Start_m0445C4FE65D88B4115471131D220C964D956E8A0 (void);
// 0x000002C8 System.Void MoreMountains.Feedbacks.MMSequencer::Initialization()
extern void MMSequencer_Initialization_m3C23705E9DF37B94D276D1B376F270F9773B4904 (void);
// 0x000002C9 System.Void MoreMountains.Feedbacks.MMSequencer::ToggleSequence()
extern void MMSequencer_ToggleSequence_m3DD0ABB5A43AE2E6A0A5F42F88571F89B15EC604 (void);
// 0x000002CA System.Void MoreMountains.Feedbacks.MMSequencer::PlaySequence()
extern void MMSequencer_PlaySequence_m4040CD4784974CC3B854A9C354F358DD27873451 (void);
// 0x000002CB System.Void MoreMountains.Feedbacks.MMSequencer::StopSequence()
extern void MMSequencer_StopSequence_m878E50179C42EDF8CF981F604F73940F79A3828A (void);
// 0x000002CC System.Void MoreMountains.Feedbacks.MMSequencer::ClearSequence()
extern void MMSequencer_ClearSequence_mF7A7BF22EF4A27F6FCEE752574F31628B39EFBF5 (void);
// 0x000002CD System.Void MoreMountains.Feedbacks.MMSequencer::Update()
extern void MMSequencer_Update_mBD5467DDD019A6CB62EF7CBD4A6661ACB8AB9426 (void);
// 0x000002CE System.Void MoreMountains.Feedbacks.MMSequencer::HandleBeat()
extern void MMSequencer_HandleBeat_m4F4A38DBB7D6C4A0E62CCFC4A14E0B114AB01123 (void);
// 0x000002CF System.Void MoreMountains.Feedbacks.MMSequencer::PlayBeat()
extern void MMSequencer_PlayBeat_m9E4B52DAC7AAF7697EE7754D2DF75AAF7DFD3BED (void);
// 0x000002D0 System.Void MoreMountains.Feedbacks.MMSequencer::OnBeat()
extern void MMSequencer_OnBeat_m0BA155C1FC1CEDC478E2D91DF734572571452C12 (void);
// 0x000002D1 System.Void MoreMountains.Feedbacks.MMSequencer::PlayTrackEvent(System.Int32)
extern void MMSequencer_PlayTrackEvent_m91F2B589A901C3564B1117141C2D8080D7462C67 (void);
// 0x000002D2 System.Void MoreMountains.Feedbacks.MMSequencer::ToggleActive(System.Int32)
extern void MMSequencer_ToggleActive_mD7F8FB6D6431B78F445148F36990415780CBCD3B (void);
// 0x000002D3 System.Void MoreMountains.Feedbacks.MMSequencer::ToggleStep(System.Int32)
extern void MMSequencer_ToggleStep_m559BE4E7BEDAE45CA24EDCB25D39D321A8B8334E (void);
// 0x000002D4 System.Void MoreMountains.Feedbacks.MMSequencer::PlayMetronomeSound()
extern void MMSequencer_PlayMetronomeSound_m645F259F5BB924D71E47DD488ADD6D8F2794AC2A (void);
// 0x000002D5 System.Void MoreMountains.Feedbacks.MMSequencer::IncrementLength()
extern void MMSequencer_IncrementLength_m7A6582B0A7FC915BED67CEAED6C52CD7CEED05F6 (void);
// 0x000002D6 System.Void MoreMountains.Feedbacks.MMSequencer::DecrementLength()
extern void MMSequencer_DecrementLength_mC199F871B876E27130D23F4F210EFEAA6B0E2CDC (void);
// 0x000002D7 System.Void MoreMountains.Feedbacks.MMSequencer::UpdateTimestampsToMatchNewBPM()
extern void MMSequencer_UpdateTimestampsToMatchNewBPM_m8A0F609B6AD92E64DF5F580636E486EEB95F0933 (void);
// 0x000002D8 System.Void MoreMountains.Feedbacks.MMSequencer::ApplySequencerLengthToSequence()
extern void MMSequencer_ApplySequencerLengthToSequence_m0304CB9034100019D04BB40B8CC9F7FD5AA81816 (void);
// 0x000002D9 System.Void MoreMountains.Feedbacks.MMSequencer::EditorMaintenance()
extern void MMSequencer_EditorMaintenance_m61BCFA1D02114F82100B56495A887C7BA6B4FA95 (void);
// 0x000002DA System.Void MoreMountains.Feedbacks.MMSequencer::SetupTrackEvents()
extern void MMSequencer_SetupTrackEvents_mEEA673117D19AD6A6616540DC33A3522B80EED3B (void);
// 0x000002DB System.Void MoreMountains.Feedbacks.MMSequencer::.ctor()
extern void MMSequencer__ctor_m73E192753C964CFAE94273FDE0B4D14A431A7D80 (void);
// 0x000002DC System.Void MoreMountains.Feedbacks.MMSoundSequencer::Initialization()
extern void MMSoundSequencer_Initialization_m7362A81C22635CCF58B5C99760D31F2EE3E2C562 (void);
// 0x000002DD System.Void MoreMountains.Feedbacks.MMSoundSequencer::OnBeat()
extern void MMSoundSequencer_OnBeat_m06715CF3BB3FE84EFC25ED3712428A2FD8C53087 (void);
// 0x000002DE System.Void MoreMountains.Feedbacks.MMSoundSequencer::PlayTrackEvent(System.Int32)
extern void MMSoundSequencer_PlayTrackEvent_m5D5C5D3A59F44DFA29F67D2F52007FC880480192 (void);
// 0x000002DF System.Void MoreMountains.Feedbacks.MMSoundSequencer::EditorMaintenance()
extern void MMSoundSequencer_EditorMaintenance_mBD2625DFB1C2531CA6C8075F2017E58AD8A6C311 (void);
// 0x000002E0 System.Void MoreMountains.Feedbacks.MMSoundSequencer::SetupSounds()
extern void MMSoundSequencer_SetupSounds_mCCEED7B0E86E5B10A555BE420F1992DFD416E5C8 (void);
// 0x000002E1 System.Void MoreMountains.Feedbacks.MMSoundSequencer::.ctor()
extern void MMSoundSequencer__ctor_mFA46E78077FD0D4C507BD8F0E9F42B8E8C2F848B (void);
// 0x000002E2 System.Void MoreMountains.Feedbacks.MMAudioFilterDistortionShaker::Initialization()
extern void MMAudioFilterDistortionShaker_Initialization_mE710DCE40D385E9556F1A47414E7A7A2D6EA5979 (void);
// 0x000002E3 System.Void MoreMountains.Feedbacks.MMAudioFilterDistortionShaker::Reset()
extern void MMAudioFilterDistortionShaker_Reset_m7A3875FCA4ABB1E76C6631F7044FFFD721E9B7A6 (void);
// 0x000002E4 System.Void MoreMountains.Feedbacks.MMAudioFilterDistortionShaker::Shake()
extern void MMAudioFilterDistortionShaker_Shake_m35A3DA92CD4AD82FDC6DC5FCFBF9621879282F03 (void);
// 0x000002E5 System.Void MoreMountains.Feedbacks.MMAudioFilterDistortionShaker::GrabInitialValues()
extern void MMAudioFilterDistortionShaker_GrabInitialValues_m078DE073A1876046B39CCD7DD3F85434AAA3D7A2 (void);
// 0x000002E6 System.Void MoreMountains.Feedbacks.MMAudioFilterDistortionShaker::OnMMAudioFilterDistortionShakeEvent(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void MMAudioFilterDistortionShaker_OnMMAudioFilterDistortionShakeEvent_mF7A17F48DB58CD7F2908F4226B7E21AEBCC6D456 (void);
// 0x000002E7 System.Void MoreMountains.Feedbacks.MMAudioFilterDistortionShaker::ResetTargetValues()
extern void MMAudioFilterDistortionShaker_ResetTargetValues_m3042C09036763D36134CF76417EAB518384014DC (void);
// 0x000002E8 System.Void MoreMountains.Feedbacks.MMAudioFilterDistortionShaker::ResetShakerValues()
extern void MMAudioFilterDistortionShaker_ResetShakerValues_mA60C7F44011116F27AB870313BCA9EBD19263313 (void);
// 0x000002E9 System.Void MoreMountains.Feedbacks.MMAudioFilterDistortionShaker::StartListening()
extern void MMAudioFilterDistortionShaker_StartListening_m7FC1F2625CE2E6580C6DBAC9BEF213D64BD67353 (void);
// 0x000002EA System.Void MoreMountains.Feedbacks.MMAudioFilterDistortionShaker::StopListening()
extern void MMAudioFilterDistortionShaker_StopListening_m30333FAB7C770F28F1E84ED02C17E2D041957B93 (void);
// 0x000002EB System.Void MoreMountains.Feedbacks.MMAudioFilterDistortionShaker::.ctor()
extern void MMAudioFilterDistortionShaker__ctor_m251114A3108B17CE09265BBDAB4032DDD860F244 (void);
// 0x000002EC System.Void MoreMountains.Feedbacks.MMAudioFilterDistortionShakeEvent::add_OnEvent(MoreMountains.Feedbacks.MMAudioFilterDistortionShakeEvent/Delegate)
extern void MMAudioFilterDistortionShakeEvent_add_OnEvent_m6C1B74957FF95E2900EAD4E47D876404BFD04950 (void);
// 0x000002ED System.Void MoreMountains.Feedbacks.MMAudioFilterDistortionShakeEvent::remove_OnEvent(MoreMountains.Feedbacks.MMAudioFilterDistortionShakeEvent/Delegate)
extern void MMAudioFilterDistortionShakeEvent_remove_OnEvent_m5AD68F71CC37FF9A72945553939E0FFCE033A046 (void);
// 0x000002EE System.Void MoreMountains.Feedbacks.MMAudioFilterDistortionShakeEvent::Register(MoreMountains.Feedbacks.MMAudioFilterDistortionShakeEvent/Delegate)
extern void MMAudioFilterDistortionShakeEvent_Register_mE0524B8DCE6081373BE4BDDC706B80F560A7CF2C (void);
// 0x000002EF System.Void MoreMountains.Feedbacks.MMAudioFilterDistortionShakeEvent::Unregister(MoreMountains.Feedbacks.MMAudioFilterDistortionShakeEvent/Delegate)
extern void MMAudioFilterDistortionShakeEvent_Unregister_mB220C8C7E04F5F374163EFC1892F2080BFAE0D18 (void);
// 0x000002F0 System.Void MoreMountains.Feedbacks.MMAudioFilterDistortionShakeEvent::Trigger(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void MMAudioFilterDistortionShakeEvent_Trigger_m756B65068BEA85145054A128640AF6A92A23CA55 (void);
// 0x000002F1 System.Void MoreMountains.Feedbacks.MMAudioFilterDistortionShakeEvent/Delegate::.ctor(System.Object,System.IntPtr)
extern void Delegate__ctor_m10C935A72D2C159510943B7D7ADA0FA452A58879 (void);
// 0x000002F2 System.Void MoreMountains.Feedbacks.MMAudioFilterDistortionShakeEvent/Delegate::Invoke(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void Delegate_Invoke_m0C10AE31C46BC1458427ACD9CC039977FF162893 (void);
// 0x000002F3 System.IAsyncResult MoreMountains.Feedbacks.MMAudioFilterDistortionShakeEvent/Delegate::BeginInvoke(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean,System.AsyncCallback,System.Object)
extern void Delegate_BeginInvoke_mA4760E135B85C0F5E249AB5FA852FB4ED0D191C9 (void);
// 0x000002F4 System.Void MoreMountains.Feedbacks.MMAudioFilterDistortionShakeEvent/Delegate::EndInvoke(System.IAsyncResult)
extern void Delegate_EndInvoke_m67C2C5F68F0CB46B362FD5B9FBD1BFCE5BD863A6 (void);
// 0x000002F5 System.Void MoreMountains.Feedbacks.MMAudioFilterEchoShaker::Initialization()
extern void MMAudioFilterEchoShaker_Initialization_m7C5217014405267443613FFCB7ACF2A01E5DE08F (void);
// 0x000002F6 System.Void MoreMountains.Feedbacks.MMAudioFilterEchoShaker::Reset()
extern void MMAudioFilterEchoShaker_Reset_m871F0864025C77CBDA4253A603B0850C78ED8641 (void);
// 0x000002F7 System.Void MoreMountains.Feedbacks.MMAudioFilterEchoShaker::Shake()
extern void MMAudioFilterEchoShaker_Shake_m4E065851A0F9064A15A3EB2BA7231216EFC79FE3 (void);
// 0x000002F8 System.Void MoreMountains.Feedbacks.MMAudioFilterEchoShaker::GrabInitialValues()
extern void MMAudioFilterEchoShaker_GrabInitialValues_m3C9F29EE64E76BC2182A27D1CFB59C318D840BD1 (void);
// 0x000002F9 System.Void MoreMountains.Feedbacks.MMAudioFilterEchoShaker::OnMMAudioFilterEchoShakeEvent(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void MMAudioFilterEchoShaker_OnMMAudioFilterEchoShakeEvent_m13F094D59FE70EB1EC005D51361316AA0A3F2059 (void);
// 0x000002FA System.Void MoreMountains.Feedbacks.MMAudioFilterEchoShaker::ResetTargetValues()
extern void MMAudioFilterEchoShaker_ResetTargetValues_m314D60B5E955219CB22F39DD561D3F4C094EC848 (void);
// 0x000002FB System.Void MoreMountains.Feedbacks.MMAudioFilterEchoShaker::ResetShakerValues()
extern void MMAudioFilterEchoShaker_ResetShakerValues_m5CE5AB6CE3C8573DA972256FA58443F2A2441CAB (void);
// 0x000002FC System.Void MoreMountains.Feedbacks.MMAudioFilterEchoShaker::StartListening()
extern void MMAudioFilterEchoShaker_StartListening_m19636A6B3375C2DB42C4D7E3A86817F2225A5112 (void);
// 0x000002FD System.Void MoreMountains.Feedbacks.MMAudioFilterEchoShaker::StopListening()
extern void MMAudioFilterEchoShaker_StopListening_m602F082149CAF92E60DC35A173E7D1684C406FB6 (void);
// 0x000002FE System.Void MoreMountains.Feedbacks.MMAudioFilterEchoShaker::.ctor()
extern void MMAudioFilterEchoShaker__ctor_m6032A29ED960E7095BC83FA79537D4D0B916F780 (void);
// 0x000002FF System.Void MoreMountains.Feedbacks.MMAudioFilterEchoShakeEvent::add_OnEvent(MoreMountains.Feedbacks.MMAudioFilterEchoShakeEvent/Delegate)
extern void MMAudioFilterEchoShakeEvent_add_OnEvent_mDD36188DA056DC56EE6E7F27E91C6EEF7B15A233 (void);
// 0x00000300 System.Void MoreMountains.Feedbacks.MMAudioFilterEchoShakeEvent::remove_OnEvent(MoreMountains.Feedbacks.MMAudioFilterEchoShakeEvent/Delegate)
extern void MMAudioFilterEchoShakeEvent_remove_OnEvent_mB16611A4392C12100D8D1D401565423553F32B5C (void);
// 0x00000301 System.Void MoreMountains.Feedbacks.MMAudioFilterEchoShakeEvent::Register(MoreMountains.Feedbacks.MMAudioFilterEchoShakeEvent/Delegate)
extern void MMAudioFilterEchoShakeEvent_Register_mABEF207986F8C870C85C010020E80BF4765490E7 (void);
// 0x00000302 System.Void MoreMountains.Feedbacks.MMAudioFilterEchoShakeEvent::Unregister(MoreMountains.Feedbacks.MMAudioFilterEchoShakeEvent/Delegate)
extern void MMAudioFilterEchoShakeEvent_Unregister_mE3377C0112225C70348D589D2F8364F37F125239 (void);
// 0x00000303 System.Void MoreMountains.Feedbacks.MMAudioFilterEchoShakeEvent::Trigger(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void MMAudioFilterEchoShakeEvent_Trigger_m56076DD47C6A4A3545578438A7121E5984F2002A (void);
// 0x00000304 System.Void MoreMountains.Feedbacks.MMAudioFilterEchoShakeEvent/Delegate::.ctor(System.Object,System.IntPtr)
extern void Delegate__ctor_m37B81E05E289381139F9F23A570DA5EC826B179E (void);
// 0x00000305 System.Void MoreMountains.Feedbacks.MMAudioFilterEchoShakeEvent/Delegate::Invoke(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void Delegate_Invoke_m48225124D486016751B52E0EE94757CE11910838 (void);
// 0x00000306 System.IAsyncResult MoreMountains.Feedbacks.MMAudioFilterEchoShakeEvent/Delegate::BeginInvoke(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean,System.AsyncCallback,System.Object)
extern void Delegate_BeginInvoke_mE48F00BECB81EC74442582E52460E7FA68A489CD (void);
// 0x00000307 System.Void MoreMountains.Feedbacks.MMAudioFilterEchoShakeEvent/Delegate::EndInvoke(System.IAsyncResult)
extern void Delegate_EndInvoke_m56C41DDDEC461DC85062C790BFD711C5DE1C8574 (void);
// 0x00000308 System.Void MoreMountains.Feedbacks.MMAudioFilterHighPassShaker::Initialization()
extern void MMAudioFilterHighPassShaker_Initialization_m2E87A428DA188619CA741A08A741AE1E5EB5C591 (void);
// 0x00000309 System.Void MoreMountains.Feedbacks.MMAudioFilterHighPassShaker::Reset()
extern void MMAudioFilterHighPassShaker_Reset_m94C8833A3B59CFF193802D2DF5FE19034F66BE4E (void);
// 0x0000030A System.Void MoreMountains.Feedbacks.MMAudioFilterHighPassShaker::Shake()
extern void MMAudioFilterHighPassShaker_Shake_m0CBA31100114E51CAA8C9784E813957BE0E805CD (void);
// 0x0000030B System.Void MoreMountains.Feedbacks.MMAudioFilterHighPassShaker::GrabInitialValues()
extern void MMAudioFilterHighPassShaker_GrabInitialValues_m2B34CD9356F1FA8F8178A4272E2E132D3CC37564 (void);
// 0x0000030C System.Void MoreMountains.Feedbacks.MMAudioFilterHighPassShaker::OnMMAudioFilterHighPassShakeEvent(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void MMAudioFilterHighPassShaker_OnMMAudioFilterHighPassShakeEvent_m7036729E0054DFF35647D0FF328BD27D4AD44E45 (void);
// 0x0000030D System.Void MoreMountains.Feedbacks.MMAudioFilterHighPassShaker::ResetTargetValues()
extern void MMAudioFilterHighPassShaker_ResetTargetValues_mED5F19FB3B4C30DFE5FA8FA28D817EEF44647809 (void);
// 0x0000030E System.Void MoreMountains.Feedbacks.MMAudioFilterHighPassShaker::ResetShakerValues()
extern void MMAudioFilterHighPassShaker_ResetShakerValues_m150313C1D77F18B110B96FBA4C573556829418FA (void);
// 0x0000030F System.Void MoreMountains.Feedbacks.MMAudioFilterHighPassShaker::StartListening()
extern void MMAudioFilterHighPassShaker_StartListening_m59CE3B8CB1E73E6081BC4AF92933550DB28B18A1 (void);
// 0x00000310 System.Void MoreMountains.Feedbacks.MMAudioFilterHighPassShaker::StopListening()
extern void MMAudioFilterHighPassShaker_StopListening_m9DBA88A3EA69F29DB08D8BB1FA08126A7AF91BF1 (void);
// 0x00000311 System.Void MoreMountains.Feedbacks.MMAudioFilterHighPassShaker::.ctor()
extern void MMAudioFilterHighPassShaker__ctor_m08E5BEC3BCB71093303A69E12A8879314A2E26BC (void);
// 0x00000312 System.Void MoreMountains.Feedbacks.MMAudioFilterHighPassShakeEvent::add_OnEvent(MoreMountains.Feedbacks.MMAudioFilterHighPassShakeEvent/Delegate)
extern void MMAudioFilterHighPassShakeEvent_add_OnEvent_mAD684D0BB67097F77B96F5C498B1A81D251EC59E (void);
// 0x00000313 System.Void MoreMountains.Feedbacks.MMAudioFilterHighPassShakeEvent::remove_OnEvent(MoreMountains.Feedbacks.MMAudioFilterHighPassShakeEvent/Delegate)
extern void MMAudioFilterHighPassShakeEvent_remove_OnEvent_m5197485E8EB4C86E447EC66A50BAAA577EE492F5 (void);
// 0x00000314 System.Void MoreMountains.Feedbacks.MMAudioFilterHighPassShakeEvent::Register(MoreMountains.Feedbacks.MMAudioFilterHighPassShakeEvent/Delegate)
extern void MMAudioFilterHighPassShakeEvent_Register_m7C24282588794DDEB79271870B632F8C88987408 (void);
// 0x00000315 System.Void MoreMountains.Feedbacks.MMAudioFilterHighPassShakeEvent::Unregister(MoreMountains.Feedbacks.MMAudioFilterHighPassShakeEvent/Delegate)
extern void MMAudioFilterHighPassShakeEvent_Unregister_m31C5C6969A4E2A2F036D1235E13182BAEA15A818 (void);
// 0x00000316 System.Void MoreMountains.Feedbacks.MMAudioFilterHighPassShakeEvent::Trigger(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void MMAudioFilterHighPassShakeEvent_Trigger_m150A1071549607CFC7E1BBD5149B59BEF1377048 (void);
// 0x00000317 System.Void MoreMountains.Feedbacks.MMAudioFilterHighPassShakeEvent/Delegate::.ctor(System.Object,System.IntPtr)
extern void Delegate__ctor_m0B2EAD031B6281232A7A58A895784CEA98B26614 (void);
// 0x00000318 System.Void MoreMountains.Feedbacks.MMAudioFilterHighPassShakeEvent/Delegate::Invoke(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void Delegate_Invoke_m1738021A9AE7A06EFB1F550DF8E3C9C385CC8EAA (void);
// 0x00000319 System.IAsyncResult MoreMountains.Feedbacks.MMAudioFilterHighPassShakeEvent/Delegate::BeginInvoke(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean,System.AsyncCallback,System.Object)
extern void Delegate_BeginInvoke_mAB796E02D12E192E000C8C88913705C132EE9BBA (void);
// 0x0000031A System.Void MoreMountains.Feedbacks.MMAudioFilterHighPassShakeEvent/Delegate::EndInvoke(System.IAsyncResult)
extern void Delegate_EndInvoke_m6D0D22074C71B8B8E8A0F132C4DB93935EF07494 (void);
// 0x0000031B System.Void MoreMountains.Feedbacks.MMAudioFilterLowPassShaker::Initialization()
extern void MMAudioFilterLowPassShaker_Initialization_m2E793D75F7E787C9F2273F71AD8D06C74B7B4997 (void);
// 0x0000031C System.Void MoreMountains.Feedbacks.MMAudioFilterLowPassShaker::Reset()
extern void MMAudioFilterLowPassShaker_Reset_m6D0719CFD71FAFA30467F096778E1C3029076F20 (void);
// 0x0000031D System.Void MoreMountains.Feedbacks.MMAudioFilterLowPassShaker::Shake()
extern void MMAudioFilterLowPassShaker_Shake_m2C533E6861D15016E080DE67C7C09335FC0DA1F3 (void);
// 0x0000031E System.Void MoreMountains.Feedbacks.MMAudioFilterLowPassShaker::GrabInitialValues()
extern void MMAudioFilterLowPassShaker_GrabInitialValues_mE8382F5E0128F77D25EAE0AAA916B34D2077DEA5 (void);
// 0x0000031F System.Void MoreMountains.Feedbacks.MMAudioFilterLowPassShaker::OnMMAudioFilterLowPassShakeEvent(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void MMAudioFilterLowPassShaker_OnMMAudioFilterLowPassShakeEvent_m08B11D0D84BD46E2CFFB423B5C51A5020E8F35A3 (void);
// 0x00000320 System.Void MoreMountains.Feedbacks.MMAudioFilterLowPassShaker::ResetTargetValues()
extern void MMAudioFilterLowPassShaker_ResetTargetValues_mFE281D35CC22B86DFEB458426637E9D555C3CB19 (void);
// 0x00000321 System.Void MoreMountains.Feedbacks.MMAudioFilterLowPassShaker::ResetShakerValues()
extern void MMAudioFilterLowPassShaker_ResetShakerValues_m4663225F435918B928C5FF8B705B7524D96B78F1 (void);
// 0x00000322 System.Void MoreMountains.Feedbacks.MMAudioFilterLowPassShaker::StartListening()
extern void MMAudioFilterLowPassShaker_StartListening_m7B3D4AFFAD56DDBEE564224B89ED2B5791993036 (void);
// 0x00000323 System.Void MoreMountains.Feedbacks.MMAudioFilterLowPassShaker::StopListening()
extern void MMAudioFilterLowPassShaker_StopListening_m5CF3C06858DF996DDBB3E8036076330DF6DDB1F5 (void);
// 0x00000324 System.Void MoreMountains.Feedbacks.MMAudioFilterLowPassShaker::.ctor()
extern void MMAudioFilterLowPassShaker__ctor_m0EE142DB2910EEB9A1834852BED16D71F3009A14 (void);
// 0x00000325 System.Void MoreMountains.Feedbacks.MMAudioFilterLowPassShakeEvent::add_OnEvent(MoreMountains.Feedbacks.MMAudioFilterLowPassShakeEvent/Delegate)
extern void MMAudioFilterLowPassShakeEvent_add_OnEvent_mFE8F76ADD3D5D406E5863959BC3CA5E20B3555A7 (void);
// 0x00000326 System.Void MoreMountains.Feedbacks.MMAudioFilterLowPassShakeEvent::remove_OnEvent(MoreMountains.Feedbacks.MMAudioFilterLowPassShakeEvent/Delegate)
extern void MMAudioFilterLowPassShakeEvent_remove_OnEvent_mC1C6CE2AC96209549E544A04F9C0317B50283D27 (void);
// 0x00000327 System.Void MoreMountains.Feedbacks.MMAudioFilterLowPassShakeEvent::Register(MoreMountains.Feedbacks.MMAudioFilterLowPassShakeEvent/Delegate)
extern void MMAudioFilterLowPassShakeEvent_Register_mA8A11B3DD7E789A7F663BB4E22CFE4644FF98CEF (void);
// 0x00000328 System.Void MoreMountains.Feedbacks.MMAudioFilterLowPassShakeEvent::Unregister(MoreMountains.Feedbacks.MMAudioFilterLowPassShakeEvent/Delegate)
extern void MMAudioFilterLowPassShakeEvent_Unregister_mD9ED361A041B97CF18A6F465CEB8DBBA0AAF3406 (void);
// 0x00000329 System.Void MoreMountains.Feedbacks.MMAudioFilterLowPassShakeEvent::Trigger(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void MMAudioFilterLowPassShakeEvent_Trigger_m2505BF9F41DA64930359E7BF763AA4BBE7AE3F4B (void);
// 0x0000032A System.Void MoreMountains.Feedbacks.MMAudioFilterLowPassShakeEvent/Delegate::.ctor(System.Object,System.IntPtr)
extern void Delegate__ctor_m659A9911D98B00A2A58F4EBDE62162BB893B599D (void);
// 0x0000032B System.Void MoreMountains.Feedbacks.MMAudioFilterLowPassShakeEvent/Delegate::Invoke(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void Delegate_Invoke_mD7D755969284F55FC1120072B0AA69002BB90A75 (void);
// 0x0000032C System.IAsyncResult MoreMountains.Feedbacks.MMAudioFilterLowPassShakeEvent/Delegate::BeginInvoke(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean,System.AsyncCallback,System.Object)
extern void Delegate_BeginInvoke_m8B1163F05337DA777C99C728AC0D092452B20241 (void);
// 0x0000032D System.Void MoreMountains.Feedbacks.MMAudioFilterLowPassShakeEvent/Delegate::EndInvoke(System.IAsyncResult)
extern void Delegate_EndInvoke_m1F1C106E4920A1EB37E282E1F2C5AF726FCC15BA (void);
// 0x0000032E System.Void MoreMountains.Feedbacks.MMAudioFilterReverbShaker::Initialization()
extern void MMAudioFilterReverbShaker_Initialization_m08739AA15E88EF7379EBB0417FA38535C244FA04 (void);
// 0x0000032F System.Void MoreMountains.Feedbacks.MMAudioFilterReverbShaker::Reset()
extern void MMAudioFilterReverbShaker_Reset_m37A7729AC51193371D8F79FCCB38B5040DBE977D (void);
// 0x00000330 System.Void MoreMountains.Feedbacks.MMAudioFilterReverbShaker::Shake()
extern void MMAudioFilterReverbShaker_Shake_m698D8F6319B7EC1E5A0E0F7E189235C94D493699 (void);
// 0x00000331 System.Void MoreMountains.Feedbacks.MMAudioFilterReverbShaker::GrabInitialValues()
extern void MMAudioFilterReverbShaker_GrabInitialValues_mDECDBDDBE90D55F74D6B7111F794E2F99AA26212 (void);
// 0x00000332 System.Void MoreMountains.Feedbacks.MMAudioFilterReverbShaker::OnMMAudioFilterReverbShakeEvent(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void MMAudioFilterReverbShaker_OnMMAudioFilterReverbShakeEvent_mAE9FDBE55B1F0A8C782BA894714328A1DBBF8B06 (void);
// 0x00000333 System.Void MoreMountains.Feedbacks.MMAudioFilterReverbShaker::ResetTargetValues()
extern void MMAudioFilterReverbShaker_ResetTargetValues_m9FF0E6CE5A24A53AFFCFC10790A6E97787ECA9F9 (void);
// 0x00000334 System.Void MoreMountains.Feedbacks.MMAudioFilterReverbShaker::ResetShakerValues()
extern void MMAudioFilterReverbShaker_ResetShakerValues_mC7345FCABB0CED81B30AD95E0B22E445323FD728 (void);
// 0x00000335 System.Void MoreMountains.Feedbacks.MMAudioFilterReverbShaker::StartListening()
extern void MMAudioFilterReverbShaker_StartListening_m5C3D0C6F78D1E96D8B56EF68A852E9574B917BC5 (void);
// 0x00000336 System.Void MoreMountains.Feedbacks.MMAudioFilterReverbShaker::StopListening()
extern void MMAudioFilterReverbShaker_StopListening_m0FCE70E0C99ED0328CFD26ECC0016F3D585083FE (void);
// 0x00000337 System.Void MoreMountains.Feedbacks.MMAudioFilterReverbShaker::.ctor()
extern void MMAudioFilterReverbShaker__ctor_mB3C4E11D7D117122077E164A434208D8287A404C (void);
// 0x00000338 System.Void MoreMountains.Feedbacks.MMAudioFilterReverbShakeEvent::add_OnEvent(MoreMountains.Feedbacks.MMAudioFilterReverbShakeEvent/Delegate)
extern void MMAudioFilterReverbShakeEvent_add_OnEvent_mE7E00540BC9476E666298002E23E56A400C4E423 (void);
// 0x00000339 System.Void MoreMountains.Feedbacks.MMAudioFilterReverbShakeEvent::remove_OnEvent(MoreMountains.Feedbacks.MMAudioFilterReverbShakeEvent/Delegate)
extern void MMAudioFilterReverbShakeEvent_remove_OnEvent_mA94664A791C5A129C8C64E1D87346D53532999F1 (void);
// 0x0000033A System.Void MoreMountains.Feedbacks.MMAudioFilterReverbShakeEvent::Register(MoreMountains.Feedbacks.MMAudioFilterReverbShakeEvent/Delegate)
extern void MMAudioFilterReverbShakeEvent_Register_mA25306BB4583BFF410CD121DBB8A047AF8B711DD (void);
// 0x0000033B System.Void MoreMountains.Feedbacks.MMAudioFilterReverbShakeEvent::Unregister(MoreMountains.Feedbacks.MMAudioFilterReverbShakeEvent/Delegate)
extern void MMAudioFilterReverbShakeEvent_Unregister_m352ADA01B96DE9EF281B0AB0171C4D1118FDDC17 (void);
// 0x0000033C System.Void MoreMountains.Feedbacks.MMAudioFilterReverbShakeEvent::Trigger(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void MMAudioFilterReverbShakeEvent_Trigger_mAB175A79779BBD4A6C3019B762CA667E85422FED (void);
// 0x0000033D System.Void MoreMountains.Feedbacks.MMAudioFilterReverbShakeEvent/Delegate::.ctor(System.Object,System.IntPtr)
extern void Delegate__ctor_m00D6E3E71770FC6FB825648AA04713C12BD6AEBE (void);
// 0x0000033E System.Void MoreMountains.Feedbacks.MMAudioFilterReverbShakeEvent/Delegate::Invoke(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void Delegate_Invoke_mB3FE8D5087587F521631032D88DEEB6E9D757FBE (void);
// 0x0000033F System.IAsyncResult MoreMountains.Feedbacks.MMAudioFilterReverbShakeEvent/Delegate::BeginInvoke(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean,System.AsyncCallback,System.Object)
extern void Delegate_BeginInvoke_m35B9359AC8A3915A9EA34239BE48D54559713E7F (void);
// 0x00000340 System.Void MoreMountains.Feedbacks.MMAudioFilterReverbShakeEvent/Delegate::EndInvoke(System.IAsyncResult)
extern void Delegate_EndInvoke_m0BF01F522F3F851438D6F3385429938DA926752B (void);
// 0x00000341 System.Void MoreMountains.Feedbacks.MMAudioSourcePitchShaker::Initialization()
extern void MMAudioSourcePitchShaker_Initialization_mABC21C431652EAC31454968FB667DF499D4C404E (void);
// 0x00000342 System.Void MoreMountains.Feedbacks.MMAudioSourcePitchShaker::Reset()
extern void MMAudioSourcePitchShaker_Reset_m31F4BE0C67B90BD9DFAF10EE295690758A0B0FAA (void);
// 0x00000343 System.Void MoreMountains.Feedbacks.MMAudioSourcePitchShaker::Shake()
extern void MMAudioSourcePitchShaker_Shake_mD24EB294C6CA37F3839E4D4970B8556B99C88F69 (void);
// 0x00000344 System.Void MoreMountains.Feedbacks.MMAudioSourcePitchShaker::GrabInitialValues()
extern void MMAudioSourcePitchShaker_GrabInitialValues_m45E4FA1277114EF4EC73A7AB18079F2C3027DA29 (void);
// 0x00000345 System.Void MoreMountains.Feedbacks.MMAudioSourcePitchShaker::OnMMAudioSourcePitchShakeEvent(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void MMAudioSourcePitchShaker_OnMMAudioSourcePitchShakeEvent_m1E03D63FED3C6DF0A13FD5353397615D6FEB9963 (void);
// 0x00000346 System.Void MoreMountains.Feedbacks.MMAudioSourcePitchShaker::ResetTargetValues()
extern void MMAudioSourcePitchShaker_ResetTargetValues_m377F786B3FFDAD51A08B9DF7E081ECD5B6A9ED67 (void);
// 0x00000347 System.Void MoreMountains.Feedbacks.MMAudioSourcePitchShaker::ResetShakerValues()
extern void MMAudioSourcePitchShaker_ResetShakerValues_m7AB45315CCD9A23C6E4DC3D2232595D971944F4F (void);
// 0x00000348 System.Void MoreMountains.Feedbacks.MMAudioSourcePitchShaker::StartListening()
extern void MMAudioSourcePitchShaker_StartListening_mB87F2A18EA33A847F268CAC2C84141F2BBFA5D16 (void);
// 0x00000349 System.Void MoreMountains.Feedbacks.MMAudioSourcePitchShaker::StopListening()
extern void MMAudioSourcePitchShaker_StopListening_m0EF1E7EC2EDC478D6FA52E11A09FBD69B911C694 (void);
// 0x0000034A System.Void MoreMountains.Feedbacks.MMAudioSourcePitchShaker::.ctor()
extern void MMAudioSourcePitchShaker__ctor_m5856690EE3A6618C38DFFCFEC81866F46DF0888C (void);
// 0x0000034B System.Void MoreMountains.Feedbacks.MMAudioSourcePitchShakeEvent::add_OnEvent(MoreMountains.Feedbacks.MMAudioSourcePitchShakeEvent/Delegate)
extern void MMAudioSourcePitchShakeEvent_add_OnEvent_m4C43FC4750F12AEF149F954EE808CA45FB75AF2C (void);
// 0x0000034C System.Void MoreMountains.Feedbacks.MMAudioSourcePitchShakeEvent::remove_OnEvent(MoreMountains.Feedbacks.MMAudioSourcePitchShakeEvent/Delegate)
extern void MMAudioSourcePitchShakeEvent_remove_OnEvent_m1298F190E6797D72FBE726B942E95B8A3E6575A4 (void);
// 0x0000034D System.Void MoreMountains.Feedbacks.MMAudioSourcePitchShakeEvent::Register(MoreMountains.Feedbacks.MMAudioSourcePitchShakeEvent/Delegate)
extern void MMAudioSourcePitchShakeEvent_Register_m13544C89AC62AA1852A43D631D119DFF7FF53940 (void);
// 0x0000034E System.Void MoreMountains.Feedbacks.MMAudioSourcePitchShakeEvent::Unregister(MoreMountains.Feedbacks.MMAudioSourcePitchShakeEvent/Delegate)
extern void MMAudioSourcePitchShakeEvent_Unregister_m02690AC42A1E3055B502C5CA6A375FCE905861B5 (void);
// 0x0000034F System.Void MoreMountains.Feedbacks.MMAudioSourcePitchShakeEvent::Trigger(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void MMAudioSourcePitchShakeEvent_Trigger_mBCA8ED3145E909496C1358C13D9BA3BD8EACED89 (void);
// 0x00000350 System.Void MoreMountains.Feedbacks.MMAudioSourcePitchShakeEvent/Delegate::.ctor(System.Object,System.IntPtr)
extern void Delegate__ctor_mCF8A9A41268551BC9D8B73A730D90911436295EC (void);
// 0x00000351 System.Void MoreMountains.Feedbacks.MMAudioSourcePitchShakeEvent/Delegate::Invoke(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void Delegate_Invoke_mA8F3CC35CE955FC617422D001BD481507F371015 (void);
// 0x00000352 System.IAsyncResult MoreMountains.Feedbacks.MMAudioSourcePitchShakeEvent/Delegate::BeginInvoke(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean,System.AsyncCallback,System.Object)
extern void Delegate_BeginInvoke_mC018DB947EB4E7604C26AEA8C14CC4052FC2C7E5 (void);
// 0x00000353 System.Void MoreMountains.Feedbacks.MMAudioSourcePitchShakeEvent/Delegate::EndInvoke(System.IAsyncResult)
extern void Delegate_EndInvoke_m77AC9DA907FD199FEA7493692DAFCB245BD4DDBC (void);
// 0x00000354 System.Void MoreMountains.Feedbacks.MMAudioSourceStereoPanShaker::Initialization()
extern void MMAudioSourceStereoPanShaker_Initialization_m739827F6D6DF7AF6A70C26D688927A6EFBC10417 (void);
// 0x00000355 System.Void MoreMountains.Feedbacks.MMAudioSourceStereoPanShaker::Reset()
extern void MMAudioSourceStereoPanShaker_Reset_m596976C4B964FF1E2A2EDCC0DBF6A7756C7530F4 (void);
// 0x00000356 System.Void MoreMountains.Feedbacks.MMAudioSourceStereoPanShaker::Shake()
extern void MMAudioSourceStereoPanShaker_Shake_mE2B90965F79A759D214A07F1E34B4F1845C5445A (void);
// 0x00000357 System.Void MoreMountains.Feedbacks.MMAudioSourceStereoPanShaker::GrabInitialValues()
extern void MMAudioSourceStereoPanShaker_GrabInitialValues_m5B9A93DAFEBA781A18522CED75C572752E230035 (void);
// 0x00000358 System.Void MoreMountains.Feedbacks.MMAudioSourceStereoPanShaker::OnMMAudioSourceStereoPanShakeEvent(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void MMAudioSourceStereoPanShaker_OnMMAudioSourceStereoPanShakeEvent_m5B6D9062536D0CD69150478929946C2EEB724B38 (void);
// 0x00000359 System.Void MoreMountains.Feedbacks.MMAudioSourceStereoPanShaker::ResetTargetValues()
extern void MMAudioSourceStereoPanShaker_ResetTargetValues_mD6301F32718E1D74D4C9F0479DD9B272DE409F26 (void);
// 0x0000035A System.Void MoreMountains.Feedbacks.MMAudioSourceStereoPanShaker::ResetShakerValues()
extern void MMAudioSourceStereoPanShaker_ResetShakerValues_m18E3EDCEDD23D62636909C400BC2A623846044F0 (void);
// 0x0000035B System.Void MoreMountains.Feedbacks.MMAudioSourceStereoPanShaker::StartListening()
extern void MMAudioSourceStereoPanShaker_StartListening_m2A821ADAA5B587B67374A3AE5F64615BCAFABD80 (void);
// 0x0000035C System.Void MoreMountains.Feedbacks.MMAudioSourceStereoPanShaker::StopListening()
extern void MMAudioSourceStereoPanShaker_StopListening_mDBC458D08E3C6EA0C963A088A8ADF424AAF7F63F (void);
// 0x0000035D System.Void MoreMountains.Feedbacks.MMAudioSourceStereoPanShaker::.ctor()
extern void MMAudioSourceStereoPanShaker__ctor_m975C51828C963DFB826783EC14189F69FA785055 (void);
// 0x0000035E System.Void MoreMountains.Feedbacks.MMAudioSourceStereoPanShakeEvent::add_OnEvent(MoreMountains.Feedbacks.MMAudioSourceStereoPanShakeEvent/Delegate)
extern void MMAudioSourceStereoPanShakeEvent_add_OnEvent_m96EE77769C8621F15791D9C138B3C491B291B0F0 (void);
// 0x0000035F System.Void MoreMountains.Feedbacks.MMAudioSourceStereoPanShakeEvent::remove_OnEvent(MoreMountains.Feedbacks.MMAudioSourceStereoPanShakeEvent/Delegate)
extern void MMAudioSourceStereoPanShakeEvent_remove_OnEvent_mE06FC8EF873961736BC1CD2B80C1518811B924F8 (void);
// 0x00000360 System.Void MoreMountains.Feedbacks.MMAudioSourceStereoPanShakeEvent::Register(MoreMountains.Feedbacks.MMAudioSourceStereoPanShakeEvent/Delegate)
extern void MMAudioSourceStereoPanShakeEvent_Register_mA52EDF362DCED0AF9C21553EC2337BB03BE99DA0 (void);
// 0x00000361 System.Void MoreMountains.Feedbacks.MMAudioSourceStereoPanShakeEvent::Unregister(MoreMountains.Feedbacks.MMAudioSourceStereoPanShakeEvent/Delegate)
extern void MMAudioSourceStereoPanShakeEvent_Unregister_m686A253DAFCCFEFD2311428BF89827EECE8E79C8 (void);
// 0x00000362 System.Void MoreMountains.Feedbacks.MMAudioSourceStereoPanShakeEvent::Trigger(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void MMAudioSourceStereoPanShakeEvent_Trigger_m2A6E8C23D3959E9B9B5F9CEC3DB1F434B25E66D2 (void);
// 0x00000363 System.Void MoreMountains.Feedbacks.MMAudioSourceStereoPanShakeEvent/Delegate::.ctor(System.Object,System.IntPtr)
extern void Delegate__ctor_m53F954389DF9EC3E36BB31A0FAAC46ECF049046F (void);
// 0x00000364 System.Void MoreMountains.Feedbacks.MMAudioSourceStereoPanShakeEvent/Delegate::Invoke(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void Delegate_Invoke_mA7448ED2CFC09FBC74100305870D805517B597A3 (void);
// 0x00000365 System.IAsyncResult MoreMountains.Feedbacks.MMAudioSourceStereoPanShakeEvent/Delegate::BeginInvoke(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean,System.AsyncCallback,System.Object)
extern void Delegate_BeginInvoke_m793D6B2B4A8596AD57616066611C63EC70CC9E0C (void);
// 0x00000366 System.Void MoreMountains.Feedbacks.MMAudioSourceStereoPanShakeEvent/Delegate::EndInvoke(System.IAsyncResult)
extern void Delegate_EndInvoke_mAD3D804E6529662E644E6FCA63ECC7205877285D (void);
// 0x00000367 System.Void MoreMountains.Feedbacks.MMAudioSourceVolumeShaker::Initialization()
extern void MMAudioSourceVolumeShaker_Initialization_m6F7B6E1136C65B196FC6923C2695A70E67721802 (void);
// 0x00000368 System.Void MoreMountains.Feedbacks.MMAudioSourceVolumeShaker::Reset()
extern void MMAudioSourceVolumeShaker_Reset_m9846A94481DD04017ECE38C8B3E53F808B2F3BF9 (void);
// 0x00000369 System.Void MoreMountains.Feedbacks.MMAudioSourceVolumeShaker::Shake()
extern void MMAudioSourceVolumeShaker_Shake_mEDC59448FBCAC55AABF6FEEA05A0516AC99C4607 (void);
// 0x0000036A System.Void MoreMountains.Feedbacks.MMAudioSourceVolumeShaker::GrabInitialValues()
extern void MMAudioSourceVolumeShaker_GrabInitialValues_m41ECC648544F26BA2064AAF764C1FBED60B8B2D7 (void);
// 0x0000036B System.Void MoreMountains.Feedbacks.MMAudioSourceVolumeShaker::OnMMAudioSourceVolumeShakeEvent(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void MMAudioSourceVolumeShaker_OnMMAudioSourceVolumeShakeEvent_mC89D6153C764F2E55A24CE677FDB17BDC2F893DA (void);
// 0x0000036C System.Void MoreMountains.Feedbacks.MMAudioSourceVolumeShaker::ResetTargetValues()
extern void MMAudioSourceVolumeShaker_ResetTargetValues_m7C045EF52CCC79D1F0543A155C6C6CEE5058A192 (void);
// 0x0000036D System.Void MoreMountains.Feedbacks.MMAudioSourceVolumeShaker::ResetShakerValues()
extern void MMAudioSourceVolumeShaker_ResetShakerValues_m9B9E8100173064BADD2CC29D8DCAF796582FDF03 (void);
// 0x0000036E System.Void MoreMountains.Feedbacks.MMAudioSourceVolumeShaker::StartListening()
extern void MMAudioSourceVolumeShaker_StartListening_m9854A36B596B4648DB5273921219D3EA926AF0F1 (void);
// 0x0000036F System.Void MoreMountains.Feedbacks.MMAudioSourceVolumeShaker::StopListening()
extern void MMAudioSourceVolumeShaker_StopListening_mA0D40CD8CBDA5B5226859C474427D7B539663484 (void);
// 0x00000370 System.Void MoreMountains.Feedbacks.MMAudioSourceVolumeShaker::.ctor()
extern void MMAudioSourceVolumeShaker__ctor_m94D411F3ABD94FCA6C2C4B6B5BE971376A8B138A (void);
// 0x00000371 System.Void MoreMountains.Feedbacks.MMAudioSourceVolumeShakeEvent::add_OnEvent(MoreMountains.Feedbacks.MMAudioSourceVolumeShakeEvent/Delegate)
extern void MMAudioSourceVolumeShakeEvent_add_OnEvent_mF2B9B24D7FE3DC35F1D9EF33FCEF07E8AD1EB105 (void);
// 0x00000372 System.Void MoreMountains.Feedbacks.MMAudioSourceVolumeShakeEvent::remove_OnEvent(MoreMountains.Feedbacks.MMAudioSourceVolumeShakeEvent/Delegate)
extern void MMAudioSourceVolumeShakeEvent_remove_OnEvent_mF856F2DE6A95B4AA80D0A66CBC7718B4636C0743 (void);
// 0x00000373 System.Void MoreMountains.Feedbacks.MMAudioSourceVolumeShakeEvent::Register(MoreMountains.Feedbacks.MMAudioSourceVolumeShakeEvent/Delegate)
extern void MMAudioSourceVolumeShakeEvent_Register_m2AC82EC63351B0DBC4D08F1A4DE2991AB37AFED1 (void);
// 0x00000374 System.Void MoreMountains.Feedbacks.MMAudioSourceVolumeShakeEvent::Unregister(MoreMountains.Feedbacks.MMAudioSourceVolumeShakeEvent/Delegate)
extern void MMAudioSourceVolumeShakeEvent_Unregister_m5B7B6CEE5AFA4C930850E7BD5019C3D7B898C7E5 (void);
// 0x00000375 System.Void MoreMountains.Feedbacks.MMAudioSourceVolumeShakeEvent::Trigger(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void MMAudioSourceVolumeShakeEvent_Trigger_m65BD14EF7D1A543F46A7429D5A1F907BE47AC0C4 (void);
// 0x00000376 System.Void MoreMountains.Feedbacks.MMAudioSourceVolumeShakeEvent/Delegate::.ctor(System.Object,System.IntPtr)
extern void Delegate__ctor_m9D80BE20FD66454DC2A247B82000F8C6430D431D (void);
// 0x00000377 System.Void MoreMountains.Feedbacks.MMAudioSourceVolumeShakeEvent/Delegate::Invoke(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void Delegate_Invoke_m9E21DF25524680A987009ECB0649E581C0397F8D (void);
// 0x00000378 System.IAsyncResult MoreMountains.Feedbacks.MMAudioSourceVolumeShakeEvent/Delegate::BeginInvoke(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean,System.AsyncCallback,System.Object)
extern void Delegate_BeginInvoke_m374B2E89BB9D3BB401C10D50190293F749941E7C (void);
// 0x00000379 System.Void MoreMountains.Feedbacks.MMAudioSourceVolumeShakeEvent/Delegate::EndInvoke(System.IAsyncResult)
extern void Delegate_EndInvoke_m0B8F23634D484E2DBA803A460705B4B63DAEAD84 (void);
// 0x0000037A System.Void MoreMountains.Feedbacks.MMCameraClippingPlanesShaker::Initialization()
extern void MMCameraClippingPlanesShaker_Initialization_mAB74F21C54F534D24AFA12EB9073B9DF934485FE (void);
// 0x0000037B System.Void MoreMountains.Feedbacks.MMCameraClippingPlanesShaker::Reset()
extern void MMCameraClippingPlanesShaker_Reset_mB6C829F3668ADC0F9D4B82CD9F97E98CE684888D (void);
// 0x0000037C System.Void MoreMountains.Feedbacks.MMCameraClippingPlanesShaker::Shake()
extern void MMCameraClippingPlanesShaker_Shake_m3CCD29F7F110A08D6B105F52FE8A9A7B0C52BE76 (void);
// 0x0000037D System.Void MoreMountains.Feedbacks.MMCameraClippingPlanesShaker::GrabInitialValues()
extern void MMCameraClippingPlanesShaker_GrabInitialValues_mAD96A12B25BAC90B3AC96A8170D06AACF4C05270 (void);
// 0x0000037E System.Void MoreMountains.Feedbacks.MMCameraClippingPlanesShaker::OnMMCameraClippingPlanesShakeEvent(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void MMCameraClippingPlanesShaker_OnMMCameraClippingPlanesShakeEvent_m7B1BD0E874CBC2AF15FACBF967D3347CFA150BEE (void);
// 0x0000037F System.Void MoreMountains.Feedbacks.MMCameraClippingPlanesShaker::ResetTargetValues()
extern void MMCameraClippingPlanesShaker_ResetTargetValues_m84C61C85871B5326157DF6A80745B18C849876DD (void);
// 0x00000380 System.Void MoreMountains.Feedbacks.MMCameraClippingPlanesShaker::ResetShakerValues()
extern void MMCameraClippingPlanesShaker_ResetShakerValues_mEE7DEC587820786672D06127F215F8987794379D (void);
// 0x00000381 System.Void MoreMountains.Feedbacks.MMCameraClippingPlanesShaker::StartListening()
extern void MMCameraClippingPlanesShaker_StartListening_mBADD2F2901C9BF4563E42233CE15C35CA74678C6 (void);
// 0x00000382 System.Void MoreMountains.Feedbacks.MMCameraClippingPlanesShaker::StopListening()
extern void MMCameraClippingPlanesShaker_StopListening_m60ABF19E631AAF780BAF3626DBC238A799C5F9E0 (void);
// 0x00000383 System.Void MoreMountains.Feedbacks.MMCameraClippingPlanesShaker::.ctor()
extern void MMCameraClippingPlanesShaker__ctor_m6F8EB5D3EB689EC3253AD26E0BF8CB34A3EF6A0B (void);
// 0x00000384 System.Void MoreMountains.Feedbacks.MMCameraClippingPlanesShakeEvent::add_OnEvent(MoreMountains.Feedbacks.MMCameraClippingPlanesShakeEvent/Delegate)
extern void MMCameraClippingPlanesShakeEvent_add_OnEvent_mA7D3A5E11DF71E52B549F05A437D1B0194E639C6 (void);
// 0x00000385 System.Void MoreMountains.Feedbacks.MMCameraClippingPlanesShakeEvent::remove_OnEvent(MoreMountains.Feedbacks.MMCameraClippingPlanesShakeEvent/Delegate)
extern void MMCameraClippingPlanesShakeEvent_remove_OnEvent_m922EB0801D52E18D5E2333850309460D6AE36DD8 (void);
// 0x00000386 System.Void MoreMountains.Feedbacks.MMCameraClippingPlanesShakeEvent::Register(MoreMountains.Feedbacks.MMCameraClippingPlanesShakeEvent/Delegate)
extern void MMCameraClippingPlanesShakeEvent_Register_m6A7DECCE069AAD9D2E02F0FC3F05C914EFCB262B (void);
// 0x00000387 System.Void MoreMountains.Feedbacks.MMCameraClippingPlanesShakeEvent::Unregister(MoreMountains.Feedbacks.MMCameraClippingPlanesShakeEvent/Delegate)
extern void MMCameraClippingPlanesShakeEvent_Unregister_m0B491D7BFD7512928B33E4143AC6923BCE0CAB18 (void);
// 0x00000388 System.Void MoreMountains.Feedbacks.MMCameraClippingPlanesShakeEvent::Trigger(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void MMCameraClippingPlanesShakeEvent_Trigger_mBAF1CB0B2CC4F9BBC13295E4AE2B69F22F13C748 (void);
// 0x00000389 System.Void MoreMountains.Feedbacks.MMCameraClippingPlanesShakeEvent/Delegate::.ctor(System.Object,System.IntPtr)
extern void Delegate__ctor_m733422BE0BC41D7D63A7B61424293CD781087971 (void);
// 0x0000038A System.Void MoreMountains.Feedbacks.MMCameraClippingPlanesShakeEvent/Delegate::Invoke(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void Delegate_Invoke_m0C4FCBBE5260E3696C6FDBB8503EEB6C2C3E6B41 (void);
// 0x0000038B System.IAsyncResult MoreMountains.Feedbacks.MMCameraClippingPlanesShakeEvent/Delegate::BeginInvoke(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean,System.AsyncCallback,System.Object)
extern void Delegate_BeginInvoke_m0C94C5BD9A7EC333CBCAD1D726AA14E54A75541C (void);
// 0x0000038C System.Void MoreMountains.Feedbacks.MMCameraClippingPlanesShakeEvent/Delegate::EndInvoke(System.IAsyncResult)
extern void Delegate_EndInvoke_m6C66B64D8A260E72F5CD11068C13575BE4B292B5 (void);
// 0x0000038D System.Void MoreMountains.Feedbacks.MMCameraFieldOfViewShaker::Initialization()
extern void MMCameraFieldOfViewShaker_Initialization_m13A0FE7F26E9FD60D0C9D64978EB5BD6CDE114A9 (void);
// 0x0000038E System.Void MoreMountains.Feedbacks.MMCameraFieldOfViewShaker::Reset()
extern void MMCameraFieldOfViewShaker_Reset_mB34A0582D12D863A2E066AC1E80924DA61D4F4DF (void);
// 0x0000038F System.Void MoreMountains.Feedbacks.MMCameraFieldOfViewShaker::Shake()
extern void MMCameraFieldOfViewShaker_Shake_m0738400E4F4D897727091A5FBC8070F163E141E1 (void);
// 0x00000390 System.Void MoreMountains.Feedbacks.MMCameraFieldOfViewShaker::GrabInitialValues()
extern void MMCameraFieldOfViewShaker_GrabInitialValues_m5E878303BAA591F23ACA9C3093676381506B7B9D (void);
// 0x00000391 System.Void MoreMountains.Feedbacks.MMCameraFieldOfViewShaker::OnMMCameraFieldOfViewShakeEvent(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void MMCameraFieldOfViewShaker_OnMMCameraFieldOfViewShakeEvent_m1B8D55671C981D3A9BF23FE232980DCB2CEE8178 (void);
// 0x00000392 System.Void MoreMountains.Feedbacks.MMCameraFieldOfViewShaker::ResetTargetValues()
extern void MMCameraFieldOfViewShaker_ResetTargetValues_mC1D9DD2383F71D79C7B0EC47E836D54E2A7FD16B (void);
// 0x00000393 System.Void MoreMountains.Feedbacks.MMCameraFieldOfViewShaker::ResetShakerValues()
extern void MMCameraFieldOfViewShaker_ResetShakerValues_m87093D801FAB0648628DBBA9F8BFD67C1652838A (void);
// 0x00000394 System.Void MoreMountains.Feedbacks.MMCameraFieldOfViewShaker::StartListening()
extern void MMCameraFieldOfViewShaker_StartListening_m1DEDA4E892A6FCDC1C298FC552E3120C234F4C17 (void);
// 0x00000395 System.Void MoreMountains.Feedbacks.MMCameraFieldOfViewShaker::StopListening()
extern void MMCameraFieldOfViewShaker_StopListening_mDE82519EE9937CE3635AFF41FE911B5D93952122 (void);
// 0x00000396 System.Void MoreMountains.Feedbacks.MMCameraFieldOfViewShaker::.ctor()
extern void MMCameraFieldOfViewShaker__ctor_m18C5B1EBEE53B4835720EA5A5A4563BB61CA79BA (void);
// 0x00000397 System.Void MoreMountains.Feedbacks.MMCameraFieldOfViewShakeEvent::add_OnEvent(MoreMountains.Feedbacks.MMCameraFieldOfViewShakeEvent/Delegate)
extern void MMCameraFieldOfViewShakeEvent_add_OnEvent_m9BA76AE92CB9125D6AB85C313AC380BD8B6DFE39 (void);
// 0x00000398 System.Void MoreMountains.Feedbacks.MMCameraFieldOfViewShakeEvent::remove_OnEvent(MoreMountains.Feedbacks.MMCameraFieldOfViewShakeEvent/Delegate)
extern void MMCameraFieldOfViewShakeEvent_remove_OnEvent_mADB6D177C90C4E69148CAD9F14EDB99BA56ED346 (void);
// 0x00000399 System.Void MoreMountains.Feedbacks.MMCameraFieldOfViewShakeEvent::Register(MoreMountains.Feedbacks.MMCameraFieldOfViewShakeEvent/Delegate)
extern void MMCameraFieldOfViewShakeEvent_Register_m19C5C43176354FCE199695648EC34C8EB18F6637 (void);
// 0x0000039A System.Void MoreMountains.Feedbacks.MMCameraFieldOfViewShakeEvent::Unregister(MoreMountains.Feedbacks.MMCameraFieldOfViewShakeEvent/Delegate)
extern void MMCameraFieldOfViewShakeEvent_Unregister_m3EB17557FCCF2C6981EB1C688AD34FB4860C28BF (void);
// 0x0000039B System.Void MoreMountains.Feedbacks.MMCameraFieldOfViewShakeEvent::Trigger(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void MMCameraFieldOfViewShakeEvent_Trigger_m2A3A3C637A68043DF3135D78F152C1EC29AE26D6 (void);
// 0x0000039C System.Void MoreMountains.Feedbacks.MMCameraFieldOfViewShakeEvent/Delegate::.ctor(System.Object,System.IntPtr)
extern void Delegate__ctor_mFBE74CEFCF7377CCFBD4E381A7C7841542690FD7 (void);
// 0x0000039D System.Void MoreMountains.Feedbacks.MMCameraFieldOfViewShakeEvent/Delegate::Invoke(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void Delegate_Invoke_m2B3B5BBBB7DAF61A7BB8EBF12352E51A55F61969 (void);
// 0x0000039E System.IAsyncResult MoreMountains.Feedbacks.MMCameraFieldOfViewShakeEvent/Delegate::BeginInvoke(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean,System.AsyncCallback,System.Object)
extern void Delegate_BeginInvoke_m8245612ABDF67A089EE78C6B6D4C8BFB2FFCAE59 (void);
// 0x0000039F System.Void MoreMountains.Feedbacks.MMCameraFieldOfViewShakeEvent/Delegate::EndInvoke(System.IAsyncResult)
extern void Delegate_EndInvoke_m8E68639AE58C45E26B9B6C1DAB98484AA85E3D63 (void);
// 0x000003A0 System.Void MoreMountains.Feedbacks.MMCameraOrthographicSizeShaker::Initialization()
extern void MMCameraOrthographicSizeShaker_Initialization_mDB51ED01B8527F8AB20C66458B81F4E53E5E1935 (void);
// 0x000003A1 System.Void MoreMountains.Feedbacks.MMCameraOrthographicSizeShaker::Reset()
extern void MMCameraOrthographicSizeShaker_Reset_m7E67ED69F8911A268476C1FDCC140A4C587B689E (void);
// 0x000003A2 System.Void MoreMountains.Feedbacks.MMCameraOrthographicSizeShaker::Shake()
extern void MMCameraOrthographicSizeShaker_Shake_m9BB532AAD51012CDEFB54699E4236B015A7CBF9E (void);
// 0x000003A3 System.Void MoreMountains.Feedbacks.MMCameraOrthographicSizeShaker::GrabInitialValues()
extern void MMCameraOrthographicSizeShaker_GrabInitialValues_m4CA28A55235AA50C268F25FC4F45B5921AA77909 (void);
// 0x000003A4 System.Void MoreMountains.Feedbacks.MMCameraOrthographicSizeShaker::OnMMCameraOrthographicSizeShakeEvent(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,System.Boolean)
extern void MMCameraOrthographicSizeShaker_OnMMCameraOrthographicSizeShakeEvent_m3F1FF6791F349E05C7529119D4EB1C544D6D5E94 (void);
// 0x000003A5 System.Void MoreMountains.Feedbacks.MMCameraOrthographicSizeShaker::ResetTargetValues()
extern void MMCameraOrthographicSizeShaker_ResetTargetValues_mD1FF6AF178DF0E7F4A27992BBCCBF0862F0C0BF6 (void);
// 0x000003A6 System.Void MoreMountains.Feedbacks.MMCameraOrthographicSizeShaker::ResetShakerValues()
extern void MMCameraOrthographicSizeShaker_ResetShakerValues_m541AC99762F216D163F1052FF96606476376AAE0 (void);
// 0x000003A7 System.Void MoreMountains.Feedbacks.MMCameraOrthographicSizeShaker::StartListening()
extern void MMCameraOrthographicSizeShaker_StartListening_m84028EFD7EF5D6BBA3C079319CD1C5408A6B0D36 (void);
// 0x000003A8 System.Void MoreMountains.Feedbacks.MMCameraOrthographicSizeShaker::StopListening()
extern void MMCameraOrthographicSizeShaker_StopListening_mEECA7F03FCC796C240C6006FA902A6A2A9EB42D5 (void);
// 0x000003A9 System.Void MoreMountains.Feedbacks.MMCameraOrthographicSizeShaker::.ctor()
extern void MMCameraOrthographicSizeShaker__ctor_m013A73EB516B47863E11D285E0CE05EC1659F0A4 (void);
// 0x000003AA System.Void MoreMountains.Feedbacks.MMCameraOrthographicSizeShakeEvent::add_OnEvent(MoreMountains.Feedbacks.MMCameraOrthographicSizeShakeEvent/Delegate)
extern void MMCameraOrthographicSizeShakeEvent_add_OnEvent_mC086DEA6D26F92FDA9A0396D1C79C905CCFA980C (void);
// 0x000003AB System.Void MoreMountains.Feedbacks.MMCameraOrthographicSizeShakeEvent::remove_OnEvent(MoreMountains.Feedbacks.MMCameraOrthographicSizeShakeEvent/Delegate)
extern void MMCameraOrthographicSizeShakeEvent_remove_OnEvent_m79CE28116F524268FC26D2B6AF2351ECAFD25BB7 (void);
// 0x000003AC System.Void MoreMountains.Feedbacks.MMCameraOrthographicSizeShakeEvent::Register(MoreMountains.Feedbacks.MMCameraOrthographicSizeShakeEvent/Delegate)
extern void MMCameraOrthographicSizeShakeEvent_Register_m61133F90064EAF651830122BC78D43A6EFA90F89 (void);
// 0x000003AD System.Void MoreMountains.Feedbacks.MMCameraOrthographicSizeShakeEvent::Unregister(MoreMountains.Feedbacks.MMCameraOrthographicSizeShakeEvent/Delegate)
extern void MMCameraOrthographicSizeShakeEvent_Unregister_m4DA6F4FFC40C414AA90819ADF3506C97FDEAC691 (void);
// 0x000003AE System.Void MoreMountains.Feedbacks.MMCameraOrthographicSizeShakeEvent::Trigger(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,System.Boolean)
extern void MMCameraOrthographicSizeShakeEvent_Trigger_m6534FDDDF3E4FEF1AB8F49F1762F04AEB5D3BD39 (void);
// 0x000003AF System.Void MoreMountains.Feedbacks.MMCameraOrthographicSizeShakeEvent/Delegate::.ctor(System.Object,System.IntPtr)
extern void Delegate__ctor_mD22CAD931157C69FAE8456E9D80ACCF5C4154BD6 (void);
// 0x000003B0 System.Void MoreMountains.Feedbacks.MMCameraOrthographicSizeShakeEvent/Delegate::Invoke(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,System.Boolean)
extern void Delegate_Invoke_m345DE6E59E2885DF67B7E0F8AB57678B15D3F9D0 (void);
// 0x000003B1 System.IAsyncResult MoreMountains.Feedbacks.MMCameraOrthographicSizeShakeEvent/Delegate::BeginInvoke(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.AsyncCallback,System.Object)
extern void Delegate_BeginInvoke_mC1A91CFBC74090A64271725D1FA079B808221333 (void);
// 0x000003B2 System.Void MoreMountains.Feedbacks.MMCameraOrthographicSizeShakeEvent/Delegate::EndInvoke(System.IAsyncResult)
extern void Delegate_EndInvoke_m9652A34AA97C7A7AF27DCC74898CC7F253FBA409 (void);
// 0x000003B3 System.Void MoreMountains.Feedbacks.MMCameraShakeProperties::.ctor(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void MMCameraShakeProperties__ctor_mD1E9963F1BFBE26F29CD2DAA9AAC7E69735F4964 (void);
// 0x000003B4 System.Void MoreMountains.Feedbacks.MMCameraZoomEvent::add_OnEvent(MoreMountains.Feedbacks.MMCameraZoomEvent/Delegate)
extern void MMCameraZoomEvent_add_OnEvent_mA8B79E30F6A18EF3945FC599D98988CA9C1C2BD8 (void);
// 0x000003B5 System.Void MoreMountains.Feedbacks.MMCameraZoomEvent::remove_OnEvent(MoreMountains.Feedbacks.MMCameraZoomEvent/Delegate)
extern void MMCameraZoomEvent_remove_OnEvent_m515415366C4BBB5F6C8906E165C4448B9DB1D08A (void);
// 0x000003B6 System.Void MoreMountains.Feedbacks.MMCameraZoomEvent::Register(MoreMountains.Feedbacks.MMCameraZoomEvent/Delegate)
extern void MMCameraZoomEvent_Register_m21AF5CFDCCC96D94362DD58129554484A7D738B4 (void);
// 0x000003B7 System.Void MoreMountains.Feedbacks.MMCameraZoomEvent::Unregister(MoreMountains.Feedbacks.MMCameraZoomEvent/Delegate)
extern void MMCameraZoomEvent_Unregister_mB161F83680ED067D665DA91006EE8A92B3083AB0 (void);
// 0x000003B8 System.Void MoreMountains.Feedbacks.MMCameraZoomEvent::Trigger(MoreMountains.Feedbacks.MMCameraZoomModes,System.Single,System.Single,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean)
extern void MMCameraZoomEvent_Trigger_m38B1F6F3137EAA8D1F06F7EE347CDDE7DB245B9D (void);
// 0x000003B9 System.Void MoreMountains.Feedbacks.MMCameraZoomEvent/Delegate::.ctor(System.Object,System.IntPtr)
extern void Delegate__ctor_m7D7D03319B4EF6154F102E5EDFF26711FB51C4AD (void);
// 0x000003BA System.Void MoreMountains.Feedbacks.MMCameraZoomEvent/Delegate::Invoke(MoreMountains.Feedbacks.MMCameraZoomModes,System.Single,System.Single,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean)
extern void Delegate_Invoke_mEF3B60660634573BB90A93133255CD6079186543 (void);
// 0x000003BB System.IAsyncResult MoreMountains.Feedbacks.MMCameraZoomEvent/Delegate::BeginInvoke(MoreMountains.Feedbacks.MMCameraZoomModes,System.Single,System.Single,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,System.AsyncCallback,System.Object)
extern void Delegate_BeginInvoke_m930360FF8349181FA43D08AC11FFE97B9329D8F7 (void);
// 0x000003BC System.Void MoreMountains.Feedbacks.MMCameraZoomEvent/Delegate::EndInvoke(System.IAsyncResult)
extern void Delegate_EndInvoke_m6982BD075BC46F5FE32574A1354685CA67B29633 (void);
// 0x000003BD System.Void MoreMountains.Feedbacks.MMCameraShakeEvent::add_OnEvent(MoreMountains.Feedbacks.MMCameraShakeEvent/Delegate)
extern void MMCameraShakeEvent_add_OnEvent_mD692C7BE9445BCC7A8B86CC85D196ECEE73E9E2D (void);
// 0x000003BE System.Void MoreMountains.Feedbacks.MMCameraShakeEvent::remove_OnEvent(MoreMountains.Feedbacks.MMCameraShakeEvent/Delegate)
extern void MMCameraShakeEvent_remove_OnEvent_mA0D3B24FCD8E23713193FC95B31E5A19D433B20A (void);
// 0x000003BF System.Void MoreMountains.Feedbacks.MMCameraShakeEvent::Register(MoreMountains.Feedbacks.MMCameraShakeEvent/Delegate)
extern void MMCameraShakeEvent_Register_m724CD0839144D1B710757E1C6AB22E941216EDCB (void);
// 0x000003C0 System.Void MoreMountains.Feedbacks.MMCameraShakeEvent::Unregister(MoreMountains.Feedbacks.MMCameraShakeEvent/Delegate)
extern void MMCameraShakeEvent_Unregister_mC1573AD7E2DA5F0AB8299CEBDD7A2F6C4D0F5E06 (void);
// 0x000003C1 System.Void MoreMountains.Feedbacks.MMCameraShakeEvent::Trigger(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Boolean,System.Int32,System.Boolean)
extern void MMCameraShakeEvent_Trigger_mCF5AAE461A7F52ADF0F8464349413173E46A8EE2 (void);
// 0x000003C2 System.Void MoreMountains.Feedbacks.MMCameraShakeEvent/Delegate::.ctor(System.Object,System.IntPtr)
extern void Delegate__ctor_m92750E4DE57041804753CA5809C649DBB44844BB (void);
// 0x000003C3 System.Void MoreMountains.Feedbacks.MMCameraShakeEvent/Delegate::Invoke(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Boolean,System.Int32,System.Boolean)
extern void Delegate_Invoke_mE6B52BF4D4B34C06A9BC9AF8FBEEF5DA883FB24E (void);
// 0x000003C4 System.IAsyncResult MoreMountains.Feedbacks.MMCameraShakeEvent/Delegate::BeginInvoke(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Boolean,System.Int32,System.Boolean,System.AsyncCallback,System.Object)
extern void Delegate_BeginInvoke_m5F44DE5F710D9BEA338751BC45BA71E954229E06 (void);
// 0x000003C5 System.Void MoreMountains.Feedbacks.MMCameraShakeEvent/Delegate::EndInvoke(System.IAsyncResult)
extern void Delegate_EndInvoke_m891123BA2AB559E669A7ABC025C30E434127AD4B (void);
// 0x000003C6 System.Void MoreMountains.Feedbacks.MMCameraShakeStopEvent::add_OnEvent(MoreMountains.Feedbacks.MMCameraShakeStopEvent/Delegate)
extern void MMCameraShakeStopEvent_add_OnEvent_mBB56D6E362F9B97F3038AD137319A1E3703BBA83 (void);
// 0x000003C7 System.Void MoreMountains.Feedbacks.MMCameraShakeStopEvent::remove_OnEvent(MoreMountains.Feedbacks.MMCameraShakeStopEvent/Delegate)
extern void MMCameraShakeStopEvent_remove_OnEvent_m293A897863522D250E2328645F8FDEA15F377AC3 (void);
// 0x000003C8 System.Void MoreMountains.Feedbacks.MMCameraShakeStopEvent::Register(MoreMountains.Feedbacks.MMCameraShakeStopEvent/Delegate)
extern void MMCameraShakeStopEvent_Register_m7116A4D9BC6DDEF692A978037A91C197C3D8D20D (void);
// 0x000003C9 System.Void MoreMountains.Feedbacks.MMCameraShakeStopEvent::Unregister(MoreMountains.Feedbacks.MMCameraShakeStopEvent/Delegate)
extern void MMCameraShakeStopEvent_Unregister_m4BDB20A81C8C1FE34062253C9D587C67901E38E6 (void);
// 0x000003CA System.Void MoreMountains.Feedbacks.MMCameraShakeStopEvent::Trigger(System.Int32)
extern void MMCameraShakeStopEvent_Trigger_mE1FD47D0BB25F16D0F405B9FF7A8FA6BB7EC7C24 (void);
// 0x000003CB System.Void MoreMountains.Feedbacks.MMCameraShakeStopEvent/Delegate::.ctor(System.Object,System.IntPtr)
extern void Delegate__ctor_m828FB777E333546230DB23E0DCAA1C081D4D1D08 (void);
// 0x000003CC System.Void MoreMountains.Feedbacks.MMCameraShakeStopEvent/Delegate::Invoke(System.Int32)
extern void Delegate_Invoke_m55496058FB43150859E3D8BD22F6AE58596059ED (void);
// 0x000003CD System.IAsyncResult MoreMountains.Feedbacks.MMCameraShakeStopEvent/Delegate::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern void Delegate_BeginInvoke_m017513A438267F0419C7AB271D40386A24A6F2E4 (void);
// 0x000003CE System.Void MoreMountains.Feedbacks.MMCameraShakeStopEvent/Delegate::EndInvoke(System.IAsyncResult)
extern void Delegate_EndInvoke_m50C5D17B821534FE67DA9608EB25862BB3AC0445 (void);
// 0x000003CF System.Void MoreMountains.Feedbacks.MMCameraShaker::Awake()
extern void MMCameraShaker_Awake_mEE2DDC76431A9277B93F648FDFEDFCAC6F59756F (void);
// 0x000003D0 System.Void MoreMountains.Feedbacks.MMCameraShaker::ShakeCamera(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Boolean)
extern void MMCameraShaker_ShakeCamera_mA3DEC28120EB17F24E3F5F409668E651D54EC7F0 (void);
// 0x000003D1 System.Void MoreMountains.Feedbacks.MMCameraShaker::OnCameraShakeEvent(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Boolean,System.Int32,System.Boolean)
extern void MMCameraShaker_OnCameraShakeEvent_m7A20C6CBF1B4781983ABF2CA154C29F8806DA603 (void);
// 0x000003D2 System.Void MoreMountains.Feedbacks.MMCameraShaker::OnEnable()
extern void MMCameraShaker_OnEnable_m994E02B97A9021BD5FB7AC4C445436371E4CC4A4 (void);
// 0x000003D3 System.Void MoreMountains.Feedbacks.MMCameraShaker::OnDisable()
extern void MMCameraShaker_OnDisable_m75FB345A12B7E02921FCB9B81810C9E0BD4A5DD9 (void);
// 0x000003D4 System.Void MoreMountains.Feedbacks.MMCameraShaker::.ctor()
extern void MMCameraShaker__ctor_m7FB89B9F689289D7F26EC5E170C0B1E55BE5AE39 (void);
// 0x000003D5 System.Void MoreMountains.Feedbacks.MMFeedbacksShaker::Initialization()
extern void MMFeedbacksShaker_Initialization_m56881E2D1FCF7D9C2748B5F63CD29F64E6BC2BA5 (void);
// 0x000003D6 System.Void MoreMountains.Feedbacks.MMFeedbacksShaker::OnMMFeedbacksShakeEvent(System.Int32,System.Boolean,System.Single,UnityEngine.Vector3)
extern void MMFeedbacksShaker_OnMMFeedbacksShakeEvent_m4956F7C91408669C0BAACC725F82015BF14FCB30 (void);
// 0x000003D7 System.Void MoreMountains.Feedbacks.MMFeedbacksShaker::ShakeStarts()
extern void MMFeedbacksShaker_ShakeStarts_mD733248D602FDC46300C4A4D5A0ABC102BF4959B (void);
// 0x000003D8 System.Void MoreMountains.Feedbacks.MMFeedbacksShaker::Reset()
extern void MMFeedbacksShaker_Reset_m8C4CF6EE0434C78F1BA2C6F7E6C4DE8895671246 (void);
// 0x000003D9 System.Void MoreMountains.Feedbacks.MMFeedbacksShaker::StartListening()
extern void MMFeedbacksShaker_StartListening_m47107E346CC0EB4113E86E5FABB83CF50C585A12 (void);
// 0x000003DA System.Void MoreMountains.Feedbacks.MMFeedbacksShaker::StopListening()
extern void MMFeedbacksShaker_StopListening_m4CF030D1D0FFD15392880D921A28246934162E95 (void);
// 0x000003DB System.Void MoreMountains.Feedbacks.MMFeedbacksShaker::.ctor()
extern void MMFeedbacksShaker__ctor_mDE795DC8DF4DD8C767D2C4BCBDDD54C563F2C146 (void);
// 0x000003DC System.Void MoreMountains.Feedbacks.MMFeedbacksShakeEvent::add_OnEvent(MoreMountains.Feedbacks.MMFeedbacksShakeEvent/Delegate)
extern void MMFeedbacksShakeEvent_add_OnEvent_mA8223F79AB281471A5C336A825267797D1163DE3 (void);
// 0x000003DD System.Void MoreMountains.Feedbacks.MMFeedbacksShakeEvent::remove_OnEvent(MoreMountains.Feedbacks.MMFeedbacksShakeEvent/Delegate)
extern void MMFeedbacksShakeEvent_remove_OnEvent_m3B1BED5AF35BF0A891BDEDFFBE47A64E27233756 (void);
// 0x000003DE System.Void MoreMountains.Feedbacks.MMFeedbacksShakeEvent::Register(MoreMountains.Feedbacks.MMFeedbacksShakeEvent/Delegate)
extern void MMFeedbacksShakeEvent_Register_m0356729ACFC7462F575DAE08B0F3A6510338BB10 (void);
// 0x000003DF System.Void MoreMountains.Feedbacks.MMFeedbacksShakeEvent::Unregister(MoreMountains.Feedbacks.MMFeedbacksShakeEvent/Delegate)
extern void MMFeedbacksShakeEvent_Unregister_m0913AB07129EE6C39FB395DF6A74F1590EFEFFB9 (void);
// 0x000003E0 System.Void MoreMountains.Feedbacks.MMFeedbacksShakeEvent::Trigger(System.Int32,System.Boolean,System.Single,UnityEngine.Vector3)
extern void MMFeedbacksShakeEvent_Trigger_m23F6F25A8B8567C2AE9F6249D1B8E36415E457BE (void);
// 0x000003E1 System.Void MoreMountains.Feedbacks.MMFeedbacksShakeEvent/Delegate::.ctor(System.Object,System.IntPtr)
extern void Delegate__ctor_m532E480D93CED4886EE6F6D0264111AF4B39E54F (void);
// 0x000003E2 System.Void MoreMountains.Feedbacks.MMFeedbacksShakeEvent/Delegate::Invoke(System.Int32,System.Boolean,System.Single,UnityEngine.Vector3)
extern void Delegate_Invoke_m59B0259F4822D050B4FBA3DFECCE59D8D4B41ADB (void);
// 0x000003E3 System.IAsyncResult MoreMountains.Feedbacks.MMFeedbacksShakeEvent/Delegate::BeginInvoke(System.Int32,System.Boolean,System.Single,UnityEngine.Vector3,System.AsyncCallback,System.Object)
extern void Delegate_BeginInvoke_mE2FC6B2C3B1718A8212C3630CAE605DC520B7423 (void);
// 0x000003E4 System.Void MoreMountains.Feedbacks.MMFeedbacksShakeEvent/Delegate::EndInvoke(System.IAsyncResult)
extern void Delegate_EndInvoke_m8CC2852E819BD09D7F01D9C8150C28DEFE17B8B3 (void);
// 0x000003E5 System.Void MoreMountains.Feedbacks.MMFlashEvent::add_OnEvent(MoreMountains.Feedbacks.MMFlashEvent/Delegate)
extern void MMFlashEvent_add_OnEvent_m5DD1CF1445D375EE9B287C822B88610D33B42C09 (void);
// 0x000003E6 System.Void MoreMountains.Feedbacks.MMFlashEvent::remove_OnEvent(MoreMountains.Feedbacks.MMFlashEvent/Delegate)
extern void MMFlashEvent_remove_OnEvent_mCC5B7BD2EFC7F08A5C127A519213FEE8411A8AF3 (void);
// 0x000003E7 System.Void MoreMountains.Feedbacks.MMFlashEvent::Register(MoreMountains.Feedbacks.MMFlashEvent/Delegate)
extern void MMFlashEvent_Register_m7961AC06CF4F58CAAE9EDD72F3665AA428E54565 (void);
// 0x000003E8 System.Void MoreMountains.Feedbacks.MMFlashEvent::Unregister(MoreMountains.Feedbacks.MMFlashEvent/Delegate)
extern void MMFlashEvent_Unregister_mF3DC65819962B8B3683E7EB83557CD68A0DE8659 (void);
// 0x000003E9 System.Void MoreMountains.Feedbacks.MMFlashEvent::Trigger(UnityEngine.Color,System.Single,System.Single,System.Int32,System.Int32,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void MMFlashEvent_Trigger_mD287B4021136FDC7CBB4765C1EA3FA6006B65090 (void);
// 0x000003EA System.Void MoreMountains.Feedbacks.MMFlashEvent/Delegate::.ctor(System.Object,System.IntPtr)
extern void Delegate__ctor_m64099DCD1DBCFE9A8B2068FC6F983A48D37372C3 (void);
// 0x000003EB System.Void MoreMountains.Feedbacks.MMFlashEvent/Delegate::Invoke(UnityEngine.Color,System.Single,System.Single,System.Int32,System.Int32,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void Delegate_Invoke_m9D0F9D5610ED5872D30A28F18B9DA37966B4B415 (void);
// 0x000003EC System.IAsyncResult MoreMountains.Feedbacks.MMFlashEvent/Delegate::BeginInvoke(UnityEngine.Color,System.Single,System.Single,System.Int32,System.Int32,MoreMountains.Feedbacks.TimescaleModes,System.Boolean,System.AsyncCallback,System.Object)
extern void Delegate_BeginInvoke_mC55C72A18946D786686C688882D7F49362B313E1 (void);
// 0x000003ED System.Void MoreMountains.Feedbacks.MMFlashEvent/Delegate::EndInvoke(System.IAsyncResult)
extern void Delegate_EndInvoke_m85E48C8B671FC95A7BCD7C6D7EC2C739BF85D3FF (void);
// 0x000003EE System.Void MoreMountains.Feedbacks.MMFlashDebugSettings::.ctor()
extern void MMFlashDebugSettings__ctor_m0E1C719F38CD9399DBCA901ECC528E418034BE7C (void);
// 0x000003EF System.Single MoreMountains.Feedbacks.MMFlash::GetTime()
extern void MMFlash_GetTime_mEAF3D2A8B416E3518B4067EC9CD7B8BA7F1C0E9F (void);
// 0x000003F0 System.Single MoreMountains.Feedbacks.MMFlash::GetDeltaTime()
extern void MMFlash_GetDeltaTime_m1FB173336C839B315A7CDA5B8954F76B15519185 (void);
// 0x000003F1 System.Void MoreMountains.Feedbacks.MMFlash::Start()
extern void MMFlash_Start_mE567722D1FBC1AB6340982F7339591944064EC40 (void);
// 0x000003F2 System.Void MoreMountains.Feedbacks.MMFlash::Update()
extern void MMFlash_Update_mF17F454FF5DE171CF81C1CFB56CC748438040735 (void);
// 0x000003F3 System.Void MoreMountains.Feedbacks.MMFlash::DebugTest()
extern void MMFlash_DebugTest_mFDA5D4B3CEFE23BDD9DA0AE23517EA7C363F21DF (void);
// 0x000003F4 System.Void MoreMountains.Feedbacks.MMFlash::OnMMFlashEvent(UnityEngine.Color,System.Single,System.Single,System.Int32,System.Int32,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void MMFlash_OnMMFlashEvent_m3BD75D26F094977B17EE5A4A8D36E657BF3E0418 (void);
// 0x000003F5 System.Void MoreMountains.Feedbacks.MMFlash::OnEnable()
extern void MMFlash_OnEnable_m4834D455A97460B110F34B2A6AC7022C0E614FD6 (void);
// 0x000003F6 System.Void MoreMountains.Feedbacks.MMFlash::OnDisable()
extern void MMFlash_OnDisable_m598E89140DC89EEDEDCE405BD4615BF536F332A4 (void);
// 0x000003F7 System.Void MoreMountains.Feedbacks.MMFlash::.ctor()
extern void MMFlash__ctor_m8299CBC97BE90C9957607823F4FAEF7D93E1EC72 (void);
// 0x000003F8 System.Void MoreMountains.Feedbacks.MMLightShaker::Initialization()
extern void MMLightShaker_Initialization_mAC28AC3B6184D2D6AAAA98460EF99041EE862C89 (void);
// 0x000003F9 System.Void MoreMountains.Feedbacks.MMLightShaker::Reset()
extern void MMLightShaker_Reset_m0059B5FCDCC375E64924E61C71EAB0E1D07EA079 (void);
// 0x000003FA System.Void MoreMountains.Feedbacks.MMLightShaker::Shake()
extern void MMLightShaker_Shake_m4C184F73BBCC6DFA29FB9F90B5BE97DA9B8FCA79 (void);
// 0x000003FB System.Void MoreMountains.Feedbacks.MMLightShaker::GrabInitialValues()
extern void MMLightShaker_GrabInitialValues_mA980E78C180868A2CF5E7C91A02B505C20C1D4C2 (void);
// 0x000003FC System.Void MoreMountains.Feedbacks.MMLightShaker::ResetTargetValues()
extern void MMLightShaker_ResetTargetValues_mE6372E4414B21250752B4694B4A2890B8B481CCD (void);
// 0x000003FD System.Void MoreMountains.Feedbacks.MMLightShaker::ResetShakerValues()
extern void MMLightShaker_ResetShakerValues_m76584D04881DABE2F7F4E7F39B0E9FF9B45A5E9B (void);
// 0x000003FE System.Void MoreMountains.Feedbacks.MMLightShaker::StartListening()
extern void MMLightShaker_StartListening_m4F2C2C07D695350DEBADC4CFBD1164C7D0D20F21 (void);
// 0x000003FF System.Void MoreMountains.Feedbacks.MMLightShaker::StopListening()
extern void MMLightShaker_StopListening_mDC65292A6003B622421794C0BE59208F3BFE7DE5 (void);
// 0x00000400 System.Void MoreMountains.Feedbacks.MMLightShaker::OnMMLightShakeEvent(System.Single,System.Boolean,System.Boolean,UnityEngine.Gradient,UnityEngine.AnimationCurve,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,System.Single,UnityEngine.Vector3)
extern void MMLightShaker_OnMMLightShakeEvent_m0AEA3FE00BDBD85060ACB70BB7A842CA38DE4921 (void);
// 0x00000401 System.Void MoreMountains.Feedbacks.MMLightShaker::.ctor()
extern void MMLightShaker__ctor_m38230B25FB49E29D7E207FB19E43458A0B8F4A92 (void);
// 0x00000402 System.Void MoreMountains.Feedbacks.MMLightShakeEvent::add_OnEvent(MoreMountains.Feedbacks.MMLightShakeEvent/Delegate)
extern void MMLightShakeEvent_add_OnEvent_m3D3F66CBE5EFE6767D5FAF4FF01BA57A73BBF7F1 (void);
// 0x00000403 System.Void MoreMountains.Feedbacks.MMLightShakeEvent::remove_OnEvent(MoreMountains.Feedbacks.MMLightShakeEvent/Delegate)
extern void MMLightShakeEvent_remove_OnEvent_m307FA66E4B929EBD934B2CA3B7B2B2FB209168D3 (void);
// 0x00000404 System.Void MoreMountains.Feedbacks.MMLightShakeEvent::Register(MoreMountains.Feedbacks.MMLightShakeEvent/Delegate)
extern void MMLightShakeEvent_Register_m4320BB3473954A1BD254C983AF99C61BB77159CD (void);
// 0x00000405 System.Void MoreMountains.Feedbacks.MMLightShakeEvent::Unregister(MoreMountains.Feedbacks.MMLightShakeEvent/Delegate)
extern void MMLightShakeEvent_Unregister_m85A35DBEE0CE332ED111B78E7171CDB201D14FE6 (void);
// 0x00000406 System.Void MoreMountains.Feedbacks.MMLightShakeEvent::Trigger(System.Single,System.Boolean,System.Boolean,UnityEngine.Gradient,UnityEngine.AnimationCurve,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,System.Single,UnityEngine.Vector3)
extern void MMLightShakeEvent_Trigger_m6A6AE7AD3EC868A764C4136C78D021FFEC93F388 (void);
// 0x00000407 System.Void MoreMountains.Feedbacks.MMLightShakeEvent/Delegate::.ctor(System.Object,System.IntPtr)
extern void Delegate__ctor_m476DEF09C16C46CF8F44DC9FCFD622D46B984754 (void);
// 0x00000408 System.Void MoreMountains.Feedbacks.MMLightShakeEvent/Delegate::Invoke(System.Single,System.Boolean,System.Boolean,UnityEngine.Gradient,UnityEngine.AnimationCurve,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,System.Single,UnityEngine.Vector3)
extern void Delegate_Invoke_m9A545DE390E3E47B6B0A27AB3217EF2B0E517D39 (void);
// 0x00000409 System.IAsyncResult MoreMountains.Feedbacks.MMLightShakeEvent/Delegate::BeginInvoke(System.Single,System.Boolean,System.Boolean,UnityEngine.Gradient,UnityEngine.AnimationCurve,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,System.Single,UnityEngine.Vector3,System.AsyncCallback,System.Object)
extern void Delegate_BeginInvoke_mC9BE683FE7204620E4284503411EBCFAC0B2DA23 (void);
// 0x0000040A System.Void MoreMountains.Feedbacks.MMLightShakeEvent/Delegate::EndInvoke(System.IAsyncResult)
extern void Delegate_EndInvoke_m4E9B4742BCB90CA9CC171D4C7CFB39F88AD0B52B (void);
// 0x0000040B System.Void MoreMountains.Feedbacks.MMSpriteRendererShaker::Initialization()
extern void MMSpriteRendererShaker_Initialization_m001014F67B58DB9DF3D631B13B7E0ADB771F64C2 (void);
// 0x0000040C System.Void MoreMountains.Feedbacks.MMSpriteRendererShaker::Reset()
extern void MMSpriteRendererShaker_Reset_m90E31160325C8026801CB4D131E7E561C9F239E0 (void);
// 0x0000040D System.Void MoreMountains.Feedbacks.MMSpriteRendererShaker::Shake()
extern void MMSpriteRendererShaker_Shake_mF874FDE36715E19C5A928B7BBA7E3BF98401351F (void);
// 0x0000040E System.Void MoreMountains.Feedbacks.MMSpriteRendererShaker::GrabInitialValues()
extern void MMSpriteRendererShaker_GrabInitialValues_m514944FD47193022C25F4AEDF6C7B45650D5EF09 (void);
// 0x0000040F System.Void MoreMountains.Feedbacks.MMSpriteRendererShaker::ResetTargetValues()
extern void MMSpriteRendererShaker_ResetTargetValues_mE4FFB5D7167123FF45B44BC06E1EA16DD7CE71E4 (void);
// 0x00000410 System.Void MoreMountains.Feedbacks.MMSpriteRendererShaker::ResetShakerValues()
extern void MMSpriteRendererShaker_ResetShakerValues_mC37B517A13A2B06ABD42AB7B242A1A65BEC52A9E (void);
// 0x00000411 System.Void MoreMountains.Feedbacks.MMSpriteRendererShaker::StartListening()
extern void MMSpriteRendererShaker_StartListening_m2CCB4855C40E243027D3AD0713FAD73DF9D07BB3 (void);
// 0x00000412 System.Void MoreMountains.Feedbacks.MMSpriteRendererShaker::StopListening()
extern void MMSpriteRendererShaker_StopListening_m341C136DCB92D1E20005873AB8983ADF8A79B985 (void);
// 0x00000413 System.Void MoreMountains.Feedbacks.MMSpriteRendererShaker::OnMMSpriteRendererShakeEvent(System.Single,System.Boolean,UnityEngine.Gradient,System.Boolean,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,System.Single,UnityEngine.Vector3)
extern void MMSpriteRendererShaker_OnMMSpriteRendererShakeEvent_m72FCBD3C05F1B9930F38F6B21915C24ACC465A0F (void);
// 0x00000414 System.Void MoreMountains.Feedbacks.MMSpriteRendererShaker::.ctor()
extern void MMSpriteRendererShaker__ctor_mFA9AF5112DB3CFB8C162E26591429FC66F797E5D (void);
// 0x00000415 System.Void MoreMountains.Feedbacks.MMSpriteRendererShakeEvent::add_OnEvent(MoreMountains.Feedbacks.MMSpriteRendererShakeEvent/Delegate)
extern void MMSpriteRendererShakeEvent_add_OnEvent_m0D612C17ABBAA342DB82594398889DA0449C77F3 (void);
// 0x00000416 System.Void MoreMountains.Feedbacks.MMSpriteRendererShakeEvent::remove_OnEvent(MoreMountains.Feedbacks.MMSpriteRendererShakeEvent/Delegate)
extern void MMSpriteRendererShakeEvent_remove_OnEvent_mD539FB4208AE396AEAEDEB68536183E79A1F399A (void);
// 0x00000417 System.Void MoreMountains.Feedbacks.MMSpriteRendererShakeEvent::Register(MoreMountains.Feedbacks.MMSpriteRendererShakeEvent/Delegate)
extern void MMSpriteRendererShakeEvent_Register_mCDC4817EAD982E0E1D2576741AD78FA26D36DAA7 (void);
// 0x00000418 System.Void MoreMountains.Feedbacks.MMSpriteRendererShakeEvent::Unregister(MoreMountains.Feedbacks.MMSpriteRendererShakeEvent/Delegate)
extern void MMSpriteRendererShakeEvent_Unregister_m39CB15CD66B854F722F52826747256F27E1EC707 (void);
// 0x00000419 System.Void MoreMountains.Feedbacks.MMSpriteRendererShakeEvent::Trigger(System.Single,System.Boolean,UnityEngine.Gradient,System.Boolean,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,System.Single,UnityEngine.Vector3)
extern void MMSpriteRendererShakeEvent_Trigger_mFDD7EDB65DD1B0019F8AA46B5A2766A97AA9E18A (void);
// 0x0000041A System.Void MoreMountains.Feedbacks.MMSpriteRendererShakeEvent/Delegate::.ctor(System.Object,System.IntPtr)
extern void Delegate__ctor_m642066DD8BD9826EC30BB6C3115731C9AFFF8442 (void);
// 0x0000041B System.Void MoreMountains.Feedbacks.MMSpriteRendererShakeEvent/Delegate::Invoke(System.Single,System.Boolean,UnityEngine.Gradient,System.Boolean,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,System.Single,UnityEngine.Vector3)
extern void Delegate_Invoke_m7739E286A564A0D5C34880A61E308A730F0A4947 (void);
// 0x0000041C System.IAsyncResult MoreMountains.Feedbacks.MMSpriteRendererShakeEvent/Delegate::BeginInvoke(System.Single,System.Boolean,UnityEngine.Gradient,System.Boolean,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,System.Single,UnityEngine.Vector3,System.AsyncCallback,System.Object)
extern void Delegate_BeginInvoke_mBFDF29AB2DDEC5408CC8E4D261E55EC8CA3E203C (void);
// 0x0000041D System.Void MoreMountains.Feedbacks.MMSpriteRendererShakeEvent/Delegate::EndInvoke(System.IAsyncResult)
extern void Delegate_EndInvoke_m931078FCAD178A8EDF9D472AB62681711A6C68C6 (void);
// 0x0000041E System.Void MoreMountains.Feedbacks.MMTimeScaleEvent::add_OnEvent(MoreMountains.Feedbacks.MMTimeScaleEvent/Delegate)
extern void MMTimeScaleEvent_add_OnEvent_m4962833A74F45A7629A9A6BEFF3B591526D5EC77 (void);
// 0x0000041F System.Void MoreMountains.Feedbacks.MMTimeScaleEvent::remove_OnEvent(MoreMountains.Feedbacks.MMTimeScaleEvent/Delegate)
extern void MMTimeScaleEvent_remove_OnEvent_mCF0847F3C17A1BBDB13460FC6DF4704DD5C48D39 (void);
// 0x00000420 System.Void MoreMountains.Feedbacks.MMTimeScaleEvent::Register(MoreMountains.Feedbacks.MMTimeScaleEvent/Delegate)
extern void MMTimeScaleEvent_Register_m28C4935DCB103FBD77D571844490690DA9D6223C (void);
// 0x00000421 System.Void MoreMountains.Feedbacks.MMTimeScaleEvent::Unregister(MoreMountains.Feedbacks.MMTimeScaleEvent/Delegate)
extern void MMTimeScaleEvent_Unregister_m8CAE055701D97B813D16B75309A62ED5E9FA0B88 (void);
// 0x00000422 System.Void MoreMountains.Feedbacks.MMTimeScaleEvent::Trigger(MoreMountains.Feedbacks.MMTimeScaleMethods,System.Single,System.Single,System.Boolean,System.Single,System.Boolean)
extern void MMTimeScaleEvent_Trigger_mF54D807E19DF44F9EB1E07B9A76C8E294632741E (void);
// 0x00000423 System.Void MoreMountains.Feedbacks.MMTimeScaleEvent/Delegate::.ctor(System.Object,System.IntPtr)
extern void Delegate__ctor_m8671B4AA31BFF8224D2C31A6E67A2C3A7ECA8A58 (void);
// 0x00000424 System.Void MoreMountains.Feedbacks.MMTimeScaleEvent/Delegate::Invoke(MoreMountains.Feedbacks.MMTimeScaleMethods,System.Single,System.Single,System.Boolean,System.Single,System.Boolean)
extern void Delegate_Invoke_m128D37A521F226D01591B936AA01A0A01C05196C (void);
// 0x00000425 System.IAsyncResult MoreMountains.Feedbacks.MMTimeScaleEvent/Delegate::BeginInvoke(MoreMountains.Feedbacks.MMTimeScaleMethods,System.Single,System.Single,System.Boolean,System.Single,System.Boolean,System.AsyncCallback,System.Object)
extern void Delegate_BeginInvoke_m9BB32F6C8B0A6EB998973CEE0579CAF81CCFE1CF (void);
// 0x00000426 System.Void MoreMountains.Feedbacks.MMTimeScaleEvent/Delegate::EndInvoke(System.IAsyncResult)
extern void Delegate_EndInvoke_mBDDF17A5A56D20C8592AC2808131A9258513E0DF (void);
// 0x00000427 System.Void MoreMountains.Feedbacks.MMFreezeFrameEvent::add_OnEvent(MoreMountains.Feedbacks.MMFreezeFrameEvent/Delegate)
extern void MMFreezeFrameEvent_add_OnEvent_m68B87281495E4229DEF2E8F7BDEEEE1627150B01 (void);
// 0x00000428 System.Void MoreMountains.Feedbacks.MMFreezeFrameEvent::remove_OnEvent(MoreMountains.Feedbacks.MMFreezeFrameEvent/Delegate)
extern void MMFreezeFrameEvent_remove_OnEvent_m843ECC5D7026D1C07A23CF3838F814CC3A068D37 (void);
// 0x00000429 System.Void MoreMountains.Feedbacks.MMFreezeFrameEvent::Register(MoreMountains.Feedbacks.MMFreezeFrameEvent/Delegate)
extern void MMFreezeFrameEvent_Register_mF1D85B4305902F2B1DEC710EC3F9A7D3CB813206 (void);
// 0x0000042A System.Void MoreMountains.Feedbacks.MMFreezeFrameEvent::Unregister(MoreMountains.Feedbacks.MMFreezeFrameEvent/Delegate)
extern void MMFreezeFrameEvent_Unregister_mDEAA2A80941E75D53EBDC015780857686113E57B (void);
// 0x0000042B System.Void MoreMountains.Feedbacks.MMFreezeFrameEvent::Trigger(System.Single)
extern void MMFreezeFrameEvent_Trigger_m218A2295294F0D4022D2FF518160A8DF0F0FF8B2 (void);
// 0x0000042C System.Void MoreMountains.Feedbacks.MMFreezeFrameEvent/Delegate::.ctor(System.Object,System.IntPtr)
extern void Delegate__ctor_m36455E5AEABC822DF46AAD4E155CF6E81D52DC3B (void);
// 0x0000042D System.Void MoreMountains.Feedbacks.MMFreezeFrameEvent/Delegate::Invoke(System.Single)
extern void Delegate_Invoke_m295A04EC8B09AB77DAB8F7F25C91E3DCD027862E (void);
// 0x0000042E System.IAsyncResult MoreMountains.Feedbacks.MMFreezeFrameEvent/Delegate::BeginInvoke(System.Single,System.AsyncCallback,System.Object)
extern void Delegate_BeginInvoke_m32C83874A7BA1A0E954C85CBC6F2F8C7F14FC307 (void);
// 0x0000042F System.Void MoreMountains.Feedbacks.MMFreezeFrameEvent/Delegate::EndInvoke(System.IAsyncResult)
extern void Delegate_EndInvoke_mA1A3CE1828417E0C4E9DF16BD6AF5968B26DC702 (void);
// 0x00000430 System.Void MoreMountains.Feedbacks.MMTimeManager::TestButtonToSlowDownTime()
extern void MMTimeManager_TestButtonToSlowDownTime_m5989141E8375BACF15704704A9DBE072FC58C04C (void);
// 0x00000431 System.Void MoreMountains.Feedbacks.MMTimeManager::Start()
extern void MMTimeManager_Start_mC7D04335ADB46CEEB7A3D8B575B7823E38513DD5 (void);
// 0x00000432 System.Void MoreMountains.Feedbacks.MMTimeManager::Initialization()
extern void MMTimeManager_Initialization_mC666136E4907D64093684BF83FF24D90B2531330 (void);
// 0x00000433 System.Void MoreMountains.Feedbacks.MMTimeManager::Update()
extern void MMTimeManager_Update_m7DC6C9C191B490D06623F9C4026837E1CAE8969F (void);
// 0x00000434 System.Void MoreMountains.Feedbacks.MMTimeManager::ApplyTimeScale(System.Single)
extern void MMTimeManager_ApplyTimeScale_m7313E9D9D5B3779BC7D9DF387659597AD2641A41 (void);
// 0x00000435 System.Void MoreMountains.Feedbacks.MMTimeManager::SetTimeScale(System.Single)
extern void MMTimeManager_SetTimeScale_mABA5058DC06E363CF2BEAEC88A500F3324549E91 (void);
// 0x00000436 System.Void MoreMountains.Feedbacks.MMTimeManager::SetTimeScale(MoreMountains.Feedbacks.TimeScaleProperties)
extern void MMTimeManager_SetTimeScale_mDA2C6513CEA87286D39220E8A3107AB6A4157BF2 (void);
// 0x00000437 System.Void MoreMountains.Feedbacks.MMTimeManager::ResetTimeScale()
extern void MMTimeManager_ResetTimeScale_m09D2F185FAAF975533132EC60F505AF68AD6DE20 (void);
// 0x00000438 System.Void MoreMountains.Feedbacks.MMTimeManager::Unfreeze()
extern void MMTimeManager_Unfreeze_mCD81C50798C98C28FC5DC819ACC529EF76641DDB (void);
// 0x00000439 System.Void MoreMountains.Feedbacks.MMTimeManager::SetTimescaleTo(System.Single)
extern void MMTimeManager_SetTimescaleTo_m4D737EC2F69F088DBA292CD289A6EF75B0926420 (void);
// 0x0000043A System.Void MoreMountains.Feedbacks.MMTimeManager::OnTimeScaleEvent(MoreMountains.Feedbacks.MMTimeScaleMethods,System.Single,System.Single,System.Boolean,System.Single,System.Boolean)
extern void MMTimeManager_OnTimeScaleEvent_m6D0BCA9C915BDEDB97EFF7A33223FDAE6655C29F (void);
// 0x0000043B System.Void MoreMountains.Feedbacks.MMTimeManager::OnMMFreezeFrameEvent(System.Single)
extern void MMTimeManager_OnMMFreezeFrameEvent_mB8F8DFE987ED86A92C40BEA6C758D3EF533D3971 (void);
// 0x0000043C System.Void MoreMountains.Feedbacks.MMTimeManager::OnEnable()
extern void MMTimeManager_OnEnable_m4AD55FBF40C91C82B57FDB3DD921125158E1860D (void);
// 0x0000043D System.Void MoreMountains.Feedbacks.MMTimeManager::OnDisable()
extern void MMTimeManager_OnDisable_m15527DAA44A4724435A4E26EF4585CE1A11E3DA5 (void);
// 0x0000043E System.Void MoreMountains.Feedbacks.MMTimeManager::.ctor()
extern void MMTimeManager__ctor_m848771FD04C1365A0697CD68B9CDE5B50CC053F2 (void);
// 0x0000043F System.Single MoreMountains.Feedbacks.WiggleProperties::GetDeltaTime()
extern void WiggleProperties_GetDeltaTime_m59B1FC49566F91EE07971CAC8D4E2EC78881BDF1 (void);
// 0x00000440 System.Single MoreMountains.Feedbacks.WiggleProperties::GetTime()
extern void WiggleProperties_GetTime_mC8DF410F9D5690221B2F8FDAC8334EFDBA5420D4 (void);
// 0x00000441 System.Void MoreMountains.Feedbacks.WiggleProperties::.ctor()
extern void WiggleProperties__ctor_m97AE90EC2207E94536225B6BA0B78F3C4FCC78F8 (void);
// 0x00000442 System.Void MoreMountains.Feedbacks.MMWiggle::WigglePosition(System.Single)
extern void MMWiggle_WigglePosition_m2490D7C3DA0CA67E1DFFC279E4DC00CF66F82365 (void);
// 0x00000443 System.Void MoreMountains.Feedbacks.MMWiggle::WiggleRotation(System.Single)
extern void MMWiggle_WiggleRotation_m7E39A1A5F0DCED24721720C3C8A5729D40761D18 (void);
// 0x00000444 System.Void MoreMountains.Feedbacks.MMWiggle::WiggleScale(System.Single)
extern void MMWiggle_WiggleScale_m92ADADB43C7BBB7783B1EAC81EE8A6C5CD3D5EC5 (void);
// 0x00000445 System.Void MoreMountains.Feedbacks.MMWiggle::WiggleValue(MoreMountains.Feedbacks.WiggleProperties&,MoreMountains.Feedbacks.InternalWiggleProperties&,System.Single)
extern void MMWiggle_WiggleValue_m079EC28489ABA0798EC61E23BD054DB5BA01277E (void);
// 0x00000446 System.Void MoreMountains.Feedbacks.MMWiggle::Start()
extern void MMWiggle_Start_mF8806C45942DC58B4BC28067E33D274DDC44CDF1 (void);
// 0x00000447 System.Void MoreMountains.Feedbacks.MMWiggle::Initialization()
extern void MMWiggle_Initialization_mBA8229E2553D6AEC112B7F309E08773348446478 (void);
// 0x00000448 System.Void MoreMountains.Feedbacks.MMWiggle::InitializeRandomValues(MoreMountains.Feedbacks.WiggleProperties&,MoreMountains.Feedbacks.InternalWiggleProperties&)
extern void MMWiggle_InitializeRandomValues_m736452E1EB2A5EFAD6D9A38190AD7310DB51907F (void);
// 0x00000449 System.Void MoreMountains.Feedbacks.MMWiggle::Update()
extern void MMWiggle_Update_m4FFC4CBB259E5DF76F4A0E4197ED19C5E32DCA4D (void);
// 0x0000044A System.Void MoreMountains.Feedbacks.MMWiggle::LateUpdate()
extern void MMWiggle_LateUpdate_m5FBD9D33DF973EADA24389361BEAD533C94F2EA5 (void);
// 0x0000044B System.Void MoreMountains.Feedbacks.MMWiggle::FixedUpdate()
extern void MMWiggle_FixedUpdate_m5183FAFB2476705DDABA15BA4EF937B919CAB494 (void);
// 0x0000044C System.Void MoreMountains.Feedbacks.MMWiggle::ProcessUpdate()
extern void MMWiggle_ProcessUpdate_m10168F7BE23ADA9D397BE3B2CC29111C7AE47E73 (void);
// 0x0000044D System.Boolean MoreMountains.Feedbacks.MMWiggle::UpdateValue(System.Boolean,MoreMountains.Feedbacks.WiggleProperties,MoreMountains.Feedbacks.InternalWiggleProperties&)
extern void MMWiggle_UpdateValue_m2E7E1A10BF0D7FBB45F35D93648D0E0BA20B651D (void);
// 0x0000044E UnityEngine.Vector3 MoreMountains.Feedbacks.MMWiggle::ApplyFalloff(UnityEngine.Vector3,MoreMountains.Feedbacks.WiggleProperties)
extern void MMWiggle_ApplyFalloff_mE69A63D6D94E1786825F10C7F1A954436CA35831 (void);
// 0x0000044F UnityEngine.Vector3 MoreMountains.Feedbacks.MMWiggle::AnimateNoiseValue(MoreMountains.Feedbacks.InternalWiggleProperties&,MoreMountains.Feedbacks.WiggleProperties)
extern void MMWiggle_AnimateNoiseValue_mAA2F63174985C881BEA9060850D79C830D755B11 (void);
// 0x00000450 UnityEngine.Vector3 MoreMountains.Feedbacks.MMWiggle::AnimateCurveValue(MoreMountains.Feedbacks.InternalWiggleProperties&,MoreMountains.Feedbacks.WiggleProperties)
extern void MMWiggle_AnimateCurveValue_m8ABC0235FEE6370E7E49F42884C6DC7846F29D8D (void);
// 0x00000451 System.Void MoreMountains.Feedbacks.MMWiggle::EvaluateCurve(UnityEngine.AnimationCurve,System.Single,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3&)
extern void MMWiggle_EvaluateCurve_m8D09D646A28AC007F55B0C2B6A121E9833599872 (void);
// 0x00000452 System.Boolean MoreMountains.Feedbacks.MMWiggle::MoveVector3TowardsTarget(UnityEngine.Vector3&,MoreMountains.Feedbacks.WiggleProperties,UnityEngine.Vector3&,UnityEngine.Vector3,UnityEngine.Vector3&,System.Single&,System.Single&,UnityEngine.Vector3&,System.Single&,System.Single&,System.Single)
extern void MMWiggle_MoveVector3TowardsTarget_m925538B42260ED20F8428AF5075FB1EAF105AF2D (void);
// 0x00000453 UnityEngine.Vector3 MoreMountains.Feedbacks.MMWiggle::DetermineNewValue(MoreMountains.Feedbacks.WiggleProperties,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single&,System.Single&)
extern void MMWiggle_DetermineNewValue_m45E374999550CC2AC747D7EDCDA6F8FBF93A7810 (void);
// 0x00000454 System.Single MoreMountains.Feedbacks.MMWiggle::RandomizeFloat(System.Single&,System.Single,System.Single)
extern void MMWiggle_RandomizeFloat_m41B62471F3FFB65CF9339B8FD5DD61A70A0EF3CD (void);
// 0x00000455 UnityEngine.Vector3 MoreMountains.Feedbacks.MMWiggle::RandomizeVector3(UnityEngine.Vector3&,UnityEngine.Vector3,UnityEngine.Vector3)
extern void MMWiggle_RandomizeVector3_m9FD26FFD3DB00D5BB321935D0495E937B1A87E6E (void);
// 0x00000456 System.Void MoreMountains.Feedbacks.MMWiggle::.ctor()
extern void MMWiggle__ctor_mE4306682B5DB6DFC965014C12978532AB2CA2F55 (void);
static Il2CppMethodPointer s_methodPointers[1110] = 
{
	MMCameraZoom_GetTime_m2068491132423511BA20DAC2D89A54D300B1A864,
	MMCameraZoom_GetDeltaTime_m7847DB5E04392CCE331C2AA265E9FB8631ADE613,
	MMCameraZoom_get_TimescaleMode_m17A6D072080C167A32D7E1099DDB509091503DD4,
	MMCameraZoom_set_TimescaleMode_m550264151A96573D6AF4413269FD40131015EA9B,
	MMCameraZoom_Awake_m194DF77FAC0B8B443AA46EDF6BF0A96476017AEF,
	MMCameraZoom_Update_m84A25601A88F888A3A809ADD0880266EF1CB0835,
	MMCameraZoom_Zoom_mA05EDB60F7F1222A0C644ABBB858D3011A4830A9,
	MMCameraZoom_TestZoom_mC31D0FD024743CB4383AF2CF073663BD094D0913,
	MMCameraZoom_OnCameraZoomEvent_mF50C3D3681E80C550701C1F006AB9841E49751D1,
	MMCameraZoom_OnEnable_m12E2D1AC820A4919A1EAEFAC8E2F47AB6C9A00EC,
	MMCameraZoom_OnDisable_m53129CF3FDB98B162CE7CDFA8172EE75CC1D4995,
	MMCameraZoom__ctor_m24634315897A63754BB1C388BC0CF3D47FDF114F,
	MMFeedback_get_Owner_m63F9C3D5969DC9FF3A9F0A53D1F6BC0EB3532701,
	MMFeedback_set_Owner_m5E2F72382AD898B827BFDE0FEC443B6713040483,
	MMFeedback_get_Pause_mC2DE89B84D2AD70BF9546DD90089551498EE54F1,
	MMFeedback_get_HoldingPause_m66061AEBE8C9C03193790BEE3B7DA3FB6830A4A6,
	MMFeedback_get_LooperPause_m902D6A894F4EF3EFE4638B580A8F0449F5BF4F8A,
	MMFeedback_get_ScriptDrivenPause_m85C6C86000E39C09F73DC8E060C05B1BE31CA475,
	MMFeedback_set_ScriptDrivenPause_m54FE672636C77CDEDD86D09EB6940C14C01E91F3,
	MMFeedback_get_ScriptDrivenPauseAutoResume_m3A8E31CE15BE39FFDF957436A6AD7B04A679BF03,
	MMFeedback_set_ScriptDrivenPauseAutoResume_m4044262829AC46F5443CBD5D5FC5535FF49004C8,
	MMFeedback_get_LooperStart_m32F59753E10FE4B3A105275C7A6F3229C1279959,
	MMFeedback_get_InCooldown_mD63F98DAF3FA28B2A69CE8A7628A517B04D0C1BE,
	MMFeedback_get_FeedbackTime_m859557BFFE2B1372640EBF091A04110D905237EA,
	MMFeedback_get_FeedbackDeltaTime_m3EF4F11444CD2F00CF1E656478EFC2EC0DF541E7,
	MMFeedback_get_TotalDuration_mBE68C3225F4D5C260128E0B017A20780DED656B9,
	MMFeedback_get_FeedbackStartedAt_m9D55849F3AE573E152B40B81601BDD63DAB70854,
	MMFeedback_get_FeedbackDuration_m779C7BCA789A12C27B51FA16DBAEC13FA14CE4F0,
	MMFeedback_set_FeedbackDuration_m041777E9B351A2FCFA282CCCC0054B6ECD9D8A55,
	MMFeedback_get_FeedbackPlaying_m6DDD0E0F5ECA73755FDCDFE3C26B980B755B704B,
	MMFeedback_OnEnable_m0AD9E20E74DCE5FF5F61C4FB851AFE61523E7B43,
	MMFeedback_Initialization_m46BB5D333ADB8DFCDC08D574D81874ADFB1ED754,
	MMFeedback_Play_m000C46C50EB43FB7602F8820C7C0AB47912A71CA,
	MMFeedback_PlayCoroutine_m3F88EED83FC1665F5E2D6ACB54C45F84AA70D8B1,
	MMFeedback_RegularPlay_m72B414DA67FD949B77A325BA76871CF10A34D944,
	MMFeedback_InfinitePlay_m0F1BEA028328D998EE7EEBB806B08408248E7F4F,
	MMFeedback_RepeatedPlay_mCF60A914ECD12385697ED9CFEF0B4EB223F35855,
	MMFeedback_SequenceCoroutine_m1EB54B24BD156A2BC19C0B2D98760437AAAB1CD6,
	MMFeedback_Stop_m95B7701199FE07D732A17EC9558E5B08276F9839,
	MMFeedback_ResetFeedback_mBABB311F16940183E4C74724CC4FDA3EB5D3F880,
	MMFeedback_SetSequence_mD700BF49C2780F56592C25A6E23A3671852BE123,
	MMFeedback_SetDelayBetweenRepeats_m873523C356931E3DCF1037D713592FD42D770FED,
	MMFeedback_SetInitialDelay_m8671E6147069DBC1F5DA94A318C511B46172D1E9,
	MMFeedback_ApplyDirection_m31EF74481694DD129B1B74B6E2109CB4BE8E16E6,
	MMFeedback_get_NormalPlayDirection_m44B2BC2A146B5B4F7DAEB23E8D8B438411F6AD8B,
	MMFeedback_get_ShouldPlayInThisSequenceDirection_m34D1046B1515B51EA22D1D94C0A290B68130ED35,
	MMFeedback_get_FinalNormalizedTime_m2145A7381BF90871988EFDFBE1565D2AA6245D72,
	MMFeedback_ApplyTimeMultiplier_mE0954046F0D9F7C571D0FDF16A192015476A5255,
	MMFeedback_CustomInitialization_mAC357FEAB8BCCF8CC643C52735DD8353A8266108,
	NULL,
	MMFeedback_CustomStopFeedback_m637BE3F6114F85830A2E7073DEEF05B95CA79A83,
	MMFeedback_CustomReset_mE76A604F77F74907462823CD9DD8C637154C4E62,
	MMFeedback__ctor_m3A0FE0C8FEDD2D759EC31C4432C65DBD4B1A06A0,
	U3CPlayCoroutineU3Ed__58__ctor_mC9C5017FD78844E0F590C43CFD73A580B1434709,
	U3CPlayCoroutineU3Ed__58_System_IDisposable_Dispose_m44DD2EA42BC393E7D82F11A428D92D5E75C1D181,
	U3CPlayCoroutineU3Ed__58_MoveNext_m7423612FC0742A009203CDFA2DDA1F71CBFAAC3A,
	U3CPlayCoroutineU3Ed__58_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9E74D0E47E0AE1A5D999511AFE5A066BD0A8C320,
	U3CPlayCoroutineU3Ed__58_System_Collections_IEnumerator_Reset_m8538C8448939CDC0819C8DB13A5C794EE27AD950,
	U3CPlayCoroutineU3Ed__58_System_Collections_IEnumerator_get_Current_mE2352A87DE9F7E54897223BFAA151AB92929FDC0,
	U3CInfinitePlayU3Ed__60__ctor_mA73B1C0ACC3B6FEA9CA35F0C970E47DF45402430,
	U3CInfinitePlayU3Ed__60_System_IDisposable_Dispose_m55FBB56BFDAE9EECD3BA8FC2B8BF5A703B81CECD,
	U3CInfinitePlayU3Ed__60_MoveNext_m8EAD1A52935D311ACE8C34B78317074E7BCBC557,
	U3CInfinitePlayU3Ed__60_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m365B5009C628E89C60865D59235B8A8A3F833589,
	U3CInfinitePlayU3Ed__60_System_Collections_IEnumerator_Reset_mC54B04E994A532C5AEEE35019ABEA377B2680353,
	U3CInfinitePlayU3Ed__60_System_Collections_IEnumerator_get_Current_mC7CC84D5220097B3FC374576454BA030424C4727,
	U3CRepeatedPlayU3Ed__61__ctor_m38C82BD8EBBE5CB0B105C4F8FE4EC8B2F87042DB,
	U3CRepeatedPlayU3Ed__61_System_IDisposable_Dispose_m06EFD40CE9952851BBB70ECA63A56C9257FE3E14,
	U3CRepeatedPlayU3Ed__61_MoveNext_m568C94D2FA3E1E8F266A98246E2745F10C1AC2D3,
	U3CRepeatedPlayU3Ed__61_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3954ADB6A68EAC2027B93A1B68844D1B47A0328D,
	U3CRepeatedPlayU3Ed__61_System_Collections_IEnumerator_Reset_m35D061548946F2AE3873A189035B44EDED0A5875,
	U3CRepeatedPlayU3Ed__61_System_Collections_IEnumerator_get_Current_m20D38276E19684B89A6037B22F70136D887237E9,
	U3CSequenceCoroutineU3Ed__62__ctor_mAD65179E5DABB6CEF15ABA9E5B8C3F31443AC10C,
	U3CSequenceCoroutineU3Ed__62_System_IDisposable_Dispose_m0BEB40525A7B844D42E3C19695E60059F01BA53A,
	U3CSequenceCoroutineU3Ed__62_MoveNext_m3C38C93033DF2855AF968B7E71FD21B498CA1032,
	U3CSequenceCoroutineU3Ed__62_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m902F740D5A7D238FA3E1AA2D1C9ED7CD078DAEF7,
	U3CSequenceCoroutineU3Ed__62_System_Collections_IEnumerator_Reset_m987A51776BCB2B27E87BB0915A3A41C6E9374EC7,
	U3CSequenceCoroutineU3Ed__62_System_Collections_IEnumerator_get_Current_m64C0C9DA602E6222F01E7B04622A7DAEEB6293AF,
	MMFeedbackTiming__ctor_m4787969CCD5E69414860BF69AB073363C35F3505,
	MMFeedbacks_get_IsPlaying_m3C443D7BB441842F86D0C290899BE5F2E59A2A10,
	MMFeedbacks_set_IsPlaying_mAEEBF8B704BC482599E1B1C407A5B70310D37208,
	MMFeedbacks_get_InScriptDrivenPause_m356B03C06DD785C0578AF32266F10997350A7752,
	MMFeedbacks_set_InScriptDrivenPause_m0FA29E2487517BE4D14A64D0B29C3C8F12331728,
	MMFeedbacks_get_ContainsLoop_m64535C00630AEA3B035B12B0739F71924C73A553,
	MMFeedbacks_set_ContainsLoop_m40799578BB397910F523CEF4B849BB0E3AE8F38C,
	MMFeedbacks_get_ShouldRevertOnNextPlay_m44C356D9B5D61FE7BE0997F8D40006D53173AF0F,
	MMFeedbacks_set_ShouldRevertOnNextPlay_m6CAD5F633C7316C193838BBC384FC6CE1124961C,
	MMFeedbacks_get_TotalDuration_m5DD5C14AE3B09159B8ECC8B49315F7DB586C7FC0,
	MMFeedbacks_Awake_m2E44C906A44F5491827A235182A6A6CB6B0BC795,
	MMFeedbacks_Start_mE74BADC5EC5F6CA76A95A03A60F389789B62BFC0,
	MMFeedbacks_OnEnable_mCD331E81D3B043EF2F77354CD3D47CCBA1E69887,
	MMFeedbacks_Initialization_m5F3790C04A488F928BAB90B02263764468C8800F,
	MMFeedbacks_Initialization_m862463CC3B0FCFE5A6850DDE14EDBBD9D2A2C97D,
	MMFeedbacks_PlayFeedbacks_m0AF9AF19A7F328FAE775FB2C97098EAC314CBFB6,
	MMFeedbacks_PlayFeedbacks_m0BB902E0BC8DF75635DC4638F57541E6EFD22498,
	MMFeedbacks_PlayFeedbacksInReverse_m54CE2D030E7208C4A6AD1892D37ABBA540DFC636,
	MMFeedbacks_PlayFeedbacksInReverse_mB6E69A04B86463C902EA1E212D4DB7748405DCD4,
	MMFeedbacks_PlayFeedbacksOnlyIfReversed_m812C631AB362A3CD01B1A990DCE297C2C9515E77,
	MMFeedbacks_PlayFeedbacksOnlyIfReversed_mFA70A32F3A1DE031A2228AB7F511ABA08E769243,
	MMFeedbacks_PlayFeedbacksOnlyIfNormalDirection_m5785FE8FE9CDE22AF0CF3B5895C66AD2ECC43057,
	MMFeedbacks_PlayFeedbacksOnlyIfNormalDirection_m77E30A22360FB55E4BE2A906A93EE0AF78310A44,
	MMFeedbacks_PlayFeedbacksCoroutine_m2FE237BD4BB72E0B2C8CA2634FEA72AEAC9FD661,
	MMFeedbacks_PlayFeedbacksInternal_mCA46E8DB621294C797EEF0D88851B19286C732DC,
	MMFeedbacks_PreparePlay_mAF7F0F42D92A27DC308B2BC28156C73F62C039A3,
	MMFeedbacks_PlayAllFeedbacks_m0E781BDF8A3624EB97ACD077C753956C9DF0C7B1,
	MMFeedbacks_HandleInitialDelayCo_mC7B15E61934CCBFDB2E909A6B3672DC58983F716,
	MMFeedbacks_Update_mA73C2E16AEA8D193037FEE8152C5724727FD0815,
	MMFeedbacks_PausedFeedbacksCo_mD4824A4FF58457AF9E6A4354195AC923422DE198,
	MMFeedbacks_StopFeedbacks_m41AABDDB5B9DAC8DA6BBF61F67D0375C8C0ED455,
	MMFeedbacks_StopFeedbacks_mE8CA30B713B29DEAC2371D8CF4A6B99D18282D23,
	MMFeedbacks_StopFeedbacks_m5A45CD3B04CC076018BCD599C1E8252DABC42E1F,
	MMFeedbacks_ResetFeedbacks_mECF6867D1FD85336ABD59F6875CE41168017396B,
	MMFeedbacks_Revert_mA2876FCFD9A9B44F4A72F254DCF512161ED5F7FE,
	MMFeedbacks_PauseFeedbacks_m48FDCCA8544B37337376BCD9DD07EDF3C7D01AC4,
	MMFeedbacks_ResumeFeedbacks_mFC86928F24E25F551638C24104A7A2C04DFE26D6,
	MMFeedbacks_CheckForLoops_mB435419B576D22C5D2B9845A772DD3C1A6F20BD8,
	MMFeedbacks_FeedbackCanPlay_m1B8CE132BA0F40691F8FC6FED3C83E6906993E5E,
	MMFeedbacks_ApplyAutoRevert_mE3E5062B711C587BEC7FF625214F3627F7E74B4E,
	MMFeedbacks_ApplyTimeMultiplier_m222E6077ECF7BB3A3F38033612F4BB37EF880A38,
	MMFeedbacks_AutoRepair_mA532BFC1ECC42AF1534CADA8BC8F4EEB7B852DFA,
	MMFeedbacks_OnDisable_mDE76F3B1AE108DF402AE21924282488E3858EDB7,
	MMFeedbacks_OnValidate_m8F01E4D093BC1F29F040A17E2099110A5B7ECD0B,
	MMFeedbacks_OnDestroy_mC30B75631467DF692D696FAC7E99F036F55D186B,
	MMFeedbacks__ctor_mCCC22610F29B910990B179458757258070A2C5C3,
	MMFeedbacks__cctor_m7FCA76CCD2A3C35199278B8920D6A9B23BECC116,
	U3CPlayFeedbacksCoroutineU3Ed__55__ctor_m2C5134D9A3D7999ACAA5E4C35EB6A42134EC6413,
	U3CPlayFeedbacksCoroutineU3Ed__55_System_IDisposable_Dispose_mD50066F28EB32BF9092EF1970941D5AB112C7E40,
	U3CPlayFeedbacksCoroutineU3Ed__55_MoveNext_mBFC0D6143B972825A9204B610916E5011BE32937,
	U3CPlayFeedbacksCoroutineU3Ed__55_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m34AF67D1655C1355F01BCE2C21DFB171A0E20F13,
	U3CPlayFeedbacksCoroutineU3Ed__55_System_Collections_IEnumerator_Reset_m8D94DB4AEDE5D4AD0866D29E4A90578F88F10079,
	U3CPlayFeedbacksCoroutineU3Ed__55_System_Collections_IEnumerator_get_Current_m4C1CE11FE20BC30743B93B057235192260FD93D1,
	U3CHandleInitialDelayCoU3Ed__59__ctor_m4FF7EA1DDA7C52CE5D6C45AFE621738D137B257C,
	U3CHandleInitialDelayCoU3Ed__59_System_IDisposable_Dispose_m7ACAE8CF67073E559A8F6A8C7B661DA37E21D38A,
	U3CHandleInitialDelayCoU3Ed__59_MoveNext_mC3656BD0516533DBFA55A45AF2ACCA87545B149C,
	U3CHandleInitialDelayCoU3Ed__59_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD9929CD4025779BABD62C1105179BD66C7812C0C,
	U3CHandleInitialDelayCoU3Ed__59_System_Collections_IEnumerator_Reset_m138C129253DBB87D95D51D9DF97074ECA9F36485,
	U3CHandleInitialDelayCoU3Ed__59_System_Collections_IEnumerator_get_Current_m36D3A25FB97FF664039DDCF195E7483D592296A1,
	U3CPausedFeedbacksCoU3Ed__61__ctor_mE828EE8E7A0A7D319E71D73195F874B871074802,
	U3CPausedFeedbacksCoU3Ed__61_System_IDisposable_Dispose_mD72C088F8929CF2F53E9394515D2C3ED73FB35D1,
	U3CPausedFeedbacksCoU3Ed__61_MoveNext_m131A82B702D2DA9FB180ADF62D8CD493E450DFBB,
	U3CPausedFeedbacksCoU3Ed__61_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m30A2361BCB01AC87C75487CAFA3C4705677B5BDC,
	U3CPausedFeedbacksCoU3Ed__61_System_Collections_IEnumerator_Reset_m602A5BF1882100229396E49AA72E831AC07AA8AD,
	U3CPausedFeedbacksCoU3Ed__61_System_Collections_IEnumerator_get_Current_mB0BB9D318DD4174EAB161F835869244CF64AA955,
	MMFeedbacksCoroutine_WaitForFrames_m889F49D355BDFCE9BB18745C3B4986F41E37CDE3,
	MMFeedbacksCoroutine_WaitFor_m7EDC39169AFDDFE2167439BBAEC3162C5A9F7F3A,
	MMFeedbacksCoroutine_WaitForUnscaled_m70F048519D44202AED12373058EAB07E73BD6392,
	U3CWaitForFramesU3Ed__0__ctor_m51A7E4D200869F6385BBCE67471F1210DD676AAE,
	U3CWaitForFramesU3Ed__0_System_IDisposable_Dispose_mBF0817F633C4CFE00D9A9BB36E67D163460912E3,
	U3CWaitForFramesU3Ed__0_MoveNext_m5EB9853CB33B0384A3E78D2221C9DB60F3BC1445,
	U3CWaitForFramesU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBFB79431CD3410C1371FA6F19B2017E12D1A1F91,
	U3CWaitForFramesU3Ed__0_System_Collections_IEnumerator_Reset_mF510577CB66261DA47F52AF852724629220F1B33,
	U3CWaitForFramesU3Ed__0_System_Collections_IEnumerator_get_Current_m9BD6AF8E22BCEE245A4639263C057B4D2F469399,
	U3CWaitForU3Ed__1__ctor_mF2D4A952345A4505C6BCB3718320FA386DE95231,
	U3CWaitForU3Ed__1_System_IDisposable_Dispose_mFFB0FC691566115E3A54E7A98D2A4C89F28A2B90,
	U3CWaitForU3Ed__1_MoveNext_m272532ADA03BAD4697311BFF660891D4CAF82C33,
	U3CWaitForU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4C08B005478726D43D693AB7E929C86213574DF4,
	U3CWaitForU3Ed__1_System_Collections_IEnumerator_Reset_mBD14C6A7E82361E0024C5F9F9D76422AB31A48BE,
	U3CWaitForU3Ed__1_System_Collections_IEnumerator_get_Current_mD9413A0EB26D8EAA3A90404C05A5B64AF32F9EC4,
	U3CWaitForUnscaledU3Ed__2__ctor_m1D107AA135A7A4BFEEBCC101EBC4F8C560C50D79,
	U3CWaitForUnscaledU3Ed__2_System_IDisposable_Dispose_m267475D7DCC00092E11755197BDA9FB114BCF304,
	U3CWaitForUnscaledU3Ed__2_MoveNext_m6F62BDA7B47E0AC95871E93E6BF1A4789F2569B7,
	U3CWaitForUnscaledU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6E41B2E3DE4BF58827B9C16B54D8909CB2799686,
	U3CWaitForUnscaledU3Ed__2_System_Collections_IEnumerator_Reset_mE7F7DCA1A43AC58DC6006C2557F040CB73575754,
	U3CWaitForUnscaledU3Ed__2_System_Collections_IEnumerator_get_Current_mD732E15C90C4EF2BDE33E230E61F6E52EB886B86,
	MMFeedbacksEnabler_get_TargetMMFeedbacks_mD20D74821D41BF1FF32C578E2954EF1594263867,
	MMFeedbacksEnabler_set_TargetMMFeedbacks_mF95CAD31DA383C45A554C0A6499C2D9B85FCCD1A,
	MMFeedbacksEnabler_OnEnable_mA9D1C595CA467AB39F490E5C4F970DFD727D2BE4,
	MMFeedbacksEnabler__ctor_m7040DC6BFB5FCD0E186DC669798DFF4757A04918,
	MMFeedbacksEvent_add_OnEvent_mB2C6B5745AE0F6BEB298CED750A428D7093889A1,
	MMFeedbacksEvent_remove_OnEvent_mF1FE13A8F480127061F78CA7E2E2C0F05F5972CE,
	MMFeedbacksEvent_Register_m8189959F836C8C02CD61B7E18CDBDABB87C255EF,
	MMFeedbacksEvent_Unregister_m44210D34C4679B739CAD0A64C233673A188937AE,
	MMFeedbacksEvent_Trigger_m706BC4BE3219AF3202527D0816A30A27B4578702,
	Delegate__ctor_mE6E828D19AFC3684CF4932FC5A3EFD004513C53E,
	Delegate_Invoke_m3680BDB32E14FDDD44A1D03C9443EB2D3FB1A548,
	Delegate_BeginInvoke_m3FD7E4A6ABC990ECB3990B66A9236430F1B617EB,
	Delegate_EndInvoke_m0D6DA0ED27360FCB04353D4EAE02AEBC6B97F67D,
	MMFeedbacksEvents_get_OnPlayIsNull_m50C6B5A87E27E4953652E25C680A51B14D244F57,
	MMFeedbacksEvents_set_OnPlayIsNull_m7E23F57BF5ED7F3FD7FA3AADBA7B62DA353FDEF6,
	MMFeedbacksEvents_get_OnPauseIsNull_m200330A23EC251094DD69F0D9BBD028695DCE579,
	MMFeedbacksEvents_set_OnPauseIsNull_m44604A1DF09463075A67BA32B3B47B310E7F0EDD,
	MMFeedbacksEvents_get_OnResumeIsNull_m5F402F0D594F64EBB248F82BFF14A8F1589E4A89,
	MMFeedbacksEvents_set_OnResumeIsNull_m3FFA6FC75D64C8970F0383B9B6FA6491883D328D,
	MMFeedbacksEvents_get_OnRevertIsNull_mE1819EB1DD5078CAB44188A09A78FD3ACD809E90,
	MMFeedbacksEvents_set_OnRevertIsNull_mF6A3F80FFE08003C99389CFCAA0B3F9A31462E0D,
	MMFeedbacksEvents_get_OnCompleteIsNull_m4C0DA7A468C4BAD3DE6649F86CF66573C265010B,
	MMFeedbacksEvents_set_OnCompleteIsNull_m6481DC89A306058719C7E3C106DAA62762ACBB18,
	MMFeedbacksEvents_Initialization_m920437E7851C073E2D24635152886AEFAE343667,
	MMFeedbacksEvents_TriggerOnPlay_m442327A8831309A0B98E2D3021173EFFDCE2520E,
	MMFeedbacksEvents_TriggerOnPause_mF27B9E8C8893FC347C5DCA0856ECBCDAFF743A5D,
	MMFeedbacksEvents_TriggerOnResume_m8070597601B720D2DFB49622E804AF41FD0E3EDE,
	MMFeedbacksEvents_TriggerOnRevert_m2581D7D5CBD3319A09D0B0E121A26F004BC1FAF6,
	MMFeedbacksEvents_TriggerOnComplete_mA45D38B3DAB67A52A5588AD9AD96A6061907F3D1,
	MMFeedbacksEvents__ctor_m9FFC68B7BCCCCC30C0D40168D4349535B95A4E4F,
	MMFeedbacksHelpers_Remap_mF91639B3964272F011DC211808184EC25AF9571F,
	MMFeedbacksHelpers__ctor_m6EBAD14745D011242953EA76E992102551D68A1D,
	MMFReadOnlyAttribute__ctor_mED0F52D7CF9B8B44132D444186F6E8148E6BDFC3,
	MMFInspectorButtonAttribute__ctor_m9F7D6C8785E6F916CBCF7844EEDFDE4E942F648D,
	MMFEnumConditionAttribute_ContainsBitFlag_m3A55739C9BE0CD1954041C6750C055FB854392D9,
	MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F,
	MMFInformationAttribute__ctor_m004E74EBE93920E7BAC370DDB10E141EDE1A073D,
	MMFHiddenAttribute__ctor_m4586259502097BA3F7078D7F8926807CB993D69B,
	MMFConditionAttribute__ctor_m2EFB750D34B58F0035B297E3968682303584D7B4,
	MMFConditionAttribute__ctor_mABD947D1EABD573CF6BED72E3FD7886D4DEA81A3,
	MMFeedbackStaticMethods_GetComponentNoAlloc_m6087AB4282C2D5B39BA0C805CF86A3068CCC103E,
	NULL,
	MMFeedbackStaticMethods__cctor_m96E545DCC2E4AE64916EB195CD35F2FF5C44B3DB,
	FeedbackPathAttribute__ctor_mF36AB244DDE78F2BF1F0B9285F977B1D7212E2BC,
	FeedbackPathAttribute_GetFeedbackDefaultName_m3FFAF101E28D8D884FF6292DD1771D3D7511446B,
	FeedbackPathAttribute_GetFeedbackDefaultPath_m134A9FA8AE3B21A5A47AFCA7881792FCD59B7EBF,
	FeedbackHelpAttribute__ctor_mACF45ABD072B9B8871B703E67DF4E7076C9AFF94,
	FeedbackHelpAttribute_GetFeedbackHelpText_mE34CAAB95E3BF984992A628438D75193BFB9D244,
	MMFeedbacksInspectorColors__ctor_m0249F7399DE838C14CCD297078C5493B281A8EEE,
	MMFeedbacksInspectorColors__cctor_m8AFDB8625880BFB650450C2F25115B44E41408A3,
	MMShaker_GetTime_m838AC29CF09C07EC55ED9B66663EC0E30825621A,
	MMShaker_GetDeltaTime_mE30E675C17F6820E8405BE7A579BA041FC9EB9B4,
	MMShaker_get_ListeningToEvents_mC4D91748475F610B92507BFCC46065A58BCC8CBF,
	MMShaker_Awake_m83FADB62D0163F5D3F9269635BB75B1BE95A361A,
	MMShaker_Initialization_mA0C4D4855C7877F7FD4BE6DB7C55D309A53753B3,
	MMShaker_StartShaking_m9558903CD708F69075930EA811BDA90E150398A1,
	MMShaker_ShakeStarts_mE85CF38CAF236AE4170F956F2E2CC8BA316E24AA,
	MMShaker_GrabInitialValues_m3D924C41760705BF9248469E5E169F1369810597,
	MMShaker_Update_m30C3E8BC8F4BF2F8AB6397F10E9E9F89808C150D,
	MMShaker_Shake_mE02B7B6B1D3ADA802A6338A641EFFBB929E76972,
	MMShaker_ShakeFloat_mF9F54D49B940E1A772BB6490100C3C348983DA39,
	MMShaker_ResetTargetValues_m4A621683BB61D3FD391AAC8F7983528F725B24C2,
	MMShaker_ResetShakerValues_m93AA8579FE316AD1BBCB482D7DC47593B5F70092,
	MMShaker_ShakeComplete_m2F8B3295AA1926EC6F3FB4AF07D73565B9B8B8D7,
	MMShaker_OnEnable_m6C88321C29032BB691EC7BDB99CBC1B77F0D3B12,
	MMShaker_OnDestroy_m3F93893AC5915BBAAE9E6F7DEFFEAB80974CDD9D,
	MMShaker_OnDisable_mE674ADF7B6D142F76FF2731F4831447241C411FC,
	MMShaker_Play_m7198F7132EA436C45FBEBF9178BA0C400844A478,
	MMShaker_Stop_m784E87C0407DCE0F89F3046FF89934CAD5ECA356,
	MMShaker_StartListening_m2FD2D6DA250E2613C19EB5F54D1B4F78803D644F,
	MMShaker_StopListening_m1D60A4786564D907C4378080302EC52998682078,
	MMShaker_CheckEventAllowed_m3D0BAF591A15E941D8ABDE2BEECCA357EF227B5F,
	MMShaker__ctor_m19139ACF2B49DCE913269ED0C2D23D9B08E85C74,
	MMMiniObjectPooler_Awake_m1AC08CFF3BFDD74842AF5F42FFB31F6B4A16CCD7,
	MMMiniObjectPooler_CreateWaitingPool_m8801BE4304E76F3B82D90F44C453E03988785B08,
	MMMiniObjectPooler_DetermineObjectPoolName_m2C89E679BAD3C4386BE6CA5C91B67CECB744714F,
	MMMiniObjectPooler_FillObjectPool_mADD73BE5FF97BE42B33CAF7ECCA48BA0E76A871C,
	MMMiniObjectPooler_GetPooledGameObject_m59EC410CBCCBC4F9F27AE50E9A06DB5A10CCB0B4,
	MMMiniObjectPooler_AddOneObjectToThePool_m5EBE1E3EC43D5992B5FE387DD3C162B9B2E22682,
	MMMiniObjectPooler_DestroyObjectPool_mD6E88BFBE8DB5634F4DAD4F130A8B06F1D89CB44,
	MMMiniObjectPooler__ctor_mF39E7F0727047EEA2317D6D5AD417633D1F14D6C,
	MMMiniObjectPool__ctor_m532EA76284743CADD3E20981AB89B2351D315FD0,
	MMMiniPoolableObject_add_OnSpawnComplete_m8FE54EC9EFE60E277A763F4AF0A8F662BF98A0E1,
	MMMiniPoolableObject_remove_OnSpawnComplete_mA7237858F56A57AC40ED152BAC2C15011DAE685A,
	MMMiniPoolableObject_Destroy_m4BB22A25373E968C2C55A2AE0F3A6D911819984B,
	MMMiniPoolableObject_OnEnable_m25E461030FD27AACE169C8085CED5D5A9D9E4850,
	MMMiniPoolableObject_OnDisable_mE970BB260BC05CB65D05897E674CE33D582B6A5F,
	MMMiniPoolableObject_TriggerOnSpawnComplete_m3626D19FC696C7BFFFDDF31947E0CCE5D8E39A07,
	MMMiniPoolableObject__ctor_m6B356DFFCC997D601B9F0BC1FB56BD6BAC2E7B06,
	Events__ctor_m5B93F86C13019CDC7733BBC5AC6E35CC6A9AEAED,
	Events_Invoke_mEDC030AD3C04E3C0CE15D459122043A10BD9B0DC,
	Events_BeginInvoke_m7540D5B581AFC55D2F51770876A18B649A9C8A72,
	Events_EndInvoke_mF454180134FE4C69A3C63955B6333438E44984B6,
	MMFeedbackAnimation_CustomInitialization_m7BB9427CCA8A923A226C524422F2A01ABCC25D88,
	MMFeedbackAnimation_CustomPlayFeedback_m8B893790F285B18673773DE6289E6642BCD52444,
	MMFeedbackAnimation_CustomStopFeedback_m3BD26DCECDFE2EC00FC5104F8E0BE4DC7766CB41,
	MMFeedbackAnimation__ctor_m5A63B89F3925BA9F6A3F0450E40CEB455D1B547C,
	MMFeedbackAudioFilterDistortion_get_FeedbackDuration_m328E2667245937BF6253BBBF5DE7C175B85C7363,
	MMFeedbackAudioFilterDistortion_set_FeedbackDuration_mA89BAB7D6E3A84100CCAEB4C6BF7BE149E5BAF34,
	MMFeedbackAudioFilterDistortion_CustomPlayFeedback_m7B27B9ED5F0F3E66283ECD2D4C22E1716F7349DA,
	MMFeedbackAudioFilterDistortion_CustomStopFeedback_mB2F08EBD42A8BC84C375FAC3512869BF58FA9B2C,
	MMFeedbackAudioFilterDistortion__ctor_m3F78BEDAD9DB540DCAE5E598214DA5FBF5366C3F,
	MMFeedbackAudioFilterEcho_get_FeedbackDuration_mACFBF8D7377DB724A5EE70509968EE6A4FFD1890,
	MMFeedbackAudioFilterEcho_set_FeedbackDuration_m848D1BDAB04EF17AC970BD02F90E69494F4CBE76,
	MMFeedbackAudioFilterEcho_CustomPlayFeedback_m4B89204964744F5F3BF3BEFE8DF4D766F802494E,
	MMFeedbackAudioFilterEcho_CustomStopFeedback_m83ED6F0546DC3D9518D15F6B5376F8D4C252325B,
	MMFeedbackAudioFilterEcho__ctor_mF22E1CAD26A3CF3AFDA89FD9A3DC8D0703DF8653,
	MMFeedbackAudioFilterHighPass_get_FeedbackDuration_m5F1DA3183EFB2CA38A4B0A721382B8EC4930DF17,
	MMFeedbackAudioFilterHighPass_set_FeedbackDuration_m4B48192EEE44880172523198B9646A4D6D8EEEA9,
	MMFeedbackAudioFilterHighPass_CustomPlayFeedback_mCC07D9533A1234D7B79D95824A46E6F37505FF73,
	MMFeedbackAudioFilterHighPass_CustomStopFeedback_m98166F96962CF2B9436D634F8935D9786B12848D,
	MMFeedbackAudioFilterHighPass__ctor_m94000BD0A0C46AFA0FDF8ADD5DDDD77F326748F4,
	MMFeedbackAudioFilterLowPass_get_FeedbackDuration_m9D2C9F1F36CDDF6C771655818170681AB3530EDB,
	MMFeedbackAudioFilterLowPass_set_FeedbackDuration_m6EACF5C25A23F105214EBB78461822144521EFC5,
	MMFeedbackAudioFilterLowPass_CustomPlayFeedback_mB01C1E46E919E305AEFE048E71E38D27417DFF36,
	MMFeedbackAudioFilterLowPass_CustomStopFeedback_mCCB9804846A0F553B65CE90671A750930D2646BC,
	MMFeedbackAudioFilterLowPass__ctor_m2481D76BA6A755AF0F7E68E77675FAD2616E11E6,
	MMFeedbackAudioFilterReverb_get_FeedbackDuration_m105EC6D8A35571439066076B4D2377737E2C3336,
	MMFeedbackAudioFilterReverb_set_FeedbackDuration_m0A7E9EF2279D16D54693D87E384E87D3E3A3B24F,
	MMFeedbackAudioFilterReverb_CustomPlayFeedback_m6564A41B68F960C003BC32FA7725F2EE8C2EAC00,
	MMFeedbackAudioFilterReverb_CustomStopFeedback_m5BCFDB3E056E5DE43089B9E02EB4EDCFAFAD040F,
	MMFeedbackAudioFilterReverb__ctor_m5177EF5F75DFB689A12AD1E5D907C1043C9E0E85,
	MMFeedbackAudioMixerSnapshotTransition_CustomPlayFeedback_mA5F0D6D5A6CE511A9E47CDB677C2C713F1A8A907,
	MMFeedbackAudioMixerSnapshotTransition__ctor_m03000066326DBEEA507B11172512ACFE2984FF2D,
	MMFeedbackAudioSource_get_FeedbackDuration_m2A3B5ABC4366B6234ACD25544074CFF0289A6356,
	MMFeedbackAudioSource_set_FeedbackDuration_mFDBC2D2C1A8ADC98CEB34A5C4E6004BBD1E2BB9A,
	MMFeedbackAudioSource_CustomInitialization_m072BD94D3EC7A276BDA0BFA07C0821F21D61FD98,
	MMFeedbackAudioSource_CustomPlayFeedback_mEBAB46EA1633C9719F091387F7AD8EAF1B0BF98F,
	MMFeedbackAudioSource_PlayAudioSource_m0C44E0D72738BFFC12C69604BDB2A37A5B22A1E1,
	MMFeedbackAudioSource_Stop_m5B8B1942A1C89A708CE100909C3B9B182AB78C26,
	MMFeedbackAudioSource__ctor_m81E3231025E14EEF9B4E873C7C528F8970822486,
	MMFeedbackAudioSourcePitch_get_FeedbackDuration_mA7FE084862F201DD6B83078B03759A63DD763C38,
	MMFeedbackAudioSourcePitch_set_FeedbackDuration_m4C470F7F807EC52F9942243693700FEA8098A404,
	MMFeedbackAudioSourcePitch_CustomPlayFeedback_m0D39FE7BD0BDF78CD1D633AB18F7D3F23C6AE3F2,
	MMFeedbackAudioSourcePitch_CustomStopFeedback_mB760852632A33386E9CA1A4BAD274B978FA08AAA,
	MMFeedbackAudioSourcePitch__ctor_mD4351639476BFAC7860F534815EA2BEC31753A7D,
	MMFeedbackAudioSourceStereoPan_get_FeedbackDuration_m697C94EEDDFAEB25B87E05A62DE39C7A4A04BCDC,
	MMFeedbackAudioSourceStereoPan_set_FeedbackDuration_mB8747CD979B5E953A513E5E503168949F1EB8680,
	MMFeedbackAudioSourceStereoPan_CustomPlayFeedback_m96280553351055A5F5246721CE9E44BA104B33CC,
	MMFeedbackAudioSourceStereoPan_CustomStopFeedback_m42DDD7A7A2D868127217BB7DCFEEC92E8C9277A4,
	MMFeedbackAudioSourceStereoPan__ctor_mEFAC2188FE0C02EFEE6F41A8B6A32E3093E91820,
	MMFeedbackAudioSourceVolume_get_FeedbackDuration_mDF91E5A7549F9DDA4D563824E3DF1412691E82E5,
	MMFeedbackAudioSourceVolume_set_FeedbackDuration_m6CF701A25EEF9C6AB71991B141352359D08B18E6,
	MMFeedbackAudioSourceVolume_CustomPlayFeedback_mA7107CBB4C69A5CC88DFF68BECB6648B0E75DECC,
	MMFeedbackAudioSourceVolume_CustomStopFeedback_mADAA24A38A0731A2CC9CED88565122E1D92B2663,
	MMFeedbackAudioSourceVolume__ctor_m28CF50087315A29A83E2280CE608C35DD8A7CA05,
	MMFeedbackCameraClippingPlanes_get_FeedbackDuration_m9E73C543A67F5BDDD80711232D0ADE90E132FA75,
	MMFeedbackCameraClippingPlanes_set_FeedbackDuration_m6DF65E7142F7690CB21A38247330C38CDB689ACD,
	MMFeedbackCameraClippingPlanes_CustomPlayFeedback_mEFB9C9C14E472E9A573467A16BB75D9DABABD47E,
	MMFeedbackCameraClippingPlanes_CustomStopFeedback_m0D6C0DC68F3B5372E3E8199A138CC157D0EF0659,
	MMFeedbackCameraClippingPlanes__ctor_mBD44901BCCEC3EAE69E969ADB6D2AEBDCFC1CD3B,
	MMFeedbackCameraFieldOfView_get_FeedbackDuration_m215E57DF280D2F5C06E695D889FA4BE7840556F3,
	MMFeedbackCameraFieldOfView_set_FeedbackDuration_mEE9CE5C02C21F7D9B5FA300FC71962FEFDD80697,
	MMFeedbackCameraFieldOfView_CustomPlayFeedback_m16DEFBCB6F851CBB9DD67FBA39F50C8F6E72F890,
	MMFeedbackCameraFieldOfView_CustomStopFeedback_mE1986923FF61C94DF08B6453F3FB5CB8CC44FBD3,
	MMFeedbackCameraFieldOfView__ctor_m418D7427A2D64555E3F9E347EADC1FAAE586A6B2,
	MMFeedbackCameraOrthographicSize_get_FeedbackDuration_m72AA01A11409AAEFB58048D2A45D0CF33B242711,
	MMFeedbackCameraOrthographicSize_set_FeedbackDuration_m2D2DA88832B78138E3B6C69339388511CC794BC5,
	MMFeedbackCameraOrthographicSize_CustomPlayFeedback_m6D8CCB36690519D140C0D95697FDCE87A6813760,
	MMFeedbackCameraOrthographicSize_CustomStopFeedback_m4702A4F1120075338BA9D23859471FC23E2F91D1,
	MMFeedbackCameraOrthographicSize__ctor_m99959D63E367629B21500EF6707085664CC44217,
	MMFeedbackCameraShake_get_FeedbackDuration_m283B4279AC7C9335E8540FF1BAADA02146FC3814,
	MMFeedbackCameraShake_set_FeedbackDuration_m48292E491E37CA9D8836F8DA4267086A25A936CB,
	MMFeedbackCameraShake_CustomPlayFeedback_m0E4CAD5D8FE1D27CD7C8B7446809CE86CC8E07F3,
	MMFeedbackCameraShake_CustomStopFeedback_m8904AADF2ED77F1CA4C64378581928B9B6F8E8C3,
	MMFeedbackCameraShake__ctor_mF9CCBDD5176B753E0DBECA3E38844DA66680F708,
	MMFeedbackCameraZoom_get_FeedbackDuration_m2523894B8E1AE95D9CF7FD1A1C0241B061963325,
	MMFeedbackCameraZoom_set_FeedbackDuration_m246212ADAA2B5DB9AA334975030DFD02AC58CE2E,
	MMFeedbackCameraZoom_CustomPlayFeedback_m783046A566543634BA75DBF0F9D78C5637302640,
	MMFeedbackCameraZoom_CustomStopFeedback_m38FDF997D3D17698CA0EFD2F9692B533D5BDC247,
	MMFeedbackCameraZoom__ctor_m9CE11AA15D0FC3150A96CD2A2F77CA9A66781D50,
	MMFeedbackCanvasGroupBlocksRaycasts_CustomPlayFeedback_m62301B04B27377C797C66A94E8E6F259DA86F076,
	MMFeedbackCanvasGroupBlocksRaycasts__ctor_mAEC6E90997EEC0856E1AA58765824306B661EFAF,
	MMFeedbackDestinationTransform_get_FeedbackDuration_mCC7ECCEED4C679FC6EEE72CA9398E409DDA7DF89,
	MMFeedbackDestinationTransform_set_FeedbackDuration_m48DEA369690EF3BBE486FADFFA3B55F9C4B90AA3,
	MMFeedbackDestinationTransform_CustomPlayFeedback_m7B5A404478DEE37FBFDB3261F145B29B8BAECF4D,
	MMFeedbackDestinationTransform_AnimateToDestination_m479F6EB59B0CA179CA2A863AF81AAE86ED4188E6,
	MMFeedbackDestinationTransform_CustomStopFeedback_mC6AC1FE7F60B74D25746F52135EB958A694C53D7,
	MMFeedbackDestinationTransform__ctor_m166DACE856BEF2785D1DABD3D44F2A6FBB9AAC46,
	U3CAnimateToDestinationU3Ed__40__ctor_m8E597C5E47D4B51D7EF677E4986AE9487AC8B103,
	U3CAnimateToDestinationU3Ed__40_System_IDisposable_Dispose_m8CB6F82FE8F28C595144DEF8849AC4EF7E10F3F2,
	U3CAnimateToDestinationU3Ed__40_MoveNext_m462AF3CD42DDE2FB0DE70C84AFC8366BB3BE59AB,
	U3CAnimateToDestinationU3Ed__40_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9EA75D1CAB42ABB2DB716F25D2FFC7C96519A8A1,
	U3CAnimateToDestinationU3Ed__40_System_Collections_IEnumerator_Reset_m308D5C300EA8D602778199AE76CCC9A9F00C0139,
	U3CAnimateToDestinationU3Ed__40_System_Collections_IEnumerator_get_Current_m9ABDEE4365722E0B7BE6C1E3E9CBE37B51FAA43D,
	MMFeedbackDestroy_CustomPlayFeedback_m00DC8C244218EB344187C8C739E04E0B604A05F6,
	MMFeedbackDestroy_ProceedWithDestruction_mFA107E2074F083F539984482A86A6C11FC3C51A5,
	MMFeedbackDestroy__ctor_mA952EC05CD41F8DBA14984855F6A704159EFED28,
	MMFeedbackEnable_CustomInitialization_mB5E4F810E8239634C022ED1877582B258E97BE49,
	MMFeedbackEnable_CustomPlayFeedback_m36E01B4CCEA1FC124CB4D5E618858109A5F6D725,
	MMFeedbackEnable_CustomStopFeedback_m85E697513B74E2BDF737A93370ACAE2EC1C10C7D,
	MMFeedbackEnable_CustomReset_m0AE901EFBABF619B7DC67E5E7EE26DEB53ECDCFA,
	MMFeedbackEnable_SetStatus_m050AA63187850EE559103B4BC4C02C86718CDB7A,
	MMFeedbackEnable__ctor_m7E43DFB3DD3451268F8E866EBE050BD4CD180CF4,
	MMFeedbackEvents_CustomInitialization_mA593134F949336906E9E622B42526F979CB90D0F,
	MMFeedbackEvents_CustomPlayFeedback_mB61426319A9D9D5F3A0E2ED8252C7558CF916035,
	MMFeedbackEvents_CustomStopFeedback_m455ED7E79552E9E7D288E40B2BFAEF16430BAC78,
	MMFeedbackEvents_CustomReset_m4620407B3833A4090B6FF4741BA068C443A0A61F,
	MMFeedbackEvents__ctor_m6BBBB623FAE015607BC34B0392745F5CC090540C,
	MMFeedbackFeedbacks_get_FeedbackDuration_mC4DD6375C212FC30F9DFEE1888698AC57CED8BC6,
	MMFeedbackFeedbacks_CustomInitialization_mE0C6C38D6B2353BAF2A4C79E8A970C5BB0E155DD,
	MMFeedbackFeedbacks_CustomPlayFeedback_mD164257FE6F7F463E5D8D56159F339D0D699149A,
	MMFeedbackFeedbacks__ctor_m78B0DAC1C5B34AE7F96C28F231DEC212064D8946,
	MMFeedbackFlash_get_FeedbackDuration_mCE75BB049C0EA50999985657B342FEADECB9BE16,
	MMFeedbackFlash_set_FeedbackDuration_mC4E55473B63888E80E30A99AB1E257E1225FD7F4,
	MMFeedbackFlash_CustomPlayFeedback_m730DF1CBD2A331DEF14F2AEB56193E8141D91F92,
	MMFeedbackFlash_CustomStopFeedback_mC29B7F2C109717CD65930E01C6FDD6604D52FF62,
	MMFeedbackFlash__ctor_mDD33BFB7BFC53C9B91020D7C980304D23F30B670,
	MMFeedbackFlicker_get_FeedbackDuration_m3E67EEDFA105D9DF46876A57AE2E4FFD14C4E30B,
	MMFeedbackFlicker_set_FeedbackDuration_m75098F331F62FF449F12C6FBDF897BC13F718A6F,
	MMFeedbackFlicker_CustomInitialization_mDDED4C37CB310F0E15B18B7EB95375D217B2C29E,
	MMFeedbackFlicker_CustomPlayFeedback_m90BABD0D01DC075064BE0FB9813F80700ECB799D,
	MMFeedbackFlicker_CustomReset_m2C9D415AAFCE67C8E5FDE44DFE9039E34B0CACE3,
	MMFeedbackFlicker_Flicker_mB6BB6EC8D51B04FB5ADDF24161A6A37F7D1B408C,
	MMFeedbackFlicker_SetColor_mF5F710072C6F51051A0CFFCE81B84509E4B096D2,
	MMFeedbackFlicker_CustomStopFeedback_mC7E355FCF799FB1B7C81B0BB392CDFC66C79E615,
	MMFeedbackFlicker__ctor_mDBE7014EB42C36C9499306DC9A7139650803351E,
	U3CFlickerU3Ed__21__ctor_m6F8921D8CC97152EB3E62B866EF935DA55CFE901,
	U3CFlickerU3Ed__21_System_IDisposable_Dispose_mF19AEAF1C7841AACCFCF9F8D1FD42FA43758541F,
	U3CFlickerU3Ed__21_MoveNext_m4B0C7815B4B19322A7EC47E63EE5B0CB87890493,
	U3CFlickerU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m722B53DCB38F58D5222B225654F18432ADC21387,
	U3CFlickerU3Ed__21_System_Collections_IEnumerator_Reset_m5F7BEC98818D9D640D6EA54BB956A0893B9364F8,
	U3CFlickerU3Ed__21_System_Collections_IEnumerator_get_Current_mAAAAD7521E2EF5896A54B3D93D3AE9AD7887BAC0,
	MMFeedbackFreezeFrame_get_FeedbackDuration_m6ADBBFE77144C965D20CAD6C1A48E72A65CE94BE,
	MMFeedbackFreezeFrame_set_FeedbackDuration_mDE35C1F80D6DBE8F65A6847558010131CE355D90,
	MMFeedbackFreezeFrame_CustomPlayFeedback_m97A3564570E9CA3AEF14FF559284189EEACB13FD,
	MMFeedbackFreezeFrame__ctor_mF96EFB23BB42266E070F88669631C4508B14E971,
	MMFeedbackHoldingPause_get_HoldingPause_mED6CF75663006193CE6C74316DECD71153C94C8F,
	MMFeedbackHoldingPause_get_FeedbackDuration_m88B8F60AB42640FF5826C9C3C10B1987665F7166,
	MMFeedbackHoldingPause_set_FeedbackDuration_mA8FDBCF907D95AA2EEAC4B1E9714D67F165C22DF,
	MMFeedbackHoldingPause_CustomPlayFeedback_m17606D5BA214761CADC9021EA85060BB5C51464A,
	MMFeedbackHoldingPause__ctor_m6533300D5B5BFFAEBF145D432EE2DD94A702B62C,
	MMFeedbackImage_get_FeedbackDuration_mFFC5DA6BF0FADDF551FC8670D1B9134DFB70698D,
	MMFeedbackImage_set_FeedbackDuration_mB1A12D73570BCD632B6EDD31196C808F0B010609,
	MMFeedbackImage_CustomInitialization_m38F907BC271B94EA078181AEAC1AF9B0257F63EF,
	MMFeedbackImage_CustomPlayFeedback_m3B92DF2751EA22B62BD1FF56267DFB61DA311653,
	MMFeedbackImage_ImageSequence_m0983D6404F04D6697C00C3845050BE0A7954A53A,
	MMFeedbackImage_SetImageValues_m7D856CF9BD723B045DC04B2832898505F617F37B,
	MMFeedbackImage_CustomStopFeedback_mA3B39BC8FD411B1CC7CB12D9C8D8F7B3B5D13ECA,
	MMFeedbackImage_Turn_m3B348627A750D65ED9DAD8CAA6ABCC78C8517433,
	MMFeedbackImage__ctor_mF136B20823F51E24DE223E8958629677A7B1DD2B,
	U3CImageSequenceU3Ed__21__ctor_m644622C038CDC21631A6C6F0AD1DF1A154116867,
	U3CImageSequenceU3Ed__21_System_IDisposable_Dispose_m31E455F7DC0F615B0A1D04491541DF105048CEDB,
	U3CImageSequenceU3Ed__21_MoveNext_m27D652A2525E052A66D871BB62B2AB00112CDDCA,
	U3CImageSequenceU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m245A51C921DFFDCE82A884049C5E1AFA177DAE65,
	U3CImageSequenceU3Ed__21_System_Collections_IEnumerator_Reset_m2CE5EDCC78D87D6B67626261522C724687F0CDE7,
	U3CImageSequenceU3Ed__21_System_Collections_IEnumerator_get_Current_mB276F1856BF5886B2517F23731BDE346A49AC1BA,
	MMFeedbackImageRaycastTarget_CustomPlayFeedback_m6F4BEF0A0C07AA74E96D21EF7A5D8633A0768E33,
	MMFeedbackImageRaycastTarget__ctor_mF21908F318EF17DA60F2200F0FE2DCD651336177,
	MMFeedbackInstantiateObject_CustomInitialization_m104AD8BAE9C39B03303E64649F3492F2F50EA79C,
	MMFeedbackInstantiateObject_CustomPlayFeedback_m87F40BC7878C61FC6D8AFFDBD4550CF7C16F2FE0,
	MMFeedbackInstantiateObject_PositionObject_m3EC28B536CFCBEF6802BBF1FCE5631969DF676D3,
	MMFeedbackInstantiateObject_GetPosition_mC19A3BC52E6F6AC0B842370B7157279BE71DA33C,
	MMFeedbackInstantiateObject_GetRotation_m0CD1FC5D4267BF2ADAD638396C0C271435510AD7,
	MMFeedbackInstantiateObject_GetScale_m5A5216D413FB073D7DA3BA100376E7EDE2CF878A,
	MMFeedbackInstantiateObject__ctor_m6E1E936D7CB750A3DFC500199DA39B0F9ED0C53F,
	MMFeedbackLight_get_FeedbackDuration_m085AC6856302EC9546A6BAE6E42C84CA4252DBF3,
	MMFeedbackLight_set_FeedbackDuration_m9F5537835C36D31E05711BCE10388BFA59F51EBA,
	MMFeedbackLight_CustomInitialization_m5359DEE0E5CF4FB480AEA0052D09D34DB4FB4D31,
	MMFeedbackLight_CustomPlayFeedback_m834DF756BDE0244ACD94EC2B989FC86AC095108E,
	MMFeedbackLight_LightSequence_m5228494B9651582CD9FD859979A28060D215DD30,
	MMFeedbackLight_SetLightValues_m8DABFA36687C341956703012B71E07DCC2A75467,
	MMFeedbackLight_CustomStopFeedback_m3942300B48AA6DEC4259CA4B323FBA1EBC8ED477,
	MMFeedbackLight_Turn_mA52FF854EE6435E9A4DF0701C2755D68F437F937,
	MMFeedbackLight__ctor_m6C909AC21CE83092C619DB4E5151D8C13DCF2444,
	U3CLightSequenceU3Ed__38__ctor_m59193416E1320E02C5F85794717BC51E397C9912,
	U3CLightSequenceU3Ed__38_System_IDisposable_Dispose_m95C894B6744861F940BDBE83F7B315B79BFCC385,
	U3CLightSequenceU3Ed__38_MoveNext_m5526294317C1B14E64CFB7269BC02F35E99E8C26,
	U3CLightSequenceU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4946412FC2EE2DA2AD890B76FA67726962209B06,
	U3CLightSequenceU3Ed__38_System_Collections_IEnumerator_Reset_m73F65ED932CA56C5D6FDE3D3B0E6F258BB9C3280,
	U3CLightSequenceU3Ed__38_System_Collections_IEnumerator_get_Current_mB3B9DEF4BFB983C67B7D4B34363D14BC6982ACBC,
	MMFeedbackLooper_get_LooperPause_m3A77F9CE20ACA7BA515DD5C73F4CE4ACF564C68F,
	MMFeedbackLooper_get_FeedbackDuration_m3AC17B24AF95EDA7932B2E738F402992CFB22129,
	MMFeedbackLooper_set_FeedbackDuration_m36C6098E75A6F5CA40ACA422EBB85B7ED3DA5AE2,
	MMFeedbackLooper_CustomInitialization_mA5E971822E1398AA20C47CF71CAB5DF13AC7D141,
	MMFeedbackLooper_CustomPlayFeedback_m3E2774EBB1BD68D9468841D38B39A9B23BA1671B,
	MMFeedbackLooper_CustomStopFeedback_mE5CC7C8EAD5B3E316786FC35774A13955DC28FC4,
	MMFeedbackLooper_CustomReset_mA94A67FAD487F2E86B30F968A16D442BB4FCA89E,
	MMFeedbackLooper__ctor_mB04DFE85B90D81C20E31A4260BF3E954ED4E0439,
	MMFeedbackLooperStart_get_LooperStart_mA32D2F44FFB40E1EF651F8C8585CFBCCE846A6FF,
	MMFeedbackLooperStart_get_FeedbackDuration_mC4F25969B5452C2078BC3572BF8A6C0FD6B446B0,
	MMFeedbackLooperStart_set_FeedbackDuration_mF75A934E9B85D68E874C55769FD8F9BB08F8F756,
	MMFeedbackLooperStart_Reset_m4BFD474E20A8C9B9902D917237762EEFD503219D,
	MMFeedbackLooperStart_CustomPlayFeedback_m9AD1BF88273A83AAC05C5A09AC5D5222F39B77F7,
	MMFeedbackLooperStart__ctor_mCB9E3DEAAB55F129A9D2BDCD70FC5281823106E3,
	MMFeedbackMaterial_get_FeedbackDuration_m57BFFA1D26F62DE393DF2751B686062FC8C31F4A,
	MMFeedbackMaterial_set_FeedbackDuration_mA199FFF43F1F885E8EF89B6B60EF6E4DE00DE721,
	MMFeedbackMaterial_GetTime_m9506396177743B0BF6F867E59E193E2E029533C3,
	MMFeedbackMaterial_GetDeltaTime_mA49CE814BC714C9E429B47CF2699C9DF2F4A1732,
	MMFeedbackMaterial_CustomInitialization_mF3034F1CEE4A776FF8D8FCC6DE629E6F36B48DC0,
	MMFeedbackMaterial_CustomPlayFeedback_m9DA98D450D9395738ED0C06D19009C9835343C22,
	MMFeedbackMaterial_TransitionMaterial_mA7C6FE1C24AC91C6993AA88689FE33CCDE4B5B1E,
	MMFeedbackMaterial_DetermineNextIndex_mA4EDD19E944C005038D381EA91DFBDBA00EF0685,
	MMFeedbackMaterial_CustomStopFeedback_m7CDD0D158CCBFDC86006FAC2B59C197D6EEC8D5A,
	MMFeedbackMaterial__ctor_mA1595E14B4DD3547C7AB09DB89659D1085DD2095,
	U3CTransitionMaterialU3Ed__20__ctor_m9FC4A446768F6604DC08CEFD597B44AA4625FC68,
	U3CTransitionMaterialU3Ed__20_System_IDisposable_Dispose_m16E93E1AE6ED44F90D813C1DE4621FB808E38075,
	U3CTransitionMaterialU3Ed__20_MoveNext_mBC5CF21EB3934BAFC097CE893B61D965F1857B52,
	U3CTransitionMaterialU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA51A692387A295AA63AE2D5182D2D8772C9697EB,
	U3CTransitionMaterialU3Ed__20_System_Collections_IEnumerator_Reset_m7596BED3EA32CF2B64D44AE3239859945E96CF68,
	U3CTransitionMaterialU3Ed__20_System_Collections_IEnumerator_get_Current_m13F75D2BDBBF60CF16B5D86C614EEE7542B1AE48,
	MMFeedbackParticles_CustomInitialization_m00FF0EF6DB7B1636CEDDE46C1E8DAE2B83206AE2,
	MMFeedbackParticles_CustomPlayFeedback_mBC5BA56FA0582F39E545E127F57BEC3BA14CFCFD,
	MMFeedbackParticles_CustomStopFeedback_mB464677A8AE9317B14A27FCDBEC4033B30E4ABCE,
	MMFeedbackParticles_CustomReset_m12CF1D8F8D576A64AF0A716876FED5CB9B3656F7,
	MMFeedbackParticles_PlayParticles_m2D0E99509DF2F7E51345BB092C6A6BAFC13D4CFE,
	MMFeedbackParticles_StopParticles_m4D7617404315C06B83B1E3EA7A96F30579FBDF27,
	MMFeedbackParticles__ctor_mCAFF0B8996D353951AE5F17C093F46710D7F6E7E,
	MMFeedbackParticlesInstantiation_CustomInitialization_m12F5BB99EF9F5FE07D7673A301CDD91CD6C55765,
	MMFeedbackParticlesInstantiation_InstantiateParticleSystem_mAB560135D6DA9AA6AE08243D742EC89BCB5A6BE9,
	MMFeedbackParticlesInstantiation_PositionParticleSystem_m0D754B07F551E25FFAD565515675916E4265493B,
	MMFeedbackParticlesInstantiation_GetRotation_m4B68F5C1235A00B01847F1231DBB8A7CFE55CCAC,
	MMFeedbackParticlesInstantiation_GetScale_m4D36DD1FA7716246FE9CDD3CDF6234E9A4EDCEED,
	MMFeedbackParticlesInstantiation_GetPosition_mDCF1D0285E232F4347F617334FB69C2D84964CB9,
	MMFeedbackParticlesInstantiation_CustomPlayFeedback_mA806CD6A024F422D76910262D15170F9ED7B87FA,
	MMFeedbackParticlesInstantiation_CustomStopFeedback_mBED162B0F8BC0354545A62D7741890A8444BE6C6,
	MMFeedbackParticlesInstantiation_CustomReset_m434E629E77047348F50E6023D17D1D2C69F321B9,
	MMFeedbackParticlesInstantiation__ctor_m9FC56BF9CB458C79EC516810E928538506258CBD,
	MMFeedbackPause_get_Pause_m5910BEF324A488BCC27427338A31A1D8DEE8D0A0,
	MMFeedbackPause_get_FeedbackDuration_mC3DCC8BA6C24B4537B48DC4FDE7EA3B756407237,
	MMFeedbackPause_set_FeedbackDuration_m2FC5BE236B07645665875B4306220E017109830E,
	MMFeedbackPause_PauseWait_m6D91157FCDD2AB1872BE0ED9760B201B0A72F318,
	MMFeedbackPause_CustomInitialization_mF17BED69AB8720429057AE47E0BEB39463108339,
	MMFeedbackPause_CustomPlayFeedback_m53318C83E28CBEB7FFF87385861642D1212C3BAE,
	MMFeedbackPause_PlayPause_m4517993403D86D98B927187460F8248CE905980C,
	MMFeedbackPause__ctor_m6D2446F76328923CB421EF492AC28057D9443E87,
	U3CPlayPauseU3Ed__16__ctor_m1D921774D1B3523F36E770BFFE3DEF0E9168CC18,
	U3CPlayPauseU3Ed__16_System_IDisposable_Dispose_mB5A11948942DBA4086A5005A12508498F3DF8765,
	U3CPlayPauseU3Ed__16_MoveNext_m890434F2361A450F5C115E5485B4F6995295E05A,
	U3CPlayPauseU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8D1B6475D31078E1AEE1729A075BA01226C00551,
	U3CPlayPauseU3Ed__16_System_Collections_IEnumerator_Reset_m679EC44C67BE9503BA41624580C7EAF0BE077A3C,
	U3CPlayPauseU3Ed__16_System_Collections_IEnumerator_get_Current_m6BEAE6E7FD360205EC5E5CFE0AC8EA18DDE34BE2,
	MMFeedbackPosition_get_FeedbackDuration_m41C0AC6ED63ED23F9F99B1DC7A300632FB234391,
	MMFeedbackPosition_set_FeedbackDuration_mA068AFF0FDA1FECC01BEC075C0A21C826581F870,
	MMFeedbackPosition_CustomInitialization_m508BAD11CBDBE402F6836901BD0FD9300765D636,
	MMFeedbackPosition_DeterminePositions_mB91310BB35D25B0423D5B89831A7028423B5FE57,
	MMFeedbackPosition_CustomPlayFeedback_mC9C87A5C1F12BFB7F91B2DA5296BDA6C55FEF5F3,
	MMFeedbackPosition_MoveAlongCurve_m12F14693C26DDE4DD83358F5599909FE92626CC3,
	MMFeedbackPosition_ComputeNewCurvePosition_mDBDCBAAF44F7F4C9E3B60B75C0E1DFA4F4CAEA72,
	MMFeedbackPosition_MoveFromTo_mFCEA35167012B6AA620CADD2B60A9AC08791E79B,
	MMFeedbackPosition_GetPosition_mB6D4B0CE2D68A4FA3BD0A3E78F47D2B6740B335D,
	MMFeedbackPosition_SetPosition_m3277ACE8ECA68A284CE918BC227CE0982C8E4B00,
	MMFeedbackPosition_CustomStopFeedback_m8746CB88773DB13B1C9972A624CD1958D34A57BD,
	MMFeedbackPosition_OnDisable_mF8BC49A5519EE3CADE74208EDED5CFD8163D6545,
	MMFeedbackPosition__ctor_m5F28D587A6B63A33FE9C66D44A52B54898AB9DE5,
	U3CMoveAlongCurveU3Ed__35__ctor_m73CE0900885E23DE43E65F74DAF231580CCF3AB3,
	U3CMoveAlongCurveU3Ed__35_System_IDisposable_Dispose_mA8FD281D0543F631E97D8F90E0CEAAC043196D1E,
	U3CMoveAlongCurveU3Ed__35_MoveNext_m1A44A040AC9DB7BC4A3764DF57B430DCE1402F74,
	U3CMoveAlongCurveU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCE714F42F97BA96AB7C550F6ABD3B257BF43937B,
	U3CMoveAlongCurveU3Ed__35_System_Collections_IEnumerator_Reset_m176344A6644E8EA051D3011378AB424DC3EABE55,
	U3CMoveAlongCurveU3Ed__35_System_Collections_IEnumerator_get_Current_m91D855D9E3FC86E3BE30C167E56AED1481D87508,
	U3CMoveFromToU3Ed__37__ctor_mCCFAF53BBE7674E5691149B59CA1A43453C20DE9,
	U3CMoveFromToU3Ed__37_System_IDisposable_Dispose_mDD539198B4CE9BA0494AAC611EC94A7FF5E9CFA2,
	U3CMoveFromToU3Ed__37_MoveNext_m9B50A388204C37EF4A88242C3657A960D0B304B6,
	U3CMoveFromToU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6C4C325373524F9B8CDB5385DB293E9F532E0BE4,
	U3CMoveFromToU3Ed__37_System_Collections_IEnumerator_Reset_mD107BBF36A7025D52B84AA9975B8D301D542E184,
	U3CMoveFromToU3Ed__37_System_Collections_IEnumerator_get_Current_m7F1913CD92901AE729C9B4147BBD4EB91E2395C9,
	MMFeedbackRigidbody_CustomPlayFeedback_mCAD1B28DC53EF25A0ADB2C5CA6CC7A43BA442E96,
	MMFeedbackRigidbody__ctor_m91B7797C1675B3D0AA605F76241FA17DFDEC1576,
	MMFeedbackRigidbody2D_CustomPlayFeedback_mE22D54348FB89D731F71902D58E69C13A4F5DE86,
	MMFeedbackRigidbody2D__ctor_mCA04A7BCB1E6394AFD04F8858F77E5F92A7CA6CF,
	MMFeedbackRotation_get_FeedbackDuration_m7881FB2C1D56B00C081EF28200963830C8308FEC,
	MMFeedbackRotation_set_FeedbackDuration_m66044F630340BB409D973814D4F87862D5658C53,
	MMFeedbackRotation_CustomInitialization_m659FFF5BE6A2BD2628F9FB617E22A455FCC8E28F,
	MMFeedbackRotation_GetInitialRotation_m562F84705695C03D837F68BB2DCB7DC056958ED4,
	MMFeedbackRotation_CustomPlayFeedback_m8C42E64D9250A125685F2CB1D8D62E18D265644E,
	MMFeedbackRotation_RotateToDestination_m2B22EE212D080C3A540CA27E0069427CF956C936,
	MMFeedbackRotation_AnimateRotation_m1EA58D02BD4C8CD3CE0CFFD07E4BE899710EA468,
	MMFeedbackRotation_ApplyRotation_mECE88B789AD15FB7E2F0568EFC956A66EB64C5BE,
	MMFeedbackRotation_CustomStopFeedback_mB753B09276633AF1493B38F0B96E038F6965B909,
	MMFeedbackRotation_OnDisable_m3F19222D3FE83D41DB524032BD24D9E89F13AEF5,
	MMFeedbackRotation__ctor_m8DCAC5F56CC12A414A8C54C94EE6375F73AC6326,
	U3CRotateToDestinationU3Ed__30__ctor_mCC46CEBFFE1476A781081BC250CC84FB65CC61DE,
	U3CRotateToDestinationU3Ed__30_System_IDisposable_Dispose_m5D1A91B4963E0372659413BC6D19365153245FED,
	U3CRotateToDestinationU3Ed__30_MoveNext_m79CCB5D39C6486360572EFE14A03BC4AFA8FA434,
	U3CRotateToDestinationU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5EC2B29A701A2D84E4DB32F0BDBDAE3B3FD33DF1,
	U3CRotateToDestinationU3Ed__30_System_Collections_IEnumerator_Reset_m7CF6C26AE7D8130E93AEF5389C6E750EAEE667A1,
	U3CRotateToDestinationU3Ed__30_System_Collections_IEnumerator_get_Current_m7DEE82F95D53FC3BBC90E29EDDD83B1314369D5C,
	U3CAnimateRotationU3Ed__31__ctor_mF3E893CACB9944A052592F7BEECBDB501B61903C,
	U3CAnimateRotationU3Ed__31_System_IDisposable_Dispose_m7F2C45B82A047FB954F23EB205684BD48ABB90AF,
	U3CAnimateRotationU3Ed__31_MoveNext_m9C354A2BC9F37E399F1DE837C204034CB2FAFAA5,
	U3CAnimateRotationU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m888EF09C54D09A71787652AB0A42624A8355E5A3,
	U3CAnimateRotationU3Ed__31_System_Collections_IEnumerator_Reset_m5D1D65CB9F03A92B9EA056CFF6CC20C948B92B73,
	U3CAnimateRotationU3Ed__31_System_Collections_IEnumerator_get_Current_m1171A16764B62EB16A7E67B16537AF01E3D49DBE,
	MMFeedbackScale_get_FeedbackDuration_mD192E2FB5146D2B4CE41FCA8FE71D6C8C40005A8,
	MMFeedbackScale_set_FeedbackDuration_m2B48177DBCF15502D51D95C2E27E6779DFDAE613,
	MMFeedbackScale_CustomInitialization_mC2B33DB4FFABAE791B3FEC4947D7CE9CFB3579CD,
	MMFeedbackScale_GetInitialScale_m321C3B873245FE15163CDCB421F7332F367D5E84,
	MMFeedbackScale_CustomPlayFeedback_m7FAB75C0501A3360E5C928AB1A586289B34870AF,
	MMFeedbackScale_ScaleToDestination_m1B7C547AAA613B43577AADB27787F3B59AD645CD,
	MMFeedbackScale_AnimateScale_m5F071C3F5B4524E99583DB09093F30BF6BF98F38,
	MMFeedbackScale_CustomStopFeedback_mC269CAE9630FA0CF44915468C44CC31D69453391,
	MMFeedbackScale_OnDisable_m475B15A332C8CD34DA8000F7FD6D353D83B6116D,
	MMFeedbackScale__ctor_mC5DD376174F264723C5031550BCDE3B4F9C7701A,
	U3CScaleToDestinationU3Ed__27__ctor_m20AD5414CB9ACEBCC297F2141593CC573B556A16,
	U3CScaleToDestinationU3Ed__27_System_IDisposable_Dispose_m6700A46FA41A84C399E845916B317BEF48EE8330,
	U3CScaleToDestinationU3Ed__27_MoveNext_m35CF66A5C133567640A0177FDBFC16A8745E9489,
	U3CScaleToDestinationU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF10EE5B9D9B06CB1A0D60A3F08C733895B5DDCF4,
	U3CScaleToDestinationU3Ed__27_System_Collections_IEnumerator_Reset_mC0BBEB91104B6BAC2EB1533BA8968EBC59FE0998,
	U3CScaleToDestinationU3Ed__27_System_Collections_IEnumerator_get_Current_m3096601618E872CF68B683982FBAB2308ADC209A,
	U3CAnimateScaleU3Ed__28__ctor_m1C239E3C280E0807E3354F199519D7A4F814F303,
	U3CAnimateScaleU3Ed__28_System_IDisposable_Dispose_m2BFA9F03D764363D583B4B0C84837239D23843F0,
	U3CAnimateScaleU3Ed__28_MoveNext_m992821804EBDDF78D317DC88E902E428EB1C20C6,
	U3CAnimateScaleU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5DB179F4A90E2A312BFF467EEE9C68618719349F,
	U3CAnimateScaleU3Ed__28_System_Collections_IEnumerator_Reset_m2E212688DF3F003D1986FD7C53997D5D3E000E19,
	U3CAnimateScaleU3Ed__28_System_Collections_IEnumerator_get_Current_m484F065959BFD4E99E4CEE8A7BE43A7FF5DD39DA,
	MMFeedbackSetActive_CustomInitialization_m54A71AD11A6C7EC8112CD88A90213DE57AEDE767,
	MMFeedbackSetActive_CustomPlayFeedback_m8BB480A010174F5EA2280AD6B6BACFD37E802F53,
	MMFeedbackSetActive_CustomStopFeedback_mCD37C9D4DD9E014E685ECA51D9C4C828BC92AAEC,
	MMFeedbackSetActive_CustomReset_mA2016A5084BDE2BE3F901E790AF4D355707D0023,
	MMFeedbackSetActive_SetStatus_mF949DFF6CDA19924F302104607639FDF6C8B9151,
	MMFeedbackSetActive__ctor_m1226C3AAB1D91840C41B713CC7B115D81E2F2A28,
	MMFeedbackSkybox_CustomPlayFeedback_mD1934B6D5AB4D7439761EBFC7A9B51E91B1D846F,
	MMFeedbackSkybox__ctor_m7B8EB2CDFD5B9C35BD4E48DECA3D2B10049941FF,
	MMFeedbackSpriteRenderer_get_FeedbackDuration_m324BCF93DD386DC91BB65CF0F2D316CF8F60E3EE,
	MMFeedbackSpriteRenderer_set_FeedbackDuration_m547EC9055B98895F3F4EEC4FE80767931BA4A2EC,
	MMFeedbackSpriteRenderer_CustomInitialization_mE10BC9839C6E9964790900A0828C5EECC1002FDB,
	MMFeedbackSpriteRenderer_CustomPlayFeedback_mE96183F492549ABEDC01E3150BDE000893F4E467,
	MMFeedbackSpriteRenderer_SpriteRendererSequence_mF738A5BD1D640F048FF872B475D89EC48768573E,
	MMFeedbackSpriteRenderer_SpriteRendererToDestinationSequence_m03A3886FC4F76CE098AFBAD101C238ED6D367BC4,
	MMFeedbackSpriteRenderer_Flip_m19558B95BCD31535848958BEEAF4DF11425997A6,
	MMFeedbackSpriteRenderer_SetSpriteRendererValues_m16C19E2B40EEB448FFB02AD53E5A0254DCC4E5F8,
	MMFeedbackSpriteRenderer_CustomStopFeedback_mA2AAACF8073A43B0E6896F3115E60B6DB9A4A54D,
	MMFeedbackSpriteRenderer_Turn_m4AD609EA6ED7C5FB83E069A83A332DF21D91CFA6,
	MMFeedbackSpriteRenderer__ctor_m606A704AF847E17F5D7AC3B9E4C0D2970212E9D3,
	U3CSpriteRendererSequenceU3Ed__28__ctor_m5CED94C7E93BEDC0C92FE05CC8868138DE1B4FB1,
	U3CSpriteRendererSequenceU3Ed__28_System_IDisposable_Dispose_m6A003D74EAC1815F5F6BBAAE0AC9B1D3B1A36E93,
	U3CSpriteRendererSequenceU3Ed__28_MoveNext_mABB226DAE4F8973F112C70E0846C5EE61FAF5345,
	U3CSpriteRendererSequenceU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m590A28C1FCA91B678282A2A7AFDC00693FC16364,
	U3CSpriteRendererSequenceU3Ed__28_System_Collections_IEnumerator_Reset_mF50832E244BFC055EA2D6F328B7501A8AE9B32D2,
	U3CSpriteRendererSequenceU3Ed__28_System_Collections_IEnumerator_get_Current_m7B4468051BB25DEBB4F1B9B0FA9DFA681695A592,
	U3CSpriteRendererToDestinationSequenceU3Ed__29__ctor_m677734AF7036B92816613F1AF39DE8EA479C8FFB,
	U3CSpriteRendererToDestinationSequenceU3Ed__29_System_IDisposable_Dispose_mEBAAACDA5E0CA3667208EFC36C4640A408987673,
	U3CSpriteRendererToDestinationSequenceU3Ed__29_MoveNext_m5ABFDA560D11A7E5282273C9750DF01C3360EA93,
	U3CSpriteRendererToDestinationSequenceU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m54D8AA907F7B9C6FA580F4C1256E5FC96580B78A,
	U3CSpriteRendererToDestinationSequenceU3Ed__29_System_Collections_IEnumerator_Reset_m2EF3DE0D7B3A99930B0011629CC3B75C8EBF5FD7,
	U3CSpriteRendererToDestinationSequenceU3Ed__29_System_Collections_IEnumerator_get_Current_m5475D85BB93572BFDC9E65885619587D84C51A98,
	MMFeedbackText_CustomPlayFeedback_m7FE53CE564718E4A1D629418C43B5D3E40167702,
	MMFeedbackText__ctor_mFCAABFBF5F7501DFBE33DB025EF48D63081E5BE4,
	MMFeedbackTextColor_get_FeedbackDuration_mD825CB737A85B59C4A3916252B187A3DAEED21BB,
	MMFeedbackTextColor_set_FeedbackDuration_m2D7ECB3FCC383E7A182007A4E28A477460DBCEE5,
	MMFeedbackTextColor_CustomInitialization_m5CE7B4E3FF185AF97A52996BB5F2C4E6977350E9,
	MMFeedbackTextColor_CustomPlayFeedback_m10D99BCFC5BDB6AEF41B6BBD272237C3BB4A1581,
	MMFeedbackTextColor_ChangeColor_mDDB2D7661C71753532D5E9312F196DA43C508C9F,
	MMFeedbackTextColor_SetColor_m115A7ED17F04F544E022BE25102AAAC4A6529FAE,
	MMFeedbackTextColor_CustomStopFeedback_mD616E56451D6307660634DCB9F4B6F76FC712B5C,
	MMFeedbackTextColor__ctor_m5C0308514E7B51D038918442B1652E55D1393023,
	U3CChangeColorU3Ed__16__ctor_mAEEDFCEDB77C2150E7D70A91FFEA167B5383E470,
	U3CChangeColorU3Ed__16_System_IDisposable_Dispose_m26902A0ED9D0BB0B1FF26BCFDBDEB73A9F716EBA,
	U3CChangeColorU3Ed__16_MoveNext_m317400CCA4A483386CEAECFC00B67603E979B7DC,
	U3CChangeColorU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB36B8CBE1719FB3A571C69B0DCE6C870643CA94A,
	U3CChangeColorU3Ed__16_System_Collections_IEnumerator_Reset_m969E98EA6343C9106E80F897DC0AF09B84FC47D2,
	U3CChangeColorU3Ed__16_System_Collections_IEnumerator_get_Current_mBEC1702CB943CA807D617A3BFE096C30B4DB20C8,
	MMFeedbackTextureOffset_get_FeedbackDuration_mF0D0DB2A3393E9769E60757160554AE21F52410E,
	MMFeedbackTextureOffset_set_FeedbackDuration_m16079FD3AA2DA39564CAAB62DDFA2A9519621879,
	MMFeedbackTextureOffset_CustomInitialization_m3551D9E9A3CDB427AA52CD19DB7C854823C359F6,
	MMFeedbackTextureOffset_CustomPlayFeedback_mE68EF79345A059F929C570F8C03AE1577B5EE35D,
	MMFeedbackTextureOffset_TransitionCo_m7C3891143737CD7F59D24A793CE9823395F036CA,
	MMFeedbackTextureOffset_SetMaterialValues_m2489F1D122936609235661BCAA8218F86D42E9DC,
	MMFeedbackTextureOffset_ApplyValue_mCA93FD39A666C86DF9FDD5E63CEC045B2104A3B6,
	MMFeedbackTextureOffset_CustomStopFeedback_m9AC036542FC6AA757CC5B2117721FD0A417E803F,
	MMFeedbackTextureOffset__ctor_m230DCB60C586E982353FEE1B9C2EEEF63DE13AB2,
	U3CTransitionCoU3Ed__23__ctor_m118C61E6A101AB2A30303CA758644F6231300AA4,
	U3CTransitionCoU3Ed__23_System_IDisposable_Dispose_mF19802027AFF49B7F7590372E2A24DAB268DE1F5,
	U3CTransitionCoU3Ed__23_MoveNext_m3110D6137A1A672398CD73FAB676524D9B38224F,
	U3CTransitionCoU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5771D7B39B6227AA30E8375F5016E400C8787877,
	U3CTransitionCoU3Ed__23_System_Collections_IEnumerator_Reset_m173232904CE6EE9E8B9A24CA09924933B4A69EB3,
	U3CTransitionCoU3Ed__23_System_Collections_IEnumerator_get_Current_m525409B35B1FC006C52176178684B7C6F851E7CE,
	MMFeedbackTextureScale_get_FeedbackDuration_mB4C7DA5A9B2F417455B6F9A11763F25D73DFC03A,
	MMFeedbackTextureScale_set_FeedbackDuration_m12002AC1146CB904AA784F45840DA1351F4CF84F,
	MMFeedbackTextureScale_CustomInitialization_m38BA70129DDEC029E27845B19E2FEFDC37766A7F,
	MMFeedbackTextureScale_CustomPlayFeedback_m3F8C978C70E75D361A5864034D35F4A66B739A6E,
	MMFeedbackTextureScale_TransitionCo_mC83BA175162BE4913418AD1FFD66E6C9AA6E1B8D,
	MMFeedbackTextureScale_SetMaterialValues_mBFC6F808A400E8EBDB8CD7ADA297047146EF67A1,
	MMFeedbackTextureScale_ApplyValue_m21D091A473BAFE19375DD459966DEF2769B6C790,
	MMFeedbackTextureScale_CustomStopFeedback_m5956907F4BC04F19385E1593CC6CA0A5C80333BD,
	MMFeedbackTextureScale__ctor_mE5E269DDAB55978411C00BA0D6415B5829323D17,
	U3CTransitionCoU3Ed__23__ctor_m0FE0498F5EB529913C30136DD7F6F714CECA1D71,
	U3CTransitionCoU3Ed__23_System_IDisposable_Dispose_m0372256108BCE4DE0D8DE082040C5E1E787572F0,
	U3CTransitionCoU3Ed__23_MoveNext_m23AD41489E7A06BE03C4342F32FAB61FD48B7B0C,
	U3CTransitionCoU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6B6EF4DC83ED6F2C59BC25D852874985FB7E97BF,
	U3CTransitionCoU3Ed__23_System_Collections_IEnumerator_Reset_mC964142325F1AAE3A13C8649811B915E38457513,
	U3CTransitionCoU3Ed__23_System_Collections_IEnumerator_get_Current_m19D31A8F86E0C1D0354BFDE36E4BB3A1D845AC38,
	MMFeedbackTimescaleModifier_get_FeedbackDuration_mD90ECAB42E8D47D0E6CE7F2196736ED33116C180,
	MMFeedbackTimescaleModifier_set_FeedbackDuration_m2888730154E9DF10E5811F3A981CF001DB07AB8F,
	MMFeedbackTimescaleModifier_CustomPlayFeedback_m99557E03A071E07E332A2412DF157A5CBA94DCC9,
	MMFeedbackTimescaleModifier_CustomStopFeedback_m3D95E41AD8E00283BA4172C4B32F602DA24443B1,
	MMFeedbackTimescaleModifier__ctor_m1B9CB06CBFFED0B3A4A80503816855648C5BEDFE,
	MMFeedbackUnloadScene_CustomPlayFeedback_m86FC4437ADBB4F3B4DDA897A51C196261C3D48B0,
	MMFeedbackUnloadScene__ctor_m5E66BEBD91A1FB5AD62575A604D10DA90D3EA8ED,
	MMFeedbackVideoPlayer_CustomPlayFeedback_m72F88568E9DF3CE4BA1F01CFB59A0D1ED386FC3F,
	MMFeedbackVideoPlayer__ctor_mF9999EABAAED2522D54918353DBCAC19CECDB1C6,
	MMFeedbackWiggle_get_FeedbackDuration_m639AEE55CB43600F69B9AE3CE73BF51FAE2619E7,
	MMFeedbackWiggle_set_FeedbackDuration_mF04D8C6D1051C35318C78B7E3B3DC8BB84392594,
	MMFeedbackWiggle_CustomPlayFeedback_mB019536151806DC35BE6DB5306BC026579B80FBA,
	MMFeedbackWiggle_CustomStopFeedback_m38268E9F35BB842D30F06E7EBC1F9F280F63FA11,
	MMFeedbackWiggle__ctor_mE6CF6CB28D2302DD2B1D3559CA9E2FDCE93C6249,
	BlinkPhase__ctor_m4A0D848606699F88E7F9BD1FB3E8515DE95EFFEE,
	MMBlink_GetTime_m46BB0E2E4A0F9639F242EB2C6F9944EE16FF066E,
	MMBlink_GetDeltaTime_mBAD5B03B691DD2D59B60BDE9DB063211964C2D4A,
	MMBlink_ToggleBlinking_mCB6DC265B2D46CC0724389116BDCA5AAF12F2DCA,
	MMBlink_StartBlinking_m2AFD839653684F6EFB893EAA93435335103DE235,
	MMBlink_StopBlinking_m6C5F0F0F87D9518093C4DDE75EEE0F83FEAF2F34,
	MMBlink_Update_m4780E237065D305D0F03A2CD780F65D1BC8F3EB3,
	MMBlink_DetermineState_mC41D1B6DCA9A12E5BCF3E689F6241C0D929DA32C,
	MMBlink_Blink_m37D42DCD20EA180E37633D263E8A433F0070E0FE,
	MMBlink_ApplyBlink_mCE21009B1C6ADCB845AEB9AAE0B6AE809AE8D314,
	MMBlink_DetermineCurrentPhase_mADBEAECE3DE01B6EE806D8D604AA2D18880E4D74,
	MMBlink_OnEnable_m189E886D64927989F21D012E078D18A396EADA0E,
	MMBlink_InitializeBlinkProperties_mC8CDC4D6DF0E1FBAD3676A1F75FF102697C7A057,
	MMBlink_ResetBlinkProperties_mA68C5F36BEDB839F570297A79C572E01EC3C5128,
	MMBlink__ctor_m919930E7CD8359A34FCFFABC3C5BE03452064217,
	MMAudioSourceSequencer_OnBeat_m8951E2C4FDA87E937591710532EB57C8C36F9318,
	MMAudioSourceSequencer_PlayTrackEvent_m39214658688549242236EA9C0171E5097315B247,
	MMAudioSourceSequencer_EditorMaintenance_m22B8A5EB0935EA8AB6BC9AA1AADEE0181E9A5BC4,
	MMAudioSourceSequencer_SetupSounds_m100825BFCFC7A8BB10A7871104C7F18367C52B79,
	MMAudioSourceSequencer__ctor_m3D983FC0B191A6303D93806318BA60C50C24AB65,
	MMFeedbacksSequencer_OnBeat_m8B64E0DE28F84CFA137F869D20E31FAE44C7642D,
	MMFeedbacksSequencer_PlayTrackEvent_m3B5D78FAE8B99A4B03991DF1A5FC9C0AF97A552E,
	MMFeedbacksSequencer_EditorMaintenance_mF81BF4D583A20AFA1EF24F3F57B750FAD85000D2,
	MMFeedbacksSequencer_SetupFeedbacks_m39429891536A7E035020229494A283186457C70D,
	MMFeedbacksSequencer__ctor_mD76AD981418A355DD89B94F2DC0B0D907307E0FD,
	MMInputSequenceRecorder_Awake_m22641C15913E7A82136119DA57423911EC4C2CE5,
	MMInputSequenceRecorder_Initialization_m47BF0E0D03725316E2B1C74430BA0052C919BFC9,
	MMInputSequenceRecorder_Start_m7C60B0C8020999C81C62813B4F276738869812A7,
	MMInputSequenceRecorder_StartRecording_m6596C98339CD208C5FC4A3F3E2DBC13D4C5DC669,
	MMInputSequenceRecorder_StopRecording_m76994357E84969793F9C44433169AF0031D28761,
	MMInputSequenceRecorder_Update_m6D5DC2658C024B564391614C961D49B296E50AB2,
	MMInputSequenceRecorder_DetectStartAndEnd_mE6F87A8D290822081C79F0CEF4801E41B1690C1D,
	MMInputSequenceRecorder_DetectRecording_mF55B0AB9FAD5B9DEABCB6CA3756108A0CF3A21AB,
	MMInputSequenceRecorder_AddNoteToTrack_m0C1DE4F57454AACD9DB6EFE082E78F8611ACC26F,
	MMInputSequenceRecorder__ctor_m02DFF8CF878BBF1782EC1C28997765B2C7268E83,
	MMSequenceNote_Copy_mF6124B2041EEE4C7C9D8432F0940734788E7F990,
	MMSequenceNote__ctor_m1B031E1E53135020C92795164BE72B91649ABD3C,
	MMSequenceTrack_SetDefaults_mFD45389DC9DA66162387F810BE42D889E20F5813,
	MMSequenceTrack__ctor_m38E01455A5630B4A20371F474F015A5A4B414C47,
	MMSequenceList__ctor_m420889EF0B48A1390C5583746851AC973DEE7DCF,
	MMSequence_SortByTimestamp_mD47E391798136023D51DD83F52661B418BD4A8A7,
	MMSequence_SortOriginalSequence_m3B5D1C2804EC58B3C6C7259B99B72DC0ADB80156,
	MMSequence_QuantizeOriginalSequence_m7CF80B6C020C9EF66E97BC0FA5EB47D034D32032,
	MMSequence_ComputeLength_mFB384860819AB5AE18C4A20DEA38284BAFE2E873,
	MMSequence_QuantizeSequenceToBPM_m75C1E17EB903D32A7AA19D8D9F51FE826E3040D2,
	MMSequence_OnValidate_mD105EABBC9DBBF88324D0C1B2D58634C75F4D722,
	MMSequence_RandomizeTrackColors_m04AE9285F7D4338C71FF179DB92192478B4727FC,
	MMSequence_RandomSequenceColor_m8D4C9849F03B2853CBA287D6A926C726C506E342,
	MMSequence_RoundFloatToArray_m926E105D8D4BD3FD3B0258E200B36223F7568A10,
	MMSequence__ctor_m5554755ECE94AEB5DC4E02A83C17B27A34216324,
	MMSequencer_Start_m0445C4FE65D88B4115471131D220C964D956E8A0,
	MMSequencer_Initialization_m3C23705E9DF37B94D276D1B376F270F9773B4904,
	MMSequencer_ToggleSequence_m3DD0ABB5A43AE2E6A0A5F42F88571F89B15EC604,
	MMSequencer_PlaySequence_m4040CD4784974CC3B854A9C354F358DD27873451,
	MMSequencer_StopSequence_m878E50179C42EDF8CF981F604F73940F79A3828A,
	MMSequencer_ClearSequence_mF7A7BF22EF4A27F6FCEE752574F31628B39EFBF5,
	MMSequencer_Update_mBD5467DDD019A6CB62EF7CBD4A6661ACB8AB9426,
	MMSequencer_HandleBeat_m4F4A38DBB7D6C4A0E62CCFC4A14E0B114AB01123,
	MMSequencer_PlayBeat_m9E4B52DAC7AAF7697EE7754D2DF75AAF7DFD3BED,
	MMSequencer_OnBeat_m0BA155C1FC1CEDC478E2D91DF734572571452C12,
	MMSequencer_PlayTrackEvent_m91F2B589A901C3564B1117141C2D8080D7462C67,
	MMSequencer_ToggleActive_mD7F8FB6D6431B78F445148F36990415780CBCD3B,
	MMSequencer_ToggleStep_m559BE4E7BEDAE45CA24EDCB25D39D321A8B8334E,
	MMSequencer_PlayMetronomeSound_m645F259F5BB924D71E47DD488ADD6D8F2794AC2A,
	MMSequencer_IncrementLength_m7A6582B0A7FC915BED67CEAED6C52CD7CEED05F6,
	MMSequencer_DecrementLength_mC199F871B876E27130D23F4F210EFEAA6B0E2CDC,
	MMSequencer_UpdateTimestampsToMatchNewBPM_m8A0F609B6AD92E64DF5F580636E486EEB95F0933,
	MMSequencer_ApplySequencerLengthToSequence_m0304CB9034100019D04BB40B8CC9F7FD5AA81816,
	MMSequencer_EditorMaintenance_m61BCFA1D02114F82100B56495A887C7BA6B4FA95,
	MMSequencer_SetupTrackEvents_mEEA673117D19AD6A6616540DC33A3522B80EED3B,
	MMSequencer__ctor_m73E192753C964CFAE94273FDE0B4D14A431A7D80,
	MMSoundSequencer_Initialization_m7362A81C22635CCF58B5C99760D31F2EE3E2C562,
	MMSoundSequencer_OnBeat_m06715CF3BB3FE84EFC25ED3712428A2FD8C53087,
	MMSoundSequencer_PlayTrackEvent_m5D5C5D3A59F44DFA29F67D2F52007FC880480192,
	MMSoundSequencer_EditorMaintenance_mBD2625DFB1C2531CA6C8075F2017E58AD8A6C311,
	MMSoundSequencer_SetupSounds_mCCEED7B0E86E5B10A555BE420F1992DFD416E5C8,
	MMSoundSequencer__ctor_mFA46E78077FD0D4C507BD8F0E9F42B8E8C2F848B,
	MMAudioFilterDistortionShaker_Initialization_mE710DCE40D385E9556F1A47414E7A7A2D6EA5979,
	MMAudioFilterDistortionShaker_Reset_m7A3875FCA4ABB1E76C6631F7044FFFD721E9B7A6,
	MMAudioFilterDistortionShaker_Shake_m35A3DA92CD4AD82FDC6DC5FCFBF9621879282F03,
	MMAudioFilterDistortionShaker_GrabInitialValues_m078DE073A1876046B39CCD7DD3F85434AAA3D7A2,
	MMAudioFilterDistortionShaker_OnMMAudioFilterDistortionShakeEvent_mF7A17F48DB58CD7F2908F4226B7E21AEBCC6D456,
	MMAudioFilterDistortionShaker_ResetTargetValues_m3042C09036763D36134CF76417EAB518384014DC,
	MMAudioFilterDistortionShaker_ResetShakerValues_mA60C7F44011116F27AB870313BCA9EBD19263313,
	MMAudioFilterDistortionShaker_StartListening_m7FC1F2625CE2E6580C6DBAC9BEF213D64BD67353,
	MMAudioFilterDistortionShaker_StopListening_m30333FAB7C770F28F1E84ED02C17E2D041957B93,
	MMAudioFilterDistortionShaker__ctor_m251114A3108B17CE09265BBDAB4032DDD860F244,
	MMAudioFilterDistortionShakeEvent_add_OnEvent_m6C1B74957FF95E2900EAD4E47D876404BFD04950,
	MMAudioFilterDistortionShakeEvent_remove_OnEvent_m5AD68F71CC37FF9A72945553939E0FFCE033A046,
	MMAudioFilterDistortionShakeEvent_Register_mE0524B8DCE6081373BE4BDDC706B80F560A7CF2C,
	MMAudioFilterDistortionShakeEvent_Unregister_mB220C8C7E04F5F374163EFC1892F2080BFAE0D18,
	MMAudioFilterDistortionShakeEvent_Trigger_m756B65068BEA85145054A128640AF6A92A23CA55,
	Delegate__ctor_m10C935A72D2C159510943B7D7ADA0FA452A58879,
	Delegate_Invoke_m0C10AE31C46BC1458427ACD9CC039977FF162893,
	Delegate_BeginInvoke_mA4760E135B85C0F5E249AB5FA852FB4ED0D191C9,
	Delegate_EndInvoke_m67C2C5F68F0CB46B362FD5B9FBD1BFCE5BD863A6,
	MMAudioFilterEchoShaker_Initialization_m7C5217014405267443613FFCB7ACF2A01E5DE08F,
	MMAudioFilterEchoShaker_Reset_m871F0864025C77CBDA4253A603B0850C78ED8641,
	MMAudioFilterEchoShaker_Shake_m4E065851A0F9064A15A3EB2BA7231216EFC79FE3,
	MMAudioFilterEchoShaker_GrabInitialValues_m3C9F29EE64E76BC2182A27D1CFB59C318D840BD1,
	MMAudioFilterEchoShaker_OnMMAudioFilterEchoShakeEvent_m13F094D59FE70EB1EC005D51361316AA0A3F2059,
	MMAudioFilterEchoShaker_ResetTargetValues_m314D60B5E955219CB22F39DD561D3F4C094EC848,
	MMAudioFilterEchoShaker_ResetShakerValues_m5CE5AB6CE3C8573DA972256FA58443F2A2441CAB,
	MMAudioFilterEchoShaker_StartListening_m19636A6B3375C2DB42C4D7E3A86817F2225A5112,
	MMAudioFilterEchoShaker_StopListening_m602F082149CAF92E60DC35A173E7D1684C406FB6,
	MMAudioFilterEchoShaker__ctor_m6032A29ED960E7095BC83FA79537D4D0B916F780,
	MMAudioFilterEchoShakeEvent_add_OnEvent_mDD36188DA056DC56EE6E7F27E91C6EEF7B15A233,
	MMAudioFilterEchoShakeEvent_remove_OnEvent_mB16611A4392C12100D8D1D401565423553F32B5C,
	MMAudioFilterEchoShakeEvent_Register_mABEF207986F8C870C85C010020E80BF4765490E7,
	MMAudioFilterEchoShakeEvent_Unregister_mE3377C0112225C70348D589D2F8364F37F125239,
	MMAudioFilterEchoShakeEvent_Trigger_m56076DD47C6A4A3545578438A7121E5984F2002A,
	Delegate__ctor_m37B81E05E289381139F9F23A570DA5EC826B179E,
	Delegate_Invoke_m48225124D486016751B52E0EE94757CE11910838,
	Delegate_BeginInvoke_mE48F00BECB81EC74442582E52460E7FA68A489CD,
	Delegate_EndInvoke_m56C41DDDEC461DC85062C790BFD711C5DE1C8574,
	MMAudioFilterHighPassShaker_Initialization_m2E87A428DA188619CA741A08A741AE1E5EB5C591,
	MMAudioFilterHighPassShaker_Reset_m94C8833A3B59CFF193802D2DF5FE19034F66BE4E,
	MMAudioFilterHighPassShaker_Shake_m0CBA31100114E51CAA8C9784E813957BE0E805CD,
	MMAudioFilterHighPassShaker_GrabInitialValues_m2B34CD9356F1FA8F8178A4272E2E132D3CC37564,
	MMAudioFilterHighPassShaker_OnMMAudioFilterHighPassShakeEvent_m7036729E0054DFF35647D0FF328BD27D4AD44E45,
	MMAudioFilterHighPassShaker_ResetTargetValues_mED5F19FB3B4C30DFE5FA8FA28D817EEF44647809,
	MMAudioFilterHighPassShaker_ResetShakerValues_m150313C1D77F18B110B96FBA4C573556829418FA,
	MMAudioFilterHighPassShaker_StartListening_m59CE3B8CB1E73E6081BC4AF92933550DB28B18A1,
	MMAudioFilterHighPassShaker_StopListening_m9DBA88A3EA69F29DB08D8BB1FA08126A7AF91BF1,
	MMAudioFilterHighPassShaker__ctor_m08E5BEC3BCB71093303A69E12A8879314A2E26BC,
	MMAudioFilterHighPassShakeEvent_add_OnEvent_mAD684D0BB67097F77B96F5C498B1A81D251EC59E,
	MMAudioFilterHighPassShakeEvent_remove_OnEvent_m5197485E8EB4C86E447EC66A50BAAA577EE492F5,
	MMAudioFilterHighPassShakeEvent_Register_m7C24282588794DDEB79271870B632F8C88987408,
	MMAudioFilterHighPassShakeEvent_Unregister_m31C5C6969A4E2A2F036D1235E13182BAEA15A818,
	MMAudioFilterHighPassShakeEvent_Trigger_m150A1071549607CFC7E1BBD5149B59BEF1377048,
	Delegate__ctor_m0B2EAD031B6281232A7A58A895784CEA98B26614,
	Delegate_Invoke_m1738021A9AE7A06EFB1F550DF8E3C9C385CC8EAA,
	Delegate_BeginInvoke_mAB796E02D12E192E000C8C88913705C132EE9BBA,
	Delegate_EndInvoke_m6D0D22074C71B8B8E8A0F132C4DB93935EF07494,
	MMAudioFilterLowPassShaker_Initialization_m2E793D75F7E787C9F2273F71AD8D06C74B7B4997,
	MMAudioFilterLowPassShaker_Reset_m6D0719CFD71FAFA30467F096778E1C3029076F20,
	MMAudioFilterLowPassShaker_Shake_m2C533E6861D15016E080DE67C7C09335FC0DA1F3,
	MMAudioFilterLowPassShaker_GrabInitialValues_mE8382F5E0128F77D25EAE0AAA916B34D2077DEA5,
	MMAudioFilterLowPassShaker_OnMMAudioFilterLowPassShakeEvent_m08B11D0D84BD46E2CFFB423B5C51A5020E8F35A3,
	MMAudioFilterLowPassShaker_ResetTargetValues_mFE281D35CC22B86DFEB458426637E9D555C3CB19,
	MMAudioFilterLowPassShaker_ResetShakerValues_m4663225F435918B928C5FF8B705B7524D96B78F1,
	MMAudioFilterLowPassShaker_StartListening_m7B3D4AFFAD56DDBEE564224B89ED2B5791993036,
	MMAudioFilterLowPassShaker_StopListening_m5CF3C06858DF996DDBB3E8036076330DF6DDB1F5,
	MMAudioFilterLowPassShaker__ctor_m0EE142DB2910EEB9A1834852BED16D71F3009A14,
	MMAudioFilterLowPassShakeEvent_add_OnEvent_mFE8F76ADD3D5D406E5863959BC3CA5E20B3555A7,
	MMAudioFilterLowPassShakeEvent_remove_OnEvent_mC1C6CE2AC96209549E544A04F9C0317B50283D27,
	MMAudioFilterLowPassShakeEvent_Register_mA8A11B3DD7E789A7F663BB4E22CFE4644FF98CEF,
	MMAudioFilterLowPassShakeEvent_Unregister_mD9ED361A041B97CF18A6F465CEB8DBBA0AAF3406,
	MMAudioFilterLowPassShakeEvent_Trigger_m2505BF9F41DA64930359E7BF763AA4BBE7AE3F4B,
	Delegate__ctor_m659A9911D98B00A2A58F4EBDE62162BB893B599D,
	Delegate_Invoke_mD7D755969284F55FC1120072B0AA69002BB90A75,
	Delegate_BeginInvoke_m8B1163F05337DA777C99C728AC0D092452B20241,
	Delegate_EndInvoke_m1F1C106E4920A1EB37E282E1F2C5AF726FCC15BA,
	MMAudioFilterReverbShaker_Initialization_m08739AA15E88EF7379EBB0417FA38535C244FA04,
	MMAudioFilterReverbShaker_Reset_m37A7729AC51193371D8F79FCCB38B5040DBE977D,
	MMAudioFilterReverbShaker_Shake_m698D8F6319B7EC1E5A0E0F7E189235C94D493699,
	MMAudioFilterReverbShaker_GrabInitialValues_mDECDBDDBE90D55F74D6B7111F794E2F99AA26212,
	MMAudioFilterReverbShaker_OnMMAudioFilterReverbShakeEvent_mAE9FDBE55B1F0A8C782BA894714328A1DBBF8B06,
	MMAudioFilterReverbShaker_ResetTargetValues_m9FF0E6CE5A24A53AFFCFC10790A6E97787ECA9F9,
	MMAudioFilterReverbShaker_ResetShakerValues_mC7345FCABB0CED81B30AD95E0B22E445323FD728,
	MMAudioFilterReverbShaker_StartListening_m5C3D0C6F78D1E96D8B56EF68A852E9574B917BC5,
	MMAudioFilterReverbShaker_StopListening_m0FCE70E0C99ED0328CFD26ECC0016F3D585083FE,
	MMAudioFilterReverbShaker__ctor_mB3C4E11D7D117122077E164A434208D8287A404C,
	MMAudioFilterReverbShakeEvent_add_OnEvent_mE7E00540BC9476E666298002E23E56A400C4E423,
	MMAudioFilterReverbShakeEvent_remove_OnEvent_mA94664A791C5A129C8C64E1D87346D53532999F1,
	MMAudioFilterReverbShakeEvent_Register_mA25306BB4583BFF410CD121DBB8A047AF8B711DD,
	MMAudioFilterReverbShakeEvent_Unregister_m352ADA01B96DE9EF281B0AB0171C4D1118FDDC17,
	MMAudioFilterReverbShakeEvent_Trigger_mAB175A79779BBD4A6C3019B762CA667E85422FED,
	Delegate__ctor_m00D6E3E71770FC6FB825648AA04713C12BD6AEBE,
	Delegate_Invoke_mB3FE8D5087587F521631032D88DEEB6E9D757FBE,
	Delegate_BeginInvoke_m35B9359AC8A3915A9EA34239BE48D54559713E7F,
	Delegate_EndInvoke_m0BF01F522F3F851438D6F3385429938DA926752B,
	MMAudioSourcePitchShaker_Initialization_mABC21C431652EAC31454968FB667DF499D4C404E,
	MMAudioSourcePitchShaker_Reset_m31F4BE0C67B90BD9DFAF10EE295690758A0B0FAA,
	MMAudioSourcePitchShaker_Shake_mD24EB294C6CA37F3839E4D4970B8556B99C88F69,
	MMAudioSourcePitchShaker_GrabInitialValues_m45E4FA1277114EF4EC73A7AB18079F2C3027DA29,
	MMAudioSourcePitchShaker_OnMMAudioSourcePitchShakeEvent_m1E03D63FED3C6DF0A13FD5353397615D6FEB9963,
	MMAudioSourcePitchShaker_ResetTargetValues_m377F786B3FFDAD51A08B9DF7E081ECD5B6A9ED67,
	MMAudioSourcePitchShaker_ResetShakerValues_m7AB45315CCD9A23C6E4DC3D2232595D971944F4F,
	MMAudioSourcePitchShaker_StartListening_mB87F2A18EA33A847F268CAC2C84141F2BBFA5D16,
	MMAudioSourcePitchShaker_StopListening_m0EF1E7EC2EDC478D6FA52E11A09FBD69B911C694,
	MMAudioSourcePitchShaker__ctor_m5856690EE3A6618C38DFFCFEC81866F46DF0888C,
	MMAudioSourcePitchShakeEvent_add_OnEvent_m4C43FC4750F12AEF149F954EE808CA45FB75AF2C,
	MMAudioSourcePitchShakeEvent_remove_OnEvent_m1298F190E6797D72FBE726B942E95B8A3E6575A4,
	MMAudioSourcePitchShakeEvent_Register_m13544C89AC62AA1852A43D631D119DFF7FF53940,
	MMAudioSourcePitchShakeEvent_Unregister_m02690AC42A1E3055B502C5CA6A375FCE905861B5,
	MMAudioSourcePitchShakeEvent_Trigger_mBCA8ED3145E909496C1358C13D9BA3BD8EACED89,
	Delegate__ctor_mCF8A9A41268551BC9D8B73A730D90911436295EC,
	Delegate_Invoke_mA8F3CC35CE955FC617422D001BD481507F371015,
	Delegate_BeginInvoke_mC018DB947EB4E7604C26AEA8C14CC4052FC2C7E5,
	Delegate_EndInvoke_m77AC9DA907FD199FEA7493692DAFCB245BD4DDBC,
	MMAudioSourceStereoPanShaker_Initialization_m739827F6D6DF7AF6A70C26D688927A6EFBC10417,
	MMAudioSourceStereoPanShaker_Reset_m596976C4B964FF1E2A2EDCC0DBF6A7756C7530F4,
	MMAudioSourceStereoPanShaker_Shake_mE2B90965F79A759D214A07F1E34B4F1845C5445A,
	MMAudioSourceStereoPanShaker_GrabInitialValues_m5B9A93DAFEBA781A18522CED75C572752E230035,
	MMAudioSourceStereoPanShaker_OnMMAudioSourceStereoPanShakeEvent_m5B6D9062536D0CD69150478929946C2EEB724B38,
	MMAudioSourceStereoPanShaker_ResetTargetValues_mD6301F32718E1D74D4C9F0479DD9B272DE409F26,
	MMAudioSourceStereoPanShaker_ResetShakerValues_m18E3EDCEDD23D62636909C400BC2A623846044F0,
	MMAudioSourceStereoPanShaker_StartListening_m2A821ADAA5B587B67374A3AE5F64615BCAFABD80,
	MMAudioSourceStereoPanShaker_StopListening_mDBC458D08E3C6EA0C963A088A8ADF424AAF7F63F,
	MMAudioSourceStereoPanShaker__ctor_m975C51828C963DFB826783EC14189F69FA785055,
	MMAudioSourceStereoPanShakeEvent_add_OnEvent_m96EE77769C8621F15791D9C138B3C491B291B0F0,
	MMAudioSourceStereoPanShakeEvent_remove_OnEvent_mE06FC8EF873961736BC1CD2B80C1518811B924F8,
	MMAudioSourceStereoPanShakeEvent_Register_mA52EDF362DCED0AF9C21553EC2337BB03BE99DA0,
	MMAudioSourceStereoPanShakeEvent_Unregister_m686A253DAFCCFEFD2311428BF89827EECE8E79C8,
	MMAudioSourceStereoPanShakeEvent_Trigger_m2A6E8C23D3959E9B9B5F9CEC3DB1F434B25E66D2,
	Delegate__ctor_m53F954389DF9EC3E36BB31A0FAAC46ECF049046F,
	Delegate_Invoke_mA7448ED2CFC09FBC74100305870D805517B597A3,
	Delegate_BeginInvoke_m793D6B2B4A8596AD57616066611C63EC70CC9E0C,
	Delegate_EndInvoke_mAD3D804E6529662E644E6FCA63ECC7205877285D,
	MMAudioSourceVolumeShaker_Initialization_m6F7B6E1136C65B196FC6923C2695A70E67721802,
	MMAudioSourceVolumeShaker_Reset_m9846A94481DD04017ECE38C8B3E53F808B2F3BF9,
	MMAudioSourceVolumeShaker_Shake_mEDC59448FBCAC55AABF6FEEA05A0516AC99C4607,
	MMAudioSourceVolumeShaker_GrabInitialValues_m41ECC648544F26BA2064AAF764C1FBED60B8B2D7,
	MMAudioSourceVolumeShaker_OnMMAudioSourceVolumeShakeEvent_mC89D6153C764F2E55A24CE677FDB17BDC2F893DA,
	MMAudioSourceVolumeShaker_ResetTargetValues_m7C045EF52CCC79D1F0543A155C6C6CEE5058A192,
	MMAudioSourceVolumeShaker_ResetShakerValues_m9B9E8100173064BADD2CC29D8DCAF796582FDF03,
	MMAudioSourceVolumeShaker_StartListening_m9854A36B596B4648DB5273921219D3EA926AF0F1,
	MMAudioSourceVolumeShaker_StopListening_mA0D40CD8CBDA5B5226859C474427D7B539663484,
	MMAudioSourceVolumeShaker__ctor_m94D411F3ABD94FCA6C2C4B6B5BE971376A8B138A,
	MMAudioSourceVolumeShakeEvent_add_OnEvent_mF2B9B24D7FE3DC35F1D9EF33FCEF07E8AD1EB105,
	MMAudioSourceVolumeShakeEvent_remove_OnEvent_mF856F2DE6A95B4AA80D0A66CBC7718B4636C0743,
	MMAudioSourceVolumeShakeEvent_Register_m2AC82EC63351B0DBC4D08F1A4DE2991AB37AFED1,
	MMAudioSourceVolumeShakeEvent_Unregister_m5B7B6CEE5AFA4C930850E7BD5019C3D7B898C7E5,
	MMAudioSourceVolumeShakeEvent_Trigger_m65BD14EF7D1A543F46A7429D5A1F907BE47AC0C4,
	Delegate__ctor_m9D80BE20FD66454DC2A247B82000F8C6430D431D,
	Delegate_Invoke_m9E21DF25524680A987009ECB0649E581C0397F8D,
	Delegate_BeginInvoke_m374B2E89BB9D3BB401C10D50190293F749941E7C,
	Delegate_EndInvoke_m0B8F23634D484E2DBA803A460705B4B63DAEAD84,
	MMCameraClippingPlanesShaker_Initialization_mAB74F21C54F534D24AFA12EB9073B9DF934485FE,
	MMCameraClippingPlanesShaker_Reset_mB6C829F3668ADC0F9D4B82CD9F97E98CE684888D,
	MMCameraClippingPlanesShaker_Shake_m3CCD29F7F110A08D6B105F52FE8A9A7B0C52BE76,
	MMCameraClippingPlanesShaker_GrabInitialValues_mAD96A12B25BAC90B3AC96A8170D06AACF4C05270,
	MMCameraClippingPlanesShaker_OnMMCameraClippingPlanesShakeEvent_m7B1BD0E874CBC2AF15FACBF967D3347CFA150BEE,
	MMCameraClippingPlanesShaker_ResetTargetValues_m84C61C85871B5326157DF6A80745B18C849876DD,
	MMCameraClippingPlanesShaker_ResetShakerValues_mEE7DEC587820786672D06127F215F8987794379D,
	MMCameraClippingPlanesShaker_StartListening_mBADD2F2901C9BF4563E42233CE15C35CA74678C6,
	MMCameraClippingPlanesShaker_StopListening_m60ABF19E631AAF780BAF3626DBC238A799C5F9E0,
	MMCameraClippingPlanesShaker__ctor_m6F8EB5D3EB689EC3253AD26E0BF8CB34A3EF6A0B,
	MMCameraClippingPlanesShakeEvent_add_OnEvent_mA7D3A5E11DF71E52B549F05A437D1B0194E639C6,
	MMCameraClippingPlanesShakeEvent_remove_OnEvent_m922EB0801D52E18D5E2333850309460D6AE36DD8,
	MMCameraClippingPlanesShakeEvent_Register_m6A7DECCE069AAD9D2E02F0FC3F05C914EFCB262B,
	MMCameraClippingPlanesShakeEvent_Unregister_m0B491D7BFD7512928B33E4143AC6923BCE0CAB18,
	MMCameraClippingPlanesShakeEvent_Trigger_mBAF1CB0B2CC4F9BBC13295E4AE2B69F22F13C748,
	Delegate__ctor_m733422BE0BC41D7D63A7B61424293CD781087971,
	Delegate_Invoke_m0C4FCBBE5260E3696C6FDBB8503EEB6C2C3E6B41,
	Delegate_BeginInvoke_m0C94C5BD9A7EC333CBCAD1D726AA14E54A75541C,
	Delegate_EndInvoke_m6C66B64D8A260E72F5CD11068C13575BE4B292B5,
	MMCameraFieldOfViewShaker_Initialization_m13A0FE7F26E9FD60D0C9D64978EB5BD6CDE114A9,
	MMCameraFieldOfViewShaker_Reset_mB34A0582D12D863A2E066AC1E80924DA61D4F4DF,
	MMCameraFieldOfViewShaker_Shake_m0738400E4F4D897727091A5FBC8070F163E141E1,
	MMCameraFieldOfViewShaker_GrabInitialValues_m5E878303BAA591F23ACA9C3093676381506B7B9D,
	MMCameraFieldOfViewShaker_OnMMCameraFieldOfViewShakeEvent_m1B8D55671C981D3A9BF23FE232980DCB2CEE8178,
	MMCameraFieldOfViewShaker_ResetTargetValues_mC1D9DD2383F71D79C7B0EC47E836D54E2A7FD16B,
	MMCameraFieldOfViewShaker_ResetShakerValues_m87093D801FAB0648628DBBA9F8BFD67C1652838A,
	MMCameraFieldOfViewShaker_StartListening_m1DEDA4E892A6FCDC1C298FC552E3120C234F4C17,
	MMCameraFieldOfViewShaker_StopListening_mDE82519EE9937CE3635AFF41FE911B5D93952122,
	MMCameraFieldOfViewShaker__ctor_m18C5B1EBEE53B4835720EA5A5A4563BB61CA79BA,
	MMCameraFieldOfViewShakeEvent_add_OnEvent_m9BA76AE92CB9125D6AB85C313AC380BD8B6DFE39,
	MMCameraFieldOfViewShakeEvent_remove_OnEvent_mADB6D177C90C4E69148CAD9F14EDB99BA56ED346,
	MMCameraFieldOfViewShakeEvent_Register_m19C5C43176354FCE199695648EC34C8EB18F6637,
	MMCameraFieldOfViewShakeEvent_Unregister_m3EB17557FCCF2C6981EB1C688AD34FB4860C28BF,
	MMCameraFieldOfViewShakeEvent_Trigger_m2A3A3C637A68043DF3135D78F152C1EC29AE26D6,
	Delegate__ctor_mFBE74CEFCF7377CCFBD4E381A7C7841542690FD7,
	Delegate_Invoke_m2B3B5BBBB7DAF61A7BB8EBF12352E51A55F61969,
	Delegate_BeginInvoke_m8245612ABDF67A089EE78C6B6D4C8BFB2FFCAE59,
	Delegate_EndInvoke_m8E68639AE58C45E26B9B6C1DAB98484AA85E3D63,
	MMCameraOrthographicSizeShaker_Initialization_mDB51ED01B8527F8AB20C66458B81F4E53E5E1935,
	MMCameraOrthographicSizeShaker_Reset_m7E67ED69F8911A268476C1FDCC140A4C587B689E,
	MMCameraOrthographicSizeShaker_Shake_m9BB532AAD51012CDEFB54699E4236B015A7CBF9E,
	MMCameraOrthographicSizeShaker_GrabInitialValues_m4CA28A55235AA50C268F25FC4F45B5921AA77909,
	MMCameraOrthographicSizeShaker_OnMMCameraOrthographicSizeShakeEvent_m3F1FF6791F349E05C7529119D4EB1C544D6D5E94,
	MMCameraOrthographicSizeShaker_ResetTargetValues_mD1FF6AF178DF0E7F4A27992BBCCBF0862F0C0BF6,
	MMCameraOrthographicSizeShaker_ResetShakerValues_m541AC99762F216D163F1052FF96606476376AAE0,
	MMCameraOrthographicSizeShaker_StartListening_m84028EFD7EF5D6BBA3C079319CD1C5408A6B0D36,
	MMCameraOrthographicSizeShaker_StopListening_mEECA7F03FCC796C240C6006FA902A6A2A9EB42D5,
	MMCameraOrthographicSizeShaker__ctor_m013A73EB516B47863E11D285E0CE05EC1659F0A4,
	MMCameraOrthographicSizeShakeEvent_add_OnEvent_mC086DEA6D26F92FDA9A0396D1C79C905CCFA980C,
	MMCameraOrthographicSizeShakeEvent_remove_OnEvent_m79CE28116F524268FC26D2B6AF2351ECAFD25BB7,
	MMCameraOrthographicSizeShakeEvent_Register_m61133F90064EAF651830122BC78D43A6EFA90F89,
	MMCameraOrthographicSizeShakeEvent_Unregister_m4DA6F4FFC40C414AA90819ADF3506C97FDEAC691,
	MMCameraOrthographicSizeShakeEvent_Trigger_m6534FDDDF3E4FEF1AB8F49F1762F04AEB5D3BD39,
	Delegate__ctor_mD22CAD931157C69FAE8456E9D80ACCF5C4154BD6,
	Delegate_Invoke_m345DE6E59E2885DF67B7E0F8AB57678B15D3F9D0,
	Delegate_BeginInvoke_mC1A91CFBC74090A64271725D1FA079B808221333,
	Delegate_EndInvoke_m9652A34AA97C7A7AF27DCC74898CC7F253FBA409,
	MMCameraShakeProperties__ctor_mD1E9963F1BFBE26F29CD2DAA9AAC7E69735F4964,
	MMCameraZoomEvent_add_OnEvent_mA8B79E30F6A18EF3945FC599D98988CA9C1C2BD8,
	MMCameraZoomEvent_remove_OnEvent_m515415366C4BBB5F6C8906E165C4448B9DB1D08A,
	MMCameraZoomEvent_Register_m21AF5CFDCCC96D94362DD58129554484A7D738B4,
	MMCameraZoomEvent_Unregister_mB161F83680ED067D665DA91006EE8A92B3083AB0,
	MMCameraZoomEvent_Trigger_m38B1F6F3137EAA8D1F06F7EE347CDDE7DB245B9D,
	Delegate__ctor_m7D7D03319B4EF6154F102E5EDFF26711FB51C4AD,
	Delegate_Invoke_mEF3B60660634573BB90A93133255CD6079186543,
	Delegate_BeginInvoke_m930360FF8349181FA43D08AC11FFE97B9329D8F7,
	Delegate_EndInvoke_m6982BD075BC46F5FE32574A1354685CA67B29633,
	MMCameraShakeEvent_add_OnEvent_mD692C7BE9445BCC7A8B86CC85D196ECEE73E9E2D,
	MMCameraShakeEvent_remove_OnEvent_mA0D3B24FCD8E23713193FC95B31E5A19D433B20A,
	MMCameraShakeEvent_Register_m724CD0839144D1B710757E1C6AB22E941216EDCB,
	MMCameraShakeEvent_Unregister_mC1573AD7E2DA5F0AB8299CEBDD7A2F6C4D0F5E06,
	MMCameraShakeEvent_Trigger_mCF5AAE461A7F52ADF0F8464349413173E46A8EE2,
	Delegate__ctor_m92750E4DE57041804753CA5809C649DBB44844BB,
	Delegate_Invoke_mE6B52BF4D4B34C06A9BC9AF8FBEEF5DA883FB24E,
	Delegate_BeginInvoke_m5F44DE5F710D9BEA338751BC45BA71E954229E06,
	Delegate_EndInvoke_m891123BA2AB559E669A7ABC025C30E434127AD4B,
	MMCameraShakeStopEvent_add_OnEvent_mBB56D6E362F9B97F3038AD137319A1E3703BBA83,
	MMCameraShakeStopEvent_remove_OnEvent_m293A897863522D250E2328645F8FDEA15F377AC3,
	MMCameraShakeStopEvent_Register_m7116A4D9BC6DDEF692A978037A91C197C3D8D20D,
	MMCameraShakeStopEvent_Unregister_m4BDB20A81C8C1FE34062253C9D587C67901E38E6,
	MMCameraShakeStopEvent_Trigger_mE1FD47D0BB25F16D0F405B9FF7A8FA6BB7EC7C24,
	Delegate__ctor_m828FB777E333546230DB23E0DCAA1C081D4D1D08,
	Delegate_Invoke_m55496058FB43150859E3D8BD22F6AE58596059ED,
	Delegate_BeginInvoke_m017513A438267F0419C7AB271D40386A24A6F2E4,
	Delegate_EndInvoke_m50C5D17B821534FE67DA9608EB25862BB3AC0445,
	MMCameraShaker_Awake_mEE2DDC76431A9277B93F648FDFEDFCAC6F59756F,
	MMCameraShaker_ShakeCamera_mA3DEC28120EB17F24E3F5F409668E651D54EC7F0,
	MMCameraShaker_OnCameraShakeEvent_m7A20C6CBF1B4781983ABF2CA154C29F8806DA603,
	MMCameraShaker_OnEnable_m994E02B97A9021BD5FB7AC4C445436371E4CC4A4,
	MMCameraShaker_OnDisable_m75FB345A12B7E02921FCB9B81810C9E0BD4A5DD9,
	MMCameraShaker__ctor_m7FB89B9F689289D7F26EC5E170C0B1E55BE5AE39,
	MMFeedbacksShaker_Initialization_m56881E2D1FCF7D9C2748B5F63CD29F64E6BC2BA5,
	MMFeedbacksShaker_OnMMFeedbacksShakeEvent_m4956F7C91408669C0BAACC725F82015BF14FCB30,
	MMFeedbacksShaker_ShakeStarts_mD733248D602FDC46300C4A4D5A0ABC102BF4959B,
	MMFeedbacksShaker_Reset_m8C4CF6EE0434C78F1BA2C6F7E6C4DE8895671246,
	MMFeedbacksShaker_StartListening_m47107E346CC0EB4113E86E5FABB83CF50C585A12,
	MMFeedbacksShaker_StopListening_m4CF030D1D0FFD15392880D921A28246934162E95,
	MMFeedbacksShaker__ctor_mDE795DC8DF4DD8C767D2C4BCBDDD54C563F2C146,
	MMFeedbacksShakeEvent_add_OnEvent_mA8223F79AB281471A5C336A825267797D1163DE3,
	MMFeedbacksShakeEvent_remove_OnEvent_m3B1BED5AF35BF0A891BDEDFFBE47A64E27233756,
	MMFeedbacksShakeEvent_Register_m0356729ACFC7462F575DAE08B0F3A6510338BB10,
	MMFeedbacksShakeEvent_Unregister_m0913AB07129EE6C39FB395DF6A74F1590EFEFFB9,
	MMFeedbacksShakeEvent_Trigger_m23F6F25A8B8567C2AE9F6249D1B8E36415E457BE,
	Delegate__ctor_m532E480D93CED4886EE6F6D0264111AF4B39E54F,
	Delegate_Invoke_m59B0259F4822D050B4FBA3DFECCE59D8D4B41ADB,
	Delegate_BeginInvoke_mE2FC6B2C3B1718A8212C3630CAE605DC520B7423,
	Delegate_EndInvoke_m8CC2852E819BD09D7F01D9C8150C28DEFE17B8B3,
	MMFlashEvent_add_OnEvent_m5DD1CF1445D375EE9B287C822B88610D33B42C09,
	MMFlashEvent_remove_OnEvent_mCC5B7BD2EFC7F08A5C127A519213FEE8411A8AF3,
	MMFlashEvent_Register_m7961AC06CF4F58CAAE9EDD72F3665AA428E54565,
	MMFlashEvent_Unregister_mF3DC65819962B8B3683E7EB83557CD68A0DE8659,
	MMFlashEvent_Trigger_mD287B4021136FDC7CBB4765C1EA3FA6006B65090,
	Delegate__ctor_m64099DCD1DBCFE9A8B2068FC6F983A48D37372C3,
	Delegate_Invoke_m9D0F9D5610ED5872D30A28F18B9DA37966B4B415,
	Delegate_BeginInvoke_mC55C72A18946D786686C688882D7F49362B313E1,
	Delegate_EndInvoke_m85E48C8B671FC95A7BCD7C6D7EC2C739BF85D3FF,
	MMFlashDebugSettings__ctor_m0E1C719F38CD9399DBCA901ECC528E418034BE7C,
	MMFlash_GetTime_mEAF3D2A8B416E3518B4067EC9CD7B8BA7F1C0E9F,
	MMFlash_GetDeltaTime_m1FB173336C839B315A7CDA5B8954F76B15519185,
	MMFlash_Start_mE567722D1FBC1AB6340982F7339591944064EC40,
	MMFlash_Update_mF17F454FF5DE171CF81C1CFB56CC748438040735,
	MMFlash_DebugTest_mFDA5D4B3CEFE23BDD9DA0AE23517EA7C363F21DF,
	MMFlash_OnMMFlashEvent_m3BD75D26F094977B17EE5A4A8D36E657BF3E0418,
	MMFlash_OnEnable_m4834D455A97460B110F34B2A6AC7022C0E614FD6,
	MMFlash_OnDisable_m598E89140DC89EEDEDCE405BD4615BF536F332A4,
	MMFlash__ctor_m8299CBC97BE90C9957607823F4FAEF7D93E1EC72,
	MMLightShaker_Initialization_mAC28AC3B6184D2D6AAAA98460EF99041EE862C89,
	MMLightShaker_Reset_m0059B5FCDCC375E64924E61C71EAB0E1D07EA079,
	MMLightShaker_Shake_m4C184F73BBCC6DFA29FB9F90B5BE97DA9B8FCA79,
	MMLightShaker_GrabInitialValues_mA980E78C180868A2CF5E7C91A02B505C20C1D4C2,
	MMLightShaker_ResetTargetValues_mE6372E4414B21250752B4694B4A2890B8B481CCD,
	MMLightShaker_ResetShakerValues_m76584D04881DABE2F7F4E7F39B0E9FF9B45A5E9B,
	MMLightShaker_StartListening_m4F2C2C07D695350DEBADC4CFBD1164C7D0D20F21,
	MMLightShaker_StopListening_mDC65292A6003B622421794C0BE59208F3BFE7DE5,
	MMLightShaker_OnMMLightShakeEvent_m0AEA3FE00BDBD85060ACB70BB7A842CA38DE4921,
	MMLightShaker__ctor_m38230B25FB49E29D7E207FB19E43458A0B8F4A92,
	MMLightShakeEvent_add_OnEvent_m3D3F66CBE5EFE6767D5FAF4FF01BA57A73BBF7F1,
	MMLightShakeEvent_remove_OnEvent_m307FA66E4B929EBD934B2CA3B7B2B2FB209168D3,
	MMLightShakeEvent_Register_m4320BB3473954A1BD254C983AF99C61BB77159CD,
	MMLightShakeEvent_Unregister_m85A35DBEE0CE332ED111B78E7171CDB201D14FE6,
	MMLightShakeEvent_Trigger_m6A6AE7AD3EC868A764C4136C78D021FFEC93F388,
	Delegate__ctor_m476DEF09C16C46CF8F44DC9FCFD622D46B984754,
	Delegate_Invoke_m9A545DE390E3E47B6B0A27AB3217EF2B0E517D39,
	Delegate_BeginInvoke_mC9BE683FE7204620E4284503411EBCFAC0B2DA23,
	Delegate_EndInvoke_m4E9B4742BCB90CA9CC171D4C7CFB39F88AD0B52B,
	MMSpriteRendererShaker_Initialization_m001014F67B58DB9DF3D631B13B7E0ADB771F64C2,
	MMSpriteRendererShaker_Reset_m90E31160325C8026801CB4D131E7E561C9F239E0,
	MMSpriteRendererShaker_Shake_mF874FDE36715E19C5A928B7BBA7E3BF98401351F,
	MMSpriteRendererShaker_GrabInitialValues_m514944FD47193022C25F4AEDF6C7B45650D5EF09,
	MMSpriteRendererShaker_ResetTargetValues_mE4FFB5D7167123FF45B44BC06E1EA16DD7CE71E4,
	MMSpriteRendererShaker_ResetShakerValues_mC37B517A13A2B06ABD42AB7B242A1A65BEC52A9E,
	MMSpriteRendererShaker_StartListening_m2CCB4855C40E243027D3AD0713FAD73DF9D07BB3,
	MMSpriteRendererShaker_StopListening_m341C136DCB92D1E20005873AB8983ADF8A79B985,
	MMSpriteRendererShaker_OnMMSpriteRendererShakeEvent_m72FCBD3C05F1B9930F38F6B21915C24ACC465A0F,
	MMSpriteRendererShaker__ctor_mFA9AF5112DB3CFB8C162E26591429FC66F797E5D,
	MMSpriteRendererShakeEvent_add_OnEvent_m0D612C17ABBAA342DB82594398889DA0449C77F3,
	MMSpriteRendererShakeEvent_remove_OnEvent_mD539FB4208AE396AEAEDEB68536183E79A1F399A,
	MMSpriteRendererShakeEvent_Register_mCDC4817EAD982E0E1D2576741AD78FA26D36DAA7,
	MMSpriteRendererShakeEvent_Unregister_m39CB15CD66B854F722F52826747256F27E1EC707,
	MMSpriteRendererShakeEvent_Trigger_mFDD7EDB65DD1B0019F8AA46B5A2766A97AA9E18A,
	Delegate__ctor_m642066DD8BD9826EC30BB6C3115731C9AFFF8442,
	Delegate_Invoke_m7739E286A564A0D5C34880A61E308A730F0A4947,
	Delegate_BeginInvoke_mBFDF29AB2DDEC5408CC8E4D261E55EC8CA3E203C,
	Delegate_EndInvoke_m931078FCAD178A8EDF9D472AB62681711A6C68C6,
	MMTimeScaleEvent_add_OnEvent_m4962833A74F45A7629A9A6BEFF3B591526D5EC77,
	MMTimeScaleEvent_remove_OnEvent_mCF0847F3C17A1BBDB13460FC6DF4704DD5C48D39,
	MMTimeScaleEvent_Register_m28C4935DCB103FBD77D571844490690DA9D6223C,
	MMTimeScaleEvent_Unregister_m8CAE055701D97B813D16B75309A62ED5E9FA0B88,
	MMTimeScaleEvent_Trigger_mF54D807E19DF44F9EB1E07B9A76C8E294632741E,
	Delegate__ctor_m8671B4AA31BFF8224D2C31A6E67A2C3A7ECA8A58,
	Delegate_Invoke_m128D37A521F226D01591B936AA01A0A01C05196C,
	Delegate_BeginInvoke_m9BB32F6C8B0A6EB998973CEE0579CAF81CCFE1CF,
	Delegate_EndInvoke_mBDDF17A5A56D20C8592AC2808131A9258513E0DF,
	MMFreezeFrameEvent_add_OnEvent_m68B87281495E4229DEF2E8F7BDEEEE1627150B01,
	MMFreezeFrameEvent_remove_OnEvent_m843ECC5D7026D1C07A23CF3838F814CC3A068D37,
	MMFreezeFrameEvent_Register_mF1D85B4305902F2B1DEC710EC3F9A7D3CB813206,
	MMFreezeFrameEvent_Unregister_mDEAA2A80941E75D53EBDC015780857686113E57B,
	MMFreezeFrameEvent_Trigger_m218A2295294F0D4022D2FF518160A8DF0F0FF8B2,
	Delegate__ctor_m36455E5AEABC822DF46AAD4E155CF6E81D52DC3B,
	Delegate_Invoke_m295A04EC8B09AB77DAB8F7F25C91E3DCD027862E,
	Delegate_BeginInvoke_m32C83874A7BA1A0E954C85CBC6F2F8C7F14FC307,
	Delegate_EndInvoke_mA1A3CE1828417E0C4E9DF16BD6AF5968B26DC702,
	MMTimeManager_TestButtonToSlowDownTime_m5989141E8375BACF15704704A9DBE072FC58C04C,
	MMTimeManager_Start_mC7D04335ADB46CEEB7A3D8B575B7823E38513DD5,
	MMTimeManager_Initialization_mC666136E4907D64093684BF83FF24D90B2531330,
	MMTimeManager_Update_m7DC6C9C191B490D06623F9C4026837E1CAE8969F,
	MMTimeManager_ApplyTimeScale_m7313E9D9D5B3779BC7D9DF387659597AD2641A41,
	MMTimeManager_SetTimeScale_mABA5058DC06E363CF2BEAEC88A500F3324549E91,
	MMTimeManager_SetTimeScale_mDA2C6513CEA87286D39220E8A3107AB6A4157BF2,
	MMTimeManager_ResetTimeScale_m09D2F185FAAF975533132EC60F505AF68AD6DE20,
	MMTimeManager_Unfreeze_mCD81C50798C98C28FC5DC819ACC529EF76641DDB,
	MMTimeManager_SetTimescaleTo_m4D737EC2F69F088DBA292CD289A6EF75B0926420,
	MMTimeManager_OnTimeScaleEvent_m6D0BCA9C915BDEDB97EFF7A33223FDAE6655C29F,
	MMTimeManager_OnMMFreezeFrameEvent_mB8F8DFE987ED86A92C40BEA6C758D3EF533D3971,
	MMTimeManager_OnEnable_m4AD55FBF40C91C82B57FDB3DD921125158E1860D,
	MMTimeManager_OnDisable_m15527DAA44A4724435A4E26EF4585CE1A11E3DA5,
	MMTimeManager__ctor_m848771FD04C1365A0697CD68B9CDE5B50CC053F2,
	WiggleProperties_GetDeltaTime_m59B1FC49566F91EE07971CAC8D4E2EC78881BDF1,
	WiggleProperties_GetTime_mC8DF410F9D5690221B2F8FDAC8334EFDBA5420D4,
	WiggleProperties__ctor_m97AE90EC2207E94536225B6BA0B78F3C4FCC78F8,
	MMWiggle_WigglePosition_m2490D7C3DA0CA67E1DFFC279E4DC00CF66F82365,
	MMWiggle_WiggleRotation_m7E39A1A5F0DCED24721720C3C8A5729D40761D18,
	MMWiggle_WiggleScale_m92ADADB43C7BBB7783B1EAC81EE8A6C5CD3D5EC5,
	MMWiggle_WiggleValue_m079EC28489ABA0798EC61E23BD054DB5BA01277E,
	MMWiggle_Start_mF8806C45942DC58B4BC28067E33D274DDC44CDF1,
	MMWiggle_Initialization_mBA8229E2553D6AEC112B7F309E08773348446478,
	MMWiggle_InitializeRandomValues_m736452E1EB2A5EFAD6D9A38190AD7310DB51907F,
	MMWiggle_Update_m4FFC4CBB259E5DF76F4A0E4197ED19C5E32DCA4D,
	MMWiggle_LateUpdate_m5FBD9D33DF973EADA24389361BEAD533C94F2EA5,
	MMWiggle_FixedUpdate_m5183FAFB2476705DDABA15BA4EF937B919CAB494,
	MMWiggle_ProcessUpdate_m10168F7BE23ADA9D397BE3B2CC29111C7AE47E73,
	MMWiggle_UpdateValue_m2E7E1A10BF0D7FBB45F35D93648D0E0BA20B651D,
	MMWiggle_ApplyFalloff_mE69A63D6D94E1786825F10C7F1A954436CA35831,
	MMWiggle_AnimateNoiseValue_mAA2F63174985C881BEA9060850D79C830D755B11,
	MMWiggle_AnimateCurveValue_m8ABC0235FEE6370E7E49F42884C6DC7846F29D8D,
	MMWiggle_EvaluateCurve_m8D09D646A28AC007F55B0C2B6A121E9833599872,
	MMWiggle_MoveVector3TowardsTarget_m925538B42260ED20F8428AF5075FB1EAF105AF2D,
	MMWiggle_DetermineNewValue_m45E374999550CC2AC747D7EDCDA6F8FBF93A7810,
	MMWiggle_RandomizeFloat_m41B62471F3FFB65CF9339B8FD5DD61A70A0EF3CD,
	MMWiggle_RandomizeVector3_m9FD26FFD3DB00D5BB321935D0495E937B1A87E6E,
	MMWiggle__ctor_mE4306682B5DB6DFC965014C12978532AB2CA2F55,
};
extern void MMCameraShakeProperties__ctor_mD1E9963F1BFBE26F29CD2DAA9AAC7E69735F4964_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[1] = 
{
	{ 0x060003B3, MMCameraShakeProperties__ctor_mD1E9963F1BFBE26F29CD2DAA9AAC7E69735F4964_AdjustorThunk },
};
static const int32_t s_InvokerIndices[1110] = 
{
	3361,
	3361,
	3316,
	2760,
	3388,
	3388,
	193,
	3388,
	81,
	3388,
	3388,
	3388,
	3336,
	2791,
	3336,
	3358,
	3358,
	3358,
	2810,
	3361,
	2812,
	3358,
	3358,
	3361,
	3361,
	3361,
	3361,
	3361,
	2812,
	3358,
	3388,
	2791,
	1670,
	1214,
	1670,
	1214,
	1214,
	1214,
	1670,
	3388,
	2791,
	2812,
	2812,
	2530,
	3358,
	3358,
	3361,
	2530,
	2791,
	1670,
	1670,
	3388,
	3388,
	2760,
	3388,
	3358,
	3336,
	3388,
	3336,
	2760,
	3388,
	3358,
	3336,
	3388,
	3336,
	2760,
	3388,
	3358,
	3336,
	3388,
	3336,
	2760,
	3388,
	3358,
	3336,
	3388,
	3336,
	3388,
	3358,
	2810,
	3358,
	2810,
	3358,
	2810,
	3358,
	2810,
	3361,
	3388,
	3388,
	3388,
	3388,
	2791,
	3388,
	1037,
	3388,
	1037,
	3388,
	1037,
	3388,
	1037,
	850,
	1037,
	1037,
	1037,
	850,
	3388,
	1214,
	3388,
	2810,
	1037,
	3388,
	3388,
	3388,
	3388,
	3388,
	2397,
	3388,
	2530,
	3388,
	3388,
	3388,
	3388,
	3388,
	5285,
	2760,
	3388,
	3358,
	3336,
	3388,
	3336,
	2760,
	3388,
	3358,
	3336,
	3388,
	3336,
	2760,
	3388,
	3358,
	3336,
	3388,
	3336,
	5089,
	5099,
	5099,
	2760,
	3388,
	3358,
	3336,
	3388,
	3336,
	2760,
	3388,
	3358,
	3336,
	3388,
	3336,
	2760,
	3388,
	3358,
	3336,
	3388,
	3336,
	3336,
	2791,
	3388,
	3388,
	5206,
	5206,
	5206,
	5206,
	4849,
	1620,
	1618,
	526,
	2791,
	3358,
	2810,
	3358,
	2810,
	3358,
	2810,
	3358,
	2810,
	3358,
	2810,
	3388,
	2791,
	2791,
	2791,
	2791,
	2791,
	3388,
	3779,
	3388,
	3388,
	2791,
	2378,
	1622,
	992,
	3388,
	2791,
	1626,
	4600,
	-1,
	5285,
	2791,
	5092,
	5092,
	2791,
	5092,
	3388,
	5285,
	3361,
	3361,
	3358,
	3388,
	3388,
	3388,
	3388,
	3388,
	3388,
	3388,
	294,
	3388,
	3388,
	3388,
	3388,
	3388,
	3388,
	3388,
	3388,
	3388,
	3388,
	590,
	3388,
	3388,
	3388,
	3336,
	3388,
	3336,
	3336,
	3388,
	3388,
	3388,
	2791,
	2791,
	3388,
	3388,
	3388,
	3388,
	3388,
	1620,
	3388,
	1201,
	2791,
	2791,
	1670,
	1670,
	3388,
	3361,
	2812,
	1670,
	1670,
	3388,
	3361,
	2812,
	1670,
	1670,
	3388,
	3361,
	2812,
	1670,
	1670,
	3388,
	3361,
	2812,
	1670,
	1670,
	3388,
	3361,
	2812,
	1670,
	1670,
	3388,
	1670,
	3388,
	3361,
	2812,
	2791,
	1670,
	1012,
	1670,
	3388,
	3361,
	2812,
	1670,
	1670,
	3388,
	3361,
	2812,
	1670,
	1670,
	3388,
	3361,
	2812,
	1670,
	1670,
	3388,
	3361,
	2812,
	1670,
	1670,
	3388,
	3361,
	2812,
	1670,
	1670,
	3388,
	3361,
	2812,
	1670,
	1670,
	3388,
	3361,
	2812,
	1670,
	1670,
	3388,
	3361,
	2812,
	1670,
	1670,
	3388,
	1670,
	3388,
	3361,
	2812,
	1670,
	3336,
	1670,
	3388,
	2760,
	3388,
	3358,
	3336,
	3388,
	3336,
	1670,
	2791,
	3388,
	2791,
	1670,
	1670,
	3388,
	2760,
	3388,
	2791,
	1670,
	1670,
	3388,
	3388,
	3361,
	2791,
	1670,
	3388,
	3361,
	2812,
	1670,
	1670,
	3388,
	3361,
	2812,
	2791,
	1670,
	3388,
	153,
	1467,
	1670,
	3388,
	2760,
	3388,
	3358,
	3336,
	3388,
	3336,
	3361,
	2812,
	1670,
	3388,
	3358,
	3361,
	2812,
	1670,
	3388,
	3361,
	2812,
	2791,
	1670,
	3336,
	2812,
	1670,
	2810,
	3388,
	2760,
	3388,
	3358,
	3336,
	3388,
	3336,
	1670,
	3388,
	2791,
	1670,
	2835,
	2564,
	3344,
	3384,
	3388,
	3361,
	2812,
	2791,
	1670,
	2114,
	1658,
	1670,
	2810,
	3388,
	2760,
	3388,
	3358,
	3336,
	3388,
	3336,
	3358,
	3361,
	2812,
	2791,
	1670,
	1670,
	3388,
	3388,
	3358,
	3361,
	2812,
	3388,
	1670,
	3388,
	3361,
	2812,
	3361,
	3361,
	2791,
	1670,
	1201,
	3316,
	1670,
	3388,
	2760,
	3388,
	3358,
	3336,
	3388,
	3336,
	2791,
	1670,
	1670,
	3388,
	2835,
	3388,
	3388,
	2791,
	3388,
	2791,
	2140,
	2562,
	2564,
	1670,
	1670,
	3388,
	3388,
	3336,
	3361,
	2812,
	3336,
	2791,
	1670,
	3336,
	3388,
	2760,
	3388,
	3358,
	3336,
	3388,
	3336,
	3361,
	2812,
	2791,
	3388,
	1670,
	544,
	687,
	284,
	2562,
	1630,
	1670,
	3388,
	3388,
	2760,
	3388,
	3358,
	3336,
	3388,
	3336,
	2760,
	3388,
	3358,
	3336,
	3388,
	3336,
	1670,
	3388,
	1670,
	3388,
	3361,
	2812,
	2791,
	3388,
	1670,
	3336,
	67,
	136,
	1670,
	3388,
	3388,
	2760,
	3388,
	3358,
	3336,
	3388,
	3336,
	2760,
	3388,
	3358,
	3336,
	3388,
	3336,
	3361,
	2812,
	2791,
	3388,
	1670,
	3336,
	67,
	1670,
	3388,
	3388,
	2760,
	3388,
	3358,
	3336,
	3388,
	3336,
	2760,
	3388,
	3358,
	3336,
	3388,
	3336,
	2791,
	1670,
	1670,
	3388,
	2760,
	3388,
	1670,
	3388,
	3361,
	2812,
	2791,
	1670,
	3336,
	2113,
	3388,
	2812,
	1670,
	2810,
	3388,
	2760,
	3388,
	3358,
	3336,
	3388,
	3336,
	2760,
	3388,
	3358,
	3336,
	3388,
	3336,
	1670,
	3388,
	3361,
	2812,
	2791,
	1670,
	3336,
	2812,
	1670,
	3388,
	2760,
	3388,
	3358,
	3336,
	3388,
	3336,
	3361,
	2812,
	2791,
	1670,
	2114,
	1658,
	2833,
	1670,
	3388,
	2760,
	3388,
	3358,
	3336,
	3388,
	3336,
	3361,
	2812,
	2791,
	1670,
	2114,
	1658,
	2833,
	1670,
	3388,
	2760,
	3388,
	3358,
	3336,
	3388,
	3336,
	3361,
	2812,
	1670,
	1670,
	3388,
	1670,
	3388,
	1670,
	3388,
	3361,
	2812,
	1670,
	1670,
	3388,
	3388,
	3361,
	3361,
	3388,
	3388,
	3388,
	3388,
	3388,
	3388,
	1651,
	3388,
	3388,
	3388,
	3388,
	3388,
	3388,
	2760,
	3388,
	3388,
	3388,
	3388,
	2760,
	3388,
	3388,
	3388,
	3388,
	3388,
	3388,
	3388,
	3388,
	3388,
	3388,
	3388,
	2791,
	3388,
	3336,
	3388,
	2760,
	3388,
	3388,
	4537,
	3388,
	3388,
	3388,
	2791,
	3388,
	3388,
	5246,
	4727,
	3388,
	3388,
	3388,
	3388,
	3388,
	3388,
	3388,
	3388,
	3388,
	3388,
	3388,
	2760,
	2760,
	2760,
	3388,
	3388,
	3388,
	3388,
	3388,
	3388,
	3388,
	3388,
	3388,
	3388,
	2760,
	3388,
	3388,
	3388,
	3388,
	3388,
	3388,
	3388,
	15,
	3388,
	3388,
	3388,
	3388,
	3388,
	5206,
	5206,
	5206,
	5206,
	3471,
	1620,
	15,
	9,
	2791,
	3388,
	3388,
	3388,
	3388,
	15,
	3388,
	3388,
	3388,
	3388,
	3388,
	5206,
	5206,
	5206,
	5206,
	3471,
	1620,
	15,
	9,
	2791,
	3388,
	3388,
	3388,
	3388,
	15,
	3388,
	3388,
	3388,
	3388,
	3388,
	5206,
	5206,
	5206,
	5206,
	3471,
	1620,
	15,
	9,
	2791,
	3388,
	3388,
	3388,
	3388,
	15,
	3388,
	3388,
	3388,
	3388,
	3388,
	5206,
	5206,
	5206,
	5206,
	3471,
	1620,
	15,
	9,
	2791,
	3388,
	3388,
	3388,
	3388,
	15,
	3388,
	3388,
	3388,
	3388,
	3388,
	5206,
	5206,
	5206,
	5206,
	3471,
	1620,
	15,
	9,
	2791,
	3388,
	3388,
	3388,
	3388,
	15,
	3388,
	3388,
	3388,
	3388,
	3388,
	5206,
	5206,
	5206,
	5206,
	3471,
	1620,
	15,
	9,
	2791,
	3388,
	3388,
	3388,
	3388,
	15,
	3388,
	3388,
	3388,
	3388,
	3388,
	5206,
	5206,
	5206,
	5206,
	3471,
	1620,
	15,
	9,
	2791,
	3388,
	3388,
	3388,
	3388,
	15,
	3388,
	3388,
	3388,
	3388,
	3388,
	5206,
	5206,
	5206,
	5206,
	3471,
	1620,
	15,
	9,
	2791,
	3388,
	3388,
	3388,
	3388,
	8,
	3388,
	3388,
	3388,
	3388,
	3388,
	5206,
	5206,
	5206,
	5206,
	3464,
	1620,
	8,
	7,
	2791,
	3388,
	3388,
	3388,
	3388,
	15,
	3388,
	3388,
	3388,
	3388,
	3388,
	5206,
	5206,
	5206,
	5206,
	3471,
	1620,
	15,
	9,
	2791,
	3388,
	3388,
	3388,
	3388,
	23,
	3388,
	3388,
	3388,
	3388,
	3388,
	5206,
	5206,
	5206,
	5206,
	3477,
	1620,
	23,
	11,
	2791,
	230,
	5206,
	5206,
	5206,
	5206,
	3517,
	1620,
	81,
	24,
	2791,
	5206,
	5206,
	5206,
	5206,
	3497,
	1620,
	57,
	19,
	2791,
	5206,
	5206,
	5206,
	5206,
	5189,
	1620,
	2760,
	804,
	2791,
	3388,
	141,
	57,
	3388,
	3388,
	3388,
	3388,
	641,
	3388,
	3388,
	3388,
	3388,
	3388,
	5206,
	5206,
	5206,
	5206,
	4072,
	1620,
	641,
	152,
	2791,
	5206,
	5206,
	5206,
	5206,
	3550,
	1620,
	115,
	32,
	2791,
	3388,
	3361,
	3361,
	3388,
	3388,
	3388,
	115,
	3388,
	3388,
	3388,
	3388,
	3388,
	3388,
	3388,
	3388,
	3388,
	3388,
	3388,
	6,
	3388,
	5206,
	5206,
	5206,
	5206,
	3461,
	1620,
	6,
	2,
	2791,
	3388,
	3388,
	3388,
	3388,
	3388,
	3388,
	3388,
	3388,
	16,
	3388,
	5206,
	5206,
	5206,
	5206,
	3472,
	1620,
	16,
	10,
	2791,
	5206,
	5206,
	5206,
	5206,
	3625,
	1620,
	192,
	62,
	2791,
	5206,
	5206,
	5206,
	5206,
	5211,
	1620,
	2812,
	839,
	2791,
	3388,
	3388,
	3388,
	3388,
	2812,
	2812,
	2826,
	3388,
	3388,
	2812,
	192,
	2812,
	3388,
	3388,
	3388,
	3361,
	3361,
	3388,
	2812,
	2812,
	2812,
	943,
	3388,
	3388,
	1368,
	3388,
	3388,
	3388,
	3388,
	918,
	1332,
	1330,
	1330,
	358,
	20,
	112,
	930,
	935,
	3388,
};
static const Il2CppTokenRangePair s_rgctxIndices[1] = 
{
	{ 0x060000CD, { 0, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[2] = 
{
	{ (Il2CppRGCTXDataType)1, 158 },
	{ (Il2CppRGCTXDataType)2, 158 },
};
extern const CustomAttributesCacheGenerator g_MoreMountains_Feedbacks_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_MoreMountains_Feedbacks_CodeGenModule;
const Il2CppCodeGenModule g_MoreMountains_Feedbacks_CodeGenModule = 
{
	"MoreMountains.Feedbacks.dll",
	1110,
	s_methodPointers,
	1,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	1,
	s_rgctxIndices,
	2,
	s_rgctxValues,
	NULL,
	g_MoreMountains_Feedbacks_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
