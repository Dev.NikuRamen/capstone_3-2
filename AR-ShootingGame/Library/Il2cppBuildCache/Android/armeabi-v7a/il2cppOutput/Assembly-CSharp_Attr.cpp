﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// ARLocation.ConditionalPropertyAttribute
struct ConditionalPropertyAttribute_t541C921FB3DDA3968D4040FB64D66D010BFBE8EE;
// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// UnityEngine.DisallowMultipleComponent
struct DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E;
// UnityEngine.ExecuteAlways
struct ExecuteAlways_tF6C3132EB025F81EAA1C682801417AE96BEBF84B;
// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173;
// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC;
// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210;
// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB;
// UnityEngine.HelpURLAttribute
struct HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// MoreMountains.Feedbacks.MMFInformationAttribute
struct MMFInformationAttribute_t0C1DA3939625AA24735CF9454EAD9AED698F3DB2;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5;
// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// UnityEngine.SpaceAttribute
struct SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8;
// System.String
struct String_t;
// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* ARAnchorManager_t969330AB785F0DC41EF9F4390D77F7ABA30F7D0F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ARLocationOrientation_tD8A832D47ECB471D892449B5ADB4143078E1C8B7_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ARPlaneManager_t4700B0BC3E8B6CD35F8D925701C89A5A21DDBAD4_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ARPlaneMeshVisualizer_t4C981AECE943267AD0363C57F2F3D32273E932F2_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ARPlane_t6336725EC68CE9029844CBE72A7FE7374AD74891_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* LaserBeam_tFCC429A545E7674E750953FA65C49D7875347FF8_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CAutoDestroyU3Ed__4_t28203B1A36A0C012905840F27C0277804C0033B0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDestroyParticleU3Ed__20_t524359CC15BFCBE7B00549E3EE37DFDA5E23832D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CFadeOutU3Ed__3_t63A1FAFDE1FFC2013D402424D2B6DDE3897E7BD7_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CFillProcessU3Ed__3_t16C38ACC665B308ED772CE1D8D3D8590403963AD_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadSceneProcessU3Ed__10_tCEEEECB67F643ED087AAAB846276DF898F600AB1_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadXMLFileFromOverpassRequestU3Ed__10_tAE0DCE7DD96AAA881075718C9F601829ECD843F6_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CMoveToU3Ed__11_t39BD7A569BA53E9B5CD4627EBFD3C0B37147628F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CProgrammedDeathU3Ed__3_t66AB9997CF14E4C726CFF62F618BBFC4DABC3C14_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CRestoreWorldFromServerU3Ed__14_t1BC195D258F6C9A677CFE33012A10FED77E61B13_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSaveWorldToServerU3Ed__15_tFDB6D1F6FE0DEE26967CAF125CED4C9F5FDA9C0B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CShakeU3Ed__5_t4D78750F530E974ECA1E718C81F0AC4A03284EB6_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSpawnDelayManagerU3Ed__9_tBD67D61B15F4EBCCAE1FD83AE18597F0882C5AE0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartDissolveU3Ed__18_t696122E2D0CFB06601A00FF2F088544EFCF6A760_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartFaceInU3Ed__8_t5FE7CDF6AF05E40079784B4E2ACFD51D58837AA0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartFadeOutU3Ed__9_tEA7B6452D46BF558C6DC7A2365C52EC47E8A199F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartInitU3Ed__18_t701C9769307CB8A0DACFE4676F2E948FE47FB66A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartLaserShootU3Ed__11_t31E05FDA8294BBDFDE83519C5A8FE302183C7FFF_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__11_t923C741C5866E5117C2111EE5A4180E589AB1FB9_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__13_tB5AD52B13DBA13A7D2D366C50585F21D7A9A8937_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__20_tBC91C2BA3682FCE4567CEAB39669F7C789CF1525_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__50_tE495468A4B52BE5F74CE7E3FFBEDAB0D1C021E74_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__89_tA04EB5BAC5E34AFA5EF0368C59EA9782780DF767_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartWorldU3Ed__19_t750B4DEF83B7D2F7C41FA78DDB0224ED46CDDA47_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CUpdateRayU3Ed__9_tAF42C0C52B4ACFDCDC70E2B1A1F726AA6756614C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitForLocationServicesU3Ed__12_t0CFACBF6935FDEC52C2AC8482C8A969EB746EB2C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitForLocationServicesU3Ed__25_tB371C83EDE76677BE0191100440D26A065116399_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* WorldBuilder_t691400967398BB82C3F8221E09896EE668F3CC77_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.AddComponentMenu::m_AddComponentMenu
	String_t* ___m_AddComponentMenu_0;
	// System.Int32 UnityEngine.AddComponentMenu::m_Ordering
	int32_t ___m_Ordering_1;

public:
	inline static int32_t get_offset_of_m_AddComponentMenu_0() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_AddComponentMenu_0)); }
	inline String_t* get_m_AddComponentMenu_0() const { return ___m_AddComponentMenu_0; }
	inline String_t** get_address_of_m_AddComponentMenu_0() { return &___m_AddComponentMenu_0; }
	inline void set_m_AddComponentMenu_0(String_t* value)
	{
		___m_AddComponentMenu_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AddComponentMenu_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Ordering_1() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_Ordering_1)); }
	inline int32_t get_m_Ordering_1() const { return ___m_Ordering_1; }
	inline int32_t* get_address_of_m_Ordering_1() { return &___m_Ordering_1; }
	inline void set_m_Ordering_1(int32_t value)
	{
		___m_Ordering_1 = value;
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.CreateAssetMenuAttribute::<menuName>k__BackingField
	String_t* ___U3CmenuNameU3Ek__BackingField_0;
	// System.String UnityEngine.CreateAssetMenuAttribute::<fileName>k__BackingField
	String_t* ___U3CfileNameU3Ek__BackingField_1;
	// System.Int32 UnityEngine.CreateAssetMenuAttribute::<order>k__BackingField
	int32_t ___U3CorderU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CmenuNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CmenuNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CmenuNameU3Ek__BackingField_0() const { return ___U3CmenuNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CmenuNameU3Ek__BackingField_0() { return &___U3CmenuNameU3Ek__BackingField_0; }
	inline void set_U3CmenuNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CmenuNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CmenuNameU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CfileNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CfileNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CfileNameU3Ek__BackingField_1() const { return ___U3CfileNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CfileNameU3Ek__BackingField_1() { return &___U3CfileNameU3Ek__BackingField_1; }
	inline void set_U3CfileNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CfileNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CfileNameU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CorderU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CorderU3Ek__BackingField_2)); }
	inline int32_t get_U3CorderU3Ek__BackingField_2() const { return ___U3CorderU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CorderU3Ek__BackingField_2() { return &___U3CorderU3Ek__BackingField_2; }
	inline void set_U3CorderU3Ek__BackingField_2(int32_t value)
	{
		___U3CorderU3Ek__BackingField_2 = value;
	}
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.DisallowMultipleComponent
struct DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// UnityEngine.ExecuteAlways
struct ExecuteAlways_tF6C3132EB025F81EAA1C682801417AE96BEBF84B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.Serialization.FormerlySerializedAsAttribute::m_oldName
	String_t* ___m_oldName_0;

public:
	inline static int32_t get_offset_of_m_oldName_0() { return static_cast<int32_t>(offsetof(FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210, ___m_oldName_0)); }
	inline String_t* get_m_oldName_0() const { return ___m_oldName_0; }
	inline String_t** get_address_of_m_oldName_0() { return &___m_oldName_0; }
	inline void set_m_oldName_0(String_t* value)
	{
		___m_oldName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_oldName_0), (void*)value);
	}
};


// UnityEngine.HelpURLAttribute
struct HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.HelpURLAttribute::m_Url
	String_t* ___m_Url_0;
	// System.Boolean UnityEngine.HelpURLAttribute::m_Dispatcher
	bool ___m_Dispatcher_1;
	// System.String UnityEngine.HelpURLAttribute::m_DispatchingFieldName
	String_t* ___m_DispatchingFieldName_2;

public:
	inline static int32_t get_offset_of_m_Url_0() { return static_cast<int32_t>(offsetof(HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023, ___m_Url_0)); }
	inline String_t* get_m_Url_0() const { return ___m_Url_0; }
	inline String_t** get_address_of_m_Url_0() { return &___m_Url_0; }
	inline void set_m_Url_0(String_t* value)
	{
		___m_Url_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Url_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Dispatcher_1() { return static_cast<int32_t>(offsetof(HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023, ___m_Dispatcher_1)); }
	inline bool get_m_Dispatcher_1() const { return ___m_Dispatcher_1; }
	inline bool* get_address_of_m_Dispatcher_1() { return &___m_Dispatcher_1; }
	inline void set_m_Dispatcher_1(bool value)
	{
		___m_Dispatcher_1 = value;
	}

	inline static int32_t get_offset_of_m_DispatchingFieldName_2() { return static_cast<int32_t>(offsetof(HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023, ___m_DispatchingFieldName_2)); }
	inline String_t* get_m_DispatchingFieldName_2() const { return ___m_DispatchingFieldName_2; }
	inline String_t** get_address_of_m_DispatchingFieldName_2() { return &___m_DispatchingFieldName_2; }
	inline void set_m_DispatchingFieldName_2(String_t* value)
	{
		___m_DispatchingFieldName_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DispatchingFieldName_2), (void*)value);
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type UnityEngine.RequireComponent::m_Type0
	Type_t * ___m_Type0_0;
	// System.Type UnityEngine.RequireComponent::m_Type1
	Type_t * ___m_Type1_1;
	// System.Type UnityEngine.RequireComponent::m_Type2
	Type_t * ___m_Type2_2;

public:
	inline static int32_t get_offset_of_m_Type0_0() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type0_0)); }
	inline Type_t * get_m_Type0_0() const { return ___m_Type0_0; }
	inline Type_t ** get_address_of_m_Type0_0() { return &___m_Type0_0; }
	inline void set_m_Type0_0(Type_t * value)
	{
		___m_Type0_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type0_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type1_1() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type1_1)); }
	inline Type_t * get_m_Type1_1() const { return ___m_Type1_1; }
	inline Type_t ** get_address_of_m_Type1_1() { return &___m_Type1_1; }
	inline void set_m_Type1_1(Type_t * value)
	{
		___m_Type1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type1_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type2_2() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type2_2)); }
	inline Type_t * get_m_Type2_2() const { return ___m_Type2_2; }
	inline Type_t ** get_address_of_m_Type2_2() { return &___m_Type2_2; }
	inline void set_m_Type2_2(Type_t * value)
	{
		___m_Type2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type2_2), (void*)value);
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// ARLocation.ConditionalPropertyAttribute
struct ConditionalPropertyAttribute_t541C921FB3DDA3968D4040FB64D66D010BFBE8EE  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String ARLocation.ConditionalPropertyAttribute::Name
	String_t* ___Name_0;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(ConditionalPropertyAttribute_t541C921FB3DDA3968D4040FB64D66D010BFBE8EE, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Name_0), (void*)value);
	}
};


// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.HeaderAttribute::header
	String_t* ___header_0;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___header_0), (void*)value);
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// MoreMountains.Feedbacks.MMFInformationAttribute
struct MMFInformationAttribute_t0C1DA3939625AA24735CF9454EAD9AED698F3DB2  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:

public:
};


// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.RangeAttribute::min
	float ___min_0;
	// System.Single UnityEngine.RangeAttribute::max
	float ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___max_1)); }
	inline float get_max_1() const { return ___max_1; }
	inline float* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(float value)
	{
		___max_1 = value;
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.SpaceAttribute
struct SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.SpaceAttribute::height
	float ___height_0;

public:
	inline static int32_t get_offset_of_height_0() { return static_cast<int32_t>(offsetof(SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8, ___height_0)); }
	inline float get_height_0() const { return ___height_0; }
	inline float* get_address_of_height_0() { return &___height_0; }
	inline void set_height_0(float value)
	{
		___height_0 = value;
	}
};


// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.TooltipAttribute::tooltip
	String_t* ___tooltip_0;

public:
	inline static int32_t get_offset_of_tooltip_0() { return static_cast<int32_t>(offsetof(TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B, ___tooltip_0)); }
	inline String_t* get_tooltip_0() const { return ___tooltip_0; }
	inline String_t** get_address_of_tooltip_0() { return &___tooltip_0; }
	inline void set_tooltip_0(String_t* value)
	{
		___tooltip_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tooltip_0), (void*)value);
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.Feedbacks.MMFInformationAttribute/InformationType
struct InformationType_t2B47BCDF424923E78151463EC97AC1ECF6FFCA31 
{
public:
	// System.Int32 MoreMountains.Feedbacks.MMFInformationAttribute/InformationType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InformationType_t2B47BCDF424923E78151463EC97AC1ECF6FFCA31, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.HeaderAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * __this, String_t* ___header0, const RuntimeMethod* method);
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042 (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * __this, String_t* ___tooltip0, const RuntimeMethod* method);
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4 (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * __this, Type_t * ___requiredComponent0, const RuntimeMethod* method);
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000 (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * __this, float ___min0, float ___max1, const RuntimeMethod* method);
// System.Void UnityEngine.RequireComponent::.ctor(System.Type,System.Type,System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequireComponent__ctor_m1D311DE3AB4EF7FE3D80292644967218D92B00B6 (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * __this, Type_t * ___requiredComponent0, Type_t * ___requiredComponent21, Type_t * ___requiredComponent32, const RuntimeMethod* method);
// System.Void UnityEngine.ExecuteAlways::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExecuteAlways__ctor_mDB73D23637E65E57DE87C7BAAFE4CE694AE9BEE0 (ExecuteAlways_tF6C3132EB025F81EAA1C682801417AE96BEBF84B * __this, const RuntimeMethod* method);
// System.Void MoreMountains.Feedbacks.MMFInformationAttribute::.ctor(System.String,MoreMountains.Feedbacks.MMFInformationAttribute/InformationType,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMFInformationAttribute__ctor_m004E74EBE93920E7BAC370DDB10E141EDE1A073D (MMFInformationAttribute_t0C1DA3939625AA24735CF9454EAD9AED698F3DB2 * __this, String_t* ___message0, int32_t ___type1, bool ___messageAfterProperty2, const RuntimeMethod* method);
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549 (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * __this, String_t* ___menuName0, const RuntimeMethod* method);
// System.Void UnityEngine.DisallowMultipleComponent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * __this, const RuntimeMethod* method);
// System.Void UnityEngine.HelpURLAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215 (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * __this, String_t* ___url0, const RuntimeMethod* method);
// System.Void UnityEngine.Serialization.FormerlySerializedAsAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * __this, String_t* ___oldName0, const RuntimeMethod* method);
// System.Void UnityEngine.SpaceAttribute::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44 (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * __this, float ___height0, const RuntimeMethod* method);
// System.Void ARLocation.ConditionalPropertyAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConditionalPropertyAttribute__ctor_mF13EDBB8FE80AC495C46C1452521FD585EE33F14 (ConditionalPropertyAttribute_t541C921FB3DDA3968D4040FB64D66D010BFBE8EE * __this, String_t* ___name0, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65 (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_fileName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_menuName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ExecuteInEditMode::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * __this, const RuntimeMethod* method);
static void AssemblyU2DCSharp_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[0];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[1];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[2];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[3];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
}
static void MonsterSearchGuide_t025127A275D8313DF60BB06111D6C87DB4B5A45A_CustomAttributesCacheGenerator_monsterSpawner(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameStartLevelController_t3CFD56CCADF2433004AE1FA5A8A5671E88CD2E24_CustomAttributesCacheGenerator_bgm(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HitEffectManager_tB12BA6F3DD666F29762CD90844BA038F3072A639_CustomAttributesCacheGenerator_hitEffects(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AudioController_tA9F9625D78D7B82E16202C1EBD62C8CA84CE7B5E_CustomAttributesCacheGenerator_audioSource(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_fadeInImage(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_fadeInTermTime(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_audioController(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_GameManager_StartFaceIn_m15A60C69C251EF3A37CCA8793C77609A8A86966D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartFaceInU3Ed__8_t5FE7CDF6AF05E40079784B4E2ACFD51D58837AA0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartFaceInU3Ed__8_t5FE7CDF6AF05E40079784B4E2ACFD51D58837AA0_0_0_0_var), NULL);
	}
}
static void GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_GameManager_StartFadeOut_mD8D5CE92B440CFDD05E2FB89ED79487742F0110E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartFadeOutU3Ed__9_tEA7B6452D46BF558C6DC7A2365C52EC47E8A199F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartFadeOutU3Ed__9_tEA7B6452D46BF558C6DC7A2365C52EC47E8A199F_0_0_0_var), NULL);
	}
}
static void GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_GameManager_LoadSceneProcess_m574AD3E5A678433A5997F7C60E9427F42286F498(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadSceneProcessU3Ed__10_tCEEEECB67F643ED087AAAB846276DF898F600AB1_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CLoadSceneProcessU3Ed__10_tCEEEECB67F643ED087AAAB846276DF898F600AB1_0_0_0_var), NULL);
	}
}
static void U3CStartFaceInU3Ed__8_t5FE7CDF6AF05E40079784B4E2ACFD51D58837AA0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartFaceInU3Ed__8_t5FE7CDF6AF05E40079784B4E2ACFD51D58837AA0_CustomAttributesCacheGenerator_U3CStartFaceInU3Ed__8__ctor_m7079818A07DBC0BD97D6E94739307F77D0F3283E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartFaceInU3Ed__8_t5FE7CDF6AF05E40079784B4E2ACFD51D58837AA0_CustomAttributesCacheGenerator_U3CStartFaceInU3Ed__8_System_IDisposable_Dispose_m87D16B124428B7770442C4A23E7D82B172B53F61(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartFaceInU3Ed__8_t5FE7CDF6AF05E40079784B4E2ACFD51D58837AA0_CustomAttributesCacheGenerator_U3CStartFaceInU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m65C44ECA4865FFD05184B4F3EE2330C8DFCD3A0B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartFaceInU3Ed__8_t5FE7CDF6AF05E40079784B4E2ACFD51D58837AA0_CustomAttributesCacheGenerator_U3CStartFaceInU3Ed__8_System_Collections_IEnumerator_Reset_mE52774F682FA16F3053C7E9AE62376B4710908AE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartFaceInU3Ed__8_t5FE7CDF6AF05E40079784B4E2ACFD51D58837AA0_CustomAttributesCacheGenerator_U3CStartFaceInU3Ed__8_System_Collections_IEnumerator_get_Current_m49C57D84E2BF0FAC0417A795CD9616B96574DA0C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartFadeOutU3Ed__9_tEA7B6452D46BF558C6DC7A2365C52EC47E8A199F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartFadeOutU3Ed__9_tEA7B6452D46BF558C6DC7A2365C52EC47E8A199F_CustomAttributesCacheGenerator_U3CStartFadeOutU3Ed__9__ctor_mD648679059EC0F73970223E316119756120FD93F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartFadeOutU3Ed__9_tEA7B6452D46BF558C6DC7A2365C52EC47E8A199F_CustomAttributesCacheGenerator_U3CStartFadeOutU3Ed__9_System_IDisposable_Dispose_m001FF71F492E1CCD1899CEE42795509435E7B5D7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartFadeOutU3Ed__9_tEA7B6452D46BF558C6DC7A2365C52EC47E8A199F_CustomAttributesCacheGenerator_U3CStartFadeOutU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m00E311E91E75B2493F96B8E68E278FEB80AE7BD3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartFadeOutU3Ed__9_tEA7B6452D46BF558C6DC7A2365C52EC47E8A199F_CustomAttributesCacheGenerator_U3CStartFadeOutU3Ed__9_System_Collections_IEnumerator_Reset_mDAAADDFC83B651B0174B00CAD21E68E516300398(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartFadeOutU3Ed__9_tEA7B6452D46BF558C6DC7A2365C52EC47E8A199F_CustomAttributesCacheGenerator_U3CStartFadeOutU3Ed__9_System_Collections_IEnumerator_get_Current_mCFA324023124B8EDD082C20F612189052FA4B9D8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadSceneProcessU3Ed__10_tCEEEECB67F643ED087AAAB846276DF898F600AB1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadSceneProcessU3Ed__10_tCEEEECB67F643ED087AAAB846276DF898F600AB1_CustomAttributesCacheGenerator_U3CLoadSceneProcessU3Ed__10__ctor_m6CDB34BE216CB09481787D2ED703468BE64D7332(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadSceneProcessU3Ed__10_tCEEEECB67F643ED087AAAB846276DF898F600AB1_CustomAttributesCacheGenerator_U3CLoadSceneProcessU3Ed__10_System_IDisposable_Dispose_m5A6F97FC6505E83BBB8C8CBF7470F607FE916B4C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadSceneProcessU3Ed__10_tCEEEECB67F643ED087AAAB846276DF898F600AB1_CustomAttributesCacheGenerator_U3CLoadSceneProcessU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCF0C7DF2C64DA02082A7A2A74DE722D39B1E0204(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadSceneProcessU3Ed__10_tCEEEECB67F643ED087AAAB846276DF898F600AB1_CustomAttributesCacheGenerator_U3CLoadSceneProcessU3Ed__10_System_Collections_IEnumerator_Reset_m509E561AF662F37FDE93B813CE99DBD9A90F55D3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadSceneProcessU3Ed__10_tCEEEECB67F643ED087AAAB846276DF898F600AB1_CustomAttributesCacheGenerator_U3CLoadSceneProcessU3Ed__10_System_Collections_IEnumerator_get_Current_m586C86E1C8409A486D4A254D98BC2A9B9B44A5DF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void LevelManager_t010B312A2B35B45291F58195216ABB5673174961_CustomAttributesCacheGenerator_monsterSpawner(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x6E\x73\x74\x65\x72\x20\x73\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
}
static void LevelManager_t010B312A2B35B45291F58195216ABB5673174961_CustomAttributesCacheGenerator_gameTime(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x79\x73\x74\x65\x6D\x20\x73\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LevelManager_t010B312A2B35B45291F58195216ABB5673174961_CustomAttributesCacheGenerator_colors(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LevelManager_t010B312A2B35B45291F58195216ABB5673174961_CustomAttributesCacheGenerator_ui_gamePlayRemainTimeText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x55\x49\x20\x73\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
}
static void LevelManager_t010B312A2B35B45291F58195216ABB5673174961_CustomAttributesCacheGenerator_ui_scoreText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LevelManager_t010B312A2B35B45291F58195216ABB5673174961_CustomAttributesCacheGenerator_colorChangeButtonImage(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LevelManager_t010B312A2B35B45291F58195216ABB5673174961_CustomAttributesCacheGenerator_gameOverUI(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LevelManager_t010B312A2B35B45291F58195216ABB5673174961_CustomAttributesCacheGenerator_LevelManager_StartInit_m30C8D1DF952A4EC8846D1832B160EF8D3D24CF54(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartInitU3Ed__18_t701C9769307CB8A0DACFE4676F2E948FE47FB66A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartInitU3Ed__18_t701C9769307CB8A0DACFE4676F2E948FE47FB66A_0_0_0_var), NULL);
	}
}
static void U3CStartInitU3Ed__18_t701C9769307CB8A0DACFE4676F2E948FE47FB66A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartInitU3Ed__18_t701C9769307CB8A0DACFE4676F2E948FE47FB66A_CustomAttributesCacheGenerator_U3CStartInitU3Ed__18__ctor_mD86FB48F2D277AF855C85A0D385274DE0F09F48B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartInitU3Ed__18_t701C9769307CB8A0DACFE4676F2E948FE47FB66A_CustomAttributesCacheGenerator_U3CStartInitU3Ed__18_System_IDisposable_Dispose_m4D19E1481379C5652564FCB7D292FDB76B331223(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartInitU3Ed__18_t701C9769307CB8A0DACFE4676F2E948FE47FB66A_CustomAttributesCacheGenerator_U3CStartInitU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC75C338E7B5727677BA7086AA0E6756C733B5D81(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartInitU3Ed__18_t701C9769307CB8A0DACFE4676F2E948FE47FB66A_CustomAttributesCacheGenerator_U3CStartInitU3Ed__18_System_Collections_IEnumerator_Reset_mFA4BED517684325FC5B7F229B7192C2D505E4C1B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartInitU3Ed__18_t701C9769307CB8A0DACFE4676F2E948FE47FB66A_CustomAttributesCacheGenerator_U3CStartInitU3Ed__18_System_Collections_IEnumerator_get_Current_m66DE071D48938C290236DBC667A0E07C9A8BDEAA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void MonsterSpawner_t38B0F58A8A9657860995DDF676E764E0DEC57B4D_CustomAttributesCacheGenerator_spawnObjects(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x70\x61\x77\x6E\x20\x6F\x70\x74\x69\x6F\x6E"), NULL);
	}
}
static void MonsterSpawner_t38B0F58A8A9657860995DDF676E764E0DEC57B4D_CustomAttributesCacheGenerator_spawnDelay(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x27\x73\x20\x6D\x75\x73\x74\x20\x62\x65\x20\x73\x65\x63\x6F\x6E\x64\x73"), NULL);
	}
}
static void MonsterSpawner_t38B0F58A8A9657860995DDF676E764E0DEC57B4D_CustomAttributesCacheGenerator_spawnZone(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MonsterSpawner_t38B0F58A8A9657860995DDF676E764E0DEC57B4D_CustomAttributesCacheGenerator_U3CSpawnedMonstersU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonsterSpawner_t38B0F58A8A9657860995DDF676E764E0DEC57B4D_CustomAttributesCacheGenerator_MonsterSpawner_get_SpawnedMonsters_mAD9C0B8FB1E2B2DD7128C57173C9240DB08BF1DE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonsterSpawner_t38B0F58A8A9657860995DDF676E764E0DEC57B4D_CustomAttributesCacheGenerator_MonsterSpawner_SpawnDelayManager_m82B84B029BB88920F85390D4989FD584D047922E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSpawnDelayManagerU3Ed__9_tBD67D61B15F4EBCCAE1FD83AE18597F0882C5AE0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSpawnDelayManagerU3Ed__9_tBD67D61B15F4EBCCAE1FD83AE18597F0882C5AE0_0_0_0_var), NULL);
	}
}
static void U3CSpawnDelayManagerU3Ed__9_tBD67D61B15F4EBCCAE1FD83AE18597F0882C5AE0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSpawnDelayManagerU3Ed__9_tBD67D61B15F4EBCCAE1FD83AE18597F0882C5AE0_CustomAttributesCacheGenerator_U3CSpawnDelayManagerU3Ed__9__ctor_m1B28413DB68191AC508C0FC77639CD3D9AD0F716(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnDelayManagerU3Ed__9_tBD67D61B15F4EBCCAE1FD83AE18597F0882C5AE0_CustomAttributesCacheGenerator_U3CSpawnDelayManagerU3Ed__9_System_IDisposable_Dispose_m69CADD0F5D17F0CBFDDE131227961C2DE15D40A0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnDelayManagerU3Ed__9_tBD67D61B15F4EBCCAE1FD83AE18597F0882C5AE0_CustomAttributesCacheGenerator_U3CSpawnDelayManagerU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3F415A5E8D0B5B05E0F9C4149AB92892F29B2354(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnDelayManagerU3Ed__9_tBD67D61B15F4EBCCAE1FD83AE18597F0882C5AE0_CustomAttributesCacheGenerator_U3CSpawnDelayManagerU3Ed__9_System_Collections_IEnumerator_Reset_m2ABA99903FDC4FD7C8184B096BAF4A4317D3D33A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnDelayManagerU3Ed__9_tBD67D61B15F4EBCCAE1FD83AE18597F0882C5AE0_CustomAttributesCacheGenerator_U3CSpawnDelayManagerU3Ed__9_System_Collections_IEnumerator_get_Current_m1842BD1717ECA5B3DF59F106E04C05420B638460(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void AnimationSettings_t3D14F6560132A34377F0519400B1FB1C606BFBE6_CustomAttributesCacheGenerator_dieAnimationTag(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AnimationSettings_t3D14F6560132A34377F0519400B1FB1C606BFBE6_CustomAttributesCacheGenerator_hitAnimationTag(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MonsterBase_t2CD611A20C74AAF093EE6B851712E7FFD2052E29_CustomAttributesCacheGenerator_placeAtLocation(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x6E\x73\x74\x65\x72\x20\x47\x50\x53\x20\x53\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
}
static void MonsterBase_t2CD611A20C74AAF093EE6B851712E7FFD2052E29_CustomAttributesCacheGenerator_spawnArea(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MonsterBase_t2CD611A20C74AAF093EE6B851712E7FFD2052E29_CustomAttributesCacheGenerator_hp(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x6E\x73\x74\x65\x72\x20\x73\x74\x61\x74\x75\x73"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MonsterBase_t2CD611A20C74AAF093EE6B851712E7FFD2052E29_CustomAttributesCacheGenerator_colorType(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MonsterBase_t2CD611A20C74AAF093EE6B851712E7FFD2052E29_CustomAttributesCacheGenerator_score(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MonsterBase_t2CD611A20C74AAF093EE6B851712E7FFD2052E29_CustomAttributesCacheGenerator_animator(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\xEC\x95\xA0\xEB\x8B\x88\xEB\xA9\x94\xEC\x9D\xB4\xEC\x85\x98\x20\xEC\x84\xA4\xEC\xA0\x95"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MonsterBase_t2CD611A20C74AAF093EE6B851712E7FFD2052E29_CustomAttributesCacheGenerator_animationSettings(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MonsterBase_t2CD611A20C74AAF093EE6B851712E7FFD2052E29_CustomAttributesCacheGenerator_dieSFX(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x46\x58"), NULL);
	}
}
static void MonsterBase_t2CD611A20C74AAF093EE6B851712E7FFD2052E29_CustomAttributesCacheGenerator_dieVFX(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x56\x46\x58"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MonsterBase_t2CD611A20C74AAF093EE6B851712E7FFD2052E29_CustomAttributesCacheGenerator_MonsterBase_StartDissolve_m7F85FF286A481851C26D95445EA2390B64CBEA58(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartDissolveU3Ed__18_t696122E2D0CFB06601A00FF2F088544EFCF6A760_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartDissolveU3Ed__18_t696122E2D0CFB06601A00FF2F088544EFCF6A760_0_0_0_var), NULL);
	}
}
static void U3CStartDissolveU3Ed__18_t696122E2D0CFB06601A00FF2F088544EFCF6A760_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartDissolveU3Ed__18_t696122E2D0CFB06601A00FF2F088544EFCF6A760_CustomAttributesCacheGenerator_U3CStartDissolveU3Ed__18__ctor_m99EBB6373EA227790398DF072DC7FDFFF4E9984F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartDissolveU3Ed__18_t696122E2D0CFB06601A00FF2F088544EFCF6A760_CustomAttributesCacheGenerator_U3CStartDissolveU3Ed__18_System_IDisposable_Dispose_m4DCDAA5EF9E0BE416FE650B81177B3141D3DEE00(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartDissolveU3Ed__18_t696122E2D0CFB06601A00FF2F088544EFCF6A760_CustomAttributesCacheGenerator_U3CStartDissolveU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA692E30108097D34B9F38D48BE9272E3526DAE28(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartDissolveU3Ed__18_t696122E2D0CFB06601A00FF2F088544EFCF6A760_CustomAttributesCacheGenerator_U3CStartDissolveU3Ed__18_System_Collections_IEnumerator_Reset_mAA39C36CA2D5936B8EEC9958D1CA3C37DE121F45(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartDissolveU3Ed__18_t696122E2D0CFB06601A00FF2F088544EFCF6A760_CustomAttributesCacheGenerator_U3CStartDissolveU3Ed__18_System_Collections_IEnumerator_get_Current_m2C53CBA6E4244BAAEA1DCED9E9887F4B3D5E33AD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void LaserAttack_t3D280ADBBA2C6DFD7F8B5D9EC9B3B44B5A4DAE91_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LaserBeam_tFCC429A545E7674E750953FA65C49D7875347FF8_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(LaserBeam_tFCC429A545E7674E750953FA65C49D7875347FF8_0_0_0_var), NULL);
	}
}
static void LaserAttack_t3D280ADBBA2C6DFD7F8B5D9EC9B3B44B5A4DAE91_CustomAttributesCacheGenerator_damage(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\xEB\xAA\xAC\xEC\x8A\xA4\xED\x84\xB0\xEB\xA5\xBC\x20\xEA\xB3\xB5\xEA\xB2\xA9\xED\x95\x98\xEB\x8A\x94\x20\xEB\x8D\xB0\xEB\xAF\xB8\xEC\xA7\x80"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[2];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x61\x73\x65\x72\x20\x73\x74\x61\x74\x75\x73"), NULL);
	}
}
static void LaserAttack_t3D280ADBBA2C6DFD7F8B5D9EC9B3B44B5A4DAE91_CustomAttributesCacheGenerator_shootDelay(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 10.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[2];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\xEB\xAA\xAC\xEC\x8A\xA4\xED\x84\xB0\xEB\xA5\xBC\x20\xEA\xB3\xB5\xEA\xB2\xA9\xED\x95\xA0\xEB\x95\x8C\x20\xEA\xB3\xB5\xEA\xB2\xA9\x20\xEC\x82\xAC\xEC\x9D\xB4\xEC\x9D\x98\x20\xED\x85\x80\x28\x73\x65\x63\x29"), NULL);
	}
}
static void LaserAttack_t3D280ADBBA2C6DFD7F8B5D9EC9B3B44B5A4DAE91_CustomAttributesCacheGenerator_laserBeam(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LaserAttack_t3D280ADBBA2C6DFD7F8B5D9EC9B3B44B5A4DAE91_CustomAttributesCacheGenerator_remainGauge(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x55\x49"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LaserAttack_t3D280ADBBA2C6DFD7F8B5D9EC9B3B44B5A4DAE91_CustomAttributesCacheGenerator_uiCrossHair(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LaserAttack_t3D280ADBBA2C6DFD7F8B5D9EC9B3B44B5A4DAE91_CustomAttributesCacheGenerator_fireSFX(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x46\x58"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LaserAttack_t3D280ADBBA2C6DFD7F8B5D9EC9B3B44B5A4DAE91_CustomAttributesCacheGenerator_LaserAttack_StartLaserShoot_mAE771AC815D2FDA14C49DC1145F4BD36F7B2F493(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartLaserShootU3Ed__11_t31E05FDA8294BBDFDE83519C5A8FE302183C7FFF_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartLaserShootU3Ed__11_t31E05FDA8294BBDFDE83519C5A8FE302183C7FFF_0_0_0_var), NULL);
	}
}
static void U3CStartLaserShootU3Ed__11_t31E05FDA8294BBDFDE83519C5A8FE302183C7FFF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartLaserShootU3Ed__11_t31E05FDA8294BBDFDE83519C5A8FE302183C7FFF_CustomAttributesCacheGenerator_U3CStartLaserShootU3Ed__11__ctor_mE1534AE9457A2F2983970899550161C0E1961184(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartLaserShootU3Ed__11_t31E05FDA8294BBDFDE83519C5A8FE302183C7FFF_CustomAttributesCacheGenerator_U3CStartLaserShootU3Ed__11_System_IDisposable_Dispose_mC16771D60C05E1F8CD1FDEEB4AC8608D5FE29AA5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartLaserShootU3Ed__11_t31E05FDA8294BBDFDE83519C5A8FE302183C7FFF_CustomAttributesCacheGenerator_U3CStartLaserShootU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7B5A398A3400E5CCA15ABF1DEF0DDACEE19D6B71(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartLaserShootU3Ed__11_t31E05FDA8294BBDFDE83519C5A8FE302183C7FFF_CustomAttributesCacheGenerator_U3CStartLaserShootU3Ed__11_System_Collections_IEnumerator_Reset_m8D5A5C60E01AAD6CC3C744000836C43B2C7125D1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartLaserShootU3Ed__11_t31E05FDA8294BBDFDE83519C5A8FE302183C7FFF_CustomAttributesCacheGenerator_U3CStartLaserShootU3Ed__11_System_Collections_IEnumerator_get_Current_m1D86B570876F58018D35CE675211107522217022(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void LaserBeam_tFCC429A545E7674E750953FA65C49D7875347FF8_CustomAttributesCacheGenerator_laserMaxDistance(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1000.0f, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6D\x65\x72\x61\x20\x73\x65\x74\x74\x69\x6E\x67"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DontDestroySingleton_1_tF85C94A33AEC78EC5129B6356600771BA9FB3508_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DontDestroySingleton_1_tF85C94A33AEC78EC5129B6356600771BA9FB3508_CustomAttributesCacheGenerator_DontDestroySingleton_1_get_Instance_mF832FA16C7100E8644E59EA113527E60FC5D34AA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DontDestroySingleton_1_tF85C94A33AEC78EC5129B6356600771BA9FB3508_CustomAttributesCacheGenerator_DontDestroySingleton_1_set_Instance_m716DF2AA008DC4D2D89A9284FDEA76F741EA98AC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SingletonMonoBehaviour_1_t13D4D87A01B2D33C83A7C1C9BD55C27324B9A877_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SingletonMonoBehaviour_1_t13D4D87A01B2D33C83A7C1C9BD55C27324B9A877_CustomAttributesCacheGenerator_SingletonMonoBehaviour_1_get_Instance_m3BBD71535D10CA29F0F813F1B99682503FB35FDB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SingletonMonoBehaviour_1_t13D4D87A01B2D33C83A7C1C9BD55C27324B9A877_CustomAttributesCacheGenerator_SingletonMonoBehaviour_1_set_Instance_mF964E17F2E86911D27D3E802B6825FA31FC38A16(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UI_CrossHair_tD67FA151EE8A866801215A40CBAAA389049CB4AB_CustomAttributesCacheGenerator_animator(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UI_CrossHair_tD67FA151EE8A866801215A40CBAAA389049CB4AB_CustomAttributesCacheGenerator_fireTag(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UI_GameOver_tA9DB42DA7F622B778079C6A0344AFA05F976554A_CustomAttributesCacheGenerator_mainMenuSceneName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x65\x6E\x65"), NULL);
	}
}
static void UI_GameOver_tA9DB42DA7F622B778079C6A0344AFA05F976554A_CustomAttributesCacheGenerator_gameSceneName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UI_GameOver_tA9DB42DA7F622B778079C6A0344AFA05F976554A_CustomAttributesCacheGenerator_finalScoreText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x55\x49"), NULL);
	}
}
static void UI_GameStart_t0FE28BA7187B7BDEEC30ED88A6E09157DE0BCB04_CustomAttributesCacheGenerator_gameSceneName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UI_RemainGauge_t7496C063310AB0F2BBB293AF8B0C4DF905F98BB7_CustomAttributesCacheGenerator_fillImage(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UI_RemainGauge_t7496C063310AB0F2BBB293AF8B0C4DF905F98BB7_CustomAttributesCacheGenerator_UI_RemainGauge_FillProcess_m420FCBD4AEB281F14DE1A1CCA2CD68C432358317(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CFillProcessU3Ed__3_t16C38ACC665B308ED772CE1D8D3D8590403963AD_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CFillProcessU3Ed__3_t16C38ACC665B308ED772CE1D8D3D8590403963AD_0_0_0_var), NULL);
	}
}
static void U3CFillProcessU3Ed__3_t16C38ACC665B308ED772CE1D8D3D8590403963AD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CFillProcessU3Ed__3_t16C38ACC665B308ED772CE1D8D3D8590403963AD_CustomAttributesCacheGenerator_U3CFillProcessU3Ed__3__ctor_m7533C355B52D97ACE9EFDB6A90F00FF6CDC7F342(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFillProcessU3Ed__3_t16C38ACC665B308ED772CE1D8D3D8590403963AD_CustomAttributesCacheGenerator_U3CFillProcessU3Ed__3_System_IDisposable_Dispose_m7734513A2A74F692A53C9EB8F90139016CB9911E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFillProcessU3Ed__3_t16C38ACC665B308ED772CE1D8D3D8590403963AD_CustomAttributesCacheGenerator_U3CFillProcessU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAE2833C5C61CAF11477658332A2503E083FECF6C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFillProcessU3Ed__3_t16C38ACC665B308ED772CE1D8D3D8590403963AD_CustomAttributesCacheGenerator_U3CFillProcessU3Ed__3_System_Collections_IEnumerator_Reset_m9EA26AD85A50E88B0FF1BA54FFE34DFC8340A108(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFillProcessU3Ed__3_t16C38ACC665B308ED772CE1D8D3D8590403963AD_CustomAttributesCacheGenerator_U3CFillProcessU3Ed__3_System_Collections_IEnumerator_get_Current_mB798384D19B7F28181D405A208D539B2EDB14DBB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void EffectController_tF93D81123371E3D7BEB00D86028212D6B180D99D_CustomAttributesCacheGenerator_particle(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EffectController_tF93D81123371E3D7BEB00D86028212D6B180D99D_CustomAttributesCacheGenerator_isDestroyable(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EffectController_tF93D81123371E3D7BEB00D86028212D6B180D99D_CustomAttributesCacheGenerator_EffectController_AutoDestroy_m5F1D4BBC35C44EECE5484E85385D7115EEC6D83C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAutoDestroyU3Ed__4_t28203B1A36A0C012905840F27C0277804C0033B0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CAutoDestroyU3Ed__4_t28203B1A36A0C012905840F27C0277804C0033B0_0_0_0_var), NULL);
	}
}
static void U3CAutoDestroyU3Ed__4_t28203B1A36A0C012905840F27C0277804C0033B0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAutoDestroyU3Ed__4_t28203B1A36A0C012905840F27C0277804C0033B0_CustomAttributesCacheGenerator_U3CAutoDestroyU3Ed__4__ctor_m3DE476309B549C7CA7EB4B061196F501A1018FDC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAutoDestroyU3Ed__4_t28203B1A36A0C012905840F27C0277804C0033B0_CustomAttributesCacheGenerator_U3CAutoDestroyU3Ed__4_System_IDisposable_Dispose_m433D1E5FB0B56EEA1B6313DAABB81F3950E2FE98(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAutoDestroyU3Ed__4_t28203B1A36A0C012905840F27C0277804C0033B0_CustomAttributesCacheGenerator_U3CAutoDestroyU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5F2B4316E69BC244BAFCBAFA3E88254FC6681645(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAutoDestroyU3Ed__4_t28203B1A36A0C012905840F27C0277804C0033B0_CustomAttributesCacheGenerator_U3CAutoDestroyU3Ed__4_System_Collections_IEnumerator_Reset_m61ECC122D0F85642C100FA58F3A6760D04468BD7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAutoDestroyU3Ed__4_t28203B1A36A0C012905840F27C0277804C0033B0_CustomAttributesCacheGenerator_U3CAutoDestroyU3Ed__4_System_Collections_IEnumerator_get_Current_mE0F3602906D6150EC37D93C40AED9868B925FD18(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void _TEST_Laser_t363B81B7173489F49DDBB73EC2D2A04CF947045A_CustomAttributesCacheGenerator_laserAttack(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CameraShakeSimpleScript_t948411B88D6F43F322BCE831BCF7E5405604259A_CustomAttributesCacheGenerator_CameraShakeSimpleScript_Shake_m42597BD58B7F3D9BC32D60DAB652F6440FC63587(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CShakeU3Ed__5_t4D78750F530E974ECA1E718C81F0AC4A03284EB6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CShakeU3Ed__5_t4D78750F530E974ECA1E718C81F0AC4A03284EB6_0_0_0_var), NULL);
	}
}
static void U3CShakeU3Ed__5_t4D78750F530E974ECA1E718C81F0AC4A03284EB6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CShakeU3Ed__5_t4D78750F530E974ECA1E718C81F0AC4A03284EB6_CustomAttributesCacheGenerator_U3CShakeU3Ed__5__ctor_m5D29E1AE6E04D92D16729B96B800C084006CB5D4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShakeU3Ed__5_t4D78750F530E974ECA1E718C81F0AC4A03284EB6_CustomAttributesCacheGenerator_U3CShakeU3Ed__5_System_IDisposable_Dispose_mC4197FEC180AA9152C9C30B75E072D8A38432F20(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShakeU3Ed__5_t4D78750F530E974ECA1E718C81F0AC4A03284EB6_CustomAttributesCacheGenerator_U3CShakeU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9688D01BD1B73DA02BCB535D7C43C2BA45DAF9DE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShakeU3Ed__5_t4D78750F530E974ECA1E718C81F0AC4A03284EB6_CustomAttributesCacheGenerator_U3CShakeU3Ed__5_System_Collections_IEnumerator_Reset_m84CD7308C3D7700156AADBB3E5FD3ECAB36CE0A1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShakeU3Ed__5_t4D78750F530E974ECA1E718C81F0AC4A03284EB6_CustomAttributesCacheGenerator_U3CShakeU3Ed__5_System_Collections_IEnumerator_get_Current_m15B1182C1BF75FAD4CAFB859C4AAB9FAE6D87FB8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ProjectileMoveScript_t8E5328E659143159EA6BDE95FD65EC7A674B237E_CustomAttributesCacheGenerator_accuracy(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x46\x72\x6F\x6D\x20\x30\x25\x20\x74\x6F\x20\x31\x30\x30\x25"), NULL);
	}
}
static void ProjectileMoveScript_t8E5328E659143159EA6BDE95FD65EC7A674B237E_CustomAttributesCacheGenerator_ProjectileMoveScript_DestroyParticle_mDC85A6227258B0E71CA43EA5947AEA8532005258(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDestroyParticleU3Ed__20_t524359CC15BFCBE7B00549E3EE37DFDA5E23832D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDestroyParticleU3Ed__20_t524359CC15BFCBE7B00549E3EE37DFDA5E23832D_0_0_0_var), NULL);
	}
}
static void U3CDestroyParticleU3Ed__20_t524359CC15BFCBE7B00549E3EE37DFDA5E23832D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDestroyParticleU3Ed__20_t524359CC15BFCBE7B00549E3EE37DFDA5E23832D_CustomAttributesCacheGenerator_U3CDestroyParticleU3Ed__20__ctor_m6CCEA1B0192AE584C47ED3E95E22C535B3B7B65C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDestroyParticleU3Ed__20_t524359CC15BFCBE7B00549E3EE37DFDA5E23832D_CustomAttributesCacheGenerator_U3CDestroyParticleU3Ed__20_System_IDisposable_Dispose_mF9AF76E483AE14F67683EB3011C23AC1FEAC05EB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDestroyParticleU3Ed__20_t524359CC15BFCBE7B00549E3EE37DFDA5E23832D_CustomAttributesCacheGenerator_U3CDestroyParticleU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBB0FD3EDEFE94591609CC8EB62D6533867F7C2EA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDestroyParticleU3Ed__20_t524359CC15BFCBE7B00549E3EE37DFDA5E23832D_CustomAttributesCacheGenerator_U3CDestroyParticleU3Ed__20_System_Collections_IEnumerator_Reset_m1D2E2E07966F4FEE5D3215EE446DC2EF3EBA05D9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDestroyParticleU3Ed__20_t524359CC15BFCBE7B00549E3EE37DFDA5E23832D_CustomAttributesCacheGenerator_U3CDestroyParticleU3Ed__20_System_Collections_IEnumerator_get_Current_mCC511785DBF4831A2F138071E4344C80A8B93AEB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void RotateToMouseScript_t484F9391662B1A307753F7AE65E5DC8BDB6764E9_CustomAttributesCacheGenerator_RotateToMouseScript_UpdateRay_m189BAF74F72D7118405984C537604D8BE919F383(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CUpdateRayU3Ed__9_tAF42C0C52B4ACFDCDC70E2B1A1F726AA6756614C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CUpdateRayU3Ed__9_tAF42C0C52B4ACFDCDC70E2B1A1F726AA6756614C_0_0_0_var), NULL);
	}
}
static void U3CUpdateRayU3Ed__9_tAF42C0C52B4ACFDCDC70E2B1A1F726AA6756614C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CUpdateRayU3Ed__9_tAF42C0C52B4ACFDCDC70E2B1A1F726AA6756614C_CustomAttributesCacheGenerator_U3CUpdateRayU3Ed__9__ctor_m3C424ED7532D389836FDAC227F621D2561122C80(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUpdateRayU3Ed__9_tAF42C0C52B4ACFDCDC70E2B1A1F726AA6756614C_CustomAttributesCacheGenerator_U3CUpdateRayU3Ed__9_System_IDisposable_Dispose_m18B52E52265964D921527AB34EF9B2D69B377033(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUpdateRayU3Ed__9_tAF42C0C52B4ACFDCDC70E2B1A1F726AA6756614C_CustomAttributesCacheGenerator_U3CUpdateRayU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0DC77B6D548F360FF981E72C8AB1930A2E066091(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUpdateRayU3Ed__9_tAF42C0C52B4ACFDCDC70E2B1A1F726AA6756614C_CustomAttributesCacheGenerator_U3CUpdateRayU3Ed__9_System_Collections_IEnumerator_Reset_m25D63F3001C98BE581B5B039926E0EAF8B406774(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUpdateRayU3Ed__9_tAF42C0C52B4ACFDCDC70E2B1A1F726AA6756614C_CustomAttributesCacheGenerator_U3CUpdateRayU3Ed__9_System_Collections_IEnumerator_get_Current_mDA0F9789BA24799B4B1A8BE256F36C6C4BCA517B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ARSessionStateExtensions_tF98CE3C37471E7FDFDA1B89FD81C123E7D62E789_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ARSessionStateExtensions_tF98CE3C37471E7FDFDA1B89FD81C123E7D62E789_CustomAttributesCacheGenerator_ARSessionStateExtensions_ToInfoString_m3ED78BE2349594A91DD2BDB4D17D5FC7A39E0E2A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ARFeatheredPlaneMeshVisualizer_t7B2E22615047B9FDFC4D5015F59BFE2A17E3140E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ARPlaneMeshVisualizer_t4C981AECE943267AD0363C57F2F3D32273E932F2_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ARPlane_t6336725EC68CE9029844CBE72A7FE7374AD74891_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m1D311DE3AB4EF7FE3D80292644967218D92B00B6(tmp, il2cpp_codegen_type_get_object(ARPlaneMeshVisualizer_t4C981AECE943267AD0363C57F2F3D32273E932F2_0_0_0_var), il2cpp_codegen_type_get_object(MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var), il2cpp_codegen_type_get_object(ARPlane_t6336725EC68CE9029844CBE72A7FE7374AD74891_0_0_0_var), NULL);
	}
}
static void ARFeatheredPlaneMeshVisualizer_t7B2E22615047B9FDFC4D5015F59BFE2A17E3140E_CustomAttributesCacheGenerator_m_FeatheringWidth(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x77\x69\x64\x74\x68\x20\x6F\x66\x20\x74\x68\x65\x20\x74\x65\x78\x74\x75\x72\x65\x20\x66\x65\x61\x74\x68\x65\x72\x69\x6E\x67\x20\x28\x69\x6E\x20\x77\x6F\x72\x6C\x64\x20\x75\x6E\x69\x74\x73\x29\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AnchorCreator_t97C108ACEDFC02ECBB6B4923EB6576F99050773C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ARAnchorManager_t969330AB785F0DC41EF9F4390D77F7ABA30F7D0F_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ARPlaneManager_t4700B0BC3E8B6CD35F8D925701C89A5A21DDBAD4_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ARAnchorManager_t969330AB785F0DC41EF9F4390D77F7ABA30F7D0F_0_0_0_var), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F_0_0_0_var), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[2];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ARPlaneManager_t4700B0BC3E8B6CD35F8D925701C89A5A21DDBAD4_0_0_0_var), NULL);
	}
}
static void AnchorCreator_t97C108ACEDFC02ECBB6B4923EB6576F99050773C_CustomAttributesCacheGenerator_m_AnchorPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PanelControlNeon_t38394C83EE1A0D50CF165CB0FA261C243ACA2461_CustomAttributesCacheGenerator_panels(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PanelControlNeon_t38394C83EE1A0D50CF165CB0FA261C243ACA2461_CustomAttributesCacheGenerator_panelTransform(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PanelControlNeon_t38394C83EE1A0D50CF165CB0FA261C243ACA2461_CustomAttributesCacheGenerator_buttonPrev(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PanelControlNeon_t38394C83EE1A0D50CF165CB0FA261C243ACA2461_CustomAttributesCacheGenerator_buttonNext(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PanelNeon_t24F392CF6472DBA84E376C555D546B5913E5D21B_CustomAttributesCacheGenerator_otherPanels(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DemoBall_t2DC8C97097BDF12123C9ECFAA6E1926345A2BC28_CustomAttributesCacheGenerator_DemoBall_ProgrammedDeath_m2DF4845C32A8963BAB27D7F0BFACC5784712BE92(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CProgrammedDeathU3Ed__3_t66AB9997CF14E4C726CFF62F618BBFC4DABC3C14_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CProgrammedDeathU3Ed__3_t66AB9997CF14E4C726CFF62F618BBFC4DABC3C14_0_0_0_var), NULL);
	}
}
static void U3CProgrammedDeathU3Ed__3_t66AB9997CF14E4C726CFF62F618BBFC4DABC3C14_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CProgrammedDeathU3Ed__3_t66AB9997CF14E4C726CFF62F618BBFC4DABC3C14_CustomAttributesCacheGenerator_U3CProgrammedDeathU3Ed__3__ctor_m594CF3FB53C5D8B5ED586479D34EE72E4943BF2F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CProgrammedDeathU3Ed__3_t66AB9997CF14E4C726CFF62F618BBFC4DABC3C14_CustomAttributesCacheGenerator_U3CProgrammedDeathU3Ed__3_System_IDisposable_Dispose_m543CD3EBD9CAC69065A27E5644411AFE8B6C7B6A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CProgrammedDeathU3Ed__3_t66AB9997CF14E4C726CFF62F618BBFC4DABC3C14_CustomAttributesCacheGenerator_U3CProgrammedDeathU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC82FC1704348F2794A1AEB9E8C43335F3189EDEA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CProgrammedDeathU3Ed__3_t66AB9997CF14E4C726CFF62F618BBFC4DABC3C14_CustomAttributesCacheGenerator_U3CProgrammedDeathU3Ed__3_System_Collections_IEnumerator_Reset_m62594DC3054FFC736C7286487579485F6501901F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CProgrammedDeathU3Ed__3_t66AB9997CF14E4C726CFF62F618BBFC4DABC3C14_CustomAttributesCacheGenerator_U3CProgrammedDeathU3Ed__3_System_Collections_IEnumerator_get_Current_mA0F24F509E8DF2CC5413AC39750F9F5B91179303(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void DemoButton_t137F4A23D860DA55E2C4A5505168231367D2FF2C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteAlways_tF6C3132EB025F81EAA1C682801417AE96BEBF84B * tmp = (ExecuteAlways_tF6C3132EB025F81EAA1C682801417AE96BEBF84B *)cache->attributes[0];
		ExecuteAlways__ctor_mDB73D23637E65E57DE87C7BAAFE4CE694AE9BEE0(tmp, NULL);
	}
}
static void DemoButton_t137F4A23D860DA55E2C4A5505168231367D2FF2C_CustomAttributesCacheGenerator_NotSupportedInWebGL(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x65\x68\x61\x76\x69\x6F\x75\x72"), NULL);
	}
}
static void DemoButton_t137F4A23D860DA55E2C4A5505168231367D2FF2C_CustomAttributesCacheGenerator_TargetButton(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x69\x6E\x64\x69\x6E\x67\x73"), NULL);
	}
}
static void DemoPackageTester_t0907E9A97CEA5E17DFB6E5E5670F5D1E60D00DD5_CustomAttributesCacheGenerator_RequiresPostProcessing(CustomAttributesCache* cache)
{
	{
		MMFInformationAttribute_t0C1DA3939625AA24735CF9454EAD9AED698F3DB2 * tmp = (MMFInformationAttribute_t0C1DA3939625AA24735CF9454EAD9AED698F3DB2 *)cache->attributes[0];
		MMFInformationAttribute__ctor_m004E74EBE93920E7BAC370DDB10E141EDE1A073D(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x69\x73\x20\x6F\x6E\x6C\x79\x20\x75\x73\x65\x64\x20\x74\x6F\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x61\x6E\x20\x65\x72\x72\x6F\x72\x20\x69\x6E\x20\x74\x68\x65\x20\x63\x6F\x6E\x73\x6F\x6C\x65\x20\x69\x6E\x20\x63\x61\x73\x65\x20\x64\x65\x70\x65\x6E\x64\x65\x6E\x63\x69\x65\x73\x20\x66\x6F\x72\x20\x74\x68\x69\x73\x20\x64\x65\x6D\x6F\x20\x68\x61\x76\x65\x6E\x27\x74\x20\x62\x65\x65\x6E\x20\x69\x6E\x73\x74\x61\x6C\x6C\x65\x64\x2E\x20\x59\x6F\x75\x20\x63\x61\x6E\x20\x73\x61\x66\x65\x6C\x79\x20\x72\x65\x6D\x6F\x76\x65\x20\x69\x74\x20\x69\x66\x20\x79\x6F\x75\x20\x77\x61\x6E\x74\x2C\x20\x61\x6E\x64\x20\x74\x79\x70\x69\x63\x61\x6C\x6C\x79\x20\x79\x6F\x75\x20\x77\x6F\x75\x6C\x64\x6E\x27\x74\x20\x77\x61\x6E\x74\x20\x74\x6F\x20\x6B\x65\x65\x70\x20\x74\x68\x61\x74\x20\x69\x6E\x20\x79\x6F\x75\x72\x20\x6F\x77\x6E\x20\x67\x61\x6D\x65\x2E"), 3LL, false, NULL);
	}
}
static void WorldBuilder_t691400967398BB82C3F8221E09896EE668F3CC77_CustomAttributesCacheGenerator_WorldBuilder_Start_mF5C8668B8938138597B9481C6EE54912A6D003C6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__11_t923C741C5866E5117C2111EE5A4180E589AB1FB9_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__11_t923C741C5866E5117C2111EE5A4180E589AB1FB9_0_0_0_var), NULL);
	}
}
static void WorldBuilder_t691400967398BB82C3F8221E09896EE668F3CC77_CustomAttributesCacheGenerator_WorldBuilder_WaitForLocationServices_mAD680BF0BABC411653D36076744A658D2CDB7F88(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitForLocationServicesU3Ed__12_t0CFACBF6935FDEC52C2AC8482C8A969EB746EB2C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitForLocationServicesU3Ed__12_t0CFACBF6935FDEC52C2AC8482C8A969EB746EB2C_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__11_t923C741C5866E5117C2111EE5A4180E589AB1FB9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__11_t923C741C5866E5117C2111EE5A4180E589AB1FB9_CustomAttributesCacheGenerator_U3CStartU3Ed__11__ctor_mEB9F8E9FB8863FCA856C475CE3953732827C45C7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__11_t923C741C5866E5117C2111EE5A4180E589AB1FB9_CustomAttributesCacheGenerator_U3CStartU3Ed__11_System_IDisposable_Dispose_m6CF5F632049E8A079C6149D25FE882EC0CF02BEB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__11_t923C741C5866E5117C2111EE5A4180E589AB1FB9_CustomAttributesCacheGenerator_U3CStartU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD1F8834D7A88F6C1E72CA8F700BDA9843C2D2760(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__11_t923C741C5866E5117C2111EE5A4180E589AB1FB9_CustomAttributesCacheGenerator_U3CStartU3Ed__11_System_Collections_IEnumerator_Reset_mF722C6C6D5BD5A2DA52B63B58E6839696D07AD6E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__11_t923C741C5866E5117C2111EE5A4180E589AB1FB9_CustomAttributesCacheGenerator_U3CStartU3Ed__11_System_Collections_IEnumerator_get_Current_m9995E0F4AADFE599BC6EE841F982DCF470C58B12(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForLocationServicesU3Ed__12_t0CFACBF6935FDEC52C2AC8482C8A969EB746EB2C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForLocationServicesU3Ed__12_t0CFACBF6935FDEC52C2AC8482C8A969EB746EB2C_CustomAttributesCacheGenerator_U3CWaitForLocationServicesU3Ed__12__ctor_mB0E081157FC6F80947552EAD52001420488B82B5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForLocationServicesU3Ed__12_t0CFACBF6935FDEC52C2AC8482C8A969EB746EB2C_CustomAttributesCacheGenerator_U3CWaitForLocationServicesU3Ed__12_System_IDisposable_Dispose_m5CAE00BE382E2B0ECBEF52701C19C94DA0DF77CF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForLocationServicesU3Ed__12_t0CFACBF6935FDEC52C2AC8482C8A969EB746EB2C_CustomAttributesCacheGenerator_U3CWaitForLocationServicesU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m362C4F91E9E792C76526D2E013841DF893902A65(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForLocationServicesU3Ed__12_t0CFACBF6935FDEC52C2AC8482C8A969EB746EB2C_CustomAttributesCacheGenerator_U3CWaitForLocationServicesU3Ed__12_System_Collections_IEnumerator_Reset_m10F9138D89CBDC158E7ADC150DCA060A55C3FB91(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForLocationServicesU3Ed__12_t0CFACBF6935FDEC52C2AC8482C8A969EB746EB2C_CustomAttributesCacheGenerator_U3CWaitForLocationServicesU3Ed__12_System_Collections_IEnumerator_get_Current_m001F487FE1FEE0A266039C66B648E70740B30C46(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec_t6560C12F9DDDBA0BE759D1E97EC142B084520DE1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WorldBuilderApplicationController_tFE48840B225954BFB9782D9799DAFF218C391228_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WorldBuilder_t691400967398BB82C3F8221E09896EE668F3CC77_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(WorldBuilder_t691400967398BB82C3F8221E09896EE668F3CC77_0_0_0_var), NULL);
	}
}
static void WorldBuilderApplicationController_tFE48840B225954BFB9782D9799DAFF218C391228_CustomAttributesCacheGenerator_WorldBuilderApplicationController_Start_m7BF24802EEBFD8E5ACA68C3E5FF3874A4F566BF2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__13_tB5AD52B13DBA13A7D2D366C50585F21D7A9A8937_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__13_tB5AD52B13DBA13A7D2D366C50585F21D7A9A8937_0_0_0_var), NULL);
	}
}
static void WorldBuilderApplicationController_tFE48840B225954BFB9782D9799DAFF218C391228_CustomAttributesCacheGenerator_WorldBuilderApplicationController_RestoreWorldFromServer_mA563DAEBCA8262FF90B99CF632F40A6D373822DF(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CRestoreWorldFromServerU3Ed__14_t1BC195D258F6C9A677CFE33012A10FED77E61B13_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CRestoreWorldFromServerU3Ed__14_t1BC195D258F6C9A677CFE33012A10FED77E61B13_0_0_0_var), NULL);
	}
}
static void WorldBuilderApplicationController_tFE48840B225954BFB9782D9799DAFF218C391228_CustomAttributesCacheGenerator_WorldBuilderApplicationController_SaveWorldToServer_mFC353BC5E5FB6C6A95D58CC10B63E490A33AC0D1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSaveWorldToServerU3Ed__15_tFDB6D1F6FE0DEE26967CAF125CED4C9F5FDA9C0B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSaveWorldToServerU3Ed__15_tFDB6D1F6FE0DEE26967CAF125CED4C9F5FDA9C0B_0_0_0_var), NULL);
	}
}
static void WorldBuilderApplicationController_tFE48840B225954BFB9782D9799DAFF218C391228_CustomAttributesCacheGenerator_WorldBuilderApplicationController_U3CInitListenersU3Eb__20_0_mC871FB82C6B59363A8E17E80EA63841B13003815(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WorldBuilderApplicationController_tFE48840B225954BFB9782D9799DAFF218C391228_CustomAttributesCacheGenerator_WorldBuilderApplicationController_U3CInitListenersU3Eb__20_1_mF8DFC4421B87BB2D906802A705C3833A7BBE8473(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WorldBuilderApplicationController_tFE48840B225954BFB9782D9799DAFF218C391228_CustomAttributesCacheGenerator_WorldBuilderApplicationController_U3CInitListenersU3Eb__20_2_m9DC706A88153D75FBB7A30F81AA9218271DDFDFC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WorldBuilderApplicationController_tFE48840B225954BFB9782D9799DAFF218C391228_CustomAttributesCacheGenerator_WorldBuilderApplicationController_U3CInitListenersU3Eb__20_3_mEA2DD74846E7886EA6536091896FA4A80B764DE6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WorldBuilderApplicationController_tFE48840B225954BFB9782D9799DAFF218C391228_CustomAttributesCacheGenerator_WorldBuilderApplicationController_U3CInitListenersU3Eb__20_4_m508550143EDA38FDF68C6667595A85CE4E69B0EF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WorldBuilderApplicationController_tFE48840B225954BFB9782D9799DAFF218C391228_CustomAttributesCacheGenerator_WorldBuilderApplicationController_U3CInitListenersU3Eb__20_5_mEDD0A78086F101395F38061E004564D447FBB1D4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WorldBuilderApplicationController_tFE48840B225954BFB9782D9799DAFF218C391228_CustomAttributesCacheGenerator_WorldBuilderApplicationController_U3CInitListenersU3Eb__20_6_mAA2BBB581B0F52725AEF21D88D79BBEA0C30C7C7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WorldBuilderApplicationController_tFE48840B225954BFB9782D9799DAFF218C391228_CustomAttributesCacheGenerator_WorldBuilderApplicationController_U3CInitListenersU3Eb__20_7_m807A32FBC85802214180E9E098C63F559C3BFD48(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WorldBuilderApplicationController_tFE48840B225954BFB9782D9799DAFF218C391228_CustomAttributesCacheGenerator_WorldBuilderApplicationController_U3CInitListenersU3Eb__20_8_m7492559B333393C3B25E10B3C35785CCC79E6860(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__13_tB5AD52B13DBA13A7D2D366C50585F21D7A9A8937_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__13_tB5AD52B13DBA13A7D2D366C50585F21D7A9A8937_CustomAttributesCacheGenerator_U3CStartU3Ed__13__ctor_m06D9792956694C6F63DE017842934D761D75FBE8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__13_tB5AD52B13DBA13A7D2D366C50585F21D7A9A8937_CustomAttributesCacheGenerator_U3CStartU3Ed__13_System_IDisposable_Dispose_mBF3B890289A394AE9CAF99B4DCDABDE522DAD059(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__13_tB5AD52B13DBA13A7D2D366C50585F21D7A9A8937_CustomAttributesCacheGenerator_U3CStartU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6200AF3D9A6BBF0809F460CE0B6C783C7B7DCB02(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__13_tB5AD52B13DBA13A7D2D366C50585F21D7A9A8937_CustomAttributesCacheGenerator_U3CStartU3Ed__13_System_Collections_IEnumerator_Reset_m40C07F31A227594C80E097030B3EDEF2C3B22F7F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__13_tB5AD52B13DBA13A7D2D366C50585F21D7A9A8937_CustomAttributesCacheGenerator_U3CStartU3Ed__13_System_Collections_IEnumerator_get_Current_m4C7C637169F0DF843ED9C5F8D911C4CD3014AF8B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRestoreWorldFromServerU3Ed__14_t1BC195D258F6C9A677CFE33012A10FED77E61B13_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRestoreWorldFromServerU3Ed__14_t1BC195D258F6C9A677CFE33012A10FED77E61B13_CustomAttributesCacheGenerator_U3CRestoreWorldFromServerU3Ed__14__ctor_mF3F4A3FDBDCF050E861ED1E3A6E9DEC5D715EB2D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRestoreWorldFromServerU3Ed__14_t1BC195D258F6C9A677CFE33012A10FED77E61B13_CustomAttributesCacheGenerator_U3CRestoreWorldFromServerU3Ed__14_System_IDisposable_Dispose_mB5F202DC734ACF01C484046EEEBAE8A48B2EACB9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRestoreWorldFromServerU3Ed__14_t1BC195D258F6C9A677CFE33012A10FED77E61B13_CustomAttributesCacheGenerator_U3CRestoreWorldFromServerU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD42336260FEAA43388A9AF7A040C6F042DBD3486(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRestoreWorldFromServerU3Ed__14_t1BC195D258F6C9A677CFE33012A10FED77E61B13_CustomAttributesCacheGenerator_U3CRestoreWorldFromServerU3Ed__14_System_Collections_IEnumerator_Reset_m38F3FA13DD5E07C58EA9E82377624F6AFBF35429(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRestoreWorldFromServerU3Ed__14_t1BC195D258F6C9A677CFE33012A10FED77E61B13_CustomAttributesCacheGenerator_U3CRestoreWorldFromServerU3Ed__14_System_Collections_IEnumerator_get_Current_mA605986B2D56E29D9FD72231E3B50AF445F37D32(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSaveWorldToServerU3Ed__15_tFDB6D1F6FE0DEE26967CAF125CED4C9F5FDA9C0B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSaveWorldToServerU3Ed__15_tFDB6D1F6FE0DEE26967CAF125CED4C9F5FDA9C0B_CustomAttributesCacheGenerator_U3CSaveWorldToServerU3Ed__15__ctor_m14D5CC1F2CC507AE1FA6D45CFD5DDA75E24F117C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSaveWorldToServerU3Ed__15_tFDB6D1F6FE0DEE26967CAF125CED4C9F5FDA9C0B_CustomAttributesCacheGenerator_U3CSaveWorldToServerU3Ed__15_System_IDisposable_Dispose_mC570EBAAD87479C60DE2D0E67F80E49B43535249(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSaveWorldToServerU3Ed__15_tFDB6D1F6FE0DEE26967CAF125CED4C9F5FDA9C0B_CustomAttributesCacheGenerator_U3CSaveWorldToServerU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m670E216A9319DE58D0BF21D1793D304B4B97C121(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSaveWorldToServerU3Ed__15_tFDB6D1F6FE0DEE26967CAF125CED4C9F5FDA9C0B_CustomAttributesCacheGenerator_U3CSaveWorldToServerU3Ed__15_System_Collections_IEnumerator_Reset_m1650694EF7B9228311E87AB511ED902BDF99B2E9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSaveWorldToServerU3Ed__15_tFDB6D1F6FE0DEE26967CAF125CED4C9F5FDA9C0B_CustomAttributesCacheGenerator_U3CSaveWorldToServerU3Ed__15_System_Collections_IEnumerator_get_Current_mE8BDB1F28C86F245BCACBAAAD2C152F342754615(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass23_0_t37D6D155705469CD530CA3E37BA55CC6F01E0AF2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WorldVoxelController_tBE60EC4C79077AEED7070B50ACC3D6E28ACCD504_CustomAttributesCacheGenerator_WorldVoxelController_StartWorld_mA7C7545E75A4B6739CE61C519D402BD39522EF81(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartWorldU3Ed__19_t750B4DEF83B7D2F7C41FA78DDB0224ED46CDDA47_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartWorldU3Ed__19_t750B4DEF83B7D2F7C41FA78DDB0224ED46CDDA47_0_0_0_var), NULL);
	}
}
static void WorldVoxelController_tBE60EC4C79077AEED7070B50ACC3D6E28ACCD504_CustomAttributesCacheGenerator_WorldVoxelController_Start_m162E8257FD63959A45673E21BA2DB4B6DECE089F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__20_tBC91C2BA3682FCE4567CEAB39669F7C789CF1525_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__20_tBC91C2BA3682FCE4567CEAB39669F7C789CF1525_0_0_0_var), NULL);
	}
}
static void WorldVoxelController_tBE60EC4C79077AEED7070B50ACC3D6E28ACCD504_CustomAttributesCacheGenerator_WorldVoxelController_WaitForLocationServices_m588EAE02EEB489DA9D369AF56BC651D4A0863CE9(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitForLocationServicesU3Ed__25_tB371C83EDE76677BE0191100440D26A065116399_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitForLocationServicesU3Ed__25_tB371C83EDE76677BE0191100440D26A065116399_0_0_0_var), NULL);
	}
}
static void WorldVoxelController_tBE60EC4C79077AEED7070B50ACC3D6E28ACCD504_CustomAttributesCacheGenerator_WorldVoxelController_U3CInitUiListenersU3Eb__39_0_mE40BF2B245962F81EE8C10668E40EF8B7CFB8134(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WorldVoxelController_tBE60EC4C79077AEED7070B50ACC3D6E28ACCD504_CustomAttributesCacheGenerator_WorldVoxelController_U3CInitUiListenersU3Eb__39_1_mAE7182B07E77340E71F56E8564EF486BB810AD0A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WorldVoxelController_tBE60EC4C79077AEED7070B50ACC3D6E28ACCD504_CustomAttributesCacheGenerator_WorldVoxelController_U3CInitUiListenersU3Eb__39_2_mE8D5158E3CD95CC70F8AD364470DB2CABF1E63A0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WorldVoxelController_tBE60EC4C79077AEED7070B50ACC3D6E28ACCD504_CustomAttributesCacheGenerator_WorldVoxelController_U3CInitUiListenersU3Eb__39_3_mF513E58BE82BB090DF1DAE6522BC7FCE36514ACB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WorldVoxelController_tBE60EC4C79077AEED7070B50ACC3D6E28ACCD504_CustomAttributesCacheGenerator_WorldVoxelController_U3CInitUiListenersU3Eb__39_4_mFC18641C18AEB5B13B49D67FBAB5939BD8CB0CFB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartWorldU3Ed__19_t750B4DEF83B7D2F7C41FA78DDB0224ED46CDDA47_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartWorldU3Ed__19_t750B4DEF83B7D2F7C41FA78DDB0224ED46CDDA47_CustomAttributesCacheGenerator_U3CStartWorldU3Ed__19__ctor_m44505E7226447BDBEB5254C492A76937A8E3DD79(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartWorldU3Ed__19_t750B4DEF83B7D2F7C41FA78DDB0224ED46CDDA47_CustomAttributesCacheGenerator_U3CStartWorldU3Ed__19_System_IDisposable_Dispose_mCAB48FFD6DE743461B606E763022F88271A9FD39(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartWorldU3Ed__19_t750B4DEF83B7D2F7C41FA78DDB0224ED46CDDA47_CustomAttributesCacheGenerator_U3CStartWorldU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m78DF8260192FAFE310E5360CD8EF02F3B43A7A0F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartWorldU3Ed__19_t750B4DEF83B7D2F7C41FA78DDB0224ED46CDDA47_CustomAttributesCacheGenerator_U3CStartWorldU3Ed__19_System_Collections_IEnumerator_Reset_m9F662F7B9C885672EFDD4222565A5E937C44C05C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartWorldU3Ed__19_t750B4DEF83B7D2F7C41FA78DDB0224ED46CDDA47_CustomAttributesCacheGenerator_U3CStartWorldU3Ed__19_System_Collections_IEnumerator_get_Current_m8BF6ABC362F36F4951247BF36FDCED71D631AD5D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__20_tBC91C2BA3682FCE4567CEAB39669F7C789CF1525_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__20_tBC91C2BA3682FCE4567CEAB39669F7C789CF1525_CustomAttributesCacheGenerator_U3CStartU3Ed__20__ctor_m7CA2FD67733BA04D3A2ACD7C80148A5469666BCC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__20_tBC91C2BA3682FCE4567CEAB39669F7C789CF1525_CustomAttributesCacheGenerator_U3CStartU3Ed__20_System_IDisposable_Dispose_m80FC7FA199E6EB9DAB383B481849F381D33FA439(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__20_tBC91C2BA3682FCE4567CEAB39669F7C789CF1525_CustomAttributesCacheGenerator_U3CStartU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m301E2F3CD89FEFBEA67102DADFF9EB0CC42FAE1C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__20_tBC91C2BA3682FCE4567CEAB39669F7C789CF1525_CustomAttributesCacheGenerator_U3CStartU3Ed__20_System_Collections_IEnumerator_Reset_m803FCDAF3A12C2A8CC7AE7B959C8A019F1D4CC59(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__20_tBC91C2BA3682FCE4567CEAB39669F7C789CF1525_CustomAttributesCacheGenerator_U3CStartU3Ed__20_System_Collections_IEnumerator_get_Current_m812D99C892C7C4A52DFA739A3C65BA559981F365(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForLocationServicesU3Ed__25_tB371C83EDE76677BE0191100440D26A065116399_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForLocationServicesU3Ed__25_tB371C83EDE76677BE0191100440D26A065116399_CustomAttributesCacheGenerator_U3CWaitForLocationServicesU3Ed__25__ctor_m1B02CDC690DCFEBCD395815B9C407A9A8DED5CD6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForLocationServicesU3Ed__25_tB371C83EDE76677BE0191100440D26A065116399_CustomAttributesCacheGenerator_U3CWaitForLocationServicesU3Ed__25_System_IDisposable_Dispose_m941288AD2EDFC591E0E75D745EEC5522E39D5A07(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForLocationServicesU3Ed__25_tB371C83EDE76677BE0191100440D26A065116399_CustomAttributesCacheGenerator_U3CWaitForLocationServicesU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBC6B654B63F027360A6713FC61A90B19D573798B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForLocationServicesU3Ed__25_tB371C83EDE76677BE0191100440D26A065116399_CustomAttributesCacheGenerator_U3CWaitForLocationServicesU3Ed__25_System_Collections_IEnumerator_Reset_mDA9FD6ADCE132254F59D30A14527991ACD9B4628(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForLocationServicesU3Ed__25_tB371C83EDE76677BE0191100440D26A065116399_CustomAttributesCacheGenerator_U3CWaitForLocationServicesU3Ed__25_System_Collections_IEnumerator_get_Current_mC431C45C29F5F8090EA663CAFCD0CB3650CCFA2C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ARLocationManager_t224AB8AD4D95A0921BA4A44A7ED5C39729FBC2FF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ARLocationOrientation_tD8A832D47ECB471D892449B5ADB4143078E1C8B7_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x41\x52\x2B\x47\x50\x53\x2F\x41\x52\x20\x4C\x6F\x63\x61\x74\x69\x6F\x6E\x20\x4D\x61\x6E\x61\x67\x65\x72"), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ARLocationOrientation_tD8A832D47ECB471D892449B5ADB4143078E1C8B7_0_0_0_var), NULL);
	}
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[2];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[3];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x68\x74\x74\x70\x3A\x2F\x2F\x64\x6F\x63\x73\x2E\x75\x6E\x69\x74\x79\x2D\x61\x72\x2D\x67\x70\x73\x2D\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x2E\x63\x6F\x6D\x2F\x67\x75\x69\x64\x65\x2F\x23\x61\x72\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x6D\x61\x6E\x61\x67\x65\x72"), NULL);
	}
}
static void ARLocationManager_t224AB8AD4D95A0921BA4A44A7ED5C39729FBC2FF_CustomAttributesCacheGenerator_Camera(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x41\x52\x20\x43\x61\x6D\x65\x72\x61\x20\x74\x68\x61\x74\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x75\x73\x65\x64\x20\x66\x6F\x72\x20\x72\x65\x6E\x64\x65\x72\x69\x6E\x67\x20\x74\x68\x65\x20\x41\x52\x20\x63\x6F\x6E\x74\x65\x6E\x74\x2E\x20\x49\x66\x20\x74\x68\x69\x73\x20\x69\x73\x20\x6E\x6F\x74\x20\x73\x65\x74\x2C\x20\x74\x68\x65\x20\x63\x61\x6D\x65\x72\x61\x20\x74\x61\x67\x67\x65\x72\x20\x61\x73\x20\x27\x4D\x61\x69\x6E\x43\x61\x6D\x65\x72\x61\x27\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x75\x73\x65\x64\x2E\x20\x4D\x61\x6B\x65\x20\x73\x75\x72\x65\x20\x65\x69\x74\x68\x65\x72\x20\x74\x68\x69\x73\x20\x69\x73\x20\x73\x65\x74\x2C\x20\x6F\x72\x20\x61\x20\x63\x61\x6D\x65\x72\x61\x20\x69\x73\x20\x74\x61\x67\x67\x65\x64\x20\x61\x73\x20\x27\x4D\x61\x69\x6E\x43\x61\x6D\x65\x72\x61\x27\x2C\x20\x6F\x72\x20\x61\x6E\x20\x65\x72\x72\x6F\x72\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x74\x68\x72\x6F\x77\x6E\x2E"), NULL);
	}
}
static void ARLocationManager_t224AB8AD4D95A0921BA4A44A7ED5C39729FBC2FF_CustomAttributesCacheGenerator_WaitForARTrackingToStart(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x74\x72\x75\x65\x2C\x20\x77\x61\x69\x74\x20\x75\x6E\x74\x69\x6C\x20\x74\x68\x65\x20\x41\x52\x20\x54\x72\x61\x63\x6B\x69\x6E\x67\x20\x73\x74\x61\x72\x74\x73\x20\x74\x6F\x20\x73\x74\x61\x72\x74\x20\x77\x69\x74\x68\x20\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x20\x61\x6E\x64\x20\x6F\x72\x69\x65\x6E\x74\x61\x74\x69\x6F\x6E\x20\x75\x70\x64\x61\x74\x65\x73\x20\x61\x6E\x64\x20\x6F\x62\x6A\x65\x63\x74\x20\x70\x6C\x61\x63\x65\x6D\x65\x6E\x74\x2E"), NULL);
	}
}
static void ARLocationManager_t224AB8AD4D95A0921BA4A44A7ED5C39729FBC2FF_CustomAttributesCacheGenerator_RestartWhenARTrackingIsRestored(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x74\x72\x75\x65\x2C\x20\x65\x76\x65\x72\x79\x20\x74\x69\x6D\x65\x20\x74\x68\x65\x20\x41\x52\x20\x74\x72\x61\x63\x6B\x69\x6E\x67\x20\x69\x73\x20\x6C\x6F\x73\x74\x20\x61\x6E\x64\x20\x72\x65\x67\x61\x69\x6E\x65\x64\x2C\x20\x74\x68\x65\x20\x41\x52\x2B\x47\x50\x53\x20\x73\x79\x73\x74\x65\x6D\x20\x69\x73\x20\x72\x65\x73\x74\x61\x72\x74\x65\x64\x2C\x20\x72\x65\x70\x6F\x73\x69\x74\x69\x6F\x6E\x69\x6E\x67\x20\x61\x6C\x6C\x20\x74\x68\x65\x20\x6F\x62\x6A\x65\x63\x74\x73\x2E"), NULL);
	}
}
static void ARLocationManager_t224AB8AD4D95A0921BA4A44A7ED5C39729FBC2FF_CustomAttributesCacheGenerator_SetTargetFrameRateTo60Mhz(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x74\x72\x75\x65\x2C\x20\x74\x68\x65\x20\x6D\x61\x6E\x61\x67\x65\x72\x20\x77\x69\x6C\x6C\x20\x73\x65\x74\x20\x27\x41\x70\x70\x6C\x69\x63\x61\x74\x69\x6F\x6E\x2E\x74\x61\x72\x67\x65\x74\x46\x72\x61\x6D\x65\x52\x61\x74\x65\x27\x20\x74\x6F\x20\x36\x30\x2E"), NULL);
	}
}
static void ARLocationManager_t224AB8AD4D95A0921BA4A44A7ED5C39729FBC2FF_CustomAttributesCacheGenerator_DebugMode(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x62\x75\x67"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x57\x68\x65\x6E\x20\x64\x65\x62\x75\x67\x20\x6D\x6F\x64\x65\x20\x69\x73\x20\x65\x6E\x61\x62\x6C\x65\x64\x2C\x20\x74\x68\x69\x73\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x77\x69\x6C\x6C\x20\x70\x72\x69\x6E\x74\x20\x72\x65\x6C\x65\x76\x61\x6E\x74\x20\x6D\x65\x73\x73\x61\x67\x65\x73\x20\x74\x6F\x20\x74\x68\x65\x20\x63\x6F\x6E\x73\x6F\x6C\x65\x2E\x20\x46\x69\x6C\x74\x65\x72\x20\x62\x79\x20\x27\x41\x52\x4C\x6F\x63\x61\x74\x69\x6F\x6E\x4D\x61\x6E\x61\x67\x65\x72\x27\x20\x69\x6E\x20\x74\x68\x65\x20\x6C\x6F\x67\x20\x6F\x75\x74\x70\x75\x74\x20\x74\x6F\x20\x73\x65\x65\x20\x74\x68\x65\x20\x6D\x65\x73\x73\x61\x67\x65\x73\x2E"), NULL);
	}
}
static void ARLocationManager_t224AB8AD4D95A0921BA4A44A7ED5C39729FBC2FF_CustomAttributesCacheGenerator_OnTrackingStarted(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x52\x20\x53\x65\x73\x73\x69\x6F\x6E\x20\x45\x76\x65\x6E\x74\x73"), NULL);
	}
}
static void ARLocationManager_t224AB8AD4D95A0921BA4A44A7ED5C39729FBC2FF_CustomAttributesCacheGenerator_U3CSessionManagerU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ARLocationManager_t224AB8AD4D95A0921BA4A44A7ED5C39729FBC2FF_CustomAttributesCacheGenerator_U3CMainCameraU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ARLocationManager_t224AB8AD4D95A0921BA4A44A7ED5C39729FBC2FF_CustomAttributesCacheGenerator_ARLocationManager_get_SessionManager_m7538DD59B2F33BB6968EA518B62372CF04742A3B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ARLocationManager_t224AB8AD4D95A0921BA4A44A7ED5C39729FBC2FF_CustomAttributesCacheGenerator_ARLocationManager_set_SessionManager_m3135CEBAF97C1405412F6D42F1078C0270C0C4DC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ARLocationManager_t224AB8AD4D95A0921BA4A44A7ED5C39729FBC2FF_CustomAttributesCacheGenerator_ARLocationManager_get_MainCamera_mAC9188AB6E6C21B8A383B47B0B33C5B7DA5A3EC9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ARLocationManager_t224AB8AD4D95A0921BA4A44A7ED5C39729FBC2FF_CustomAttributesCacheGenerator_ARLocationManager_set_MainCamera_m9632FECE6D0F301084EDBCCC85CFB89F7EA13DCC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass28_0_tA71036D989189B71D0132456123D35395E375F18_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ARLocationOrientation_tD8A832D47ECB471D892449B5ADB4143078E1C8B7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x68\x74\x74\x70\x3A\x2F\x2F\x64\x6F\x63\x73\x2E\x75\x6E\x69\x74\x79\x2D\x61\x72\x2D\x67\x70\x73\x2D\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x2E\x63\x6F\x6D\x2F\x67\x75\x69\x64\x65\x2F\x23\x61\x72\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x6F\x72\x69\x65\x6E\x74\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void ARLocationOrientation_tD8A832D47ECB471D892449B5ADB4143078E1C8B7_CustomAttributesCacheGenerator_MaxNumberOfUpdates(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x55\x70\x64\x61\x74\x65\x20\x53\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x6D\x61\x78\x69\x6D\x75\x6D\x20\x6E\x75\x6D\x62\x65\x72\x20\x6F\x66\x20\x6F\x72\x69\x65\x6E\x74\x61\x74\x69\x6F\x6E\x20\x75\x70\x64\x61\x74\x65\x73\x2E\x20\x54\x68\x65\x20\x75\x70\x64\x61\x74\x65\x73\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x70\x61\x75\x73\x65\x64\x20\x61\x66\x74\x65\x72\x20\x74\x68\x69\x73\x20\x61\x6D\x6F\x75\x6E\x74\x2E\x20\x5A\x65\x72\x6F\x20\x6D\x65\x61\x6E\x73\x20\x74\x68\x65\x72\x65\x20\x69\x73\x20\x6E\x6F\x20\x6C\x69\x6D\x69\x74\x20\x61\x6E\x64\x20\x74\x68\x65\x20\x75\x70\x64\x61\x74\x65\x73\x20\x77\x6F\x6E\x27\x74\x20\x62\x65\x20\x70\x61\x75\x73\x65\x64\x20\x61\x75\x74\x6F\x6D\x61\x74\x69\x63\x61\x6C\x6C\x79\x2E"), NULL);
	}
}
static void ARLocationOrientation_tD8A832D47ECB471D892449B5ADB4143078E1C8B7_CustomAttributesCacheGenerator_AverageCount(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x6E\x6C\x79\x20\x75\x70\x64\x61\x74\x65\x20\x61\x66\x74\x65\x72\x20\x6D\x65\x61\x73\x75\x72\x69\x6E\x67\x20\x74\x68\x65\x20\x68\x65\x61\x64\x69\x6E\x67\x20\x4E\x20\x74\x69\x6D\x65\x73\x2C\x20\x61\x6E\x64\x20\x74\x61\x6B\x65\x20\x74\x68\x65\x20\x61\x76\x65\x72\x61\x67\x65\x2E"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 1.0f, 500.0f, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[2];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x76\x65\x72\x61\x67\x69\x6E\x67"), NULL);
	}
}
static void ARLocationOrientation_tD8A832D47ECB471D892449B5ADB4143078E1C8B7_CustomAttributesCacheGenerator_UseRawUntilFirstAverage(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x73\x65\x74\x20\x74\x6F\x20\x74\x72\x75\x65\x2C\x20\x75\x73\x65\x20\x72\x61\x77\x20\x68\x65\x61\x64\x69\x6E\x67\x20\x76\x61\x6C\x75\x65\x73\x20\x75\x6E\x74\x69\x6C\x20\x6D\x65\x61\x73\x75\x72\x69\x6E\x67\x20\x74\x68\x65\x20\x66\x69\x72\x73\x74\x20\x61\x76\x65\x72\x61\x67\x65\x2E"), NULL);
	}
}
static void ARLocationOrientation_tD8A832D47ECB471D892449B5ADB4143078E1C8B7_CustomAttributesCacheGenerator_MovementSmoothingFactor(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x6D\x6F\x6F\x74\x68\x69\x6E\x67"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[2];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x73\x6D\x6F\x6F\x74\x68\x69\x6E\x67\x20\x66\x61\x63\x74\x6F\x72\x2E\x20\x5A\x65\x72\x6F\x20\x6D\x65\x61\x6E\x73\x20\x64\x69\x73\x61\x62\x6C\x65\x64\x2E"), NULL);
	}
}
static void ARLocationOrientation_tD8A832D47ECB471D892449B5ADB4143078E1C8B7_CustomAttributesCacheGenerator_TrueNorthOffset(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6C\x69\x62\x72\x61\x74\x69\x6F\x6E"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x20\x63\x75\x73\x74\x6F\x6D\x20\x6F\x66\x66\x73\x65\x74\x20\x74\x6F\x20\x74\x68\x65\x20\x64\x65\x76\x69\x63\x65\x2D\x63\x61\x6C\x63\x75\x6C\x61\x74\x65\x64\x20\x74\x72\x75\x65\x20\x6E\x6F\x72\x74\x68\x20\x64\x69\x72\x65\x63\x74\x69\x6F\x6E\x2E\x20\x57\x68\x65\x6E\x20\x73\x65\x74\x20\x74\x6F\x20\x61\x20\x76\x61\x6C\x75\x65\x20\x6F\x74\x68\x65\x72\x20\x74\x68\x61\x6E\x20\x7A\x65\x72\x6F\x2C\x20\x74\x68\x65\x20\x64\x65\x76\x69\x63\x65\x27\x73\x20\x74\x72\x75\x65\x20\x6E\x6F\x72\x74\x68\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x69\x67\x6E\x6F\x72\x65\x64\x2C\x20\x61\x6E\x64\x20\x72\x65\x70\x6C\x61\x63\x65\x64\x20\x62\x79\x20\x74\x68\x65\x20\x6D\x61\x67\x6E\x65\x74\x69\x63\x20\x68\x65\x61\x64\x69\x6E\x67\x20\x61\x64\x64\x65\x64\x20\x74\x6F\x20\x74\x68\x69\x73\x20\x6F\x66\x66\x73\x65\x74\x2E"), NULL);
	}
}
static void ARLocationOrientation_tD8A832D47ECB471D892449B5ADB4143078E1C8B7_CustomAttributesCacheGenerator_ApplyCompassTiltCompensationOnAndroid(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x74\x72\x75\x65\x2C\x20\x61\x70\x70\x6C\x79\x20\x61\x20\x74\x69\x6C\x74\x2D\x63\x6F\x6D\x70\x65\x6E\x73\x61\x74\x69\x6F\x6E\x20\x61\x6C\x67\x6F\x72\x69\x74\x68\x6D\x20\x6F\x6E\x20\x41\x6E\x64\x72\x6F\x69\x64\x20\x64\x65\x76\x69\x63\x65\x73\x2E\x20\x4F\x6E\x6C\x79\x20\x64\x69\x73\x61\x62\x6C\x65\x20\x74\x68\x69\x73\x20\x69\x66\x20\x79\x6F\x75\x20\x72\x75\x6E\x20\x69\x6E\x74\x6F\x20\x61\x6E\x79\x20\x69\x73\x73\x75\x65\x73\x2E"), NULL);
	}
}
static void ARLocationOrientation_tD8A832D47ECB471D892449B5ADB4143078E1C8B7_CustomAttributesCacheGenerator_LowPassFilterFactor(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x69\x73\x20\x74\x68\x65\x20\x6C\x6F\x77\x20\x70\x61\x73\x73\x20\x66\x69\x6C\x74\x65\x72\x20\x66\x61\x63\x74\x6F\x72\x20\x61\x70\x70\x6C\x69\x65\x64\x20\x74\x6F\x20\x74\x68\x65\x20\x68\x65\x61\x64\x69\x6E\x67\x20\x76\x61\x6C\x75\x65\x73\x20\x74\x6F\x20\x72\x65\x64\x75\x63\x65\x20\x6A\x69\x74\x74\x65\x72\x2E\x20\x41\x20\x7A\x65\x72\x6F\x20\x76\x61\x6C\x75\x65\x20\x64\x69\x73\x61\x62\x6C\x65\x73\x20\x74\x68\x65\x20\x6C\x6F\x77\x2D\x70\x61\x73\x73\x20\x66\x69\x6C\x74\x65\x72\x2C\x20\x77\x68\x69\x6C\x65\x20\x61\x20\x76\x61\x6C\x75\x65\x20\x6F\x66\x20\x31\x20\x77\x69\x6C\x6C\x20\x6D\x61\x6B\x65\x20\x74\x68\x65\x20\x66\x69\x6C\x74\x65\x72\x20\x62\x6C\x6F\x63\x6B\x20\x61\x6C\x6C\x20\x76\x61\x6C\x75\x65\x20\x63\x68\x61\x6E\x67\x65\x73\x2E\x20\x4E\x6F\x74\x20\x61\x70\x70\x6C\x69\x65\x64\x20\x6F\x6E\x20\x69\x4F\x53\x2C\x20\x6F\x6E\x6C\x79\x20\x6F\x6E\x20\x41\x6E\x64\x72\x6F\x69\x64\x20\x77\x68\x65\x6E\x20\x74\x68\x65\x20\x74\x69\x6C\x74\x2D\x63\x6F\x6D\x70\x65\x6E\x73\x61\x74\x69\x6F\x6E\x20\x69\x73\x20\x65\x6E\x61\x62\x6C\x65\x64\x2E"), NULL);
	}
}
static void ARLocationOrientation_tD8A832D47ECB471D892449B5ADB4143078E1C8B7_CustomAttributesCacheGenerator_OnOrientationUpdated(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6C\x6C\x65\x64\x20\x61\x66\x74\x65\x72\x20\x74\x68\x65\x20\x6F\x72\x69\x65\x6E\x74\x61\x74\x69\x6F\x6E\x20\x68\x61\x73\x20\x62\x65\x65\x6E\x20\x75\x70\x64\x61\x74\x65\x64\x2E"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x45\x76\x65\x6E\x74\x73"), NULL);
	}
}
static void ARLocationOrientation_tD8A832D47ECB471D892449B5ADB4143078E1C8B7_CustomAttributesCacheGenerator_OnBeforeOrientationUpdated(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6C\x6C\x65\x64\x20\x6A\x75\x73\x74\x20\x62\x65\x66\x6F\x72\x65\x20\x74\x68\x65\x20\x6F\x72\x69\x65\x6E\x74\x61\x74\x69\x6F\x6E\x20\x68\x61\x73\x20\x62\x65\x65\x6E\x20\x75\x70\x64\x61\x74\x65\x64\x2E"), NULL);
	}
}
static void ARLocationOrientation_tD8A832D47ECB471D892449B5ADB4143078E1C8B7_CustomAttributesCacheGenerator_ARLocationOrientation_U3CStartU3Eb__19_0_m16FDD14BAFB949736D63B5D028648801A8CB64E4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ARLocationProvider_t47C5F7E2A5840C8C1056AA5CCEB31F2517BF4605_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x68\x74\x74\x70\x3A\x2F\x2F\x64\x6F\x63\x73\x2E\x75\x6E\x69\x74\x79\x2D\x61\x72\x2D\x67\x70\x73\x2D\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x2E\x63\x6F\x6D\x2F\x67\x75\x69\x64\x65\x2F\x23\x61\x72\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x70\x72\x6F\x76\x69\x64\x65\x72"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[2];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x41\x52\x2B\x47\x50\x53\x2F\x41\x52\x20\x4C\x6F\x63\x61\x74\x69\x6F\x6E\x20\x50\x72\x6F\x76\x69\x64\x65\x72"), NULL);
	}
}
static void ARLocationProvider_t47C5F7E2A5840C8C1056AA5CCEB31F2517BF4605_CustomAttributesCacheGenerator_LocationProviderSettings(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x55\x70\x64\x61\x74\x65\x20\x53\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x6F\x70\x74\x69\x6F\x6E\x73\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x4C\x6F\x63\x61\x74\x69\x6F\x6E\x20\x50\x72\x6F\x76\x69\x64\x65\x72\x2E"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[2];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x6F\x63\x61\x74\x69\x6F\x6E\x55\x70\x64\x61\x74\x65\x53\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
}
static void ARLocationProvider_t47C5F7E2A5840C8C1056AA5CCEB31F2517BF4605_CustomAttributesCacheGenerator_MockLocationData(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x63\x6B\x20\x44\x61\x74\x61"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x64\x61\x74\x61\x20\x6F\x66\x20\x6D\x6F\x63\x6B\x20\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x2E\x20\x49\x66\x20\x70\x72\x65\x73\x65\x6E\x74\x2C\x20\x6F\x76\x65\x72\x72\x69\x64\x65\x73\x20\x74\x68\x65\x20\x4D\x6F\x63\x6B\x20\x4C\x6F\x63\x61\x74\x69\x6F\x6E\x20\x61\x62\x6F\x76\x65\x2E"), NULL);
	}
}
static void ARLocationProvider_t47C5F7E2A5840C8C1056AA5CCEB31F2517BF4605_CustomAttributesCacheGenerator_MaxWaitTime(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x69\x74\x69\x61\x6C\x69\x7A\x61\x74\x69\x6F\x6E"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x6D\x61\x78\x69\x6D\x75\x6D\x20\x77\x61\x69\x74\x20\x74\x69\x6D\x65\x20\x74\x6F\x20\x77\x61\x69\x74\x20\x66\x6F\x72\x20\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x20\x69\x6E\x69\x74\x69\x61\x6C\x69\x7A\x61\x74\x69\x6F\x6E\x2E"), NULL);
	}
}
static void ARLocationProvider_t47C5F7E2A5840C8C1056AA5CCEB31F2517BF4605_CustomAttributesCacheGenerator_StartUpDelay(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x57\x61\x69\x74\x20\x74\x68\x69\x73\x20\x6D\x61\x6E\x79\x20\x73\x65\x63\x6F\x6E\x64\x73\x20\x62\x65\x66\x6F\x72\x65\x20\x73\x74\x61\x72\x74\x69\x6E\x67\x20\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x20\x73\x65\x72\x76\x69\x63\x65\x73\x2E\x20\x55\x73\x65\x66\x75\x6C\x20\x77\x68\x65\x6E\x20\x75\x73\x69\x6E\x67\x20\x55\x6E\x69\x74\x79\x20\x52\x65\x6D\x6F\x74\x65\x2E"), NULL);
	}
}
static void ARLocationProvider_t47C5F7E2A5840C8C1056AA5CCEB31F2517BF4605_CustomAttributesCacheGenerator_DebugMode(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x57\x68\x65\x6E\x20\x64\x65\x62\x75\x67\x20\x6D\x6F\x64\x65\x20\x69\x73\x20\x65\x6E\x61\x62\x6C\x65\x64\x2C\x20\x74\x68\x69\x73\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x77\x69\x6C\x6C\x20\x70\x72\x69\x6E\x74\x20\x72\x65\x6C\x65\x76\x61\x6E\x74\x20\x6D\x65\x73\x73\x61\x67\x65\x73\x20\x74\x6F\x20\x74\x68\x65\x20\x63\x6F\x6E\x73\x6F\x6C\x65\x2E\x20\x46\x69\x6C\x74\x65\x72\x20\x62\x79\x20\x27\x41\x52\x4C\x6F\x63\x61\x74\x69\x6F\x6E\x50\x72\x6F\x76\x69\x64\x65\x72\x27\x20\x69\x6E\x20\x74\x68\x65\x20\x6C\x6F\x67\x20\x6F\x75\x74\x70\x75\x74\x20\x74\x6F\x20\x73\x65\x65\x20\x74\x68\x65\x20\x6D\x65\x73\x73\x61\x67\x65\x73\x2E"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x62\x75\x67"), NULL);
	}
}
static void ARLocationProvider_t47C5F7E2A5840C8C1056AA5CCEB31F2517BF4605_CustomAttributesCacheGenerator_OnEnabled(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x45\x76\x65\x6E\x74\x73"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6C\x6C\x65\x64\x20\x61\x66\x74\x65\x72\x20\x74\x68\x65\x20\x66\x69\x72\x73\x74\x20\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x20\x69\x73\x20\x72\x65\x61\x64\x2E"), NULL);
	}
}
static void ARLocationProvider_t47C5F7E2A5840C8C1056AA5CCEB31F2517BF4605_CustomAttributesCacheGenerator_OnLocationUpdated(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6C\x6C\x65\x64\x20\x61\x66\x74\x65\x72\x20\x65\x61\x63\x68\x20\x6E\x65\x77\x20\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x20\x75\x70\x64\x61\x74\x65\x2E"), NULL);
	}
}
static void ARLocationProvider_t47C5F7E2A5840C8C1056AA5CCEB31F2517BF4605_CustomAttributesCacheGenerator_OnRawLocationUpdated(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6C\x6C\x65\x64\x20\x61\x66\x74\x65\x72\x20\x65\x61\x63\x68\x20\x6E\x65\x77\x20\x72\x61\x77\x20\x64\x65\x76\x69\x63\x65\x20\x47\x50\x53\x20\x64\x61\x74\x61\x20\x69\x73\x20\x6F\x62\x74\x61\x69\x6E\x65\x64\x2E"), NULL);
	}
}
static void ARLocationProvider_t47C5F7E2A5840C8C1056AA5CCEB31F2517BF4605_CustomAttributesCacheGenerator_OnCompassUpdated(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6C\x6C\x65\x64\x20\x61\x66\x74\x65\x72\x20\x65\x61\x63\x68\x20\x6E\x65\x77\x20\x63\x6F\x6D\x70\x61\x73\x73\x20\x75\x70\x64\x61\x74\x65\x2E"), NULL);
	}
}
static void ARLocationProvider_t47C5F7E2A5840C8C1056AA5CCEB31F2517BF4605_CustomAttributesCacheGenerator_U3CProviderU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ARLocationProvider_t47C5F7E2A5840C8C1056AA5CCEB31F2517BF4605_CustomAttributesCacheGenerator_OnLocationUpdatedDelegate(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ARLocationProvider_t47C5F7E2A5840C8C1056AA5CCEB31F2517BF4605_CustomAttributesCacheGenerator_OnCompassUpdateDelegate(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ARLocationProvider_t47C5F7E2A5840C8C1056AA5CCEB31F2517BF4605_CustomAttributesCacheGenerator_OnRestartDelegate(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ARLocationProvider_t47C5F7E2A5840C8C1056AA5CCEB31F2517BF4605_CustomAttributesCacheGenerator_ARLocationProvider_get_Provider_m887F7C21BC73DB9CAD4B509C9512C2F195F14F06(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ARLocationProvider_t47C5F7E2A5840C8C1056AA5CCEB31F2517BF4605_CustomAttributesCacheGenerator_ARLocationProvider_set_Provider_mB59E69874BFB9942095C19D1A10B7C60BEA52755(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ARLocationProvider_t47C5F7E2A5840C8C1056AA5CCEB31F2517BF4605_CustomAttributesCacheGenerator_ARLocationProvider_add_OnLocationUpdatedDelegate_mBECC6CBE675617268F495916CA8AE6F9A9BFB516(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ARLocationProvider_t47C5F7E2A5840C8C1056AA5CCEB31F2517BF4605_CustomAttributesCacheGenerator_ARLocationProvider_remove_OnLocationUpdatedDelegate_m13F50E2CE9DE3A18AB494417C1A55BE5D8C6C7F9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ARLocationProvider_t47C5F7E2A5840C8C1056AA5CCEB31F2517BF4605_CustomAttributesCacheGenerator_ARLocationProvider_add_OnCompassUpdateDelegate_m337750E84FA36388355DD5570262B0B01166AAFD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ARLocationProvider_t47C5F7E2A5840C8C1056AA5CCEB31F2517BF4605_CustomAttributesCacheGenerator_ARLocationProvider_remove_OnCompassUpdateDelegate_mC392E83FB55C2BE9AC31D703173926B3CFA18B55(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ARLocationProvider_t47C5F7E2A5840C8C1056AA5CCEB31F2517BF4605_CustomAttributesCacheGenerator_ARLocationProvider_add_OnRestartDelegate_m78C024AD631AD87AF747ACCF956151432093435B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ARLocationProvider_t47C5F7E2A5840C8C1056AA5CCEB31F2517BF4605_CustomAttributesCacheGenerator_ARLocationProvider_remove_OnRestartDelegate_mD4983BA1806542C3E3A6E28412FDEC564C6ED49F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ARLocationProvider_t47C5F7E2A5840C8C1056AA5CCEB31F2517BF4605_CustomAttributesCacheGenerator_ARLocationProvider_Start_m69C6FC9955364F2684BA22E24DD9C9E27567D71D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__50_tE495468A4B52BE5F74CE7E3FFBEDAB0D1C021E74_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__50_tE495468A4B52BE5F74CE7E3FFBEDAB0D1C021E74_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__50_tE495468A4B52BE5F74CE7E3FFBEDAB0D1C021E74_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__50_tE495468A4B52BE5F74CE7E3FFBEDAB0D1C021E74_CustomAttributesCacheGenerator_U3CStartU3Ed__50__ctor_m2B4DF061E1072A472835FE6256FF60D401AD8CA2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__50_tE495468A4B52BE5F74CE7E3FFBEDAB0D1C021E74_CustomAttributesCacheGenerator_U3CStartU3Ed__50_System_IDisposable_Dispose_m0E91CC8D29B8EF95CEE495E16650F8D83AB1BDF6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__50_tE495468A4B52BE5F74CE7E3FFBEDAB0D1C021E74_CustomAttributesCacheGenerator_U3CStartU3Ed__50_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF20E6BE6EB3C485DE54B36A7F29E47C907105304(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__50_tE495468A4B52BE5F74CE7E3FFBEDAB0D1C021E74_CustomAttributesCacheGenerator_U3CStartU3Ed__50_System_Collections_IEnumerator_Reset_m5FFC148EF84C94EEAA5CD46AD158E9DE3B66AA4A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__50_tE495468A4B52BE5F74CE7E3FFBEDAB0D1C021E74_CustomAttributesCacheGenerator_U3CStartU3Ed__50_System_Collections_IEnumerator_get_Current_m7E5C18D9EA3A76035949ACF59E44559CA348C494(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void GroundHeight_t4265E0D341906907B659299C70E5C1D1C74A7E3F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void SettingsData_t400E00E773911845F27120BD77E2A5A5C7F05585_CustomAttributesCacheGenerator_InitialGroundHeightGuess(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 10.0f, NULL);
	}
}
static void SettingsData_t400E00E773911845F27120BD77E2A5A5C7F05585_CustomAttributesCacheGenerator_MinGroundHeight(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 10.0f, NULL);
	}
}
static void SettingsData_t400E00E773911845F27120BD77E2A5A5C7F05585_CustomAttributesCacheGenerator_MaxGroundHeight(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 10.0f, NULL);
	}
}
static void SettingsData_t400E00E773911845F27120BD77E2A5A5C7F05585_CustomAttributesCacheGenerator_Smoothing(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void SettingsData_t400E00E773911845F27120BD77E2A5A5C7F05585_CustomAttributesCacheGenerator_Precision(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 0.100000001f, NULL);
	}
}
static void Hotspot_t94B43358E06B6906BE49D9AD465A62C33E50C6DE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x41\x52\x2B\x47\x50\x53\x2F\x48\x6F\x74\x73\x70\x6F\x74"), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x68\x74\x74\x70\x3A\x2F\x2F\x64\x6F\x63\x73\x2E\x75\x6E\x69\x74\x79\x2D\x61\x72\x2D\x67\x70\x73\x2D\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x2E\x63\x6F\x6D\x2F\x67\x75\x69\x64\x65\x2F\x23\x68\x6F\x74\x73\x70\x6F\x74"), NULL);
	}
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[2];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void Hotspot_t94B43358E06B6906BE49D9AD465A62C33E50C6DE_CustomAttributesCacheGenerator_DebugMode(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x57\x68\x65\x6E\x20\x64\x65\x62\x75\x67\x20\x6D\x6F\x64\x65\x20\x69\x73\x20\x65\x6E\x61\x62\x6C\x65\x64\x2C\x20\x74\x68\x69\x73\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x77\x69\x6C\x6C\x20\x70\x72\x69\x6E\x74\x20\x72\x65\x6C\x65\x76\x61\x6E\x74\x20\x6D\x65\x73\x73\x61\x67\x65\x73\x20\x74\x6F\x20\x74\x68\x65\x20\x63\x6F\x6E\x73\x6F\x6C\x65\x2E\x20\x46\x69\x6C\x74\x65\x72\x20\x62\x79\x20\x27\x48\x6F\x74\x73\x70\x6F\x74\x27\x20\x69\x6E\x20\x74\x68\x65\x20\x6C\x6F\x67\x20\x6F\x75\x74\x70\x75\x74\x20\x74\x6F\x20\x73\x65\x65\x20\x74\x68\x65\x20\x6D\x65\x73\x73\x61\x67\x65\x73\x2E"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x62\x75\x67"), NULL);
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[2];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 4.0f, NULL);
	}
}
static void Hotspot_t94B43358E06B6906BE49D9AD465A62C33E50C6DE_CustomAttributesCacheGenerator_UseOnLeaveHotspotEvent(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x74\x72\x75\x65\x2C\x20\x6D\x6F\x6E\x69\x74\x6F\x72\x20\x64\x69\x73\x74\x61\x6E\x63\x65\x20\x74\x6F\x20\x65\x6D\x69\x74\x20\x74\x68\x65\x20\x27\x4F\x6E\x4C\x65\x61\x76\x65\x48\x6F\x74\x73\x70\x6F\x74\x27\x20\x65\x76\x65\x6E\x74\x2E\x20\x49\x66\x20\x79\x6F\x75\x20\x64\x6F\x6E\x27\x74\x20\x6E\x65\x65\x64\x20\x69\x74\x2C\x20\x6B\x65\x65\x70\x20\x74\x68\x69\x73\x20\x64\x69\x73\x61\x62\x6C\x65\x64\x20\x66\x6F\x72\x20\x62\x65\x74\x74\x65\x72\x20\x70\x65\x72\x66\x6F\x72\x6D\x61\x6E\x63\x65\x2E"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x45\x76\x65\x6E\x74\x73"), NULL);
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[2];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 4.0f, NULL);
	}
}
static void Hotspot_t94B43358E06B6906BE49D9AD465A62C33E50C6DE_CustomAttributesCacheGenerator_OnHotspotActivated(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x45\x76\x65\x6E\x74\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x48\x6F\x74\x73\x70\x6F\x74\x20\x69\x73\x20\x61\x63\x74\x69\x76\x61\x74\x65\x64\x2E"), NULL);
	}
}
static void Hotspot_t94B43358E06B6906BE49D9AD465A62C33E50C6DE_CustomAttributesCacheGenerator_OnHotspotLeave(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x65\x76\x65\x6E\x74\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x65\x6D\x69\x74\x65\x64\x20\x6F\x6E\x6C\x79\x20\x6F\x6E\x63\x65\x2C\x20\x77\x68\x65\x6E\x20\x74\x68\x65\x20\x75\x73\x65\x72\x20\x6C\x65\x61\x76\x65\x73\x20\x74\x68\x65\x20\x68\x6F\x74\x73\x70\x6F\x74\x20\x61\x72\x65\x61\x20\x61\x66\x74\x65\x72\x20\x69\x74\x20\x69\x73\x20\x61\x63\x74\x69\x76\x61\x74\x65\x64\x2E"), NULL);
	}
}
static void HotspotSettingsData_t57879FD5B635460588A94FDFD5877B38B4E1DF83_CustomAttributesCacheGenerator_Prefab(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x70\x72\x65\x66\x61\x62\x2F\x47\x61\x6D\x65\x4F\x62\x6A\x65\x63\x74\x20\x74\x68\x61\x74\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x69\x6E\x73\x74\x61\x6E\x74\x69\x61\x74\x65\x64\x20\x62\x79\x20\x74\x68\x65\x20\x48\x6F\x74\x73\x70\x6F\x74\x2E"), NULL);
	}
}
static void HotspotSettingsData_t57879FD5B635460588A94FDFD5877B38B4E1DF83_CustomAttributesCacheGenerator_PositionMode(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x70\x6F\x73\x69\x74\x69\x6F\x6E\x69\x6E\x67\x20\x6D\x6F\x64\x65\x2E\x20\x27\x48\x6F\x74\x73\x70\x6F\x74\x43\x65\x6E\x74\x65\x72\x27\x20\x6D\x65\x61\x6E\x73\x20\x74\x68\x65\x20\x6F\x62\x6A\x65\x63\x74\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x69\x6E\x73\x74\x61\x6E\x74\x69\x61\x74\x65\x64\x20\x61\x74\x20\x74\x68\x65\x20\x48\x6F\x74\x70\x6F\x74\x27\x73\x20\x63\x65\x6E\x74\x65\x72\x20\x67\x65\x6F\x2D\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x2E\x20\x27\x43\x61\x6D\x65\x72\x61\x50\x6F\x73\x69\x74\x69\x6F\x6E\x27\x20\x6D\x65\x61\x6E\x73\x20\x69\x74\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x70\x6F\x73\x69\x74\x69\x6F\x6E\x65\x64\x20\x61\x74\x20\x74\x68\x65\x20\x66\x72\x6F\x6E\x74\x20\x6F\x66\x20\x74\x68\x65\x20\x63\x61\x6D\x65\x72\x61\x2E"), NULL);
	}
}
static void HotspotSettingsData_t57879FD5B635460588A94FDFD5877B38B4E1DF83_CustomAttributesCacheGenerator_ActivationRadius(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x64\x69\x73\x74\x61\x6E\x63\x65\x20\x66\x72\x6F\x6D\x20\x74\x68\x65\x20\x63\x65\x6E\x74\x65\x72\x20\x74\x68\x61\x74\x20\x74\x68\x65\x20\x75\x73\x65\x72\x20\x6D\x75\x73\x74\x20\x62\x65\x20\x6C\x6F\x63\x61\x74\x65\x64\x20\x74\x6F\x20\x61\x63\x74\x69\x76\x61\x74\x65\x20\x74\x68\x65\x20\x48\x6F\x74\x73\x70\x6F\x74\x2E"), NULL);
	}
}
static void HotspotSettingsData_t57879FD5B635460588A94FDFD5877B38B4E1DF83_CustomAttributesCacheGenerator_AlignToCamera(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x74\x72\x75\x65\x2C\x20\x61\x6C\x69\x67\x6E\x20\x74\x68\x65\x20\x69\x6E\x73\x74\x61\x6E\x74\x69\x61\x74\x65\x64\x20\x6F\x62\x6A\x65\x63\x74\x20\x74\x6F\x20\x66\x61\x63\x65\x20\x74\x68\x65\x20\x63\x61\x6D\x65\x72\x61\x20\x28\x68\x6F\x72\x69\x7A\x6F\x6E\x74\x61\x6C\x6C\x79\x29\x2E"), NULL);
	}
}
static void HotspotSettingsData_t57879FD5B635460588A94FDFD5877B38B4E1DF83_CustomAttributesCacheGenerator_DistanceFromCamera(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x27\x50\x6F\x73\x69\x74\x69\x6F\x6E\x4D\x6F\x64\x65\x27\x20\x69\x73\x20\x73\x65\x74\x20\x74\x6F\x20\x27\x43\x61\x6D\x65\x72\x61\x50\x6F\x73\x69\x74\x69\x6F\x6E\x27\x2C\x20\x68\x6F\x77\x20\x66\x61\x72\x20\x66\x72\x6F\x6D\x20\x74\x68\x65\x20\x63\x61\x6D\x65\x72\x61\x20\x74\x68\x65\x20\x69\x6E\x73\x74\x61\x6E\x74\x69\x61\x74\x65\x64\x20\x6F\x62\x6A\x65\x63\x74\x20\x73\x68\x6F\x75\x6C\x64\x20\x62\x65\x20\x70\x6C\x61\x63\x65\x64\x2E"), NULL);
	}
}
static void HotspotSettingsData_t57879FD5B635460588A94FDFD5877B38B4E1DF83_CustomAttributesCacheGenerator_UseRawLocation(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x74\x72\x75\x65\x2C\x20\x75\x73\x65\x20\x72\x61\x77\x20\x47\x50\x53\x20\x64\x61\x74\x61\x2C\x20\x69\x6E\x73\x74\x65\x61\x64\x20\x6F\x66\x20\x74\x68\x65\x20\x66\x69\x6C\x74\x65\x72\x65\x64\x20\x47\x50\x53\x20\x64\x61\x74\x61\x2E"), NULL);
	}
}
static void MoveAlongPath_t8123BE990CF5AD253811E75F1D2B67697084A372_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x68\x74\x74\x70\x3A\x2F\x2F\x64\x6F\x63\x73\x2E\x75\x6E\x69\x74\x79\x2D\x61\x72\x2D\x67\x70\x73\x2D\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x2E\x63\x6F\x6D\x2F\x67\x75\x69\x64\x65\x2F\x23\x6D\x6F\x76\x65\x61\x6C\x6F\x6E\x67\x70\x61\x74\x68"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[2];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x41\x52\x2B\x47\x50\x53\x2F\x4D\x6F\x76\x65\x20\x41\x6C\x6F\x6E\x67\x20\x50\x61\x74\x68"), NULL);
	}
}
static void MoveAlongPath_t8123BE990CF5AD253811E75F1D2B67697084A372_CustomAttributesCacheGenerator_DebugMode(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x57\x68\x65\x6E\x20\x64\x65\x62\x75\x67\x20\x6D\x6F\x64\x65\x20\x69\x73\x20\x65\x6E\x61\x62\x6C\x65\x64\x2C\x20\x74\x68\x69\x73\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x77\x69\x6C\x6C\x20\x70\x72\x69\x6E\x74\x20\x72\x65\x6C\x65\x76\x61\x6E\x74\x20\x6D\x65\x73\x73\x61\x67\x65\x73\x20\x74\x6F\x20\x74\x68\x65\x20\x63\x6F\x6E\x73\x6F\x6C\x65\x2E\x20\x46\x69\x6C\x74\x65\x72\x20\x62\x79\x20\x27\x4D\x6F\x76\x65\x41\x6C\x6F\x6E\x67\x50\x61\x74\x68\x27\x20\x69\x6E\x20\x74\x68\x65\x20\x6C\x6F\x67\x20\x6F\x75\x74\x70\x75\x74\x20\x74\x6F\x20\x73\x65\x65\x20\x74\x68\x65\x20\x6D\x65\x73\x73\x61\x67\x65\x73\x2E"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x62\x75\x67"), NULL);
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[2];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 4.0f, NULL);
	}
}
static void MoveAlongPath_t8123BE990CF5AD253811E75F1D2B67697084A372_CustomAttributesCacheGenerator_state(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 4.0f, NULL);
	}
}
static void PathSettingsData_t6602AA7CE8BA6CC07093445804B1A4C362D59C50_CustomAttributesCacheGenerator_LocationPath(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x4C\x6F\x63\x61\x74\x69\x6F\x6E\x50\x61\x74\x68\x20\x64\x65\x73\x63\x72\x69\x62\x69\x6E\x67\x20\x74\x68\x65\x20\x70\x61\x74\x68\x20\x74\x6F\x20\x62\x65\x20\x74\x72\x61\x76\x65\x72\x73\x65\x64\x2E"), NULL);
	}
}
static void PathSettingsData_t6602AA7CE8BA6CC07093445804B1A4C362D59C50_CustomAttributesCacheGenerator_SplineSampleCount(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x6E\x75\x6D\x62\x65\x72\x20\x6F\x66\x20\x70\x6F\x69\x6E\x74\x73\x2D\x70\x65\x72\x2D\x73\x65\x67\x6D\x65\x6E\x74\x20\x75\x73\x65\x64\x20\x74\x6F\x20\x63\x61\x6C\x63\x75\x6C\x61\x74\x65\x20\x74\x68\x65\x20\x73\x70\x6C\x69\x6E\x65\x2E"), NULL);
	}
}
static void PathSettingsData_t6602AA7CE8BA6CC07093445804B1A4C362D59C50_CustomAttributesCacheGenerator_LineRenderer(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x70\x72\x65\x73\x65\x6E\x74\x2C\x20\x72\x65\x6E\x64\x65\x72\x73\x20\x74\x68\x65\x20\x73\x70\x6C\x69\x6E\x65\x20\x69\x6E\x20\x74\x68\x65\x20\x73\x63\x65\x6E\x65\x20\x75\x73\x69\x6E\x67\x20\x74\x68\x65\x20\x67\x69\x76\x65\x6E\x20\x6C\x69\x6E\x65\x20\x72\x65\x6E\x64\x65\x72\x65\x72\x2E"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x6C\x69\x6E\x65\x52\x65\x6E\x64\x65\x72\x65\x72"), NULL);
	}
}
static void PlaybackSettingsData_tCD647CAB0B9808022DAD6E1ECBE95C692B817499_CustomAttributesCacheGenerator_Speed(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x73\x70\x65\x65\x64\x20\x61\x6C\x6F\x6E\x67\x20\x74\x68\x65\x20\x70\x61\x74\x68\x2E"), NULL);
	}
}
static void PlaybackSettingsData_tCD647CAB0B9808022DAD6E1ECBE95C692B817499_CustomAttributesCacheGenerator_Up(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x75\x70\x20\x64\x69\x72\x65\x63\x74\x69\x6F\x6E\x20\x74\x6F\x20\x62\x65\x20\x75\x73\x65\x64\x20\x66\x6F\x72\x20\x6F\x72\x69\x65\x6E\x74\x61\x74\x69\x6F\x6E\x20\x61\x6C\x6F\x6E\x67\x20\x74\x68\x65\x20\x70\x61\x74\x68\x2E"), NULL);
	}
}
static void PlaybackSettingsData_tCD647CAB0B9808022DAD6E1ECBE95C692B817499_CustomAttributesCacheGenerator_Loop(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x74\x72\x75\x65\x2C\x20\x70\x6C\x61\x79\x20\x74\x68\x65\x20\x70\x61\x74\x68\x20\x74\x72\x61\x76\x65\x72\x73\x61\x6C\x20\x69\x6E\x20\x61\x20\x6C\x6F\x6F\x70\x2E"), NULL);
	}
}
static void PlaybackSettingsData_tCD647CAB0B9808022DAD6E1ECBE95C692B817499_CustomAttributesCacheGenerator_AutoPlay(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x74\x72\x75\x65\x2C\x20\x73\x74\x61\x72\x74\x20\x70\x6C\x61\x79\x69\x6E\x67\x20\x61\x75\x74\x6F\x6D\x61\x74\x69\x63\x61\x6C\x6C\x79\x2E"), NULL);
	}
}
static void PlaybackSettingsData_tCD647CAB0B9808022DAD6E1ECBE95C692B817499_CustomAttributesCacheGenerator_Offset(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x6F\x66\x66\x73\x65\x74"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x70\x61\x72\x61\x6D\x65\x74\x65\x72\x73\x20\x6F\x66\x66\x73\x65\x74\x3B\x20\x6D\x61\x72\x6B\x73\x20\x74\x68\x65\x20\x69\x6E\x69\x74\x69\x61\x6C\x20\x70\x6F\x73\x69\x74\x69\x6F\x6E\x20\x6F\x66\x20\x74\x68\x65\x20\x6F\x62\x6A\x65\x63\x74\x20\x61\x6C\x6F\x6E\x67\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x2E"), NULL);
	}
}
static void PlacementSettingsData_t3C467CCF2933E055B5578E749735434BD6F15C26_CustomAttributesCacheGenerator_AltitudeMode(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x61\x6C\x74\x69\x74\x75\x64\x65\x20\x6D\x6F\x64\x65\x2E\x20\x54\x68\x65\x20\x61\x6C\x74\x69\x74\x75\x64\x65\x20\x6D\x6F\x64\x65\x73\x20\x6F\x66\x20\x74\x68\x65\x20\x69\x6E\x64\x69\x76\x69\x64\x75\x61\x6C\x20\x70\x61\x74\x68\x20\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x73\x20\x61\x72\x65\x20\x69\x67\x6E\x6F\x72\x65\x64\x2C\x20\x61\x6E\x64\x20\x74\x68\x69\x73\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x75\x73\x65\x64\x20\x69\x6E\x73\x74\x65\x61\x64\x2E"), NULL);
	}
}
static void PlacementSettingsData_t3C467CCF2933E055B5578E749735434BD6F15C26_CustomAttributesCacheGenerator_MaxNumberOfLocationUpdates(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x6D\x61\x78\x69\x6D\x75\x6D\x20\x6E\x75\x6D\x62\x65\x72\x20\x6F\x66\x20\x74\x69\x6D\x65\x73\x20\x74\x68\x69\x73\x20\x6F\x62\x6A\x65\x63\x74\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x61\x66\x66\x65\x63\x74\x65\x64\x20\x62\x79\x20\x47\x50\x53\x20\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x20\x75\x70\x64\x61\x74\x65\x73\x2E\x20\x5A\x65\x72\x6F\x20\x6D\x65\x61\x6E\x73\x20\x6E\x6F\x20\x6C\x69\x6D\x69\x74\x73\x20\x61\x72\x65\x20\x69\x6D\x70\x6F\x73\x65\x64\x2E"), NULL);
	}
}
static void U3CU3Ec__DisplayClass34_0_tE995B7736DECF544630BFC7962E5608C5E1B2530_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlaceAlongPath_t0F05120A71DC49EFA8C7D544DFB7B92C878BAB52_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x68\x74\x74\x70\x3A\x2F\x2F\x64\x6F\x63\x73\x2E\x75\x6E\x69\x74\x79\x2D\x61\x72\x2D\x67\x70\x73\x2D\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x2E\x63\x6F\x6D\x2F\x67\x75\x69\x64\x65\x2F\x23\x70\x6C\x61\x63\x65\x61\x6C\x6F\x6E\x67\x70\x61\x74\x68"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x41\x52\x2B\x47\x50\x53\x2F\x50\x6C\x61\x63\x65\x20\x41\x6C\x6F\x6E\x67\x20\x50\x61\x74\x68"), NULL);
	}
}
static void PlaceAlongPath_t0F05120A71DC49EFA8C7D544DFB7B92C878BAB52_CustomAttributesCacheGenerator_Path(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x70\x61\x74\x68"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x50\x61\x74\x68\x20\x53\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[2];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x70\x61\x74\x68\x20\x74\x6F\x20\x70\x6C\x61\x63\x65\x20\x74\x68\x65\x20\x70\x72\x65\x66\x61\x62\x20\x69\x6E\x73\x74\x61\x6E\x63\x65\x73\x20\x6F\x6E\x2E"), NULL);
	}
}
static void PlaceAlongPath_t0F05120A71DC49EFA8C7D544DFB7B92C878BAB52_CustomAttributesCacheGenerator_Prefab(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x70\x72\x65\x66\x61\x62\x2F\x47\x61\x6D\x65\x4F\x62\x6A\x65\x63\x74\x20\x74\x6F\x20\x62\x65\x20\x70\x61\x6C\x63\x65\x64\x20\x61\x6C\x6F\x6E\x67\x20\x74\x68\x65\x20\x70\x61\x74\x68\x2E"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x70\x72\x65\x66\x61\x62"), NULL);
	}
}
static void PlaceAlongPath_t0F05120A71DC49EFA8C7D544DFB7B92C878BAB52_CustomAttributesCacheGenerator_ObjectCount(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x6F\x62\x6A\x65\x63\x74\x43\x6F\x75\x6E\x74"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x6E\x75\x6D\x62\x65\x72\x20\x6F\x66\x20\x6F\x62\x6A\x65\x63\x74\x20\x69\x6E\x73\x74\x61\x6E\x63\x65\x73\x20\x74\x6F\x20\x62\x65\x20\x70\x6C\x61\x63\x65\x64\x2C\x20\x65\x78\x63\x6C\x75\x64\x69\x6E\x67\x20\x74\x68\x65\x20\x65\x6E\x64\x70\x6F\x69\x6E\x74\x73\x2E\x20\x54\x68\x61\x74\x20\x69\x73\x2C\x20\x74\x68\x65\x20\x74\x6F\x74\x61\x6C\x20\x6E\x75\x6D\x62\x65\x72\x20\x6F\x66\x20\x69\x6E\x73\x74\x61\x6E\x63\x65\x73\x20\x69\x73\x20\x65\x71\x75\x61\x6C\x20\x74\x6F\x20\x6F\x62\x6A\x65\x63\x74\x43\x6F\x75\x6E\x74\x20\x2B\x20\x32"), NULL);
	}
}
static void PlaceAlongPath_t0F05120A71DC49EFA8C7D544DFB7B92C878BAB52_CustomAttributesCacheGenerator_SplineSampleSize(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x73\x70\x6C\x69\x6E\x65\x53\x61\x6D\x70\x6C\x65\x53\x69\x7A\x65"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x73\x69\x7A\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x61\x6D\x70\x6C\x65\x20\x75\x73\x65\x64\x20\x74\x6F\x20\x63\x61\x6C\x63\x75\x6C\x61\x74\x65\x20\x74\x68\x65\x20\x73\x70\x6C\x69\x6E\x65\x2E"), NULL);
	}
}
static void PlaceAlongPath_t0F05120A71DC49EFA8C7D544DFB7B92C878BAB52_CustomAttributesCacheGenerator_DebugMode(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x62\x75\x67"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x57\x68\x65\x6E\x20\x64\x65\x62\x75\x67\x20\x6D\x6F\x64\x65\x20\x69\x73\x20\x65\x6E\x61\x62\x6C\x65\x64\x2C\x20\x74\x68\x69\x73\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x77\x69\x6C\x6C\x20\x70\x72\x69\x6E\x74\x20\x72\x65\x6C\x65\x76\x61\x6E\x74\x20\x6D\x65\x73\x73\x61\x67\x65\x73\x20\x74\x6F\x20\x74\x68\x65\x20\x63\x6F\x6E\x73\x6F\x6C\x65\x2E\x20\x46\x69\x6C\x74\x65\x72\x20\x62\x79\x20\x27\x50\x6C\x61\x63\x65\x41\x6C\x6F\x6E\x67\x50\x61\x74\x68\x27\x20\x69\x6E\x20\x74\x68\x65\x20\x6C\x6F\x67\x20\x6F\x75\x74\x70\x75\x74\x20\x74\x6F\x20\x73\x65\x65\x20\x74\x68\x65\x20\x6D\x65\x73\x73\x61\x67\x65\x73\x2E"), NULL);
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[2];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 4.0f, NULL);
	}
}
static void PlaceAlongPath_t0F05120A71DC49EFA8C7D544DFB7B92C878BAB52_CustomAttributesCacheGenerator_spline(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 4.0f, NULL);
	}
}
static void OverrideAltitudeData_tC8B4EAB3FF4B6428F3185C23310A9A4E23CD82DA_CustomAttributesCacheGenerator_OverrideAltitude(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x74\x72\x75\x65\x2C\x20\x6F\x76\x65\x72\x72\x69\x64\x65\x20\x74\x68\x65\x20\x4C\x6F\x63\x61\x74\x69\x6F\x6E\x44\x61\x74\x61\x27\x73\x20\x61\x6C\x74\x69\x74\x75\x64\x65\x20\x6F\x70\x74\x69\x6F\x6E\x73\x2E"), NULL);
	}
}
static void OverrideAltitudeData_tC8B4EAB3FF4B6428F3185C23310A9A4E23CD82DA_CustomAttributesCacheGenerator_Altitude(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x6F\x76\x65\x72\x72\x69\x64\x65\x20\x61\x6C\x74\x69\x74\x75\x64\x65\x20\x76\x61\x6C\x75\x65\x2E"), NULL);
	}
}
static void OverrideAltitudeData_tC8B4EAB3FF4B6428F3185C23310A9A4E23CD82DA_CustomAttributesCacheGenerator_AltitudeMode(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x6F\x76\x65\x72\x72\x69\x64\x65\x20\x61\x6C\x74\x69\x74\x75\x64\x65\x20\x6D\x6F\x64\x65\x2E"), NULL);
	}
}
static void LocationPropertyData_t9266443C0DBD58342B2A5B94E35D3FBA4F254341_CustomAttributesCacheGenerator_LocationInputType(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x74\x79\x70\x65\x20\x6F\x66\x20\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x20\x63\x6F\x6F\x72\x64\x69\x6E\x61\x74\x65\x20\x69\x6E\x70\x75\x74\x20\x75\x73\x65\x64\x2E\x20\x45\x69\x74\x68\x65\x72\x20\x27\x4C\x6F\x63\x61\x74\x69\x6F\x6E\x27\x20\x74\x6F\x20\x64\x69\x72\x65\x63\x74\x6C\x79\x20\x69\x6E\x70\x75\x74\x20\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x20\x63\x6F\x6F\x72\x64\x69\x6E\x61\x74\x65\x73\x2C\x20\x6F\x72\x20\x27\x4C\x6F\x63\x61\x74\x69\x6F\x6E\x44\x61\x74\x61\x27\x20\x74\x6F\x20\x75\x73\x65\x20\x61\x20\x53\x63\x72\x69\x70\x74\x61\x62\x6C\x65\x4F\x62\x6A\x65\x63\x74\x2E"), NULL);
	}
}
static void LocationPropertyData_t9266443C0DBD58342B2A5B94E35D3FBA4F254341_CustomAttributesCacheGenerator_LocationData(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x20\x4C\x6F\x63\x61\x74\x69\x6F\x6E\x44\x61\x74\x61\x20\x53\x63\x72\x69\x70\x74\x61\x62\x6C\x65\x4F\x62\x6A\x65\x63\x74\x20\x73\x74\x6F\x72\x69\x6E\x67\x20\x74\x68\x65\x20\x64\x65\x73\x69\x72\x65\x64\x20\x47\x50\x53\x20\x63\x6F\x6F\x72\x64\x69\x6E\x61\x74\x65\x73\x20\x74\x6F\x20\x70\x6C\x61\x63\x65\x20\x74\x68\x65\x20\x6F\x62\x6A\x65\x63\x74\x2E"), NULL);
	}
}
static void LocationPropertyData_t9266443C0DBD58342B2A5B94E35D3FBA4F254341_CustomAttributesCacheGenerator_Location(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x70\x75\x74\x20\x74\x68\x65\x20\x64\x65\x73\x69\x72\x65\x64\x20\x47\x50\x53\x20\x63\x6F\x6F\x72\x64\x69\x6E\x61\x74\x65\x73\x20\x68\x65\x72\x65\x2E"), NULL);
	}
}
static void LocationPropertyData_t9266443C0DBD58342B2A5B94E35D3FBA4F254341_CustomAttributesCacheGenerator_OverrideAltitudeData(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x20\x74\x68\x69\x73\x20\x74\x6F\x20\x6F\x76\x65\x72\x72\x69\x64\x65\x20\x74\x68\x65\x20\x4C\x6F\x63\x61\x74\x69\x6F\x6E\x44\x61\x74\x61\x27\x73\x20\x61\x6C\x74\x69\x74\x75\x64\x65\x20\x6F\x70\x74\x69\x6F\x6E\x73\x2E"), NULL);
	}
}
static void PlaceAtLocation_t8F470A16D7F7E3A37FEB114A3879DD5136EE24BF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x41\x52\x2B\x47\x50\x53\x2F\x50\x6C\x61\x63\x65\x20\x41\x74\x20\x4C\x6F\x63\x61\x74\x69\x6F\x6E"), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x68\x74\x74\x70\x3A\x2F\x2F\x64\x6F\x63\x73\x2E\x75\x6E\x69\x74\x79\x2D\x61\x72\x2D\x67\x70\x73\x2D\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x2E\x63\x6F\x6D\x2F\x67\x75\x69\x64\x65\x2F\x23\x70\x6C\x61\x63\x65\x61\x74\x6C\x6F\x63\x61\x74\x69\x6F\x6E"), NULL);
	}
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[2];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void PlaceAtLocation_t8F470A16D7F7E3A37FEB114A3879DD5136EE24BF_CustomAttributesCacheGenerator_PlacementOptions(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 4.0f, NULL);
	}
}
static void PlaceAtLocation_t8F470A16D7F7E3A37FEB114A3879DD5136EE24BF_CustomAttributesCacheGenerator_DebugMode(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 4.0f, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x62\x75\x67"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[2];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x57\x68\x65\x6E\x20\x64\x65\x62\x75\x67\x20\x6D\x6F\x64\x65\x20\x69\x73\x20\x65\x6E\x61\x62\x6C\x65\x64\x2C\x20\x74\x68\x69\x73\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x77\x69\x6C\x6C\x20\x70\x72\x69\x6E\x74\x20\x72\x65\x6C\x65\x76\x61\x6E\x74\x20\x6D\x65\x73\x73\x61\x67\x65\x73\x20\x74\x6F\x20\x74\x68\x65\x20\x63\x6F\x6E\x73\x6F\x6C\x65\x2E\x20\x46\x69\x6C\x74\x65\x72\x20\x62\x79\x20\x27\x50\x6C\x61\x74\x65\x41\x74\x4C\x6F\x63\x61\x74\x69\x6F\x6E\x27\x20\x69\x6E\x20\x74\x68\x65\x20\x6C\x6F\x67\x20\x6F\x75\x74\x70\x75\x74\x20\x74\x6F\x20\x73\x65\x65\x20\x74\x68\x65\x20\x6D\x65\x73\x73\x61\x67\x65\x73\x2E\x20\x49\x74\x20\x77\x69\x6C\x6C\x20\x61\x6C\x73\x6F\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x74\x68\x65\x20\x64\x69\x72\x65\x63\x74\x69\x6F\x6E\x20\x66\x72\x6F\x6D\x20\x74\x68\x65\x20\x75\x73\x65\x72\x20\x74\x6F\x20\x74\x68\x65\x20\x6F\x62\x6A\x65\x63\x74\x20\x6F\x6E\x20\x74\x68\x65\x20\x73\x63\x72\x65\x65\x6E\x2C\x20\x61\x73\x20\x77\x65\x6C\x6C\x20\x61\x73\x20\x61\x20\x6C\x69\x6E\x65\x20\x72\x65\x6E\x64\x65\x72\x65\x72\x20\x66\x72\x6F\x6D\x20\x74\x68\x65\x20\x63\x61\x6D\x65\x72\x61\x20\x74\x6F\x20\x74\x68\x65\x20\x6F\x62\x6A\x65\x63\x74\x20\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x2E\x20\x54\x6F\x20\x63\x75\x73\x74\x6F\x6D\x69\x7A\x65\x20\x68\x6F\x77\x20\x74\x68\x69\x73\x20\x6C\x69\x6E\x65\x20\x6C\x6F\x6F\x6B\x73\x2C\x20\x61\x64\x64\x20\x61\x20\x4C\x69\x6E\x65\x20\x52\x65\x6E\x64\x65\x72\x65\x72\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x74\x6F\x20\x74\x68\x69\x73\x20\x67\x61\x6D\x65\x20\x6F\x62\x6A\x65\x63\x74\x2E"), NULL);
	}
}
static void PlaceAtLocation_t8F470A16D7F7E3A37FEB114A3879DD5136EE24BF_CustomAttributesCacheGenerator_ObjectLocationUpdated(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x45\x76\x65\x6E\x74\x20\x63\x61\x6C\x6C\x65\x64\x20\x77\x68\x65\x6E\x20\x74\x68\x65\x20\x6F\x62\x6A\x65\x63\x74\x27\x73\x20\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x20\x69\x73\x20\x75\x70\x64\x61\x74\x65\x64\x2E\x20\x54\x68\x65\x20\x61\x72\x67\x75\x6D\x65\x6E\x74\x73\x20\x61\x72\x65\x20\x74\x68\x65\x20\x63\x75\x72\x72\x65\x6E\x74\x20\x47\x61\x6D\x65\x4F\x62\x6A\x65\x63\x74\x2C\x20\x74\x68\x65\x20\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x2C\x20\x61\x6E\x64\x20\x74\x68\x65\x20\x6E\x75\x6D\x62\x65\x72\x20\x6F\x66\x20\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x20\x75\x70\x64\x61\x74\x65\x73\x20\x72\x65\x63\x65\x69\x76\x65\x64\x20\x62\x79\x20\x74\x68\x65\x20\x6F\x62\x6A\x65\x63\x74\x20\x73\x6F\x20\x66\x61\x72\x2E"), NULL);
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[1];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 4.0f, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[2];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x45\x76\x65\x6E\x74\x73"), NULL);
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[3];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 4.0f, NULL);
	}
}
static void PlaceAtLocation_t8F470A16D7F7E3A37FEB114A3879DD5136EE24BF_CustomAttributesCacheGenerator_ObjectPositionUpdated(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x45\x76\x65\x6E\x74\x20\x63\x61\x6C\x6C\x65\x64\x20\x77\x68\x65\x6E\x20\x74\x68\x65\x20\x6F\x62\x6A\x65\x63\x74\x27\x73\x20\x70\x6F\x73\x69\x74\x69\x6F\x6E\x20\x69\x73\x20\x75\x70\x64\x61\x74\x65\x64\x20\x61\x66\x74\x65\x72\x20\x61\x20\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x20\x75\x70\x64\x61\x74\x65\x2E\x20\x49\x66\x20\x74\x68\x65\x20\x4D\x6F\x76\x65\x6D\x65\x6E\x74\x20\x53\x6D\x6F\x6F\x74\x68\x69\x6E\x67\x20\x69\x73\x20\x6C\x61\x72\x67\x65\x72\x20\x74\x68\x61\x6E\x20\x30\x2C\x20\x74\x68\x69\x73\x20\x77\x69\x6C\x6C\x20\x66\x69\x72\x65\x20\x61\x74\x20\x61\x20\x6C\x61\x74\x65\x72\x20\x74\x69\x6D\x65\x20\x74\x68\x61\x6E\x20\x74\x68\x65\x20\x4C\x6F\x63\x61\x74\x69\x6F\x6E\x20\x55\x70\x64\x61\x74\x65\x64\x20\x65\x76\x65\x6E\x74\x2E\x20\x20\x54\x68\x65\x20\x61\x72\x67\x75\x6D\x65\x6E\x74\x73\x20\x61\x72\x65\x20\x74\x68\x65\x20\x63\x75\x72\x72\x65\x6E\x74\x20\x47\x61\x6D\x65\x4F\x62\x6A\x65\x63\x74\x2C\x20\x74\x68\x65\x20\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x2C\x20\x61\x6E\x64\x20\x74\x68\x65\x20\x6E\x75\x6D\x62\x65\x72\x20\x6F\x66\x20\x70\x6F\x73\x69\x74\x69\x6F\x6E\x20\x75\x70\x64\x61\x74\x65\x73\x20\x72\x65\x63\x65\x69\x76\x65\x64\x20\x62\x79\x20\x74\x68\x65\x20\x6F\x62\x6A\x65\x63\x74\x20\x73\x6F\x20\x66\x61\x72\x2E"), NULL);
	}
}
static void PlaceAtOptions_t502DAE3C6DF99BCAC137CD1E1F292A46DE07C4DD_CustomAttributesCacheGenerator_MovementSmoothing(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x73\x6D\x6F\x6F\x74\x68\x69\x6E\x67\x20\x66\x61\x63\x74\x6F\x72\x20\x66\x6F\x72\x20\x6D\x6F\x76\x65\x6D\x65\x6E\x74\x20\x64\x75\x65\x20\x74\x6F\x20\x47\x50\x53\x20\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x20\x61\x64\x6A\x75\x73\x74\x6D\x65\x6E\x74\x73\x3B\x20\x69\x66\x20\x73\x65\x74\x20\x74\x6F\x20\x7A\x65\x72\x6F\x20\x69\x74\x20\x69\x73\x20\x64\x69\x73\x61\x62\x6C\x65\x64\x2E"), NULL);
	}
}
static void PlaceAtOptions_t502DAE3C6DF99BCAC137CD1E1F292A46DE07C4DD_CustomAttributesCacheGenerator_MaxNumberOfLocationUpdates(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x6D\x61\x78\x69\x6D\x75\x6D\x20\x6E\x75\x6D\x62\x65\x72\x20\x6F\x66\x20\x74\x69\x6D\x65\x73\x20\x74\x68\x69\x73\x20\x6F\x62\x6A\x65\x63\x74\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x61\x66\x66\x65\x63\x74\x65\x64\x20\x62\x79\x20\x47\x50\x53\x20\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x20\x75\x70\x64\x61\x74\x65\x73\x2E\x20\x5A\x65\x72\x6F\x20\x6D\x65\x61\x6E\x73\x20\x6E\x6F\x20\x6C\x69\x6D\x69\x74\x73\x20\x61\x72\x65\x20\x69\x6D\x70\x6F\x73\x65\x64\x2E"), NULL);
	}
}
static void PlaceAtOptions_t502DAE3C6DF99BCAC137CD1E1F292A46DE07C4DD_CustomAttributesCacheGenerator_UseMovingAverage(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x74\x72\x75\x65\x2C\x20\x75\x73\x65\x20\x61\x20\x6D\x6F\x76\x69\x6E\x67\x20\x61\x76\x65\x72\x61\x67\x65\x20\x66\x69\x6C\x74\x65\x72\x2E"), NULL);
	}
}
static void PlaceAtOptions_t502DAE3C6DF99BCAC137CD1E1F292A46DE07C4DD_CustomAttributesCacheGenerator_HideObjectUntilItIsPlaced(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x74\x72\x75\x65\x2C\x20\x74\x68\x65\x20\x6F\x62\x6A\x65\x63\x74\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x68\x69\x64\x64\x65\x6E\x20\x75\x6E\x74\x69\x6C\x20\x74\x68\x65\x20\x6F\x62\x6A\x65\x63\x74\x20\x69\x73\x20\x70\x6C\x61\x63\x65\x64\x20\x61\x74\x20\x74\x68\x65\x20\x67\x65\x6F\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x2E\x20\x49\x66\x20\x77\x69\x6C\x6C\x20\x65\x6E\x61\x62\x6C\x65\x2F\x64\x69\x73\x61\x62\x6C\x65\x20\x74\x68\x65\x20\x4D\x65\x73\x68\x52\x65\x6E\x64\x65\x72\x65\x72\x20\x6F\x72\x20\x53\x6B\x69\x6E\x6E\x65\x64\x4D\x65\x73\x68\x52\x65\x6E\x64\x65\x72\x65\x72\x20\x77\x68\x65\x6E\x20\x61\x76\x61\x69\x6C\x61\x62\x6C\x65\x2C\x20\x61\x6E\x64\x20\x65\x6E\x61\x62\x6C\x65\x2F\x64\x69\x73\x61\x62\x6C\x65\x20\x61\x6C\x6C\x20\x63\x68\x69\x6C\x64\x20\x67\x61\x6D\x65\x20\x6F\x62\x6A\x65\x63\x74\x73\x2E"), NULL);
	}
}
static void PlaceAtOptions_t502DAE3C6DF99BCAC137CD1E1F292A46DE07C4DD_CustomAttributesCacheGenerator_ShowObjectAfterThisManyUpdates(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x6E\x75\x6D\x62\x65\x72\x20\x6F\x66\x20\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x20\x75\x70\x64\x61\x74\x65\x73\x20\x74\x6F\x20\x77\x61\x69\x74\x20\x75\x6E\x74\x69\x6C\x20\x74\x68\x65\x20\x6F\x62\x6A\x65\x63\x74\x20\x69\x73\x20\x73\x68\x6F\x77\x6E\x20\x61\x66\x74\x65\x72\x20\x62\x65\x69\x6E\x67\x20\x69\x6E\x69\x74\x69\x61\x6C\x6C\x79\x20\x68\x69\x64\x64\x65\x6E\x20\x66\x72\x6F\x6D\x20\x76\x69\x65\x77\x2E\x20\x4F\x6E\x6C\x79\x20\x77\x6F\x72\x6B\x73\x20\x77\x68\x65\x6E\x20\x27\x48\x69\x64\x65\x20\x4F\x62\x6A\x65\x63\x74\x20\x55\x6E\x74\x69\x6C\x20\x49\x74\x20\x49\x73\x20\x50\x6C\x61\x63\x65\x64\x27\x20\x69\x73\x20\x73\x65\x74\x20\x74\x6F\x20\x74\x72\x75\x65\x2E\x20\x49\x66\x20\x74\x68\x69\x73\x20\x69\x73\x20\x73\x65\x74\x20\x74\x6F\x20\x30\x2C\x20\x27\x48\x69\x64\x65\x20\x4F\x62\x6A\x65\x63\x74\x20\x55\x6E\x74\x69\x6C\x20\x49\x74\x20\x49\x73\x20\x50\x6C\x61\x63\x65\x64\x27\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x64\x69\x73\x61\x62\x6C\x65\x64\x2E"), NULL);
	}
	{
		ConditionalPropertyAttribute_t541C921FB3DDA3968D4040FB64D66D010BFBE8EE * tmp = (ConditionalPropertyAttribute_t541C921FB3DDA3968D4040FB64D66D010BFBE8EE *)cache->attributes[1];
		ConditionalPropertyAttribute__ctor_mF13EDBB8FE80AC495C46C1452521FD585EE33F14(tmp, il2cpp_codegen_string_new_wrapper("\x48\x69\x64\x65\x4F\x62\x6A\x65\x63\x74\x55\x6E\x74\x69\x6C\x49\x74\x49\x73\x50\x6C\x61\x63\x65\x64"), NULL);
	}
}
static void PlaceAtLocations_t8C777F409D11DA3480CDE0F8723857FAA7A4CB3C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x41\x52\x2B\x47\x50\x53\x2F\x50\x6C\x61\x63\x65\x20\x41\x74\x20\x4C\x6F\x63\x61\x74\x69\x6F\x6E\x73"), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x68\x74\x74\x70\x3A\x2F\x2F\x64\x6F\x63\x73\x2E\x75\x6E\x69\x74\x79\x2D\x61\x72\x2D\x67\x70\x73\x2D\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x2E\x63\x6F\x6D\x2F\x67\x75\x69\x64\x65\x2F\x23\x70\x6C\x61\x63\x65\x61\x74\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x73"), NULL);
	}
}
static void PlaceAtLocations_t8C777F409D11DA3480CDE0F8723857FAA7A4CB3C_CustomAttributesCacheGenerator_Locations(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x73\x20\x77\x68\x65\x72\x65\x20\x74\x68\x65\x20\x6F\x62\x6A\x65\x63\x74\x73\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x69\x6E\x73\x74\x61\x6E\x74\x69\x61\x74\x65\x64\x2E"), NULL);
	}
}
static void PlaceAtLocations_t8C777F409D11DA3480CDE0F8723857FAA7A4CB3C_CustomAttributesCacheGenerator_Prefab(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x67\x61\x6D\x65\x20\x6F\x62\x6A\x65\x63\x74\x20\x74\x68\x61\x74\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x69\x6E\x73\x74\x61\x6E\x74\x69\x61\x74\x65\x64\x2E"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x70\x72\x65\x66\x61\x62"), NULL);
	}
}
static void PlaceAtLocations_t8C777F409D11DA3480CDE0F8723857FAA7A4CB3C_CustomAttributesCacheGenerator_DebugMode(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x62\x75\x67"), NULL);
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[1];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 4.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[2];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x57\x68\x65\x6E\x20\x64\x65\x62\x75\x67\x20\x6D\x6F\x64\x65\x20\x69\x73\x20\x65\x6E\x61\x62\x6C\x65\x64\x2C\x20\x74\x68\x69\x73\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x77\x69\x6C\x6C\x20\x70\x72\x69\x6E\x74\x20\x72\x65\x6C\x65\x76\x61\x6E\x74\x20\x6D\x65\x73\x73\x61\x67\x65\x73\x20\x74\x6F\x20\x74\x68\x65\x20\x63\x6F\x6E\x73\x6F\x6C\x65\x2E\x20\x46\x69\x6C\x74\x65\x72\x20\x62\x79\x20\x27\x50\x6C\x61\x74\x65\x41\x74\x4C\x6F\x63\x61\x74\x69\x6F\x6E\x73\x27\x20\x69\x6E\x20\x74\x68\x65\x20\x6C\x6F\x67\x20\x6F\x75\x74\x70\x75\x74\x20\x74\x6F\x20\x73\x65\x65\x20\x74\x68\x65\x20\x6D\x65\x73\x73\x61\x67\x65\x73\x2E"), NULL);
	}
}
static void PlaceAtLocations_t8C777F409D11DA3480CDE0F8723857FAA7A4CB3C_CustomAttributesCacheGenerator_locations(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 4.0f, NULL);
	}
}
static void RenderPathLine_t3873DC0DE3A6052CE288EAA763EDFDB34FBED02A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x41\x52\x2B\x47\x50\x53\x2F\x52\x65\x6E\x64\x65\x72\x20\x50\x61\x74\x68\x20\x4C\x69\x6E\x65"), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x68\x74\x74\x70\x3A\x2F\x2F\x64\x6F\x63\x73\x2E\x75\x6E\x69\x74\x79\x2D\x61\x72\x2D\x67\x70\x73\x2D\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x2E\x63\x6F\x6D\x2F\x67\x75\x69\x64\x65\x2F\x23\x72\x65\x6E\x64\x65\x72\x70\x61\x74\x68\x6C\x69\x6E\x65"), NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_U3COptionsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_U3CCurrentLocationU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_U3CLastLocationU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_U3CLastLocationRawU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_U3CCurrentLocationRawU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_U3CCurrentHeadingU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_U3CLastHeadingU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_U3CFirstLocationU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_U3CStatusU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_U3CIsEnabledU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_U3CFirstReadingU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_U3CStartTimeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_U3CPausedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_U3CLocationUpdateCountU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_U3CApplyCompassTiltCompensationOnAndroidU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_LocationUpdated(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_CompassUpdated(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_LocationEnabled(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_LocationFailed(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_LocationUpdatedRaw(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_get_Options_m45AFB8A040845CC8970328A9FE96293DFD66CCBD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_set_Options_m384397626F3231DE2648F61DFEFEDD8BFBCB1A44(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_get_CurrentLocation_mC04CF8FFD09A254E309F0EBBFC0434F4E2041684(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_set_CurrentLocation_mD9694B36EA445C60C398D489B7EBAF69F50B7EB1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_get_LastLocation_m5E185AE31CF2C37237F036F1DCC7BD459596BB77(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_set_LastLocation_mC3B207433454B09C3868932D3DC1AFD16B600415(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_get_LastLocationRaw_mDEADE5CF1022B15A1592F0929B0BC21D9F2CC977(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_set_LastLocationRaw_m5D57CA6540787FD2E24B6478CDAF86B859A16C0E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_get_CurrentLocationRaw_m880B4211D7DAEB0AB39ED52F4A813758A9605056(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_set_CurrentLocationRaw_m26F24C4C0B3B60DCF9B22EC4E8466A0A0ED8675F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_get_CurrentHeading_mD80D19546F4443BB60E7E9D61E59B58CDEC1199F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_set_CurrentHeading_mB74106DF4587EBDE3F57FF479E60F32F5E9FFFF1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_get_LastHeading_mB01E086263805EA68D7B80ABA0CDDFD0445F917C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_set_LastHeading_mD2CF2AF6A78A7E114287296DBEB2EC0ABBB12F96(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_get_FirstLocation_m60E03F687A379A4335F2DBE0F58F1E20A2C5C5D2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_set_FirstLocation_mF220D93D45C42245EC4771C0F8C59C8864BA1732(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_get_Status_m0BEEE7DFFE907C117F8A146C6065F383FAD19A43(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_set_Status_m3F097F1D9F22EB71392A734225177BB87A295D47(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_get_IsEnabled_mBD269C10E329A27A38D66A7CA158743B97B58F89(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_set_IsEnabled_m3A32454858129B89353398A2743017144C6B685F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_get_FirstReading_m5006CFC216447EE18516095A7CF062C6A81650B2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_set_FirstReading_m85CC459283A9388061409706249A862A29577952(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_get_StartTime_m64773366D843563C916E242F620BE5E473A7B689(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_set_StartTime_mCD10C0C5AEC1FAE41EAB6CF699EF02B13237CFB4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_get_Paused_m8D420B15FA656A2B16A0C47AFD05679D151A4180(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_set_Paused_m2F74FD61266871E92E9CDAD730AB25AE7B69EE39(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_get_LocationUpdateCount_mEA51D40856CC1555C931628EDEEEDAB73FA14F53(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_set_LocationUpdateCount_m814551EB5ACE513616AB4C6BB2C612D6EB71A59E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_get_ApplyCompassTiltCompensationOnAndroid_m298A7CD45897CC2D57A5BA30527F08DC4E54302A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_set_ApplyCompassTiltCompensationOnAndroid_mD59851680B505575BA8A0D87823C1281C854E17A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_add_LocationUpdated_m99954AADAB5627FAB20F3A3718C7DBFAC4E164AB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_remove_LocationUpdated_m90482A177E20A2BDF8C35EB365F6072FE1F3A528(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_add_CompassUpdated_m63BE5871D33D25A91831B24568F9766D22150521(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_remove_CompassUpdated_m834BB886E4F3C2D8F0914735E9C7D9B19206FAB2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_add_LocationEnabled_m04F24C06BA328800DCF23DEF9B1B444D9161B6E9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_remove_LocationEnabled_m56E3C2816E869919163DAEBA9519EF47272A4C13(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_add_LocationFailed_m62E22AD9698B257D3CE4DDE77031156BB1C19739(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_remove_LocationFailed_mAA22271EB80E5E787F10963DBEE054C0D5D928EC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_add_LocationUpdatedRaw_m4B2F37C64CBCFFE32FD651D8C7027A405E5277EB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_remove_LocationUpdatedRaw_m113322049E3C08175A459F1FAD2FF9F6FC7E1EC6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_Start_mD5EBFCF2163C86BB9ECEA01D142CFB6E24C5BAD5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__89_tA04EB5BAC5E34AFA5EF0368C59EA9782780DF767_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__89_tA04EB5BAC5E34AFA5EF0368C59EA9782780DF767_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__89_tA04EB5BAC5E34AFA5EF0368C59EA9782780DF767_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__89_tA04EB5BAC5E34AFA5EF0368C59EA9782780DF767_CustomAttributesCacheGenerator_U3CStartU3Ed__89__ctor_m1D15935BFD90E695E3C979F3F1F4BCF71CE3F9DC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__89_tA04EB5BAC5E34AFA5EF0368C59EA9782780DF767_CustomAttributesCacheGenerator_U3CStartU3Ed__89_System_IDisposable_Dispose_mB8AA0F35E6C40D991491A3390F07BC1EB6B96B30(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__89_tA04EB5BAC5E34AFA5EF0368C59EA9782780DF767_CustomAttributesCacheGenerator_U3CStartU3Ed__89_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6423F0FB4D41529D08427B4F0A636A851EC9F630(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__89_tA04EB5BAC5E34AFA5EF0368C59EA9782780DF767_CustomAttributesCacheGenerator_U3CStartU3Ed__89_System_Collections_IEnumerator_Reset_m554CA534C945FB9A68B28404FFC7BED1B9387085(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__89_tA04EB5BAC5E34AFA5EF0368C59EA9782780DF767_CustomAttributesCacheGenerator_U3CStartU3Ed__89_System_Collections_IEnumerator_get_Current_m339872A51678CE1543372092B74593D2BE1272EC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void LocationProviderOptions_t89EC102FE8FE7276EC9DFED9C3D71040619CE601_CustomAttributesCacheGenerator_TimeBetweenUpdates(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x6D\x69\x6E\x69\x6D\x75\x6D\x20\x64\x65\x73\x69\x72\x65\x64\x20\x75\x70\x64\x61\x74\x65\x20\x74\x69\x6D\x65\x2C\x20\x69\x6E\x20\x73\x65\x63\x6F\x6E\x64\x73\x2E"), NULL);
	}
}
static void LocationProviderOptions_t89EC102FE8FE7276EC9DFED9C3D71040619CE601_CustomAttributesCacheGenerator_MinDistanceBetweenUpdates(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x6D\x69\x6E\x69\x6D\x75\x6D\x20\x64\x69\x73\x74\x61\x6E\x63\x65\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x63\x6F\x6E\x73\x65\x63\x75\x74\x69\x76\x65\x20\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x20\x75\x70\x64\x61\x74\x65\x73\x2C\x20\x69\x6E\x20\x6D\x65\x74\x65\x72\x73\x2E"), NULL);
	}
}
static void LocationProviderOptions_t89EC102FE8FE7276EC9DFED9C3D71040619CE601_CustomAttributesCacheGenerator_AccuracyRadius(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x6D\x69\x6E\x69\x6D\x75\x6D\x20\x61\x63\x63\x75\x72\x61\x63\x79\x20\x6F\x66\x20\x61\x63\x63\x65\x70\x74\x65\x64\x20\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x20\x6D\x65\x61\x73\x75\x72\x65\x6D\x65\x6E\x74\x73\x2C\x20\x69\x6E\x20\x6D\x65\x74\x65\x72\x73\x2E\x20\x41\x63\x63\x75\x72\x61\x63\x79\x20\x68\x65\x72\x65\x20\x6D\x65\x61\x6E\x73\x20\x74\x68\x65\x20\x72\x61\x64\x69\x75\x73\x20\x6F\x66\x20\x75\x6E\x63\x65\x72\x74\x61\x69\x6E\x74\x79\x20\x6F\x66\x20\x74\x68\x65\x20\x64\x65\x76\x69\x63\x65\x27\x73\x20\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x2C\x20\x64\x65\x66\x69\x6E\x69\x6E\x67\x20\x61\x20\x63\x69\x72\x63\x6C\x65\x20\x77\x68\x65\x72\x65\x20\x69\x74\x20\x63\x61\x6E\x20\x70\x6F\x73\x73\x69\x62\x6C\x79\x20\x62\x65\x20\x66\x6F\x75\x6E\x64\x20\x69\x6E\x2E"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x78\x41\x63\x63\x75\x72\x61\x63\x79\x52\x61\x64\x69\x75\x73"), NULL);
	}
}
static void LocationProviderOptions_t89EC102FE8FE7276EC9DFED9C3D71040619CE601_CustomAttributesCacheGenerator_MaxNumberOfUpdates(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x67\x6C\x6F\x62\x61\x6C\x20\x6D\x61\x78\x69\x6D\x75\x6D\x20\x6E\x75\x6D\x62\x65\x72\x20\x6F\x66\x20\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x20\x75\x70\x64\x61\x74\x65\x73\x2E\x20\x54\x68\x65\x20\x75\x70\x64\x61\x74\x65\x73\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x70\x61\x75\x73\x65\x64\x20\x61\x66\x74\x65\x72\x20\x74\x68\x69\x73\x20\x61\x6D\x6F\x75\x6E\x74\x2E\x20\x5A\x65\x72\x6F\x20\x6D\x65\x61\x6E\x73\x20\x74\x68\x65\x72\x65\x20\x69\x73\x20\x6E\x6F\x20\x6C\x69\x6D\x69\x74\x20\x61\x6E\x64\x20\x74\x68\x65\x20\x75\x70\x64\x61\x74\x65\x73\x20\x77\x6F\x6E\x27\x74\x20\x62\x65\x20\x70\x61\x75\x73\x65\x64\x20\x61\x75\x74\x6F\x6D\x61\x74\x69\x63\x61\x6C\x6C\x79\x2E\x20\x4E\x6F\x74\x65\x20\x74\x68\x61\x74\x20\x74\x68\x69\x73\x20\x77\x69\x6C\x6C\x20\x70\x6F\x73\x73\x69\x62\x6C\x79\x20\x6F\x76\x65\x72\x72\x69\x64\x65\x20\x74\x68\x65\x20\x73\x65\x74\x74\x69\x6E\x67\x73\x20\x66\x72\x6F\x6D\x20\x69\x6E\x64\x69\x76\x69\x64\x75\x61\x6C\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x73\x2C\x20\x6C\x69\x6B\x65\x20\x27\x50\x6C\x61\x63\x65\x41\x74\x4C\x6F\x63\x61\x74\x69\x6F\x6E\x27\x2E"), NULL);
	}
}
static void ILocationProvider_t4046DCCD18259875A852462DFC63D67B7DFBF640_CustomAttributesCacheGenerator_ILocationProvider_add_LocationUpdated_mBF35FA6BB2B61615AEC352A7958C89A04F6993E3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ILocationProvider_t4046DCCD18259875A852462DFC63D67B7DFBF640_CustomAttributesCacheGenerator_ILocationProvider_remove_LocationUpdated_mF2A8F2F6F36403B4B915291B2C65D83D545E979E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ILocationProvider_t4046DCCD18259875A852462DFC63D67B7DFBF640_CustomAttributesCacheGenerator_ILocationProvider_add_LocationUpdatedRaw_m1B6A89FB978ECC363F7505786082825D5DF75958(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ILocationProvider_t4046DCCD18259875A852462DFC63D67B7DFBF640_CustomAttributesCacheGenerator_ILocationProvider_remove_LocationUpdatedRaw_m6BC3AE80D28DAE0FCBC5366F8FBDCB1CA41E0D76(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ILocationProvider_t4046DCCD18259875A852462DFC63D67B7DFBF640_CustomAttributesCacheGenerator_ILocationProvider_add_CompassUpdated_mF281DEC5D92A5255375CDFD02C15F2EC40C104CA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ILocationProvider_t4046DCCD18259875A852462DFC63D67B7DFBF640_CustomAttributesCacheGenerator_ILocationProvider_remove_CompassUpdated_mFC84A1AB8A828A30442E998763ED32DA957A9F34(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ILocationProvider_t4046DCCD18259875A852462DFC63D67B7DFBF640_CustomAttributesCacheGenerator_ILocationProvider_add_LocationEnabled_m4E14056B4D6803810444AFA24FE6977B165C5B48(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ILocationProvider_t4046DCCD18259875A852462DFC63D67B7DFBF640_CustomAttributesCacheGenerator_ILocationProvider_remove_LocationEnabled_m201B4E151A7D9D3C46F2887E203D55132D0B4184(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ILocationProvider_t4046DCCD18259875A852462DFC63D67B7DFBF640_CustomAttributesCacheGenerator_ILocationProvider_add_LocationFailed_m330504FC106356485A538FB65453FA6888D7F915(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ILocationProvider_t4046DCCD18259875A852462DFC63D67B7DFBF640_CustomAttributesCacheGenerator_ILocationProvider_remove_LocationFailed_m4F850E583325BE578362CE7417E021964A100D4F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Location_tF89221C24420A624E9A69BB9E7D678B78ABCFF89_CustomAttributesCacheGenerator_Latitude(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x6C\x61\x74\x69\x74\x75\x64\x65"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x6C\x61\x74\x69\x74\x75\x64\x65\x2C\x20\x69\x6E\x20\x64\x65\x67\x72\x65\x65\x73\x2E"), NULL);
	}
}
static void Location_tF89221C24420A624E9A69BB9E7D678B78ABCFF89_CustomAttributesCacheGenerator_Longitude(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x6C\x6F\x6E\x67\x69\x74\x75\x64\x65\x2C\x20\x69\x6E\x20\x64\x65\x67\x72\x65\x65\x73\x2E"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x6C\x6F\x6E\x67\x69\x74\x75\x64\x65"), NULL);
	}
}
static void Location_tF89221C24420A624E9A69BB9E7D678B78ABCFF89_CustomAttributesCacheGenerator_Altitude(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x61\x6C\x74\x69\x74\x75\x64\x65\x2C\x20\x69\x6E\x20\x6D\x65\x74\x65\x72\x73\x2E"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x61\x6C\x74\x69\x74\x75\x64\x65"), NULL);
	}
}
static void Location_tF89221C24420A624E9A69BB9E7D678B78ABCFF89_CustomAttributesCacheGenerator_AltitudeMode(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x61\x6C\x74\x69\x74\x75\x64\x65\x20\x6D\x6F\x64\x65\x2E\x20\x27\x41\x62\x73\x6F\x6C\x75\x74\x65\x27\x20\x6D\x65\x61\x6E\x73\x20\x61\x62\x73\x6F\x6C\x75\x74\x65\x20\x61\x6C\x74\x69\x74\x75\x64\x65\x2C\x20\x72\x65\x6C\x61\x74\x69\x76\x65\x20\x74\x6F\x20\x74\x68\x65\x20\x73\x65\x61\x20\x6C\x65\x76\x65\x6C\x2E\x20\x27\x44\x65\x76\x69\x63\x65\x52\x65\x6C\x61\x74\x69\x76\x65\x27\x20\x6D\x65\x61\x73\x20\x69\x74\x20\x69\x73\x20\x72\x65\x6C\x61\x74\x69\x76\x65\x20\x74\x6F\x20\x74\x68\x65\x20\x64\x65\x76\x69\x63\x65\x27\x73\x20\x69\x6E\x69\x74\x69\x61\x6C\x20\x70\x6F\x73\x69\x74\x69\x6F\x6E\x2E\x20\x27\x47\x72\x6F\x75\x6E\x64\x52\x65\x6C\x61\x74\x69\x76\x65\x27\x20\x6D\x65\x61\x6E\x73\x20\x72\x65\x6C\x61\x74\x69\x76\x65\x20\x74\x6F\x20\x74\x68\x65\x20\x6E\x65\x61\x72\x65\x73\x74\x20\x64\x65\x74\x65\x63\x74\x65\x64\x20\x70\x6C\x61\x6E\x65\x2C\x20\x61\x6E\x64\x20\x27\x49\x67\x6E\x6F\x72\x65\x27\x20\x6D\x65\x61\x6E\x73\x20\x74\x68\x65\x20\x61\x6C\x74\x69\x74\x75\x64\x65\x20\x69\x73\x20\x69\x67\x6E\x6F\x72\x65\x64\x20\x28\x65\x71\x75\x69\x76\x61\x6C\x65\x6E\x74\x20\x74\x6F\x20\x73\x65\x74\x74\x69\x6E\x67\x20\x69\x74\x20\x74\x6F\x20\x7A\x65\x72\x6F\x29\x2E"), NULL);
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[1];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 4.0f, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[2];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x61\x6C\x74\x69\x74\x75\x64\x65\x4D\x6F\x64\x65"), NULL);
	}
}
static void Location_tF89221C24420A624E9A69BB9E7D678B78ABCFF89_CustomAttributesCacheGenerator_Label(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x6C\x61\x62\x65\x6C"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x20\x6F\x70\x74\x69\x6F\x6E\x61\x6C\x20\x6C\x61\x62\x65\x6C\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x2E"), NULL);
	}
}
static void Spline_t7C4ACB7CA2206C647091AA2974A814D7CC653503_CustomAttributesCacheGenerator_U3CPointsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Spline_t7C4ACB7CA2206C647091AA2974A814D7CC653503_CustomAttributesCacheGenerator_U3CLengthU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Spline_t7C4ACB7CA2206C647091AA2974A814D7CC653503_CustomAttributesCacheGenerator_Spline_get_Points_m111B8595E12A32C1CE96DEFEFDD0B48878715273(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Spline_t7C4ACB7CA2206C647091AA2974A814D7CC653503_CustomAttributesCacheGenerator_Spline_set_Points_m1290D1B1F8FE867E67DA67B29C640FE2418FA9E3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Spline_t7C4ACB7CA2206C647091AA2974A814D7CC653503_CustomAttributesCacheGenerator_Spline_get_Length_mAC4A0448D7DE253DCE2E6281AC4106CA6E9ED377(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Spline_t7C4ACB7CA2206C647091AA2974A814D7CC653503_CustomAttributesCacheGenerator_Spline_set_Length_mE2FBD5C23E4D96D3B6DAACE44300215CBFA35F93(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t82EC83268F7F39B4AD49369938686D006EE9FA7B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ARLocationConfig_t56B7DB5281EAA81B29A1DD1283C2F793A6B1D5B8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x41\x52\x4C\x6F\x63\x61\x74\x69\x6F\x6E\x43\x6F\x6E\x66\x69\x67"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x41\x52\x2B\x47\x50\x53\x2F\x41\x52\x4C\x6F\x63\x61\x74\x69\x6F\x6E\x43\x6F\x6E\x66\x69\x67"), NULL);
	}
}
static void ARLocationConfig_t56B7DB5281EAA81B29A1DD1283C2F793A6B1D5B8_CustomAttributesCacheGenerator_EarthMeanRadiusInKM(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x45\x61\x72\x74\x68\x27\x73\x20\x6D\x65\x61\x6E\x20\x72\x61\x64\x69\x75\x73\x2C\x20\x69\x6E\x20\x6B\x69\x6C\x6F\x6D\x65\x74\x65\x72\x73\x2C\x20\x74\x6F\x20\x62\x65\x20\x75\x73\x65\x64\x20\x69\x6E\x20\x64\x69\x73\x74\x61\x6E\x63\x65\x20\x63\x61\x6C\x63\x75\x6C\x61\x74\x69\x6F\x6E\x73\x2E"), NULL);
	}
}
static void ARLocationConfig_t56B7DB5281EAA81B29A1DD1283C2F793A6B1D5B8_CustomAttributesCacheGenerator_EarthEquatorialRadiusInKM(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x65\x71\x75\x61\x74\x6F\x72\x69\x61\x6C\x20\x45\x61\x72\x74\x68\x20\x72\x61\x64\x69\x75\x73\x2C\x20\x69\x6E\x20\x6B\x69\x6C\x6F\x6D\x65\x74\x65\x72\x73\x2C\x20\x75\x73\x65\x64\x20\x69\x6E\x20\x67\x65\x6F\x2D\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x20\x63\x61\x6C\x63\x75\x6C\x61\x74\x69\x6F\x6E\x73\x2E"), NULL);
	}
}
static void ARLocationConfig_t56B7DB5281EAA81B29A1DD1283C2F793A6B1D5B8_CustomAttributesCacheGenerator_EarthFirstEccentricitySquared(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x45\x61\x72\x74\x68\x27\x73\x20\x65\x63\x63\x65\x6E\x74\x72\x69\x63\x75\x74\x79\x20\x73\x71\x75\x61\x72\x65\x64\x2C\x20\x75\x73\x65\x64\x20\x69\x6E\x20\x67\x65\x6F\x2D\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x20\x63\x61\x6C\x63\x75\x6C\x61\x74\x69\x6F\x6E\x73\x2E"), NULL);
	}
}
static void ARLocationConfig_t56B7DB5281EAA81B29A1DD1283C2F793A6B1D5B8_CustomAttributesCacheGenerator_InitialGroundHeightGuess(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x69\x6E\x69\x74\x69\x61\x6C\x20\x67\x72\x6F\x75\x6E\x64\x20\x68\x65\x69\x67\x68\x74\x20\x67\x75\x65\x73\x73\x2C\x20\x72\x65\x6C\x61\x74\x69\x76\x65\x20\x66\x72\x6F\x6D\x20\x74\x68\x65\x20\x64\x65\x76\x69\x63\x65\x20\x70\x6F\x73\x69\x74\x69\x6F\x6E\x2E"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 10.0f, NULL);
	}
}
static void ARLocationConfig_t56B7DB5281EAA81B29A1DD1283C2F793A6B1D5B8_CustomAttributesCacheGenerator_MinGroundHeight(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x69\x6E\x69\x74\x69\x61\x6C\x20\x67\x72\x6F\x75\x6E\x64\x20\x68\x65\x69\x67\x68\x74\x20\x67\x75\x65\x73\x73\x2C\x20\x72\x65\x6C\x61\x74\x69\x76\x65\x20\x66\x72\x6F\x6D\x20\x74\x68\x65\x20\x64\x65\x76\x69\x63\x65\x20\x70\x6F\x73\x69\x74\x69\x6F\x6E\x2E"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 10.0f, NULL);
	}
}
static void ARLocationConfig_t56B7DB5281EAA81B29A1DD1283C2F793A6B1D5B8_CustomAttributesCacheGenerator_MaxGroundHeight(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x69\x6E\x69\x74\x69\x61\x6C\x20\x67\x72\x6F\x75\x6E\x64\x20\x68\x65\x69\x67\x68\x74\x20\x67\x75\x65\x73\x73\x2C\x20\x72\x65\x6C\x61\x74\x69\x76\x65\x20\x66\x72\x6F\x6D\x20\x74\x68\x65\x20\x64\x65\x76\x69\x63\x65\x20\x70\x6F\x73\x69\x74\x69\x6F\x6E\x2E"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 10.0f, NULL);
	}
}
static void ARLocationConfig_t56B7DB5281EAA81B29A1DD1283C2F793A6B1D5B8_CustomAttributesCacheGenerator_VuforiaGroundHitTestDistance(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x64\x69\x73\x74\x61\x6E\x63\x65\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x56\x75\x66\x6F\x72\x69\x61\x20\x67\x72\x6F\x75\x6E\x64\x20\x70\x6C\x61\x6E\x65\x20\x68\x69\x74\x20\x74\x65\x73\x74\x73\x2E\x20\x4C\x6F\x77\x65\x72\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x6D\x6F\x72\x65\x20\x70\x72\x65\x63\x69\x73\x65\x20\x62\x75\x74\x20\x77\x69\x6C\x6C\x20\x61\x66\x66\x65\x63\x74\x20\x70\x65\x72\x66\x6F\x72\x6D\x61\x6E\x63\x65\x2E"), NULL);
	}
}
static void ARLocationConfig_t56B7DB5281EAA81B29A1DD1283C2F793A6B1D5B8_CustomAttributesCacheGenerator_GroundHeightSmoothingFactor(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x73\x6D\x6F\x6F\x74\x68\x69\x6E\x67\x20\x66\x61\x63\x74\x6F\x72\x20\x66\x6F\x72\x20\x6F\x62\x6A\x65\x63\x74\x20\x68\x65\x69\x67\x68\x74\x20\x61\x64\x6A\x75\x73\x74\x6D\x65\x6E\x74\x73\x2E"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void ARLocationConfig_t56B7DB5281EAA81B29A1DD1283C2F793A6B1D5B8_CustomAttributesCacheGenerator_UseVuforia(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x74\x72\x75\x65\x2C\x20\x75\x73\x65\x20\x56\x75\x66\x6F\x72\x69\x61\x20\x69\x6E\x73\x74\x65\x61\x64\x20\x6F\x66\x20\x41\x52\x46\x6F\x75\x6E\x64\x61\x74\x69\x6F\x6E\x2E"), NULL);
	}
}
static void ARLocationConfig_t56B7DB5281EAA81B29A1DD1283C2F793A6B1D5B8_CustomAttributesCacheGenerator_UseCustomGeoCalculator(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x74\x72\x75\x65\x2C\x20\x67\x65\x6F\x2D\x70\x6F\x73\x69\x74\x69\x6F\x6E\x69\x6E\x67\x20\x63\x61\x6C\x63\x75\x6C\x61\x74\x69\x6F\x6E\x73\x20\x61\x72\x65\x20\x70\x65\x72\x66\x6F\x72\x6D\x65\x64\x20\x62\x79\x20\x63\x61\x6C\x6C\x69\x6E\x64\x20\x61\x20\x75\x73\x65\x72\x20\x64\x65\x66\x69\x6E\x65\x64\x20\x73\x74\x61\x74\x69\x63\x20\x6D\x65\x74\x68\x6F\x64\x2C\x20\x27\x41\x72\x47\x70\x73\x43\x75\x73\x74\x6F\x6D\x47\x65\x6F\x43\x61\x6C\x63\x2E\x48\x6F\x72\x69\x7A\x6F\x6E\x74\x61\x6C\x56\x65\x63\x74\x6F\x72\x46\x72\x6F\x6D\x54\x6F\x28\x4C\x6F\x63\x61\x74\x69\x6F\x6E\x20\x6C\x31\x2C\x20\x4C\x6F\x63\x61\x74\x69\x6F\x6E\x20\x6C\x31\x29\x27\x2E"), NULL);
	}
}
static void LocationData_t1E35051773A207EE9EA92BB2B9B0BEF35BBC927C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x41\x52\x20\x4C\x6F\x63\x61\x74\x69\x6F\x6E\x20\x44\x61\x74\x61"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x41\x52\x2B\x47\x50\x53\x2F\x4C\x6F\x63\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void LocationData_t1E35051773A207EE9EA92BB2B9B0BEF35BBC927C_CustomAttributesCacheGenerator_Location(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x6C\x6F\x63\x61\x74\x69\x6F\x6E"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x67\x65\x6F\x67\x72\x61\x70\x68\x69\x63\x61\x6C\x20\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x73\x20\x74\x68\x61\x74\x20\x74\x68\x65\x20\x70\x61\x74\x68\x20\x77\x69\x6C\x6C\x20\x69\x6E\x74\x65\x72\x70\x6F\x6C\x61\x74\x65\x2E"), NULL);
	}
}
static void LocationPath_t457B71C57B13E32DA60EDEF83BCF0F7350AE83D9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x41\x52\x20\x4C\x6F\x63\x61\x74\x69\x6F\x6E\x20\x50\x61\x74\x68"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x41\x52\x2B\x47\x50\x53\x2F\x50\x61\x74\x68"), NULL);
	}
}
static void LocationPath_t457B71C57B13E32DA60EDEF83BCF0F7350AE83D9_CustomAttributesCacheGenerator_Locations(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x73"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x67\x65\x6F\x67\x72\x61\x70\x68\x69\x63\x61\x6C\x20\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x73\x20\x74\x68\x61\x74\x20\x74\x68\x65\x20\x70\x61\x74\x68\x20\x77\x69\x6C\x6C\x20\x69\x6E\x74\x65\x72\x70\x6F\x6C\x61\x74\x65\x2E"), NULL);
	}
}
static void LocationPath_t457B71C57B13E32DA60EDEF83BCF0F7350AE83D9_CustomAttributesCacheGenerator_SplineType(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x73\x70\x6C\x69\x6E\x65\x54\x79\x70\x65"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x74\x79\x70\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x70\x6C\x69\x6E\x65\x20\x75\x73\x65\x64"), NULL);
	}
}
static void LocationPath_t457B71C57B13E32DA60EDEF83BCF0F7350AE83D9_CustomAttributesCacheGenerator_Alpha(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x61\x6C\x70\x68\x61"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x70\x61\x74\x68\x27\x73\x20\x61\x6C\x70\x68\x61\x2F\x74\x65\x6E\x73\x69\x6F\x6E\x20\x66\x61\x63\x74\x6F\x72\x2E"), NULL);
	}
}
static void LocationPath_t457B71C57B13E32DA60EDEF83BCF0F7350AE83D9_CustomAttributesCacheGenerator_SceneViewScale(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x73\x63\x65\x6E\x65\x56\x69\x65\x77\x53\x63\x61\x6C\x65"), NULL);
	}
}
static void PrefabDatabase_tA214EAA0B86FA31ADBF8614F56217CF7BC2E5714_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x50\x72\x65\x66\x61\x62\x44\x62"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x41\x52\x2B\x47\x50\x53\x2F\x50\x72\x65\x66\x61\x62\x44\x61\x74\x61\x62\x61\x73\x65"), NULL);
	}
}
static void OverpassRequestData_tB94BF30CC59E9C1FB3EB3C3A8BE75D520F3DE872_CustomAttributesCacheGenerator_SouthWest(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x53\x6F\x75\x74\x68\x57\x65\x73\x74\x20\x65\x6E\x64\x20\x6F\x66\x20\x74\x68\x65\x20\x62\x6F\x75\x6E\x64\x69\x6E\x67\x20\x62\x6F\x78\x2E"), NULL);
	}
}
static void OverpassRequestData_tB94BF30CC59E9C1FB3EB3C3A8BE75D520F3DE872_CustomAttributesCacheGenerator_NorthEast(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x4E\x6F\x72\x74\x68\x45\x61\x73\x74\x20\x65\x6E\x64\x20\x6F\x66\x20\x74\x68\x65\x20\x62\x6F\x75\x6E\x64\x69\x6E\x67\x20\x62\x6F\x78\x2E"), NULL);
	}
}
static void OpenStreetMapOptions_tC879CDAF74668DC0F0A00F4B21913196183F6409_CustomAttributesCacheGenerator_OsmXmlFile(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x20\x58\x4D\x4C\x20\x77\x69\x74\x68\x20\x74\x68\x65\x20\x72\x65\x73\x75\x6C\x74\x73\x20\x6F\x66\x20\x61\x20\x4F\x76\x65\x72\x70\x61\x73\x73\x20\x41\x50\x49\x20\x71\x75\x65\x72\x79\x2E\x20\x59\x6F\x75\x20\x63\x61\x6E\x20\x75\x73\x65\x20\x68\x74\x74\x70\x3A\x2F\x2F\x6F\x76\x65\x72\x70\x61\x73\x73\x2D\x74\x75\x72\x62\x6F\x2E\x65\x75\x2F\x20\x74\x6F\x20\x67\x65\x6E\x65\x72\x61\x74\x65\x20\x6F\x6E\x65\x2E"), NULL);
	}
}
static void OpenStreetMapOptions_tC879CDAF74668DC0F0A00F4B21913196183F6409_CustomAttributesCacheGenerator_FetchFromOverpassApi(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x74\x72\x75\x65\x2C\x20\x69\x6E\x73\x74\x65\x61\x64\x20\x6F\x66\x20\x74\x68\x65\x20\x58\x4D\x4C\x20\x66\x69\x6C\x65\x20\x61\x62\x6F\x76\x65\x2C\x20\x77\x65\x20\x66\x65\x74\x63\x68\x20\x74\x68\x65\x20\x64\x61\x74\x61\x20\x64\x69\x72\x65\x63\x74\x6C\x79\x20\x66\x72\x6F\x6D\x20\x61\x20\x4F\x76\x65\x72\x70\x61\x73\x73\x20\x41\x50\x49\x20\x72\x65\x71\x75\x65\x73\x74\x20\x74\x6F\x20\x68\x74\x74\x70\x73\x3A\x2F\x2F\x77\x77\x77\x2E\x6F\x76\x65\x72\x70\x61\x73\x73\x2D\x61\x70\x69\x2E\x64\x65\x2F\x61\x70\x69\x2F\x69\x6E\x74\x65\x72\x70\x72\x65\x74\x65\x72\x2E"), NULL);
	}
}
static void OpenStreetMapOptions_tC879CDAF74668DC0F0A00F4B21913196183F6409_CustomAttributesCacheGenerator_overPassRequestData(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x64\x61\x74\x61\x20\x63\x6F\x6E\x66\x69\x67\x75\x72\x61\x74\x69\x6F\x6E\x20\x75\x73\x65\x64\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x4F\x76\x65\x72\x70\x61\x73\x73\x20\x41\x50\x49\x20\x72\x65\x71\x75\x65\x73\x74\x2E\x20\x42\x61\x73\x69\x63\x61\x6C\x6C\x79\x20\x61\x20\x62\x6F\x75\x6E\x64\x69\x6E\x67\x20\x62\x6F\x78\x20\x72\x65\x63\x74\x61\x6E\x67\x6C\x65\x20\x64\x65\x66\x69\x6E\x65\x64\x20\x62\x79\x20\x74\x77\x6F\x20\x70\x6F\x69\x6E\x74\x73\x2E"), NULL);
	}
}
static void CreatePointOfInterestTextMeshes_t4893F19D46EE04CF68EBBC3DAD7196E87EF18E28_CustomAttributesCacheGenerator_height(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x68\x65\x69\x67\x68\x74\x20\x6F\x66\x20\x74\x68\x65\x20\x74\x65\x78\x74\x20\x6D\x65\x73\x68\x2C\x20\x72\x65\x6C\x61\x74\x69\x76\x65\x20\x74\x6F\x20\x74\x68\x65\x20\x64\x65\x76\x69\x63\x65\x2E"), NULL);
	}
}
static void CreatePointOfInterestTextMeshes_t4893F19D46EE04CF68EBBC3DAD7196E87EF18E28_CustomAttributesCacheGenerator_textPrefab(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x54\x65\x78\x74\x4D\x65\x73\x68\x20\x70\x72\x65\x66\x61\x62\x2E"), NULL);
	}
}
static void CreatePointOfInterestTextMeshes_t4893F19D46EE04CF68EBBC3DAD7196E87EF18E28_CustomAttributesCacheGenerator_movementSmoothingFactor(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x73\x6D\x6F\x6F\x74\x68\x69\x6E\x67\x20\x66\x61\x63\x74\x6F\x72\x20\x66\x6F\x72\x20\x6D\x6F\x76\x65\x6D\x65\x6E\x74\x20\x64\x75\x65\x20\x74\x6F\x20\x47\x50\x53\x20\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x20\x61\x64\x6A\x75\x73\x74\x6D\x65\x6E\x74\x73\x3B\x20\x69\x66\x20\x73\x65\x74\x20\x74\x6F\x20\x7A\x65\x72\x6F\x20\x69\x74\x20\x69\x73\x20\x64\x69\x73\x61\x62\x6C\x65\x64\x2E"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 500.0f, NULL);
	}
}
static void CreatePointOfInterestTextMeshes_t4893F19D46EE04CF68EBBC3DAD7196E87EF18E28_CustomAttributesCacheGenerator_locations(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x6F\x63\x61\x74\x69\x6F\x6E\x73\x20\x77\x68\x65\x72\x65\x20\x74\x68\x65\x20\x74\x65\x78\x74\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x64\x69\x73\x70\x6C\x61\x79\x65\x64\x2E\x20\x54\x68\x65\x20\x74\x65\x78\x74\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x74\x68\x65\x20\x4C\x61\x62\x65\x6C\x20\x70\x72\x6F\x70\x65\x72\x74\x79\x2E\x20\x28\x4F\x70\x74\x69\x6F\x6E\x61\x6C\x29"), NULL);
	}
}
static void CreatePointOfInterestTextMeshes_t4893F19D46EE04CF68EBBC3DAD7196E87EF18E28_CustomAttributesCacheGenerator_openStreetMapOptions(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x20\x74\x68\x69\x73\x20\x74\x6F\x20\x65\x69\x74\x68\x65\x72\x20\x66\x65\x74\x63\x68\x20\x4F\x70\x65\x6E\x53\x74\x72\x65\x65\x74\x4D\x61\x70\x20\x64\x61\x74\x61\x20\x66\x72\x6F\x6D\x20\x61\x20\x4F\x76\x65\x72\x70\x61\x73\x73\x20\x41\x50\x49\x20\x72\x65\x71\x75\x65\x73\x74\x2C\x20\x6F\x72\x20\x76\x69\x61\x20\x61\x20\x6C\x6F\x63\x61\x6C\x6C\x79\x20\x73\x74\x6F\x72\x65\x64\x20\x58\x4D\x4C\x20\x66\x69\x6C\x65\x2E\x20\x28\x4F\x70\x74\x69\x6F\x6E\x61\x6C\x29"), NULL);
	}
}
static void CreatePointOfInterestTextMeshes_t4893F19D46EE04CF68EBBC3DAD7196E87EF18E28_CustomAttributesCacheGenerator_CreatePointOfInterestTextMeshes_LoadXMLFileFromOverpassRequest_m6DBAB5D5E1EA7722AE5F7B668AEB88943EF47A38(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadXMLFileFromOverpassRequestU3Ed__10_tAE0DCE7DD96AAA881075718C9F601829ECD843F6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CLoadXMLFileFromOverpassRequestU3Ed__10_tAE0DCE7DD96AAA881075718C9F601829ECD843F6_0_0_0_var), NULL);
	}
}
static void U3CLoadXMLFileFromOverpassRequestU3Ed__10_tAE0DCE7DD96AAA881075718C9F601829ECD843F6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadXMLFileFromOverpassRequestU3Ed__10_tAE0DCE7DD96AAA881075718C9F601829ECD843F6_CustomAttributesCacheGenerator_U3CLoadXMLFileFromOverpassRequestU3Ed__10__ctor_mAF6B905BE3065ED6BE60424CF57F777239A8570F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadXMLFileFromOverpassRequestU3Ed__10_tAE0DCE7DD96AAA881075718C9F601829ECD843F6_CustomAttributesCacheGenerator_U3CLoadXMLFileFromOverpassRequestU3Ed__10_System_IDisposable_Dispose_mF85AAF3EF5980C11CEDBF4B2A28B569558FB2E28(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadXMLFileFromOverpassRequestU3Ed__10_tAE0DCE7DD96AAA881075718C9F601829ECD843F6_CustomAttributesCacheGenerator_U3CLoadXMLFileFromOverpassRequestU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3A34CE79C2E009E8F0B3BD3C52CD02973ADAFAB0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadXMLFileFromOverpassRequestU3Ed__10_tAE0DCE7DD96AAA881075718C9F601829ECD843F6_CustomAttributesCacheGenerator_U3CLoadXMLFileFromOverpassRequestU3Ed__10_System_Collections_IEnumerator_Reset_mB869557E697089B355822B3AF8682D6116001D3F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadXMLFileFromOverpassRequestU3Ed__10_tAE0DCE7DD96AAA881075718C9F601829ECD843F6_CustomAttributesCacheGenerator_U3CLoadXMLFileFromOverpassRequestU3Ed__10_System_Collections_IEnumerator_get_Current_mDACA87BB7A9DC4BC6F0A85CE2240F0B42758F4A6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void DevCameraController_tBF345CCD78C34223573FB45F279083FD82C221C4_CustomAttributesCacheGenerator_Speed(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x73\x70\x65\x65\x64"), NULL);
	}
}
static void FadeOutTextMesh_tC2451DD0C4D6BE981FA439AAF6494404DF9FA1C4_CustomAttributesCacheGenerator_Duration(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x64\x75\x72\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void FadeOutTextMesh_tC2451DD0C4D6BE981FA439AAF6494404DF9FA1C4_CustomAttributesCacheGenerator_FadeOutTextMesh_FadeOut_m268B3329732D0A10EEADADD15A20627EE1CC5A1F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CFadeOutU3Ed__3_t63A1FAFDE1FFC2013D402424D2B6DDE3897E7BD7_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CFadeOutU3Ed__3_t63A1FAFDE1FFC2013D402424D2B6DDE3897E7BD7_0_0_0_var), NULL);
	}
}
static void U3CFadeOutU3Ed__3_t63A1FAFDE1FFC2013D402424D2B6DDE3897E7BD7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CFadeOutU3Ed__3_t63A1FAFDE1FFC2013D402424D2B6DDE3897E7BD7_CustomAttributesCacheGenerator_U3CFadeOutU3Ed__3__ctor_m21132BB33E8FAA9FBBDD345C4F279C62855F8662(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFadeOutU3Ed__3_t63A1FAFDE1FFC2013D402424D2B6DDE3897E7BD7_CustomAttributesCacheGenerator_U3CFadeOutU3Ed__3_System_IDisposable_Dispose_m424B074A864C2B145D864A64FFE15166BB394E03(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFadeOutU3Ed__3_t63A1FAFDE1FFC2013D402424D2B6DDE3897E7BD7_CustomAttributesCacheGenerator_U3CFadeOutU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB2BAF570D34EE60A4C29EE183BE2B509CFC3B4BA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFadeOutU3Ed__3_t63A1FAFDE1FFC2013D402424D2B6DDE3897E7BD7_CustomAttributesCacheGenerator_U3CFadeOutU3Ed__3_System_Collections_IEnumerator_Reset_mC7AAFD31B0EFD2F4BCF022A723026BCE0B65CF87(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFadeOutU3Ed__3_t63A1FAFDE1FFC2013D402424D2B6DDE3897E7BD7_CustomAttributesCacheGenerator_U3CFadeOutU3Ed__3_System_Collections_IEnumerator_get_Current_m7370CA01F921987DC870E2AD0F9F458E3D0155BF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void SmoothMove_t99450B9A6922B54E5255BDC49A305A2F81864644_CustomAttributesCacheGenerator_Epsilon(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x73\x6D\x6F\x6F\x74\x68\x69\x6E\x67\x20\x66\x61\x63\x74\x6F\x72\x2E"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void SmoothMove_t99450B9A6922B54E5255BDC49A305A2F81864644_CustomAttributesCacheGenerator_Precision(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x50\x72\x65\x63\x69\x73\x69\x6F\x6E\x2E"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 0.100000001f, NULL);
	}
}
static void SmoothMove_t99450B9A6922B54E5255BDC49A305A2F81864644_CustomAttributesCacheGenerator_SmoothMoveMode(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x6D\x6F\x64\x65\x2E\x20\x49\x66\x20\x73\x65\x74\x20\x74\x6F\x20\x27\x48\x6F\x72\x69\x7A\x6F\x6E\x74\x61\x6C\x27\x2C\x20\x77\x69\x6C\x6C\x20\x6C\x65\x61\x76\x65\x20\x74\x68\x65\x20\x79\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x75\x6E\x63\x68\x61\x6E\x67\x65\x64\x2E\x20\x46\x75\x6C\x6C\x20\x6D\x65\x61\x6E\x73\x20\x74\x68\x65\x20\x6F\x62\x6A\x65\x63\x74\x20\x77\x69\x6C\x6C\x20\x6D\x6F\x76\x65\x20\x69\x6E\x20\x61\x6C\x6C\x20\x33\x44\x20\x63\x6F\x6F\x72\x64\x69\x6E\x61\x74\x65\x73\x2E"), NULL);
	}
}
static void SmoothMove_t99450B9A6922B54E5255BDC49A305A2F81864644_CustomAttributesCacheGenerator_SmoothMove_MoveTo_mEAF9AC05F02013D4FC48A465D057A7A44BD1E0A4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CMoveToU3Ed__11_t39BD7A569BA53E9B5CD4627EBFD3C0B37147628F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CMoveToU3Ed__11_t39BD7A569BA53E9B5CD4627EBFD3C0B37147628F_0_0_0_var), NULL);
	}
}
static void U3CMoveToU3Ed__11_t39BD7A569BA53E9B5CD4627EBFD3C0B37147628F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CMoveToU3Ed__11_t39BD7A569BA53E9B5CD4627EBFD3C0B37147628F_CustomAttributesCacheGenerator_U3CMoveToU3Ed__11__ctor_m887B7A1814EA9E6BE59A7E3F5D1A706B9D7364B3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CMoveToU3Ed__11_t39BD7A569BA53E9B5CD4627EBFD3C0B37147628F_CustomAttributesCacheGenerator_U3CMoveToU3Ed__11_System_IDisposable_Dispose_mCD211B0ECC4D98BC7F6C702BA7DFB58BBB84D065(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CMoveToU3Ed__11_t39BD7A569BA53E9B5CD4627EBFD3C0B37147628F_CustomAttributesCacheGenerator_U3CMoveToU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m01E37D522523EC1BE28EFFD441C6D3371C9E84B5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CMoveToU3Ed__11_t39BD7A569BA53E9B5CD4627EBFD3C0B37147628F_CustomAttributesCacheGenerator_U3CMoveToU3Ed__11_System_Collections_IEnumerator_Reset_m6EE3D9D660CB5F3557551A17CD9CD2E96E093A6E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CMoveToU3Ed__11_t39BD7A569BA53E9B5CD4627EBFD3C0B37147628F_CustomAttributesCacheGenerator_U3CMoveToU3Ed__11_System_Collections_IEnumerator_get_Current_m968A9BF3B0381A2871C701432AA26FC17547AE91(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void DebugInfoOverlay_tCBE3576E5308FD421CF002A4F457FC5D6A3D1BEF_CustomAttributesCacheGenerator_Show(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x73\x68\x6F\x77"), NULL);
	}
}
static void DebugInfoOverlay_tCBE3576E5308FD421CF002A4F457FC5D6A3D1BEF_CustomAttributesCacheGenerator_ShowObjectInfo(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x73\x68\x6F\x77\x4F\x62\x6A\x65\x63\x74\x49\x6E\x66\x6F"), NULL);
	}
}
static void LoadingBar_tB573C0002332A4C1E969B01F1532E2261DA385BA_CustomAttributesCacheGenerator_FillPercentage(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x66\x69\x6C\x6C\x50\x65\x72\x63\x65\x6E\x74\x61\x67\x65"), NULL);
	}
}
static void LoadingBar_tB573C0002332A4C1E969B01F1532E2261DA385BA_CustomAttributesCacheGenerator_StartColor(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x73\x74\x61\x72\x74\x43\x6F\x6C\x6F\x72"), NULL);
	}
}
static void LoadingBar_tB573C0002332A4C1E969B01F1532E2261DA385BA_CustomAttributesCacheGenerator_MiddleColor(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x6D\x69\x64\x64\x6C\x65\x43\x6F\x6C\x6F\x72"), NULL);
	}
}
static void LoadingBar_tB573C0002332A4C1E969B01F1532E2261DA385BA_CustomAttributesCacheGenerator_EndColor(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x65\x6E\x64\x43\x6F\x6C\x6F\x72"), NULL);
	}
}
static void LoadingBar_tB573C0002332A4C1E969B01F1532E2261DA385BA_CustomAttributesCacheGenerator_TextColor(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x74\x65\x78\x74\x43\x6F\x6C\x6F\x72"), NULL);
	}
}
static void LoadingBar_tB573C0002332A4C1E969B01F1532E2261DA385BA_CustomAttributesCacheGenerator_UsePercentageText(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x75\x73\x65\x50\x65\x72\x63\x65\x6E\x74\x61\x67\x65\x54\x65\x78\x74"), NULL);
	}
}
static void LoadingBar_tB573C0002332A4C1E969B01F1532E2261DA385BA_CustomAttributesCacheGenerator_Text(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x74\x65\x78\x74"), NULL);
	}
}
static void ARFoundationSessionManager_t51DA8ECC9055C8565CCDE4A970EBFAE9F83B84E0_CustomAttributesCacheGenerator_U3CDebugModeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ARFoundationSessionManager_t51DA8ECC9055C8565CCDE4A970EBFAE9F83B84E0_CustomAttributesCacheGenerator_ARFoundationSessionManager_get_DebugMode_m71C4447004798139D1AC193D1D75EBC93047CC4E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ARFoundationSessionManager_t51DA8ECC9055C8565CCDE4A970EBFAE9F83B84E0_CustomAttributesCacheGenerator_ARFoundationSessionManager_set_DebugMode_m5EC9658AE686227451186F4704D6F1A80E8B0499(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ParticleSystemController_t1D86FB558689533EF955720E10734884EF5AAC6A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[511] = 
{
	U3CStartFaceInU3Ed__8_t5FE7CDF6AF05E40079784B4E2ACFD51D58837AA0_CustomAttributesCacheGenerator,
	U3CStartFadeOutU3Ed__9_tEA7B6452D46BF558C6DC7A2365C52EC47E8A199F_CustomAttributesCacheGenerator,
	U3CLoadSceneProcessU3Ed__10_tCEEEECB67F643ED087AAAB846276DF898F600AB1_CustomAttributesCacheGenerator,
	U3CStartInitU3Ed__18_t701C9769307CB8A0DACFE4676F2E948FE47FB66A_CustomAttributesCacheGenerator,
	U3CSpawnDelayManagerU3Ed__9_tBD67D61B15F4EBCCAE1FD83AE18597F0882C5AE0_CustomAttributesCacheGenerator,
	U3CStartDissolveU3Ed__18_t696122E2D0CFB06601A00FF2F088544EFCF6A760_CustomAttributesCacheGenerator,
	LaserAttack_t3D280ADBBA2C6DFD7F8B5D9EC9B3B44B5A4DAE91_CustomAttributesCacheGenerator,
	U3CStartLaserShootU3Ed__11_t31E05FDA8294BBDFDE83519C5A8FE302183C7FFF_CustomAttributesCacheGenerator,
	U3CFillProcessU3Ed__3_t16C38ACC665B308ED772CE1D8D3D8590403963AD_CustomAttributesCacheGenerator,
	U3CAutoDestroyU3Ed__4_t28203B1A36A0C012905840F27C0277804C0033B0_CustomAttributesCacheGenerator,
	U3CShakeU3Ed__5_t4D78750F530E974ECA1E718C81F0AC4A03284EB6_CustomAttributesCacheGenerator,
	U3CDestroyParticleU3Ed__20_t524359CC15BFCBE7B00549E3EE37DFDA5E23832D_CustomAttributesCacheGenerator,
	U3CUpdateRayU3Ed__9_tAF42C0C52B4ACFDCDC70E2B1A1F726AA6756614C_CustomAttributesCacheGenerator,
	ARSessionStateExtensions_tF98CE3C37471E7FDFDA1B89FD81C123E7D62E789_CustomAttributesCacheGenerator,
	ARFeatheredPlaneMeshVisualizer_t7B2E22615047B9FDFC4D5015F59BFE2A17E3140E_CustomAttributesCacheGenerator,
	AnchorCreator_t97C108ACEDFC02ECBB6B4923EB6576F99050773C_CustomAttributesCacheGenerator,
	U3CProgrammedDeathU3Ed__3_t66AB9997CF14E4C726CFF62F618BBFC4DABC3C14_CustomAttributesCacheGenerator,
	DemoButton_t137F4A23D860DA55E2C4A5505168231367D2FF2C_CustomAttributesCacheGenerator,
	U3CStartU3Ed__11_t923C741C5866E5117C2111EE5A4180E589AB1FB9_CustomAttributesCacheGenerator,
	U3CWaitForLocationServicesU3Ed__12_t0CFACBF6935FDEC52C2AC8482C8A969EB746EB2C_CustomAttributesCacheGenerator,
	U3CU3Ec_t6560C12F9DDDBA0BE759D1E97EC142B084520DE1_CustomAttributesCacheGenerator,
	WorldBuilderApplicationController_tFE48840B225954BFB9782D9799DAFF218C391228_CustomAttributesCacheGenerator,
	U3CStartU3Ed__13_tB5AD52B13DBA13A7D2D366C50585F21D7A9A8937_CustomAttributesCacheGenerator,
	U3CRestoreWorldFromServerU3Ed__14_t1BC195D258F6C9A677CFE33012A10FED77E61B13_CustomAttributesCacheGenerator,
	U3CSaveWorldToServerU3Ed__15_tFDB6D1F6FE0DEE26967CAF125CED4C9F5FDA9C0B_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass23_0_t37D6D155705469CD530CA3E37BA55CC6F01E0AF2_CustomAttributesCacheGenerator,
	U3CStartWorldU3Ed__19_t750B4DEF83B7D2F7C41FA78DDB0224ED46CDDA47_CustomAttributesCacheGenerator,
	U3CStartU3Ed__20_tBC91C2BA3682FCE4567CEAB39669F7C789CF1525_CustomAttributesCacheGenerator,
	U3CWaitForLocationServicesU3Ed__25_tB371C83EDE76677BE0191100440D26A065116399_CustomAttributesCacheGenerator,
	ARLocationManager_t224AB8AD4D95A0921BA4A44A7ED5C39729FBC2FF_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass28_0_tA71036D989189B71D0132456123D35395E375F18_CustomAttributesCacheGenerator,
	ARLocationOrientation_tD8A832D47ECB471D892449B5ADB4143078E1C8B7_CustomAttributesCacheGenerator,
	ARLocationProvider_t47C5F7E2A5840C8C1056AA5CCEB31F2517BF4605_CustomAttributesCacheGenerator,
	U3CStartU3Ed__50_tE495468A4B52BE5F74CE7E3FFBEDAB0D1C021E74_CustomAttributesCacheGenerator,
	GroundHeight_t4265E0D341906907B659299C70E5C1D1C74A7E3F_CustomAttributesCacheGenerator,
	Hotspot_t94B43358E06B6906BE49D9AD465A62C33E50C6DE_CustomAttributesCacheGenerator,
	MoveAlongPath_t8123BE990CF5AD253811E75F1D2B67697084A372_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass34_0_tE995B7736DECF544630BFC7962E5608C5E1B2530_CustomAttributesCacheGenerator,
	PlaceAlongPath_t0F05120A71DC49EFA8C7D544DFB7B92C878BAB52_CustomAttributesCacheGenerator,
	PlaceAtLocation_t8F470A16D7F7E3A37FEB114A3879DD5136EE24BF_CustomAttributesCacheGenerator,
	PlaceAtLocations_t8C777F409D11DA3480CDE0F8723857FAA7A4CB3C_CustomAttributesCacheGenerator,
	RenderPathLine_t3873DC0DE3A6052CE288EAA763EDFDB34FBED02A_CustomAttributesCacheGenerator,
	U3CStartU3Ed__89_tA04EB5BAC5E34AFA5EF0368C59EA9782780DF767_CustomAttributesCacheGenerator,
	U3CU3Ec_t82EC83268F7F39B4AD49369938686D006EE9FA7B_CustomAttributesCacheGenerator,
	ARLocationConfig_t56B7DB5281EAA81B29A1DD1283C2F793A6B1D5B8_CustomAttributesCacheGenerator,
	LocationData_t1E35051773A207EE9EA92BB2B9B0BEF35BBC927C_CustomAttributesCacheGenerator,
	LocationPath_t457B71C57B13E32DA60EDEF83BCF0F7350AE83D9_CustomAttributesCacheGenerator,
	PrefabDatabase_tA214EAA0B86FA31ADBF8614F56217CF7BC2E5714_CustomAttributesCacheGenerator,
	U3CLoadXMLFileFromOverpassRequestU3Ed__10_tAE0DCE7DD96AAA881075718C9F601829ECD843F6_CustomAttributesCacheGenerator,
	U3CFadeOutU3Ed__3_t63A1FAFDE1FFC2013D402424D2B6DDE3897E7BD7_CustomAttributesCacheGenerator,
	U3CMoveToU3Ed__11_t39BD7A569BA53E9B5CD4627EBFD3C0B37147628F_CustomAttributesCacheGenerator,
	ParticleSystemController_t1D86FB558689533EF955720E10734884EF5AAC6A_CustomAttributesCacheGenerator,
	MonsterSearchGuide_t025127A275D8313DF60BB06111D6C87DB4B5A45A_CustomAttributesCacheGenerator_monsterSpawner,
	GameStartLevelController_t3CFD56CCADF2433004AE1FA5A8A5671E88CD2E24_CustomAttributesCacheGenerator_bgm,
	HitEffectManager_tB12BA6F3DD666F29762CD90844BA038F3072A639_CustomAttributesCacheGenerator_hitEffects,
	AudioController_tA9F9625D78D7B82E16202C1EBD62C8CA84CE7B5E_CustomAttributesCacheGenerator_audioSource,
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_fadeInImage,
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_fadeInTermTime,
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_audioController,
	LevelManager_t010B312A2B35B45291F58195216ABB5673174961_CustomAttributesCacheGenerator_monsterSpawner,
	LevelManager_t010B312A2B35B45291F58195216ABB5673174961_CustomAttributesCacheGenerator_gameTime,
	LevelManager_t010B312A2B35B45291F58195216ABB5673174961_CustomAttributesCacheGenerator_colors,
	LevelManager_t010B312A2B35B45291F58195216ABB5673174961_CustomAttributesCacheGenerator_ui_gamePlayRemainTimeText,
	LevelManager_t010B312A2B35B45291F58195216ABB5673174961_CustomAttributesCacheGenerator_ui_scoreText,
	LevelManager_t010B312A2B35B45291F58195216ABB5673174961_CustomAttributesCacheGenerator_colorChangeButtonImage,
	LevelManager_t010B312A2B35B45291F58195216ABB5673174961_CustomAttributesCacheGenerator_gameOverUI,
	MonsterSpawner_t38B0F58A8A9657860995DDF676E764E0DEC57B4D_CustomAttributesCacheGenerator_spawnObjects,
	MonsterSpawner_t38B0F58A8A9657860995DDF676E764E0DEC57B4D_CustomAttributesCacheGenerator_spawnDelay,
	MonsterSpawner_t38B0F58A8A9657860995DDF676E764E0DEC57B4D_CustomAttributesCacheGenerator_spawnZone,
	MonsterSpawner_t38B0F58A8A9657860995DDF676E764E0DEC57B4D_CustomAttributesCacheGenerator_U3CSpawnedMonstersU3Ek__BackingField,
	AnimationSettings_t3D14F6560132A34377F0519400B1FB1C606BFBE6_CustomAttributesCacheGenerator_dieAnimationTag,
	AnimationSettings_t3D14F6560132A34377F0519400B1FB1C606BFBE6_CustomAttributesCacheGenerator_hitAnimationTag,
	MonsterBase_t2CD611A20C74AAF093EE6B851712E7FFD2052E29_CustomAttributesCacheGenerator_placeAtLocation,
	MonsterBase_t2CD611A20C74AAF093EE6B851712E7FFD2052E29_CustomAttributesCacheGenerator_spawnArea,
	MonsterBase_t2CD611A20C74AAF093EE6B851712E7FFD2052E29_CustomAttributesCacheGenerator_hp,
	MonsterBase_t2CD611A20C74AAF093EE6B851712E7FFD2052E29_CustomAttributesCacheGenerator_colorType,
	MonsterBase_t2CD611A20C74AAF093EE6B851712E7FFD2052E29_CustomAttributesCacheGenerator_score,
	MonsterBase_t2CD611A20C74AAF093EE6B851712E7FFD2052E29_CustomAttributesCacheGenerator_animator,
	MonsterBase_t2CD611A20C74AAF093EE6B851712E7FFD2052E29_CustomAttributesCacheGenerator_animationSettings,
	MonsterBase_t2CD611A20C74AAF093EE6B851712E7FFD2052E29_CustomAttributesCacheGenerator_dieSFX,
	MonsterBase_t2CD611A20C74AAF093EE6B851712E7FFD2052E29_CustomAttributesCacheGenerator_dieVFX,
	LaserAttack_t3D280ADBBA2C6DFD7F8B5D9EC9B3B44B5A4DAE91_CustomAttributesCacheGenerator_damage,
	LaserAttack_t3D280ADBBA2C6DFD7F8B5D9EC9B3B44B5A4DAE91_CustomAttributesCacheGenerator_shootDelay,
	LaserAttack_t3D280ADBBA2C6DFD7F8B5D9EC9B3B44B5A4DAE91_CustomAttributesCacheGenerator_laserBeam,
	LaserAttack_t3D280ADBBA2C6DFD7F8B5D9EC9B3B44B5A4DAE91_CustomAttributesCacheGenerator_remainGauge,
	LaserAttack_t3D280ADBBA2C6DFD7F8B5D9EC9B3B44B5A4DAE91_CustomAttributesCacheGenerator_uiCrossHair,
	LaserAttack_t3D280ADBBA2C6DFD7F8B5D9EC9B3B44B5A4DAE91_CustomAttributesCacheGenerator_fireSFX,
	LaserBeam_tFCC429A545E7674E750953FA65C49D7875347FF8_CustomAttributesCacheGenerator_laserMaxDistance,
	DontDestroySingleton_1_tF85C94A33AEC78EC5129B6356600771BA9FB3508_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField,
	SingletonMonoBehaviour_1_t13D4D87A01B2D33C83A7C1C9BD55C27324B9A877_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField,
	UI_CrossHair_tD67FA151EE8A866801215A40CBAAA389049CB4AB_CustomAttributesCacheGenerator_animator,
	UI_CrossHair_tD67FA151EE8A866801215A40CBAAA389049CB4AB_CustomAttributesCacheGenerator_fireTag,
	UI_GameOver_tA9DB42DA7F622B778079C6A0344AFA05F976554A_CustomAttributesCacheGenerator_mainMenuSceneName,
	UI_GameOver_tA9DB42DA7F622B778079C6A0344AFA05F976554A_CustomAttributesCacheGenerator_gameSceneName,
	UI_GameOver_tA9DB42DA7F622B778079C6A0344AFA05F976554A_CustomAttributesCacheGenerator_finalScoreText,
	UI_GameStart_t0FE28BA7187B7BDEEC30ED88A6E09157DE0BCB04_CustomAttributesCacheGenerator_gameSceneName,
	UI_RemainGauge_t7496C063310AB0F2BBB293AF8B0C4DF905F98BB7_CustomAttributesCacheGenerator_fillImage,
	EffectController_tF93D81123371E3D7BEB00D86028212D6B180D99D_CustomAttributesCacheGenerator_particle,
	EffectController_tF93D81123371E3D7BEB00D86028212D6B180D99D_CustomAttributesCacheGenerator_isDestroyable,
	_TEST_Laser_t363B81B7173489F49DDBB73EC2D2A04CF947045A_CustomAttributesCacheGenerator_laserAttack,
	ProjectileMoveScript_t8E5328E659143159EA6BDE95FD65EC7A674B237E_CustomAttributesCacheGenerator_accuracy,
	ARFeatheredPlaneMeshVisualizer_t7B2E22615047B9FDFC4D5015F59BFE2A17E3140E_CustomAttributesCacheGenerator_m_FeatheringWidth,
	AnchorCreator_t97C108ACEDFC02ECBB6B4923EB6576F99050773C_CustomAttributesCacheGenerator_m_AnchorPrefab,
	PanelControlNeon_t38394C83EE1A0D50CF165CB0FA261C243ACA2461_CustomAttributesCacheGenerator_panels,
	PanelControlNeon_t38394C83EE1A0D50CF165CB0FA261C243ACA2461_CustomAttributesCacheGenerator_panelTransform,
	PanelControlNeon_t38394C83EE1A0D50CF165CB0FA261C243ACA2461_CustomAttributesCacheGenerator_buttonPrev,
	PanelControlNeon_t38394C83EE1A0D50CF165CB0FA261C243ACA2461_CustomAttributesCacheGenerator_buttonNext,
	PanelNeon_t24F392CF6472DBA84E376C555D546B5913E5D21B_CustomAttributesCacheGenerator_otherPanels,
	DemoButton_t137F4A23D860DA55E2C4A5505168231367D2FF2C_CustomAttributesCacheGenerator_NotSupportedInWebGL,
	DemoButton_t137F4A23D860DA55E2C4A5505168231367D2FF2C_CustomAttributesCacheGenerator_TargetButton,
	DemoPackageTester_t0907E9A97CEA5E17DFB6E5E5670F5D1E60D00DD5_CustomAttributesCacheGenerator_RequiresPostProcessing,
	ARLocationManager_t224AB8AD4D95A0921BA4A44A7ED5C39729FBC2FF_CustomAttributesCacheGenerator_Camera,
	ARLocationManager_t224AB8AD4D95A0921BA4A44A7ED5C39729FBC2FF_CustomAttributesCacheGenerator_WaitForARTrackingToStart,
	ARLocationManager_t224AB8AD4D95A0921BA4A44A7ED5C39729FBC2FF_CustomAttributesCacheGenerator_RestartWhenARTrackingIsRestored,
	ARLocationManager_t224AB8AD4D95A0921BA4A44A7ED5C39729FBC2FF_CustomAttributesCacheGenerator_SetTargetFrameRateTo60Mhz,
	ARLocationManager_t224AB8AD4D95A0921BA4A44A7ED5C39729FBC2FF_CustomAttributesCacheGenerator_DebugMode,
	ARLocationManager_t224AB8AD4D95A0921BA4A44A7ED5C39729FBC2FF_CustomAttributesCacheGenerator_OnTrackingStarted,
	ARLocationManager_t224AB8AD4D95A0921BA4A44A7ED5C39729FBC2FF_CustomAttributesCacheGenerator_U3CSessionManagerU3Ek__BackingField,
	ARLocationManager_t224AB8AD4D95A0921BA4A44A7ED5C39729FBC2FF_CustomAttributesCacheGenerator_U3CMainCameraU3Ek__BackingField,
	ARLocationOrientation_tD8A832D47ECB471D892449B5ADB4143078E1C8B7_CustomAttributesCacheGenerator_MaxNumberOfUpdates,
	ARLocationOrientation_tD8A832D47ECB471D892449B5ADB4143078E1C8B7_CustomAttributesCacheGenerator_AverageCount,
	ARLocationOrientation_tD8A832D47ECB471D892449B5ADB4143078E1C8B7_CustomAttributesCacheGenerator_UseRawUntilFirstAverage,
	ARLocationOrientation_tD8A832D47ECB471D892449B5ADB4143078E1C8B7_CustomAttributesCacheGenerator_MovementSmoothingFactor,
	ARLocationOrientation_tD8A832D47ECB471D892449B5ADB4143078E1C8B7_CustomAttributesCacheGenerator_TrueNorthOffset,
	ARLocationOrientation_tD8A832D47ECB471D892449B5ADB4143078E1C8B7_CustomAttributesCacheGenerator_ApplyCompassTiltCompensationOnAndroid,
	ARLocationOrientation_tD8A832D47ECB471D892449B5ADB4143078E1C8B7_CustomAttributesCacheGenerator_LowPassFilterFactor,
	ARLocationOrientation_tD8A832D47ECB471D892449B5ADB4143078E1C8B7_CustomAttributesCacheGenerator_OnOrientationUpdated,
	ARLocationOrientation_tD8A832D47ECB471D892449B5ADB4143078E1C8B7_CustomAttributesCacheGenerator_OnBeforeOrientationUpdated,
	ARLocationProvider_t47C5F7E2A5840C8C1056AA5CCEB31F2517BF4605_CustomAttributesCacheGenerator_LocationProviderSettings,
	ARLocationProvider_t47C5F7E2A5840C8C1056AA5CCEB31F2517BF4605_CustomAttributesCacheGenerator_MockLocationData,
	ARLocationProvider_t47C5F7E2A5840C8C1056AA5CCEB31F2517BF4605_CustomAttributesCacheGenerator_MaxWaitTime,
	ARLocationProvider_t47C5F7E2A5840C8C1056AA5CCEB31F2517BF4605_CustomAttributesCacheGenerator_StartUpDelay,
	ARLocationProvider_t47C5F7E2A5840C8C1056AA5CCEB31F2517BF4605_CustomAttributesCacheGenerator_DebugMode,
	ARLocationProvider_t47C5F7E2A5840C8C1056AA5CCEB31F2517BF4605_CustomAttributesCacheGenerator_OnEnabled,
	ARLocationProvider_t47C5F7E2A5840C8C1056AA5CCEB31F2517BF4605_CustomAttributesCacheGenerator_OnLocationUpdated,
	ARLocationProvider_t47C5F7E2A5840C8C1056AA5CCEB31F2517BF4605_CustomAttributesCacheGenerator_OnRawLocationUpdated,
	ARLocationProvider_t47C5F7E2A5840C8C1056AA5CCEB31F2517BF4605_CustomAttributesCacheGenerator_OnCompassUpdated,
	ARLocationProvider_t47C5F7E2A5840C8C1056AA5CCEB31F2517BF4605_CustomAttributesCacheGenerator_U3CProviderU3Ek__BackingField,
	ARLocationProvider_t47C5F7E2A5840C8C1056AA5CCEB31F2517BF4605_CustomAttributesCacheGenerator_OnLocationUpdatedDelegate,
	ARLocationProvider_t47C5F7E2A5840C8C1056AA5CCEB31F2517BF4605_CustomAttributesCacheGenerator_OnCompassUpdateDelegate,
	ARLocationProvider_t47C5F7E2A5840C8C1056AA5CCEB31F2517BF4605_CustomAttributesCacheGenerator_OnRestartDelegate,
	SettingsData_t400E00E773911845F27120BD77E2A5A5C7F05585_CustomAttributesCacheGenerator_InitialGroundHeightGuess,
	SettingsData_t400E00E773911845F27120BD77E2A5A5C7F05585_CustomAttributesCacheGenerator_MinGroundHeight,
	SettingsData_t400E00E773911845F27120BD77E2A5A5C7F05585_CustomAttributesCacheGenerator_MaxGroundHeight,
	SettingsData_t400E00E773911845F27120BD77E2A5A5C7F05585_CustomAttributesCacheGenerator_Smoothing,
	SettingsData_t400E00E773911845F27120BD77E2A5A5C7F05585_CustomAttributesCacheGenerator_Precision,
	Hotspot_t94B43358E06B6906BE49D9AD465A62C33E50C6DE_CustomAttributesCacheGenerator_DebugMode,
	Hotspot_t94B43358E06B6906BE49D9AD465A62C33E50C6DE_CustomAttributesCacheGenerator_UseOnLeaveHotspotEvent,
	Hotspot_t94B43358E06B6906BE49D9AD465A62C33E50C6DE_CustomAttributesCacheGenerator_OnHotspotActivated,
	Hotspot_t94B43358E06B6906BE49D9AD465A62C33E50C6DE_CustomAttributesCacheGenerator_OnHotspotLeave,
	HotspotSettingsData_t57879FD5B635460588A94FDFD5877B38B4E1DF83_CustomAttributesCacheGenerator_Prefab,
	HotspotSettingsData_t57879FD5B635460588A94FDFD5877B38B4E1DF83_CustomAttributesCacheGenerator_PositionMode,
	HotspotSettingsData_t57879FD5B635460588A94FDFD5877B38B4E1DF83_CustomAttributesCacheGenerator_ActivationRadius,
	HotspotSettingsData_t57879FD5B635460588A94FDFD5877B38B4E1DF83_CustomAttributesCacheGenerator_AlignToCamera,
	HotspotSettingsData_t57879FD5B635460588A94FDFD5877B38B4E1DF83_CustomAttributesCacheGenerator_DistanceFromCamera,
	HotspotSettingsData_t57879FD5B635460588A94FDFD5877B38B4E1DF83_CustomAttributesCacheGenerator_UseRawLocation,
	MoveAlongPath_t8123BE990CF5AD253811E75F1D2B67697084A372_CustomAttributesCacheGenerator_DebugMode,
	MoveAlongPath_t8123BE990CF5AD253811E75F1D2B67697084A372_CustomAttributesCacheGenerator_state,
	PathSettingsData_t6602AA7CE8BA6CC07093445804B1A4C362D59C50_CustomAttributesCacheGenerator_LocationPath,
	PathSettingsData_t6602AA7CE8BA6CC07093445804B1A4C362D59C50_CustomAttributesCacheGenerator_SplineSampleCount,
	PathSettingsData_t6602AA7CE8BA6CC07093445804B1A4C362D59C50_CustomAttributesCacheGenerator_LineRenderer,
	PlaybackSettingsData_tCD647CAB0B9808022DAD6E1ECBE95C692B817499_CustomAttributesCacheGenerator_Speed,
	PlaybackSettingsData_tCD647CAB0B9808022DAD6E1ECBE95C692B817499_CustomAttributesCacheGenerator_Up,
	PlaybackSettingsData_tCD647CAB0B9808022DAD6E1ECBE95C692B817499_CustomAttributesCacheGenerator_Loop,
	PlaybackSettingsData_tCD647CAB0B9808022DAD6E1ECBE95C692B817499_CustomAttributesCacheGenerator_AutoPlay,
	PlaybackSettingsData_tCD647CAB0B9808022DAD6E1ECBE95C692B817499_CustomAttributesCacheGenerator_Offset,
	PlacementSettingsData_t3C467CCF2933E055B5578E749735434BD6F15C26_CustomAttributesCacheGenerator_AltitudeMode,
	PlacementSettingsData_t3C467CCF2933E055B5578E749735434BD6F15C26_CustomAttributesCacheGenerator_MaxNumberOfLocationUpdates,
	PlaceAlongPath_t0F05120A71DC49EFA8C7D544DFB7B92C878BAB52_CustomAttributesCacheGenerator_Path,
	PlaceAlongPath_t0F05120A71DC49EFA8C7D544DFB7B92C878BAB52_CustomAttributesCacheGenerator_Prefab,
	PlaceAlongPath_t0F05120A71DC49EFA8C7D544DFB7B92C878BAB52_CustomAttributesCacheGenerator_ObjectCount,
	PlaceAlongPath_t0F05120A71DC49EFA8C7D544DFB7B92C878BAB52_CustomAttributesCacheGenerator_SplineSampleSize,
	PlaceAlongPath_t0F05120A71DC49EFA8C7D544DFB7B92C878BAB52_CustomAttributesCacheGenerator_DebugMode,
	PlaceAlongPath_t0F05120A71DC49EFA8C7D544DFB7B92C878BAB52_CustomAttributesCacheGenerator_spline,
	OverrideAltitudeData_tC8B4EAB3FF4B6428F3185C23310A9A4E23CD82DA_CustomAttributesCacheGenerator_OverrideAltitude,
	OverrideAltitudeData_tC8B4EAB3FF4B6428F3185C23310A9A4E23CD82DA_CustomAttributesCacheGenerator_Altitude,
	OverrideAltitudeData_tC8B4EAB3FF4B6428F3185C23310A9A4E23CD82DA_CustomAttributesCacheGenerator_AltitudeMode,
	LocationPropertyData_t9266443C0DBD58342B2A5B94E35D3FBA4F254341_CustomAttributesCacheGenerator_LocationInputType,
	LocationPropertyData_t9266443C0DBD58342B2A5B94E35D3FBA4F254341_CustomAttributesCacheGenerator_LocationData,
	LocationPropertyData_t9266443C0DBD58342B2A5B94E35D3FBA4F254341_CustomAttributesCacheGenerator_Location,
	LocationPropertyData_t9266443C0DBD58342B2A5B94E35D3FBA4F254341_CustomAttributesCacheGenerator_OverrideAltitudeData,
	PlaceAtLocation_t8F470A16D7F7E3A37FEB114A3879DD5136EE24BF_CustomAttributesCacheGenerator_PlacementOptions,
	PlaceAtLocation_t8F470A16D7F7E3A37FEB114A3879DD5136EE24BF_CustomAttributesCacheGenerator_DebugMode,
	PlaceAtLocation_t8F470A16D7F7E3A37FEB114A3879DD5136EE24BF_CustomAttributesCacheGenerator_ObjectLocationUpdated,
	PlaceAtLocation_t8F470A16D7F7E3A37FEB114A3879DD5136EE24BF_CustomAttributesCacheGenerator_ObjectPositionUpdated,
	PlaceAtOptions_t502DAE3C6DF99BCAC137CD1E1F292A46DE07C4DD_CustomAttributesCacheGenerator_MovementSmoothing,
	PlaceAtOptions_t502DAE3C6DF99BCAC137CD1E1F292A46DE07C4DD_CustomAttributesCacheGenerator_MaxNumberOfLocationUpdates,
	PlaceAtOptions_t502DAE3C6DF99BCAC137CD1E1F292A46DE07C4DD_CustomAttributesCacheGenerator_UseMovingAverage,
	PlaceAtOptions_t502DAE3C6DF99BCAC137CD1E1F292A46DE07C4DD_CustomAttributesCacheGenerator_HideObjectUntilItIsPlaced,
	PlaceAtOptions_t502DAE3C6DF99BCAC137CD1E1F292A46DE07C4DD_CustomAttributesCacheGenerator_ShowObjectAfterThisManyUpdates,
	PlaceAtLocations_t8C777F409D11DA3480CDE0F8723857FAA7A4CB3C_CustomAttributesCacheGenerator_Locations,
	PlaceAtLocations_t8C777F409D11DA3480CDE0F8723857FAA7A4CB3C_CustomAttributesCacheGenerator_Prefab,
	PlaceAtLocations_t8C777F409D11DA3480CDE0F8723857FAA7A4CB3C_CustomAttributesCacheGenerator_DebugMode,
	PlaceAtLocations_t8C777F409D11DA3480CDE0F8723857FAA7A4CB3C_CustomAttributesCacheGenerator_locations,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_U3COptionsU3Ek__BackingField,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_U3CCurrentLocationU3Ek__BackingField,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_U3CLastLocationU3Ek__BackingField,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_U3CLastLocationRawU3Ek__BackingField,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_U3CCurrentLocationRawU3Ek__BackingField,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_U3CCurrentHeadingU3Ek__BackingField,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_U3CLastHeadingU3Ek__BackingField,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_U3CFirstLocationU3Ek__BackingField,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_U3CStatusU3Ek__BackingField,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_U3CIsEnabledU3Ek__BackingField,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_U3CFirstReadingU3Ek__BackingField,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_U3CStartTimeU3Ek__BackingField,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_U3CPausedU3Ek__BackingField,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_U3CLocationUpdateCountU3Ek__BackingField,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_U3CApplyCompassTiltCompensationOnAndroidU3Ek__BackingField,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_LocationUpdated,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_CompassUpdated,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_LocationEnabled,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_LocationFailed,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_LocationUpdatedRaw,
	LocationProviderOptions_t89EC102FE8FE7276EC9DFED9C3D71040619CE601_CustomAttributesCacheGenerator_TimeBetweenUpdates,
	LocationProviderOptions_t89EC102FE8FE7276EC9DFED9C3D71040619CE601_CustomAttributesCacheGenerator_MinDistanceBetweenUpdates,
	LocationProviderOptions_t89EC102FE8FE7276EC9DFED9C3D71040619CE601_CustomAttributesCacheGenerator_AccuracyRadius,
	LocationProviderOptions_t89EC102FE8FE7276EC9DFED9C3D71040619CE601_CustomAttributesCacheGenerator_MaxNumberOfUpdates,
	Location_tF89221C24420A624E9A69BB9E7D678B78ABCFF89_CustomAttributesCacheGenerator_Latitude,
	Location_tF89221C24420A624E9A69BB9E7D678B78ABCFF89_CustomAttributesCacheGenerator_Longitude,
	Location_tF89221C24420A624E9A69BB9E7D678B78ABCFF89_CustomAttributesCacheGenerator_Altitude,
	Location_tF89221C24420A624E9A69BB9E7D678B78ABCFF89_CustomAttributesCacheGenerator_AltitudeMode,
	Location_tF89221C24420A624E9A69BB9E7D678B78ABCFF89_CustomAttributesCacheGenerator_Label,
	Spline_t7C4ACB7CA2206C647091AA2974A814D7CC653503_CustomAttributesCacheGenerator_U3CPointsU3Ek__BackingField,
	Spline_t7C4ACB7CA2206C647091AA2974A814D7CC653503_CustomAttributesCacheGenerator_U3CLengthU3Ek__BackingField,
	ARLocationConfig_t56B7DB5281EAA81B29A1DD1283C2F793A6B1D5B8_CustomAttributesCacheGenerator_EarthMeanRadiusInKM,
	ARLocationConfig_t56B7DB5281EAA81B29A1DD1283C2F793A6B1D5B8_CustomAttributesCacheGenerator_EarthEquatorialRadiusInKM,
	ARLocationConfig_t56B7DB5281EAA81B29A1DD1283C2F793A6B1D5B8_CustomAttributesCacheGenerator_EarthFirstEccentricitySquared,
	ARLocationConfig_t56B7DB5281EAA81B29A1DD1283C2F793A6B1D5B8_CustomAttributesCacheGenerator_InitialGroundHeightGuess,
	ARLocationConfig_t56B7DB5281EAA81B29A1DD1283C2F793A6B1D5B8_CustomAttributesCacheGenerator_MinGroundHeight,
	ARLocationConfig_t56B7DB5281EAA81B29A1DD1283C2F793A6B1D5B8_CustomAttributesCacheGenerator_MaxGroundHeight,
	ARLocationConfig_t56B7DB5281EAA81B29A1DD1283C2F793A6B1D5B8_CustomAttributesCacheGenerator_VuforiaGroundHitTestDistance,
	ARLocationConfig_t56B7DB5281EAA81B29A1DD1283C2F793A6B1D5B8_CustomAttributesCacheGenerator_GroundHeightSmoothingFactor,
	ARLocationConfig_t56B7DB5281EAA81B29A1DD1283C2F793A6B1D5B8_CustomAttributesCacheGenerator_UseVuforia,
	ARLocationConfig_t56B7DB5281EAA81B29A1DD1283C2F793A6B1D5B8_CustomAttributesCacheGenerator_UseCustomGeoCalculator,
	LocationData_t1E35051773A207EE9EA92BB2B9B0BEF35BBC927C_CustomAttributesCacheGenerator_Location,
	LocationPath_t457B71C57B13E32DA60EDEF83BCF0F7350AE83D9_CustomAttributesCacheGenerator_Locations,
	LocationPath_t457B71C57B13E32DA60EDEF83BCF0F7350AE83D9_CustomAttributesCacheGenerator_SplineType,
	LocationPath_t457B71C57B13E32DA60EDEF83BCF0F7350AE83D9_CustomAttributesCacheGenerator_Alpha,
	LocationPath_t457B71C57B13E32DA60EDEF83BCF0F7350AE83D9_CustomAttributesCacheGenerator_SceneViewScale,
	OverpassRequestData_tB94BF30CC59E9C1FB3EB3C3A8BE75D520F3DE872_CustomAttributesCacheGenerator_SouthWest,
	OverpassRequestData_tB94BF30CC59E9C1FB3EB3C3A8BE75D520F3DE872_CustomAttributesCacheGenerator_NorthEast,
	OpenStreetMapOptions_tC879CDAF74668DC0F0A00F4B21913196183F6409_CustomAttributesCacheGenerator_OsmXmlFile,
	OpenStreetMapOptions_tC879CDAF74668DC0F0A00F4B21913196183F6409_CustomAttributesCacheGenerator_FetchFromOverpassApi,
	OpenStreetMapOptions_tC879CDAF74668DC0F0A00F4B21913196183F6409_CustomAttributesCacheGenerator_overPassRequestData,
	CreatePointOfInterestTextMeshes_t4893F19D46EE04CF68EBBC3DAD7196E87EF18E28_CustomAttributesCacheGenerator_height,
	CreatePointOfInterestTextMeshes_t4893F19D46EE04CF68EBBC3DAD7196E87EF18E28_CustomAttributesCacheGenerator_textPrefab,
	CreatePointOfInterestTextMeshes_t4893F19D46EE04CF68EBBC3DAD7196E87EF18E28_CustomAttributesCacheGenerator_movementSmoothingFactor,
	CreatePointOfInterestTextMeshes_t4893F19D46EE04CF68EBBC3DAD7196E87EF18E28_CustomAttributesCacheGenerator_locations,
	CreatePointOfInterestTextMeshes_t4893F19D46EE04CF68EBBC3DAD7196E87EF18E28_CustomAttributesCacheGenerator_openStreetMapOptions,
	DevCameraController_tBF345CCD78C34223573FB45F279083FD82C221C4_CustomAttributesCacheGenerator_Speed,
	FadeOutTextMesh_tC2451DD0C4D6BE981FA439AAF6494404DF9FA1C4_CustomAttributesCacheGenerator_Duration,
	SmoothMove_t99450B9A6922B54E5255BDC49A305A2F81864644_CustomAttributesCacheGenerator_Epsilon,
	SmoothMove_t99450B9A6922B54E5255BDC49A305A2F81864644_CustomAttributesCacheGenerator_Precision,
	SmoothMove_t99450B9A6922B54E5255BDC49A305A2F81864644_CustomAttributesCacheGenerator_SmoothMoveMode,
	DebugInfoOverlay_tCBE3576E5308FD421CF002A4F457FC5D6A3D1BEF_CustomAttributesCacheGenerator_Show,
	DebugInfoOverlay_tCBE3576E5308FD421CF002A4F457FC5D6A3D1BEF_CustomAttributesCacheGenerator_ShowObjectInfo,
	LoadingBar_tB573C0002332A4C1E969B01F1532E2261DA385BA_CustomAttributesCacheGenerator_FillPercentage,
	LoadingBar_tB573C0002332A4C1E969B01F1532E2261DA385BA_CustomAttributesCacheGenerator_StartColor,
	LoadingBar_tB573C0002332A4C1E969B01F1532E2261DA385BA_CustomAttributesCacheGenerator_MiddleColor,
	LoadingBar_tB573C0002332A4C1E969B01F1532E2261DA385BA_CustomAttributesCacheGenerator_EndColor,
	LoadingBar_tB573C0002332A4C1E969B01F1532E2261DA385BA_CustomAttributesCacheGenerator_TextColor,
	LoadingBar_tB573C0002332A4C1E969B01F1532E2261DA385BA_CustomAttributesCacheGenerator_UsePercentageText,
	LoadingBar_tB573C0002332A4C1E969B01F1532E2261DA385BA_CustomAttributesCacheGenerator_Text,
	ARFoundationSessionManager_t51DA8ECC9055C8565CCDE4A970EBFAE9F83B84E0_CustomAttributesCacheGenerator_U3CDebugModeU3Ek__BackingField,
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_GameManager_StartFaceIn_m15A60C69C251EF3A37CCA8793C77609A8A86966D,
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_GameManager_StartFadeOut_mD8D5CE92B440CFDD05E2FB89ED79487742F0110E,
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_GameManager_LoadSceneProcess_m574AD3E5A678433A5997F7C60E9427F42286F498,
	U3CStartFaceInU3Ed__8_t5FE7CDF6AF05E40079784B4E2ACFD51D58837AA0_CustomAttributesCacheGenerator_U3CStartFaceInU3Ed__8__ctor_m7079818A07DBC0BD97D6E94739307F77D0F3283E,
	U3CStartFaceInU3Ed__8_t5FE7CDF6AF05E40079784B4E2ACFD51D58837AA0_CustomAttributesCacheGenerator_U3CStartFaceInU3Ed__8_System_IDisposable_Dispose_m87D16B124428B7770442C4A23E7D82B172B53F61,
	U3CStartFaceInU3Ed__8_t5FE7CDF6AF05E40079784B4E2ACFD51D58837AA0_CustomAttributesCacheGenerator_U3CStartFaceInU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m65C44ECA4865FFD05184B4F3EE2330C8DFCD3A0B,
	U3CStartFaceInU3Ed__8_t5FE7CDF6AF05E40079784B4E2ACFD51D58837AA0_CustomAttributesCacheGenerator_U3CStartFaceInU3Ed__8_System_Collections_IEnumerator_Reset_mE52774F682FA16F3053C7E9AE62376B4710908AE,
	U3CStartFaceInU3Ed__8_t5FE7CDF6AF05E40079784B4E2ACFD51D58837AA0_CustomAttributesCacheGenerator_U3CStartFaceInU3Ed__8_System_Collections_IEnumerator_get_Current_m49C57D84E2BF0FAC0417A795CD9616B96574DA0C,
	U3CStartFadeOutU3Ed__9_tEA7B6452D46BF558C6DC7A2365C52EC47E8A199F_CustomAttributesCacheGenerator_U3CStartFadeOutU3Ed__9__ctor_mD648679059EC0F73970223E316119756120FD93F,
	U3CStartFadeOutU3Ed__9_tEA7B6452D46BF558C6DC7A2365C52EC47E8A199F_CustomAttributesCacheGenerator_U3CStartFadeOutU3Ed__9_System_IDisposable_Dispose_m001FF71F492E1CCD1899CEE42795509435E7B5D7,
	U3CStartFadeOutU3Ed__9_tEA7B6452D46BF558C6DC7A2365C52EC47E8A199F_CustomAttributesCacheGenerator_U3CStartFadeOutU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m00E311E91E75B2493F96B8E68E278FEB80AE7BD3,
	U3CStartFadeOutU3Ed__9_tEA7B6452D46BF558C6DC7A2365C52EC47E8A199F_CustomAttributesCacheGenerator_U3CStartFadeOutU3Ed__9_System_Collections_IEnumerator_Reset_mDAAADDFC83B651B0174B00CAD21E68E516300398,
	U3CStartFadeOutU3Ed__9_tEA7B6452D46BF558C6DC7A2365C52EC47E8A199F_CustomAttributesCacheGenerator_U3CStartFadeOutU3Ed__9_System_Collections_IEnumerator_get_Current_mCFA324023124B8EDD082C20F612189052FA4B9D8,
	U3CLoadSceneProcessU3Ed__10_tCEEEECB67F643ED087AAAB846276DF898F600AB1_CustomAttributesCacheGenerator_U3CLoadSceneProcessU3Ed__10__ctor_m6CDB34BE216CB09481787D2ED703468BE64D7332,
	U3CLoadSceneProcessU3Ed__10_tCEEEECB67F643ED087AAAB846276DF898F600AB1_CustomAttributesCacheGenerator_U3CLoadSceneProcessU3Ed__10_System_IDisposable_Dispose_m5A6F97FC6505E83BBB8C8CBF7470F607FE916B4C,
	U3CLoadSceneProcessU3Ed__10_tCEEEECB67F643ED087AAAB846276DF898F600AB1_CustomAttributesCacheGenerator_U3CLoadSceneProcessU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCF0C7DF2C64DA02082A7A2A74DE722D39B1E0204,
	U3CLoadSceneProcessU3Ed__10_tCEEEECB67F643ED087AAAB846276DF898F600AB1_CustomAttributesCacheGenerator_U3CLoadSceneProcessU3Ed__10_System_Collections_IEnumerator_Reset_m509E561AF662F37FDE93B813CE99DBD9A90F55D3,
	U3CLoadSceneProcessU3Ed__10_tCEEEECB67F643ED087AAAB846276DF898F600AB1_CustomAttributesCacheGenerator_U3CLoadSceneProcessU3Ed__10_System_Collections_IEnumerator_get_Current_m586C86E1C8409A486D4A254D98BC2A9B9B44A5DF,
	LevelManager_t010B312A2B35B45291F58195216ABB5673174961_CustomAttributesCacheGenerator_LevelManager_StartInit_m30C8D1DF952A4EC8846D1832B160EF8D3D24CF54,
	U3CStartInitU3Ed__18_t701C9769307CB8A0DACFE4676F2E948FE47FB66A_CustomAttributesCacheGenerator_U3CStartInitU3Ed__18__ctor_mD86FB48F2D277AF855C85A0D385274DE0F09F48B,
	U3CStartInitU3Ed__18_t701C9769307CB8A0DACFE4676F2E948FE47FB66A_CustomAttributesCacheGenerator_U3CStartInitU3Ed__18_System_IDisposable_Dispose_m4D19E1481379C5652564FCB7D292FDB76B331223,
	U3CStartInitU3Ed__18_t701C9769307CB8A0DACFE4676F2E948FE47FB66A_CustomAttributesCacheGenerator_U3CStartInitU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC75C338E7B5727677BA7086AA0E6756C733B5D81,
	U3CStartInitU3Ed__18_t701C9769307CB8A0DACFE4676F2E948FE47FB66A_CustomAttributesCacheGenerator_U3CStartInitU3Ed__18_System_Collections_IEnumerator_Reset_mFA4BED517684325FC5B7F229B7192C2D505E4C1B,
	U3CStartInitU3Ed__18_t701C9769307CB8A0DACFE4676F2E948FE47FB66A_CustomAttributesCacheGenerator_U3CStartInitU3Ed__18_System_Collections_IEnumerator_get_Current_m66DE071D48938C290236DBC667A0E07C9A8BDEAA,
	MonsterSpawner_t38B0F58A8A9657860995DDF676E764E0DEC57B4D_CustomAttributesCacheGenerator_MonsterSpawner_get_SpawnedMonsters_mAD9C0B8FB1E2B2DD7128C57173C9240DB08BF1DE,
	MonsterSpawner_t38B0F58A8A9657860995DDF676E764E0DEC57B4D_CustomAttributesCacheGenerator_MonsterSpawner_SpawnDelayManager_m82B84B029BB88920F85390D4989FD584D047922E,
	U3CSpawnDelayManagerU3Ed__9_tBD67D61B15F4EBCCAE1FD83AE18597F0882C5AE0_CustomAttributesCacheGenerator_U3CSpawnDelayManagerU3Ed__9__ctor_m1B28413DB68191AC508C0FC77639CD3D9AD0F716,
	U3CSpawnDelayManagerU3Ed__9_tBD67D61B15F4EBCCAE1FD83AE18597F0882C5AE0_CustomAttributesCacheGenerator_U3CSpawnDelayManagerU3Ed__9_System_IDisposable_Dispose_m69CADD0F5D17F0CBFDDE131227961C2DE15D40A0,
	U3CSpawnDelayManagerU3Ed__9_tBD67D61B15F4EBCCAE1FD83AE18597F0882C5AE0_CustomAttributesCacheGenerator_U3CSpawnDelayManagerU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3F415A5E8D0B5B05E0F9C4149AB92892F29B2354,
	U3CSpawnDelayManagerU3Ed__9_tBD67D61B15F4EBCCAE1FD83AE18597F0882C5AE0_CustomAttributesCacheGenerator_U3CSpawnDelayManagerU3Ed__9_System_Collections_IEnumerator_Reset_m2ABA99903FDC4FD7C8184B096BAF4A4317D3D33A,
	U3CSpawnDelayManagerU3Ed__9_tBD67D61B15F4EBCCAE1FD83AE18597F0882C5AE0_CustomAttributesCacheGenerator_U3CSpawnDelayManagerU3Ed__9_System_Collections_IEnumerator_get_Current_m1842BD1717ECA5B3DF59F106E04C05420B638460,
	MonsterBase_t2CD611A20C74AAF093EE6B851712E7FFD2052E29_CustomAttributesCacheGenerator_MonsterBase_StartDissolve_m7F85FF286A481851C26D95445EA2390B64CBEA58,
	U3CStartDissolveU3Ed__18_t696122E2D0CFB06601A00FF2F088544EFCF6A760_CustomAttributesCacheGenerator_U3CStartDissolveU3Ed__18__ctor_m99EBB6373EA227790398DF072DC7FDFFF4E9984F,
	U3CStartDissolveU3Ed__18_t696122E2D0CFB06601A00FF2F088544EFCF6A760_CustomAttributesCacheGenerator_U3CStartDissolveU3Ed__18_System_IDisposable_Dispose_m4DCDAA5EF9E0BE416FE650B81177B3141D3DEE00,
	U3CStartDissolveU3Ed__18_t696122E2D0CFB06601A00FF2F088544EFCF6A760_CustomAttributesCacheGenerator_U3CStartDissolveU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA692E30108097D34B9F38D48BE9272E3526DAE28,
	U3CStartDissolveU3Ed__18_t696122E2D0CFB06601A00FF2F088544EFCF6A760_CustomAttributesCacheGenerator_U3CStartDissolveU3Ed__18_System_Collections_IEnumerator_Reset_mAA39C36CA2D5936B8EEC9958D1CA3C37DE121F45,
	U3CStartDissolveU3Ed__18_t696122E2D0CFB06601A00FF2F088544EFCF6A760_CustomAttributesCacheGenerator_U3CStartDissolveU3Ed__18_System_Collections_IEnumerator_get_Current_m2C53CBA6E4244BAAEA1DCED9E9887F4B3D5E33AD,
	LaserAttack_t3D280ADBBA2C6DFD7F8B5D9EC9B3B44B5A4DAE91_CustomAttributesCacheGenerator_LaserAttack_StartLaserShoot_mAE771AC815D2FDA14C49DC1145F4BD36F7B2F493,
	U3CStartLaserShootU3Ed__11_t31E05FDA8294BBDFDE83519C5A8FE302183C7FFF_CustomAttributesCacheGenerator_U3CStartLaserShootU3Ed__11__ctor_mE1534AE9457A2F2983970899550161C0E1961184,
	U3CStartLaserShootU3Ed__11_t31E05FDA8294BBDFDE83519C5A8FE302183C7FFF_CustomAttributesCacheGenerator_U3CStartLaserShootU3Ed__11_System_IDisposable_Dispose_mC16771D60C05E1F8CD1FDEEB4AC8608D5FE29AA5,
	U3CStartLaserShootU3Ed__11_t31E05FDA8294BBDFDE83519C5A8FE302183C7FFF_CustomAttributesCacheGenerator_U3CStartLaserShootU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7B5A398A3400E5CCA15ABF1DEF0DDACEE19D6B71,
	U3CStartLaserShootU3Ed__11_t31E05FDA8294BBDFDE83519C5A8FE302183C7FFF_CustomAttributesCacheGenerator_U3CStartLaserShootU3Ed__11_System_Collections_IEnumerator_Reset_m8D5A5C60E01AAD6CC3C744000836C43B2C7125D1,
	U3CStartLaserShootU3Ed__11_t31E05FDA8294BBDFDE83519C5A8FE302183C7FFF_CustomAttributesCacheGenerator_U3CStartLaserShootU3Ed__11_System_Collections_IEnumerator_get_Current_m1D86B570876F58018D35CE675211107522217022,
	DontDestroySingleton_1_tF85C94A33AEC78EC5129B6356600771BA9FB3508_CustomAttributesCacheGenerator_DontDestroySingleton_1_get_Instance_mF832FA16C7100E8644E59EA113527E60FC5D34AA,
	DontDestroySingleton_1_tF85C94A33AEC78EC5129B6356600771BA9FB3508_CustomAttributesCacheGenerator_DontDestroySingleton_1_set_Instance_m716DF2AA008DC4D2D89A9284FDEA76F741EA98AC,
	SingletonMonoBehaviour_1_t13D4D87A01B2D33C83A7C1C9BD55C27324B9A877_CustomAttributesCacheGenerator_SingletonMonoBehaviour_1_get_Instance_m3BBD71535D10CA29F0F813F1B99682503FB35FDB,
	SingletonMonoBehaviour_1_t13D4D87A01B2D33C83A7C1C9BD55C27324B9A877_CustomAttributesCacheGenerator_SingletonMonoBehaviour_1_set_Instance_mF964E17F2E86911D27D3E802B6825FA31FC38A16,
	UI_RemainGauge_t7496C063310AB0F2BBB293AF8B0C4DF905F98BB7_CustomAttributesCacheGenerator_UI_RemainGauge_FillProcess_m420FCBD4AEB281F14DE1A1CCA2CD68C432358317,
	U3CFillProcessU3Ed__3_t16C38ACC665B308ED772CE1D8D3D8590403963AD_CustomAttributesCacheGenerator_U3CFillProcessU3Ed__3__ctor_m7533C355B52D97ACE9EFDB6A90F00FF6CDC7F342,
	U3CFillProcessU3Ed__3_t16C38ACC665B308ED772CE1D8D3D8590403963AD_CustomAttributesCacheGenerator_U3CFillProcessU3Ed__3_System_IDisposable_Dispose_m7734513A2A74F692A53C9EB8F90139016CB9911E,
	U3CFillProcessU3Ed__3_t16C38ACC665B308ED772CE1D8D3D8590403963AD_CustomAttributesCacheGenerator_U3CFillProcessU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAE2833C5C61CAF11477658332A2503E083FECF6C,
	U3CFillProcessU3Ed__3_t16C38ACC665B308ED772CE1D8D3D8590403963AD_CustomAttributesCacheGenerator_U3CFillProcessU3Ed__3_System_Collections_IEnumerator_Reset_m9EA26AD85A50E88B0FF1BA54FFE34DFC8340A108,
	U3CFillProcessU3Ed__3_t16C38ACC665B308ED772CE1D8D3D8590403963AD_CustomAttributesCacheGenerator_U3CFillProcessU3Ed__3_System_Collections_IEnumerator_get_Current_mB798384D19B7F28181D405A208D539B2EDB14DBB,
	EffectController_tF93D81123371E3D7BEB00D86028212D6B180D99D_CustomAttributesCacheGenerator_EffectController_AutoDestroy_m5F1D4BBC35C44EECE5484E85385D7115EEC6D83C,
	U3CAutoDestroyU3Ed__4_t28203B1A36A0C012905840F27C0277804C0033B0_CustomAttributesCacheGenerator_U3CAutoDestroyU3Ed__4__ctor_m3DE476309B549C7CA7EB4B061196F501A1018FDC,
	U3CAutoDestroyU3Ed__4_t28203B1A36A0C012905840F27C0277804C0033B0_CustomAttributesCacheGenerator_U3CAutoDestroyU3Ed__4_System_IDisposable_Dispose_m433D1E5FB0B56EEA1B6313DAABB81F3950E2FE98,
	U3CAutoDestroyU3Ed__4_t28203B1A36A0C012905840F27C0277804C0033B0_CustomAttributesCacheGenerator_U3CAutoDestroyU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5F2B4316E69BC244BAFCBAFA3E88254FC6681645,
	U3CAutoDestroyU3Ed__4_t28203B1A36A0C012905840F27C0277804C0033B0_CustomAttributesCacheGenerator_U3CAutoDestroyU3Ed__4_System_Collections_IEnumerator_Reset_m61ECC122D0F85642C100FA58F3A6760D04468BD7,
	U3CAutoDestroyU3Ed__4_t28203B1A36A0C012905840F27C0277804C0033B0_CustomAttributesCacheGenerator_U3CAutoDestroyU3Ed__4_System_Collections_IEnumerator_get_Current_mE0F3602906D6150EC37D93C40AED9868B925FD18,
	CameraShakeSimpleScript_t948411B88D6F43F322BCE831BCF7E5405604259A_CustomAttributesCacheGenerator_CameraShakeSimpleScript_Shake_m42597BD58B7F3D9BC32D60DAB652F6440FC63587,
	U3CShakeU3Ed__5_t4D78750F530E974ECA1E718C81F0AC4A03284EB6_CustomAttributesCacheGenerator_U3CShakeU3Ed__5__ctor_m5D29E1AE6E04D92D16729B96B800C084006CB5D4,
	U3CShakeU3Ed__5_t4D78750F530E974ECA1E718C81F0AC4A03284EB6_CustomAttributesCacheGenerator_U3CShakeU3Ed__5_System_IDisposable_Dispose_mC4197FEC180AA9152C9C30B75E072D8A38432F20,
	U3CShakeU3Ed__5_t4D78750F530E974ECA1E718C81F0AC4A03284EB6_CustomAttributesCacheGenerator_U3CShakeU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9688D01BD1B73DA02BCB535D7C43C2BA45DAF9DE,
	U3CShakeU3Ed__5_t4D78750F530E974ECA1E718C81F0AC4A03284EB6_CustomAttributesCacheGenerator_U3CShakeU3Ed__5_System_Collections_IEnumerator_Reset_m84CD7308C3D7700156AADBB3E5FD3ECAB36CE0A1,
	U3CShakeU3Ed__5_t4D78750F530E974ECA1E718C81F0AC4A03284EB6_CustomAttributesCacheGenerator_U3CShakeU3Ed__5_System_Collections_IEnumerator_get_Current_m15B1182C1BF75FAD4CAFB859C4AAB9FAE6D87FB8,
	ProjectileMoveScript_t8E5328E659143159EA6BDE95FD65EC7A674B237E_CustomAttributesCacheGenerator_ProjectileMoveScript_DestroyParticle_mDC85A6227258B0E71CA43EA5947AEA8532005258,
	U3CDestroyParticleU3Ed__20_t524359CC15BFCBE7B00549E3EE37DFDA5E23832D_CustomAttributesCacheGenerator_U3CDestroyParticleU3Ed__20__ctor_m6CCEA1B0192AE584C47ED3E95E22C535B3B7B65C,
	U3CDestroyParticleU3Ed__20_t524359CC15BFCBE7B00549E3EE37DFDA5E23832D_CustomAttributesCacheGenerator_U3CDestroyParticleU3Ed__20_System_IDisposable_Dispose_mF9AF76E483AE14F67683EB3011C23AC1FEAC05EB,
	U3CDestroyParticleU3Ed__20_t524359CC15BFCBE7B00549E3EE37DFDA5E23832D_CustomAttributesCacheGenerator_U3CDestroyParticleU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBB0FD3EDEFE94591609CC8EB62D6533867F7C2EA,
	U3CDestroyParticleU3Ed__20_t524359CC15BFCBE7B00549E3EE37DFDA5E23832D_CustomAttributesCacheGenerator_U3CDestroyParticleU3Ed__20_System_Collections_IEnumerator_Reset_m1D2E2E07966F4FEE5D3215EE446DC2EF3EBA05D9,
	U3CDestroyParticleU3Ed__20_t524359CC15BFCBE7B00549E3EE37DFDA5E23832D_CustomAttributesCacheGenerator_U3CDestroyParticleU3Ed__20_System_Collections_IEnumerator_get_Current_mCC511785DBF4831A2F138071E4344C80A8B93AEB,
	RotateToMouseScript_t484F9391662B1A307753F7AE65E5DC8BDB6764E9_CustomAttributesCacheGenerator_RotateToMouseScript_UpdateRay_m189BAF74F72D7118405984C537604D8BE919F383,
	U3CUpdateRayU3Ed__9_tAF42C0C52B4ACFDCDC70E2B1A1F726AA6756614C_CustomAttributesCacheGenerator_U3CUpdateRayU3Ed__9__ctor_m3C424ED7532D389836FDAC227F621D2561122C80,
	U3CUpdateRayU3Ed__9_tAF42C0C52B4ACFDCDC70E2B1A1F726AA6756614C_CustomAttributesCacheGenerator_U3CUpdateRayU3Ed__9_System_IDisposable_Dispose_m18B52E52265964D921527AB34EF9B2D69B377033,
	U3CUpdateRayU3Ed__9_tAF42C0C52B4ACFDCDC70E2B1A1F726AA6756614C_CustomAttributesCacheGenerator_U3CUpdateRayU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0DC77B6D548F360FF981E72C8AB1930A2E066091,
	U3CUpdateRayU3Ed__9_tAF42C0C52B4ACFDCDC70E2B1A1F726AA6756614C_CustomAttributesCacheGenerator_U3CUpdateRayU3Ed__9_System_Collections_IEnumerator_Reset_m25D63F3001C98BE581B5B039926E0EAF8B406774,
	U3CUpdateRayU3Ed__9_tAF42C0C52B4ACFDCDC70E2B1A1F726AA6756614C_CustomAttributesCacheGenerator_U3CUpdateRayU3Ed__9_System_Collections_IEnumerator_get_Current_mDA0F9789BA24799B4B1A8BE256F36C6C4BCA517B,
	ARSessionStateExtensions_tF98CE3C37471E7FDFDA1B89FD81C123E7D62E789_CustomAttributesCacheGenerator_ARSessionStateExtensions_ToInfoString_m3ED78BE2349594A91DD2BDB4D17D5FC7A39E0E2A,
	DemoBall_t2DC8C97097BDF12123C9ECFAA6E1926345A2BC28_CustomAttributesCacheGenerator_DemoBall_ProgrammedDeath_m2DF4845C32A8963BAB27D7F0BFACC5784712BE92,
	U3CProgrammedDeathU3Ed__3_t66AB9997CF14E4C726CFF62F618BBFC4DABC3C14_CustomAttributesCacheGenerator_U3CProgrammedDeathU3Ed__3__ctor_m594CF3FB53C5D8B5ED586479D34EE72E4943BF2F,
	U3CProgrammedDeathU3Ed__3_t66AB9997CF14E4C726CFF62F618BBFC4DABC3C14_CustomAttributesCacheGenerator_U3CProgrammedDeathU3Ed__3_System_IDisposable_Dispose_m543CD3EBD9CAC69065A27E5644411AFE8B6C7B6A,
	U3CProgrammedDeathU3Ed__3_t66AB9997CF14E4C726CFF62F618BBFC4DABC3C14_CustomAttributesCacheGenerator_U3CProgrammedDeathU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC82FC1704348F2794A1AEB9E8C43335F3189EDEA,
	U3CProgrammedDeathU3Ed__3_t66AB9997CF14E4C726CFF62F618BBFC4DABC3C14_CustomAttributesCacheGenerator_U3CProgrammedDeathU3Ed__3_System_Collections_IEnumerator_Reset_m62594DC3054FFC736C7286487579485F6501901F,
	U3CProgrammedDeathU3Ed__3_t66AB9997CF14E4C726CFF62F618BBFC4DABC3C14_CustomAttributesCacheGenerator_U3CProgrammedDeathU3Ed__3_System_Collections_IEnumerator_get_Current_mA0F24F509E8DF2CC5413AC39750F9F5B91179303,
	WorldBuilder_t691400967398BB82C3F8221E09896EE668F3CC77_CustomAttributesCacheGenerator_WorldBuilder_Start_mF5C8668B8938138597B9481C6EE54912A6D003C6,
	WorldBuilder_t691400967398BB82C3F8221E09896EE668F3CC77_CustomAttributesCacheGenerator_WorldBuilder_WaitForLocationServices_mAD680BF0BABC411653D36076744A658D2CDB7F88,
	U3CStartU3Ed__11_t923C741C5866E5117C2111EE5A4180E589AB1FB9_CustomAttributesCacheGenerator_U3CStartU3Ed__11__ctor_mEB9F8E9FB8863FCA856C475CE3953732827C45C7,
	U3CStartU3Ed__11_t923C741C5866E5117C2111EE5A4180E589AB1FB9_CustomAttributesCacheGenerator_U3CStartU3Ed__11_System_IDisposable_Dispose_m6CF5F632049E8A079C6149D25FE882EC0CF02BEB,
	U3CStartU3Ed__11_t923C741C5866E5117C2111EE5A4180E589AB1FB9_CustomAttributesCacheGenerator_U3CStartU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD1F8834D7A88F6C1E72CA8F700BDA9843C2D2760,
	U3CStartU3Ed__11_t923C741C5866E5117C2111EE5A4180E589AB1FB9_CustomAttributesCacheGenerator_U3CStartU3Ed__11_System_Collections_IEnumerator_Reset_mF722C6C6D5BD5A2DA52B63B58E6839696D07AD6E,
	U3CStartU3Ed__11_t923C741C5866E5117C2111EE5A4180E589AB1FB9_CustomAttributesCacheGenerator_U3CStartU3Ed__11_System_Collections_IEnumerator_get_Current_m9995E0F4AADFE599BC6EE841F982DCF470C58B12,
	U3CWaitForLocationServicesU3Ed__12_t0CFACBF6935FDEC52C2AC8482C8A969EB746EB2C_CustomAttributesCacheGenerator_U3CWaitForLocationServicesU3Ed__12__ctor_mB0E081157FC6F80947552EAD52001420488B82B5,
	U3CWaitForLocationServicesU3Ed__12_t0CFACBF6935FDEC52C2AC8482C8A969EB746EB2C_CustomAttributesCacheGenerator_U3CWaitForLocationServicesU3Ed__12_System_IDisposable_Dispose_m5CAE00BE382E2B0ECBEF52701C19C94DA0DF77CF,
	U3CWaitForLocationServicesU3Ed__12_t0CFACBF6935FDEC52C2AC8482C8A969EB746EB2C_CustomAttributesCacheGenerator_U3CWaitForLocationServicesU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m362C4F91E9E792C76526D2E013841DF893902A65,
	U3CWaitForLocationServicesU3Ed__12_t0CFACBF6935FDEC52C2AC8482C8A969EB746EB2C_CustomAttributesCacheGenerator_U3CWaitForLocationServicesU3Ed__12_System_Collections_IEnumerator_Reset_m10F9138D89CBDC158E7ADC150DCA060A55C3FB91,
	U3CWaitForLocationServicesU3Ed__12_t0CFACBF6935FDEC52C2AC8482C8A969EB746EB2C_CustomAttributesCacheGenerator_U3CWaitForLocationServicesU3Ed__12_System_Collections_IEnumerator_get_Current_m001F487FE1FEE0A266039C66B648E70740B30C46,
	WorldBuilderApplicationController_tFE48840B225954BFB9782D9799DAFF218C391228_CustomAttributesCacheGenerator_WorldBuilderApplicationController_Start_m7BF24802EEBFD8E5ACA68C3E5FF3874A4F566BF2,
	WorldBuilderApplicationController_tFE48840B225954BFB9782D9799DAFF218C391228_CustomAttributesCacheGenerator_WorldBuilderApplicationController_RestoreWorldFromServer_mA563DAEBCA8262FF90B99CF632F40A6D373822DF,
	WorldBuilderApplicationController_tFE48840B225954BFB9782D9799DAFF218C391228_CustomAttributesCacheGenerator_WorldBuilderApplicationController_SaveWorldToServer_mFC353BC5E5FB6C6A95D58CC10B63E490A33AC0D1,
	WorldBuilderApplicationController_tFE48840B225954BFB9782D9799DAFF218C391228_CustomAttributesCacheGenerator_WorldBuilderApplicationController_U3CInitListenersU3Eb__20_0_mC871FB82C6B59363A8E17E80EA63841B13003815,
	WorldBuilderApplicationController_tFE48840B225954BFB9782D9799DAFF218C391228_CustomAttributesCacheGenerator_WorldBuilderApplicationController_U3CInitListenersU3Eb__20_1_mF8DFC4421B87BB2D906802A705C3833A7BBE8473,
	WorldBuilderApplicationController_tFE48840B225954BFB9782D9799DAFF218C391228_CustomAttributesCacheGenerator_WorldBuilderApplicationController_U3CInitListenersU3Eb__20_2_m9DC706A88153D75FBB7A30F81AA9218271DDFDFC,
	WorldBuilderApplicationController_tFE48840B225954BFB9782D9799DAFF218C391228_CustomAttributesCacheGenerator_WorldBuilderApplicationController_U3CInitListenersU3Eb__20_3_mEA2DD74846E7886EA6536091896FA4A80B764DE6,
	WorldBuilderApplicationController_tFE48840B225954BFB9782D9799DAFF218C391228_CustomAttributesCacheGenerator_WorldBuilderApplicationController_U3CInitListenersU3Eb__20_4_m508550143EDA38FDF68C6667595A85CE4E69B0EF,
	WorldBuilderApplicationController_tFE48840B225954BFB9782D9799DAFF218C391228_CustomAttributesCacheGenerator_WorldBuilderApplicationController_U3CInitListenersU3Eb__20_5_mEDD0A78086F101395F38061E004564D447FBB1D4,
	WorldBuilderApplicationController_tFE48840B225954BFB9782D9799DAFF218C391228_CustomAttributesCacheGenerator_WorldBuilderApplicationController_U3CInitListenersU3Eb__20_6_mAA2BBB581B0F52725AEF21D88D79BBEA0C30C7C7,
	WorldBuilderApplicationController_tFE48840B225954BFB9782D9799DAFF218C391228_CustomAttributesCacheGenerator_WorldBuilderApplicationController_U3CInitListenersU3Eb__20_7_m807A32FBC85802214180E9E098C63F559C3BFD48,
	WorldBuilderApplicationController_tFE48840B225954BFB9782D9799DAFF218C391228_CustomAttributesCacheGenerator_WorldBuilderApplicationController_U3CInitListenersU3Eb__20_8_m7492559B333393C3B25E10B3C35785CCC79E6860,
	U3CStartU3Ed__13_tB5AD52B13DBA13A7D2D366C50585F21D7A9A8937_CustomAttributesCacheGenerator_U3CStartU3Ed__13__ctor_m06D9792956694C6F63DE017842934D761D75FBE8,
	U3CStartU3Ed__13_tB5AD52B13DBA13A7D2D366C50585F21D7A9A8937_CustomAttributesCacheGenerator_U3CStartU3Ed__13_System_IDisposable_Dispose_mBF3B890289A394AE9CAF99B4DCDABDE522DAD059,
	U3CStartU3Ed__13_tB5AD52B13DBA13A7D2D366C50585F21D7A9A8937_CustomAttributesCacheGenerator_U3CStartU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6200AF3D9A6BBF0809F460CE0B6C783C7B7DCB02,
	U3CStartU3Ed__13_tB5AD52B13DBA13A7D2D366C50585F21D7A9A8937_CustomAttributesCacheGenerator_U3CStartU3Ed__13_System_Collections_IEnumerator_Reset_m40C07F31A227594C80E097030B3EDEF2C3B22F7F,
	U3CStartU3Ed__13_tB5AD52B13DBA13A7D2D366C50585F21D7A9A8937_CustomAttributesCacheGenerator_U3CStartU3Ed__13_System_Collections_IEnumerator_get_Current_m4C7C637169F0DF843ED9C5F8D911C4CD3014AF8B,
	U3CRestoreWorldFromServerU3Ed__14_t1BC195D258F6C9A677CFE33012A10FED77E61B13_CustomAttributesCacheGenerator_U3CRestoreWorldFromServerU3Ed__14__ctor_mF3F4A3FDBDCF050E861ED1E3A6E9DEC5D715EB2D,
	U3CRestoreWorldFromServerU3Ed__14_t1BC195D258F6C9A677CFE33012A10FED77E61B13_CustomAttributesCacheGenerator_U3CRestoreWorldFromServerU3Ed__14_System_IDisposable_Dispose_mB5F202DC734ACF01C484046EEEBAE8A48B2EACB9,
	U3CRestoreWorldFromServerU3Ed__14_t1BC195D258F6C9A677CFE33012A10FED77E61B13_CustomAttributesCacheGenerator_U3CRestoreWorldFromServerU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD42336260FEAA43388A9AF7A040C6F042DBD3486,
	U3CRestoreWorldFromServerU3Ed__14_t1BC195D258F6C9A677CFE33012A10FED77E61B13_CustomAttributesCacheGenerator_U3CRestoreWorldFromServerU3Ed__14_System_Collections_IEnumerator_Reset_m38F3FA13DD5E07C58EA9E82377624F6AFBF35429,
	U3CRestoreWorldFromServerU3Ed__14_t1BC195D258F6C9A677CFE33012A10FED77E61B13_CustomAttributesCacheGenerator_U3CRestoreWorldFromServerU3Ed__14_System_Collections_IEnumerator_get_Current_mA605986B2D56E29D9FD72231E3B50AF445F37D32,
	U3CSaveWorldToServerU3Ed__15_tFDB6D1F6FE0DEE26967CAF125CED4C9F5FDA9C0B_CustomAttributesCacheGenerator_U3CSaveWorldToServerU3Ed__15__ctor_m14D5CC1F2CC507AE1FA6D45CFD5DDA75E24F117C,
	U3CSaveWorldToServerU3Ed__15_tFDB6D1F6FE0DEE26967CAF125CED4C9F5FDA9C0B_CustomAttributesCacheGenerator_U3CSaveWorldToServerU3Ed__15_System_IDisposable_Dispose_mC570EBAAD87479C60DE2D0E67F80E49B43535249,
	U3CSaveWorldToServerU3Ed__15_tFDB6D1F6FE0DEE26967CAF125CED4C9F5FDA9C0B_CustomAttributesCacheGenerator_U3CSaveWorldToServerU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m670E216A9319DE58D0BF21D1793D304B4B97C121,
	U3CSaveWorldToServerU3Ed__15_tFDB6D1F6FE0DEE26967CAF125CED4C9F5FDA9C0B_CustomAttributesCacheGenerator_U3CSaveWorldToServerU3Ed__15_System_Collections_IEnumerator_Reset_m1650694EF7B9228311E87AB511ED902BDF99B2E9,
	U3CSaveWorldToServerU3Ed__15_tFDB6D1F6FE0DEE26967CAF125CED4C9F5FDA9C0B_CustomAttributesCacheGenerator_U3CSaveWorldToServerU3Ed__15_System_Collections_IEnumerator_get_Current_mE8BDB1F28C86F245BCACBAAAD2C152F342754615,
	WorldVoxelController_tBE60EC4C79077AEED7070B50ACC3D6E28ACCD504_CustomAttributesCacheGenerator_WorldVoxelController_StartWorld_mA7C7545E75A4B6739CE61C519D402BD39522EF81,
	WorldVoxelController_tBE60EC4C79077AEED7070B50ACC3D6E28ACCD504_CustomAttributesCacheGenerator_WorldVoxelController_Start_m162E8257FD63959A45673E21BA2DB4B6DECE089F,
	WorldVoxelController_tBE60EC4C79077AEED7070B50ACC3D6E28ACCD504_CustomAttributesCacheGenerator_WorldVoxelController_WaitForLocationServices_m588EAE02EEB489DA9D369AF56BC651D4A0863CE9,
	WorldVoxelController_tBE60EC4C79077AEED7070B50ACC3D6E28ACCD504_CustomAttributesCacheGenerator_WorldVoxelController_U3CInitUiListenersU3Eb__39_0_mE40BF2B245962F81EE8C10668E40EF8B7CFB8134,
	WorldVoxelController_tBE60EC4C79077AEED7070B50ACC3D6E28ACCD504_CustomAttributesCacheGenerator_WorldVoxelController_U3CInitUiListenersU3Eb__39_1_mAE7182B07E77340E71F56E8564EF486BB810AD0A,
	WorldVoxelController_tBE60EC4C79077AEED7070B50ACC3D6E28ACCD504_CustomAttributesCacheGenerator_WorldVoxelController_U3CInitUiListenersU3Eb__39_2_mE8D5158E3CD95CC70F8AD364470DB2CABF1E63A0,
	WorldVoxelController_tBE60EC4C79077AEED7070B50ACC3D6E28ACCD504_CustomAttributesCacheGenerator_WorldVoxelController_U3CInitUiListenersU3Eb__39_3_mF513E58BE82BB090DF1DAE6522BC7FCE36514ACB,
	WorldVoxelController_tBE60EC4C79077AEED7070B50ACC3D6E28ACCD504_CustomAttributesCacheGenerator_WorldVoxelController_U3CInitUiListenersU3Eb__39_4_mFC18641C18AEB5B13B49D67FBAB5939BD8CB0CFB,
	U3CStartWorldU3Ed__19_t750B4DEF83B7D2F7C41FA78DDB0224ED46CDDA47_CustomAttributesCacheGenerator_U3CStartWorldU3Ed__19__ctor_m44505E7226447BDBEB5254C492A76937A8E3DD79,
	U3CStartWorldU3Ed__19_t750B4DEF83B7D2F7C41FA78DDB0224ED46CDDA47_CustomAttributesCacheGenerator_U3CStartWorldU3Ed__19_System_IDisposable_Dispose_mCAB48FFD6DE743461B606E763022F88271A9FD39,
	U3CStartWorldU3Ed__19_t750B4DEF83B7D2F7C41FA78DDB0224ED46CDDA47_CustomAttributesCacheGenerator_U3CStartWorldU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m78DF8260192FAFE310E5360CD8EF02F3B43A7A0F,
	U3CStartWorldU3Ed__19_t750B4DEF83B7D2F7C41FA78DDB0224ED46CDDA47_CustomAttributesCacheGenerator_U3CStartWorldU3Ed__19_System_Collections_IEnumerator_Reset_m9F662F7B9C885672EFDD4222565A5E937C44C05C,
	U3CStartWorldU3Ed__19_t750B4DEF83B7D2F7C41FA78DDB0224ED46CDDA47_CustomAttributesCacheGenerator_U3CStartWorldU3Ed__19_System_Collections_IEnumerator_get_Current_m8BF6ABC362F36F4951247BF36FDCED71D631AD5D,
	U3CStartU3Ed__20_tBC91C2BA3682FCE4567CEAB39669F7C789CF1525_CustomAttributesCacheGenerator_U3CStartU3Ed__20__ctor_m7CA2FD67733BA04D3A2ACD7C80148A5469666BCC,
	U3CStartU3Ed__20_tBC91C2BA3682FCE4567CEAB39669F7C789CF1525_CustomAttributesCacheGenerator_U3CStartU3Ed__20_System_IDisposable_Dispose_m80FC7FA199E6EB9DAB383B481849F381D33FA439,
	U3CStartU3Ed__20_tBC91C2BA3682FCE4567CEAB39669F7C789CF1525_CustomAttributesCacheGenerator_U3CStartU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m301E2F3CD89FEFBEA67102DADFF9EB0CC42FAE1C,
	U3CStartU3Ed__20_tBC91C2BA3682FCE4567CEAB39669F7C789CF1525_CustomAttributesCacheGenerator_U3CStartU3Ed__20_System_Collections_IEnumerator_Reset_m803FCDAF3A12C2A8CC7AE7B959C8A019F1D4CC59,
	U3CStartU3Ed__20_tBC91C2BA3682FCE4567CEAB39669F7C789CF1525_CustomAttributesCacheGenerator_U3CStartU3Ed__20_System_Collections_IEnumerator_get_Current_m812D99C892C7C4A52DFA739A3C65BA559981F365,
	U3CWaitForLocationServicesU3Ed__25_tB371C83EDE76677BE0191100440D26A065116399_CustomAttributesCacheGenerator_U3CWaitForLocationServicesU3Ed__25__ctor_m1B02CDC690DCFEBCD395815B9C407A9A8DED5CD6,
	U3CWaitForLocationServicesU3Ed__25_tB371C83EDE76677BE0191100440D26A065116399_CustomAttributesCacheGenerator_U3CWaitForLocationServicesU3Ed__25_System_IDisposable_Dispose_m941288AD2EDFC591E0E75D745EEC5522E39D5A07,
	U3CWaitForLocationServicesU3Ed__25_tB371C83EDE76677BE0191100440D26A065116399_CustomAttributesCacheGenerator_U3CWaitForLocationServicesU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBC6B654B63F027360A6713FC61A90B19D573798B,
	U3CWaitForLocationServicesU3Ed__25_tB371C83EDE76677BE0191100440D26A065116399_CustomAttributesCacheGenerator_U3CWaitForLocationServicesU3Ed__25_System_Collections_IEnumerator_Reset_mDA9FD6ADCE132254F59D30A14527991ACD9B4628,
	U3CWaitForLocationServicesU3Ed__25_tB371C83EDE76677BE0191100440D26A065116399_CustomAttributesCacheGenerator_U3CWaitForLocationServicesU3Ed__25_System_Collections_IEnumerator_get_Current_mC431C45C29F5F8090EA663CAFCD0CB3650CCFA2C,
	ARLocationManager_t224AB8AD4D95A0921BA4A44A7ED5C39729FBC2FF_CustomAttributesCacheGenerator_ARLocationManager_get_SessionManager_m7538DD59B2F33BB6968EA518B62372CF04742A3B,
	ARLocationManager_t224AB8AD4D95A0921BA4A44A7ED5C39729FBC2FF_CustomAttributesCacheGenerator_ARLocationManager_set_SessionManager_m3135CEBAF97C1405412F6D42F1078C0270C0C4DC,
	ARLocationManager_t224AB8AD4D95A0921BA4A44A7ED5C39729FBC2FF_CustomAttributesCacheGenerator_ARLocationManager_get_MainCamera_mAC9188AB6E6C21B8A383B47B0B33C5B7DA5A3EC9,
	ARLocationManager_t224AB8AD4D95A0921BA4A44A7ED5C39729FBC2FF_CustomAttributesCacheGenerator_ARLocationManager_set_MainCamera_m9632FECE6D0F301084EDBCCC85CFB89F7EA13DCC,
	ARLocationOrientation_tD8A832D47ECB471D892449B5ADB4143078E1C8B7_CustomAttributesCacheGenerator_ARLocationOrientation_U3CStartU3Eb__19_0_m16FDD14BAFB949736D63B5D028648801A8CB64E4,
	ARLocationProvider_t47C5F7E2A5840C8C1056AA5CCEB31F2517BF4605_CustomAttributesCacheGenerator_ARLocationProvider_get_Provider_m887F7C21BC73DB9CAD4B509C9512C2F195F14F06,
	ARLocationProvider_t47C5F7E2A5840C8C1056AA5CCEB31F2517BF4605_CustomAttributesCacheGenerator_ARLocationProvider_set_Provider_mB59E69874BFB9942095C19D1A10B7C60BEA52755,
	ARLocationProvider_t47C5F7E2A5840C8C1056AA5CCEB31F2517BF4605_CustomAttributesCacheGenerator_ARLocationProvider_add_OnLocationUpdatedDelegate_mBECC6CBE675617268F495916CA8AE6F9A9BFB516,
	ARLocationProvider_t47C5F7E2A5840C8C1056AA5CCEB31F2517BF4605_CustomAttributesCacheGenerator_ARLocationProvider_remove_OnLocationUpdatedDelegate_m13F50E2CE9DE3A18AB494417C1A55BE5D8C6C7F9,
	ARLocationProvider_t47C5F7E2A5840C8C1056AA5CCEB31F2517BF4605_CustomAttributesCacheGenerator_ARLocationProvider_add_OnCompassUpdateDelegate_m337750E84FA36388355DD5570262B0B01166AAFD,
	ARLocationProvider_t47C5F7E2A5840C8C1056AA5CCEB31F2517BF4605_CustomAttributesCacheGenerator_ARLocationProvider_remove_OnCompassUpdateDelegate_mC392E83FB55C2BE9AC31D703173926B3CFA18B55,
	ARLocationProvider_t47C5F7E2A5840C8C1056AA5CCEB31F2517BF4605_CustomAttributesCacheGenerator_ARLocationProvider_add_OnRestartDelegate_m78C024AD631AD87AF747ACCF956151432093435B,
	ARLocationProvider_t47C5F7E2A5840C8C1056AA5CCEB31F2517BF4605_CustomAttributesCacheGenerator_ARLocationProvider_remove_OnRestartDelegate_mD4983BA1806542C3E3A6E28412FDEC564C6ED49F,
	ARLocationProvider_t47C5F7E2A5840C8C1056AA5CCEB31F2517BF4605_CustomAttributesCacheGenerator_ARLocationProvider_Start_m69C6FC9955364F2684BA22E24DD9C9E27567D71D,
	U3CStartU3Ed__50_tE495468A4B52BE5F74CE7E3FFBEDAB0D1C021E74_CustomAttributesCacheGenerator_U3CStartU3Ed__50__ctor_m2B4DF061E1072A472835FE6256FF60D401AD8CA2,
	U3CStartU3Ed__50_tE495468A4B52BE5F74CE7E3FFBEDAB0D1C021E74_CustomAttributesCacheGenerator_U3CStartU3Ed__50_System_IDisposable_Dispose_m0E91CC8D29B8EF95CEE495E16650F8D83AB1BDF6,
	U3CStartU3Ed__50_tE495468A4B52BE5F74CE7E3FFBEDAB0D1C021E74_CustomAttributesCacheGenerator_U3CStartU3Ed__50_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF20E6BE6EB3C485DE54B36A7F29E47C907105304,
	U3CStartU3Ed__50_tE495468A4B52BE5F74CE7E3FFBEDAB0D1C021E74_CustomAttributesCacheGenerator_U3CStartU3Ed__50_System_Collections_IEnumerator_Reset_m5FFC148EF84C94EEAA5CD46AD158E9DE3B66AA4A,
	U3CStartU3Ed__50_tE495468A4B52BE5F74CE7E3FFBEDAB0D1C021E74_CustomAttributesCacheGenerator_U3CStartU3Ed__50_System_Collections_IEnumerator_get_Current_m7E5C18D9EA3A76035949ACF59E44559CA348C494,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_get_Options_m45AFB8A040845CC8970328A9FE96293DFD66CCBD,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_set_Options_m384397626F3231DE2648F61DFEFEDD8BFBCB1A44,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_get_CurrentLocation_mC04CF8FFD09A254E309F0EBBFC0434F4E2041684,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_set_CurrentLocation_mD9694B36EA445C60C398D489B7EBAF69F50B7EB1,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_get_LastLocation_m5E185AE31CF2C37237F036F1DCC7BD459596BB77,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_set_LastLocation_mC3B207433454B09C3868932D3DC1AFD16B600415,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_get_LastLocationRaw_mDEADE5CF1022B15A1592F0929B0BC21D9F2CC977,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_set_LastLocationRaw_m5D57CA6540787FD2E24B6478CDAF86B859A16C0E,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_get_CurrentLocationRaw_m880B4211D7DAEB0AB39ED52F4A813758A9605056,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_set_CurrentLocationRaw_m26F24C4C0B3B60DCF9B22EC4E8466A0A0ED8675F,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_get_CurrentHeading_mD80D19546F4443BB60E7E9D61E59B58CDEC1199F,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_set_CurrentHeading_mB74106DF4587EBDE3F57FF479E60F32F5E9FFFF1,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_get_LastHeading_mB01E086263805EA68D7B80ABA0CDDFD0445F917C,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_set_LastHeading_mD2CF2AF6A78A7E114287296DBEB2EC0ABBB12F96,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_get_FirstLocation_m60E03F687A379A4335F2DBE0F58F1E20A2C5C5D2,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_set_FirstLocation_mF220D93D45C42245EC4771C0F8C59C8864BA1732,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_get_Status_m0BEEE7DFFE907C117F8A146C6065F383FAD19A43,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_set_Status_m3F097F1D9F22EB71392A734225177BB87A295D47,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_get_IsEnabled_mBD269C10E329A27A38D66A7CA158743B97B58F89,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_set_IsEnabled_m3A32454858129B89353398A2743017144C6B685F,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_get_FirstReading_m5006CFC216447EE18516095A7CF062C6A81650B2,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_set_FirstReading_m85CC459283A9388061409706249A862A29577952,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_get_StartTime_m64773366D843563C916E242F620BE5E473A7B689,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_set_StartTime_mCD10C0C5AEC1FAE41EAB6CF699EF02B13237CFB4,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_get_Paused_m8D420B15FA656A2B16A0C47AFD05679D151A4180,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_set_Paused_m2F74FD61266871E92E9CDAD730AB25AE7B69EE39,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_get_LocationUpdateCount_mEA51D40856CC1555C931628EDEEEDAB73FA14F53,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_set_LocationUpdateCount_m814551EB5ACE513616AB4C6BB2C612D6EB71A59E,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_get_ApplyCompassTiltCompensationOnAndroid_m298A7CD45897CC2D57A5BA30527F08DC4E54302A,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_set_ApplyCompassTiltCompensationOnAndroid_mD59851680B505575BA8A0D87823C1281C854E17A,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_add_LocationUpdated_m99954AADAB5627FAB20F3A3718C7DBFAC4E164AB,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_remove_LocationUpdated_m90482A177E20A2BDF8C35EB365F6072FE1F3A528,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_add_CompassUpdated_m63BE5871D33D25A91831B24568F9766D22150521,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_remove_CompassUpdated_m834BB886E4F3C2D8F0914735E9C7D9B19206FAB2,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_add_LocationEnabled_m04F24C06BA328800DCF23DEF9B1B444D9161B6E9,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_remove_LocationEnabled_m56E3C2816E869919163DAEBA9519EF47272A4C13,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_add_LocationFailed_m62E22AD9698B257D3CE4DDE77031156BB1C19739,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_remove_LocationFailed_mAA22271EB80E5E787F10963DBEE054C0D5D928EC,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_add_LocationUpdatedRaw_m4B2F37C64CBCFFE32FD651D8C7027A405E5277EB,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_remove_LocationUpdatedRaw_m113322049E3C08175A459F1FAD2FF9F6FC7E1EC6,
	AbstractLocationProvider_t742B31A1CC23048B7B2ECB51575CBDC370E36767_CustomAttributesCacheGenerator_AbstractLocationProvider_Start_mD5EBFCF2163C86BB9ECEA01D142CFB6E24C5BAD5,
	U3CStartU3Ed__89_tA04EB5BAC5E34AFA5EF0368C59EA9782780DF767_CustomAttributesCacheGenerator_U3CStartU3Ed__89__ctor_m1D15935BFD90E695E3C979F3F1F4BCF71CE3F9DC,
	U3CStartU3Ed__89_tA04EB5BAC5E34AFA5EF0368C59EA9782780DF767_CustomAttributesCacheGenerator_U3CStartU3Ed__89_System_IDisposable_Dispose_mB8AA0F35E6C40D991491A3390F07BC1EB6B96B30,
	U3CStartU3Ed__89_tA04EB5BAC5E34AFA5EF0368C59EA9782780DF767_CustomAttributesCacheGenerator_U3CStartU3Ed__89_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6423F0FB4D41529D08427B4F0A636A851EC9F630,
	U3CStartU3Ed__89_tA04EB5BAC5E34AFA5EF0368C59EA9782780DF767_CustomAttributesCacheGenerator_U3CStartU3Ed__89_System_Collections_IEnumerator_Reset_m554CA534C945FB9A68B28404FFC7BED1B9387085,
	U3CStartU3Ed__89_tA04EB5BAC5E34AFA5EF0368C59EA9782780DF767_CustomAttributesCacheGenerator_U3CStartU3Ed__89_System_Collections_IEnumerator_get_Current_m339872A51678CE1543372092B74593D2BE1272EC,
	ILocationProvider_t4046DCCD18259875A852462DFC63D67B7DFBF640_CustomAttributesCacheGenerator_ILocationProvider_add_LocationUpdated_mBF35FA6BB2B61615AEC352A7958C89A04F6993E3,
	ILocationProvider_t4046DCCD18259875A852462DFC63D67B7DFBF640_CustomAttributesCacheGenerator_ILocationProvider_remove_LocationUpdated_mF2A8F2F6F36403B4B915291B2C65D83D545E979E,
	ILocationProvider_t4046DCCD18259875A852462DFC63D67B7DFBF640_CustomAttributesCacheGenerator_ILocationProvider_add_LocationUpdatedRaw_m1B6A89FB978ECC363F7505786082825D5DF75958,
	ILocationProvider_t4046DCCD18259875A852462DFC63D67B7DFBF640_CustomAttributesCacheGenerator_ILocationProvider_remove_LocationUpdatedRaw_m6BC3AE80D28DAE0FCBC5366F8FBDCB1CA41E0D76,
	ILocationProvider_t4046DCCD18259875A852462DFC63D67B7DFBF640_CustomAttributesCacheGenerator_ILocationProvider_add_CompassUpdated_mF281DEC5D92A5255375CDFD02C15F2EC40C104CA,
	ILocationProvider_t4046DCCD18259875A852462DFC63D67B7DFBF640_CustomAttributesCacheGenerator_ILocationProvider_remove_CompassUpdated_mFC84A1AB8A828A30442E998763ED32DA957A9F34,
	ILocationProvider_t4046DCCD18259875A852462DFC63D67B7DFBF640_CustomAttributesCacheGenerator_ILocationProvider_add_LocationEnabled_m4E14056B4D6803810444AFA24FE6977B165C5B48,
	ILocationProvider_t4046DCCD18259875A852462DFC63D67B7DFBF640_CustomAttributesCacheGenerator_ILocationProvider_remove_LocationEnabled_m201B4E151A7D9D3C46F2887E203D55132D0B4184,
	ILocationProvider_t4046DCCD18259875A852462DFC63D67B7DFBF640_CustomAttributesCacheGenerator_ILocationProvider_add_LocationFailed_m330504FC106356485A538FB65453FA6888D7F915,
	ILocationProvider_t4046DCCD18259875A852462DFC63D67B7DFBF640_CustomAttributesCacheGenerator_ILocationProvider_remove_LocationFailed_m4F850E583325BE578362CE7417E021964A100D4F,
	Spline_t7C4ACB7CA2206C647091AA2974A814D7CC653503_CustomAttributesCacheGenerator_Spline_get_Points_m111B8595E12A32C1CE96DEFEFDD0B48878715273,
	Spline_t7C4ACB7CA2206C647091AA2974A814D7CC653503_CustomAttributesCacheGenerator_Spline_set_Points_m1290D1B1F8FE867E67DA67B29C640FE2418FA9E3,
	Spline_t7C4ACB7CA2206C647091AA2974A814D7CC653503_CustomAttributesCacheGenerator_Spline_get_Length_mAC4A0448D7DE253DCE2E6281AC4106CA6E9ED377,
	Spline_t7C4ACB7CA2206C647091AA2974A814D7CC653503_CustomAttributesCacheGenerator_Spline_set_Length_mE2FBD5C23E4D96D3B6DAACE44300215CBFA35F93,
	CreatePointOfInterestTextMeshes_t4893F19D46EE04CF68EBBC3DAD7196E87EF18E28_CustomAttributesCacheGenerator_CreatePointOfInterestTextMeshes_LoadXMLFileFromOverpassRequest_m6DBAB5D5E1EA7722AE5F7B668AEB88943EF47A38,
	U3CLoadXMLFileFromOverpassRequestU3Ed__10_tAE0DCE7DD96AAA881075718C9F601829ECD843F6_CustomAttributesCacheGenerator_U3CLoadXMLFileFromOverpassRequestU3Ed__10__ctor_mAF6B905BE3065ED6BE60424CF57F777239A8570F,
	U3CLoadXMLFileFromOverpassRequestU3Ed__10_tAE0DCE7DD96AAA881075718C9F601829ECD843F6_CustomAttributesCacheGenerator_U3CLoadXMLFileFromOverpassRequestU3Ed__10_System_IDisposable_Dispose_mF85AAF3EF5980C11CEDBF4B2A28B569558FB2E28,
	U3CLoadXMLFileFromOverpassRequestU3Ed__10_tAE0DCE7DD96AAA881075718C9F601829ECD843F6_CustomAttributesCacheGenerator_U3CLoadXMLFileFromOverpassRequestU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3A34CE79C2E009E8F0B3BD3C52CD02973ADAFAB0,
	U3CLoadXMLFileFromOverpassRequestU3Ed__10_tAE0DCE7DD96AAA881075718C9F601829ECD843F6_CustomAttributesCacheGenerator_U3CLoadXMLFileFromOverpassRequestU3Ed__10_System_Collections_IEnumerator_Reset_mB869557E697089B355822B3AF8682D6116001D3F,
	U3CLoadXMLFileFromOverpassRequestU3Ed__10_tAE0DCE7DD96AAA881075718C9F601829ECD843F6_CustomAttributesCacheGenerator_U3CLoadXMLFileFromOverpassRequestU3Ed__10_System_Collections_IEnumerator_get_Current_mDACA87BB7A9DC4BC6F0A85CE2240F0B42758F4A6,
	FadeOutTextMesh_tC2451DD0C4D6BE981FA439AAF6494404DF9FA1C4_CustomAttributesCacheGenerator_FadeOutTextMesh_FadeOut_m268B3329732D0A10EEADADD15A20627EE1CC5A1F,
	U3CFadeOutU3Ed__3_t63A1FAFDE1FFC2013D402424D2B6DDE3897E7BD7_CustomAttributesCacheGenerator_U3CFadeOutU3Ed__3__ctor_m21132BB33E8FAA9FBBDD345C4F279C62855F8662,
	U3CFadeOutU3Ed__3_t63A1FAFDE1FFC2013D402424D2B6DDE3897E7BD7_CustomAttributesCacheGenerator_U3CFadeOutU3Ed__3_System_IDisposable_Dispose_m424B074A864C2B145D864A64FFE15166BB394E03,
	U3CFadeOutU3Ed__3_t63A1FAFDE1FFC2013D402424D2B6DDE3897E7BD7_CustomAttributesCacheGenerator_U3CFadeOutU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB2BAF570D34EE60A4C29EE183BE2B509CFC3B4BA,
	U3CFadeOutU3Ed__3_t63A1FAFDE1FFC2013D402424D2B6DDE3897E7BD7_CustomAttributesCacheGenerator_U3CFadeOutU3Ed__3_System_Collections_IEnumerator_Reset_mC7AAFD31B0EFD2F4BCF022A723026BCE0B65CF87,
	U3CFadeOutU3Ed__3_t63A1FAFDE1FFC2013D402424D2B6DDE3897E7BD7_CustomAttributesCacheGenerator_U3CFadeOutU3Ed__3_System_Collections_IEnumerator_get_Current_m7370CA01F921987DC870E2AD0F9F458E3D0155BF,
	SmoothMove_t99450B9A6922B54E5255BDC49A305A2F81864644_CustomAttributesCacheGenerator_SmoothMove_MoveTo_mEAF9AC05F02013D4FC48A465D057A7A44BD1E0A4,
	U3CMoveToU3Ed__11_t39BD7A569BA53E9B5CD4627EBFD3C0B37147628F_CustomAttributesCacheGenerator_U3CMoveToU3Ed__11__ctor_m887B7A1814EA9E6BE59A7E3F5D1A706B9D7364B3,
	U3CMoveToU3Ed__11_t39BD7A569BA53E9B5CD4627EBFD3C0B37147628F_CustomAttributesCacheGenerator_U3CMoveToU3Ed__11_System_IDisposable_Dispose_mCD211B0ECC4D98BC7F6C702BA7DFB58BBB84D065,
	U3CMoveToU3Ed__11_t39BD7A569BA53E9B5CD4627EBFD3C0B37147628F_CustomAttributesCacheGenerator_U3CMoveToU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m01E37D522523EC1BE28EFFD441C6D3371C9E84B5,
	U3CMoveToU3Ed__11_t39BD7A569BA53E9B5CD4627EBFD3C0B37147628F_CustomAttributesCacheGenerator_U3CMoveToU3Ed__11_System_Collections_IEnumerator_Reset_m6EE3D9D660CB5F3557551A17CD9CD2E96E093A6E,
	U3CMoveToU3Ed__11_t39BD7A569BA53E9B5CD4627EBFD3C0B37147628F_CustomAttributesCacheGenerator_U3CMoveToU3Ed__11_System_Collections_IEnumerator_get_Current_m968A9BF3B0381A2871C701432AA26FC17547AE91,
	ARFoundationSessionManager_t51DA8ECC9055C8565CCDE4A970EBFAE9F83B84E0_CustomAttributesCacheGenerator_ARFoundationSessionManager_get_DebugMode_m71C4447004798139D1AC193D1D75EBC93047CC4E,
	ARFoundationSessionManager_t51DA8ECC9055C8565CCDE4A970EBFAE9F83B84E0_CustomAttributesCacheGenerator_ARFoundationSessionManager_set_DebugMode_m5EC9658AE686227451186F4704D6F1A80E8B0499,
	AssemblyU2DCSharp_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CfileNameU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CmenuNameU3Ek__BackingField_0(L_0);
		return;
	}
}
