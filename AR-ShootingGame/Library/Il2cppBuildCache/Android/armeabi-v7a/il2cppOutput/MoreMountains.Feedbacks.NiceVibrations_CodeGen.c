﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackHaptics_CustomPlayFeedback_m6AA592FE07B84FC795577FCB970A43F0B8426A9D (void);
// 0x00000002 System.Collections.IEnumerator MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::ContinuousHapticsCoroutine()
extern void MMFeedbackHaptics_ContinuousHapticsCoroutine_m01993FA703820E851718C960CB95D508AF49E51F (void);
// 0x00000003 System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::ComputeElapsedTime()
extern void MMFeedbackHaptics_ComputeElapsedTime_m5C35BF378C9BF0870200F5C0F55B854260AFA6C4 (void);
// 0x00000004 System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::Remap(System.Single,System.Single,System.Single,System.Single,System.Single)
extern void MMFeedbackHaptics_Remap_m86D254DD73ED991398E7B74A4C7B8559E0963A5E (void);
// 0x00000005 System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::.ctor()
extern void MMFeedbackHaptics__ctor_mBC83ABA2DC49DA6DA6F80A87382D07B559559068 (void);
// 0x00000006 System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics::.cctor()
extern void MMFeedbackHaptics__cctor_m8EB9BB4B754FBA8BEAB3E7398646C6648D61FE16 (void);
// 0x00000007 System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics/<ContinuousHapticsCoroutine>d__38::.ctor(System.Int32)
extern void U3CContinuousHapticsCoroutineU3Ed__38__ctor_mE821571FC13CCCEB8D334847F1B87F3150DD02F9 (void);
// 0x00000008 System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics/<ContinuousHapticsCoroutine>d__38::System.IDisposable.Dispose()
extern void U3CContinuousHapticsCoroutineU3Ed__38_System_IDisposable_Dispose_mF3F216154793B8C8257532C6FDDF27832CA71807 (void);
// 0x00000009 System.Boolean MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics/<ContinuousHapticsCoroutine>d__38::MoveNext()
extern void U3CContinuousHapticsCoroutineU3Ed__38_MoveNext_mB05D03D0CEA6E67203D782F92F64CDAACAE6E53A (void);
// 0x0000000A System.Object MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics/<ContinuousHapticsCoroutine>d__38::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CContinuousHapticsCoroutineU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEE0FF6509222F8F64F5E91BE269030C2C034A0AB (void);
// 0x0000000B System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics/<ContinuousHapticsCoroutine>d__38::System.Collections.IEnumerator.Reset()
extern void U3CContinuousHapticsCoroutineU3Ed__38_System_Collections_IEnumerator_Reset_mD2AAB00CAD47726A832F2DAE13EF8651AAA1CFD0 (void);
// 0x0000000C System.Object MoreMountains.FeedbacksForThirdParty.MMFeedbackHaptics/<ContinuousHapticsCoroutine>d__38::System.Collections.IEnumerator.get_Current()
extern void U3CContinuousHapticsCoroutineU3Ed__38_System_Collections_IEnumerator_get_Current_mEAC49B3532158B78993DF5C64BDAA742D8BA0AA1 (void);
static Il2CppMethodPointer s_methodPointers[12] = 
{
	MMFeedbackHaptics_CustomPlayFeedback_m6AA592FE07B84FC795577FCB970A43F0B8426A9D,
	MMFeedbackHaptics_ContinuousHapticsCoroutine_m01993FA703820E851718C960CB95D508AF49E51F,
	MMFeedbackHaptics_ComputeElapsedTime_m5C35BF378C9BF0870200F5C0F55B854260AFA6C4,
	MMFeedbackHaptics_Remap_m86D254DD73ED991398E7B74A4C7B8559E0963A5E,
	MMFeedbackHaptics__ctor_mBC83ABA2DC49DA6DA6F80A87382D07B559559068,
	MMFeedbackHaptics__cctor_m8EB9BB4B754FBA8BEAB3E7398646C6648D61FE16,
	U3CContinuousHapticsCoroutineU3Ed__38__ctor_mE821571FC13CCCEB8D334847F1B87F3150DD02F9,
	U3CContinuousHapticsCoroutineU3Ed__38_System_IDisposable_Dispose_mF3F216154793B8C8257532C6FDDF27832CA71807,
	U3CContinuousHapticsCoroutineU3Ed__38_MoveNext_mB05D03D0CEA6E67203D782F92F64CDAACAE6E53A,
	U3CContinuousHapticsCoroutineU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEE0FF6509222F8F64F5E91BE269030C2C034A0AB,
	U3CContinuousHapticsCoroutineU3Ed__38_System_Collections_IEnumerator_Reset_mD2AAB00CAD47726A832F2DAE13EF8651AAA1CFD0,
	U3CContinuousHapticsCoroutineU3Ed__38_System_Collections_IEnumerator_get_Current_mEAC49B3532158B78993DF5C64BDAA742D8BA0AA1,
};
static const int32_t s_InvokerIndices[12] = 
{
	1670,
	3336,
	3361,
	3779,
	3388,
	5285,
	2760,
	3388,
	3358,
	3336,
	3388,
	3336,
};
extern const CustomAttributesCacheGenerator g_MoreMountains_Feedbacks_NiceVibrations_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_MoreMountains_Feedbacks_NiceVibrations_CodeGenModule;
const Il2CppCodeGenModule g_MoreMountains_Feedbacks_NiceVibrations_CodeGenModule = 
{
	"MoreMountains.Feedbacks.NiceVibrations.dll",
	12,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_MoreMountains_Feedbacks_NiceVibrations_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
