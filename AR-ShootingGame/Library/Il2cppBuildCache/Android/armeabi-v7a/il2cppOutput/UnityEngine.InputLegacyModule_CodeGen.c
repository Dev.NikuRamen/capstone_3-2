﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Int32 UnityEngine.Touch::get_fingerId()
extern void Touch_get_fingerId_mCED0E66949120E69BFE9294DC0A11A6F9FDBD129 (void);
// 0x00000002 UnityEngine.Vector2 UnityEngine.Touch::get_position()
extern void Touch_get_position_mE32B04C6DA32A0965C403A31847ED7F1725EA1DE (void);
// 0x00000003 UnityEngine.Vector2 UnityEngine.Touch::get_deltaPosition()
extern void Touch_get_deltaPosition_mF9D60C253E41DC4E4F832F88A1041BE8A9E7C0FB (void);
// 0x00000004 System.Single UnityEngine.Touch::get_deltaTime()
extern void Touch_get_deltaTime_mFDE8430AB54C2B10F32F0375ED5B2F9CE790AE37 (void);
// 0x00000005 UnityEngine.TouchPhase UnityEngine.Touch::get_phase()
extern void Touch_get_phase_m576EA3F4FE1D12EB85510326AD8EC3C2EB267257 (void);
// 0x00000006 UnityEngine.TouchType UnityEngine.Touch::get_type()
extern void Touch_get_type_m33FB24B6A53A307E8AC9881ED3B483DD4B44C050 (void);
// 0x00000007 System.Void UnityEngine.Gyroscope::.ctor(System.Int32)
extern void Gyroscope__ctor_m59C98DED96D3AC3EC33D22242AB68CBF05AA3540 (void);
// 0x00000008 UnityEngine.Vector3 UnityEngine.Gyroscope::rotationRateUnbiased_Internal(System.Int32)
extern void Gyroscope_rotationRateUnbiased_Internal_m9DFF2142C85D05F468BD8EE97AD974B0DDF0A23D (void);
// 0x00000009 UnityEngine.Vector3 UnityEngine.Gyroscope::gravity_Internal(System.Int32)
extern void Gyroscope_gravity_Internal_m06DBE3A9B198211AB60A67C8BF2F453940162C31 (void);
// 0x0000000A UnityEngine.Vector3 UnityEngine.Gyroscope::userAcceleration_Internal(System.Int32)
extern void Gyroscope_userAcceleration_Internal_mA056F4DF8E0067EDE08D13BFDF9858AF9A2DB541 (void);
// 0x0000000B UnityEngine.Quaternion UnityEngine.Gyroscope::attitude_Internal(System.Int32)
extern void Gyroscope_attitude_Internal_mFD7EC5584645E9F3EAF099D96837D449B86229E5 (void);
// 0x0000000C System.Void UnityEngine.Gyroscope::setEnabled_Internal(System.Int32,System.Boolean)
extern void Gyroscope_setEnabled_Internal_mA8233E80210D91EBB36F768E5F5B3A7F9CA48BFB (void);
// 0x0000000D UnityEngine.Vector3 UnityEngine.Gyroscope::get_rotationRateUnbiased()
extern void Gyroscope_get_rotationRateUnbiased_m80D7378C8A6D38D90126146CA3BEA8911A5CAE33 (void);
// 0x0000000E UnityEngine.Vector3 UnityEngine.Gyroscope::get_gravity()
extern void Gyroscope_get_gravity_m5DF53BC11722D847BE27BEC9A05DF3011423943A (void);
// 0x0000000F UnityEngine.Vector3 UnityEngine.Gyroscope::get_userAcceleration()
extern void Gyroscope_get_userAcceleration_m16E0201BAF2076EBD97810861C9E23C5E94073F7 (void);
// 0x00000010 UnityEngine.Quaternion UnityEngine.Gyroscope::get_attitude()
extern void Gyroscope_get_attitude_m599BF954B806CD2CA5A1C7C97A32E6B8BE6E6481 (void);
// 0x00000011 System.Void UnityEngine.Gyroscope::set_enabled(System.Boolean)
extern void Gyroscope_set_enabled_mCB6436A1155EE9C7D81B96F58F15E75FCE95C1D1 (void);
// 0x00000012 System.Void UnityEngine.Gyroscope::rotationRateUnbiased_Internal_Injected(System.Int32,UnityEngine.Vector3&)
extern void Gyroscope_rotationRateUnbiased_Internal_Injected_mDD925151017ACAF2B850ABF0D3BB794A9DBE3AAB (void);
// 0x00000013 System.Void UnityEngine.Gyroscope::gravity_Internal_Injected(System.Int32,UnityEngine.Vector3&)
extern void Gyroscope_gravity_Internal_Injected_m3E6DF2AC216DB4BA43499F62670DAAAFC49B4EE4 (void);
// 0x00000014 System.Void UnityEngine.Gyroscope::userAcceleration_Internal_Injected(System.Int32,UnityEngine.Vector3&)
extern void Gyroscope_userAcceleration_Internal_Injected_m29DEC1EE762893E13DCCD33D79C5A33F28293C4F (void);
// 0x00000015 System.Void UnityEngine.Gyroscope::attitude_Internal_Injected(System.Int32,UnityEngine.Quaternion&)
extern void Gyroscope_attitude_Internal_Injected_m8717A035C3FDA9FD7B55234BE57E839E602CFBBE (void);
// 0x00000016 System.Single UnityEngine.LocationInfo::get_latitude()
extern void LocationInfo_get_latitude_m09F2DFBDB716C46B0ECFD11F6A5DB2B93B06174B (void);
// 0x00000017 System.Single UnityEngine.LocationInfo::get_longitude()
extern void LocationInfo_get_longitude_mD0F77FFCF02AFB63058C77A4EBAE228528F57A50 (void);
// 0x00000018 System.Single UnityEngine.LocationInfo::get_altitude()
extern void LocationInfo_get_altitude_m2EC063255D07139449D5D56D3FA24AEDAAE7A189 (void);
// 0x00000019 System.Single UnityEngine.LocationInfo::get_horizontalAccuracy()
extern void LocationInfo_get_horizontalAccuracy_m79421A184DF51D7645FEA5BBAF20DD000860BFC5 (void);
// 0x0000001A System.Double UnityEngine.LocationInfo::get_timestamp()
extern void LocationInfo_get_timestamp_m16DAEBB8C94F2E000E874254FA0FF6BD0E02AFE2 (void);
// 0x0000001B UnityEngine.LocationServiceStatus UnityEngine.LocationService::GetLocationStatus()
extern void LocationService_GetLocationStatus_m9B85B6CA1DFE35ECE0B86B6C4847E7B9842E0B5D (void);
// 0x0000001C UnityEngine.LocationInfo UnityEngine.LocationService::GetLastLocation()
extern void LocationService_GetLastLocation_mC203EB89E8E03EA9FE04DE46158B72C07BB31CD8 (void);
// 0x0000001D System.Void UnityEngine.LocationService::SetDesiredAccuracy(System.Single)
extern void LocationService_SetDesiredAccuracy_m2E4DA9923CEB9E6D34E4329DDE33C6D66765334B (void);
// 0x0000001E System.Void UnityEngine.LocationService::SetDistanceFilter(System.Single)
extern void LocationService_SetDistanceFilter_m2DCEBE0A746199024520B58FAE9178895E1D2B18 (void);
// 0x0000001F System.Void UnityEngine.LocationService::StartUpdatingLocation()
extern void LocationService_StartUpdatingLocation_m3F0A36410DA6F8FF49E3652072A8878BC0974C77 (void);
// 0x00000020 UnityEngine.LocationService/HeadingInfo UnityEngine.LocationService::GetLastHeading()
extern void LocationService_GetLastHeading_m7530296212CF484DE1A071C43C8091AE3B80ADA4 (void);
// 0x00000021 System.Boolean UnityEngine.LocationService::IsHeadingUpdatesEnabled()
extern void LocationService_IsHeadingUpdatesEnabled_m6577D67022294D828803A78ED124FA93F82233A6 (void);
// 0x00000022 System.Void UnityEngine.LocationService::SetHeadingUpdatesEnabled(System.Boolean)
extern void LocationService_SetHeadingUpdatesEnabled_m499D46345A5A075EAF8A3FEA2BDCC2A408AA5E8D (void);
// 0x00000023 UnityEngine.LocationServiceStatus UnityEngine.LocationService::get_status()
extern void LocationService_get_status_m1E90C9991825BDF5A3AC066D97F9198568055C54 (void);
// 0x00000024 UnityEngine.LocationInfo UnityEngine.LocationService::get_lastData()
extern void LocationService_get_lastData_mD6EAC681E09427969B18EEC9B85DA43633EFEA1F (void);
// 0x00000025 System.Void UnityEngine.LocationService::Start(System.Single,System.Single)
extern void LocationService_Start_m625DBB1AF4D9E01C6DD790A2E536AC278072D53A (void);
// 0x00000026 System.Void UnityEngine.LocationService::.ctor()
extern void LocationService__ctor_m56E613208DF3884D18A8B50E106F7358C334580C (void);
// 0x00000027 System.Void UnityEngine.LocationService::GetLastLocation_Injected(UnityEngine.LocationInfo&)
extern void LocationService_GetLastLocation_Injected_m1B2A33CE8025C6FC2FDF578B7B1082C9A2774088 (void);
// 0x00000028 System.Void UnityEngine.LocationService::GetLastHeading_Injected(UnityEngine.LocationService/HeadingInfo&)
extern void LocationService_GetLastHeading_Injected_m4BE669A143FFC621E1E459BDE38B03583218AE3A (void);
// 0x00000029 System.Single UnityEngine.Compass::get_magneticHeading()
extern void Compass_get_magneticHeading_mA67B887F44550F05F98BABE85E50624CC8280FF9 (void);
// 0x0000002A System.Single UnityEngine.Compass::get_trueHeading()
extern void Compass_get_trueHeading_m81777B4965C5057255EA53312EDF273EBA068F2B (void);
// 0x0000002B System.Single UnityEngine.Compass::get_headingAccuracy()
extern void Compass_get_headingAccuracy_m9B6BB3282568BCA85129ECB66A24BB40AFDBA96C (void);
// 0x0000002C System.Double UnityEngine.Compass::get_timestamp()
extern void Compass_get_timestamp_m9FA309505EE68A4C615FA264D869CFE4D334C060 (void);
// 0x0000002D System.Boolean UnityEngine.Compass::get_enabled()
extern void Compass_get_enabled_mD08F61A4E59CDA6808ADCF3B6BF550561E54D849 (void);
// 0x0000002E System.Void UnityEngine.Compass::set_enabled(System.Boolean)
extern void Compass_set_enabled_m487581253439E98D30E648EE4F0D1EA37B5C4740 (void);
// 0x0000002F System.Void UnityEngine.Compass::.ctor()
extern void Compass__ctor_m8EB48027E36FF0551820B2FC21916D87332D5DA1 (void);
// 0x00000030 UnityEngine.GameObject UnityEngine.CameraRaycastHelper::RaycastTry(UnityEngine.Camera,UnityEngine.Ray,System.Single,System.Int32)
extern void CameraRaycastHelper_RaycastTry_m8AA2714ED46E79851C77B83A3916C515D7280FD1 (void);
// 0x00000031 UnityEngine.GameObject UnityEngine.CameraRaycastHelper::RaycastTry2D(UnityEngine.Camera,UnityEngine.Ray,System.Single,System.Int32)
extern void CameraRaycastHelper_RaycastTry2D_mAA0B0BAC7BE8A2F640A236BB6655EB47E5408C9D (void);
// 0x00000032 UnityEngine.GameObject UnityEngine.CameraRaycastHelper::RaycastTry_Injected(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)
extern void CameraRaycastHelper_RaycastTry_Injected_mFAED8F3DC204691A92849F36D8BC69CFE68F43E8 (void);
// 0x00000033 UnityEngine.GameObject UnityEngine.CameraRaycastHelper::RaycastTry2D_Injected(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)
extern void CameraRaycastHelper_RaycastTry2D_Injected_m1AB3B5B899C17D5D6FC41D8F8CF2BE549F7D9F21 (void);
// 0x00000034 System.Boolean UnityEngine.Input::GetKeyInt(UnityEngine.KeyCode)
extern void Input_GetKeyInt_mCBF4A2379EF913E589DFF4AE9C9407D0AC6B4E4D (void);
// 0x00000035 System.Boolean UnityEngine.Input::GetKeyString(System.String)
extern void Input_GetKeyString_mDD80ED27B61F349398E3C955DA99A21562298B03 (void);
// 0x00000036 System.Boolean UnityEngine.Input::GetKeyUpInt(UnityEngine.KeyCode)
extern void Input_GetKeyUpInt_mBF5453011D089E0D585F38D8BC72AB743CE243BE (void);
// 0x00000037 System.Boolean UnityEngine.Input::GetKeyDownInt(UnityEngine.KeyCode)
extern void Input_GetKeyDownInt_mEAC027107792507CA7F0B1DFBEA4E6289CDCCCBA (void);
// 0x00000038 System.Single UnityEngine.Input::GetAxis(System.String)
extern void Input_GetAxis_m939297DEB2ECF8D8D09AD66EB69979AAD2B62326 (void);
// 0x00000039 System.Single UnityEngine.Input::GetAxisRaw(System.String)
extern void Input_GetAxisRaw_mC07AC23FD8D04A69CDB07C6399C93FFFAEB0FC5B (void);
// 0x0000003A System.Boolean UnityEngine.Input::GetButton(System.String)
extern void Input_GetButton_m95EE8314087068F3AA9CEF3C3F6A246D55C4734C (void);
// 0x0000003B System.Boolean UnityEngine.Input::GetButtonDown(System.String)
extern void Input_GetButtonDown_m2001112EBCA3D5C7B0344EF62C896667F7E67DDF (void);
// 0x0000003C System.Boolean UnityEngine.Input::GetMouseButton(System.Int32)
extern void Input_GetMouseButton_m27BF2DDBF38A38787B83A13D3E6F0F88F7C834C1 (void);
// 0x0000003D System.Boolean UnityEngine.Input::GetMouseButtonDown(System.Int32)
extern void Input_GetMouseButtonDown_m466D81FDCC87C9CB07557B39DCB435EB691F1EF3 (void);
// 0x0000003E System.Boolean UnityEngine.Input::GetMouseButtonUp(System.Int32)
extern void Input_GetMouseButtonUp_m2BA562F8C4FE8364EEC93970093E776371DF4022 (void);
// 0x0000003F UnityEngine.Touch UnityEngine.Input::GetTouch(System.Int32)
extern void Input_GetTouch_m6A2A31482B1A7D018C3AAC188C02F5D14500C81F (void);
// 0x00000040 System.Boolean UnityEngine.Input::GetKey(UnityEngine.KeyCode)
extern void Input_GetKey_mFDD450A4C61F2930928B12287FFBD1ACCB71E429 (void);
// 0x00000041 System.Boolean UnityEngine.Input::GetKey(System.String)
extern void Input_GetKey_m77E2F3719EC63690632731872A691FF6A27C589C (void);
// 0x00000042 System.Boolean UnityEngine.Input::GetKeyUp(UnityEngine.KeyCode)
extern void Input_GetKeyUp_mDE9D56FE11715566D4D54FD96F8E1EF9734D225F (void);
// 0x00000043 System.Boolean UnityEngine.Input::GetKeyDown(UnityEngine.KeyCode)
extern void Input_GetKeyDown_m476116696E71771641BBECBAB1A4C55E69018220 (void);
// 0x00000044 UnityEngine.Vector3 UnityEngine.Input::get_mousePosition()
extern void Input_get_mousePosition_m79528BC2F30C57054641F709C855130AE586AC0E (void);
// 0x00000045 UnityEngine.Vector2 UnityEngine.Input::get_mouseScrollDelta()
extern void Input_get_mouseScrollDelta_m018B3C74FC710A166684FC8391CAC93D8EC0ADB7 (void);
// 0x00000046 UnityEngine.IMECompositionMode UnityEngine.Input::get_imeCompositionMode()
extern void Input_get_imeCompositionMode_m04AD6A8C7FEE55E7C4F70885DB5AF222E9F904E5 (void);
// 0x00000047 System.Void UnityEngine.Input::set_imeCompositionMode(UnityEngine.IMECompositionMode)
extern void Input_set_imeCompositionMode_m7D4AA771F1F616FE74A97CA186C4B55EF268E112 (void);
// 0x00000048 System.String UnityEngine.Input::get_compositionString()
extern void Input_get_compositionString_mF957B324E35155878D307CE2AEE0AACC9BEC25BD (void);
// 0x00000049 UnityEngine.Vector2 UnityEngine.Input::get_compositionCursorPos()
extern void Input_get_compositionCursorPos_m70946478FB2B607BC3BC5EC1280AA217323518B3 (void);
// 0x0000004A System.Void UnityEngine.Input::set_compositionCursorPos(UnityEngine.Vector2)
extern void Input_set_compositionCursorPos_mA2A9D63F782E3C75F065F031C67C2A1363D47D9C (void);
// 0x0000004B System.Boolean UnityEngine.Input::get_mousePresent()
extern void Input_get_mousePresent_mBCACCE1C97E146FF46C7AE7FFE693F7BAB4E4FE5 (void);
// 0x0000004C System.Int32 UnityEngine.Input::get_touchCount()
extern void Input_get_touchCount_mE1A06AB1973E3456AE398B3CC5105F27CC7335D6 (void);
// 0x0000004D System.Boolean UnityEngine.Input::get_touchSupported()
extern void Input_get_touchSupported_mE5B2F5199B4CC16D89AD2C3125B5CB38F4B4867B (void);
// 0x0000004E UnityEngine.Vector3 UnityEngine.Input::get_acceleration()
extern void Input_get_acceleration_mE04EFD6EDBEBA7B29231FAE71149899AB94B3361 (void);
// 0x0000004F UnityEngine.LocationService UnityEngine.Input::get_location()
extern void Input_get_location_m5D0AE1016E410D3D7411904D0706456DE383F4E5 (void);
// 0x00000050 UnityEngine.Compass UnityEngine.Input::get_compass()
extern void Input_get_compass_mCF060AEBF0E1A08584ED9B861F88D27CEAEF55E0 (void);
// 0x00000051 System.Int32 UnityEngine.Input::GetGyroInternal()
extern void Input_GetGyroInternal_m7DE349896B9EEFE14A16A6D568ED3DA341C887CB (void);
// 0x00000052 UnityEngine.Gyroscope UnityEngine.Input::get_gyro()
extern void Input_get_gyro_mCDF4A8D1C7F0A599A13B94376AB49822AAB32FBF (void);
// 0x00000053 UnityEngine.Touch[] UnityEngine.Input::get_touches()
extern void Input_get_touches_m26E3034CAE32931E59A480327A3CF4BFC2045E8F (void);
// 0x00000054 System.Void UnityEngine.Input::GetTouch_Injected(System.Int32,UnityEngine.Touch&)
extern void Input_GetTouch_Injected_m19710838FFBDDC3E60536B0932D3B5A392BE539D (void);
// 0x00000055 System.Void UnityEngine.Input::get_mousePosition_Injected(UnityEngine.Vector3&)
extern void Input_get_mousePosition_Injected_m4E5460D301ECE27CC322AB79C0914A4503ABC06E (void);
// 0x00000056 System.Void UnityEngine.Input::get_mouseScrollDelta_Injected(UnityEngine.Vector2&)
extern void Input_get_mouseScrollDelta_Injected_m8B072340853637C9CF5A23CEA63ED51B14886EC9 (void);
// 0x00000057 System.Void UnityEngine.Input::get_compositionCursorPos_Injected(UnityEngine.Vector2&)
extern void Input_get_compositionCursorPos_Injected_mE31464243AB1819574A268B942B5667B03F4822E (void);
// 0x00000058 System.Void UnityEngine.Input::set_compositionCursorPos_Injected(UnityEngine.Vector2&)
extern void Input_set_compositionCursorPos_Injected_m4B961AC7900B41DDBFBA03BE2F65F49C030CCB4D (void);
// 0x00000059 System.Void UnityEngine.Input::get_acceleration_Injected(UnityEngine.Vector3&)
extern void Input_get_acceleration_Injected_m77989E1DE2037CB8B8DA4E6D09B056D5A9776802 (void);
// 0x0000005A System.Void UnityEngine.SendMouseEvents::SetMouseMoved()
extern void SendMouseEvents_SetMouseMoved_mEC659144183FB490A2E1F12112C8F08569A511CD (void);
// 0x0000005B System.Void UnityEngine.SendMouseEvents::DoSendMouseEvents(System.Int32)
extern void SendMouseEvents_DoSendMouseEvents_m21561D473C27F19BA9CDBC53B4A13D40DDFBE785 (void);
// 0x0000005C System.Void UnityEngine.SendMouseEvents::SendEvents(System.Int32,UnityEngine.SendMouseEvents/HitInfo)
extern void SendMouseEvents_SendEvents_m7A59BBFBB15C1BF3E47D56CDD4921599A686F9C7 (void);
// 0x0000005D System.Void UnityEngine.SendMouseEvents::.cctor()
extern void SendMouseEvents__cctor_m6B63654E024F338414361C995EAFEC615743A0E0 (void);
// 0x0000005E System.Void UnityEngine.SendMouseEvents/HitInfo::SendMessage(System.String)
extern void HitInfo_SendMessage_m2D813691948EAB9CDA487A3B8668678EABFCFA62 (void);
// 0x0000005F System.Boolean UnityEngine.SendMouseEvents/HitInfo::op_Implicit(UnityEngine.SendMouseEvents/HitInfo)
extern void HitInfo_op_Implicit_m8332A3930623A2248D797F3A8020FFF4E05A9420 (void);
// 0x00000060 System.Boolean UnityEngine.SendMouseEvents/HitInfo::Compare(UnityEngine.SendMouseEvents/HitInfo,UnityEngine.SendMouseEvents/HitInfo)
extern void HitInfo_Compare_m3AD170E7A52826C73DEF8C3C8F6507C9EAB28363 (void);
static Il2CppMethodPointer s_methodPointers[96] = 
{
	Touch_get_fingerId_mCED0E66949120E69BFE9294DC0A11A6F9FDBD129,
	Touch_get_position_mE32B04C6DA32A0965C403A31847ED7F1725EA1DE,
	Touch_get_deltaPosition_mF9D60C253E41DC4E4F832F88A1041BE8A9E7C0FB,
	Touch_get_deltaTime_mFDE8430AB54C2B10F32F0375ED5B2F9CE790AE37,
	Touch_get_phase_m576EA3F4FE1D12EB85510326AD8EC3C2EB267257,
	Touch_get_type_m33FB24B6A53A307E8AC9881ED3B483DD4B44C050,
	Gyroscope__ctor_m59C98DED96D3AC3EC33D22242AB68CBF05AA3540,
	Gyroscope_rotationRateUnbiased_Internal_m9DFF2142C85D05F468BD8EE97AD974B0DDF0A23D,
	Gyroscope_gravity_Internal_m06DBE3A9B198211AB60A67C8BF2F453940162C31,
	Gyroscope_userAcceleration_Internal_mA056F4DF8E0067EDE08D13BFDF9858AF9A2DB541,
	Gyroscope_attitude_Internal_mFD7EC5584645E9F3EAF099D96837D449B86229E5,
	Gyroscope_setEnabled_Internal_mA8233E80210D91EBB36F768E5F5B3A7F9CA48BFB,
	Gyroscope_get_rotationRateUnbiased_m80D7378C8A6D38D90126146CA3BEA8911A5CAE33,
	Gyroscope_get_gravity_m5DF53BC11722D847BE27BEC9A05DF3011423943A,
	Gyroscope_get_userAcceleration_m16E0201BAF2076EBD97810861C9E23C5E94073F7,
	Gyroscope_get_attitude_m599BF954B806CD2CA5A1C7C97A32E6B8BE6E6481,
	Gyroscope_set_enabled_mCB6436A1155EE9C7D81B96F58F15E75FCE95C1D1,
	Gyroscope_rotationRateUnbiased_Internal_Injected_mDD925151017ACAF2B850ABF0D3BB794A9DBE3AAB,
	Gyroscope_gravity_Internal_Injected_m3E6DF2AC216DB4BA43499F62670DAAAFC49B4EE4,
	Gyroscope_userAcceleration_Internal_Injected_m29DEC1EE762893E13DCCD33D79C5A33F28293C4F,
	Gyroscope_attitude_Internal_Injected_m8717A035C3FDA9FD7B55234BE57E839E602CFBBE,
	LocationInfo_get_latitude_m09F2DFBDB716C46B0ECFD11F6A5DB2B93B06174B,
	LocationInfo_get_longitude_mD0F77FFCF02AFB63058C77A4EBAE228528F57A50,
	LocationInfo_get_altitude_m2EC063255D07139449D5D56D3FA24AEDAAE7A189,
	LocationInfo_get_horizontalAccuracy_m79421A184DF51D7645FEA5BBAF20DD000860BFC5,
	LocationInfo_get_timestamp_m16DAEBB8C94F2E000E874254FA0FF6BD0E02AFE2,
	LocationService_GetLocationStatus_m9B85B6CA1DFE35ECE0B86B6C4847E7B9842E0B5D,
	LocationService_GetLastLocation_mC203EB89E8E03EA9FE04DE46158B72C07BB31CD8,
	LocationService_SetDesiredAccuracy_m2E4DA9923CEB9E6D34E4329DDE33C6D66765334B,
	LocationService_SetDistanceFilter_m2DCEBE0A746199024520B58FAE9178895E1D2B18,
	LocationService_StartUpdatingLocation_m3F0A36410DA6F8FF49E3652072A8878BC0974C77,
	LocationService_GetLastHeading_m7530296212CF484DE1A071C43C8091AE3B80ADA4,
	LocationService_IsHeadingUpdatesEnabled_m6577D67022294D828803A78ED124FA93F82233A6,
	LocationService_SetHeadingUpdatesEnabled_m499D46345A5A075EAF8A3FEA2BDCC2A408AA5E8D,
	LocationService_get_status_m1E90C9991825BDF5A3AC066D97F9198568055C54,
	LocationService_get_lastData_mD6EAC681E09427969B18EEC9B85DA43633EFEA1F,
	LocationService_Start_m625DBB1AF4D9E01C6DD790A2E536AC278072D53A,
	LocationService__ctor_m56E613208DF3884D18A8B50E106F7358C334580C,
	LocationService_GetLastLocation_Injected_m1B2A33CE8025C6FC2FDF578B7B1082C9A2774088,
	LocationService_GetLastHeading_Injected_m4BE669A143FFC621E1E459BDE38B03583218AE3A,
	Compass_get_magneticHeading_mA67B887F44550F05F98BABE85E50624CC8280FF9,
	Compass_get_trueHeading_m81777B4965C5057255EA53312EDF273EBA068F2B,
	Compass_get_headingAccuracy_m9B6BB3282568BCA85129ECB66A24BB40AFDBA96C,
	Compass_get_timestamp_m9FA309505EE68A4C615FA264D869CFE4D334C060,
	Compass_get_enabled_mD08F61A4E59CDA6808ADCF3B6BF550561E54D849,
	Compass_set_enabled_m487581253439E98D30E648EE4F0D1EA37B5C4740,
	Compass__ctor_m8EB48027E36FF0551820B2FC21916D87332D5DA1,
	CameraRaycastHelper_RaycastTry_m8AA2714ED46E79851C77B83A3916C515D7280FD1,
	CameraRaycastHelper_RaycastTry2D_mAA0B0BAC7BE8A2F640A236BB6655EB47E5408C9D,
	CameraRaycastHelper_RaycastTry_Injected_mFAED8F3DC204691A92849F36D8BC69CFE68F43E8,
	CameraRaycastHelper_RaycastTry2D_Injected_m1AB3B5B899C17D5D6FC41D8F8CF2BE549F7D9F21,
	Input_GetKeyInt_mCBF4A2379EF913E589DFF4AE9C9407D0AC6B4E4D,
	Input_GetKeyString_mDD80ED27B61F349398E3C955DA99A21562298B03,
	Input_GetKeyUpInt_mBF5453011D089E0D585F38D8BC72AB743CE243BE,
	Input_GetKeyDownInt_mEAC027107792507CA7F0B1DFBEA4E6289CDCCCBA,
	Input_GetAxis_m939297DEB2ECF8D8D09AD66EB69979AAD2B62326,
	Input_GetAxisRaw_mC07AC23FD8D04A69CDB07C6399C93FFFAEB0FC5B,
	Input_GetButton_m95EE8314087068F3AA9CEF3C3F6A246D55C4734C,
	Input_GetButtonDown_m2001112EBCA3D5C7B0344EF62C896667F7E67DDF,
	Input_GetMouseButton_m27BF2DDBF38A38787B83A13D3E6F0F88F7C834C1,
	Input_GetMouseButtonDown_m466D81FDCC87C9CB07557B39DCB435EB691F1EF3,
	Input_GetMouseButtonUp_m2BA562F8C4FE8364EEC93970093E776371DF4022,
	Input_GetTouch_m6A2A31482B1A7D018C3AAC188C02F5D14500C81F,
	Input_GetKey_mFDD450A4C61F2930928B12287FFBD1ACCB71E429,
	Input_GetKey_m77E2F3719EC63690632731872A691FF6A27C589C,
	Input_GetKeyUp_mDE9D56FE11715566D4D54FD96F8E1EF9734D225F,
	Input_GetKeyDown_m476116696E71771641BBECBAB1A4C55E69018220,
	Input_get_mousePosition_m79528BC2F30C57054641F709C855130AE586AC0E,
	Input_get_mouseScrollDelta_m018B3C74FC710A166684FC8391CAC93D8EC0ADB7,
	Input_get_imeCompositionMode_m04AD6A8C7FEE55E7C4F70885DB5AF222E9F904E5,
	Input_set_imeCompositionMode_m7D4AA771F1F616FE74A97CA186C4B55EF268E112,
	Input_get_compositionString_mF957B324E35155878D307CE2AEE0AACC9BEC25BD,
	Input_get_compositionCursorPos_m70946478FB2B607BC3BC5EC1280AA217323518B3,
	Input_set_compositionCursorPos_mA2A9D63F782E3C75F065F031C67C2A1363D47D9C,
	Input_get_mousePresent_mBCACCE1C97E146FF46C7AE7FFE693F7BAB4E4FE5,
	Input_get_touchCount_mE1A06AB1973E3456AE398B3CC5105F27CC7335D6,
	Input_get_touchSupported_mE5B2F5199B4CC16D89AD2C3125B5CB38F4B4867B,
	Input_get_acceleration_mE04EFD6EDBEBA7B29231FAE71149899AB94B3361,
	Input_get_location_m5D0AE1016E410D3D7411904D0706456DE383F4E5,
	Input_get_compass_mCF060AEBF0E1A08584ED9B861F88D27CEAEF55E0,
	Input_GetGyroInternal_m7DE349896B9EEFE14A16A6D568ED3DA341C887CB,
	Input_get_gyro_mCDF4A8D1C7F0A599A13B94376AB49822AAB32FBF,
	Input_get_touches_m26E3034CAE32931E59A480327A3CF4BFC2045E8F,
	Input_GetTouch_Injected_m19710838FFBDDC3E60536B0932D3B5A392BE539D,
	Input_get_mousePosition_Injected_m4E5460D301ECE27CC322AB79C0914A4503ABC06E,
	Input_get_mouseScrollDelta_Injected_m8B072340853637C9CF5A23CEA63ED51B14886EC9,
	Input_get_compositionCursorPos_Injected_mE31464243AB1819574A268B942B5667B03F4822E,
	Input_set_compositionCursorPos_Injected_m4B961AC7900B41DDBFBA03BE2F65F49C030CCB4D,
	Input_get_acceleration_Injected_m77989E1DE2037CB8B8DA4E6D09B056D5A9776802,
	SendMouseEvents_SetMouseMoved_mEC659144183FB490A2E1F12112C8F08569A511CD,
	SendMouseEvents_DoSendMouseEvents_m21561D473C27F19BA9CDBC53B4A13D40DDFBE785,
	SendMouseEvents_SendEvents_m7A59BBFBB15C1BF3E47D56CDD4921599A686F9C7,
	SendMouseEvents__cctor_m6B63654E024F338414361C995EAFEC615743A0E0,
	HitInfo_SendMessage_m2D813691948EAB9CDA487A3B8668678EABFCFA62,
	HitInfo_op_Implicit_m8332A3930623A2248D797F3A8020FFF4E05A9420,
	HitInfo_Compare_m3AD170E7A52826C73DEF8C3C8F6507C9EAB28363,
};
extern void Touch_get_fingerId_mCED0E66949120E69BFE9294DC0A11A6F9FDBD129_AdjustorThunk (void);
extern void Touch_get_position_mE32B04C6DA32A0965C403A31847ED7F1725EA1DE_AdjustorThunk (void);
extern void Touch_get_deltaPosition_mF9D60C253E41DC4E4F832F88A1041BE8A9E7C0FB_AdjustorThunk (void);
extern void Touch_get_deltaTime_mFDE8430AB54C2B10F32F0375ED5B2F9CE790AE37_AdjustorThunk (void);
extern void Touch_get_phase_m576EA3F4FE1D12EB85510326AD8EC3C2EB267257_AdjustorThunk (void);
extern void Touch_get_type_m33FB24B6A53A307E8AC9881ED3B483DD4B44C050_AdjustorThunk (void);
extern void LocationInfo_get_latitude_m09F2DFBDB716C46B0ECFD11F6A5DB2B93B06174B_AdjustorThunk (void);
extern void LocationInfo_get_longitude_mD0F77FFCF02AFB63058C77A4EBAE228528F57A50_AdjustorThunk (void);
extern void LocationInfo_get_altitude_m2EC063255D07139449D5D56D3FA24AEDAAE7A189_AdjustorThunk (void);
extern void LocationInfo_get_horizontalAccuracy_m79421A184DF51D7645FEA5BBAF20DD000860BFC5_AdjustorThunk (void);
extern void LocationInfo_get_timestamp_m16DAEBB8C94F2E000E874254FA0FF6BD0E02AFE2_AdjustorThunk (void);
extern void HitInfo_SendMessage_m2D813691948EAB9CDA487A3B8668678EABFCFA62_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[12] = 
{
	{ 0x06000001, Touch_get_fingerId_mCED0E66949120E69BFE9294DC0A11A6F9FDBD129_AdjustorThunk },
	{ 0x06000002, Touch_get_position_mE32B04C6DA32A0965C403A31847ED7F1725EA1DE_AdjustorThunk },
	{ 0x06000003, Touch_get_deltaPosition_mF9D60C253E41DC4E4F832F88A1041BE8A9E7C0FB_AdjustorThunk },
	{ 0x06000004, Touch_get_deltaTime_mFDE8430AB54C2B10F32F0375ED5B2F9CE790AE37_AdjustorThunk },
	{ 0x06000005, Touch_get_phase_m576EA3F4FE1D12EB85510326AD8EC3C2EB267257_AdjustorThunk },
	{ 0x06000006, Touch_get_type_m33FB24B6A53A307E8AC9881ED3B483DD4B44C050_AdjustorThunk },
	{ 0x06000016, LocationInfo_get_latitude_m09F2DFBDB716C46B0ECFD11F6A5DB2B93B06174B_AdjustorThunk },
	{ 0x06000017, LocationInfo_get_longitude_mD0F77FFCF02AFB63058C77A4EBAE228528F57A50_AdjustorThunk },
	{ 0x06000018, LocationInfo_get_altitude_m2EC063255D07139449D5D56D3FA24AEDAAE7A189_AdjustorThunk },
	{ 0x06000019, LocationInfo_get_horizontalAccuracy_m79421A184DF51D7645FEA5BBAF20DD000860BFC5_AdjustorThunk },
	{ 0x0600001A, LocationInfo_get_timestamp_m16DAEBB8C94F2E000E874254FA0FF6BD0E02AFE2_AdjustorThunk },
	{ 0x0600005E, HitInfo_SendMessage_m2D813691948EAB9CDA487A3B8668678EABFCFA62_AdjustorThunk },
};
static const int32_t s_InvokerIndices[96] = 
{
	3316,
	3382,
	3382,
	3361,
	3316,
	3316,
	2760,
	5167,
	5167,
	5167,
	5109,
	4835,
	3384,
	3384,
	3384,
	3344,
	2810,
	4830,
	4830,
	4830,
	4830,
	3361,
	3361,
	3361,
	3361,
	3296,
	5254,
	5258,
	5211,
	5211,
	5285,
	5300,
	5272,
	5208,
	3316,
	3326,
	1658,
	3388,
	5181,
	5181,
	3361,
	3361,
	3361,
	3296,
	3358,
	2810,
	3388,
	3986,
	3986,
	3969,
	3969,
	5122,
	5125,
	5122,
	5122,
	5149,
	5149,
	5125,
	5125,
	5122,
	5122,
	5122,
	5157,
	5122,
	5125,
	5122,
	5122,
	5282,
	5280,
	5254,
	5189,
	5262,
	5280,
	5214,
	5272,
	5254,
	5272,
	5282,
	5262,
	5262,
	5254,
	5262,
	5262,
	4830,
	5181,
	5181,
	5181,
	5181,
	5181,
	5285,
	5189,
	4837,
	5285,
	2791,
	5137,
	4715,
};
extern const CustomAttributesCacheGenerator g_UnityEngine_InputLegacyModule_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_UnityEngine_InputLegacyModule_CodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_InputLegacyModule_CodeGenModule = 
{
	"UnityEngine.InputLegacyModule.dll",
	96,
	s_methodPointers,
	12,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_UnityEngine_InputLegacyModule_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
