using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_GameStart : MonoBehaviour
{
    [SerializeField] private string gameSceneName;

    public void OnGameStartButtonDown()
    {
        GameManager.Instance.LoadScene(gameSceneName);
    }
}
