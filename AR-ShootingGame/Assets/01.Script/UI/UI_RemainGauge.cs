using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_RemainGauge : MonoBehaviour
{

	[SerializeField] private Image fillImage;

	private float _fireTum;

	public void StartReload(float delay)
	{
		fillImage.fillAmount = 0;
		_fireTum = delay;
		StartCoroutine(FillProcess());
	}

	private IEnumerator FillProcess()
	{
		float time = 0;
		while( fillImage.fillAmount <= 1 ) 
		{
			time += Time.deltaTime;

			fillImage.fillAmount = time / _fireTum;
			yield return new WaitForEndOfFrame();
		}
	}
}
