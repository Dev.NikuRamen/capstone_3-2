using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_CrossHair : MonoBehaviour
{
    [SerializeField] private Animator animator;
    [SerializeField] private string fireTag = "Fire";

    public void Fire()
    {
        animator.SetTrigger(fireTag);
    }
}
