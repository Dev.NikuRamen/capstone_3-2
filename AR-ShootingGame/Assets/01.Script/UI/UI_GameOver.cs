using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UI_GameOver : MonoBehaviour
{
    [Header("Scene")]
    [SerializeField] private string mainMenuSceneName;
    [SerializeField] private string gameSceneName;

    [Header("UI")]
    [SerializeField] private TextMeshProUGUI finalScoreText;
    public void Popup()
    {
        gameObject.SetActive(true);
        finalScoreText.text = LevelManager.Instance.Score.ToString();
    }
    
    public void OnRestartButtonDown()
    {
        GameManager.Instance.LoadScene(gameSceneName);
    }

    public void OnMainMenuButtonDown()
    {
        GameManager.Instance.LoadScene(mainMenuSceneName);
    }
}
