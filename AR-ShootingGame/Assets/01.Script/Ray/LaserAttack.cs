using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[RequireComponent(typeof(LaserBeam))]
public class LaserAttack : MonoBehaviour
{
	[Header("Laser status")]
	[Tooltip("몬스터를 공격하는 데미지")]
	[SerializeField] private int damage;
	[Tooltip("몬스터를 공격할때 공격 사이의 텀(sec)")]
	[SerializeField,Range(0,10)] private int shootDelay;
	[SerializeField] private LaserBeam laserBeam;

	[Header("UI")]
	[SerializeField] private UI_RemainGauge remainGauge;
	[SerializeField] private UI_CrossHair uiCrossHair;

	[Header("SFX")]
	[SerializeField] private AudioClip fireSFX;
	
	private float _shootInterval = 0;
	private bool _isShooting = false;
	private IEnumerator _laserBeamFireProcess = null;
	private ColorType _colorType = ColorType.Red;
	
	
	/// <summary>
	/// 발사! 찍!
	/// </summary>
	public void Fire()
	{
		if ( _laserBeamFireProcess == null )
		{
			_laserBeamFireProcess = StartLaserShoot();
			remainGauge.StartReload(shootDelay);
			uiCrossHair.Fire();
			GameManager.Instance.AudioController.PlaySFX(fireSFX);
			
			StartCoroutine(_laserBeamFireProcess);
		}

	}
	
	private IEnumerator StartLaserShoot()
	{
		if ( !_isShooting ) // 만약 발사 중이 아니면
		{
			_isShooting = true;
			EditorDebug.Log("Fire");
			var hitMonster = laserBeam.GetHitMonster(); // 레이저 빔에서 맞은 몬스터를 가져온다
			if ( hitMonster )                           // 만약 몬스터가 없으면 공격 프로세스 종료.
			{
				if ( hitMonster.ColorType == LevelManager.Instance.CurrentColorType )
				{
						hitMonster.Damage(damage);
				}
			}
			while( _shootInterval <= shootDelay ) // 다음 레이저가 발사될때 까지 딜레이 되는 시간
			{
				_shootInterval += Time.deltaTime;     // 이전 프레임과 현재 프레임까지의 시간을 interval에 넣어준다
				yield return new WaitForEndOfFrame(); // 다음 프레임까지 대기.
			}

			_shootInterval = 0;
			_isShooting = false;
			_laserBeamFireProcess = null;
		}
	}
}
