using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserBeam : MonoBehaviour
{
    [Header("Camera setting")]
    [SerializeField, Range(0, 1000f)] private float laserMaxDistance;


    /// <summary>
    /// 레이캐스트로 맞은 몬스터를 가져온다
    /// </summary>
    /// <returns>맞은 몬스터의 컴포넌트</returns>
    public MonsterBase GetHitMonster()
    {
        var cameraTransform = Camera.main.transform;
        EditorDebug.Log("Camera : " + Camera.main.name);
        Ray ray = new Ray(cameraTransform.position, cameraTransform.forward);

#if UNITY_EDITOR
        Debug.DrawRay(cameraTransform.position, cameraTransform.forward, Color.red,3f);
        EditorDebug.Log("fire");
#endif
        
        MonsterBase hitMonster = null;
        if ( Physics.Raycast(ray,out var hit, laserMaxDistance, 1 << LayerMask.NameToLayer("Monster")) )
        {
            hitMonster = hit.collider.GetComponent<MonsterBase>();
            EditorDebug.Log(hit.collider.name + "Hit");
        }

        return hitMonster;
    }
    
}
