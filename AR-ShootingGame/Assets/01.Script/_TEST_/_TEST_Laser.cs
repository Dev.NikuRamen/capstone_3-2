using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class _TEST_Laser : MonoBehaviour
{
    [SerializeField] private LaserAttack laserAttack;

    // Update is called once per frame
    void Update()
    {
        if ( Input.GetButton("Fire1") )
        {
            laserAttack.Fire();
        }
    }
}
