using System.Collections;
using UnityEngine;

public class EffectController : MonoBehaviour
{
    [SerializeField] private ParticleSystem particle;
    [SerializeField] private bool isDestroyable = false;

    private void Awake()
    {
        if ( !particle )
        {
            particle = GetComponentInChildren<ParticleSystem>();
        }
    }

    private void OnEnable()
    {
        StartCoroutine(AutoDestroy());
    }

    private IEnumerator AutoDestroy()
    {
        while( !particle.isStopped )
        {
            yield return new WaitForEndOfFrame();
        }

        if ( isDestroyable )
        {
            Destroy(gameObject);
        }
        else
        {
            gameObject.SetActive(false);
        }

    }
    
}
