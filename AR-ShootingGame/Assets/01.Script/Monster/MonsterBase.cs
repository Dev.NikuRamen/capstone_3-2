using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using ARLocation;
using UnityEngine;
using Random = System.Random;

[System.Serializable]
public class AnimationSettings
{
	[SerializeField] private string dieAnimationTag;
	[SerializeField] private string hitAnimationTag;
	public string DieAnimTag => dieAnimationTag;
	public string HitAnimTag => hitAnimationTag;
}

public class MonsterBase : MonoBehaviour
{
	[Header("Monster GPS Settings")]
	public PlaceAtLocation placeAtLocation;
	[SerializeField] private double spawnArea = 0.001d;
	
	[Header("Monster status")]
	[SerializeField] private int hp;
	[SerializeField] private ColorType colorType;
	[SerializeField] private int score = 10;
	
	[Header("애니메이션 설정")]
	[SerializeField] private Animator animator;
	[SerializeField] private AnimationSettings animationSettings;

	[Header("SFX")]
	[SerializeField] private AudioClip dieSFX;

	[Header("VFX")]
	[SerializeField] private GameObject dieVFX;

	private bool _isDead = false;
	public ColorType ColorType => colorType;

	private void Awake()
	{
		OnAwake();
	}

	protected virtual void OnAwake()
	{
		if ( !placeAtLocation )
		{
			placeAtLocation = GetComponent<PlaceAtLocation>();
		}

		if ( !animator )
		{
			animator = GetComponentInChildren<Animator>();
		}
	}

	private void Start()
	{
		OnStart(); 
	}

	private void FixedUpdate()
	{
		transform.LookAt(Camera.main.transform);
	}

	protected virtual void OnStart()
	{
		Random random = new Random();
		DVector3 pos = new DVector3(random.NextDouble() * spawnArea * (random.NextDouble() > 0.5f ? -1 : 1),
		                            random.NextDouble() * spawnArea * (random.NextDouble() > 0.5f ? -1 : 1),
		                            random.NextDouble() * spawnArea * (random.NextDouble() > 0.5f ? -1 : 1));
		var data = Resources.Load<LocationData>("WonkwangLocationData");

		var newLocatoin = new Location()
		{
			Latitude = data.Location.Latitude + pos.x,
			Longitude = data.Location.Longitude + pos.z,
			Altitude = data.Location.Altitude + pos.y,
			AltitudeMode = AltitudeMode.Absolute
		};

		placeAtLocation.Location = newLocatoin;
	}
	

	/// <summary>
	/// On monster die. 
	/// </summary>
	public void Die()
	{
		if ( _isDead ) return;

		_isDead = true;
		animator.SetTrigger(animationSettings.DieAnimTag);
		StartCoroutine(StartDissolve());
		
		LevelManager.Instance.ScoreUp(score);
		GameManager.Instance.AudioController.PlaySFX(dieSFX);
	}

	private IEnumerator StartDissolve()
	{
		yield return new WaitForSeconds(5);

		SpawnDieEffect();
		// Play die fx.
		Destroy(gameObject);
	}
	
	/// <summary>
	/// On monster damage.
	/// </summary>
	/// <param name="value">Hit value.</param>
	public void Damage(int value)
	{
		hp -= value;
		SpawnHitEffect();
		if ( hp <= 0 )
		{
			hp = 0;
			Die();
			return;
		}
		animator.SetTrigger(animationSettings.HitAnimTag);
	}
	
	protected virtual void SpawnHitEffect() {}

	protected virtual void SpawnDieEffect()
	{
		var vfx = Instantiate(dieVFX);
		vfx.transform.position = transform.position;
	}
	
}
