using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster_Green : MonsterBase
{
	protected override void SpawnHitEffect()
	{
		base.SpawnHitEffect();
		Debug.Log("Green");
		HitEffectManager.Instance.SpawnHitEffect(ColorType.Green,transform.position);
	}
}
