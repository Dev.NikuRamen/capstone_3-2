using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster_Blue : MonsterBase
{
	protected override void SpawnHitEffect()
	{
		base.SpawnHitEffect();
		Debug.Log("Blue");
		HitEffectManager.Instance.SpawnHitEffect(ColorType.Blue,transform.position);
	}
}
