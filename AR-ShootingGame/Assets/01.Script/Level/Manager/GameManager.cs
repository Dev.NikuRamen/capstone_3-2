using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : DontDestroySingleton<GameManager>
{
    [SerializeField] private Image fadeInImage;
    [SerializeField] private float fadeInTermTime;
    [SerializeField] private AudioController audioController;

    private float _deltaTime = 0;
    public AudioController AudioController => audioController;

    protected override void OnAwake()
    {
        base.OnAwake();
    }

    public void LoadScene(string sceneName)
    {
        StartCoroutine(LoadSceneProcess(sceneName));
    }

    private IEnumerator StartFaceIn()
    {
        _deltaTime = 0;
        Color color = fadeInImage.color;
        while( color.a <= 1 )
        {
            _deltaTime += Time.deltaTime;
            color.a = _deltaTime / fadeInTermTime;
            fadeInImage.color = color;
            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator StartFadeOut()
    {
        _deltaTime = 0;
        Color color = fadeInImage.color;
        while( color.a >= 0 )
        {
            _deltaTime += Time.deltaTime;
            color.a = 1 - _deltaTime / fadeInTermTime;
            fadeInImage.color = color;
            yield return new WaitForEndOfFrame();
        }
    }
    
    private IEnumerator LoadSceneProcess(string sceneName)
    {
        yield return StartFaceIn();
        
        var sceneAsync = SceneManager.LoadSceneAsync(sceneName);
        while( !sceneAsync.isDone )
        {
            yield return new WaitForEndOfFrame();

        }

        yield return StartFadeOut();
    }
    
}
