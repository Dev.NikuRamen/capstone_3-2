using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using ARLocation;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public enum ColorType
{
    Red = 0,
    Green,
    Blue,
	
    MAX
}

public class LevelManager : SingletonMonoBehaviour<LevelManager>
{
    [Header("Monster settings")]
    [SerializeField] private MonsterSpawner monsterSpawner;

    [Header("System settings")]
    [SerializeField] private float gameTime = 120;
    [SerializeField] private Color[] colors;

    [Header("UI settings")]
    [SerializeField] private TextMeshProUGUI ui_gamePlayRemainTimeText;
    [SerializeField] private TextMeshProUGUI ui_scoreText;
    [SerializeField] private Image colorChangeButtonImage;
    [SerializeField] private UI_GameOver gameOverUI;
    
    private ColorType _currentColorType = ColorType.Red;
    private bool _isGamePlaying = false;
    private float _gamePlayRemainTime;
    private int _score = 0;

    public bool IsGamePlaying => _isGamePlaying;
    public int Score => _score;
    public ColorType CurrentColorType => _currentColorType;

    protected override void OnStart()
    {
        base.OnStart();
        StartCoroutine(StartInit());
    }

    public IEnumerator StartInit()
    {
        if ( gameOverUI.gameObject.activeInHierarchy )
        {
            gameOverUI.gameObject.SetActive(false);
        }
        
        // ARLocationProvider의 인스턴스가 생성될 때 까지 대기
        while(!ARLocationProvider.Instance.IsEnabled)
        {
            yield return null;
        }
        Init();
    }

    public void Init()
    {
        _gamePlayRemainTime = gameTime;
        _isGamePlaying = true;
        colorChangeButtonImage.color = colors[0];
        
        monsterSpawner.StartSpawn();
    }

    private void Update()
    {
        if ( _isGamePlaying )
        {
            UpdateCountDownTime();
        }
    }

    private void UpdateCountDownTime()
    {
        _gamePlayRemainTime -= Time.deltaTime;
        ui_gamePlayRemainTimeText.text = ((int)_gamePlayRemainTime).ToString();
        if ( _gamePlayRemainTime <= 0 )
        {
            GameOver();
        }
    }

    public void OnColorTypeChangeButtonClick()
    {
        _currentColorType++;
        if ( _currentColorType > ColorType.MAX - 1 ) 
        {
            _currentColorType = 0;
        }
        colorChangeButtonImage.color = colors[(int)_currentColorType];
    }

    public void ScoreUp(int amount)
    {
        _score += amount;
        ui_scoreText.text = _score.ToString();
    }
    
    public void GameOver()
    {
        _isGamePlaying = false;
        gameOverUI.Popup();
    }
}
