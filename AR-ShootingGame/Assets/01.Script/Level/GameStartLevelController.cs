using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStartLevelController : MonoBehaviour
{
    [SerializeField] private AudioClip bgm;
    
    void Start()
    {
        GameManager.Instance.AudioController.PlayBGM(bgm);
    }
}
