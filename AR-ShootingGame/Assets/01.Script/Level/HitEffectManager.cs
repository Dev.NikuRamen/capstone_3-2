using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitEffectManager : SingletonMonoBehaviour<HitEffectManager>
{
    [SerializeField] private EffectController[] hitEffects;
    
    private ObjectPoolComponent<EffectController> _redHitEffectPool;
    private ObjectPoolComponent<EffectController> _blueHitEffectPool;
    private ObjectPoolComponent<EffectController> _greenHitEffectPool;

    protected override void OnAwake()
    {
        base.OnAwake();
        _redHitEffectPool = new ObjectPoolComponent<EffectController>(hitEffects[0]);
        _greenHitEffectPool = new ObjectPoolComponent<EffectController>(hitEffects[1]);
        _blueHitEffectPool = new ObjectPoolComponent<EffectController>(hitEffects[2]);
        
        _redHitEffectPool.InitPool();
        _greenHitEffectPool.InitPool();
        _blueHitEffectPool.InitPool();
    }

    public void SpawnHitEffect(ColorType type, Vector3 spawnPosition)
    {
        EffectController effect = null;
        switch( type )
        {
            case ColorType.Red:
                effect = _redHitEffectPool.Create();
                break;
            case ColorType.Green:
                effect = _greenHitEffectPool.Create();
                break;
            case ColorType.Blue:
                effect = _blueHitEffectPool.Create();
                break;
            default: return;
        }

        effect.gameObject.transform.position = spawnPosition;
    }
    
}
