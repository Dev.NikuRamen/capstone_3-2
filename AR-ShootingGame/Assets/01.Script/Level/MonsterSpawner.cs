using System;
using System.Collections;
using System.Collections.Generic;
using ARLocation;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class MonsterSpawner : MonoBehaviour
{
    [Header("Spawn option")]
    [SerializeField] private MonsterBase[] spawnObjects;
    [Tooltip("It's must be seconds")]
    [SerializeField] private float spawnDelay; // It's second.
    [SerializeField] private BoxCollider spawnZone;
    

    public List<MonsterBase> SpawnedMonsters { get; } = new List<MonsterBase>();

    

    private void Awake()
    {
        if ( !spawnZone )
        {
            spawnZone = GetComponentInChildren<BoxCollider>();
        }
    }


    /// <summary>
    /// Spawn monster at random location.
    /// </summary>
    public void Spawn()
    {
        var randomSelect = Random.Range(0, spawnObjects.Length);
        var resource = spawnObjects[randomSelect];
        var monster = Instantiate(resource);

        Transform lookAtTransform = Camera.main.transform;
        var rotation = lookAtTransform.rotation;
        rotation = new Quaternion(0,rotation.y,rotation.z,0);
        lookAtTransform.rotation = rotation;
        monster.transform.LookAt(Camera.main.transform);
    }

    /// <summary>
    /// Start spawn process.
    /// </summary>
    public void StartSpawn()
    {
        StartCoroutine(SpawnDelayManager());
    }

    private IEnumerator SpawnDelayManager()
    {
        while( enabled )
        {
            Spawn();
            yield return new WaitForSeconds(spawnDelay);
        }
    }
}
