﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPoolComponent<T> where T : MonoBehaviour
{
	#region Variable

	private readonly List<T> _pools = new List<T>(); // Pool
	private int _count;                              // Count of created object
	private readonly T _prefab;                      // To instantiate prefab
	private Transform _parent;
	private readonly T[] _prefabs;
	private bool _isMultiResource = false;

	#endregion

	public ObjectPoolComponent(T prefab)
	{
		_prefab = prefab;
		_isMultiResource = false;
	}

	public ObjectPoolComponent(T[] prefabs)
	{
		_prefabs = prefabs;
		_isMultiResource = true;
	}


	/// <summary>
	/// Initialized pool
	/// </summary>
	/// <param name="count">To Push count</param>
	/// <param name="container">Parent of spawn transform</param>
	public void InitPool(Transform container = null,int count = 1)
	{
		for (int i = 0; i < count; i++)
		{
			T resource;
			if ( !_isMultiResource )
			{
				resource = _prefab;
			}
			else
			{
				var randomSelect = Random.Range(0, _prefabs.Length);
				resource = _prefabs[randomSelect];
			}
			T go = Object.Instantiate(resource).GetComponent<T>(); // Instance

			// Set Parent transform

			if (container)
			{
				_parent = container;

				go.transform.SetParent(container);

			}

			_pools.Add(go); // Push

			go.gameObject.SetActive(false);
		}
	}


	/// <summary>
	/// Create Pooled object
	/// </summary>
	public T Create()
	{
		// No more pool memory exception
		if (_count > _pools.Count - 1)
		{
			// If all pool object is active
			if (_pools[0].gameObject.activeSelf)
			{
				InitPool(_parent);
			}
			else
			{
				_count = 0;
			}
		}
		// Spawn
		_pools[_count].gameObject.SetActive(true);
		return _pools[_count++];
	}

	public List<T> GetPool()
	{
		return _pools;
	}
}
