﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EditorDebug : MonoBehaviour
{
	public enum LogType
	{
		Message,
		Error,
		Warning
	}
	/// <summary>
	/// Only print debug at editor.
	/// </summary>
	/// <param name="message">Log message.</param>
	/// <param name="type">Log type.</param>
    public static void Log(string message,LogType type = LogType.Message)
    {
#if UNITY_EDITOR
	    switch( type )
	    {
		    case LogType.Message:
				Debug.Log(message);
			    break;
		    case LogType.Error:
			    Debug.LogError(message);
			    break;
		    case LogType.Warning:
			    Debug.LogWarning(message);
			    break;
	    }
#endif
    }
}
