using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterSearchGuide : MonoBehaviour
{
	[SerializeField] private MonsterSpawner monsterSpawner;
	
	private void Update()
	{
		Search();
	}

	public void Search()
	{
		if ( !monsterSpawner )
		{
			Debug.Log("Monster spawner no instance");
			return;
		}

		var spawnedMonsterList = monsterSpawner.SpawnedMonsters;
		
		foreach( var monster in spawnedMonsterList )
		{
			var camPosition = Camera.main.transform.position;
			var monsterTransform = monster.transform;
			var distance = monsterTransform.position - camPosition;
			var degree = Vector3.Cross(monsterTransform.forward, distance.normalized).y;
			if ( degree > 0f )
			{
				Debug.Log("오른쪽");
			}
			else
			{
				Debug.Log("왼쪽");
			}
			// TODO:: 왼쪽 오른쪽 구분 하는 UI 만들기
		}
	}
	
}
